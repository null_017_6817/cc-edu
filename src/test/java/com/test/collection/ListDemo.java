package com.test.collection;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class ListDemo {

	public static void main(String[] args){
		
		List<String> list = new ArrayList<String>();
		list.add("a");
		list.add("a");
		list.add("b");
		list.add("b");
		list.add("c");
		list.add("c");
		list.add("d");
		list.add("d");
		
		System.out.println("list总长度："+list.size());
		for(int i = 0; i< list.size(); i++){
			System.out.print(" "+list.get(i));
		}
		
		Set<String> set = new HashSet<String>();
		set.addAll(list);
		System.out.println("");
		System.out.println("set总长度："+set.size());
		Iterator<String> iterator = set.iterator();
        while(iterator.hasNext()) {
            System.out.print(" "+iterator.next());
        }
        System.out.println("");
        for(String str : set){
        	System.out.print(" "+str);
        }
        
	}
	
	
	
}
