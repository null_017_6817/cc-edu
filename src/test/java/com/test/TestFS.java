package com.test;

import org.json.JSONObject;

import cn.dreampie.client.Client;
import cn.dreampie.client.ClientRequest;
import cn.dreampie.client.ClientResult;

public class TestFS {

	public static void main(String[] args){
		getToken();
	}
	
	
	
	/**
	 * 获取token
	 * @return
	 */
	public static String getToken() {
		
	
		Client httpClient = new Client("http://192.168.10.241:8085/api/");
	
		ClientRequest postRequest = new ClientRequest("login");// 实例化一个request对象
		
		
		postRequest
				.setJsonParam("{\"username\": \"admin\", \"password\": \"admin\"}");// 设置用户名密码
		ClientResult postResult = httpClient.build(postRequest).post();// post调用
		String json = postResult.getResult();// 返回结果

		JSONObject jsonObject = new JSONObject(json);// 转换
		String token = (String) jsonObject.get("token");// 获取token
		return token;
	}
}
