package com.test.nio;

import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class NioTest {

	public static void main(String[] args){
		
		try{
			
			nioCopyTest3();
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	 private static void   nioCopyTest3() throws IOException {
	        long startTime = System.currentTimeMillis();
	 
	        FileChannel inChannel = FileChannel.open(Paths.get("D:\\BaiduNetdiskDownload\\apache-maven-3.5.0-bin.zip"), StandardOpenOption.READ);
	 
	        FileChannel outChennel = FileChannel.open(Paths.get("E:\\apache-maven-3.5.0-bin-copy.zip"),StandardOpenOption.WRITE,StandardOpenOption.READ,StandardOpenOption.CREATE_NEW);
	 
	        outChennel.transferFrom(inChannel,0,inChannel.size());
	 
	        long end = System.currentTimeMillis();
	        System.out.println("nioCopyTest3耗费时间:"+(end-startTime));
	    }

	
}
