package com.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;

import com.qtrmoon.common.Constant;
import com.qtrmoon.util.EncryptUtil;


public class AuthorizeTest {

	public static void main(String[] args) throws Exception{
		String filename = "sss.jpg";
		String path = System.getProperty("user.dir"); 
		String filePath = path+File.separator+"src\\main\\webapp\\book\\img\\icon"+File.separator+filename;
		File file =  new File(filePath);
		String outfile = path+File.separator+"src\\main\\webapp\\book\\img\\icon"+File.separator+"ss.jpg";
		//指定解码字符集  
		OutputStream out=new FileOutputStream(outfile);
		
		InputStream inn = new FileInputStream(file);
		byte bb [] = new byte [100];
		int len;
		int n=0;
		String endTime = "2025/08/30";
		int endTimeLengeh = 0;
		EncryptUtil enc = new EncryptUtil(Constant.key,Constant.charset);
		String en = enc.encode(endTime);
		endTimeLengeh = en.length();
		while((len=inn.read(bb))>0){
			if(n==0){
				out.write(bb, 0, 2);
				out.write(en.getBytes());
				out.write(bb, 3, len-3-1);
			}else{
				out.write(bb, 0, len);
			}
			n++;
		}
		inn.close();
		out.close();
		
		
		InputStream out2 = new FileInputStream(outfile);
		out2.read(new byte[2]);
		byte[] tm=new byte[24];
		out2.read(tm);
		out2.close();
		String time=new String(tm);
		System.out.println("加密前："+endTime);
		System.out.println("加密后："+time);
		System.out.println("加密后长度："+time.length());
		System.out.println("解密后："+enc.decode(time));
	}
	
}
