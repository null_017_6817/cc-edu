package com.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class ReplaceStrTest {

	public static void main(String[] args){
		
		String rootPath = "D:\\TestEditPath\\20180903\\do\\html";
		String newPath = "D:\\TestEditPath\\html";
		
		String oldStr = "cc-pt";
		String newStr = "cc-edu";
		replaceStr(rootPath,newPath,oldStr,newStr);
	}
	
	public static void replaceStr(String rootPath ,String newPath, String oldStr, String newStr) {
			
		File root = new File(rootPath);
		File[] files = root.listFiles();
		if(files == null || files.length == 0){
			return;
		}
		
		
		for(File file : files){
			if(file != null ){
				
				if(file.isDirectory()){
					System.out.println("path: "+file.getPath());
					
					replaceStr(file.getPath(),file.getPath(),oldStr,newStr);
					
				}else if(file.isFile()){
					if(file.getName() != null && (file.getName().endsWith("html")
						|| file.getName().endsWith("HTML"))){
						
						System.out.println("fileName: " + file.getName());
						String newFilePath = newPath+File.separator +file.getName();
						doFile(file,newFilePath,oldStr,newStr);
					}
					
				}else{
					
				}
				
			}
		}
	}
	
	
	public static void doFile(File file,String newFilePath,String oldStr,String newStr){
		try{
			BufferedReader bufReader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));//数据流读取文件
			
			StringBuffer strBuffer = new StringBuffer();
			for (String temp = null; (temp = bufReader.readLine()) != null; temp = null) {
				if(temp != null && temp.indexOf(oldStr) != -1){ //判断当前行是否存在想要替换掉的字符 -1表示存在
					temp = temp.replace(oldStr, newStr);//替换为你想要的东东
				}
				strBuffer.append(temp);
				strBuffer.append(System.getProperty("line.separator"));//行与行之间的分割
			}
			bufReader.close();
			PrintWriter printWriter = new PrintWriter(newFilePath);//替换后输出的文件位置
			printWriter.write(strBuffer.toString().toCharArray());
			printWriter.flush();
			printWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
