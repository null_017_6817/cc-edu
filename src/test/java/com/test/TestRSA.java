package com.test;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
 
import org.junit.Test;
 
public class TestRSA {
	@Test
	public void testRSA(){
		try {
			//===============生成公钥和私钥，公钥传给客户端，私钥服务端保留==================
			//生成RSA公钥和私钥，并Base64编码
			KeyPair keyPair = RSAUtil.getKeyPair();
			String publicKeyStr = RSAUtil.getPublicKey(keyPair);
			
			//String publicKeyStr = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC3tijbGl+MuzO4TIXNUiVPJPJoD+uY0tj7inR9XBp1hadeipmrmI1x10t40fjSVi9wc5g47ptaOSQBNlfvUwx+g8pvgliMrlHmMLJYePsqc6VQ+BTlLaTCOcJIiO4mloKioOKmAZdTXT2DNJ5ejNyMhUuxdeS6UQlFbOVWoJIXQQIDAQAB";
			String privateKeyStr = RSAUtil.getPrivateKey(keyPair);
			System.out.println("RSA公钥Base64编码:" + publicKeyStr);
			System.out.println("RSA私钥Base64编码:" + privateKeyStr);
			
			//=================客户端=================
			//hello, i am infi, good night!加密
			String message = "3100A390AC58BC3B9EE68583594F4476";
			//将Base64编码后的公钥转换成PublicKey对象
			PublicKey publicKey = RSAUtil.string2PublicKey(publicKeyStr);
			//用公钥加密
			byte[] publicEncrypt = RSAUtil.publicEncrypt(message.getBytes(), publicKey);
			//加密后的内容Base64编码
			String byte2Base64 = RSAUtil.byte2Base64(publicEncrypt);
			System.out.println("公钥加密并Base64编码的结果：" + byte2Base64);
			
			
			//##############	网络上传输的内容有Base64编码后的公钥 和 Base64编码后的公钥加密的内容	 #################
			
			
			
			//===================服务端================
			//将Base64编码后的私钥转换成PrivateKey对象
			PrivateKey privateKey = RSAUtil.string2PrivateKey(privateKeyStr);
			//加密后的内容Base64解码
			byte[] base642Byte = RSAUtil.base642Byte(byte2Base64);
			//用私钥解密
			byte[] privateDecrypt = RSAUtil.privateDecrypt(base642Byte, privateKey);
			//解密后的明文
			System.out.println("解密后的明文: " + new String(privateDecrypt));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/*RSA公钥Base64编码:MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDIIQfMyKfRbMPNx1+15UxDhKgxmivKR5DNsLhn
	JjwR+M7XLGmWP9SHjAtpJem8FG6Fu+xKUD3v0KR8t4bdv1n+GZQv74T15nFv2bgki7GaWv4/svV7
	eVb57kwa2IzeCTHDM3dSaQEpj3tR9njAGPM9076hTO+QwNhE9t5lGemWtwIDAQAB
	RSA私钥Base64编码:MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAMghB8zIp9Fsw83HX7XlTEOEqDGa
	K8pHkM2wuGcmPBH4ztcsaZY/1IeMC2kl6bwUboW77EpQPe/QpHy3ht2/Wf4ZlC/vhPXmcW/ZuCSL
	sZpa/j+y9Xt5VvnuTBrYjN4JMcMzd1JpASmPe1H2eMAY8z3TvqFM75DA2ET23mUZ6Za3AgMBAAEC
	gYBPg8gslmEPNY7KmPevNs7JWNDW/HF/qlWfVCLAYRGm9luAZNYC65OO05bJDVZD8z5ChnsIv78M
	JLLmFqlQO7IOLsvzwq7kf+UQCLlHpcEDMun7IHTo1Ww3DQAUfsT1YAnGwW/fueICfe77SnFsNk1Q
	mddmdoS2gJeScazPI2o4wQJBAOVOgz/Qe5nbAyav79KT8wnvIkMpDApVVG0KEHSV3KMRJWpXIDSN
	evvhXF4g230ItaIlSRHNDfX7lYS1YPT2n+ECQQDfbQEpUCV8V9Gj953hJxUVw5l8L5AAdixrUB/G
	eJoFj9TM03XbcOBVeNrUb4p6N/6fD9bwYAQXk2bUKD3InmmXAkB2a36ClTNPuHg/IXrzv+4ygimq
	j2clav3HtfvbiY1BNViiO05pdWs+6xpdjhBr2PScQS8kH0aHcx67Q0RIHZlhAkAj0sp44UzhXwNS
	EwXRb/7FepNFgirIqCn/Gt29aODpO2KXZOBkyvhtO+96WFgPuKKuxk9kQ0DkttGp+rnNTPkJAkEA
	rAzF1R4k6JXYwb4/4YbDZaPGJ6+ILOq4/MMdhmTj3KSoTZlMOhCCsRnzCTFXYHZ2MX01OeY9bwjF
	oVBmb/fa2A==
	公钥加密并Base64编码的结果：ADeT8rXHP4MmPqxPuE0YCTQoU7kOKQAFF55GC3mhxhtXPrb2m2g6PBnH3VTvh/FrC5TWh4NeK1XL
	DB6odXYLuYcvmRRR+oTZI9IfdyEovSfrIwecHwTWq39RWJ6EsUwZMFzdpl52p3JCWVfUq6sVHI5X
	p/4aYZ6iaL70RHl7d7U=
	解密后的明文: 3100A390AC58BC3B9EE68583594F4476*/
	
}
