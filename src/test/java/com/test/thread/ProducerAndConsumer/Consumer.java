package com.test.thread.ProducerAndConsumer;


public class Consumer implements Runnable{
	
	private Storage storage;
	
	public Consumer(Storage storage){
		this.storage = storage;
	}

	@Override
	public void run() {
		while(true){
			try {
				storage.consume();
				Thread.sleep(3000);
			} catch ( Exception e ){
				e.printStackTrace();
			}
		}
	}
}
