package com.test.thread.ProducerAndConsumer;

import java.util.ArrayList;
import java.util.List;

public class Storage {

	private int MAX_SIZE = 100;
	
	private List<String> list = new ArrayList<String>();
	
	// 生产
	public void produce() {
		synchronized (list) {
			while(list.size() >= MAX_SIZE){
				System.out.println("满了");
				try{
					list.wait();
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			list.add("");
			System.out.println("生产，，当前长度:"+list.size());
			list.notifyAll();
		}
	}
	
	// 消费
	public void consume(){
		synchronized (list) {
			while(list.size() <= 0){
				System.out.println("空了，，当前长度:"+list.size());
				try{
					list.wait();
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			list.remove(0);
			System.out.println("消费，，当前长度:"+list.size());
			list.notifyAll();
		}
	}
}
