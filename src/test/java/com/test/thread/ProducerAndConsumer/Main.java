package com.test.thread.ProducerAndConsumer;

public class Main {

	public static void main(String[] args){
		Storage s = new Storage();
		
		// 生产者对象
		Producer p1 = new Producer(s);
		Producer p2 = new Producer(s);
		
		// 消费者对象
		Consumer c1 = new Consumer(s);
		Consumer c2 = new Consumer(s);
		Consumer c3 = new Consumer(s);
		
		// 生产者线程
		Thread tp1 = new Thread(p1);
		Thread tp2 = new Thread(p2);
		
		// 消费者线程
		Thread tc1 = new Thread(c1);
		Thread tc2 = new Thread(c2);
		Thread tc3 = new Thread(c3);
		
		// 生产者线程
		tp1.start();
		tp2.start();
		
		// 消费者线程
		tc1.start();
		tc2.start();
		tc3.start();
	}
	
}
