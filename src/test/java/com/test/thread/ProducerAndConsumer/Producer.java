package com.test.thread.ProducerAndConsumer;

public class Producer implements Runnable {

	private Storage storage;
	
	public Producer(Storage storage) {
		this.storage = storage;
	}
	
	@Override
	public void run() {
		while(true){
			try{
				storage.produce();
				Thread.sleep(1000);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
}
