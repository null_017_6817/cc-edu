package com.test.thread;


public class TestThread1 {

	public static void main(String[] args) throws InterruptedException{
		
		t1.start();
		t1.join();
		
		t2.start();
		t2.join();
		
		t3.start();
		//t3.join();
		
	}
	
	
	static Thread t1 = new Thread(new Runnable() {
		@Override
		public void run() {
			System.out.println("t1");
		}
	});
		
	static Thread t2 = new Thread(new Runnable() {
		@Override
		public void run() {
			System.out.println("t2");
		}
	});
	
	static Thread t3 = new Thread(new Runnable() {
		@Override
		public void run() {
			System.out.println("t3");
		}
	});
	
}


