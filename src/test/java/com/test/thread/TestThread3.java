package com.test.thread;

import java.util.ArrayList;
import java.util.List;import java.util.concurrent.TimeUnit;


public class TestThread3 {
	
	List<Object> li =  new ArrayList<Object>();
	
	public void add(Object e){
		li.add(e);
	}
	
	public int size(){
		return li.size();
	}
	
	public static void main(String[] args){
		
		TestThread3 c = new TestThread3();
		
		Object lock = new Object();
		
		new Thread(()->{
			synchronized (lock) {
				System.out.println("线程2开始");
				lock.notify();
				if(c.size() != 5){
					try{
						lock.wait();
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				System.out.println("停止");
				lock.notify();
			}
			
		},"t2").start();
		
		new Thread(()->{
			synchronized (lock) {
				for(int i =0 ; c.size() < 10; i++ ){
					c.add(i);
					
					System.out.println("长度："+c.size());
					if(c.size() == 5){
						lock.notify();
						try{
							lock.wait();
						}catch(Exception e){
							e.printStackTrace();
						}
					}
					
					try{
						TimeUnit.SECONDS.sleep(1);
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
			
		},"t1").start();
	}
	
	
}
