package com.test.thread;

import java.util.ArrayList;
import java.util.List;import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;


public class TestThread4 {
	
	List<Object> li =  new ArrayList<Object>();
	
	public void add(Object e){
		li.add(e);
	}
	
	public int size(){
		return li.size();
	}
	
	public static void main(String[] args){
		
		TestThread4 c = new TestThread4();
		
		CountDownLatch latch = new CountDownLatch(1);
		
		
		new Thread(()->{
				System.out.println("线程2开始");
				if(c.size() != 5){
					try{
						latch.wait();
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				System.out.println("停止");
			
		},"t2").start();
		
		new Thread(()->{
			for(int i =0 ; c.size() < 10; i++ ){
				c.add(i);
				
				System.out.println("长度："+c.size());
				if(c.size() == 5){
					try{
						latch.countDown();
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				try{
					TimeUnit.SECONDS.sleep(1);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		},"t1").start();
	}
	
	
}
