package com.test.thread;

import java.util.ArrayList;
import java.util.List;import java.util.concurrent.TimeUnit;


public class TestThread2 {
	
	List<Object> li =  new ArrayList<Object>();
	
	public void add(Object e){
		li.add(e);
	}
	
	public int size(){
		return li.size();
	}
	
	public static void main(String[] args){
		
		TestThread2 c = new TestThread2();
		
		new Thread(()->{
			while(true){
				if(c.size() == 5){
					System.out.println("停止："+c.size()+"======");
					break;
				}
			}
		},"t2").start();
		
		new Thread(()->{
			for(int i =0 ; c.size() < 10; i++ ){
				c.add(i);
				System.out.println("长度："+c.size());
				try{
					TimeUnit.SECONDS.sleep(1);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		},"t1").start();
	}
	
	
}
