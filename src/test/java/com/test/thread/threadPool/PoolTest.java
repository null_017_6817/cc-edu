package com.test.thread.threadPool;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.locks.ReentrantLock;

public class PoolTest {

	
	public static void main(String[] args){
		
		
		ReentrantLock locak = new ReentrantLock();
		
		
		// 在这个例子中，我们首先创建了一个固定长度为5的线程池。然后使用循环的方式往线程池中提交了10个任务，
		// 每个任务休眠1秒。在任务休眠之前，将任务所在的线程id进行打印输出。

		// 所以，理论上只会打印5个不同的线程id，且每个线程id会被打印2次。
		// 是不是这样的呢？检验真理最好的方式就是运行一下。我们看看执行结果如何。

		ExecutorService executor = Executors.newFixedThreadPool(5);
		for (int i = 0; i < 10; i++) {
			executor.submit(() -> {
				System.out.println("thread id is: " + Thread.currentThread().getId());
				try {
					Thread.sleep(1000L);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			});
		}
		
		
		Executor exec=Executors.newSingleThreadExecutor();
		
		
		// 创建单一线程的线程池
		ExecutorService exs1 = Executors.newSingleThreadExecutor();
		// 创建固定数量的线程池
		ExecutorService exs2 = Executors.newFixedThreadPool(5);
		// 创建带缓存的线程池
		ExecutorService exs3 = Executors.newCachedThreadPool();
		// 创建定时调度的线程池
		ScheduledExecutorService exs4 = Executors.newScheduledThreadPool(1000);
		// 创建流式（fork-join）线程池
		ExecutorService exs5 = Executors.newWorkStealingPool();
		
		
	}
	
	
}
