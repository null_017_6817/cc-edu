package com.test;

import java.util.LinkedHashMap;
import java.util.Map;

// 猴子选大王
public class TestMonkeyKing {

	public static void main(String[] args){
		
		int k = 3;
		Map<Integer,String> map = new LinkedHashMap<Integer, String>();
		map.put(1, "a");
		map.put(2, "b");
		map.put(3, "c");
		map.put(4, "d");
		
		int max = map.size();
		int i = 1;
		
		while(map.size() > 1){
			if( !(i%k == 0) ){
				max++;
				map.put(max, map.get(i));
			}
			map.remove(i);
			i++;
		}
		
		
		System.out.print(map.size());
			
			
		
		
	}
	
	
}
