function VccBarConfLoader(confLoadUrl, ctxLoadOkCallBackFunc) {

	this.ctxLoadOkCallBackFunc = ctxLoadOkCallBackFunc;
	//
	this.httpRequest = new MyHttpRequest(confLoadUrl, true);

 	this.httpRequest.setRequestOkFunc(this.loadedOk);
 	this.httpRequest.setRequestBadFunc(this.reportError);
 	this.httpRequest.setItsOwner(this);
}

VccBarConfLoader.prototype.requestConfCtx = function() {
 	this.httpRequest.doPost();
}

VccBarConfLoader.prototype.loadedOk = function(itsRequest, itsSelf) {
	window.status = "电话条初始化信息加载完毕!";
	//
	var jsonText = itsRequest.getResponseText();
	try {
		eval("var vccBarConfCtx = " + jsonText);
		
		//
		itsSelf.ctxLoadOkCallBackFunc(vccBarConfCtx);
	}
	catch(ex) {
		itsSelf.reportError(itsRequest, itsSelf);
	}
}

VccBarConfLoader.prototype.reportError = function(itsRequest, itsSelf) {
	windown.status = "电话条初始化信息加载失败!";
}