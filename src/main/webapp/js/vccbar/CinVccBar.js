/**
File: CinVccBar.js
History: 
1.0.0 wangdejun  2010/03/04 created
1.0.1 wangshuda  2012/05/10 modified.
**/

var CinVccBar;

var CinVccBar_Status = {
	"NoAnswer":20,// - 无应答
	"Busy":21,// - 忙
	"Unreachable":22,// - 不可达
	"Ring":23,// - 振玲
	"Hook":24,// - 摘机
	"Hang":25,// - 已挂机
	"DialIn":38,// - 话机主动拨入会议
	"PageEndMeeting":39,// - 页面结束会议
	"MeetingEnd":42// - 会议结束
};
if(CinVccBar){
	//CinVccBar Exist
}else{
CinVccBar = {
	bDebuggerVersion: false,
	bEnableLog: false,
	isInFrameSet: false,
	bInit: false,
	targetWin: null,
	logWin: null,
	logDlg: null,
	maxLogs: 1000,
	body: null,
	status: null,
	id : "cinvccbar",
	index: 0,
	header: null,
	operator: null,
	parent: null,
	control: null,
	isShow: true,
	ctrlBtnStatus: "0,1,2,9,15,16",//"0,1,2,3,4,5,6,7,8,9,10,11,13,15,16,17,18,19,20,21,22,23,24,25,26,27",
	lastSize:{w:'100%', h:'48px'},
	evtCallBacks:[],
	isShowSelTaskDlg: true,
	curAgentInfo:{
		busy:true,
		idle:false,
		btnIDS:'0|1|2|9|15|16'
	},
	event:{
		ComingCall:{},
		OnCallRing:{},
		RecordFileUpLoaded:{},
		OnCallEnd:{},
		ReportBtnStatus:{},
		OnTelephoneStatusChanged:{},
		OnAgentStatusChanged:{},
		AnswerCall:{},
		OnPrompt:{},
		OnQueueStatusChanged:{},
		OnlockStatusReportChanged:{},
		OnInitalSuccess:{},
		OnShortCutKey:{},
		OnIVRStatusChanged:{},
		OnEventPrompt:{},
		OnAgentWorkReport:{},
		OnInitalFailure:{},
		OnAgentReport:{},
		OnIvrReport:{},
		OnTelReport:{},
		OnServiceReport:{},
		OnTaskReport:{},
		OnOutboundReport:{},
		OnCallReportInfo:{},
		OnCallQueueQuery:{},
		OnQueueReport:{},
		OnCallDataChanged:{},
		OnBarExit:{},
		OnQueryGroupAgentStatus:{},
		OnQueryMonitorSumReport:{}
		
	},
	agentConf:{
		agentType:1,
		passWord:'111111',		//必填写     : From CTI    
		agentID:'0001',			//必填写     : From CTI    
		bindTelNumber:'01088428881',		//必填写     : From SS/ACD   
		mainIP:'192.168.2.95',	//必填写     : From 配置
		backIP:'192.168.2.95',	//必填写     : From 配置
		mainPortID:14800,		//必填写     : From 配置
		backPortID:14800,		//必填写     : From 配置
		corpVccId:'100992',		//必填写     : From CTI 
		monitorIP:'192.168.2.95',//必填写		AppType为1或者2的时候
		monitorPort:14850,		//必填写		AppType为1或者2的时候
		appType:2,				//必填写 //Agent 0 Monitor 1  agent+minitor 2 Int ：应用类型：0：Agent 1：Monitor 2：agent+minitor 必填
		TimeOut:1000,			//消息超时间
		ftpServerIP:'192.168.2.95',
		ftpServerPort:21,
		ftpUser:'wsj',
		ftpPassWord:'123456',
		ftpDirectory:'/data/wushengjun/wsj',
		sipServerIP:'192.168.2.95',			//必填写		PhonType为1的时候
		sipServerPort:5060,					//必填写		PhonType为1的时候   SS配置
		sipProtocol:'UDP',					//必填写    	PhonType为1的时候   固定 
		phonType:1,							//必填写        固定1  //0:内置坐席卡；1：内置Sip；2：外置其他终端；3：外置sip电话；（特殊4为前转到软交换的号码）
		selfPrompt:1,						//必填写		有错误发生时，电话条是否用Message框来提示 可选提示：１,外部处理：0 默认自提示
		sipAuthType:0,						//Int ：鉴权方式：1: 鉴权0：没有鉴权	必填
		sipServerAuthType:0,						//Int ：鉴权方式：1: 鉴权0：没有鉴权	必填
		sipDomain:'',						//所在域		必填
		sipPassWord:'',						//密码		必填
		sipBackServerIP:'',					//Sip备份服务器，最大长度12。  必填
		sipBackServerPort:5040,					//Int ：Sip备份服务器端口号。  必填
		sipBackProtocol:'UDP',					//备份协议方式。默认“UDP”。  必填
		sipBackAuthType:0,					//备份服务器上鉴权方式：1:鉴权 0：没有鉴权  必填
		sipBackDomain:'',					//备份服务器上所在域   必填
		sipBackPassWord:'',					//备份服务器上密码  必填
		isAllTimeRecord:0,					//是否全程录音   是：1   否：0  只读
		recordType:0,					//录音方式：0：服务器端录音 1：客户端录音  只读
		msgFlag:1,							/*String：接受质检消息类型，只有在AppType为非0的时候才能使用；含义：0000000000： 位含义
													1：座席状态：接收坐席状态报告标志
													0：不收 1：收
													2：电话状态：接收排队状态报告标志
													0：不收 1：收简单 2：收详细
													3：排队状态：接收电话状态报告标志
													0：不收 1：收
															必填*/
		MinotorGroupID:'',						//String 16  监控组的编号
		telForwardNumber:'',
		sipPassWdCryptType:'0'					//是否加密sip密码 DES加密
	},
	retVal:'',
	agentFuncInfo:{
		
	},
	
	/**********************************************************************************
	 * //private variables:
	 **********************************************************************************
	 */
	_agentConf_:{	//agentConf的名称到控件名称的转换
		agentType:'AgentType',
		passWord:'PassWord',    
		agentID:'AgentID',    
		bindTelNumber:'Dn',   
		mainIP:'MainIP',
		backIP:'BackIP',
		mainPortID:'MainPortID',
		backPortID:'BackPortID',
		corpVccId:'MediaFlag', 
		monitorIP:'MonitorIP',
		monitorPort:'MonitorPort',
		appType:'AppType',
		timeOut:'TimeOut',
		
		sipServerIP:'SipServerIP',
		sipServerPort:'SipServerPort',
		sipProtocol:'SipProtocol', 
		phonType:'PhonType',
		selfPrompt:'SelfPrompt',
		sipAuthType:'SipAuthType',
		sipServerAuthType:'SipAuthType',
		sipDomain:'SipDomain',	
		sipPassWord:'SipPassWord',	
		sipBackServerIP:'SipBackServerIP',	
		sipBackServerPort:'SipBackServerPort',	
		sipBackProtocol:'SipBackProtocol',	
		sipBackAuthType:'SipBackAuthType',
		sipBackDomain:'SipBackDomain',
		sipBackPassWord:'SipBackPassWord',	
		isAllTimeRecord:'IsAllTimeRecord',
		recordType:'RecordType',	
		appType:'AppType',
		minotorGroupID:'MinotorGroupID',
		telForwardNumber:'SipDn',
		sipPassWdCryptType:'SipPassWdCryptType',
		extSipServerRegPwd:'SipPassWord',
		confAgtInnerCall:'AutoSelectAgent'	//是否限定座席可咨询其它座席。0为不限定
	},
	humanTaskList:[],
	curTaskId:'',
	returnCodeTranslater:{
		'Common':{
			0:{'code':0, 'desc':'Success', 'description':'成功'},
			1:{'code':1, 'desc':'Fail', 'description':'失败'}
		},
		'GetBtnStatus':{//1,2,5,7,12,4 需详写
			0:{'code':0, 'desc':'Success', 'description':'成功'},
			1:{'code':1, 'desc':'Fail', 'description':'失败'}
		},
		'GetCallID':{//返回callID  返回空的话，代表当前座席不在呼叫中
			0:{'code':0, 'desc':'Success', 'description':'成功'},
			1:{'code':1, 'desc':'Fail', 'description':'失败'}
		},
		'GetCTICalloutTask':{ //非空：成功；空：不存在正在进行的任务编号
			0:{'code':0, 'desc':'Success', 'description':'成功'},
			1:{'code':1, 'desc':'Fail', 'description':'失败'}
		},
		'QuerySPGroupList':{//本功能得到某个技能组的中某些状态的座席
		
			0:{'code':0, 'desc':'Success', 'description':'成功'},
			1:{'code':1, 'desc':'Fail', 'description':'失败'}
		},
		'QueryGroupAgentStatus':{ // 查询指定组的座席状态 
			0:{'code':0, 'desc':'Success', 'description':'成功'},
			1:{'code':1, 'desc':'Fail', 'description':'失败'}
		},
		'QueryMonitorSumInfo':{ // （指定参数统计信息查询结果报告
			0:{'code':0, 'desc':'Success', 'description':'成功'},
			1:{'code':1, 'desc':'Fail', 'description':'失败'}
		},
		
		'CallQueueQuery':{ // 查询排队信息
			0:{'code':0, 'desc':'Success', 'description':'成功'},
			1:{'code':1, 'desc':'Fail', 'description':'失败'}
		},
		
		'GetTransfer':{//非空：成功  参数格式：前转号码|前转状态（0：启用 1：暂停）|坐席接听方式（0：内部号码接听 1：前转号码接听）；空：失败
			0:{'code':0, 'desc':'Success', 'description':'成功'},
			1:{'code':1, 'desc':'Fail', 'description':'失败'}
		},
		'GetMonitoredAgentList':{
			0:{'code':0, 'desc':'Success', 'description':'成功'},
			1:{'code':1, 'desc':'Fail', 'description':'失败'}
		},
		'QueryPreViewCallOutNumbers':{//查询并分配预览外呼号码
		
			0:{'code':0, 'desc':'Success', 'description':'成功'},
			1:{'code':1, 'desc':'Fail', 'description':'失败'}
		},
		'GetCallInfo':{//得到本座席监控的坐席列表。
			0:{'code':0, 'desc':'Success', 'description':'成功'},
			1:{'code':1, 'desc':'Fail', 'description':'失败'}
		}
	},
	MODE: {
		AGENT:'agent',
		MONITOR:'monitor'
	},
	btnCodeNameMap: {
		SetWrapUp:		{code:  0, name: 'SetWrapUp', 		desc: '后续态'},
		SetBusy:		{code:  1, name: 'SetBusy', 		desc: '示忙'},
		SetIdle:		{code:  2, name: 'SetIdle', 		desc: '示闲'},
		MakeCall:		{code:  3, name: 'MakeCall', 		desc: '呼出'},
		CallIn:			{code:  3, name: 'CallIn', 			desc: '呼出'},
		Hold:			{code:  4, name: 'Hold', 			desc: '保持'},
		RetrieveHold:	{code:  5, name: 'RetrieveHold', 	desc: '接回'},
		Disconnect:		{code:  6, name: 'Disconnect', 		desc: '挂断'},
		Transfer:		{code:  7, name: 'Transfer', 		desc: '转移'},
		Conference:		{code:  8, name: 'Conference', 		desc: '会议'},
		Answer:			{code:  9, name: 'Answer', 			desc: '应答'},
		TransferOut:	{code: 10, name: 'TransferOut', 	desc: '转出'},
		Consult:		{code: 11, name: 'Consult', 		desc: '咨询'},
		SendDTMF:		{code: 12, name: 'SendDTMF', 		desc: '二次拨号'},
		Bridge:			{code: 13, name: 'Bridge', 			desc: '桥接'},
		Alternate:		{code: 14, name: 'Alternate', 		desc: '切换'},
		Configurate:	{code: 15, name: 'Configurate', 	desc: '设置'},
		FroceReset:		{code: 16, name: 'FroceReset', 		desc: '强制复位'},
		BeginRecord:	{code: 17, name: 'BeginRecord', 	desc: '录音'},
		StopRecord:		{code: 18, name: 'StopRecord', 		desc: '停止录音'},
		Listen:			{code: 19, name: 'Listen', 			desc: '监听'},
		Insert:			{code: 20, name: 'Insert', 			desc: '强插'},
		Intercept:		{code: 21, name: 'Intercept', 		desc: '拦截、代答'},
		ForeReleaseCall:{code: 22, name: 'ForeReleaseCall', desc: '强拆'},
		BeginPlay:		{code: 23, name: 'BeginPlay', 		desc: '放音'},
		StopPlay:		{code: 24, name: 'StopPlay', 		desc: '结束放音'},
		Lock:			{code: 25, name: 'Lock', 			desc: '加锁'},
		UnLock:			{code: 26, name: 'UnLock', 			desc: '解锁'},
		Mute:			{code: 27, name: 'Mute', 			desc: '静音'},
		CallBack:		{code: 28, name: 'CallBack', 		desc: '返回'},
		ReCall:			{code: 29, name: 'ReCall', 			desc: '重拨'}
	},
	curBtnCodeNameMap: {
		outCall:		{code: '3', 	name: 'outCall', 		desc: '呼出功能'},
		keepCall:		{code: '4,5', 	name: 'keepCall', 		desc: '呼叫保持'},
		activeOver:		{code: '6', 	name: 'activeOver', 	desc: '主动挂断'},
		forwardFlag:	{code: '7,11', 	name: 'forwardFlag', 	desc: '转移功能'},
		meetingFlag:	{code: '8,11', 	name: 'meetingFlag', 	desc: '会议功能'},
		passFlag:		{code: '10', 	name: 'passFlag', 		desc: '转出功能'},
		askingFlag:		{code: '11', 	name: 'askingFlag', 	desc: '咨询功能'},
		bridgeFlag:		{code: '13', 	name: 'bridgeFlag', 	desc: '桥接功能'},
		recordFlag:		{code: '17,18', name: 'recordFlag', 	desc: '录音功能'},
		listenFlag:		{code: '19', 	name: 'listenFlag', 	desc: '监听'},
		insertFlag:		{code: '20', 	name: 'insertFlag', 	desc: '强插'},
		proxyFlag:		{code: '21', 	name: 'proxyFlag', 		desc: '拦截&代答'},
		forceOverFlag:	{code: '22', 	name: 'forceOverFlag', 	desc: '强拆'},
		pronounceFlag:	{code: '23,24', name: 'pronounceFlag', 	desc: '放音功能'},
		lockFlag:		{code: '25,26', name: 'lockFlag', 		desc: '加/解锁'},
		muteFlag:		{code: '27', 	name: 'muteFlag', 		desc: '静音/取消静音'}
	},
	curBtnPosCodeMap: [0,1,2,9,6,3,4,5,11,7,8,10,12,15,16,19,20,21,22,13,23,24,25,26,27,17,18],
	
	curBlackBtnList: [23,24,25,26,17,18],
	_commingCallDetail:{
		callingTime:0,
		isCalling: false,
		isAlerting: false,
		userNo: '',
		callID: '',
		userParam: '',
		serviceDirect: '',
		answerTime: 0,
		endTime: 0,
		localAnswerTime: 0,
		destNumber: '',
		callData:'',		//透明参数
		oralCalled:'',
		srfEvent:0,
		srfValue:'',
		taskId: '',
		areaCode: '',
		recordFileName: '',
		ringRecordFile: '',
		accessGwId: ''
	},
	/**********************************************************************************
	 * //public methods:
	 **********************************************************************************
	 */
	//Extend methods from JavaScript
	onMouseOver: function(evt){
		//
	},
	onMouseOut: function(evt){
	},
	Hidden: function(){
		if(arguments.length > 0){
			if(!arguments[0]){
				this.isShow = true;
			}else{
				this.isShow = false;
			}
		}else{
			this.isShow = !this.isShow;
		}
		if(this.isShow){
			this.body.style.width = this.lastSize.w;// = '100%';
			this.body.style.height = this.lastSize.h;// = this.body.style.height;
		}else{
			this.lastSize.w = this.body.style.width;
			this.lastSize.h = this.body.style.height;
			this.body.style.width = "0px";
			this.body.style.height = "0px";
		}
		return this.isShow;
	},
	initContrlDefaultValue: function(){
		var errorCount = 0;
		var realKey = '';
		if(this.agentConf['sipPassWdCryptType']){
		}else{
			this.agentConf['sipPassWdCryptType'] = '0';
		}
		for(var key in this.agentConf){
			if(key == 'propertiesCount'){
				continue;
			}
			realKey = key + '';
			realKey = (realKey in this._agentConf_)?this._agentConf_[realKey]:realKey;
			if(key == 'phonType' && this.agentConf[key] == 4){
				this.agentConf[key] = '1';
			}
			try{
				if(this.agentConf[key].constructor == String){
					eval('this.control.' + realKey + ' = "' + this.agentConf[key] + '"');
				}else{
					eval('this.control.' + realKey + ' = ' + this.agentConf[key]);
				}
			}catch(e){
				if(this.bDebuggerVersion){
					this.debugMsg("Error in initContrlDefaultValue: key->" + key + " ; value->" + this.agentConf[key]);
				}
				errorCount = errorCount + 1;
			}
		}
		try{
			if(this.curTaskId != ''){
				this.control.TaskID = this.curTaskId;
			}else{
				this.control.TaskID = '0';//默认进内呼
			}
		}catch(e){
		}
		return errorCount;
	},
	init: function(father, targetWindow, logWin, id){
		if(this.bInit){
			return;
		}
		this.body = document.createElement("DIV");
		this.body.className = "CinVccBar";
		this.parent = father;
		if(arguments.length > 3){
			this.id = id;
		}
		if(arguments.length > 2){
			this.logWin = logWin;
		}
		if(arguments.length > 1){
			this.isInFrameSet = true;
			this.targetWin = targetWindow;
		}else{
			this.targetWin = window;
		}
		this.body.value = this.id;
		this.body.innerHTML = "<OBJECT id='" + this.id + "' height='48' width='100%' classid='clsid:3E006E71-9408-4F24-9F6D-69C81BD5682F'><PARAM NAME='_Version' VALUE='65536'><PARAM NAME='_ExtentX' VALUE='25321'><PARAM NAME='_ExtentY' VALUE='794'><PARAM NAME='_StockProps' VALUE='0'></OBJECT>";
		
		father.appendChild(this.body);
		this.createEventCallBack("ComingCall(CallingNo,CalledNo,OrgCalledNo,CallData,SerialID,ServiceDirect,CallID, UserParam, TaskID, AreaCode, RingRecordFile, AccessGwId)");
		this.createEventCallBack("OnCallRing(CallingNo,CalledNo,OrgCalledNo,CallData,SerialID,ServiceDirect,CallID, UserParam, TaskID, UserDn, AgentDn, AreaCode, RingRecordFile, AccessGwId)");
		this.createEventCallBack("RecordFileUpLoaded(FileName,Directory,BgnTime,EndTime,UserNo,SerialID,ServiceDirect, CallID)");
		this.createEventCallBack("OnCallEnd(CallID, SerialID, ServiceDirect, UserNo, BgnTime, EndTime, AgentAlertTime, UserAlertTime, FileName, Directory, DisconnectType, UserParam, TaskID, MsServerName, AccessGwId)");
		this.createEventCallBack("AnswerCall(UserNo,AnswerTime,SerialID,ServiceDirect, CallID, UserParam, TaskID)");
		this.createEventCallBack("OnPrompt(code,description)");
		this.createEventCallBack("ReportBtnStatus(btnIDS)");
		this.createEventCallBack("OnTelephoneStatusChanged(vccID,deviceID,status)");
		this.createEventCallBack("OnAgentStatusChanged(vccID,agentID,deviceID,status)");
		this.createEventCallBack("OnQueueStatusChanged(QueueInfo)");
		this.createEventCallBack("OnlockStatusReport(vccID, agentID, deviceID, flag)");
		this.createEventCallBack("OnInitalSuccess()");
		this.createEventCallBack("OnShortCutKey(strkey,value)");
		this.createEventCallBack("OnIVRStatusChanged(IVRInfo)");
		this.createEventCallBack("OnEventPrompt(eventIndex, eventParam)");
		this.createEventCallBack("OnAgentWorkReport(workStatus, description)");
		this.createEventCallBack("OnInitalFailure(code, description)");
		this.createEventCallBack("OnAgentReport(AgentReportInfo)");
		this.createEventCallBack("OnIvrReport(IvrReportInfo)");
		this.createEventCallBack("OnTelReport(TelReportInfo)");
		this.createEventCallBack("OnServiceReport(ServiceReportInfo)");
		this.createEventCallBack("OnTaskReport(TaskReportInfo)");
		this.createEventCallBack("OnOutboundReport(OutboundReportInfo)");
		this.createEventCallBack("OnCallReportInfo(CallReportInfo)");
		this.createEventCallBack("OnCallQueueQuery(queueInfo)");
		this.createEventCallBack("OnQueueReport(queueInfo)");
		this.createEventCallBack("OnCallDataChanged(callldata)");
		this.createEventCallBack("OnBarExit(code, description)");
		this.createEventCallBack("OnQueryGroupAgentStatus(queueInfo)");
		this.createEventCallBack("OnQueryMonitorSumReport(cmdName, xmlRptStr)");
		
		this.unloadFunc = function(){CinVccBar.unInit();};
		window.attachEvent('onunload', CinVccBar.unloadFunc);
		//window.attachEvent('onbeforeunload', function(){CinVccBar.UnInitial();});
		//top.top.attachEvent('onunload', function(){alert('on top unload');});
		//top.top.attachEvent('onbeforeunload', function(){alert('before top unload');});
		
		this.control = eval(this.id);

		/*
		var ret = this.initContrlDefaultValue();
		if(ret > 0){
			this.debugMsg("初始化默认值时存在" + ret + "个错误");
		}
		*/
		CinVccBar.debugMsg("呼叫信息列表");
		if(document.readyState=="complete"){
			setTimeout("try{OnCinVccBarLoaded();}catch(e){}", 0);
		}
		this.bInit = true;
	},
	showMode: function(mode){
		switch(mode.toLowerCase()){
			case 'monitor':
				this.Hidden(true);
				break;
			case 'agent':
			default:
				this.Hidden(false);
				break;
		}
	},
	
	initFuncBtnStr: function(btnInfo){		
		var retStr = '0,1,2,9,';
		if(btnInfo && btnInfo.constructor == Object){
			for(var key in btnInfo){
				if(btnInfo[key] == '1'){
					if(this.curBtnCodeNameMap[key]){
						retStr += this.curBtnCodeNameMap[key].code + ',';
					}
				}
			}
			retStr += '12,15,16';
			var tArray = retStr.split(',');
			tArray.sort(function(a,b){return (parseInt(a) - parseInt(b));});
			var tTmp = [];
			tTmp.push(tArray[0]);	//sure there is a item in tArray at least;
		    for(var i=1; i<tArray.length; i++){
		    	if(tTmp[tTmp.length - 1] == tArray[i]){
		    		continue;
		    	}
		    	tTmp.push(tArray[i]);
		    }
		    var tRe = [];
		    for(var i = 0; i < this.curBtnPosCodeMap.length; i++){
		    	for(var j = 0; j < tTmp.length; j++){
		    		if(this.curBtnPosCodeMap[i] == tTmp[j]){
		    			tRe.push(tTmp[j]);
		    		}
		    	}
		    }
		    tTmp = [];
		    var isBlackKey = false;
		    for(var i = 0; i < tRe.length; i++){
		    	isBlackKey = false;
		    	for(var j = 0; j < this.curBlackBtnList.length; j++){
		    		if(tRe[i] == this.curBlackBtnList[j]){
		    			isBlackKey = true;
		    			break;
		    		}
		    	}
		    	if(isBlackKey){
		    		continue;
		    	}
		    	tTmp.push(tRe[i]);
		    }
		    retStr = tTmp.toString();
		}else if(btnInfo && btnInfo.constructor == String){
			if(parseInt(btnInfo) != NaN){	//Should test btnInfo have ',' ?
				retStr = btnInfo;
			}else{
				retStr = this.ctrlBtnStatus;
			}
		}
		this.ctrlBtnStatus = retStr;
		
	},
	getCallingDetail: function(){
		return this._commingCallDetail;
	},
	getCallingTime: function(){
		if(this._commingCallDetail.isCalling){
			var curTime = new Date();
			return (curTime - this._commingCallDetail.localAnswerTime);
		}
		return this.bigInt2Date(this._commingCallDetail.endTime) - this.bigInt2Date(this._commingCallDetail.answerTime);
	},
	date2BigInt: function(date){ // return like 20091228204650
		if(date.constructor == Date){
			var month = date.getMonth() + 1;
			if(month < 10){
				month = '0' + month;
			}
			var day = date.getDate();
			if(day < 10){
				day = '0' + day;
			}
			var hour = date.getHours();
			if(hour < 10){
				hour = '0' + hour;
			}
			var min = date.getMinutes();
			if(min < 10){
				min = '0' + min;
			}
			var sec = date.getSeconds();
			if(sec < 10){
				sec = '0' + sec;
			}
			return parseInt('' + date.getFullYear() + '' + month + '' + day + '' + hour + '' + min + '' + sec);
		}
		return 0;
	},
	bigInt2Date: function(date){
		var str = date + '';
		var year = str.substr(0, 4);
		var month = str.substr(4, 2);
		var day = str.substr(6, 2);
		var hour = str.substr(8, 2);
		var min = str.substr(10, 2);
		var sec = str.substr(12, 2);
		var d = new Date();
		d.setYear(year);
		d.setMonth(month);
		d.setDate(day);
		d.setHours(hour);
		d.setMinutes(min);
		d.setSeconds(sec);
		d.setMilliseconds(0);
		return d;
	},
	unInit: function(){
		var evt = null;
		if(window.event){
			evt = window.event;
		}
		//should add other functions call to extend it
		if(this.getCallingDetail().isCalling || this.getCallingDetail().isAlerting){
			//this.Disconnect(); //异常时，确保座席通话不被掐断，不能发Disconnect
		}
		this.UnInitial();
		if(evt && evt.type == 'unload'){
			return;
		}
		window.detachEvent('onunload', CinVccBar.unloadFunc);
		this.removeAllEventCallBack();
		this.parent.removeChild(this.body);
		this.bInit = false;
	},
	
	/**********************************************************************************
	 * //public methods: control internal methods
	 **********************************************************************************
	 */
	
	Initial: function(AgentIdNum, bindTelNumber)
	{
		var ret = '';
		var obj = [];
		var pobj = {};
		CinVccBar.logMsg(CinVccBar.Initial, 'Initial');
		CinVccBar.initFuncBtnStr(CinVccBar.agentFuncInfo);
		//this.ctrlBtnStatus = '';
		//this.SerialBtn();
		ret = this.initContrlDefaultValue();
		if(ret > 0){
			this.debugMsg("初始化默认值时存在" + ret + "个错误");
		}
		if(arguments.length > 0){
			try{
				//"1002"
				this.control.AgentID = AgentIdNum;
				//必填写: From SS/ACD  "01088886001"
				this.control.Dn = bindTelNumber;
			}catch(e){CinVccBar.logMsg(e.description);}
		}else{
			//
		}
		this.SerialBtn();
		try{
			ret = this.control.Initial();
			setTimeout("try{OnCinVccBarInitial();}catch(e){}", 0);
		}catch(e){CinVccBar.logMsg(e.description);}
		//this.SerialBtn();
		return ret;
	},
	btnCallOutOnclik: function()
	{
		//if(DestNum.value != "")
		{
		   try
		   {
			//   CinVccBar.control.MakeCall(DestNum.value);
		   }
		   catch(e)
		   {
			   CinVccBar.logMsg(e.description);
		   }
		 }
		//DestNum.value = "";	
	},
	btnForceClearOnclik: function()
	{
		//if(DestNum.value != "")
		{
		   try
		   {
			  // CinVccBar.control.ForeReleaseCall(DestNum.value);
		   }
		   catch(e)
		   {
			   CinVccBar.logMsg(e.description);
		   }
		 }
		 //DestNum.value = "";	
	},
	Insert: function(destAgentID, type, callID){
		CinVccBar.logMsg(CinVccBar.Insert, 'Insert');
		switch(arguments.length){
			case 0:
				return;
				break;
			case 1:
				type = 0;
				callID = "";
				break;
			case 2:
				callID = "";
				break;
			case 3:
				break;
			default:
				break;
		}
	   try{
			return this.control.Insert(destAgentID, type, callID);
	   }catch(e){
			CinVccBar.logMsg(e.description);
	   }
	},
	Listen: function(destAgentID, type, callID){
		CinVccBar.logMsg(CinVccBar.Listen, 'Listen');
		switch(arguments.length){
			case 0:
				return;
				break;
			case 1:
				type = 0;
				callID = "";
				break;
			case 2:
				callID = "";
				break;
			case 3:
				break;
			default:
				break;
		}
		try{
			return this.control.Listen(destAgentID, type, callID);
	    }catch(e){
			CinVccBar.logMsg(e.description);
	    }
	},
	Intercept: function(destAgentID, type, callID){
		CinVccBar.logMsg(CinVccBar.Intercept, 'Intercept');
		switch(arguments.length){
			case 0:
				return;
				break;
			case 1:
				type = 0;
				callID = "";
				break;
			case 2:
				callID = "";
				break;
			case 3:
				break;
			default:
				break;
		}
	   try{
			return this.control.Intercept(destAgentID, type, callID);
	   }catch(e){
			CinVccBar.logMsg(e.description);
	   }
	},
	SendDTMF: function(TapKey){
		CinVccBar.logMsg(CinVccBar.SendDTMF, 'SendDTMF');
		try{
			return this.control.SendDTMF(TapKey);
	   }catch(e){
			CinVccBar.logMsg(e.description);
	   }
	},
	SerialBtn: function(btnIDS){
		CinVccBar.logMsg(CinVccBar.SerialBtn, 'SerialBtn');
		switch(arguments.length){
			case 0:
				btnIDS = this.ctrlBtnStatus;
				break;
			default:
				break;
		}
		try {
			// 监控相关按钮，不在话条中显示
		   var monitorBtnIds4HiddenInBar = '12,13,19,20,21,22';
		   return this.control.SerialBtn(btnIDS, monitorBtnIds4HiddenInBar);
		}catch(e){
		   CinVccBar.logMsg(e.description);
		}
	},
	HiddenBtn: function(btnIds4HiddenInBar){
		try {
			if(btnIds4HiddenInBar && this.ctrlBtnStatus) {
				return this.control.SerialBtn(this.ctrlBtnStatus, btnIds4HiddenInBar);
			}
		}
		catch(e){
		   CinVccBar.logMsg(e.description);
		}
	},
	GetBtnStatus: function(CallNum){
		CinVccBar.logMsg(CinVccBar.GetBtnStatus, 'GetBtnStatus');
		try{
		   return this.control.GetBtnStatus(CallNum);
		}catch(e){
		   CinVccBar.logMsg(e.description);
		}
	},
	MakeCall: function(DestNum, Variant, taskID, transParentParam, phoneIdInTask){
		CinVccBar.logMsg(CinVccBar.MakeCall, 'MakeCall');
		this._commingCallDetail.destNumber = DestNum;
		this._commingCallDetail.oralCalled = DestNum;
		this._commingCallDetail.userParam = transParentParam;
		
		var callRtn = -1;
		try {
			var argCnt = arguments.length;
			
			switch(argCnt) {
				case 5 : {
					callRtn = this.control.MakeCall(DestNum, Variant, taskID, transParentParam, phoneIdInTask);
					break;
				}
				case 4 : {
					callRtn = this.control.MakeCall(DestNum, Variant, taskID, transParentParam);
					break;
				}
				case 3 : {
					callRtn = this.control.MakeCall(DestNum, Variant, taskID);
					break;
				}			
				default: {
					callRtn = this.control.MakeCall(DestNum, Variant);
					break;
				}
			}
		}
		catch(ex) {
			alert("MakeCall Failed: " + ex.description);
		}
		return callRtn;
	},
	CallIn: function(DestAgentID, serviceDirect, taskID, transParentParam){
		CinVccBar.logMsg(CinVccBar.CallIn, 'CallIn');
		try{
			if(arguments.length > 3){
				return this.control.CallIn(DestAgentID, serviceDirect, taskID, transParentParam);
			}else if(arguments.length > 2){
				return this.control.CallIn(DestAgentID, serviceDirect, taskID);
			}else{
				return this.control.CallIn(DestAgentID, serviceDirect);
			}
		}catch(e){
		   CinVccBar.logMsg(e.description);
		}
	},
	TransferOut: function(lTransferType, DestNum){
		CinVccBar.logMsg(CinVccBar.TransferOut, 'TransferOut');
		try{
		   return this.control.TransferOut(lTransferType, DestNum);
		}catch(e){
		   CinVccBar.logMsg(e.description);
		}
	},
	Bridge: function(IVRNum, bEndCall){
		CinVccBar.logMsg(CinVccBar.Bridge, 'Bridge');
		try{
		   return this.control.Bridge(IVRNum, bEndCall);
		}catch(e){
		   CinVccBar.logMsg(e.description);
		}
	},
	SetBusy: function(subStatus){
		CinVccBar.logMsg(CinVccBar.SetBusy, 'SetBusy');
		if(arguments.length < 1){
			subStatus = 0;
		}
		try{
		   return this.control.SetBusy(subStatus);
		}catch(e){
		   CinVccBar.logMsg(e.description);
		}
	},
	SetIdle: function(){
		CinVccBar.logMsg(CinVccBar.SetIdle, 'SetIdle');
		try{
		   return this.control.SetIdle();
		}catch(e){
		   CinVccBar.logMsg(e.description);
		}
	},
	SetWrapUp: function(){
		CinVccBar.logMsg(CinVccBar.SetWrapUp, 'SetWrapUp');
		try{
		   return this.control.SetWrapUp();
		}catch(e){
		   CinVccBar.logMsg(e.description);
		}
	},
	Hold: function(){
		CinVccBar.logMsg(CinVccBar.Hold, 'Hold');
		try{
		   return this.control.Hold();
		}catch(e){
		   CinVccBar.logMsg(e.description);
		}
	},
	RetrieveHold: function(){
		CinVccBar.logMsg(CinVccBar.RetrieveHold, 'RetrieveHold');
		try{
		   return this.control.RetrieveHold();
		}catch(e){
		   CinVccBar.logMsg(e.description);
		}
	},
	Disconnect: function(){
		CinVccBar.logMsg(CinVccBar.Disconnect, 'Disconnect');
		try{
		   return this.control.Disconnect();
		}catch(e){
		   CinVccBar.logMsg(e.description);
		}
	},
	Conference: function(){
		CinVccBar.logMsg(CinVccBar.Conference, 'Conference');
		try{
		   return this.control.Conference();
		}catch(e){
		   CinVccBar.logMsg(e.description);
		}
	},
	Answer: function(){
		CinVccBar.logMsg(CinVccBar.Answer, 'Answer');
		try{
		   return this.control.Answer();
		}catch(e){
		   CinVccBar.logMsg(e.description);
		}
	},
	Consult: function(lConsultType, ConsultNum){
		CinVccBar.logMsg(CinVccBar.Consult, 'Consult');
		try{
		   return this.control.Consult(lConsultType, ConsultNum);
		}catch(e){
		   CinVccBar.logMsg(e.description);
		}
	},
	Transfer: function(){
		CinVccBar.logMsg(CinVccBar.Transfer, 'Transfer');
		try{
		   return this.control.Transfer();
		}catch(e){
		   CinVccBar.logMsg(e.description);
		}
	},
	ForeReleaseCall: function(DestAgentID){
		CinVccBar.logMsg(CinVccBar.ForeReleaseCall, 'ForeReleaseCall');
		try{
		   return this.control.ForeReleaseCall(DestAgentID);
		}catch(e){
		   CinVccBar.logMsg(e.description);
		}
	},
	btnClickOnclik: function()
	{
		CinVccBar.logMsg(CinVccBar.btnClickOnclik, 'btnClickOnclik');
	   try
	   {
		   CinVccBar.control.ClickBtnItem(DestNum.value,0);
	   }
	   catch(e)
	   {
		   CinVccBar.logMsg(e.description);
	   }
	},
	BeginPlay: function(destAgentID, destDeviceID, nType, fileName){
		CinVccBar.logMsg(CinVccBar.BeginPlay, 'BeginPlay');
	   try{
		   return this.control.BeginPlay(destAgentID, destDeviceID, nType, fileName);//DestNum.value,"","mnt/192.168.2.4/11000.wav");
	   }catch(e){
		   CinVccBar.logMsg(e.description);
	   }
	},
	StopPlay: function(destAgentID, destDeviceID){
		CinVccBar.logMsg(CinVccBar.StopPlay, 'StopPlay');
	   try{
		   return this.control.StopPlay(destAgentID, destDeviceID);//DestNum.value,"");
	   }catch(e){
		   CinVccBar.logMsg(e.description);
	   }
	},
	BeginRecord: function(destAgentID, fileName){
		CinVccBar.logMsg(CinVccBar.BeginRecord, 'BeginRecord');
		switch(arguments.length){
			case 0:
				return;
			case 1:
				fileName = "";
			default:
				break;
		}
		try{
			return this.control.BeginRecord(destAgentID, fileName);
		}catch(e){
		   CinVccBar.logMsg(e.description);
		}
	},
	StopRecord: function(destAgentID){
		CinVccBar.logMsg(CinVccBar.StopRecord, 'StopRecord');
	   try{
		   return this.control.StopRecord(destAgentID);
	   }catch(e){
		   CinVccBar.logMsg(e.description);
	   }
	},
	ForceReset: function(){
		CinVccBar.logMsg(CinVccBar.ForceReset, 'ForceReset');
		try{
			return this.control.ForceReset();
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	Configurate: function(Params){
		CinVccBar.logMsg(CinVccBar.Configurate, 'Configurate');
		try{
			return this.control.Configurate(Params);
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	GetConfiguration: function(){
		CinVccBar.logMsg(CinVccBar.GetConfiguration, 'GetConfiguration');
		try{
			return this.control.GetConfiguration();
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	Mute: function(flag){
		CinVccBar.logMsg(CinVccBar.Mute, 'Mute');
		try{
			return this.control.Mute(flag);
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	Lock: function(destAgentID){
		CinVccBar.logMsg(CinVccBar.Lock, 'Lock');
		try{
			return this.control.Lock(destAgentID);
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	UnLock: function(destAgentID){
		CinVccBar.logMsg(CinVccBar.UnLock, 'UnLock');
		try{
			return this.control.UnLock(destAgentID);
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	ForceIdle: function(destAgentID){
		CinVccBar.logMsg(CinVccBar.ForceIdle, 'ForceIdle');
		try{
			return this.control.ForceIdle(destAgentID);
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	ForceBusy: function(destAgentID){
		CinVccBar.logMsg(CinVccBar.ForceBusy, 'ForceBusy');
		try{
			return this.control.ForceBusy(destAgentID);
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	ForceOut: function(destAgentID){
		CinVccBar.logMsg(CinVccBar.ForceOut, 'ForceOut');
		try{
			return this.control.ForceOut(destAgentID);
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	AlterNate: function(destDeviceID){
		CinVccBar.logMsg(CinVccBar.AlterNate, 'AlterNate');
		try{
			return this.control.AlterNate(destDeviceID);
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	CallBack: function(){
		CinVccBar.logMsg(CinVccBar.CallBack, 'CallBack');
		try{
			return this.control.CallBack();
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	ReCall: function(){
		CinVccBar.logMsg(CinVccBar.ReCall, 'ReCall');
		try{
			return this.control.ReCall();
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	ChangeCallQueue: function(calling, sid, orderid){
		CinVccBar.logMsg(CinVccBar.ChangeCallQueue, 'ChangeCallQueue');
		try{
			return this.control.ChangeCallQueue(calling, sid, orderid);
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	QueryCallQueue: function(serviceId){
		CinVccBar.logMsg(CinVccBar.QueryCallQueue, 'QueryCallQueue');
		try{
			return this.control.QueryCallQueue(serviceId);
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	GetCallID: function(){
		CinVccBar.logMsg(CinVccBar.GetCallID, 'GetCallID');
		try{
			return this.control.GetCallID();
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	SMMsg: function(DestAddress, ShortMessage){
		CinVccBar.logMsg(CinVccBar.SMMsg, 'SMMsg');
		try{
			return this.control.SMMsg(DestAddress, ShortMessage);
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	SetCTICalloutTask: function(TastNum){
		CinVccBar.curTaskId = TastNum;
		CinVccBar.logMsg(CinVccBar.SetCTICalloutTask, 'SetCTICalloutTask');
		try{
			return this.control.SetCTICalloutTask(TastNum);
		}catch(e){CinVccBar.logMsg(e.description);CinVccBar.curTaskId = '';}
	},
	GetCTICalloutTask: function(){
		CinVccBar.logMsg(CinVccBar.GetCTICalloutTask, 'GetCTICalloutTask');
		try{
			return this.control.GetCTICalloutTask();
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	QuerySPGroupList: function(groupID, agentStatus, cmdType, checkAuthor){
		CinVccBar.logMsg(CinVccBar.QuerySPGroupList, 'QuerySPGroupList');
		try{
			return this.control.QuerySPGroupList(groupID, agentStatus, cmdType, checkAuthor); //DestNum.value,2
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	QueryGroupAgentStatus: function(groupIds, action, interval){
		CinVccBar.logMsg(CinVccBar.QueryGroupAgentStatus, 'QueryGroupAgentStatus');
		try{
			return this.control.QueryGroupAgentStatus(groupIds, action, interval); 
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	QueryMonitorSumInfo: function(cmdName, amdParam){
		CinVccBar.logMsg(CinVccBar.QueryMonitorSumInfo, 'QueryMonitorSumInfo');
		try{
			return this.control.QueryMonitorSumInfo(cmdName, amdParam); 
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	CallQueueQuery: function(serviceID){
		CinVccBar.logMsg(CinVccBar.CallQueueQuery, 'CallQueueQuery');
		try{
			return this.control.CallQueueQuery(serviceID);
			
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	ReleaseThirdOne: function(){
		CinVccBar.logMsg(CinVccBar.ReleaseThirdOne, 'ReleaseThirdOne');
		try{
			return this.control.ReleaseThirdOne();
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	GetCallData: function(destAgentID){
		CinVccBar.logMsg(CinVccBar.GetCallData, 'GetCallData');
		try{
			return this.control.GetCallData(destAgentID);
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	SetCallData: function(destAgentID, calldata){
		CinVccBar.logMsg(CinVccBar.SetCallData, 'SetCallData');
		try{
			return this.control.SetCallData(destAgentID, calldata);
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	BeginCollect: function(destAgentID, destDeviceID, playType, filename, minlen, maxlen, end, cel, fto, ito){
		CinVccBar.logMsg(CinVccBar.BeginCollect, 'BeginCollect');
		try{
			return this.control.BeginCollect(destAgentID, destDeviceID, playType, filename, minlen, maxlen, end, cel, fto, ito);
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	StopCollect: function(destAgentID, StopCollect){
		CinVccBar.logMsg(CinVccBar.AlterNate, 'AlterNate');
		try{
			return this.control.StopCollect(destAgentID, calldata);
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	GetTransfer: function(){
		CinVccBar.logMsg(CinVccBar.GetTransfer, 'GetTransfer');
		try{
			return this.control.GetTransfer();
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	SetTransfer: function(destDeviceID, forwardState, answerType){
		CinVccBar.logMsg(CinVccBar.SetTransfer, 'SetTransfer');
		try{
			return this.control.SetTransfer(destDeviceID, forwardState, answerType);
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	GetMonitoredAgentList: function(){
		CinVccBar.logMsg(CinVccBar.GetMonitoredAgentList, 'GetMonitoredAgentList');
		try{
			return this.control.GetMonitoredAgentList();
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	UnInitial: function(){
		CinVccBar.logMsg(CinVccBar.UnInitial, 'UnInitial');
		try{
			return this.control.UnInitial();
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	GetCallInfo: function(){
		CinVccBar.logMsg(CinVccBar.GetCallInfo, 'GetCallInfo');
		try{
			return this.control.GetCallInfo();
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	InitialState: function(){
		CinVccBar.logMsg(CinVccBar.InitialState, 'InitialState');
		try{
			return this.control.InitialState();
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	AgentQuery: function(monitorid, curPos){
		CinVccBar.logMsg(CinVccBar.AgentQuery, 'AgentQuery');
		try{
			if(arguments.length > 1){
				return this.control.AgentQuery(monitorid, curPos);
			}else{
				return this.control.AgentQuery(monitorid);
			}
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	IvrQuery: function(monitorid, curPos){
		CinVccBar.logMsg(CinVccBar.IvrQuery, 'IvrQuery');
		try{
			if(arguments.length > 1){
				return this.control.IvrQuery(monitorid, curPos);
			}else{
				return this.control.IvrQuery(monitorid);
			}
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	TelQuery: function(monitorid, curPos){
		CinVccBar.logMsg(CinVccBar.TelQuery, 'TelQuery');
		try{
			if(arguments.length > 1){
				return this.control.TelQuery(monitorid, curPos);
			}else{
				return this.control.TelQuery(monitorid);
			}
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	ServiceQuery: function(monitorid, curPos, serviceid){
		CinVccBar.logMsg(CinVccBar.ServiceQuery, 'ServiceQuery');
		try{
			if(arguments.length > 2){
				return this.control.ServiceQuery(monitorid, curPos, serviceid);
			}else if(arguments.length > 1){
				return this.control.ServiceQuery(monitorid, curPos);
			}else{
				return this.control.ServiceQuery(monitorid);
			}
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	TaskQuery: function(monitorid, curPos){
		CinVccBar.logMsg(CinVccBar.TaskQuery, 'TaskQuery');
		try{
			if(arguments.length > 1){
				return this.control.TaskQuery(monitorid, curPos);
			}else{
				return this.control.TaskQuery(monitorid);
			}
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	CallReportQuery: function(monitorid, curPos){
		CinVccBar.logMsg(CinVccBar.CallReportQuery, 'CallReportQuery');
		try{
			if(arguments.length > 1){
				return this.control.CallReportQuery(monitorid, curPos);
			}else{
				return this.control.CallReportQuery(monitorid);
			}
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	StartNotification: function(id, type){
		CinVccBar.logMsg(CinVccBar.StartNotification, 'StartNotification');
		try{
			//id: String：参数，监控组的ID，多个组以“,”分割
			//type: String：参数，监控组的类型，多个组以“,”分割
			return this.control.StartNotification(id, type);
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	EndNotification: function(id){
		CinVccBar.logMsg(CinVccBar.EndNotification, 'EndNotification');
		try{
			//id: String：参数，监控组的ID，多个组以“,”分割
			return this.control.EndNotification(id);
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	GetTaskSummary: function(monitorid, taskid){
		CinVccBar.logMsg(CinVccBar.GetTaskSummary, 'GetTaskSummary');
		try{
			return this.control.GetTaskSummary(monitorid, taskid);
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	SetTransparentParameter: function(transparentParameter){
		CinVccBar.logMsg(CinVccBar.SetTransparentParameter, 'SetTransparentParameter');
		try{
			return this.control.SetTransparentParameter(transparentParameter);
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	GetAgentStatus: function(){
		CinVccBar.logMsg(CinVccBar.GetAgentStatus, 'GetAgentStatus');
		try{
			return this.control.GetAgentStatus();
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	GetBusySubStatus: function(){
		CinVccBar.logMsg(CinVccBar.GetBusySubStatus, 'GetBusySubStatus');
		try{
			return this.control.GetBusySubStatus();
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	QueryPreViewCallOutNumbers: function(taskId, agtId, telCnt, realloc){
		CinVccBar.logMsg(CinVccBar.QueryPreViewCallOutNumbers, 'QueryPreViewCallOutNumbers');
		try{
			return this.control.QueryPreViewCallOutNumbers(taskId, agtId, telCnt, realloc);
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	SetUIStyle: function(styleMode){
		CinVccBar.logMsg(CinVccBar.SetUIStyle, 'SetUIStyle');
		try{
			return this.control.SetUIStyle(styleMode);
		}catch(e){CinVccBar.logMsg(e.description);}
	},
	/**********************************************************************************
	 * //private methodes:
	 **********************************************************************************
	 */
	$: function(id){
		if(this.targetWin){
			return this.targetWin.document.getElementById(id);
		}else{
			return document.getElementById(id);
		}
	},
	toString: function(){
		return this.body.toString();
	},
	onOperator: function(obj){
	},
	getObj: function(){
		return this.body;
	},
	setStatus: function(status){
	},
	createEventCallBack: function(eventStr){
		var oEventObject = null;
		try {
			//IE6
			oEventObject = document.createElement("<script language='javascript' for='" + CinVccBar.id + "' event='" + eventStr + "'>");
		}
		catch(ex) {
			// IE7,8,9
			oEventObject = document.createElement('script');
			//
			oEventObject.setAttribute('language', 'javascript');
			oEventObject.setAttribute('for', CinVccBar.id);
			oEventObject.setAttribute('event', eventStr);
		}
		if(oEventObject) {
			//
			oEventObject.text = "CinVccBar.onEvent" + eventStr + ";";
			document.body.appendChild(oEventObject);
			this.evtCallBacks.push(oEventObject);
		}
		else {
			alert("createEventCallBack error!");
		}
	},
	removeAllEventCallBack: function(){
		for(var i = 0; i < this.evtCallBacks.length; i++){
			document.body.removeChild(this.evtCallBacks[i]);
		}
		this.evtCallBacks = [];
	},
	processEventHandler: function(evtHandler, args){
		for(var key in evtHandler){
			if(key == 'propertiesCount'){
				continue;
			}
			try{
				(evtHandler[key]).apply(this, args);
			}catch(e){
				try{
					if((evtHandler[key]).__errorCount){
						delete evtHandler[key];
						this.logMsg(key + ' 因错从事件回调中移除！');
						return;
					}
					(evtHandler[key]).__errorCount = 1;
				}catch(e){
					delete evtHandler[key];
					this.logMsg(key + ' 因错从事件回调中移除！');
				}		
			};
		}
	},
	/**********************************************************************************
	 * //public methodes:
	 **********************************************************************************
	 */
	hasBtn: function(param){
		var keyValue = parseInt(param);
		if(keyValue > 9){
			if(this.curAgentInfo.btnIDS.indexOf(param) >= 0){
				return true;
			}
			return false;
		}
		if(this.curAgentInfo.btnIDS.indexOf(param) >= 0){
			if(this.curAgentInfo.btnIDS.indexOf('|' + param + '|') > 0){
				return true;
			}else if(this.curAgentInfo.btnIDS.substr(0,2) == param + '|'){
				return true;
			}
		}
		return false;
	},
	enableLog: function(param)
	{
		if(param){
			this.bEnableLog = param;
		}else{
			this.bEnableLog = !this.bEnableLog;
		}
		if(this.bEnableLog){
			if(this.logWin){
				this.logWin.style.display = 'block';
				if(this.logWin._parent){
					if(this.$(this.logWin._parent).style.display == 'none'){
						this.$(this.logWin._parent).style.display = 'block';
					}
				}
			}
		}else{
			if(this.logWin){
				this.logWin.style.display = 'none';
				if(this.logWin._parent){
					this.$(this.logWin._parent).style.display == 'none';
				}
			}
		}
	},
	debugMode: function(param)
	{
		if(param){
			this.bDebuggerVersion = param;
		}else{
			this.bDebuggerVersion = !this.bDebuggerVersion;
		}
	},
	getCurTime: function()
	{
		var d = new Date();
		return d.toTimeString().substring(0, 8);
	},
	logMsg: function (obj, name)
	{
		if(!this.bEnableLog){
			return;
		}
		var logStr = '';
		if(!this.logWin){
			return;
		}
		if(arguments.length < 1){
			return;
		}
		//try{
			logStr += 'CinVccBar.logWin.innerHTML += \'<div style="clear:both;">';
		//}catch(e){
		//	return;
		//};
		switch(obj.constructor){
			case String:
			case Number:
				try{
					logStr += this.getCurTime() + '： ' + obj;
					//this.logWin.innerHTML += this.getCurTime() + '： ' + obj;
				}catch(e){}
				break;
			case Function:
				try{
					logStr += this.getCurTime() + '： ';
					if(arguments.length > 1){
						logStr += name + ' ';
					}
					logStr += translateFuncArguments(obj);
				}catch(e){}
				break;
			case Object:
			default:
				try{
					logStr += this.getCurTime() + '： unknown log target';
				}catch(e){}
				break;
		}
		logStr += '</div>\';if(CinVccBar.logWin.children.length > CinVccBar.maxLogs){CinVccBar.logWin.removeChild(CinVccBar.logWin.firstChild);};CinVccBar.logWin.scrollTop=CinVccBar.logWin.scrollHeight;';
		setTimeout(logStr, 0);
		
		/*
		if(this.logWin.children.length > this.maxLogs){
			this.logWin.removeChild(this.logWin.firstChild);
		}
		this.logWin.scrollTop = this.logWin.scrollHeight;
		//*/
		return;
		function translateFuncArguments(o){
			var str = '';
			for(var i = 0; o.arguments && o.arguments.length && i < o.arguments.length; i++){
				str += o.arguments[i] + ', ';
			}
			if(str.length > 1)
				str = str.substr(0, str.length - 2);
			return 'Function(' + str + ')';
		}
	},
	debugMsg: function (str){
		
	},
	addEventHandler: function(evtHandler, handlerName, type){
		if(arguments.length < 2){
			CinVccBar.debugMsg('addEventHandler 调用参数不足!');
			return;
		}
		var handler = null;
		handler = eval('CinVccBar.event.' + type);
		if(!handler){
			CinVccBar.logMsg('addEventHandler中未找到类型 ' + type);
			return;
		}
		if(handler[handlerName]){
			CinVccBar.logMsg('addEventHandler中原 ' + handlerName + ' 已被替换！');
			delete handler[handlerName];
			//return;
		}
		handler[handlerName] = evtHandler;
		CinVccBar.logMsg('addEventHandler中 ' + handlerName + ' 注册成功！');
	},
	removeEventHandler: function(handlerName, type){
		if(arguments.length < 1){
			CinVccBar.debugMsg('removeEventHandler 调用参数不足!');
			return;
		}
		var handler = null;
		handler = eval('CinVccBar.event.' + type);
		if(!handler){
			CinVccBar.debugMsg('removeEventHandler中未找到类型 ' + type);
			return;
		}
		delete handler[handlerName];
		CinVccBar.debugMsg('removeEventHandler中 ' + handlerName + ' 注销成功！');
	},
	/**********************************************************************************
	 * //Event handler:
	 **********************************************************************************
	 */
	onEventComingCall: function(CallingNo,CalledNo,OrgCalledNo,CallData,SerialID,ServiceDirect,CallID, UserParam, TaskID, AreaCode, RingRecordFile, AccessGwId){

		CinVccBar._commingCallDetail.callData = CallData;
		CinVccBar._commingCallDetail.isAlerting = true;
		CinVccBar._commingCallDetail.oralCalled = OrgCalledNo;
		CinVccBar._commingCallDetail.userParam = UserParam;
		CinVccBar._commingCallDetail.taskId = TaskID; 
		CinVccBar._commingCallDetail.callId = CallID;
		CinVccBar._commingCallDetail.areaCode = AreaCode;
		//
		CinVccBar._commingCallDetail.ringRecordFile = RingRecordFile;
		CinVccBar._commingCallDetail.accessGwId = AccessGwId;
		//
		switch(ServiceDirect){
			case 3:		//3：人工外呼
				CinVccBar._commingCallDetail.destNumber = OrgCalledNo;
				break;
			default:
				CinVccBar._commingCallDetail.destNumber = CallingNo;
				break;
		}
		try{
			if(CinVccBar.targetWin.onEventComingCallCallBack){
				CinVccBar.targetWin.onEventComingCallCallBack(CallingNo,CalledNo,OrgCalledNo,CallData,SerialID,ServiceDirect,CallID, UserParam, TaskID, AreaCode, RingRecordFile, AccessGwId);
			}
		}catch(e){};
		
		CinVccBar.processEventHandler(CinVccBar.event.ComingCall, arguments);
	},
	onEventOnCallRing: function(CallingNo,CalledNo,OrgCalledNo,CallData,SerialID,ServiceDirect,CallID, UserParam, TaskID, UserDn, AgentDn, AreaCode, RingRecordFile, AccessGwId){

		CinVccBar._commingCallDetail.callData = CallData;
		CinVccBar._commingCallDetail.isAlerting = true;
		CinVccBar._commingCallDetail.oralCalled = OrgCalledNo;
		CinVccBar._commingCallDetail.userParam = UserParam;
		CinVccBar._commingCallDetail.taskId = TaskID; 
		CinVccBar._commingCallDetail.callId = CallID;
		CinVccBar._commingCallDetail.areaCode = AreaCode;
		//
		CinVccBar._commingCallDetail.ringRecordFile = RingRecordFile;
		CinVccBar._commingCallDetail.accessGwId = AccessGwId;
		//
		switch(ServiceDirect){
			case 3:		//3：人工外呼
				CinVccBar._commingCallDetail.destNumber = OrgCalledNo;
				break;
			default:
				CinVccBar._commingCallDetail.destNumber = CallingNo;
				break;
		}
		try{
			if(CinVccBar.targetWin.onEventOnCallRingCallBack){
				CinVccBar.targetWin.onEventOnCallRingCallBack(CallingNo,CalledNo,OrgCalledNo,CallData,SerialID,ServiceDirect,CallID, UserParam, TaskID, UserDn, AgentDn, AreaCode, RingRecordFile, AccessGwId);
			}
		}catch(e){};
		
		CinVccBar.processEventHandler(CinVccBar.event.OnCallRing, arguments);
	},
	onEventRecordFileUpLoaded: function(FileName,Directory,BgnTime,EndTime,UserNo,SerialID,ServiceDirect, CallID)
	{
		try{
			if(CinVccBar.targetWin.onEventRecordFileUpLoadedCallBack){
				CinVccBar.targetWin.onEventRecordFileUpLoadedCallBack(FileName,Directory,BgnTime,EndTime,UserNo,SerialID,ServiceDirect, CallID);
			}
		}catch(e){};
		
		CinVccBar.processEventHandler(CinVccBar.event.RecordFileUpLoaded, arguments);
	},
	onEventOnCallEnd: function(CallID, SerialID, ServiceDirect, UserNo, BgnTime, EndTime, AgentAlertTime, UserAlertTime, FileName, Directory, DisconnectType, UserParam, TaskID, MsServerName, AccessGwId) {
		try {
			this._commingCallDetail.endTime = EndTime;
			this._commingCallDetail.userParam = UserParam;
			this._commingCallDetail.recordFileName = FileName;
			this._commingCallDetail.taskId = TaskID;
			//
			if(CinVccBar.targetWin.onEventOnCallEndCallBack){
				CinVccBar.targetWin.onEventOnCallEndCallBack(CallID, SerialID, ServiceDirect, UserNo, BgnTime, EndTime, AgentAlertTime, UserAlertTime, FileName, Directory, DisconnectType, UserParam, TaskID, MsServerName, AccessGwId);
			}
		}catch(e){alert(e.description)};
		
		CinVccBar.processEventHandler(CinVccBar.event.OnCallEnd, arguments);
	},
	onEventReportBtnStatus: function(btnIDS)
	{
		CinVccBar.logMsg(CinVccBar.onEventReportBtnStatus, 'onEventReportBtnStatus');
		CinVccBar.debugMsg("电话条状态报告成功：");
		CinVccBar.debugMsg("可现状态值   ："+btnIDS);
		CinVccBar.curAgentInfo.btnIDS = btnIDS;
		if(btnIDS.indexOf('2') >= 0){
			if(btnIDS.indexOf('|2|') > 0){
				CinVccBar.curAgentInfo.busy = true;
				CinVccBar.curAgentInfo.idle = false;
			}else if(btnIDS.substr(0,2) == '2|'){
				CinVccBar.curAgentInfo.busy = true;
				CinVccBar.curAgentInfo.idle = false;
			}else{
				CinVccBar.curAgentInfo.busy = false;
				CinVccBar.curAgentInfo.idle = true;
			}
		}else{
			CinVccBar.curAgentInfo.busy = false;
			CinVccBar.curAgentInfo.idle = true;
		}
		if(btnIDS.indexOf('|6|') > 0){
			this._commingCallDetail.isCalling = true;
			this._commingCallDetail.isAlerting = false;
		}else{
			this._commingCallDetail.isCalling = false;
			this._commingCallDetail.isAlerting = false;
			this._commingCallDetail.callData = '';
			this._commingCallDetail.destNumber = '';
			this._commingCallDetail.oralCalled = '';
			this._commingCallDetail.userParam = '';
			this._commingCallDetail.taskId = '';
			this._commingCallDetail.recordFileName = '';
			this._commingCallDetail.areaCode = '';
			
			this._commingCallDetail.ringRecordFile = '';
			this._commingCallDetail.accessGwId = '';
		}
		try{
			if(CinVccBar.targetWin.onEventReportBtnStatusCallBack){
				CinVccBar.targetWin.onEventReportBtnStatusCallBack(btnIDS);
			}
		}catch(e){};
		
		CinVccBar.processEventHandler(CinVccBar.event.ReportBtnStatus, arguments);
	},
	onEventOnTelephoneStatusChanged: function(vccID,deviceID,status)
	{
		CinVccBar.logMsg(CinVccBar.onEventOnTelephoneStatusChanged, 'onEventOnTelephoneStatusChanged');
		CinVccBar.debugMsg("监控电话报告:VccID:"+vccID+" 电话号码:"+deviceID+" 电话状态:"+status);
		try{
			if(CinVccBar.targetWin.onEventOnTelephoneStatusChangedCallBack){
				CinVccBar.targetWin.onEventOnTelephoneStatusChangedCallBack(vccID,deviceID,status);
			}
		}catch(e){};
		
		CinVccBar.processEventHandler(CinVccBar.event.OnTelephoneStatusChanged, arguments);
	},
	onEventOnAgentStatusChanged: function(vccID,agentID,deviceID,status)
	{
		CinVccBar.logMsg(CinVccBar.onEventOnAgentStatusChanged, 'onEventOnAgentStatusChanged');
		CinVccBar.debugMsg("监控座席报告:VccID:"+vccID+" 工号："+agentID+" 电话号码:"+deviceID+" 座席状态:"+status);
		try{
			if(CinVccBar.targetWin.onEventOnAgentStatusChangedCallBack){
				CinVccBar.targetWin.onEventOnAgentStatusChangedCallBack(vccID,agentID,deviceID,status);
			}
		}catch(e){};
		
		CinVccBar.processEventHandler(CinVccBar.event.OnAgentStatusChanged, arguments);
	},
	onEventAnswerCall: function(UserNo,AnswerTime,SerialID,ServiceDirect, CallID, UserParam, TaskID)
	{
		//set for comming call detail;
		this._commingCallDetail.userNo = UserNo;
		this._commingCallDetail.answerTime = AnswerTime;
		this._commingCallDetail.serviceDirect = ServiceDirect;
		this._commingCallDetail.callID = CallID;
		this._commingCallDetail.userParam = UserParam;
		this._commingCallDetail.localAnswerTime = new Date();
		CinVccBar._commingCallDetail.isCalling = true;
		CinVccBar._commingCallDetail.isAlerting = false;
		this._commingCallDetail.callData = UserParam;
		this._commingCallDetail.userParam = UserParam;
		this._commingCallDetail.taskId = TaskID;
		this._commingCallDetail.callId = CallID;
		try{
			if(CinVccBar.targetWin.onEventAnswerCallCallBack){
				CinVccBar.targetWin.onEventAnswerCallCallBack(UserNo,AnswerTime,SerialID,ServiceDirect, CallID, UserParam, TaskID);
			}
		}catch(e){};
		
		CinVccBar.processEventHandler(CinVccBar.event.AnswerCall, arguments);
	},	
	onEventOnPrompt: function(code,description)
	{
		try{
			if(CinVccBar.targetWin.onEventOnPromptCallBack){
				CinVccBar.targetWin.onEventOnPromptCallBack(code,description);
			}else if(CinVccBar.event.OnPromptCall){
				CinVccBar.event.OnPromptCall(code,description);
			}
		}catch(e){};
		
		CinVccBar.processEventHandler(CinVccBar.event.OnPromptCall, arguments);
	},
	onEventOnQueueStatusChanged: function(QueueInfo)
	{
		CinVccBar.logMsg(CinVccBar.onEventOnQueueStatusChanged, 'onEventOnQueueStatusChanged');
		CinVccBar.debugMsg("服务器上的排队信息变化:"+QueueInfo);
		try{
			if(CinVccBar.targetWin.onEventOnQueueStatusChangedCallBack){
				CinVccBar.targetWin.onEventOnQueueStatusChangedCallBack(QueueInfo);
			}
		}catch(e){};
		
		CinVccBar.processEventHandler(CinVccBar.event.OnQueueStatus, arguments);
	},
	onEventOnlockStatusReport: function(vccID, agentID, deviceID, flag)
	{
		CinVccBar.logMsg(CinVccBar.onEventOnlockStatusReport, 'onEventOnlockStatusReport');
		CinVccBar.debugMsg("座席被锁定 : 集团编号 "+vccID+ "  座席工号:"+agentID + "  电话号码:"+deviceID + "  锁定标识 0：解锁 1：锁定:"+flag);
		try{
			if(CinVccBar.targetWin.onEventOnlockStatusReportChangedCallBack){
				CinVccBar.targetWin.onEventOnlockStatusReportChangedCallBack(vccID, agentID, deviceID, flag);
			}
		}catch(e){};
		
		CinVccBar.processEventHandler(CinVccBar.event.OnlockStatusReportChanged, arguments);
	},
	onEventOnInitalSuccess: function()
	{
		CinVccBar.logMsg(CinVccBar.onEventOnInitalSuccess, 'onEventOnInitalSuccess');
		CinVccBar.debugMsg("座席初始化完毕");
		try{
			if(CinVccBar.targetWin.onEventOnInitalSuccessCallBack){
				CinVccBar.targetWin.onEventOnInitalSuccessCallBack();
			}
		}catch(e){};
		
		CinVccBar.processEventHandler(CinVccBar.event.OnInitalSuccess, arguments);
	},
	onEventOnShortCutKey: function(strkey,value)
	{
		CinVccBar.logMsg(CinVccBar.onEventOnShortCutKey, 'onEventOnShortCutKey');
		CinVccBar.debugMsg("快捷键值:"+strkey+ "  可选参数:"+value);
		try{
			if(CinVccBar.targetWin.onEventOnShortCutKeyCallBack){
				CinVccBar.targetWin.onEventOnShortCutKeyCallBack(strkey,value);
			}
		}catch(e){};
		
		CinVccBar.processEventHandler(CinVccBar.event.OnShortCutKey, arguments);
	},
	onEventOnIVRStatusChanged: function(IVRInfo)
	{
		CinVccBar.logMsg(CinVccBar.onEventOnIVRStatusChanged, 'onEventOnIVRStatusChanged');
		CinVccBar.debugMsg("IVR资源发生变化:"+IVRInfo);
		try{
			if(CinVccBar.targetWin.onEventOnIVRStatusChangedCallBack){
				CinVccBar.targetWin.onEventOnIVRStatusChangedCallBack(IVRInfo);
			}
		}catch(e){};
		
		CinVccBar.processEventHandler(CinVccBar.event.OnIVRStatusChanged, arguments);
	},
	onEventOnEventPrompt: function(eventIndex, eventParam)
	{
		CinVccBar._commingCallDetail.srfEvent = eventIndex;
		CinVccBar._commingCallDetail.srfValue = eventParam;
		CinVccBar.logMsg(CinVccBar.onEventOnEventPrompt, 'onEventOnEventPrompt');
		CinVccBar.debugMsg("OnEventPrompt:"+eventIndex, + ', ' + eventParam);
		try{
			if(CinVccBar.targetWin.onEventOnEventPromptCallBack){
				CinVccBar.targetWin.onEventOnEventPromptCallBack(eventIndex, eventParam);
			}
		}catch(e){};
		
		CinVccBar.processEventHandler(CinVccBar.event.OnEventPrompt, arguments);
	},
	onEventOnAgentWorkReport: function(workStatus, description)
	{
		CinVccBar.logMsg(CinVccBar.onEventOnAgentWorkReport, 'onEventOnAgentWorkReport');
		CinVccBar.debugMsg("座席工作报告:workStatus:" + workStatus + " description:" +description);
		try{
			if(CinVccBar.targetWin.onEventOnAgentWorkReportCallBack){
				CinVccBar.targetWin.onEventOnAgentWorkReportCallBack(workStatus, description);
			}
		}catch(e){};
		
		CinVccBar.processEventHandler(CinVccBar.event.OnAgentWorkReport, arguments);
	},
	onEventOnInitalFailure: function(code, description)
	{
		CinVccBar.logMsg(CinVccBar.onEventOnInitalFailure, 'onEventOnInitalFailure');
		CinVccBar.debugMsg("注册失败:code:" + code + " description:" +description);
		try{
			if(CinVccBar.targetWin.onEventOnInitalFailureCallBack){
				CinVccBar.targetWin.onEventOnInitalFailureCallBack(code, description);
			}
		}catch(e){};
		
		CinVccBar.processEventHandler(CinVccBar.event.OnInitalFailure, arguments);
	},
	onEventOnAgentReport: function(AgentReportInfo)
	{
		CinVccBar.logMsg(CinVccBar.onEventOnAgentReport, 'onEventOnAgentReport');
		CinVccBar.debugMsg(AgentReportInfo);
		try{
			if(CinVccBar.targetWin.onEventOnAgentReportCallBack){
				CinVccBar.targetWin.onEventOnAgentReportCallBack(AgentReportInfo);
			}
		}catch(e){};
		
		CinVccBar.processEventHandler(CinVccBar.event.OnAgentReport, arguments);
	},
	onEventOnIvrReport: function(IvrReportInfo)
	{
		CinVccBar.logMsg(CinVccBar.onEventOnIvrReport, 'onEventOnIvrReport');
		CinVccBar.debugMsg(IvrReportInfo);
		try{
			if(CinVccBar.targetWin.onEventOnIvrReportCallBack){
				CinVccBar.targetWin.onEventOnIvrReportCallBack(IvrReportInfo);
			}
		}catch(e){};
		
		CinVccBar.processEventHandler(CinVccBar.event.OnIvrReport, arguments);
	},
	onEventOnTelReport: function(TelReportInfo)
	{
		CinVccBar.logMsg(CinVccBar.onEventOnTelReport, 'onEventOnTelReport');
		CinVccBar.debugMsg(TelReportInfo);
		try{
			if(CinVccBar.targetWin.onEventOnTelReportCallBack){
				CinVccBar.targetWin.onEventOnTelReportCallBack(TelReportInfo);
			}
		}catch(e){};
		
		CinVccBar.processEventHandler(CinVccBar.event.OnTelReport, arguments);
	},
	onEventOnServiceReport: function(ServiceReportInfo)
	{
		CinVccBar.logMsg(CinVccBar.onEventOnServiceReport, 'onEventOnServiceReport');
		CinVccBar.debugMsg(ServiceReportInfo);
		try{
			if(CinVccBar.targetWin.onEventOnServiceReportCallBack){
				CinVccBar.targetWin.onEventOnServiceReportCallBack(ServiceReportInfo);
			}
		}catch(e){};
		
		CinVccBar.processEventHandler(CinVccBar.event.OnServiceReport, arguments);
	},
	onEventOnTaskReport: function(TaskReportInfo)
	{
		CinVccBar.logMsg(CinVccBar.onEventOnTaskReport, 'onEventOnTaskReport');
		CinVccBar.debugMsg(TaskReportInfo);
		try{
			if(CinVccBar.targetWin.onEventOnTaskReportCallBack){
				CinVccBar.targetWin.onEventOnTaskReportCallBack(TaskReportInfo);
			}
		}catch(e){};
		
		CinVccBar.processEventHandler(CinVccBar.event.OnTaskReport, arguments);
	},
	onEventOnOutboundReport: function(OutboundReportInfo)
	{
		CinVccBar.logMsg(CinVccBar.onEventOnOutboundReport, 'onEventOnOutboundReport');
		CinVccBar.debugMsg(OutboundReportInfo);
		try{
			if(CinVccBar.targetWin.onEventOnOutboundReportCallBack){
				CinVccBar.targetWin.onEventOnOutboundReportCallBack(OutboundReportInfo);
			}
		}catch(e){};
		
		CinVccBar.processEventHandler(CinVccBar.event.OnOutboundReport, arguments);
	},
	onEventOnCallReportInfo: function(CallReportInfo)
	{
		CinVccBar.logMsg(CinVccBar.onEventOnCallReportInfo, 'onEventOnCallReportInfo');
		CinVccBar.debugMsg(CallReportInfo);
		try{
			if(CinVccBar.targetWin.onEventOnCallReportInfoCallBack){
				CinVccBar.targetWin.onEventOnCallReportInfoCallBack(CallReportInfo);
			}
		}catch(e){};
		
		CinVccBar.processEventHandler(CinVccBar.event.OnCallReportInfo, arguments);
	},
	onEventOnCallQueueQuery: function(queueInfo)
	{
		CinVccBar.logMsg(CinVccBar.onEventOnCallQueueQuery, 'onEventOnCallQueueQuery');
		CinVccBar.debugMsg(queueInfo);
		try{
			if(CinVccBar.targetWin.onEventOnCallQueueQueryCallBack){
				CinVccBar.targetWin.onEventOnCallQueueQueryCallBack(queueInfo);
			}
		}catch(e){};
		
		CinVccBar.processEventHandler(CinVccBar.event.OnCallQueueQuery, arguments);
	},
	onEventOnQueueReport: function(queueInfo)
	{
		CinVccBar.logMsg(CinVccBar.onEventOnQueueReport, 'onEventOnQueueReport');
		CinVccBar.debugMsg(queueInfo);
		try{
			if(CinVccBar.targetWin.onEventOnQueueReportCallBack){
				CinVccBar.targetWin.onEventOnQueueReportCallBack(queueInfo);
			}
		}catch(e){};
		
		CinVccBar.processEventHandler(CinVccBar.event.OnQueueReport, arguments);
	},
	onEventOnCallDataChanged: function(calldata)
	{
		CinVccBar.logMsg(CinVccBar.onEventOnCallDataChanged, 'onEventOnCallDataChanged');
		CinVccBar.debugMsg(calldata);
		try{
			if(CinVccBar.targetWin.onEventOnCallDataChangedCallBack){
				CinVccBar.targetWin.onEventOnCallDataChangedCallBack(calldata);
			}
		}catch(e){};
		
		CinVccBar.processEventHandler(CinVccBar.event.OnCallDataChanged, arguments);
	},
	
	onEventOnBarExit: function(code, description)
	{
		CinVccBar.logMsg(CinVccBar.onEventOnBarExit, 'onEventOnBarExit');
		CinVccBar.debugMsg(code);
		try{
			if(CinVccBar.targetWin.onEventOnBarExitCallBack){
				CinVccBar.targetWin.onEventOnBarExitCallBack(code, description);
			}
		}catch(e){};
		
		CinVccBar.processEventHandler(CinVccBar.event.OnBarExit, arguments);
	},
	onEventOnQueryGroupAgentStatus: function(queueInfo)
	{
		CinVccBar.logMsg(CinVccBar.onEventOnQueryGroupAgentStatus, 'onEventOnQueryGroupAgentStatus');
		CinVccBar.debugMsg(queueInfo);
		try{
			if(CinVccBar.targetWin.onEventOnQueryGroupAgentStatusCallBack){
				CinVccBar.targetWin.onEventOnQueryGroupAgentStatusCallBack(queueInfo);
			}
		}catch(e){};
		
		CinVccBar.processEventHandler(CinVccBar.event.OnQueryGroupAgentStatus, arguments);
	},
	onEventOnQueryMonitorSumReport: function(cmdName, xmlRptStr)
	{
		//
		CinVccBar.logMsg(CinVccBar.onEventOnQueryMonitorSumReport, 'onEventOnQueryMonitorSumReport');
		CinVccBar.debugMsg(cmdName + xmlRptStr);
		try{
			if(CinVccBar.targetWin.onEventOnQueryMonitorSumReportCallBack){
				CinVccBar.targetWin.onEventOnQueryMonitorSumReportCallBack(cmdName, xmlRptStr);
			}
		}catch(e){};
		
		CinVccBar.processEventHandler(CinVccBar.event.OnQueryMonitorSumReport, arguments);
	}
};
}
