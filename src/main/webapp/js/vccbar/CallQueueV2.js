/*
新版本的排队信息
*/
function QueueRptManager() {
}

QueueRptManager.initial = function(vccBarOcxCtr, queLenRptDivId, canSeeListWin, autoOpenListWin) {
	this.cinVccBar = vccBarOcxCtr;
	this.queLenRptDiv = $(queLenRptDivId);
	this.canSeeListWin = canSeeListWin;
	this.autoOpenListWin = autoOpenListWin;
	// 排队详情页
	this.listWinX = -1;
	this.listWinY = -1;
	this.queueListWin = null;
	this.callQueueQueryTimer = null;
	//
	this.cinVccBar.addEventHandler(this.onQueueReport, 'onQueueReport', 'OnQueueReport');
	this.cinVccBar.addEventHandler(this.onCallQueueQuery, 'onCallQueueQuery', 'OnCallQueueQuery');
	//
	this.queueRptCtxArr = new Array();
	this.totalQueLen = 0;
}

QueueRptManager.unload = function() {
	if(this.queueListWin) {
		this.queueListWin.close();
		this.queueListWin = null;
	}
}

QueueRptManager.onQueueReport = function(queueXmlStr) {
	try {
		var jsonBuilder = new JsonBuilder();
		
		var xmlDoc = jsonBuilder.xmlStr2Doc(queueXmlStr);
		var xmlCtxObj = jsonBuilder.xmlDoc2Obj(xmlDoc);
		//alert(jsonBuilder.obj2Json(xmlCtxObj));
		var serviceCtxObj = xmlCtxObj.serviceInfo._attributeObj;
		var queueNotifyCtxObj = xmlCtxObj.serviceInfo.service._attributeObj;
		//
		var actionMode = queueNotifyCtxObj.mode;
		switch(actionMode) {
			case 'addQueue': {
				QueueRptManager.addQueue(serviceCtxObj, queueNotifyCtxObj);
				break;
			}
			case 'removeQueue': {
				QueueRptManager.removeQueue(serviceCtxObj, queueNotifyCtxObj);
				break;
			}
			case 'queueLen': {
				QueueRptManager.notifyQueueLen(serviceCtxObj, queueNotifyCtxObj);
				break;
			}
		}
	}
	catch(ex) {
		alert(ex.description);
	}
}
QueueRptManager.queueCtxFilter = function(serviceCtx, queueNotifyCtx) {
	var serviceId = serviceCtx.serviceid;
	var queueRptCtx = {serviceId:serviceId, serviceName:serviceCtx.servicename, queueLen:queueNotifyCtx.queuelen};
	this.queueRptCtxArr[serviceId] = queueRptCtx;
	return queueRptCtx;
}

QueueRptManager.addQueue = function(serviceCtx, queueNotifyCtx) {
	this.queueCtxFilter(serviceCtx, queueNotifyCtx);
	this.writeQueueLenBoard();
}

QueueRptManager.removeQueue = function(serviceCtx, queueNotifyCtx) {
	this.queueCtxFilter(serviceCtx, queueNotifyCtx);
	this.writeQueueLenBoard();
}

QueueRptManager.notifyQueueLen = function(serviceCtx, queueNotifyCtx) {
	this.queueCtxFilter(serviceCtx, queueNotifyCtx);
	this.writeQueueLenBoard();
}

QueueRptManager.writeQueueLenBoard = function() {
	var queueRptObj;
	
	var queueLenTxt = "";
	this.totalQueLen = 0;
	for(var serviceId in this.queueRptCtxArr) {
		queueRptObj = this.queueRptCtxArr[serviceId];
		
		if(queueRptObj && queueRptObj.queueLen) {
			var queLen = new Number(queueRptObj.queueLen);
			this.totalQueLen += queLen;
			if(queLen > 0) {
				queueLenTxt += "{0}:<font color='red'>{1}</font>&nbsp;".format(queueRptObj.serviceName, queueRptObj.queueLen);
			}
		}
	}
	if(queueLenTxt) {
		queueLenTxt = '<span class=\"gd_Font\">&nbsp;排队：</span>' + queueLenTxt;
		// 排队详情可见
		if(this.canSeeListWin) {
			queueLenTxt += "<a onclick='QueueRptManager.openQueueListWin();' style='cursor:hand;'>&nbsp;[详细]</a>";
		}
	}
	//
	this.queLenRptDiv.innerHTML = queueLenTxt;
	// 当排队数大于0,并设置为自动弹屏显示列表信息
	if(this.totalQueLen > 0 && this.autoOpenListWin) {
		this.openQueueListWin();
	}
}

QueueRptManager.doQueueQuery = function() {
	var queueRptObj;
	for(var serviceId in this.queueRptCtxArr) {
		queueRptObj = this.queueRptCtxArr[serviceId];
		this.cinVccBar.CallQueueQuery(serviceId);
	}
}

QueueRptManager.changeCallQueue = function(calling, sid, orderId) {
	this.cinVccBar.ChangeCallQueue(calling, sid, orderId);
}

QueueRptManager.startQueueQueryTimer = function() {
	this.callQueueQueryTimer = setInterval("QueueRptManager.doQueueQuery()", 5*1000); // 5秒
}

QueueRptManager.stopQueueQueryTimer = function() {
	clearInterval(this.callQueueQueryTimer);
	this.callQueueQueryTimer = null;
}

QueueRptManager.onQueueListWinClose = function(winX, winY) {
	this.queueListWin = null;
	this.listWinX = winX;
	this.listWinY = winY;
	//
	this.stopQueueQueryTimer();
}

QueueRptManager.onQueueListWinOpen = function() {
	this.startQueueQueryTimer();
}

QueueRptManager.onCallQueueQuery = function(queueXmlStr) {
	if(QueueRptManager.queueListWin) {
		QueueRptManager.queueListWin.writeQueueList(queueXmlStr);
	}
}

QueueRptManager.openQueueListWin = function(needFocus) {
	if(this.queueListWin != null){
		if(needFocus) {
			this.queueListWin.focus();
		}
		return;
	}
	//
	if(this.listWinX == -1){
		x = parseInt(window.screen.availWidth) - 220;
		y = parseInt(window.screen.availHeight) - 250;
	}
	else{
		x = this.listWinX;
		y = this.listWinY;
	}
	//
	var winPageUrl = '/cc-crm/forward.do?method=forward&forwardName=queueListWin';
	var winStyle = 'dialogLeft:{0}px;dialogTop:{1}px;dialogWidth:220px;dialogHeight:250px;center:no;resizable:yes;status:no;scroll:auto;help:no;edge:raised;'.format(x, y);
	this.queueListWin = showModelessDialog(winPageUrl, window, winStyle);
}

