/**
Version : 1.0.6
Release : 2013/08/01
**/
function CcMeetManager(vccBarCtr, argObj) {
	this._RTN_OK = 0;
	//
	this.varKey = "_globalCcMeetManager";
	this.vccBarCtr = vccBarCtr;
	//
	this.corpVccId = '';
	this.callAgtId = '';
	//
	this.firstAgtId = '';
	this.firstAgtTel = '';
	this.firstAgtShort = '';
	//
	this.nextAgtId = '';
	this.nextAgtTel = '';
	this.nextAgtShort = '';
	//
	this.meetStatus = '';
	this.firstAgtAnswer = false;
	this.nextAgtAnswer = false;
	this.callId = '';
	//
	this.vccBarCtr.attachEvent("OnCallRing", onCallRing4Meet, false);
	this.vccBarCtr.attachEvent("OnInitalSuccess", onInitalSuccess4Meet, false);
	this.vccBarCtr.attachEvent("OnPrompt", onPrompt4Meet, false);
	this.vccBarCtr.attachEvent("AnswerCall", onAnswerCall4Meet, false);
	this.vccBarCtr.attachEvent("OnCallEnd", onCallEnd4Meet, false);
	this.vccBarCtr.attachEvent("OnEventPrompt", onEventPrompt4Meet, false);
	//
	if(argObj) {
		var tipsSpanId = argObj.tipsSpanId;
		tipsSpanId = tipsSpanId ? tipsSpanId: "CcMeetTipsSpan";
		this.tipsSpan = document.getElementById(tipsSpanId);
		//
		this.callRingNotifyFunc = argObj.ringNotifyFunc;
	}	
}

CcMeetManager.prototype.clearMeetStatus = function() {
	this.meetStatus = '';
	this.firstAgtAnswer = false;
	this.nextAgtAnswer = false;
	//
	this.firstAgtId = '';
	this.firstAgtTel = '';
	this.firstAgtShort = '';
	//
	this.nextAgtId = '';
	this.nextAgtTel = '';
	this.nextAgtShort = '';
	//
	this.doTips("");
}

CcMeetManager.prototype.endMeet = function() {
	try {
		var _IS_CALLING = 3;
		//
		var agtStatus = this.vccBarCtr.GetAgentStatus();
		if(agtStatus == _IS_CALLING) {
			var forceEnd = 1;
			var callEndRtn = this.vccBarCtr.Disconnect(forceEnd);
		}
	}
	catch(ex) {
	}
	//
}

CcMeetManager.prototype.startMeet = function(firstAgtId, nextAgtId, callData) {
	this.clearMeetStatus();
	//
	var agtIdPrefix = "000010";
	var agtTelPrefix = "000002";
	var agtPrefix = "0000";
	//
	if(firstAgtId.indexOf(agtPrefix) != 0) {
		firstAgtId = agtIdPrefix + this.corpVccId + firstAgtId;
	}
	if(nextAgtId.indexOf(agtPrefix) != 0) {
		nextAgtId = agtIdPrefix + this.corpVccId + nextAgtId;
	}
	//
	var callRtn = this.vccBarCtr.CallIn(firstAgtId, 5, '', callData);
	if(this.isOkRtn(callRtn)) {
		this.firstAgtId = firstAgtId;
		this.nextAgtId = nextAgtId;
		//
		this.firstAgtTel = agtTelPrefix + this.firstAgtId.substr(6);
		this.nextAgtTel = agtTelPrefix + this.nextAgtId.substr(6);
		//
		this.firstAgtShort = this.firstAgtId.substr(12);
		this.nextAgtShort = this.nextAgtId.substr(12);
		//
		this.doTips("开始呼叫:" + this.firstAgtShort + "!");
	}
	else {
		this.doTips('呼叫:' + firstAgtId + '失败!', 5);
	}
}

CcMeetManager.prototype.isOkRtn = function(callRtn) {
	return callRtn == this._RTN_OK;
}

CcMeetManager.prototype.isCallingFirst = function() {
	return (this.meetStatus == 'callingFirst');
}

CcMeetManager.prototype.doTips = function(tipsMsg, timeOut) {
	if(this.tipsSpan) {
		this.tipsSpan.innerHTML = tipsMsg ;
	}
	else {
		window.status = tipsMsg;
	}
	//
	if(timeOut) {
		setTimeout(this.varKey + ".doTips('');", timeOut*1000);
	}
}

CcMeetManager.prototype.onInitalSuccess = function() {
	this.corpVccId = this.vccBarCtr.MediaFlag;
	this.callAgtId = this.vccBarCtr.AgentID;
}

CcMeetManager.prototype.isFirstAgtCall = function(callTelNum) {
	return (this.firstAgtTel == callTelNum || this.firstAgtId == callTelNum)
}
CcMeetManager.prototype.isNextAgtCall = function(callTelNum) {
	return (this.nextAgtTel == callTelNum || this.nextAgtId == callTelNum)
}


CcMeetManager.prototype.onCallRing = function(callTelNum, callId, callSid, callData, callType) {
	var _INNER_CALL = 5;
	var _CONSULT_CALL = 6;
	//
	if((callType == _INNER_CALL || callType == _CONSULT_CALL) && !this.isFirstAgtCall(callTelNum) && !this.isNextAgtCall(callTelNum)) {
		this.meetStatus = '';
		if(this.callRingNotifyFunc) {
			try {
				this.callRingNotifyFunc(callTelNum, callId, callSid, callData);
			}
			catch(ex) {
				this.doTips("callRingNotifyFunc error:" + ex.description);
			}
		}
	}
}

CcMeetManager.prototype.onAnswerCall = function(callTelNum, callId, callSid, callData) {
}

CcMeetManager.prototype.onEventPrompt = function(eventKey, eventPara) {
	//
	var _FIRST_AGT_RING = 26;
	var _FIRST_AGT_FAIL = 27;
	var _FIRST_AGT_ANSWER = 68;
	
	var _NEXT_AGT_RING = 201;
	var _NEXT_AGT_ANSWER = 202;
	var _NEXT_AGT_FAIL = 203;
	//
	switch(eventKey) {
		case _FIRST_AGT_RING: {
			this.meetStatus = 'callingFirst';
			//
			this.doTips(this.firstAgtShort + " 正在振铃!");
			break;
		}
		case _FIRST_AGT_FAIL: {
			this.doTips("呼叫:" + this.firstAgtShort + "失败!", 5);
			break;
		}
		case _FIRST_AGT_ANSWER: {
			var callRtn = this.vccBarCtr.Consult(0, this.nextAgtId);
			if(this.isOkRtn(callRtn)) {
				this.meetStatus = 'callingNext';
				this.doTips("开始呼叫:" + this.nextAgtShort);
			}
			break;
		}
		case _NEXT_AGT_RING: {
			this.doTips(this.nextAgtShort + " 正在振铃!");
		}
		case _NEXT_AGT_FAIL: {
			this.doTips("呼叫:" + this.nextAgtShort + "失败!", 5);
			break;
		}
		case _NEXT_AGT_ANSWER: {
			var callRtn = this.vccBarCtr.Conference();
			//
			if(this.isOkRtn(callRtn)) {
				this.doTips("三方呼叫完成!", 3);
			}
			else {
				this.doTips("三方会议失败!", 3);
			}
		}
	}
}

CcMeetManager.prototype.onCallEnd = function() {
	this.clearMeetStatus();
	
	this.doTips("呼叫结束!", 3);
}

CcMeetManager.prototype.onPrompt = function(msgCode, msgTxt) {
	var tipsMsg = msgCode + ":" + msgTxt;
	this.doTips(tipsMsg);
}

//
function onAnswerCall4Meet(callTelNum, callTime, callSid, callType, callId, callData) {
	_globalCcMeetManager.onAnswerCall(callTelNum, callId, callSid, callData);
}

function onCallEnd4Meet() {
	_globalCcMeetManager.onCallEnd();
}
function onInitalSuccess4Meet() {
	_globalCcMeetManager.onInitalSuccess();
}

function onEventPrompt4Meet(eventKey, eventPara) {
	_globalCcMeetManager.onEventPrompt(eventKey, eventPara);
}

function onPrompt4Meet(msgCode, msgTxt) {
	_globalCcMeetManager.onPrompt(msgCode, msgTxt);
}

function onCallRing4Meet(CallingNo,CalledNo,OrgCalledNo,CallData,SerialID,ServiceDirect,CallID,UserParam,TaskID,UserDn,AgentDn, AreaCode) {
	_globalCcMeetManager.onCallRing(UserDn, CallID, SerialID, UserParam, ServiceDirect);
}
//
function startCcMeet(firstAgtId, nextAgtId, callData) {
	_globalCcMeetManager.startMeet(firstAgtId, nextAgtId, callData);
}
function endCcMeet() {
	_globalCcMeetManager.endMeet();
}
//
var _globalCcMeetManager = null;
CcMeetManager.init = function(vccBarOcx, argObj) {
	_globalCcMeetManager = new CcMeetManager(vccBarOcx, argObj);
}