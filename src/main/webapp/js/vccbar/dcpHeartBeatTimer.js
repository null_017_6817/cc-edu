_globalHeatBeatHttpRequest = null;

function _doHeartBeat() {
	_globalHeatBeatHttpRequest.doPost();
}

function DcpHeartBeatTimer(heartBeatUrl, heartBeatInterval) {
	var enableAddTimeStampInUrl = true;
	this.httpRequest = new MyHttpRequest(heartBeatUrl, enableAddTimeStampInUrl);
 	
 	this.httpRequest.setRequestOkFunc(this.heartBeatOk);
 	this.httpRequest.setRequestBadFunc(this.reportError);
 	this.httpRequest.setItsOwner(this);
 	
	this.heartBeatInterval = heartBeatInterval;
	if(!this.heartBeatInterval) {
		this.heartBeatInterval = 5;
	}
	this.heartBeatTimer = null;
}

DcpHeartBeatTimer.prototype.heartBeatOk = function(itsRequest, itsSelf) {
}

DcpHeartBeatTimer.prototype.reportError = function(itsRequest, itsSelf) {
}

DcpHeartBeatTimer.prototype.start = function() {
	_globalHeatBeatHttpRequest = this.httpRequest;
	//
	this.heartBeatTimer = window.setInterval("_doHeartBeat();", this.heartBeatInterval*60*1000);
}

DcpHeartBeatTimer.prototype.stop = function() {
	window.clearInterval(this.heartBeatTimer);
}