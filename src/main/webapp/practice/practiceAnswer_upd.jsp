<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.qtrmoon.com/tags-dictionary" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>编辑</title><!--practiceAnswer-->
	<%@include file="/common/include.jsp" %>
<script>
var submitable=true;//防止重复提交
<%-- 查询页点击修改时调用此方法加载记录详情 --%>
function load(id){
	$('#_form').form('load','/${projectName}/practice/practiceAnswer/viePracticeAnswer.action?id='+id);
};

<%-- 点击保存按钮时调用此方法。(注:保存按钮在查询列表页) --%>
function submitForm(){
	if(!submitable)return;
	if(!$("#_form").form("validate")){
		return;
	}
	$('#_form').form('submit',{
		success:function(data) {
			parent.reloadData();
		}
	});
	submitable=false;
}
</script>
</head>
<body>
<form id="_form" method="post" action="/${projectName}/practice/practiceAnswer/updPracticeAnswer.action">
	<input type="hidden" name='id' id='id'/>
	<table style='width:100%'>
		<tr><td class='label' style="width:120px">：</td><td><input id="eid" name="eid" class="easyui-textbox" data-options="required:true,validType:['int']"/></td></tr>
		<tr><td class='label' >：</td><td><input id="etid" name="etid" class="easyui-textbox" data-options="required:true,validType:['int']"/></td></tr>
		<tr><td class='label' >：</td><td><input id="stunum" name="stunum" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >：</td><td><input id="title" name="title" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >：</td><td><input id="stepinfo" name="stepinfo" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >：</td><td><input id="stuanswer" name="stuanswer" class="easyui-textbox" data-options="required:true"/></td></tr>
	</table>

</form>
</body>
</html>
