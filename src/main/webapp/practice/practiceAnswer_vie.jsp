<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.qtrmoon.com/tags-dictionary" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>查看</title><!--practiceAnswer-->
	<%@include file="/common/include.jsp" %>
<script>
function load(id){
	$('#_form').form('load','/${projectName}/practice/practiceAnswer/viePracticeAnswer.action?toL=true&id='+id);
};
</script>
</head>
<body>
<form id="_form">
	<input type="hidden" name='id' value=""/>
	<table>
		<tr><td class='label'>：</td><td><input type='text' name='eid' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>：</td><td><input type='text' name='etid' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>：</td><td><input type='text' name='stunum' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>：</td><td><input type='text' name='title' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>：</td><td><input type='text' name='stepinfo' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>：</td><td><input type='text' name='stuanswer' class='viewbox' readonly='readonly'/></td></tr>
	</table>

</form>
</body>
</html>
