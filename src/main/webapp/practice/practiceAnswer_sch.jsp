<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.qtrmoon.com/tags-dictionary" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>查询</title><!--practiceAnswer-->
	<%@include file="/common/include.jsp" %>

<script>
var winToggle;
$(function(){
	winToggle=new QWinToggle().regist(["practiceAnswerEditWin","practiceAnswerViewWin"]);
	var pager = $('#practiceAnswerGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});			
})

<%-- 执行查询，点击查询按钮时调用 --%>
function doPracticeAnswerSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#practiceAnswerGrid').datagrid('load',{
		stunum:$('#stunum').val(),
		title:$('#title').val(),
		stepinfo:$('#stepinfo').val(),
		stuanswer:$('#stuanswer').val()
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatPracticeAnswerOper(val,row,index){  
    return "&emsp;<a href='javascript:void(0)' onclick='updPracticeAnswer("+val+")' class='_oper' title='修改'>&nbsp;<i class='icon-edit'></i>&nbsp;</a>"
    			+"<a href='javascript:void(0)' onclick='viePracticeAnswer("+val+")' class='_oper' title='查看'>&nbsp;<i class='icon-info-sign'></i>&nbsp;</a>&emsp;"; 
} 

<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
function updPracticeAnswer(id){
	winToggle.open("practiceAnswerEditWin");
	$('#practiceAnswerEditWin').get(0).contentWindow.load(id);
} 

<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
function viePracticeAnswer(id){
	winToggle.open("practiceAnswerViewWin");
	$('#practiceAnswerViewWin').get(0).contentWindow.load(id);
}

<%-- 删除记录 --%>
function delPracticeAnswer(){
	var ids=_collectIds('#practiceAnswerGrid');
	if(ids.length==0){$.messager.alert('敬告','请选择删除的记录');return;}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/practice/practiceAnswer/delPracticeAnswer.action",{ids:ids},function(data){
				$('#practiceAnswerGrid').datagrid('reload');
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#practiceAnswerEditWin').dialog('close');
	$('#practiceAnswerGrid').datagrid('reload');
}
</script>
</head>
<body>
<table id="practiceAnswerGrid" title="" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#practiceAnswerGridToolbar',collapsible:true,url:'/${projectName}/practice/practiceAnswer/schPracticeAnswer.action',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" ></th>
			<th data-options="field:'eid',width:80,sortable:true"></th>
			<th data-options="field:'etid',sortable:true"></th>
			<th data-options="field:'stunum',sortable:true"></th>
			<th data-options="field:'title',sortable:true"></th>
			<th data-options="field:'stepinfo',sortable:true"></th>
			<th data-options="field:'stuanswer',sortable:true"></th>

			<th data-options="field:'_oper',align:'center',formatter:formatPracticeAnswerOper">操作</th>
		</tr>
	</thead>
</table>
<%-- 查询条件设定栏 --%>
<div id="practiceAnswerGridToolbar">
	<form id="_form" class="shline">
	：<input name='stunum' id="stunum" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	：<input name='title' id="title" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	：<input name='stepinfo' id="stepinfo" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	：<input name='stuanswer' id="stuanswer" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>

	<a href="javascript:void(0)" onclick="doPracticeAnswerSearch()" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>
	</form>
	<div class="opline">
		<a href="javascript:updPracticeAnswer(0)" class="btn btn-success"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">添加</span></a>
		<a href="javascript:delPracticeAnswer()" class="btn btn-danger"><span class="icon-trash" style="font-size:16px"></span> <span style="font-size:14px">删除</span></a>
	</div>
</div>


<%-- 修改or添加面板 --%>
<iframe id="practiceAnswerEditWin" src="/${projectName}/practice/practiceAnswer/page.action?page=practiceAnswer_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#practiceAnswerEditButton'" title="编辑" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="practiceAnswerEditButton">
	<a href="javascript:void(0)" onclick="$('#practiceAnswerEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#practiceAnswerEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >取 消</a>
</div>
<%-- 查看面板 --%>
<iframe id="practiceAnswerViewWin" src="/${projectName}/practice/practiceAnswer/page.action?page=practiceAnswer_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#practiceAnswerViewButton'" title="查看" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="practiceAnswerViewButton">
	<a href="javascript:$('#practiceAnswerViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >关 闭</a>
</div>
</body>
</html>
