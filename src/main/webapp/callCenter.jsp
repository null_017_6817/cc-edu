<%@ page trimDirectiveWhitespaces="true" language="java" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<html>
  <head>
  	<title>${SystemTitle}</title>
  	
	<script type="text/javascript" src="<c:url value='/'/>js/vccbar/ccMeetManage.js"></script>
	<script type="text/javascript" src="<c:url value='/'/>js/vccbar/CinVccBar.js" charset="UTF-8" ></script>
	<script type="text/javascript" src="<c:url value='/'/>js/vccbar/vccBarConfLoader.js" charset="UTF-8" ></script>
	<script type="text/javascript" src="<c:url value='/'/>js/vccbar/dcpHeartBeatTimer.js" charset="UTF-8" ></script>
	<script type="text/javascript" src="<c:url value='/'/>js/vccbar/CallQueueV2.js" charset="UTF-8" ></script>
	
	<script>

	function initialVccBarMeet(agentId, corpVccId, ccHostIp) {
		agentId = "1101"
		objCinVccBar.AgentID = "000010" + corpVccId + agentId;
		
		objCinVccBar.Dn = "000002" + corpVccId + agentId;

		objCinVccBar.MainIP = ccHostIp;
		objCinVccBar.BackIP = ccHostIp;
		
		objCinVccBar.SipServerIP = ccHostIp;
		objCinVccBar.MonitorIP = ccHostIp;
		
		objCinVccBar.MediaFlag = corpVccId;
		//
		objCinVccBar.Initial();
	}
	
	function onWinLoadMeet() {
		//alert('初始化三方呼叫功能开始');
		if(document.readyState=="complete") {
			regEvent2VccBar();
			
			confVccBar();
			//
			initCcMeetManager();// 初始化三方呼叫功能
			//alert('初始化三方呼叫功能结束');
		}
	}
	
	function initCcMeetManager() {
			var meetArgObj = {tipsSpanId:'CcMeetPromptSpan', ringNotifyFunc:meetCallRingNotifier};
			CcMeetManager.init(objCinVccBar, meetArgObj); // declare in ccMeetManage.js
	}
	
	function meetCallRingNotifier(callTelNum, callId, callSid, callData) {
		//alert("meetCallRingNotifier=" + callTelNum + '随路数据 ：'+callData);
		//practiceFunc(callData);
	}
	</script>
</head>

<body  class="_body" onload="onWinLoadMeet();initialVccBarMeet(agentIdTxt.value, corpVccIdTxt.value, ccHostIpTxt.value);"   onbeforeunload="RunOnBeforeUnload();" scroll="no">
<%--<div class="logoinf2"><p>全国服务外包及联络中心 / 实训平台与专业建设的领航者</p></div>--%>
<div style="width:12.5%;float:left;height:50%;margin-top:2%;margin-left:-20px;position:relative;"> 
<%--电话条--%>
			<div id="vccbarcontainer">
			
				<OBJECT id="objCinVccBar" height="48" width="750" classid="clsid:3E006E71-9408-4F24-9F6D-69C81BD5682F" CODEBASE="CINVCCBAR.CAB">
					<PARAM NAME="_Version" VALUE="65536">
					<PARAM NAME="_ExtentX" VALUE="25321">
					<PARAM NAME="_ExtentY" VALUE="794">
					<PARAM NAME="_StockProps" VALUE="0">
				</OBJECT>
			 
				<INPUT type="hidden" id="ccHostIpTxt" name="ccHostIpTxt" value="cinmedia" size="18" maxlength="20">&nbsp;&nbsp;&nbsp;&nbsp;
				<INPUT type="hidden" id="corpVccIdTxt" name="corpVccIdTxt" value="444444" size="8" maxlength="6">&nbsp;&nbsp;&nbsp;&nbsp;
				<INPUT type="hidden" id="agentIdTxt" name="agentIdTxt" value="${agentId }" size="6" maxlength="10">&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="hidden" id="msgTextArea" name="msgTextArea" value="" />
			</div>

		<input type="hidden" id="recordPage" value="recordPage"/>
</div>
</body>

<script language="javascript">

	function confVccBar() {
		try {
			objCinVccBar.PassWord = "111111";
			objCinVccBar.AppType = 0;		//0：普通坐席；1：监控；2：普通坐席+监控；
			objCinVccBar.AgentType = 0;
			//
			objCinVccBar.LocalPort = 9008;
			//
			objCinVccBar.MonitorPort = 14850;
			objCinVccBar.BackPortID = 14800;
			objCinVccBar.MainPortID = 14800;
			objCinVccBar.SipServerPort = 5060; // SIP Server端口：内网：5060，公网：5040
			objCinVccBar.SipAuthType = 1;
			objCinVccBar.SipPassWord = 111111;
			//必填写    固定       
			objCinVccBar.SipProtocol = "UDP";
			//必填写        固定1   
			objCinVccBar.PhonType = 1;  //0:内置坐席卡；1：内置Sip；2：外置其他终端
			objCinVccBar.SelfPrompt = 1;
			objCinVccBar.VersionType = 1;//0:企业版 1:教育版
			
			
			objCinVccBar.SetUIStyle("depBlack");
		}
		catch(e){
			alert("Error ConfVccBar :" + ex.description);
		}
	}

	function appendCallBack2VccBar(eventStr, callBackFuncStr) {
		var oEventObject = document.createElement("<script language=\"javascript\" for=\"objCinVccBar\" event=\"" + eventStr +"\">");
		oEventObject.text = callBackFuncStr;
		document.body.appendChild(oEventObject);
	}
	
	// 为电话条注册回调函数
	function regEvent2VccBar() {
		// 初始化成功
		appendCallBack2VccBar("OnInitalSuccess()", "onEventInitalSuccess();");
		
		// 初始化失败
		appendCallBack2VccBar("OnInitalFailure(code,description)", "onEventInitalFailure(code,description);");
		
		// 来电振铃
		appendCallBack2VccBar("OnCallRing(CallingNo,CalledNo,OrgCalledNo,CallData,SerialID,ServiceDirect,CallID,UserParam,TaskID,UserDn,AgentDn,AreaCode,fileName,networkInfo)"
				, "onEventOnCallRing(CallingNo, CalledNo, OrgCalledNo, CallData, SerialID, ServiceDirect, CallID, UserParam, TaskID, UserDn, AgentDn, AreaCode, fileName, networkInfo);");
		// 应答
		appendCallBack2VccBar("AnswerCall(UserNo,AnswerTime,SerialID,ServiceDirect,CallID,UserParam,TaskID)"
				, "onEventAnswerCall(UserNo,AnswerTime,SerialID,ServiceDirect,CallID,UserParam,TaskID);");
				
		// 呼叫结束
		appendCallBack2VccBar("OnCallEnd(callID,serialID,serviceDirect,userNo,bgnTime,endTime,agentAlertTime,userAlertTime,fileName,directory,disconnectType,userParam,taskID)"
				, "onOnCallEnd(callID,serialID,serviceDirect,userNo,bgnTime,endTime,agentAlertTime,userAlertTime,fileName,directory,disconnectType,userParam,taskID);");
	}

	String.prototype.format = function() {
		var args = arguments;
		return this.replace(/\{(\d+)\}/g, 
			function(m,i){
				return args[i];
			});
	}

	function onEventOnCallRing(CallingNo, CalledNo, OrgCalledNo, CallData, SerialID, ServiceDirect, CallID, UserParam, TaskID, UserDn, AgentDn) {
		var argInfoPattern = "CallingNo={0}, CalledNo={1}, OrgCalledNo={2}, CallData={3}, 呼叫标识={4}, ServiceDirect={5}, CallID={6}, UserParam={7}, TaskID={8}, 用户号码={9}, AgentDn={10}\r\n";
		
		msgTextArea.value = msgTextArea.value + "来电振铃：\r\n";
		msgTextArea.value = msgTextArea.value + argInfoPattern.format(CallingNo, CalledNo, OrgCalledNo, CallData, SerialID, ServiceDirect, CallID, UserParam, TaskID, UserDn, AgentDn);
		msgTextArea.value = msgTextArea.value + " *******************************************************************\r\n";
	}
	
	function onEventOnCallRing(CallingNo, CalledNo, OrgCalledNo, CallData, SerialID, ServiceDirect, CallID, UserParam, TaskID, UserDn, AgentDn, AreaCode, fileName, networkInfo) {
	    var mediaFile = "http://cinmedia:9999/media/444444/" +  fileName;
		document.getElementById("recordPage").value = mediaFile;
	}
	

	function onEventAnswerCall(UserNo, AnswerTime, SerialID, ServiceDirect, CallID, UserParam, TaskID) {
		var argInfoPattern = "用户号码={0}, 接通时间={1}, 呼叫标识={2}, ServiceDirect={3}, CallID={4}, UserParam={5}, TaskID={6}\r\n";
		
		msgTextArea.value = msgTextArea.value + " 电话接通:\r\n";
		//
		msgTextArea.value = msgTextArea.value + argInfoPattern.format(UserNo, AnswerTime, SerialID, ServiceDirect, CallID, UserParam, TaskID);
		msgTextArea.value = msgTextArea.value + " *******************************************************************\r\n";

	}

	function onEventInitalSuccess() {
		msgTextArea.value = msgTextArea.value + "初始化成功！\r\n";
		msgTextArea.value = msgTextArea.value + " *******************************************************************\r\n";
	}

	function onEventInitalFailure(code,description) {
		msgTextArea.value = msgTextArea.value + "初始化失败！--("+code+")"+description+"\r\n";
		msgTextArea.value = msgTextArea.value + " *******************************************************************\r\n";
	}

	function onOnCallEnd(callID, serialID, serviceDirect, userNo, bgnTime, endTime, agentAlertTime, userAlertTime, fileName, directory, disconnectType, userParam, taskID) {
		var argInfoPattern = "callID={0}, 呼叫标识={1}, serviceDirect={2}, 用户号码={3}, bgnTime={4}, endTime={5}, agentAlertTime={6}, userAlertTime={7}, fileName={8}, directory={9}, disconnectType={10}, userParam={11}, taskID={12}\r\n";
		
		msgTextArea.value = msgTextArea.value + "呼叫结束：\r\n";
		msgTextArea.value = msgTextArea.value + argInfoPattern.format(callID, serialID, serviceDirect, userNo, bgnTime, endTime, agentAlertTime, userAlertTime, fileName, directory, disconnectType, userParam, taskID);
		msgTextArea.value = msgTextArea.value + " *******************************************************************\r\n";
	}
	//开始呼叫
	function makeCall(ivrServiceNum,transParam){
		objCinVccBar.MakeCall(ivrServiceNum, 4, '', transParam);

	}
	//挂机
	function disConnect(){
		objCinVccBar.Disconnect();
	}
	//接听
	function meetAnswer(){
		objCinVccBar.Answer();
	}
		</script>	
</html>


