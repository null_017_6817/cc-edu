var sort="ABCDEFGHIJKLMNOPQRSTUVWXYZ";

$(function() {
	initUEditor();
	initTopicAndStep();
	setBankId();
	setAddQuestionEvent();
	setTopicNameKeyupEvent();
	setStepNameKeyupEvent();
	setTopicTypeComboboxChangeEvent();
});

function initUEditor() {
	var ue1 = UE.getEditor('editor1',{toolbars:[
		[
		 	'simpleupload','source', //源代码
			'removeformat',
		 	'undo', //撤销
			'redo', //重做
			'formatmatch', //格式刷
			'bold', //加粗
			'indent', //首行缩进
			'italic', //斜体
			'underline', //下划线
			'strikethrough', //删除线
			'time', //时间
			'date', //日期
			'justifyleft', //居左对齐
			'justifyright', //居右对齐
			'justifycenter', //居中对齐
			'justifyjustify', //两端对齐
			'fullscreen', //全屏
			'autotypeset', //自动排版
			'touppercase', //字母大写
			'tolowercase', //字母小写
			'forecolor', // 字体颜色
			'rowspacingtop',/*段前*/
			'rowspacingbottom',/*段前*/
			'lineheight',/*行间距*/
			'fontfamily', // 字体
			'fontsize', // 字体大小
			'inserttable', //插入表格
			'insertrow', //前插入行
			'insertcol', //前插入列
			'mergeright', //右合并单元格
			'mergedown', //下合并单元格
			'deleterow', //删除行
			'deletecol', //删除列
			'splittorows', //拆分成行
			'splittocols', //拆分成列
			'splittocells', //完全拆分单元格
			'deletecaption', //删除表格标题
			'inserttitle', //插入标题
			'mergecells', //合并多个单元格
			'deletetable', //删除表格
			'backcolor', //背景色
		]
	],
	autoHeightEnabled:false,
	autoFloatEnabled:false,
	elementPathEnabled:false});
	
	// 标准答案编辑器
	var ue2 = UE.getEditor('editor2',{toolbars:[
		[
			'source', //源代码
			'removeformat',
			'undo', //撤销
			'redo', //重做
			'formatmatch', //格式刷
			'bold', //加粗
			'indent', //首行缩进
			'italic', //斜体
			'underline', //下划线
			'strikethrough', //删除线
			'time', //时间
			'date', //日期
			'justifyleft', //居左对齐
			'justifyright', //居右对齐
			'justifycenter', //居中对齐
			'justifyjustify', //两端对齐
			'fullscreen', //全屏
			'autotypeset', //自动排版
			'touppercase', //字母大写
			'tolowercase', //字母小写
			'forecolor', // 字体颜色
			'rowspacingtop',/*段前*/
			'rowspacingbottom',/*段前*/
			'lineheight',/*行间距*/
			'fontfamily', // 字体
			'fontsize', // 字体大小
			'inserttable', //插入表格
			'insertrow', //前插入行
			'insertcol', //前插入列
			'mergeright', //右合并单元格
			'mergedown', //下合并单元格
			'deleterow', //删除行
			'deletecol', //删除列
			'splittorows', //拆分成行
			'splittocols', //拆分成列
			'splittocells', //完全拆分单元格
			'deletecaption', //删除表格标题
			'inserttitle', //插入标题
			'mergecells', //合并多个单元格
			'deletetable', //删除表格
			'backcolor', //背景色
		]
	],
	autoHeightEnabled:false,
	autoFloatEnabled:true,
	elementPathEnabled:false});
	
	UE.Editor.prototype._bkGetActionUrl = UE.Editor.prototype.getActionUrl;  
	UE.Editor.prototype.getActionUrl = function(action){  
		if(action == 'uploadimage' || action == 'uploadscrawl' || action == 'uploadimage'){  
			return '/'+projectName+'/uploaditemimage.action';  
		}else{  
			return this._bkGetActionUrl.call(this, action);  
		}
	}
}

function initTopic(topic) {
	$("#topicId").val(topic.topicId);
	$("#stepTopicId").val(topic.topicId);
	$("#topicTitle").html("");
		var title_btn = topic.title 
			+ "<div title='添加步骤' class='add-btn' onclick='doAddStep();return false'>+</div>"
			+ "<input type='hidden' name='topicIdLi_' id='topicIdLi_' value='"+topic.topicId+"' />";
	$("#topicTitle").html(title_btn);
	if(topic.title != undefined && topic.title != null && topic.title != "") {
		$("#topicName").textbox("setValue", topic.title);
	}
	if(topic.type != undefined && topic.type != null && topic.type != "") {
		$("#topicType").combobox("setValue", topic.type);
	}
	if("1" == topic.type) {
		changeTopicType(topic.type);
		$("#keyWords").textbox("setValue", topic.keyWords);
		if(topic.bookId != undefined && topic.bookId != null && topic.bookId != "") {
			$("#section").combotree("setValue", topic.bookId);
		}
		var radioArray = $("#topicTable").find("input[type='radio']");
		for(var j = 0; j < radioArray.length; j++) {
			var radioValue = $(radioArray[j]).val();
			if(radioValue == topic.difficulty) {
				$(radioArray[j]).attr("checked", "true");
			}
		}
		$("#totalScore").val(topic.totalScore);
		$("input[name='remark']").val(topic.remark);
	}
	if("2" == topic.type) {
		changeTopicType(topic.type);
		$("#keyWords").textbox("setValue", topic.keyWords);
		var trainType = "";
		if(topic.bookId != undefined && topic.bookId != null && topic.bookId != "") {
			trainType = "1";
			$("#section").combotree("setValue", topic.bookId);
			$("#section_tr").show();
			$("#profession_tr").hide();
		}
		if(topic.profession != undefined && topic.profession != null && topic.profession != "") {
			trainType = "2";
			$("#profession").combotree("setValue", topic.profession);
			$("#profession_tr").show();
			$("#section_tr").hide();
		}
		var trainTypeRadio = $("#topicTable").find("input[name='trainType']");
		for(var j = 0; j < trainTypeRadio.length; j++) {
			var radioValue = $(trainTypeRadio[j]).val();
			if(radioValue == trainType) {
				$(trainTypeRadio[j]).attr("checked", "true");
			}
		}

		var difficultyRadio = $("#topicTable").find("input[name='difficulty']");
		for(var j = 0; j < difficultyRadio.length; j++) {
			var radioValue = $(difficultyRadio[j]).val();
			if(radioValue == topic.difficulty) {
				$(difficultyRadio[j]).attr("checked", "true");
			}
		}

		$("#totalScore").val(topic.totalScore);
		//$("#remark").html(topic.remark);
		$("input[name='remark']").val(topic.remark);
	}
	if("3" == topic.type) {
		changeTopicType(topic.type);
		$("#keyWords").textbox("setValue", topic.keyWords);
		if(topic.profession != undefined && topic.profession != null && topic.profession != "") {
			$("#profession").combotree("setValue", topic.profession);
		}
		var difficultyRadio = $("#topicTable").find("input[name='difficulty']");
		for(var j = 0; j < difficultyRadio.length; j++) {
			var radioValue = $(difficultyRadio[j]).val();
			if(radioValue == topic.difficulty) {
				$(difficultyRadio[j]).attr("checked", "true");
			}
		}

		$("#totalScore").val(topic.totalScore);
		//$("#remark").html(topic.remark);
		$("input[name='remark']").val(topic.remark);
	}
	if("4" == topic.type) {
		changeTopicType(topic.type);
		if(topic.keyWords != undefined && topic.keyWords != null && topic.keyWords != "") {
			$("#keyWords").textbox("setValue", topic.keyWords);
		}
		if(topic.bookId != undefined && topic.bookId != null && topic.bookId != "") {
			$("#section").combotree("setValue", topic.bookId);
		}
		if(topic.templateId != undefined && topic.templateId != null && topic.templateId != "") {
			$("#template").combotree("setValue", topic.templateId);
		}
		var difficultyRadio = $("#topicTable").find("input[name='difficulty']");
		for(var j = 0; j < difficultyRadio.length; j++) {
			var radioValue = $(difficultyRadio[j]).val();
			if(radioValue == topic.difficulty) {
				$(difficultyRadio[j]).attr("checked", "true");
			}
		}
		//$("#remark").html(topic.remark);
		$("input[name='remark']").val(topic.remark);
	}
}

function initSteps(steps, topic) {
	var stepSize = steps.length;
	for(var i = 0; i < stepSize; i++) {
		initStepHead(i);
		initStepBody(steps[i], topic)
	}
}

function initTopicAndStep() {
	var topicId = $(window.parent.document).find("input[name='topicIdHide']").val();
	// bankIdParam = $(window.parent.document).find("input[name='bankIdHide']").val();
	if(topicId != null && topicId != "" && topicId != 0 && typeof(topicId) != "undefined"){
		$.get("/"+projectName+"/manager/bankTopic/loadTopicAndSteps.action",{topicId:topicId},function(result){
			console.log(result);
			var res = JSON.parse(result);
			var topic = res.topic;
			var steps = res.steps;
			initTopic(topic);
			initSteps(steps, topic);
		});
	}
}

function setBankId() {
	var bankId_ = parent.window.getBankId();
	$("#bankId").val(bankId_);
	$("#stepBankId").val(bankId_);
}

/** 添加步骤 */
function setAddQuestionEvent() {
	$("#addQuestion").click(function() {
		var topicName_ = $("#title").val();
		var topicType_ = $("#topicType").val();
		if(topicName_ == "" || topicName_ == null) {
			$.messager.alert("温馨提示","请填写题目名称");
			return;
		}
		if(topicType_ == "" || topicType_ == null) {
			$.messager.alert("温馨提示", "请选择题目类型");
			return;
		}
	});
}

/** 设置题目名称 */
function setTopicNameKeyupEvent() {
	$("#topicName").textbox('textbox').bind('keyup',(function() {
		var titleValue = $(this).val();
		$("#topicTitle").html("");
		var title_btn = titleValue 
		    + "<div title='添加步骤' class='add-btn' onclick='doAddStep();return false'>+</div>";
		$("#topicTitle").html(title_btn);
	}));
}

/** 设置步骤名称 */
function setStepNameKeyupEvent() {
	$("#stepName").textbox('textbox').bind('keyup',(function() {
		var titleValue = $(this).val();
		$("#sortable > li.active").html("");
		var title_btn = titleValue 
		    + "<div title='删除步骤' class='del-btn' onclick='delStepInfo(this, null);return false'>-</div>";
		$("#sortable > li.active").html(title_btn);
	}));
}

/** 题目类型 */
function setTopicTypeComboboxChangeEvent() {
	$("#topicType").combobox({
		onSelect: function(record) {
			var selectValue = record.value;
			// changeTopicType(selectValue);
		},
		onChange: function(newValue, oldValue) {
			if(oldValue == undefined || oldValue == null || oldValue == "") {
				changeTopicType(newValue);
				return;
			}
			if(oldValue == newValue) {
				return;
			}
			var changeFlag = $("#changeFlag").val();
			if("1" == changeFlag) {
				$("#changeFlag").val("0");
				return;
			}
			$.messager.confirm("温馨提示", "变更题目类型会清空当前题目下所有类型步骤,请谨慎操作!", function(v) {
				if(v) {
					// var stepLiEle = $("#sortable").find
					var ids = [];
					var stepLiEle = $("#sortable").children("li");
					var stepSize = stepLiEle.length;
					if(stepSize > 4) {
						for(var i = 2; i < stepSize -2; i++) {
							var stepId_ = $(stepLiEle[i]).val();
							ids.push(stepId_);
							$("#indexIco_division").prev("li").remove();
							$(stepLiEle[i]).remove();
						}
						$.get("/"+projectName+"/manager/bankStep/delBankStep.action",{ids:ids},function(data){
						});
					}	
					changeTopicType(newValue);
				}else {
					$("#changeFlag").val("1");
					$("#topicType").combobox("setValue", oldValue);
				}
			});
		}
	});
}

/** 获取实训营地题干和答案 */
function getCampingPromptAndAnswer(){
	var obj = new Object();
	// var result = "";
	try{
		var u1 = UE.getEditor('editor1').getContent();// 题干
		var u2 = UE.getEditor('editor2').getContent();// 答案
		obj.prompt = u1;
		obj.standardAnswer = u2;
	}catch(error) {
		obj.prompt = "";
		obj.standardAnswer = "";
	}
	return obj;
}

/** 题目类型变更事件 */
function changeTopicType(topicType) {
	$("#topic_form_division").nextAll().remove();
	/** 理论题 */
    if(topicType == "1") { 
		var theory = ""
		+ "<tr>"
			+ "<td class='label w'>章节:</td>"
			+ "<td><input id='section' name='section' type='text' style='width:70%;' /></td>"
		+ "</tr>"
		+ "<tr style='height:36px;'>" 
			+ "<td class='label w'>难度:</td>"
			+ "<td>"
				+ "<label style='margin-left:0px;'>"
                    	+ "<input name='difficulty' type='radio' value='1' style='vertical-align:middle;margin-top:-2px;margin-bottom:1px;' data-options='required:true' />初级"
				+ "</label>"
				+ "<label style='margin-left:10px;'>"
						+ "<input name='difficulty' type='radio' value='2' style='vertical-align:middle;margin-top:-2px;margin-bottom:1px;' />中级" 
				+ "</label>"
                + "<label style='margin-left:10px;'>"
						+ "<input name='difficulty' type='radio' value='3' style='vertical-align:middle;margin-top:-2px;margin-bottom:1px;' />高级"
                + "</label>"
            + "</td>"
		+ "</tr>"
		+ "<tr>"
			+ "<td class='label w'>总分:</td>"
			+ "<td><input id='totalScore' name='totalScore' readonly='readonly' value='' style='width:70%;background-color:#eeeeee;' /></td>"
		+ "</tr>"
		+ "<tr>"
			+ "<td class='label w'>备注:</td>"
			/*+ "<td><textarea id='remark' name='remark' style='width:70%;height:65px;'></textarea></td>"*/
			+ "<td><button type='button' onclick='showRemark(this);'>编辑</button><input name='remark' type='hidden' /></td>"
		+ "</tr>"; 
		$("#topic_form_division").after(theory);
		$("#section").combotree({
			url: "/"+projectName+"/dict?method=synchTree&dictId=CC_BOOK&dynroot=",
			required: false
		});
	}
	/** 实训题 */
	if(topicType == "2") {
		var practical_training = ""
		+ "<tr style='height:36px;'>"
			+ "<td class='label w'>实训类型:</td>"
			+ "<td>"
				+ "<label style='margin-left:0px;'>"
					+ "<input name='trainType' type='radio' checked value='1' style='vertical-align:middle;margin-top:-2px;margin-bottom:1px;' />课程实训"
				+ "</label>"
				+ "<label style='margin-left:10px;'>"
					+ "<input name='trainType' type='radio' value='2' style='vertical-align:middle;margin-top:-2px;margin-bottom:1px;' />行业实训"
				+ "</label>"
			+ "</td>"
		+ "</tr>"
		+ "<tr id='section_tr'>"
			+ "<td class='label w'>章节:</td>"
			+ "<td><input id='section' name='section' type='text' style='width:70%;' /></td>"
		+ "</tr>"
		+ "<tr id='profession_tr'>"
			+ "<td class='label w'>行业:</td>"
			+ "<td><input id='profession' name='profession' type='text' style='width:70%;' /></td>"
		+ "</tr>"

		+ "<tr style='height:36px;'>" 
			+ "<td class='label w'>难度:</td>"
			+ "<td>"
				+ "<label style='margin-left:0px;'>"
                    	+ "<input name='difficulty' type='radio' value='1' style='vertical-align:middle;margin-top:-2px;margin-bottom:1px;' />初级"
				+ "</label>"
				+ "<label style='margin-left:10px;'>"
						+ "<input name='difficulty' type='radio' value='2' style='vertical-align:middle;margin-top:-2px;margin-bottom:1px;' />中级" 
				+ "</label>"
                + "<label style='margin-left:10px;'>"
						+ "<input name='difficulty' type='radio' value='3' style='vertical-align:middle;margin-top:-2px;margin-bottom:1px;' />高级"
                + "</label>"
            + "</td>"
		+ "</tr>"
		+ "<tr>"
			+ "<td class='label w'>总分:</td>"
			+ "<td><input id='totalScore' name='totalScore' readonly='readonly' value='' style='width:70%;background-color:#eeeeee;' /></td>"
		+ "</tr>"
		+ "<tr>"
			+ "<td class='label w'>备注:</td>"
			/*+ "<td><textarea id='remark' name='remark' style='width:70%;height:65px;'></textarea></td>"*/
			+ "<td><button type='button' onclick='showRemark(this);'>编辑</button><input name='remark' type='hidden' /></td>"
		+ "</tr>"; 
		$("#topic_form_division").after(practical_training);
		$("#profession_tr").hide();
		$("input[name='trainType']").click(function() {
			var radioValue = $(this).val();
			if(radioValue == "1") {
				$("#section_tr").show();
				$("#profession_tr").hide();
			}
			if(radioValue == "2") {
				$("#section_tr").hide();
				$("#profession_tr").show();
			}
		});

		$("#profession").combotree({
			url: "/"+projectName+"/dict?method=synchTree&dictId=ZD_REPOSITORY&dynroot=",
			required: true
		});

		$("#section").combotree({
			url: "/"+projectName+"/dict?method=synchTree&dictId=CC_BOOK&dynroot=",
			required: true
		});
	}
	/** 技能训练题 */
	if(topicType == "3") {
		var skill_training = ""
		+ "<tr>"
			+ "<td class='label w'>行业:</td>"
			+ "<td><input id='profession' name='profession' type='text' style='width:70%;' /></td>"
		+ "</tr>"

		+ "<tr style='height:36px;'>" 
			+ "<td class='label w'>难度:</td>"
			+ "<td>"
				+ "<label style='margin-left:0px;'>"
                    	+ "<input name='difficulty' type='radio' value='1' style='vertical-align:middle;margin-top:-2px;margin-bottom:1px;' />初级"
				+ "</label>"
				+ "<label style='margin-left:10px;'>"
						+ "<input name='difficulty' type='radio' value='2' style='vertical-align:middle;margin-top:-2px;margin-bottom:1px;' />中级" 
				+ "</label>"
                + "<label style='margin-left:10px;'>"
						+ "<input name='difficulty' type='radio' value='3' style='vertical-align:middle;margin-top:-2px;margin-bottom:1px;' />高级"
                + "</label>"
            + "</td>"
		+ "</tr>"
		+ "<tr>"
			+ "<td class='label w'>总分:</td>"
			+ "<td><input id='totalScore' name='totalScore' readonly='readonly' value='' style='width:70%;background-color:#eeeeee;' /></td>"
		+ "</tr>"
		+ "<tr>"
			+ "<td class='label w'>备注:</td>"
			/*+ "<td><textarea id='remark' name='remark' style='width:70%;height:65px;'></textarea></td>"*/
			+ "<td><button type='button' onclick='showRemark(this);'>编辑</button><input name='remark' type='hidden' /></td>"
		+ "</tr>"; 
		$("#topic_form_division").after(skill_training);
		$("#profession").combotree({
			url: "/"+projectName+"/dict?method=synchTree&dictId=ZD_REPOSITORY&dynroot=",
			required: true
		});
	}
	/** 实训营地题 */
	if(topicType == "4") {
		var training_camp = ""
		+ "<tr>"
			+ "<td class='label w'>章节:</td>"
			+ "<td><input id='section' name='section' type='text' style='width:70%;' /></td>"
		+ "</tr>"

		+ "<tr>"
			+ "<td class='label w'>评分模板:</td>"
			+ "<td><input id='template' name='template' type='text' style='width:70%;' /></td>"
		+ "</tr>"

		+ "<tr style='height:36px;'>" 
			+ "<td class='label w'>难度:</td>"
			+ "<td>"
				+ "<label style='margin-left:0px;'>"
                    	+ "<input name='difficulty' type='radio' value='1' style='vertical-align:middle;margin-top:-2px;margin-bottom:1px;' />初级"
				+ "</label>"
				+ "<label style='margin-left:10px;'>"
						+ "<input name='difficulty' type='radio' value='2' style='vertical-align:middle;margin-top:-2px;margin-bottom:1px;' />中级" 
				+ "</label>"
                + "<label style='margin-left:10px;'>"
						+ "<input name='difficulty' type='radio' value='3' style='vertical-align:middle;margin-top:-2px;margin-bottom:1px;' />高级"
                + "</label>"
            + "</td>"
		+ "</tr>"
		// + "<tr>"
		// 	+ "<td class='label w'>总分:</td>"
		// 	+ "<td><input id='totalScore' name='totalScore' readonly='readonly' value='' style='width:70%;background-color:#eeeeee;' /></td>"
		// + "</tr>"
		+ "<tr>"
			+ "<td class='label w'>备注:</td>"
			/*+ "<td><textarea id='remark' name='remark' style='width:70%;height:65px;'></textarea></td>"*/
			+ "<td><button type='button' onclick='showRemark(this);'>编辑</button><input name='remark' type='hidden' /></td>"
		+ "</tr>"; 
		$("#topic_form_division").after(training_camp);
		$("#section").combotree({
			url: "/"+projectName+"/dict?method=synchTree&dictId=CC_BOOK&dynroot=",
			required: true
		});
		$("#template").combotree({
			url: "/"+projectName+"/dict?method=synchTree&dictId=CC_TEMPLATE&dynroot=",
			required: true
		});
	}
}

function doShowTopic(firstLi_, liType_) {
	// saveTopicDetail();
	showTopicInfo(firstLi_, liType_);
}

function saveTopicDetail() {
	var difficultyValue_ = $("input[name='difficulty']:checked").val();
	if(difficultyValue_ == undefined || difficultyValue_ == null || difficultyValue_ == "") {
		$.messager.alert("温馨提示", "请选择题目难度");
		return;
	}
	var topicFormData_ = $("#topic_form").serialize();
	var status = "";
	$.ajax({
			type:"POST",
			url:'/'+projectName+'/manager/bankTopic/saveBankTopicAndStep.action',
			data:{"topicFormData":topicFormData_,"stepFormData":""},
			dataType:'json',
			async: false,
			success:function(result){
				var topicId = result.topicId;
				status = result.status;
				if(status == "1") {
					$("#topicId").val(topicId);
					$("#topicIdLi_").val(topicId);
					$("#stepTopicId").val(topicId);
				}
				// if(result.success){
				// 	var data = result.data;
				// 	var topic = data.topic;
				// 	var topictype = topic.type;
				// 	// 回显topic
				// 	var stepInfoList = data.stepInfoList;
				// 	showTopic(topic,stepInfoList);
				// }else{
				// 	alert(result.msg);
				// }
			}
	});
}

function saveStepDetail() {
	var campPromptAndAnswer = getCampingPromptAndAnswer();
	var campPrompt = campPromptAndAnswer.prompt;
	var campAnswer = campPromptAndAnswer.standardAnswer;
	$("#campPrompt").val(campPrompt);
	$("#campAnswer").val(campAnswer);
	var stepFormData_ = $("#step_form").serialize();
	var status = "";
	$.ajax({
			type:"POST",
			url:'/'+projectName+'/manager/bankTopic/saveBankTopicAndStep.action',
			data:{"topicFormData": "", "stepFormData":stepFormData_},
			dataType:'json',
			async: false,
			success:function(result){
				var stepId = result.stepId;
				status = result.status;
				if(status == "1") {
					$("#stepId").val(stepId);
					$("#sortable > li.active").val(stepId);
					// $("#topicId").val(topicId);
					// $("#topicIdLi_").val(topicId);
					// $("#stepTopicId").val(topicId);
				}
				// if(result.success){
				// 	var data = result.data;
				// 	var topic = data.topic;
				// 	var topictype = topic.type;
				// 	// 回显topic
				// 	var stepInfoList = data.stepInfoList;
				// 	showTopic(topic,stepInfoList);
				// }else{
				// 	alert(result.msg);
				// }
			}
	});
}

function loadTopicDetail(topicId_) {

}

function loadStepDetail(stepId_) {
	window.parent.openStatusDialog();
	$.ajax({
		type:"GET",
		url:'/'+projectName+'/manager/bankTopic/loadBankTopicAndStep.action?topicId=&stepId='+stepId_,
		async: false,
		success:function(result){
			console.log("result:"+result);
			var res = JSON.parse(result);
			var topic = res.topic;
			var step = res.step;
			if(step == undefined || step == null || step == "") {
				return;
			}
			if(step.stepType == undefined || step.stepType == null || step.stepType == "") {
				return;
			}
			changeStepType(step.stepType);
			$("#stepId").val(step.stepId);
			$("#stepBankId").val(step.bankId);
			$("#stepTopicId").val(step.topicId);
			if(step.stepName != undefined && step.stepName != null && step.stepName != "") {
				$("#stepName").textbox("setValue",step.stepName);
			}
			if(step.stepType != undefined && step.stepType != null && step.stepType != "") {
				$("#stepType").combobox("select", step.stepType);
			}
			if(step.stepType == "1") {
				$("#prompt").html(step.promptContent);
				$("#audioContainer").next("input").val(step.promptAudio);
				var audioUrl = "/"+projectName+"/paper/question/appendUrl.action?filename="+step.bankId+"/"+step.topicId+"/"+step.promptAudio;
				$("#audioContainer").find('audio').eq(0).get(0).src = audioUrl;

				var singleChoiceContent = step.singleChoiceContent;
				var singleChoiceAudio = step.singleChoiceAudio;
				var choiceSize = singleChoiceContent.length;
				if(choiceSize > 2) {
					for(var s = 0; s < choiceSize - 2; s++) {
						doAddCho();
					}
				}
				var choiceArray = $("#stepTable").find("tr[name='choice']");
				for(var i = 0; i < choiceArray.length; i++) {
					var choiceEle = $(choiceArray[i]).find("input[id*='choice_']");
					var choiceContentId = $(choiceEle[0]).attr("id")+"";
					var choiceContentKey = choiceContentId.substr(7, 1);
					var choiceContent = singleChoiceContent[i][choiceContentKey];
					$(choiceEle[0]).val(choiceContent);
					var choiceAudioId = $(choiceEle[1]).attr("id")+"";
					var choiceAudioKey = choiceAudioId.substr(7, 1);
					var choiceAudio = singleChoiceAudio[i][choiceAudioKey];
					if(choiceAudio != undefined && choiceAudio != null && choiceAudio != "") {
						var choiceAudioUrl = "/"+projectName+"/paper/question/appendUrl.action?filename="+step.bankId+"/"+step.topicId+"/"+step.promptAudio;
						var choiceAudioSource = $(choiceArray[i]).find("audio").eq(0).get(0);
						choiceAudioSource.src = choiceAudioUrl;
					}
					$(choiceEle[1]).val(choiceAudio);
				}
				var radioArray = $("#stepTable").find("input[type='radio']");
				for(var j = 0; j < radioArray.length; j++) {
					var radioValue = $(radioArray[j]).val();
					if(radioValue == step.standardAnswer) {
						$(radioArray[j]).attr("checked", "true");
					}
				}
				$("#score").val(step.score);
			}
			// 多选
			if(step.stepType == "2") {
				$("#prompt").html(step.promptContent);
				$("#audioContainer").next("input").val(step.promptAudio);
				var audioUrl = "/"+projectName+"/paper/question/appendUrl.action?filename="+step.bankId+"/"+step.topicId+"/"+step.promptAudio;
				$("#audioContainer").find('audio').eq(0).get(0).src = audioUrl;

				var multiChoiceContent = step.multiChoiceContent;
				var multiChoiceAudio = step.multiChoiceAudio;
				var multiChoiceSize = multiChoiceContent.length;
				if(multiChoiceSize > 2) {
					for(var s = 0; s < multiChoiceSize - 2; s++) {
						doAddChos();
					}
				}
				var choiceArray = $("#stepTable").find("tr[name='choice']");
				for(var i = 0; i < choiceArray.length; i++) {
					var choiceEle = $(choiceArray[i]).find("input[id*='choice_']");
					var choiceContentId = $(choiceEle[0]).attr("id")+"";
					var choiceContentKey = choiceContentId.substr(7, 1);
					var choiceContent = multiChoiceContent[i][choiceContentKey];
					$(choiceEle[0]).val(choiceContent);
					var choiceAudioId = $(choiceEle[1]).attr("id")+"";
					var choiceAudioKey = choiceAudioId.substr(7, 1);
					var choiceAudio = multiChoiceAudio[i][choiceAudioKey];
					if(choiceAudio != undefined && choiceAudio != null && choiceAudio != "") {
						var choiceAudioUrl = "/"+projectName+"/paper/question/appendUrl.action?filename="+step.bankId+"/"+step.topicId+"/"+step.promptAudio;
						var choiceAudioSource = $(choiceArray[i]).find("audio").eq(0).get(0);
						choiceAudioSource.src = choiceAudioUrl;
					}
					$(choiceEle[1]).val(choiceAudio);
				}

				var multiChoiceAnswer = step.multiChoiceAnswer;

				var answerArray = $("#stepTable").find("input[name*='standardAnswer_']");
				for(var i = 0; i < answerArray.length; i++) {
					var answerInputName = $(answerArray[i]).attr("name")+"";
					var answerCheckedKey = answerInputName.substr(15, 1);
					for(var j = 0; j < multiChoiceAnswer.length; j++) {
						if(multiChoiceAnswer[j] == answerCheckedKey) {
							$(answerArray[i]).attr("checked", "true");
						}
					}
				}
				$("#score").val(step.score);
			}
			// 判断
			if(step.stepType == "4") {
				$("#prompt").html(step.promptContent);
				$("#audioContainer").next("input").val(step.promptAudio);
				var audioUrl = "/"+projectName+"/paper/question/appendUrl.action?filename="+step.bankId+"/"+step.topicId+"/"+step.promptAudio;
				$("#audioContainer").find('audio').eq(0).get(0).src = audioUrl;

				var radioArray = $("#stepTable").find("input[type='radio']");
				var standardAnswer = step.standardAnswer;
				if(standardAnswer == "true") {
					$(radioArray[0]).attr("checked", "true");
				}
				if(standardAnswer == "false") {
					$(radioArray[1]).attr("checked", "false");
				}
				$("#score").val(step.score);
			}
			// 填空
			if(step.stepType == "9") {
				$("#prompt").html(step.promptContent);
				$("#everyScore").val(step.score);
				var clozedAnswerContent = step.clozedAnswerContent;
				var clozedContentSize = clozedAnswerContent.length;
				if(clozedContentSize > 1) {
					for(var s = 0; s < clozedContentSize - 1; s++) {
						addFilld();
					}
				}
				
				// var clozedAnswerScore = step.clozedAnswerScore;
				var clozedContentArray = $("#stepTable").find("input[name*='standardAnswer_']");
				// var clozedScoreArray = $("#stepTable").find("input[name*='score_']");
				for(var i = 0; i < clozedContentArray.length; i++) {
					var clozedContentName = $(clozedContentArray[i]).attr("name");
					var clozedContentKey = clozedContentName.substr(15, 1);
					$(clozedContentArray[i]).val(clozedAnswerContent[i][clozedContentKey]);
				}
				// for(var j = 0; j < clozedScoreArray.length; j++) {
				// 	var clozedScoreName = $(clozedScoreArray[j]).attr("name");
				// 	var clozedScoreKey = clozedScoreName.substr(6, 1);
				// 	$(clozedScoreArray[j]).val(clozedAnswerScore[j][clozedScoreKey]);
				// }
			}
			// 问答
			if(step.stepType == "3") {
				$("#prompt").html(step.promptContent);
				$("#audioContainer").next("input").val(step.promptAudio);
				var audioUrl = "/"+projectName+"/paper/question/appendUrl.action?filename="+step.bankId+"/"+step.topicId+"/"+step.promptAudio;
				$("#audioContainer").find('audio').eq(0).get(0).src = audioUrl;

				$("#standardAnswer").html(step.standardAnswer);
				$("#score").val(step.score);

				var origiName = step.origiName;
				var fileName = step.fileName;
				$("#origiName").val(origiName);
				$("#fileName").val(fileName);
				var fileLink = "" +
					"<a name='downloadZipA' " +
					"href='javascript:downloadZip(\"" + fileName + "\",\"" + origiName + "\")'>" + 
						origiName +
					"</a>";
				$("#importPraFile").after(fileLink);
			}
			// 复读陈述题
			if(step.stepType == "6") {
				$("#prompt").html(step.promptContent);
				$("#audioContainer").next("input").val(step.promptAudio);
				var audioUrl = "/"+projectName+"/paper/question/appendUrl.action?filename="+step.bankId+"/"+step.topicId+"/"+step.promptAudio;
				$("#audioContainer").find('audio').eq(0).get(0).src = audioUrl;

				$("#standardAnswer").html(step.standardAnswer);
				$("#score").val(step.score);
			}
			// 应答陈述题
			if(step.stepType == "7") {
				$("#prompt").html(step.promptContent);
				$("#audioContainer").next("input").val(step.promptAudio);
				var audioUrl = "/"+projectName+"/paper/question/appendUrl.action?filename="+step.bankId+"/"+step.topicId+"/"+step.promptAudio;
				$("#audioContainer").find('audio').eq(0).get(0).src = audioUrl;

				$("#standardAnswer").html(step.standardAnswer);
				$("#score").val(step.score);
			}
			// 语音提示
			if(step.stepType == "8") {
				$("#prompt").html(step.promptContent);
				$("#audioContainer").next("input").val(step.promptAudio);
				var audioUrl = "/"+projectName+"/paper/question/appendUrl.action?filename="+step.bankId+"/"+step.topicId+"/"+step.promptAudio;
				$("#audioContainer").find('audio').eq(0).get(0).src = audioUrl;

				$("#standardAnswer").html(step.standardAnswer);
				$("#score").val(step.score);
			}
			// 听打录入
			if(step.stepType == "5") {
				$("#audioContainer").next("input").val(step.promptAudio);
				var audioUrl = "/"+projectName+"/paper/question/appendUrl.action?filename="+step.bankId+"/"+step.topicId+"/"+step.promptAudio;
				$("#audioContainer").find('audio').eq(0).get(0).src = audioUrl;

				var clozePromptContent = step.clozePromptContent;
				var clozeSize = clozePromptContent.length;
				if(clozeSize > 1) {
					for(var s = 0; s < clozeSize - 1; s++) {
						addFill();
					}
				}
				var clozePromptArray = $("#stepTable").find("input[name*='prompt_']");
				for(var i = 0; i < clozePromptArray.length; i++) {
					var clozePromptName = $(clozePromptArray[i]).attr("name");
					var clozePromptKey = clozePromptName.substr(7, 1);
					$(clozePromptArray[i]).val(clozePromptContent[i][clozePromptKey]);
				}

				var clozeAnswerContent = step.clozeAnswerContent;
				var clozeAnswerArray = $("#stepTable").find("input[name*='standardAnswer_']");
				for(var j = 0; j < clozeAnswerArray.length; j++) {
					var clozeAnswerName = $(clozeAnswerArray[j]).attr("name");
					var clozeAnswerKey = clozeAnswerName.substr(15, 1);
					$(clozeAnswerArray[j]).val(clozeAnswerContent[j][clozeAnswerKey]);
				}

				var clozeAnswerScore = step.clozeAnswerScore;
				var clozeScoreArray = $("#stepTable").find("input[name*='score_']");
				for(var k = 0; k < clozeScoreArray.length; k++) {
					var clozeScoreName = $(clozeScoreArray[k]).attr("name");
					var clozeScoreKey = clozeScoreName.substr(6, 1);
					$(clozeScoreArray[k]).val(clozeAnswerScore[k][clozeScoreKey]);
				}
			}
			// 数据分析题
			if(step.stepType == "11") {
				$("#prompt").html(step.promptContent);
				$("#audioContainer").next("input").val(step.promptAudio);
				var audioUrl = "/"+projectName+"/paper/question/appendUrl.action?filename="+step.bankId+"/"+step.topicId+"/"+step.promptAudio;
				$("#audioContainer").find('audio').eq(0).get(0).src = audioUrl;

				$("#standardAnswer").html(step.standardAnswer);
				$("#score").val(step.score);

				var origiName = step.origiName;
				var fileName = step.fileName;
				$("#origiName").val(origiName);
				$("#fileName").val(fileName);
				var fileLink = "" +
					"<a name='downloadZipA' " +
					"href='javascript:downloadZip(\"" + fileName + "\",\"" + origiName + "\")'>" + 
						origiName +
					"</a>";
				$("#importPraFile").after(fileLink);
			}
			// 话术编写题
			if(step.stepType == "12") {
				$("#prompt").html(step.promptContent);
				$("#audioContainer").next("input").val(step.promptAudio);
				var audioUrl = "/"+projectName+"/paper/question/appendUrl.action?filename="+step.bankId+"/"+step.topicId+"/"+step.promptAudio;
				$("#audioContainer").find('audio').eq(0).get(0).src = audioUrl;

				$("#standardAnswer").html(step.standardAnswer);
				$("#score").val(step.score);

				var origiName = step.origiName;
				var fileName = step.fileName;
				$("#origiName").val(origiName);
				$("#fileName").val(fileName);
				var fileLink = "" +
					"<a name='downloadZipA' " +
					"href='javascript:downloadZip(\"" + fileName + "\",\"" + origiName + "\")'>" + 
						origiName +
					"</a>";
				$("#importPraFile").after(fileLink);
			}
			// 连线画图题
			if(step.stepType == "14") {
				$("#prompt").html(step.promptContent);
				$("#audioContainer").next("input").val(step.promptAudio);
				var audioUrl = "/"+projectName+"/paper/question/appendUrl.action?filename="+step.bankId+"/"+step.topicId+"/"+step.promptAudio;
				$("#audioContainer").find('audio').eq(0).get(0).src = audioUrl;

				$("#standardAnswer").html(step.standardAnswer);
				$("#score").val(step.score);

				var origiName = step.origiName;
				var fileName = step.fileName;
				$("#origiName").val(origiName);
				$("#fileName").val(fileName);
				var fileLink = "" +
					"<a name='downloadZipA' " +
					"href='javascript:downloadZip(\"" + fileName + "\",\"" + origiName + "\")'>" + 
						origiName +
					"</a>";
				$("#importPraFile").after(fileLink);
			}

			// 实训营地
			if(step.stepType == "13") {
				UE.getEditor('editor1').setContent(step.promptContent);
				UE.getEditor('editor2').setContent(step.standardAnswer);
			}
		}
	});
	// $("#loading").hide();
    window.parent.closeStatusDialog();
}

function doAddStep() {
	var topicName = $("#topicName").val();
	if(topicName == undefined || topicName == null || topicName == "") {
		$.messager.alert("温馨提示", "请输入题目名称");
		return;
	}
	var topicType = $("#topicType").combobox("getValue");
	if(topicType == undefined || topicType == null || topicType == "") {
		$.messager.alert("温馨提示", "请选择题目类型");
		return;
	}
	var step_li = $("#sortable").children("li");
	var stepIndex = step_li.length - 4 + 1;
	if(topicType == "1") {
		saveTopicDetail();
		addTheoryStep(stepIndex, topicType);
	}
	if(topicType == "2") {
		saveTopicDetail();
		addPracticalStep(stepIndex, topicType);
	}
	if(topicType == "3") {
		saveTopicDetail();
		addSkillStep(stepIndex, topicType);
	}
	if(topicType == "4") {
		saveTopicDetail();
		addCampingStep(stepIndex, topicType);
	}
}

function addStepHead(index) {
	var stepHead = "" 
		+ "<li>" + "o" + "</li>";
	$("#indexIco_division").before(stepHead);
}

function initStepHead(index) {
	var stepHead = "" 
		+ "<li>" + "o" + "</li>";
	$("#indexIco_division").before(stepHead);
}

function showTopicInfo(showLi_, liType_) {

	var allLi_ = $("#sortable").children("li");
	for(var i = 0; i < allLi_.length; i++) {
		var isActive = $(allLi_[i]).hasClass("active");
		if(isActive) {
			$(allLi_[i]).removeClass("active");
			$(allLi_[i]).children(":first").hide();
		}
	}
	$(showLi_).addClass("active");
	$(showLi_).children(":first").show();
	if(liType_ == "topic") {
		$("#topicContainer").show();
		$("#stepContainer").hide();
	}
	if(liType_ == "step") {
		$("#topicContainer").hide();
		$("#stepContainer").show();
	}
}

function showTheoryStepInfo(showLi_, liType_) {
	var allLi_ = $("#sortable").children("li");
	for(var i = 0; i < allLi_.length; i++) {
		var isActive = $(allLi_[i]).hasClass("active");
		if(isActive) {
			$(allLi_[i]).removeClass("active");
			$(allLi_[i]).children(":first").hide();
		}
	}
	$(showLi_).addClass("active");
	$(showLi_).children(":first").show();
	if(liType_ == "topic") {
		$("#topicContainer").show();
		$("#stepContainer").hide();
	}
	if(liType_ == "step") {
		$("#topicContainer").hide();
		$("#stepContainer").show();
	}

	var stepId = $(showLi_).val();
	// $("#stepId").val(stepId);
	loadStepDetail(stepId);
}

function showPracticalStepInfo(showLi_, liType_) {
	/** 保存当前active步骤 */
	// $.messager.confirm("", "", function(e) {
	// 	if(e) {
	// 		saveStepDetail();
	// 	}else {
	// 		return;
	// 	}
	// });
	var isCurrActive = $(showLi_).hasClass("active");
	if(isCurrActive) {
		return;
	}
	var topicTitleActive = $("#topicTitle").hasClass("active");
	if(!topicTitleActive) {
		saveStepDetail();
	}
	/** 样式 */
	var allLi_ = $("#sortable").children("li");
	for(var i = 0; i < allLi_.length; i++) {
		var isActive = $(allLi_[i]).hasClass("active");
		if(isActive) {
			var liActiveValue = $(allLi_[i]).val();
			if(liActiveValue != undefined && liActiveValue != null && liActiveValue != "") {
				$("#stepId").val(liActiveValue);
			}else {
				$("#stepId").val("");
			}
			$(allLi_[i]).removeClass("active");
			$(allLi_[i]).children(":first").hide();
		}
	}
	$(showLi_).addClass("active");
	$(showLi_).children(":first").show();
	if(liType_ == "topic") {
		$("#topicContainer").show();
		$("#stepContainer").hide();
	}
	if(liType_ == "step") {
		$("#topicContainer").hide();
		$("#stepContainer").show();
	}
	var praBankId = $("#stepBankId").val();
	var praTopicId = $("#stepTopicId").val();
	var stepId = $(showLi_).val();
	/** 清空step form */
	// $("#step_form")[0].clear();
	$("#step_form").form("clear");
	/** 加载即将选择的步骤 */
	if("0" == stepId) {
		$("#stepId").val("");
	}else {
		$("#stepId").val(stepId);
	}
	$("#stepBankId").val(praBankId);
	$("#stepTopicId").val(praTopicId);
	if(stepId == undefined || stepId == null || stepId == "") {
		$("#step_form_division").nextAll().remove();	
		return;
	}
	loadStepDetail(stepId);
}

function showSkillStepInfo(showLi_, liType_) {
	/** 保存当前active步骤 */
	// $.messager.confirm("", "", function(e) {
	// 	if(e) {
	// 		saveStepDetail();
	// 	}else {
	// 		return;
	// 	}
	// });
	var isCurrActive = $(showLi_).hasClass("active");
	if(isCurrActive) {
		return;
	}
	var topicTitleActive = $("#topicTitle").hasClass("active");
	if(!topicTitleActive) {
		saveStepDetail();
	}
	/** 样式 */
	var allLi_ = $("#sortable").children("li");
	for(var i = 0; i < allLi_.length; i++) {
		var isActive = $(allLi_[i]).hasClass("active");
		if(isActive) {
			var liActiveValue = $(allLi_[i]).val();
			if(liActiveValue != undefined && liActiveValue != null && liActiveValue != "") {
				$("#stepId").val(liActiveValue);
			}else {
				$("#stepId").val("");
			}
			$(allLi_[i]).removeClass("active");
			$(allLi_[i]).children(":first").hide();
		}
	}
	$(showLi_).addClass("active");
	$(showLi_).children(":first").show();
	if(liType_ == "topic") {
		$("#topicContainer").show();
		$("#stepContainer").hide();
	}
	if(liType_ == "step") {
		$("#topicContainer").hide();
		$("#stepContainer").show();
	}
	var praBankId = $("#stepBankId").val();
	var praTopicId = $("#stepTopicId").val();
	var stepId = $(showLi_).val();
	/** 清空step form */
	// $("#step_form")[0].clear();
	$("#step_form").form("clear");
	/** 加载即将选择的步骤 */
	if("0" == stepId) {
		$("#stepId").val("");
	}else {
		$("#stepId").val(stepId);
	}
	$("#stepBankId").val(praBankId);
	$("#stepTopicId").val(praTopicId);
	if(stepId == undefined || stepId == null || stepId == "") {
		$("#step_form_division").nextAll().remove();	
		return;
	}
	loadStepDetail(stepId);
}

function showCampingStepInfo(showLi_, liType_) {
	/** 保存当前active步骤 */
	// $.messager.confirm("", "", function(e) {
	// 	if(e) {
	// 		saveStepDetail();
	// 	}else {
	// 		return;
	// 	}
	// });
	var isCurrActive = $(showLi_).hasClass("active");
	if(isCurrActive) {
		return;
	}
	var topicTitleActive = $("#topicTitle").hasClass("active");
	if(!topicTitleActive) {
		saveStepDetail();
	}
	/** 样式 */
	var allLi_ = $("#sortable").children("li");
	for(var i = 0; i < allLi_.length; i++) {
		var isActive = $(allLi_[i]).hasClass("active");
		if(isActive) {
			var liActiveValue = $(allLi_[i]).val();
			if(liActiveValue != undefined && liActiveValue != null && liActiveValue != "") {
				$("#stepId").val(liActiveValue);
			}else {
				$("#stepId").val("");
			}
			$(allLi_[i]).removeClass("active");
			$(allLi_[i]).children(":first").hide();
		}
	}
	$(showLi_).addClass("active");
	$(showLi_).children(":first").show();
	if(liType_ == "topic") {
		$("#topicContainer").show();
		$("#stepContainer").hide();
	}
	if(liType_ == "step") {
		$("#topicContainer").hide();
		$("#stepContainer").show();
	}
	var praBankId = $("#stepBankId").val();
	var praTopicId = $("#stepTopicId").val();
	var stepId = $(showLi_).val();
	/** 清空step form */
	// $("#step_form")[0].clear();
	$("#step_form").form("clear");
	/** 加载即将选择的步骤 */
	if("0" == stepId) {
		$("#stepId").val("");
	}else {
		$("#stepId").val(stepId);
	}
	$("#stepBankId").val(praBankId);
	$("#stepTopicId").val(praTopicId);
	if(stepId == undefined || stepId == null || stepId == "") {
		$("#step_form_division").nextAll().remove();	
		return;
	}
	loadStepDetail(stepId);
}

function delStepInfo(delDiv_, index) {
	var stepId_ =  $(delDiv_).parent().val();
	var ids = [];
	ids.push(stepId_);
	$.get("/"+projectName+"/manager/bankStep/delBankStep.action",{ids:ids},function(data){
	});
	
	$(delDiv_).parent().remove();
	$("#indexIco_division").prev("li").remove();
	var step_lis = $("#sortable").children("li");
	var stepSize = step_lis.length;
	if(stepSize > 4) {
		// var currLi_ = $(delDiv_).parent();
		// var prevLi_ = $(currLi_).prev
		$("#step_division").prev("li").trigger("click");
		// $(lastLi_).trigger("click");
	}	
	if(stepSize == 4) {
		$("#topicTitle").trigger("click");
	}
	// var index_lis = $("#indexIco").children("li");
	// for(var i = 0; i < index_lis.length; i++) {
	// 	var index_li_html = $(index_lis[i]).html();
	// 	if(index_li_html != index) {
	// 		continue;
	// 	}else {
	// 		$(index_lis[i]).remove();
	// 	}
	// }
}

function delAllStep() {

}

function addStepBody(index, type) {
	/** 理论题步骤 */
	if(type == "1") {
		var theoryStep = ""
			+ "<li class='question' onclick='showTheoryStepInfo(this, \"step\");'>步骤" + index
				+ "<div title='删除步骤' class='del-btn' style='display:none;' onclick='delStepInfo(this, " + index + ");return false;'>-</div>"
			+ "</li>";
		$("#step_division").before(theoryStep);
	}
	if(type == "2") {
		var practicalStep = ""
			+ "<li class='question' onclick='showPracticalStepInfo(this, \"step\");'>步骤" + index
				+ "<div title='删除步骤' class='del-btn' style='display:none;' onclick='delStepInfo(this, " + index + ");return false;'>-</div>"
			+ "</li>";
		$("#step_division").before(practicalStep);
	}
	if(type == "3") {
		var skillStep = ""
			+ "<li class='question' onclick='showSkillStepInfo(this, \"step\");'>步骤" + index
				+ "<div title='删除步骤' class='del-btn' style='display:none;' onclick='delStepInfo(this, " + index + ");return false;'>-</div>"
			+ "</li>";
		$("#step_division").before(skillStep);
	}
	if(type == "4") {
		var campingStep = ""
			+ "<li class='question' onclick='showCampingStepInfo(this, \"step\");'>步骤" + index
				+ "<div title='删除步骤' class='del-btn' style='display:none;' onclick='delStepInfo(this, " + index + ");return false;'>-</div>"
			+ "</li>";
		$("#step_division").before(campingStep);
	}
}

function initStepBody(step, topic) {
	var type = topic.type;
	/** 理论题步骤 */
	if(type == "1") {
		var theoryStep = ""
			+ "<li class='question' value='"+step.stepId+"' onclick='showTheoryStepInfo(this, \"step\");'>" + step.stepName
				+ "<div title='删除步骤' class='del-btn' style='display:none;' onclick=\"delStepInfo(this, '');return false;\">-</div>"
			+ "</li>";
		$("#step_division").before(theoryStep);
		$("#stepType").combobox({
			valueField:'value',
			textField:'label',
			data:[
				{label:'单选题',value:'1'},
				{label:'多选题',value:'2'},
				{label:'判断题',value:'4'},
				{label:'填空题',value:'9'},
				{label:'问答题',value:'3'}
			],
			required: true,
			onSelect: function(record) {
				var selectStepType = record.value;
				changeStepType(selectStepType);
			}
		});
	}
	if(type == "2") {
		var practicalStep = ""
			+ "<li class='question' value='"+step.stepId+"' onclick='showPracticalStepInfo(this, \"step\");'>" + step.stepName
				+ "<div title='删除步骤' class='del-btn' style='display:none;' onclick=\"delStepInfo(this, '');return false;\">-</div>"
			+ "</li>";
		$("#step_division").before(practicalStep);
		$("#stepType").combobox({
			valueField:'value',
			textField:'label',
			data:[
				{label:'单选题',value:'1'},
				{label:'多选题',value:'2'},
				{label:'判断题',value:'4'},
				{label:'填空题',value:'9'},
				{label:'问答题',value:'3'},
				{label:'复读陈述题',value:'6'},
				{label:'应答陈述题',value:'7'},
				{label:'语音提示',value:'8'}
			],
			required: true,
			onSelect: function(record) {
				var selectStepType = record.value;
				changeStepType(selectStepType);
			}
		});
	}
	if(type == "3") {
		var skillStep = ""
			+ "<li class='question' value='"+step.stepId+"' onclick='showSkillStepInfo(this, \"step\");'>" + step.stepName
				+ "<div title='删除步骤' class='del-btn' style='display:none;' onclick=\"delStepInfo(this, '');return false;\">-</div>"
			+ "</li>";
		$("#step_division").before(skillStep);
		$("#stepType").combobox({
			valueField:'value',
			textField:'label',
			data:[
				{label:'听打录入题',value:'5'},
				{label:'数据分析题',value:'11'},
				{label:'话术编写题',value:'12'},
				{label:'连线画图题',value:'14'},
				{label:'语音提示题',value:'8'}
			],
			required: true,
			onSelect: function(record) {
				var selectStepType = record.value;
				if(selectStepType != undefined) {
					changeStepType(selectStepType);
				}
			}
		});
	}
	if(type == "4") {
		var campingStep = ""
			+ "<li class='question' value='"+step.stepId+"' onclick='showCampingStepInfo(this, \"step\");'>" + step.stepName
				+ "<div title='删除步骤' class='del-btn' style='display:none;' onclick=\"delStepInfo(this, '');return false;\">-</div>"
			+ "</li>";
		$("#step_division").before(campingStep);
		$("#stepType").combobox({
			valueField:'value',
			textField:'label',
			data:[
				{label:'实训营地',value:'13'}
			],
			required: true,
			onSelect: function(record) {
				var selectStepType = record.value;
				changeStepType(selectStepType);
			}
		});
	}
}

function appendSingleSelectStep() {
	var singleSelect = "" +
		"<tr name='choiceRoot'>" +
			"<td class='label'>题干:</td>" +
			"<td>" +
				"<textarea id='prompt' name='prompt' style='width:70%;height:50px;'></textarea>" +
				"<p id='audioContainer'>" +
					"<audio controls='controls'><source src='' type='audio/mpeg'/></audio><br>" +
					"<input onclick='startRecording(this)' type='button' value='录音' />" +
					"<input onclick='stopRecording(this)' type='button' value='停止' />" +
					"<input onclick='playRecording(this)' type='button' value='播放' />" +
					"<input onclick='uploadAudio(this)' type='button' value='提交' />" +
					"<input onclick='handleUp(this)' type='button'  value='导入' />" +
				"</p>" +
				"<input type='hidden' name='promptVoice' />" +
			"</td>" +
		"</tr>" +
		"<tr name='choice'>" +
			"<td class='label'>A：</td>" +
			"<td>" +
				"<input id='choice_A_' name='choice_A_' autocomplete='off' type='text' value='' />" +
				"<button onclick='addChoiceVoice(this);' >添加</button>" +
				"<button onclick='showP(this);'>展开</button>" +
				"<br>" +
				"<p style='display:none'>" +
					"<audio controls='controls'><source src='' type='audio/mpeg'/></audio><br>" +
					"<input onclick='startRecording(this)' type='button' value='录音' />" +
					"<input onclick='stopRecording(this)' type='button' value='停止' />" +
					"<input onclick='playRecording(this)' type='button' value='播放' />" +
					"<input onclick='uploadAudio(this)' type='button' value='提交' />" +
					"<input onclick='handleUp(this)' type='button'  value='导入' />" +
				"</p>" +
				"<input id='choice_A_voice' type='hidden' name='choice_A_voice' value='' />" +
			"</td>" +
		"</tr>" +
		"<tr name='choice'>" +
			"<td class='label'>B：</td>" +
			"<td>" +
				"<input id='choice_B_' name='choice_B_' autocomplete='off' type='text' value='' />" +
				"<button onclick='addChoiceVoice(this);' >添加</button>" +
				"<button onclick='showP(this);'>展开</button><br>" +
				"<p style='display:none'>" +
					"<audio controls='controls'><source src='' type='audio/mpeg'/></audio><br>" +
					"<input onclick='startRecording(this)' type='button' value='录音' />" +
					"<input onclick='stopRecording(this)' type='button' value='停止' />" + 
					"<input onclick='playRecording(this)' type='button' value='播放' />" +
					"<input onclick='uploadAudio(this)' type='button' value='提交' />" +
					"<input onclick='handleUp(this)' type='button'  value='导入' />" +
				"</p>" +
				"<input id='choice_B_voice' type='hidden' name='choice_B_voice' value='' />" +
			"</td>" +
		"</tr>" +
		"<tr>" +
			"<td class='label'>正确答案：</td>" +
			"<td name='choiceAnswer'>" +
				"<label>A:<input type='radio' value='A' name='standardAnswer' /></label>" +
				"<label>B:<input type='radio' value='B' name='standardAnswer' /></label>" +
			"</td>" +
		"</tr>" +
		"<tr>" +
			"<td class='label'>得分：</td>" +
			"<td><input id='score' name='score' type='number' class='easyui-textbox' style='width:70%;height:22px;'/></td>" +
		"</tr>" +
		"";
	$("#step_form_division").after(singleSelect);
}

function appendMultiSelectStep() {
	var multiSelect = "" +
		"<tr name='choiceRoot'>" +
			"<td class='label'>题干：</td>" +
			"<td>" +
				"<textarea id='prompt' name='prompt' style='width:70%;height:50px;'></textarea>" +
				"<p id='audioContainer'>" +
					"<audio controls='controls'><source src='' type='audio/mpeg'/></audio><br>" +
					"<input onclick='startRecording(this)' type='button' value='录音' />" +
					"<input onclick='stopRecording(this)' type='button' value='停止' />" +
					"<input onclick='playRecording(this)' type='button' value='播放' />" +
					"<input onclick='uploadAudio(this)' type='button' value='提交' />" +
					"<input onclick='handleUp(this)' type='button'  value='导入' />" +
				"</p>" +
				"<input type='hidden' name='promptVoice' />" +
			"</td>" +
		"</tr>" +
		"<tr name='choice'>" +
			"<td class='label'>A：</td>" +
			"<td >" +
				"<input id='choice_A_' name='choice_A_' autocomplete='off' type='text' value='' />" +
				"<button onclick='addChoiceVoices(this);' >添加</button>" +
				"<button onclick='showP(this);'>展开</button>" +
				"<br>" +
				"<p style='display:none'>" +
					"<audio controls='controls'><source src='' type='audio/mpeg'/></audio><br>" +
					"<input onclick='startRecording(this)' type='button' value='录音' /> " +
					"<input onclick='stopRecording(this)' type='button' value='停止' /> " +
					"<input onclick='playRecording(this)' type='button' value='播放' /> " +
					"<input onclick='uploadAudio(this)' type='button' value='提交' /> " +
					"<input onclick='handleUp(this)' type='button'  value='导入' /> " +
				"</p>" +
				"<input type='hidden' id='choice_A_voice' name='choice_A_voice' value='' />" +
			"</td>" +
		"</tr>" +
		"<tr name='choice'>" +
			"<td class='label'>B：</td>" +
			"<td >" +
				"<input id='choice_B_' name='choice_B_' autocomplete='off' type='text' value='' />" +
				"<button onclick='addChoiceVoices(this);' >添加</button>" +
				"<button onclick='showP(this);'>展开</button><br>" +
				"<p style='display:none'>" +
					"<audio controls='controls'><source src='' type='audio/mpeg'/></audio><br>" +
					"<input onclick='startRecording(this)' type='button' value='录音' /> " +
					"<input onclick='stopRecording(this)' type='button' value='停止' /> " +
					"<input onclick='playRecording(this)' type='button' value='播放' /> " +
					"<input onclick='uploadAudio(this)' type='button' value='提交' /> " +
					"<input onclick='handleUp(this)' type='button'  value='导入' /> " +
				"</p>" +
				"<input type='hidden' id='choice_B_voice' name='choice_B_voice' value='' />" +
			"</td>" +
		"</tr>" +
		"<tr>" +
			"<td class='label'>正确答案：</td>" +
			"<td name='choiceAnswer'>" +
				"<label>A:<input type='checkBox' value='A' name='standardAnswer_A_' /></label>" +
				"<label>B:<input type='checkBox' value='B' name='standardAnswer_B_' /></label>" +
			"</td>" +
		"</tr>" +
		"<tr>" +
			"<td class='label'>得分：</td>" +
			"<td><input id='score' name='score' type='number' class='easyui-textbox' style='width:70%;height:22px;'/></td>" +
		"</tr>";
	$("#step_form_division").after(multiSelect);
}

function appendJudgeStep() {
	var judge = "" +
		"<tr>" +
			"<td class='label'>问题：</td>" +
			"<td>" +
				"<textarea id='prompt' name='prompt' style='width:70%;height:50px;'></textarea>" +
				"<p id='audioContainer'>" +
					"<audio controls><source src='' type='audio/mpeg'/></audio><br>" +
					"<input onclick='startRecording(this)' type='button' value='录音' />" +
					"<input onclick='stopRecording(this)' type='button' value='停止' />" +
					"<input onclick='playRecording(this)' type='button' value='播放' />" +
					"<input onclick='uploadAudio(this)' type='button' value='提交' />" +
					"<input onclick='handleUp(this)' type='button'  value='导入' />" +
				"</p>" +
				"<input type='hidden' id='promptVoice' name='promptVoice' />" +
			"</td>" +
		"</tr>" +
		"<tr>" +
			"<td class='label'>对：</td>" +
			"<td><input type='radio' name='standardAnswer' value='true' /></td>" +
		"</tr>" +
		"<tr>" +
			"<td class='label'>错：</td>" +
			"<td><input type='radio' name='standardAnswer' value='false' /></td>" +
		"</tr>" +
		"<tr>" +
			"<td class='label'>得分：</td>" +
			"<td><input id='score' name='score' type='number' style='width:70%;height:22px;'/></td>" +
		"</tr>";
	$("#step_form_division").after(judge);
}

/** 填空 */
function appendClozedStep() {
	$("#stepNameTd").attr("colspan", "2");
	$("#stepTypeTd").attr("colspan", "2");
	var clozed = "" 
		+ "<tr name='voicTr'>" +
			"<td class='label'>问题：</td>" +
			"<td colspan='2'>" +
				"<textarea id='prompt' name='prompt' style='width:69%;height:50px;'></textarea>" +
				"<input type='hidden' id='promptVoice' name='promptVoice' value='' />" +
			"</td>" +
		+ "</tr>" 
		+ "<tr>"
			+ "<td class='label w'>每空得分:</td>"
			+ "<td colspan='2'><input id='everyScore' name='everyScore' type='number' class='easyui-textbox' value='' style='width:70%;' /></td>"
		+ "</tr>"
		+ "<tr>" 
			+ "<td></td>" 
			+ "<td colspan='2' style='height:25px;text-align:center;'>正确答案</td>" +
			// "<td>分数</td>" +
		"</tr>" +
		"<tr name='fillTr'>" +
			"<td></td>" +
			"<td colspan='2'><input name='standardAnswer_A_' type='text' autocomplete='off' style='width:70%;'/>" 
				+ "&nbsp;<button onclick='addFilld(this);'>添加</button>" 
			+ "</td>" +
			// "<td><input name='score_A_' type='number' readonly='readonly' style='width:60px;height:22px;background:#eeeeee;'/>" +
			// 	"<button onclick='addFilld(this);'>添加</button>" +
			// "</td>" +
		"</tr>";
	$("#step_form_division").after(clozed);
}

/** 语音提示 */
function appendVoiceStep() {
	var voice = "" +
		"<tr>" +
			"<td class='label'>问题：</td>" +
			"<td>" +
				"<textarea name='prompt' style='width:70%;height:50px;'></textarea>" +
				"<p id='audioContainer'>" +
					"<audio controls><source src='' type='audio/mpeg'/></audio><br>" +
					"<input onclick='startRecording(this)' type='button' value='录音' />" +
					"<input onclick='stopRecording(this)' type='button' value='停止' />" +
					"<input onclick='playRecording(this)' type='button' value='播放' />" +
					"<input onclick='uploadAudio(this)' type='button' value='提交' />" +
					"<input onclick='handleUp(this)' type='button'  value='导入' />" +
				"</p>" +
				"<input type='hidden' id='promptVoice' name='promptVoice' />" +
			"</td>" +
		"</tr>" +
		"<tr>" +
			"<td class='label'>正确答案：</td>" +
			"<td><textarea id='standardAnswer' name='standardAnswer' style='width:70%;height:100px;'></textarea></td>" +
		"</tr>" +
		"<tr>" +
			"<td class='label'>得分：</td>" +
			"<td><input id='score' name='score' type='number' style='width:70%;height:22px;'/></td>" +
		"</tr>";
	$("#step_form_division").after(voice);
}

/** 应答陈述题 */
function appendStateStep() {
	var state = "" +
		"<tr>" +
			"<td class='label'>问题：</td>" +
			"<td>" +
				"<textarea id='prompt' name='prompt' style='width:70%;height:50px;'></textarea>" +
				"<p id='audioContainer'>" +
					"<audio controls><source src='' type='audio/mpeg'/></audio><br>" +
					"<input onclick='startRecording(this)' type='button' value='录音' />" +
					"<input onclick='stopRecording(this)' type='button' value='停止' />" +
					"<input onclick='playRecording(this)' type='button' value='播放' />" +
					"<input onclick='uploadAudio(this)' type='button' value='提交'/>" +
					"<input onclick='handleUp(this)' type='button'  value='导入' />" +
				"</p>" +
				"<input type='hidden' id='promptVoice' name='promptVoice' />" +
			"</td>" +
		"</tr>" +
		"<tr>" +
			"<td class='label'>正确答案：</td>" +
			"<td><textarea id='standardAnswer' name='standardAnswer' style='width:70%;height:100px;'></textarea></td>" +
		"</tr>" +
		"<tr>" +
			"<td class='label'>得分：</td>" +
			"<td><input id='score' name='score' type='number' style='width:70%;height:22px;'/></td>" +
		"</tr>";
	$("#step_form_division").after(state);
}

/** 复读陈述题 */
function appendRepeatStep() {
	var repeat = "" + 
		"<tr>" +
			"<td class='label'>问题：</td>" +
			"<td>" +
				"<textarea id='prompt' name='prompt' style='width:70%;height:50px;'></textarea>" +
				"<p id='audioContainer'>" +
					"<audio controls><source src='' type='audio/mpeg'/></audio><br>" +
					"<input onclick='startRecording(this)' type='button' value='录音' />" +
					"<input onclick='stopRecording(this)' type='button' value='停止' />" +
					"<input onclick='playRecording(this)' type='button' value='播放' />" +
					"<input onclick='uploadAudio(this)' type='button' value='提交' />" +
					"<input onclick='handleUp(this)' type='button'  value='导入' />" +
				"</p>" +
				"<input type='hidden' id='promptVoice' name='promptVoice' />" +
			"</td>" +
		"</tr>" +
		"<tr>" +
			"<td class='label'>正确答案：</td>" +
			"<td><textarea id='standardAnswer' name='standardAnswer' style='width:70%;height:100px;'></textarea></td>" +
		"</tr>" +
		"<tr>" + 
			"<td class='label'>得分：</td>" +
			"<td><input id='score' name='score' type='number' style='width:70%;height:22px;'/></td>" +
		"</tr>";
	$("#step_form_division").after(repeat);
}

/** 问答 */
function appendAnswerStep() {
	var answer = "" +
		"<tr>" +
			"<td class='label'>问题：</td>" +
			"<td>" +
				"<textarea id='prompt' name='prompt' style='width:70%;height:50px;'></textarea>" +
				"<p id='audioContainer'>" +
					"<audio controls><source src='' type='audio/mpeg'/></audio><br>" +
					"<input onclick='startRecording(this)' type='button' value='录音' />" +
					"<input onclick='stopRecording(this)' type='button' value='停止' />" +
					"<input onclick='playRecording(this)' type='button' value='播放' />" +
					"<input onclick='uploadAudio(this)' type='button' value='提交' />" +
					"<input onclick='handleUp(this)' type='button'  value='导入' />" +
				"</p>" +
				"<input type='hidden' id='promptVoice' name='promptVoice' value='' />" +
			"</td>" +
		"</tr>" +
		"<tr>" +
			"<td class='label'>正确答案：</td>" +
			"<td><textarea id='standardAnswer' name='standardAnswer' style='width:70%;height:100px;'></textarea></td>" +
		"</tr>" +
		"<tr>" +
			"<td class='label'>得分：</td>" +
			"<td><input id='score' name='score' type='number' style='width:70%;height:22px;'/></td>" +
		// "</tr>" +
		// "<tr>" +
		// 	"<td class='label'>实操题：</td>" +
		// 	"<td>" +
		// 		"<input id='importPraFile' onclick='handleZipUp(this)' type='button'  value='导入' />" +
		// 		"<input type='hidden' id='origiName' name='origiName' value='' />" +
		// 		"<input type='hidden' id='fileName' name='fileName' value='' />" +
		// 	"</td>" +
		"</tr>";
	$("#step_form_division").after(answer);
}

function computClozeScore(answer) {
	var standardAnswerValue = $(answer).val();
	$(answer).parent().next("td").find("input").val(standardAnswerValue.trim().length);
}

function appendClozeStep() {
	$("#stepNameTd").attr("colspan", "2");
	$("#stepTypeTd").attr("colspan", "2");
	var cloze = "" +
		"<tr name='voicTr'>" +
			"<td colspan='3'>" +
				"<p id='audioContainer'>" +
					"<audio controls><source src='' type='audio/mpeg'/></audio><br>" +
					"<input onclick='startRecording(this)' type='button' value='录音' />" +
					"<input onclick='stopRecording(this)' type='button' value='停止' />" +
					"<input onclick='playRecording(this)' type='button' value='播放' />" +
					"<input onclick='uploadAudio(this)' type='button' value='提交' />" +
					"<input onclick='handleUp(this)' type='button'  value='导入' />" +
				"</p>" +
				"<input type='hidden' name='promptVoice' />" +
			"</td>" +
		"</tr>" +
		"<tr>" +
			"<td>问题</td>" +
			"<td>正确答案</td>" +
			"<td>得分</td>" +
		"</tr>" +
		"<tr name='fillTr'>" +
			"<td><input name='prompt_A_' type='text' autocomplete='off' style='width:120px;height:22px;'/></td>" +
			"<td><input name='standardAnswer_A_' type='text' onKeyup='computClozeScore(this)' autocomplete='off' style='width:100px;height:22px;'/></td>" +
			"<td><input name='score_A_' type='number' readonly='readonly' style='width:60px;height:22px;margin-right:3px;background-color:#eeeeee;'/>" +
				"<button onclick='addFill(this);'>添加</button>" +
			"</td>" +
		"</tr>";

	$("#step_form_division").after(cloze);
}

function appendAnalyzeStep() {
	var analyze = "" +
		"<tr>" +
			"<td class='label'>问题：</td>" +
			"<td>" +
				"<textarea id='prompt' name='prompt' style='width:70%;height:50px;'></textarea>" +
				"<p id='audioContainer'>" +
					"<audio controls><source src='' type='audio/mpeg'/></audio><br>" +
					"<input onclick='startRecording(this)' type='button' value='录音' />" +
					"<input onclick='stopRecording(this)' type='button' value='停止' />" +
					"<input onclick='playRecording(this)' type='button' value='播放' />" +
					"<input onclick='uploadAudio(this)' type='button' value='提交' />" +
					"<input onclick='handleUp(this)' type='button'  value='导入' />" +
				"</p>" +
				"<input type='hidden' id='promptVoice' name='promptVoice' value='' />" +
			"</td>" +
		"</tr>" +
		"<tr>" +
			"<td class='label'>正确答案：</td>" +
			"<td><textarea id='standardAnswer' name='standardAnswer' style='width:70%;height:100px;'></textarea></td>" +
		"</tr>" +
		"<tr>" +
			"<td class='label'>得分：</td>" +
			"<td><input id='score' name='score' type='number' style='width:70%;height:22px;'/></td>" +
		"</tr>" +
		"<tr>" +
			"<td class='label'>实操题：</td>" +
			"<td>" +
				"<input id='importPraFile' onclick='handleZipUp(this)' type='button'  value='导入' />" +
				"<input type='hidden' id='origiName' name='origiName' value='' />" +
				"<input type='hidden' id='fileName' name='fileName' value='' />" +
			"</td>" +
		"</tr>";
	$("#step_form_division").after(analyze);
}

function appendVerbalStep() {
	var verbal = "" +
		"<tr>" +
			"<td class='label'>问题：</td>" +
			"<td>" +
				"<textarea id='prompt' name='prompt' style='width:70%;height:50px;'></textarea>" +
				"<p id='audioContainer'>" +
					"<audio controls><source src='' type='audio/mpeg'/></audio><br>" +
					"<input onclick='startRecording(this)' type='button' value='录音' />" +
					"<input onclick='stopRecording(this)' type='button' value='停止' />" +
					"<input onclick='playRecording(this)' type='button' value='播放' />" +
					"<input onclick='uploadAudio(this)' type='button' value='提交' />" +
					"<input onclick='handleUp(this)' type='button'  value='导入' />" +
				"</p>" +
				"<input type='hidden' id='promptVoice' name='promptVoice' value='' />" +
			"</td>" +
		"</tr>" +
		"<tr>" +
			"<td class='label'>正确答案：</td>" +
			"<td><textarea id='standardAnswer' name='standardAnswer' style='width:70%;height:100px;'></textarea></td>" +
		"</tr>" +
		"<tr>" +
			"<td class='label'>得分：</td>" +
			"<td><input id='score' name='score' type='number' style='width:70%;height:22px;'/></td>" +
		"</tr>" +
		"<tr>" +
			"<td class='label'>实操题：</td>" +
			"<td>" +
				"<input id='importPraFile' onclick='handleZipUp(this)' type='button'  value='导入' />" +
				"<input type='hidden' id='origiName' name='origiName' value='' />" +
				"<input type='hidden' id='fileName' name='fileName' value='' />" +
			"</td>" +
		"</tr>";
	$("#step_form_division").after(verbal);
}

function appendDrawStep() {
	var draw = "" +
		"<tr>" +
			"<td class='label'>问题：</td>" +
			"<td>" +
				"<textarea id='prompt' name='prompt' style='width:70%;height:50px;'></textarea>" +
				"<p id='audioContainer'>" +
					"<audio controls><source src='' type='audio/mpeg'/></audio><br>" +
					"<input onclick='startRecording(this)' type='button' value='录音' />" +
					"<input onclick='stopRecording(this)' type='button' value='停止' />" +
					"<input onclick='playRecording(this)' type='button' value='播放' />" +
					"<input onclick='uploadAudio(this)' type='button' value='提交' />" +
					"<input onclick='handleUp(this)' type='button'  value='导入' />" +
				"</p>" +
				"<input type='hidden' id='promptVoice' name='promptVoice' value='' />" +
			"</td>" +
		"</tr>" +
		"<tr>" +
			"<td class='label'>正确答案：</td>" +
			"<td><textarea id='standardAnswer' name='standardAnswer' style='width:70%;height:100px;'></textarea></td>" +
		"</tr>" +
		"<tr>" +
			"<td class='label'>得分：</td>" +
			"<td><input id='score' name='score' type='number' style='width:70%;height:22px;'/></td>" +
		"</tr>" +
		"<tr>" +
			"<td class='label'>实操题：</td>" +
			"<td>" +
				"<input id='importPraFile' onclick='handleZipUp(this)' type='button'  value='导入' />" +
				"<input type='hidden' id='origiName' name='origiName' value='' />" +
				"<input type='hidden' id='fileName' name='fileName' value='' />" +
			"</td>" +
		"</tr>";
	$("#step_form_division").after(draw);
}

function appendCampingStep() {
	$("#sx").show();
}

function changeStepType(stepType) {
	/** 单选 */
	$("#step_form_division").nextAll().remove();	
	$("#sx").hide();
	if(stepType == "1")	{
		appendSingleSelectStep();
	}
	/** 多选 */
	if(stepType == "2") {
		appendMultiSelectStep();
	}
	/** 判断 */
	if(stepType == "4") {
		appendJudgeStep();
	}
	/** 填空 */
	if(stepType == "9") {
		appendClozedStep();
	}
	/** 问答 */
	if(stepType == "3") {
		appendAnswerStep();
	}
	/** 复读陈述题 */
	if(stepType == "6") {
		appendRepeatStep();
	}
	/** 应答陈述题 */
	if(stepType == "7") {
		appendStateStep();
	}
	/** 语音提示 */
	if(stepType == "8") {
		appendVoiceStep();
	}
	/** 听打录入 */
	if(stepType == "5") {
		appendClozeStep();
	}
	/** 数据分析 */
	if(stepType == "11") {
		appendAnalyzeStep();
	}
	/** 话术编写 */
	if(stepType == "12") {
		appendVerbalStep();
	}
	/** 实训营地 */
	if(stepType == "13") {
		appendCampingStep();
	}
	/** 连线画图题 */
	if(stepType == "14") {
		appendDrawStep();
	}
}

function addTheoryStep(index, type) {
	var step_lis = $("#sortable").children("li");
	var stepSize = step_lis.length;
	if(stepSize >= 5) {
		$.messager.alert("温馨提示", "理论题只允许有一个步骤");
		return;
	}
	$("#stepType").combobox({
		valueField:'value',
		textField:'label',
		data:[
				{label:'单选题',value:'1'},
				{label:'多选题',value:'2'},
				{label:'判断题',value:'4'},
				{label:'填空题',value:'9'},
				{label:'问答题',value:'3'}
		],
		required: true,
		onSelect: function(record) {
			var selectStepType = record.value;
			changeStepType(selectStepType);
		}
	});
	addStepHead(index);
	addStepBody(index, type);
}

function addPracticalStep(index, type) {
	var step_lis = $("#sortable").children("li");
	var stepSize = step_lis.length;
	$("#stepType").combobox({
		valueField:'value',
		textField:'label',
		data:[
				{label:'单选题',value:'1'},
				{label:'多选题',value:'2'},
				{label:'判断题',value:'4'},
				{label:'填空题',value:'9'},
				{label:'问答题',value:'3'},
				{label:'复读陈述题',value:'6'},
				{label:'应答陈述题',value:'7'},
				{label:'语音提示',value:'8'}
		],
		required: true,
		onSelect: function(record) {
			var selectStepType = record.value;
			changeStepType(selectStepType);
		}
	});
	addStepHead(index);
	addStepBody(index, type);
}

function addSkillStep(index, type) {
	var step_lis = $("#sortable").children("li");
	var stepSize = step_lis.length;
	$("#stepType").combobox({
		valueField:'value',
		textField:'label',
		data:[
				{label:'听打录入题',value:'5'},
				{label:'数据分析题',value:'11'},
				{label:'话术编写题',value:'12'},
				{label:'连线画图题',value:'14'},
				{label:'语音提示题',value:'8'}
			],
		required: true,
		onSelect: function(record) {
			var selectStepType = record.value;
			changeStepType(selectStepType);
		}
	});
	addStepHead(index);
	addStepBody(index, type);
}

function addCampingStep(index, type) {
	var step_lis = $("#sortable").children("li");
	var stepSize = step_lis.length;
	if(stepSize >= 5) {
		$.messager.alert("温馨提示", "实训营地只允许有一个步骤");
		return;
	}
	$("#stepType").combobox({
		valueField:'value',
		textField:'label',
		data:[
				{label:'实训营地',value:'13'}
		],
		required: true,
		onSelect: function(record) {
			var selectStepType = record.value;
			changeStepType(selectStepType);
		}
	});
	addStepHead(index);
	addStepBody(index, type);
}

function showP(t){
	var txt = $(t).text();
	var newTxt = "";
	if(txt == '展开'){
		newTxt = "关闭"
	}else{
		newTxt = "展开"
	}
	$(t).text(newTxt);
	$(t).next("br").next('p').toggle(300);
}

/** 上传录音 */
var recorder = new Array();
var recIdx = 0;

function startRecording(t) {
	var audio = $(t).parent('p').find('audio').eq(0);
	var idx=audio.attr("idx");
	if(idx==""||idx==undefined){
		audio.attr("idx",recIdx);
		idx=recIdx;
		recIdx++;
	}
	HZRecorder.get(function(rec) {
		recorder[idx] = {'rec':rec};
		rec.start();
	});
}

function stopRecording(t) {
	var audio = $(t).parent('p').find('audio').eq(0);
	var idx=audio.attr("idx");
	if(recorder[idx] != null && typeof(recorder[idx]) != "undefined" 
		&& recorder[idx].rec != null && typeof(recorder[idx].rec) != "undefined"){
		audio.get(0).pause();
		recorder[idx].rec.stop();
	}else{
		audio.get(0).pause();
	}
}

function playRecording(t) {
	var audio = $(t).parent('p').find('audio').eq(0);
	var idx=audio.attr("idx");
	if(recorder[idx] != null && typeof(recorder[idx]) != "undefined" 
			&& recorder[idx].rec != null && typeof(recorder[idx].rec) != "undefined"){
		recorder[idx].rec.play(audio.get(0));
		audio.get(0).play();
	}else{
		audio.get(0).pause();
		playVedio(audio);
	}
}

function uploadAudio(t) {
	var audio = $(t).parent('p').find('audio').eq(0);
	var idx=audio.attr("idx");
	var btn = $(t);
	var bankId = $("#bankId").val();
	var topicId = $("#topicId").val();
	recorder[idx].rec.upload("/"+projectName+"/paper/question/uploadAudio.action?bankId="+bankId+"&topicId="+topicId, function(state, resultStr) {
		switch (state) {
		case 'uploading':
			break;
		case 'ok':
			var result = JSON.parse(resultStr);
			if(result.success){
				btn.parent("p").next("input").val(result.fullName);
				btn.parent("p").find('audio').eq(0).get(0).src = result.fileUrl;
				$.messager.alert("温馨提示","上传成功");
			}else{
				
			}
			break;
		case 'error':
			$.messager.alert("温馨提示","上传失败");
			break;
		case 'cancel':
			$.messager.alert("温馨提示","上传被取消");
			break;
		}
	});
}

function playVedio(elem) {
	//var audio = $(elem).next("audio");
	var audio = elem;
	audio = audio.get(0);
	var audioState = audio.paused;
	if(audioState) {
		audio.play();
	}else{
		audio.pause();
	}
}
//**********************voice end*******************
	
function handleUp(t){
	var btn = $(t);
	var bankId = $("#bankId").val();
	var topicId = $("#topicId").val();
	$("#handleUpForm").find("input[name='file']").unbind();
	$("#handleUpForm").find("input[name='file']").change(function(){
		var fil = $("#handleUpForm").find("input[name='file']").val();
		if(fil != ""){
			$("#handleUpForm").find("input[name='bankId']").val(bankId);
			$("#handleUpForm").find("input[name='topicId']").val(topicId);
			$("#handleUpForm").ajaxSubmit({
				url: '/'+projectName+'/paper/question/handleUploadVoice.action', 
				type: "post", 
				dataType: "json", 
				success: function (result) {
					if(result.success){
						btn.parent("p").next("input").val(result.fullName);
						btn.parent("p").find('audio').eq(0).get(0).src = result.fileUrl;
					}else{
					}
					$("#handleUpForm").find("input[name='file']").unbind();
					$("#handleUpForm").find("input[name='file']").val("");
				},
			});
		}
	});
	$("#handleUpForm").find("input[name='file']").trigger('click');
}

// 上传实操题压缩包
function handleZipUp(t){
	var btn = $(t);
	var bankId = $("#bankId").val();
	var topicId = $("#topicId").val();
	$("#handleZipUpForm").find("input[name='zipFile']").unbind();
	$("#handleZipUpForm").find("input[name='zipFile']").change(function(){
		var fil = $("#handleZipUpForm").find("input[name='zipFile']").val();
		if(fil != ""){
			$("#handleZipUpForm").find("input[name='bankId']").val(bankId);
			$("#handleZipUpForm").find("input[name='topicId']").val(topicId);
			$("#handleZipUpForm").ajaxSubmit({
				url: '/'+projectName+'/paper/question/handleUploadZip.action', 
				type: "post", 
				dataType: "json", 
				success: function (result) {
					if(result.success){
						btn.parent("td").find("input[name='origiName']").val(result.origiName);
						btn.parent("td").find("input[name='fileName']").val(result.fullName);
						btn.parent("td").find("a[name='downloadZipA']").remove();
						btn.parent("td").append("<a name='downloadZipA' href='javascript:downloadZip(\""+result.fullName+"\",\""+result.origiName+"\")'>"+result.origiName +"</a>");
					}
					$("#handleZipUpForm").find("input[name='file']").unbind();
					$("#handleZipUpForm").find("input[name='file']").val("");
				},
			});
		}
	});
	$("#handleZipUpForm").find("input[name='zipFile']").trigger('click');
}

// 下载zip
function downloadZip(fileName,origiName){
	var bankId = $("#stepBankId").val();
	var topicId = $("#stepTopicId").val();
	var param = "fileName="+fileName+"&origiName="+origiName+"&bankId="+bankId+"&topicId="+topicId;
	window.location.href="/"+projectName+"/paper/question/downloadZip.action?"+param;
}

// 添加选项（单选）
function addChoiceVoice(t){
	doAddCho(t,"voice");
}

// 多选
function addChoiceVoices(t){
	doAddChos(t,"voice");
}

// 添加选项（非语音）
function addChoice(t){
	doAddCho(t,"text");
}
// 添加操作(单选)
// function doAddCho(t,type){
function doAddCho(){
	var len = $("#stepTable").find("tr[name='choice']").length;
	var letter = sort.charAt(len);
	var abtn = "";
	var ch = "<tr name='choice'>"
			+	"<td class='label'>"+letter+"：</td>"
			+	"<td name='td"+letter+"'>"
			+		"<input id='choice_"+letter+"_' name='choice_"+letter+"_' autocomplete='off' type='text' value='' />"
			+		" <button onclick='addChoiceVoice(this);' >添加  </button>"
			+		" <button onclick=\"delChoice(this);\" >删除 </button>"
			+		" <button onclick='showP(this);';>展开</button><br>"
			+		"<p style='display:none'>"
			+			"<audio controls><source src='' type='audio/mpeg'/></audio><br>"
			+			"<input onclick='startRecording(this)' type='button' value='录音' /> "
			+			"<input onclick='stopRecording(this)' type='button' value='停止' /> "
			+			"<input onclick='playRecording(this)' type='button' value='播放' /> "
			+			"<input onclick='uploadAudio(this)' type='button' value='提交' /> "
			+			"<input onclick='handleUp(this)' type='button'  value='导入' /> "
			+		"</p>"
			+		"<input type='hidden' id='choice_"+letter+"_voice' name='choice_"+letter+"_voice' value='' />"
			+	"</td>"
			+"</tr><br>";
	$("#stepTable").find("tr[name='choice']:last").after(ch);
	var an = "";
	if(len != 0 && len%5 == 0){
		an = "<br> <label><span>"+letter+":</span><input type='radio' value='"+letter+"' name='standardAnswer' /></label>";
	}else{
		an = " <label><span>"+letter+":</span><input type='radio' value='"+letter+"' name='standardAnswer' /></label>";
	}
	$("#stepTable").find("td[name='choiceAnswer']").append(an);
}

//多选
// function doAddChos(t,type){
function doAddChos(){
	var len = $("#stepTable").find("tr[name='choice']").length;
	var letter = sort.charAt(len);
	var abtn = "";
	var ch = "<tr name='choice'>"
			+	"<td class='label'>"+letter+"：</td>"
			+	"<td name='td"+letter+"'>"
			+		"<input id='choice_"+letter+"_' name='choice_"+letter+"_' autocomplete='off' type='text' value='' />"
			+		" <button onclick='addChoiceVoices(this);' >添加  </button>"
			+		" <button onclick=\"delChoice(this);\" >删除 </button>"
			+		" <button onclick='showP(this);';>展开</button><br>"
			+		"<p style='display:none'>"
			+			"<audio controls><source src='' type='audio/mpeg'/></audio><br>"
			+			"<input onclick='startRecording(this)' type='button' value='录音' /> "
			+			"<input onclick='stopRecording(this)' type='button' value='停止' /> "
			+			"<input onclick='playRecording(this)' type='button' value='播放' /> "
			+			"<input onclick='uploadAudio(this)' type='button' value='提交' /> "
			+			"<input onclick='handleUp(this)' type='button'  value='导入' /> "
			+		"</p>"
			+		"<input type='hidden' id='choice_"+letter+"_voice' name='choice_"+letter+"_voice' value='' />"
			+	"</td>"
			+"</tr><br>";
	$("#stepTable").find("tr[name='choice']:last").after(ch);
	var an = "";
	if(len != 0 && len%5 == 0){
		an = "<br> <label><span>"+letter+":</span><input type='checkBox' value='"+letter+"' name='standardAnswer_"+letter+"_' /></label>";
	}else{
		an = " <label><span>"+letter+":</span><input type='checkBox' value='"+letter+"' name='standardAnswer_"+letter+"_' /></label>";
	}
	$("#stepTable").find("td[name='choiceAnswer']").append(an);
}

//添加选项
function delChoice(t){
	var letter = $(t).parent('td').attr("name");
	letter = letter.substring(2,3);
	$(t).parent('td').parent('tr').remove();
	var trArg = $("#stepTable").find("tr[name='choice']");
	for( var i = 0; i < trArg.length; i++ ){
		$(trArg[i]).find("td").eq(0).html( sort.charAt(i)+"：" );
		$(trArg[i]).find("td").eq(1).find('input').attr("name","choice"+sort.charAt(i));
		$(trArg[i]).find("td").eq(1).find("a[name='delBtn']").attr("href","javascript:delChoice(this);");
		$(trArg[i]).find("td").eq(1).attr("name","td"+sort.charAt(i));
	}
	$("#stepTable").find("td[name='choiceAnswer']").find('br').remove();
	$("#stepTable").find("td[name='choiceAnswer']").find('label').eq(sort.indexOf(letter)).remove();
	$("#stepTable").find("td[name='choiceAnswer']").find('label').each(function(i,item){
		$(this).find('span').html(sort.charAt(i)+":");
		$(this).find('input').val(sort.charAt(i));
		if(i != 0 && (i+1)%5 == 0){
			$(this).after("<br>");
		}
	});
}

// 添加问答项
function addFill(t){
	var len = $("#stepTable").find("tr[name='fillTr']").length;
	var letter = sort.charAt(len);
	var tr = "<tr name='fillTr' str='"+len+"'>"
			+	"<td><input name='prompt_"+letter+"_' type='text' autocomplete='off' style='width:120px;height:22px;'/></td>"
			+	"<td><input name='standardAnswer_"+letter+"_' type='text' onKeyup='computClozeScore(this);' autocomplete='off' style='width:100px;height:22px;'/></td>"
			+	"<td><input name='score_"+letter+"_' type='number' readonly='readonly' style='width:60px;height:22px;background-color:#eeeeee;'/>"
			+		" <button onclick='addFill(this);'>添加</button>"
			+		" <button onclick='delFill(this);'>删除</button>"
			+	"</td>"
			+"</tr>";
	$("#stepTable").find("tr[name='fillTr']:last").after(tr);
}

// 添加填空选项
function addFilld(){
	var len = $("#stepTable").find("tr[name='fillTr']").length;
	var letter = sort.charAt(len);
	var tr = "<tr name='fillTr' str='"+len+"'>"
			+	"<td></td>"
			+	"<td colspan='2'><input name='standardAnswer_"+letter+"_' type='text' autocomplete='off' style='width:70%;'/>"
				+	"&nbsp;<button onclick='addFilld(this);'>添加</button>"
				+	"<button onclick='delFill(this);'>删除</button>"
			+   "</td>"
			// +	"<td><input name='score_"+letter+"_' type='number' style='width:60px;height:22px;'/>"
			// +		" <button onclick='addFilld(this);'>添加</button>"
			// +		" <button onclick='delFill(this);'>删除</button>"
			// +	"</td>"
			+"</tr>";
			
	$("#stepTable").find("tr[name='fillTr']:last").after(tr);
}

// 删除问答项
function delFill(t){
	$(t).parent('td').parent('tr').remove();
}

/** topic form */
function getTopicFormData() {
	var topicFormData_ = $("#topic_form").serialize();
	return topicFormData_;
}

/** step form */
function getStepFormData() {
	var stepType = $("#stepType").combobox("getValue");
	var stepType_ = stepType + "";
	if(stepType == "13") {
		var campPromptAndAnswer = getCampingPromptAndAnswer();
		var campPrompt = campPromptAndAnswer.prompt;
		var campAnswer = campPromptAndAnswer.standardAnswer;
		$("#campPrompt").val(campPrompt);
		$("#campAnswer").val(campAnswer);
	}else {
		// $("#campPrompt").val("");
		// $("#campAnswer").val("");
	}

	var stepFormData_ = $("#step_form").serialize();
	return stepFormData_;
}

/** save result */
function setSaveResult(topicId, stepId, totalScore) {
	$("#topicId").val(topicId);
	$("#topicIdLi_").val(topicId);
	$("#stepTopicId").val(topicId);
	$("#stepId").val(stepId);
	$("#sortable > li.active").val(stepId);
	$("#totalScore").val(totalScore);
}

// function doSelect(step){
// 	var sort="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
// 	var prompt = step.find("textarea[name='prompt']").val();// 题干
// 	if(prompt != null && typeof(prompt) != "undefined"){
// 		prompt = prompt.replace(/,/g , "，");
// 	}
// 	var promptVoice = step.find("input[name='promptVoice']").val();// 题干录音
// 	var title = step.find("input[name='title']").val();// 标题
// 	var choice = "";
// 	step.find("tr[name='choice']").each(function(i,item){
// 		var voice = $(this).find("input[name='voice']").val();
// 		var content = $(this).find("input[name='choice"+sort.charAt(i)+"']").val();
// 		choice = choice + "<choice id=\""+sort.charAt(i)+"\" audio=\""+voice+"\" >"+content+"</choice>"
		
// 	});
// 	var standardAnswer = "";// 答案
// 	step.find("input[name='standardAnswer']").each(function(i,item){
// 		if($(item).is(':checked')){
// 			if(standardAnswer == ""){
// 				standardAnswer = standardAnswer + sort.charAt(i);
// 			}else{
// 				standardAnswer = standardAnswer + "@@" + sort.charAt(i);
// 			}
// 		}
// 	});
// 	var score = step.find("input[name='score']").val();// 得分
// 	var choiceType = "choice_single";
// 	if(standardAnswer.length > 1){
// 		choiceType = "choice_multi";
// 	}
// 	var stepXml = "<step type=\"question\">"
// 					+	"<title>"+title+"</title>"
// 					+	"<score>"+score+"</score>"
// 					+	"<prompt audio=\""+promptVoice+"\" repeat=\"\">"+prompt+"</prompt>"
// 					+	"<answer type=\""+choiceType+"\">"
// 					+	choice
// 					+	"</answer>"
// 					+	"<standard_answer>"+standardAnswer+"</standard_answer>"
// 					+"</step>";
// 	return stepXml;
// }

var bjButton ;
//获取背景
function getRemark(){
	return $(bjButton).next('input').val();
}

function showRemark(t){
	bjButton = t;
	
	//多窗口模式，层叠置顶
	var index = layer.open({
		type: 2 //此处以iframe举例
		,title: '设置背景信息'
		,closeBtn :0
		,area: ['800px', '550px']
		,resize: true
		,maxmin: true 
		,content: '/'+projectName+'/editor/topicPremise.jsp'
		,btn: '保存背景信息'
		,btnAlign: 'c' //按钮居中
		,shade: 0.1 //不显示遮罩
		,yes: function(){
			//layer.closeAll();
			var fh = window["layui-layer-iframe" + index].saveRemark();
			$(bjButton).next('input').val(fh);
			layer.close(index);
		},cancel : function(){
			alert("取消");
		}
	});
	
	/*var index = layer.open({
		title:'背景信息'
		,type: 2
		,closeBtn :0
		,area: ['600px', '450px']
		,anim: 1
		,offset: 'auto' //具体配置参考：http://www.layui.com/doc/modules/layer.html#offset
		,id: 'layerDemo'//防止重复弹出
		,content: '/'+projectName+'/editor/topicPremise.jsp'
		,btn: '保存背景信息'
		,btnAlign: 'c' //按钮居中
		,shade: 0 //不显示遮罩
		,yes: function(){
			//layer.closeAll();
			var fh = window["layui-layer-iframe" + index].saveRemark();
			$(bjButton).next('input').val(fh);
			layer.close(index);
		},cancel : function(){
			alert("取消");
		}
	});*/
	
	
}

