<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title></title>
	<link rel="stylesheet" type="text/css" href="/${projectName}/common/base.css">
 	<link id="easyuiTheme" rel="stylesheet" type="text/css" href="/${projectName}/easyui1_5/themes/custom/uimaker/easyui.css">
	<link rel="stylesheet" type="text/css" href="/${projectName}/easyui1_5/themes/custom/uimaker/icon.css">
	<link rel="stylesheet" type="text/css" href="/${projectName}/common/com.css">
	<link rel="stylesheet" type="text/css" href="/${projectName}/editor/sortable.css">
	<script type="text/javascript" src="/${projectName}/book/js/jqueryForm/jquery-1.8.2.min.js"></script>
	<script src="jquery-ui.js"></script>
	<script type="text/javascript" src="/${projectName}/book/js/jqueryForm/jquery.form.js"></script>
	<script type="text/javascript" src="/${projectName}/easyui1_5/themes/custom/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="/${projectName}/easyui1_5/themes/custom/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="/${projectName}/dictionary/dictionary2.js"></script>
	<!-- 录音插件 -->
	<script type="text/javascript" src="/${projectName}/js/record/HZRecorder.js"></script>
	<!-- 富文本编辑器 -->
	<script type="text/javascript" charset="utf-8" src="../ueditor/ueditor.config.js"></script>
	<script type="text/javascript" charset="utf-8" src="../ueditor/ueditor.all.js"> </script>
	<script type="text/javascript" charset="utf-8" src="../ueditor/lang/zh-cn/zh-cn.js"></script>
	<script type="text/javascript" charset="utf-8" src="./topic_add.js"></script>
	
	<script src="/${projectName}/book/js/layui/js/lib/layui2.3/layui.all.js"></script>
	
	<style>
		/* span.textbox.textbox-invalid.easyui-fluid.combo {
			width: 65% !important;
		}
		span.textbox.easyui-fluid.combo {
			width: 65% !important;
		}

		input.textbox-text.validatebox-text.validatebox-invalid.textbox-prompt {
			width: 65% !important;
		}

		input.textbox-text.validatebox-text {
			width: 65% !important;
		} */
		.textbox {
			width: 70%!important;
		}
		
		.textbox-text {
			width: 70%!important;
		}

		.add-btn {
			float:right;
			width:25px;
			height:25px;
			text-align:center;
			color:white;
			font-size:24px;
			line-height:21px;
			background-color:#5bb75b;
			border-radius:5px;
			margin-right:-25px;
		}
		
		.del-btn {
			float:right;
			width:25px;
			height:25px;
			text-align:center;
			color:white;
			font-size:24px;
			line-height:21px;
			background-color:#cc4036;
			border-radius:5px;
			margin-right:-25px;
		}
		li {
			padding-right:10px;
		}
	</style>

<script>
var projectName = "${projectName}";
</script>
</head>
<body style="position:relative;">
	<%-- <div id="loading" style="display:none;width:100%;height:100%;position:absolute;background:#e3e3e3;opacity:0.8;filter:alpha(opacity=80);z-index:9999;vertical-align:middle;line-height:380px;text-align:center;">
		数据加载中......
	</div> --%>

    <div style="display:flex;width:100%;height:100%;position:position:relative;">
        <div style="padding:5px 0;overflow-y:scroll;flex:1;">
            <div style="margin-bottom:10px;margin-left:15px;height:30px;vertical-align:middle;line-height:30px;">
                <span>题目流程</span>
            </div>
		    <ul id="indexIco">
			    <li>o</li>
			    <li id="beginO">o</li>
				<li id="indexIco_division" style="display:none;"></li>
			    <li class="end1">o</li>
		    </ul>
		    <ul id="sortable">
			    <li id="topicTitle" class="question active" name="leftTitle" onclick="doShowTopic(this, 'topic');">题目名称
		    		<div title="添加步骤" class="add-btn" onclick="doAddStep();return false">+</div>
					<input type="hidden" name="topicIdLi_" id="topicIdLi_" />
				</li>
			    <li id="beginTitle" class="static">开始</li>
				<li id="step_division" class="question" style="display:none;"></li>
			    <li class="static end2">结束</li>
		    </ul>
	    </div>
        <div id="topicContainer" style="flex:1 1 0%;padding:5px 0px;">
            <div style="margin-bottom:10px;margin-left:15px;height:30px;vertical-align:middle;line-height:30px;">
                <span>题目信息</span> 
            </div>
	        <div id="topicPane">
			    <div id="topicContent">
				    <form id="topic_form">
					    <input type="hidden" name="bankId" id="bankId" />
					    <input type="hidden" name="topicId" id="topicId" />
					    <input type="hidden" name="changeFlag" id="changeFlag" value="0" />
					    <table id="topicTable" style="width:85%;">
						    <tr>
							    <td class='label w'>题目名称:</td>
							    <td><input id="topicName" name="topicName"  value="" class="easyui-textbox" style="width:70%;" data-options="required:true" /></td>
						    </tr>
							<tr>
							    <td class='label w'>类型:</td>
								<td>
									<input id="topicType" name="topicType" class="easyui-combobox" data-options="valueField:'value',textField:'label',data:[{label:'理论题',value:'1'},{label:'实训题',value:'2'},{label:'技能训练题',value:'3'},{label:'实训营地题',value:'4'}]" required="true" editable="false" />
								</td>
						    </tr>

						    <tr>
							    <td class='label w'>关键字:</td>
							    <td><input id="keyWords" name="keyWords" value="" class="easyui-textbox" style="width:70%;" /></td>
						    </tr>
							<tr id="topic_form_division" style="display:none;"></tr>
					    </table>
			        </form>
			    </div>
		    </div>
	    </div>
		<div id="stepContainer" style="flex:1 1 0%;padding:5px 0px;display:none;">
            <div style="margin-bottom:10px;margin-left:15px;height:30px;vertical-align:middle;line-height:30px;">
                <span>步骤信息</span> 
            </div>
	        <div id="stepPane">
			    <div id="stepContent">
				    <form id="step_form">
					    <input type="hidden" name='stepId' id='stepId' />
					    <input type="hidden" name='stepTopicId' id='stepTopicId' />
					    <input type="hidden" name='stepBankId' id='stepBankId' />
						<%-- <input type="hidden" name="questionType" value="selectMode"/> --%>
						<%-- <input type="hidden" name="title" /> --%>
					    <table id="stepTable" style="width:85%;">
						    <tr>
							    <td class='label w'>步骤名称:</td>
							    <td id="stepNameTd"><input id="stepName" name="stepName"  value="" class="easyui-textbox" style="width:70%;" autocomplete="off" data-options="required:true" /></td>
						    </tr>
							<tr>
							    <td class='label w'>类型:</td>
								<td id="stepTypeTd">
									<input id="stepType" name="stepType" class="easyui-combobox" autocomplete="off" required="true" editable="false" />
								</td>
						    </tr>

							<tr id="step_form_division" style="display:none;"></tr>
					    </table>
						<div id="sx" style="width:85%;display:none">
							<div>
								<input type="hidden" id="campPrompt" name="campPrompt" value=""/>
								<input type="hidden" id="campAnswer" name="campAnswer" value=""/>
								<hr>
								<span style="vertical-align:top;">&nbsp;题干：</span>
								<script id="editor1" type="text/plain" style="width:100%;height:150px;margin-top:5px;"></script>
								<br>
								<span style="vertical-align:top;">&nbsp;答案：</span>
								<script id="editor2" type="text/plain" style="width:100%;height:150px;margin-top:5px;"></script>
							</div>
						</div>
			        </form>
			    </div>
		    </div>
	    </div>
		<div>
			<!-- 手动导入录音form表单 -->
			<form id="handleUpForm" action="" enctype="multipart/form-data" style="display:none;">
				<input type="file" name="file" value="" style="display:"/>
				<input type="hidden" name='bankId' value="" />
				<input type="hidden" name='topicId' value="" />
			</form>
		</div>
		<div>
			<!-- 手动导入压缩包form表单 -->
			<form id="handleZipUpForm" action="" enctype="multipart/form-data" style="display:none;">
				<input type="file" name="zipFile" value="" style="display:"/>
				<input type="hidden" name='bankId' value="" />
				<input type="hidden" name='topicId' value="" />
			</form>
		</div>
    </div>
</body>
</html>