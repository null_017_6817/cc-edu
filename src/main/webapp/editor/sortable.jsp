<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title></title>

	<link rel="stylesheet" type="text/css" href="/${projectName}/common/base.css">
 	<link id="easyuiTheme" rel="stylesheet" type="text/css" href="/${projectName}/easyui1_5/themes/custom/uimaker/easyui.css">
	<link rel="stylesheet" type="text/css" href="/${projectName}/easyui1_5/themes/custom/uimaker/icon.css">
	<link rel="stylesheet" type="text/css" href="/${projectName}/common/com.css">
	<link rel="stylesheet" type="text/css" href="/${projectName}/editor/sortable.css">
	
	<script type="text/javascript" src="/${projectName}/book/js/jqueryForm/jquery-1.8.2.min.js"></script>
	<script src="jquery-ui.js"></script>
	<script type="text/javascript" src="/${projectName}/book/js/jqueryForm/jquery.form.js"></script>
	<script type="text/javascript" src="/${projectName}/easyui1_5/themes/custom/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="/${projectName}/easyui1_5/themes/custom/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="/${projectName}/dictionary/dictionary2.js"></script>
	
	<!-- 录音插件 -->
	<script type="text/javascript" src="/${projectName}/js/record/HZRecorder.js"></script>
	<!-- 富文本编辑器 -->
	<script type="text/javascript" charset="utf-8" src="../ueditor/ueditor.config.js"></script>
	<script type="text/javascript" charset="utf-8" src="../ueditor/ueditor.all.js"> </script>
	<script type="text/javascript" charset="utf-8" src="../ueditor/lang/zh-cn/zh-cn.js"></script>
	<style>
		td > span {
			width: 155px !important;
		}	
	</style>

<script>
var ss;
var projectName="${projectName}";
var ue1 = UE.getEditor('editor1',{toolbars:[
		[
		 	'simpleupload','source', //源代码
			'removeformat',
		 	'undo', //撤销
			'redo', //重做
			'formatmatch', //格式刷
			'bold', //加粗
			'indent', //首行缩进
			'italic', //斜体
			'underline', //下划线
			'strikethrough', //删除线
			'time', //时间
			'date', //日期
			'justifyleft', //居左对齐
			'justifyright', //居右对齐
			'justifycenter', //居中对齐
			'justifyjustify', //两端对齐
			'fullscreen', //全屏
			'autotypeset', //自动排版
			'touppercase', //字母大写
			'tolowercase', //字母小写
			'forecolor', // 字体颜色
			'fontfamily', // 字体
			'fontsize', // 字体大小
			'inserttable', //插入表格
			'insertrow', //前插入行
			'insertcol', //前插入列
			'mergeright', //右合并单元格
			'mergedown', //下合并单元格
			'deleterow', //删除行
			'deletecol', //删除列
			'splittorows', //拆分成行
			'splittocols', //拆分成列
			'splittocells', //完全拆分单元格
			'deletecaption', //删除表格标题
			'inserttitle', //插入标题
			'mergecells', //合并多个单元格
			'deletetable', //删除表格
			'backcolor', //背景色
		]
	],
	autoHeightEnabled:false,
	autoFloatEnabled:false,
	elementPathEnabled:false});
	
	// 标准答案编辑器
	var ue2 = UE.getEditor('editor2',{toolbars:[
		[
			'simpleupload','source', //源代码
			'removeformat',
			'undo', //撤销
			'redo', //重做
			'formatmatch', //格式刷
			'bold', //加粗
			'indent', //首行缩进
			'italic', //斜体
			'underline', //下划线
			'strikethrough', //删除线
			'time', //时间
			'date', //日期
			'justifyleft', //居左对齐
			'justifyright', //居右对齐
			'justifycenter', //居中对齐
			'justifyjustify', //两端对齐
			'fullscreen', //全屏
			'autotypeset', //自动排版
			'touppercase', //字母大写
			'tolowercase', //字母小写
			'forecolor', // 字体颜色
			'fontsize', // 字体大小
			'inserttable', //插入表格
			'insertrow', //前插入行
			'insertcol', //前插入列
			'mergeright', //右合并单元格
			'mergedown', //下合并单元格
			'deleterow', //删除行
			'deletecol', //删除列
			'splittorows', //拆分成行
			'splittocols', //拆分成列
			'splittocells', //完全拆分单元格
			'deletecaption', //删除表格标题
			'inserttitle', //插入标题
			'mergecells', //合并多个单元格
			'deletetable', //删除表格
			'backcolor', //背景色
		]
	],
	autoHeightEnabled:false,
	autoFloatEnabled:true,
	elementPathEnabled:false});
	
	UE.Editor.prototype._bkGetActionUrl = UE.Editor.prototype.getActionUrl;  
	UE.Editor.prototype.getActionUrl = function(action){  
		if(action == 'uploadimage' || action == 'uploadscrawl' || action == 'uploadimage'){  
			return '/${projectName}/uploaditemimage.action';  
		}else{  
			return this._bkGetActionUrl.call(this, action);  
		}
	}
	
	
//***********************init start**********************
var topicIdParam;
var bankIdParam;
function loadStep(){
	$("#sortable li").get(0).click();
	var topicId = $(window.parent.document).find("input[name='topicIdHide']").val();
	bankIdParam = $(window.parent.document).find("input[name='bankIdHide']").val();
	if(topicId != null && topicId != "" && topicId != 0 && typeof(topicId) != "undefined"){
		topicIdParam = topicId;
		$.ajax({
			type:"POST",
			url:'/'+projectName+'/paper/question/getTopicAndStep.action',
			data:{"topicId":topicId},
			dataType:'json',
			async: false,
			success:function(result){
				if(result.success){
					var data = result.data;
					var topic = data.topic;
					var topictype = topic.type;
					// 回显topic
					var stepInfoList = data.stepInfoList;
					showTopic(topic,stepInfoList);
				}else{
					alert(result.msg);
				}
			}
		});
	}else{
		$.ajax({
			type:"POST",
			url:'/${projectName}/paper/question/getNewTopicId.action',
			data:{"id":"1"},
			dataType:'json',
			success:function(result){
				if(result.success){
					var newTopicId = result.data
					topicIdParam = newTopicId;
					$(window.parent.document).find("input[name='topicIdHide']").val(newTopicId);
				}
			}
		});
	}
}
// 回显 topic
function showTopic(topic,stepInfoList){
	if( topic != null && typeof(topic) != "undefined" ){
		var topicForm = $('#topicContent').find('form');
		topicForm.find("input[name='topicId']").val(topic.topicId);// topicId
		topicForm.find("input[name='title']").val(topic.title);// title
		if(topic.title != null && topic.title != "" && typeof(topic.title) != "undefined"){
			$("li[name='leftTitle']").html(topic.title)
		}
		if(topic.type == "实训营地"){
			topicForm.find("#topicType").val(topic.type);// type
		}else{
			topicForm.find("#topicType").val(topic.type);// type
		}
		topicForm.find("input[name='level']").val(topic.level);// level
		professionDict.select(topic.profession);
		topicForm.find("input[name='keyWords']").val(topic.keyWords);// keyWords
		topicForm.find("input[name='bookmodul']").val(topic.bookmodul);// bookmodul
		topicForm.find("input[name='talkSkill']").val(topic.talkSkill);// talkSkill
		difficultyDict.select(topic.difficulty);
		topicForm.find("input[name='totalScore']").val(topic.totalScore);// totalScore
		bookIdDict.select(topic.bookId);
		topicForm.find("textarea[name='remark']").val(topic.remark);// remark
		var topichtm = $(".topic").clone(true);
		
		var topictype = topic.type;
		topicForm.find("#topicType").trigger('change');// 改变下拉框
		if(topictype == "实训营地"){
			ss = stepInfoList;
			var t = setTimeout("showSxyd()",200);
		}else{
			// 回显step
			showStepList(stepInfoList);
		}
	}
}
// 回显步骤
function showStepList( stepInfoList ){
	if(stepInfoList != null && stepInfoList.length > 0){
		for(var i = 0; i < stepInfoList.length; i++ ){
			var stepInfo = stepInfoList[i];
			var type = getType(stepInfo);
			// state-应答陈述题,repeat-复读陈述题,answer-问题-问答题,voice-语音提示题
			if(type == "state" || type == "answer" || type == "repeat" || type == "voice"){
				var aud = stepInfo.prompt.audio;
				$("."+type+"Mode").find("tr").eq(0).find("audio").eq(0).find('source').attr("src","");
				$("."+type+"Mode").find("tr").eq(0).find("audio").eq(0).find('source').attr("src",aud);
				$("."+type+"Mode").find("input[name='promptVoice']").val(stepInfo.prompt.audioHid);
				
				$("."+type+"Mode").find("textarea[name='prompt']").text(stepInfo.prompt.context);
				$("."+type+"Mode").find("input[name='title']").val(stepInfo.title);
				$("."+type+"Mode").find("input[name='score']").val(stepInfo.score);
				$("."+type+"Mode").find("textarea[name='standardAnswer']").text(stepInfo.standardAnswer);
				if(type == "answer" ){
					var origiName = stepInfo.prompt.origiName;
					var fileName = stepInfo.prompt.fileName;
					if(origiName != null && origiName != "" && typeof(origiName) != "undefined"){
						$("."+type+"Mode").find("input[name='origiName']").val(origiName);
						$("."+type+"Mode").find("input[name='fileName']").parent("td").append("<a name='downloadZipA' href='javascript:downloadZip(\""+fileName+"\",\""+origiName+"\");'>"+origiName +"</a>");
					}
					if( fileName != null && fileName != "" && typeof(fileName) != "undefined" ){
						$("."+type+"Mode").find("input[name='fileName']").val(fileName);
					}
				}
				var askAnswerVoice = $("."+type+"Mode").clone(true);
				stepData.push(askAnswerVoice);
				newQuest(stepInfo.title);
			}
			// 单选/多选题
			if( type == "choice_single" || type == "choice_multi" ){
				var claType = "";
				var addBtn = "";
				if(type == "choice_single"){
					claType = "selectMode";
					addBtn = " <button onclick='addChoiceVoice(this);' >添加  </button>";
				}
				if(type == "choice_multi"){
					claType = "selectsMode";
					addBtn = " <button onclick='addChoiceVoices(this);' >添加  </button>";
				}
				var aud = stepInfo.prompt.audio;
				
				$("."+claType).find("tr[name='choiceRoot']").find("audio").eq(0).find('source').attr("src","");
				$("."+claType).find("tr[name='choiceRoot']").find("audio").eq(0).find('source').attr("src",aud);
				$("."+claType).find("input[name='promptVoice']").val(stepInfo.prompt.audioHid);
				
				$("."+claType).find("textarea[name='prompt']").text(stepInfo.prompt.context);
				$("."+claType).find("input[name='title']").val(stepInfo.title);
				$("."+claType).find("input[name='score']").val(stepInfo.score);
				$("."+claType).find("tr[name='choice']:gt(1)").remove();
				$("."+claType).find("td[name='choiceAnswer']").find("label:gt(1)").remove();
				var choices = stepInfo.choiceQuestionList;
				for( var c = 0; c < choices.length; c++ ){
					var letter = sort.charAt(c);
					var choice = choices[c];
					var chAudio = choice.audio;
					
					if( c >= 2 ){
						// 拼接选项
						var ch = "<tr name='choice'>"
								+	"<td class='label'>"+letter+"：</td>"
								+	"<td name='td"+letter+"'>"
								+		"<input name='choice"+letter+"' type='text' value='"+choice.context+"' />"
								+	addBtn
								+		" <button onclick=\"delChoice(this);\" >删除 </button>"
								+		" <button onclick='showP(this);';>展开</button><br>"
								+		"<p style='display:none'>"
								+			"<audio controls><source src='"+choice.audio+"' type='audio/mpeg'/></audio><br>"
								+			"<input onclick='startRecording(this)' type='button' value='录音' /> "
								+			"<input onclick='stopRecording(this)' type='button' value='停止' /> "
								+			"<input onclick='playRecording(this)' type='button' value='播放' /> "
								+			"<input onclick='uploadAudio(this)' type='button' value='提交' /> "
								+			"<input onclick='handleUp(this)' type='button'  value='导入' /> "
								+		"</p>"
								+		"<input type='hidden' name='voice' value='"+choice.audioHid+"' />"
								+	"</td>"
								+"</tr><br>";
								$("."+claType).find("tr[name='choice']:last").after(ch);
							// 拼接答案
							var an = "";
							if(claType == "selectMode"){
								if(c != 0 && c%5 == 0){
									an = "<br> <label><span>"+letter+":</span><input type='radio' value='"+letter+"' name='standardAnswer' /></label>";
								}else{
									an = " <label><span>"+letter+":</span><input type='radio' value='"+letter+"' name='standardAnswer' /></label>";
								}
							}
							if(claType == "selectsMode"){
								if(c != 0 && c%5 == 0){
									an = "<br> <label><span>"+letter+":</span><input type='checkBox' value='"+letter+"' name='standardAnswer' /></label>";
								}else{
									an = " <label><span>"+letter+":</span><input type='checkBox' value='"+letter+"' name='standardAnswer' /></label>";
								}
							}
						$("."+claType).find("td[name='choiceAnswer']").append(an);
					}else{
						$("."+claType).find("tr[name='choice']").eq(c).find("input[name='choice"+letter+"']").val(choice.context);
						$("."+claType).find("tr[name='choice']").eq(c).find("input[name='voice']").val(choice.audioHid);
						$("."+claType).find("tr[name='choice']").eq(c).find("source").eq(0).attr("src",chAudio);
					}
				}
				
				// 清空选中
				$("."+claType).find("td[name='choiceAnswer']").find("label").find("input").each(function(){
					$(this).prop("checked", "");
				});
				// 赋值答案
				var standardAnswer = stepInfo.standardAnswer;
				if( standardAnswer.length == 1 ){
					var ind = sort.indexOf(standardAnswer);
					$("."+claType).find("td[name='choiceAnswer']").find("label").eq(ind).find("input").prop("checked",true);
				}
				if(standardAnswer.length > 1){
					var standardArry = standardAnswer.split(",");
					for( var s = 0; s < standardArry.length; s++ ){
						var index = sort.indexOf(standardArry[s]);
						$("."+claType).find("td[name='choiceAnswer']").find("label").eq(index).find("input").prop("checked",true);
					}
				}
				// 复制表格
				var selectVoice = $("."+claType).clone(true);
				stepData.push(selectVoice);// 保存进变量值中
				newQuest(stepInfo.title);// 新加左侧导航
			}
			// 填空题
			if( type == "judge" ){
				
				var aud = stepInfo.prompt.audio;
				$("."+type+"Mode").find("tr").eq(0).find("audio").eq(0).find('source').attr("src","");
				$("."+type+"Mode").find("tr").eq(0).find("audio").eq(0).find('source').attr("src",aud);
				$("."+type+"Mode").find('tr').eq(0).find("input[name='promptVoice']").val(stepInfo.prompt.audioHid);
				
				$("."+type+"Mode").find("textarea[name='prompt']").text(stepInfo.prompt.context);
				$("."+type+"Mode").find("input[name='title']").val(stepInfo.title);
				$("."+type+"Mode").find("input[name='score']").val(stepInfo.score);
				$("."+type+"Mode").find("input[name='standardAnswer'][value='"+stepInfo.standardAnswer+"']").attr("checked",true);
				var table = $("."+type+"Mode").clone(true);
				stepData.push(table);// 保存进变量值中 
				newQuest(stepInfo.title);// 新加左侧导航 
			}
			// 填空题
			if( type == "cloze" ){
				if(stepInfo.prompt.audioHid != null && stepInfo.prompt.audioHid != '' 
					|| typeof(stepInfo.prompt.audioHid) != "undefined"){
					
					$("."+type+"Mode").find("tr[name='fillTr']:gt(0)").remove();
					var aud = stepInfo.prompt.audio;
					$("."+type+"Mode").find("tr").eq(0).find("audio").eq(0).find('source').attr("src","");
					$("."+type+"Mode").find("tr").eq(0).find("audio").eq(0).find('source').attr("src",aud);
					$("."+type+"Mode").find("input[name='promptVoice']").val(stepInfo.prompt.audioHid);
					var prompts = stepInfo.prompt.context;
					promptArg = prompts.split("[]");
					var standardAnswers = stepInfo.standardAnswer;
					var standardAnswerArg = standardAnswers.split(",");
					var scores = stepInfo.score;
					var scoreArg = scores.split(",");
					
					$("."+type+"Mode").find("input[name='title']").val(stepInfo.title);
					for( var j = 0; j < promptArg.length; j++ ){
						if(promptArg[j] != null && $.trim(promptArg[j]) != "" && typeof(promptArg[j]) != "undefined" ){
							if(scoreArg[j] == null || typeof(scoreArg[j]) == "undefined"){
								scoreArg[j] = ""
							}
							if(standardAnswerArg[j] == null || typeof(standardAnswerArg[j]) == "undefined"){
								standardAnswerArg[j] = ""
							}
							if( j == 0){
								$("."+type+"Mode").find("tr[name='fillTr']").find("input[name='prompt']").val(promptArg[j]);
								$("."+type+"Mode").find("tr[name='fillTr']").find("input[name='standardAnswer']").val(standardAnswerArg[j]);
								$("."+type+"Mode").find("tr[name='fillTr']").find("input[name='score']").val(scoreArg[j]);
							}
							if( j > 0 ){
								var tr = "<tr name='fillTr'>"
										+	"<td><input name='prompt' type='text' value='"+promptArg[j]+"' style='width:120px;height:22px;'/></td>"
										+	"<td><input name='standardAnswer' value='"+standardAnswerArg[j]+"' type='text' style='width:100px;height:22px;'/></td>"
										+	"<td><input name='score' type='number' value='"+scoreArg[j]+"' style='width:60px;height:22px;'/>"
										+	" <button onclick='addFill(this);'>添加</button>"
										+	" <button onclick='delFill(this);'>删除</button>"
										+	"</td>"
										+"</tr>";
										
								$("."+type+"Mode").find("tr[name='fillTr']:last").after(tr);
							}
						}
					}
					var table = $("."+type+"Mode").clone(true);
					stepData.push(table);// 保存进变量值中
					newQuest(stepInfo.title);// 新加左侧导航 
				}else{
					showclozed(stepInfo);
				}
			}
		}
	}
}

// 显示截取一句话类型的填空题
function showclozed(stepInfo){
	var aud = stepInfo.prompt.audio;
	$(".clozedMode").find("tr").eq(0).find("audio").eq(0).find('source').attr("src","");
	$(".clozedMode").find("tr").eq(0).find("audio").eq(0).find('source').attr("src",aud);
	$(".clozedMode").find("input[name='promptVoice']").val(stepInfo.prompt.audioHid);
	
	$(".clozedMode").find("textarea[name='prompt']").text(stepInfo.prompt.context);
	$(".clozedMode").find("textarea[name='prompt']").text(stepInfo.prompt.context);
	$(".clozedMode").find("input[name='title']").val(stepInfo.title);
	var standardAnswers = stepInfo.standardAnswer;
	var standardAnswerArg = standardAnswers.split(",");
	var scores = stepInfo.score;
	var scoreArg = scores.split(",");
	
	$(".clozedMode").find("input[name='title']").val(stepInfo.title);
	for( var j = 0; j < standardAnswerArg.length; j++ ){
		if(standardAnswerArg[j] != null && $.trim(standardAnswerArg[j]) != "" && typeof(standardAnswerArg[j]) != "undefined" ){
			if(scoreArg[j] == null || typeof(scoreArg[j]) == "undefined"){
				scoreArg[j] = ""
			}
			if(standardAnswerArg[j] == null || typeof(standardAnswerArg[j]) == "undefined"){
				standardAnswerArg[j] = ""
			}
			if( j == 0){
				$(".clozedMode").find("tr[name='fillTr']").find("input[name='standardAnswer']").val(standardAnswerArg[j]);
				$(".clozedMode").find("tr[name='fillTr']").find("input[name='score']").val(scoreArg[j]);
			}
			if( j > 0 ){
				var tr = "<tr name='fillTr'>"
						+	"<td></td>"
						+	"<td><input name='standardAnswer' value='"+standardAnswerArg[j]+"' type='text' style='width:100px;height:22px;'/></td>"
						+	"<td><input name='score' type='number' value='"+scoreArg[j]+"' style='width:60px;height:22px;'/>"
						+	" <button onclick='addFilld(this);'>添加</button>"
						+	" <button onclick='delFill(this);'>删除</button>"
						+	"</td>"
						+"</tr>";
						
				$(".clozedMode").find("tr[name='fillTr']:last").after(tr);
			}
		}
	}
	
	var askAnswerVoice = $(".clozedMode").clone(true);
	stepData.push(askAnswerVoice);
	newQuest(stepInfo.title);
	
}


function showSxyd(){
	stepInfoList = ss;
	
	if(stepInfoList != null && stepInfoList.length > 0){
		var stepInfo = stepInfoList[0];
		$("#sx").find("input[name='title']").val(stepInfo.title);
		if(stepInfo.prompt != null && stepInfo.prompt.context != null
				&& typeof(stepInfo.prompt.context) != "undefined" ){
			var param = stepInfo.prompt.context;
			// param = param.replace(/\%/g,"%25").replace(/\#/g,"%23").replace(/\&/g,"%26").replace(/\+/g,"%2B");
			
			
			UE.getEditor('editor1').setContent(param);
		}
		
		if(stepInfo.standardAnswer != null && typeof(stepInfo.standardAnswer) != "undefined" ){
			var standardAnswer = stepInfo.standardAnswer;
			// standardAnswer = standardAnswer.replace(/\%/g,"%25").replace(/\#/g,"%23").replace(/\&/g,"%26").replace(/\+/g,"%2B");
			UE.getEditor('editor2').setContent(standardAnswer);
		}
		newQuest(stepInfo.title);// 新加左侧导航 
	}
}
//***********************init end**********************

//***********************base start********************

var stepData=[];
var snum=0;
var currLi="";
var practicalData = "";
var sort="ABCDEFGHIJKLMNOPQRSTUVWXYZ";

var u1Data = "";
var u2Data = "";

$(function(){
    window.setTimeout(function(){
    	
    }, 2000)
	$("#sortable").sortable({
		placeholder: "highlight",
		items: "li:not(.static)"
	});
	$("#addQuestion").click(function(){
		var topicType = $("select[name='type']").val();
		if(topicType != ""){
			if(topicType == "实训营地"){
				//$("#addQuestion").css("display","none");
				var len = $("#sortable").find("li[class='question']").length;
				var lenac = $("#sortable").find("li[class='question active']").length;
				if(len >= 1 || lenac >= 1){
					alert("只能添加一个");
					return false;
				}
			}
			if(topicType != "实训营地"){
				//$("#addQuestion").css("display","block");
			}
			$("#newQuestDialog input[name='name']").val("步骤"+(snum+1));
			$("#newQuestDialog").show();
			$("#mask").show();
		}else{
			alert("请先选择题目类型");
		}
	});
	//修改时加载原step
	loadStep();
	$("#sortable" ).disableSelection();
	$("select[name='questionType']").css("display","none");
});



// 获取类型
function getType(stepInfo){
	return stepInfo.type;
}

//加左面版题条
function newQuest(title){
	if(title==undefined){
		title=$("#newQuestDialog input[name='name']").val();
	}
	$(".end2").before("<li step='"+snum+"' class='question' onclick='openQuestion(this)'><font>"+title+"</font><em onclick='delQuestion(this)'>-</em></li>");//题条
	$(".end1").before("<li step='"+snum+"' class='xh'>"+(snum+1)+"</li>");//题号
	$("#newQuestDialog").hide();
	$("#mask").fadeOut();
	snum++;
}
function openQuestion(li){
	
	$("select[name='questionType']").show(); 
	if(currLi!=""){
		currLi.removeClass("active");
	}
	$('li').removeClass("active");
	$(li).addClass("active");
	currLi=$(li);
	
	var topicType = $("select[name='type']").val();
	if(topicType == "实训营地"){
		$("#lx").css("display","none");
		$("#sx").css("display","block");
		$("#topicContent").css("display","none");
		$("select[name='questionType']").val("practical");
		$("#sx").find("input[name='title']").val(currLi.find("font").html());
	}else{
		$("#lx").css("display","block");
		$("#sx").css("display","none");
		$("#topicContent").css("display","none");
		var stepid=currLi.attr("step");
		if(typeof(stepData[stepid]) != "undefined" ){
			if(stepData[stepid] != null && stepData[stepid] != "" && typeof(stepData[stepid]) != "undefined"){
				var type = stepData[stepid].find("input[name='questionType']").val();
				if("clozeMode" == type){
					var voice = stepData[stepid].find("input[name='promptVoice']").val();
					if(voice == null || voice == "" || typeof(voice) == "undefined"){
						type = "clozedMode";
					}
				}
				$("select[name='questionType']").val(type);
				$("#lx").html(stepData[stepid]);
			}else{
				$("#lx").html("");
				$("select[name='questionType']").val("");
			}
		}else{
			$("#lx").html("");
			$("select[name='questionType']").val("");
		}
	}
}
// 删除问题
function delQuestion(em){
	var oEvent = event;
	oEvent.cancelBubble = true; 
	
	var li=$(em).parent("li");
	var step=li.attr("step");
	stepData[step]="";
	li.remove();
	$(".xh[step='"+step+"']").remove();
	snum--;
	sorts();
	delSx();
}

// 删除实训
function delSx(){
	var u1 = UE.getEditor('editor1').setContent("");// 题干
	var u2 = UE.getEditor('editor2').setContent("");// 答案
	$("#sortable li").get(0).click();
	$("#sx").find("input[name='title']").val('');

}

// 重新排序
function sorts(){
	$(".xh").each(function(i,item){
		$(this).html(i+1);
	});
}

//改变题型
function changeQuestion(sel){
	$("#topicContent").css("display","none");
	if( currLi != null && currLi != "" && typeof(currLi) != "undefined"){
		var stepid=currLi.attr("step");
		var qtype=sel.value;
		if(qtype != ""){
			if(qtype != "实训营地"){
				$("#lx").css("display","block");
				var questionType = $("."+qtype).find("input[name='questionType']").val();
				$('source',"."+qtype).attr("src","");
				$(':input','.'+qtype).not(':button, :submit, :reset').val('').removeAttr('checked').removeAttr('selected');
				if(qtype == "selectMode" || qtype == "selectsMode"){
					$("."+qtype).find("tr[name='choice']:gt(1)").remove();
					$("."+qtype).find("td[name='choiceAnswer']").find('label:gt(1)').remove();
				}
				if(qtype == "clozeMode"){
					$("."+qtype).find("tr[name='fillTr']:gt(0)").remove();
				}
				if(qtype == "clozedMode"){
					$("."+qtype).find("tr[name='fillTr']:gt(0)").remove();
				}
				
				$("."+qtype).find("input[name='questionType']").val(questionType);
				stepData[stepid]=$("."+qtype).clone(true);
				$("#lx").html(stepData[stepid]);
				$("#lx").find("input[name='title']").val(currLi.find("font").html());
			}else{
				$("#sx").css("display","block");
				practicalData=$("."+qtype).clone(true);
				$("#sx").html($("."+qtype).html());
				$("#sx").find("input[name='title']").val(currLi.find("font").html());
			}
		}else{
			stepData.splice(stepid,stepid);
			$("#lx").html("");
			$("#lx").css("display","none");
			$("#sx").css("display","none");
		}
	}else{
		$(sel).val("");
	}
}

// 添加选项（单选）
function addChoiceVoice(t){
	doAddCho(t,"voice");
}

// 多选
function addChoiceVoices(t){
	doAddChos(t,"voice");
}

// 添加选项（非语音）
function addChoice(t){
	doAddCho(t,"text");
}
// 添加操作(单选)
function doAddCho(t,type){
	var len = $("#lx").find("tr[name='choice']").length;
	var letter = sort.charAt(len);
	var abtn = "";
	var ch = "<tr name='choice'>"
			+	"<td class='label'>"+letter+"：</td>"
			+	"<td name='td"+letter+"'>"
			+		"<input name='choice"+letter+"' type='text' value='' />"
			+		" <button onclick='addChoiceVoice(this);' >添加  </button>"
			+		" <button onclick=\"delChoice(this);\" >删除 </button>"
			+		" <button onclick='showP(this);';>展开</button><br>"
			+		"<p style='display:none'>"
			+			"<audio controls><source src='' type='audio/mpeg'/></audio><br>"
			+			"<input onclick='startRecording(this)' type='button' value='录音' /> "
			+			"<input onclick='stopRecording(this)' type='button' value='停止' /> "
			+			"<input onclick='playRecording(this)' type='button' value='播放' /> "
			+			"<input onclick='uploadAudio(this)' type='button' value='提交' /> "
			+			"<input onclick='handleUp(this)' type='button'  value='导入' /> "
			+		"</p>"
			+		"<input type='hidden' name='voice' value='' />"
			+	"</td>"
			+"</tr><br>";
	$("#lx").find("tr[name='choice']:last").after(ch);
	var an = "";
	if(len != 0 && len%5 == 0){
		an = "<br> <label><span>"+letter+":</span><input type='radio' value='"+letter+"' name='standardAnswer' /></label>";
	}else{
		an = " <label><span>"+letter+":</span><input type='radio' value='"+letter+"' name='standardAnswer' /></label>";
	}
	$("#lx").find("td[name='choiceAnswer']").append(an);
}

//多选
function doAddChos(t,type){
	var len = $("#lx").find("tr[name='choice']").length;
	var letter = sort.charAt(len);
	var abtn = "";
	var ch = "<tr name='choice'>"
			+	"<td class='label'>"+letter+"：</td>"
			+	"<td name='td"+letter+"'>"
			+		"<input name='choice"+letter+"' type='text' value='' />"
			+		" <button onclick='addChoiceVoices(this);' >添加  </button>"
			+		" <button onclick=\"delChoice(this);\" >删除 </button>"
			+		" <button onclick='showP(this);';>展开</button><br>"
			+		"<p style='display:none'>"
			+			"<audio controls><source src='' type='audio/mpeg'/></audio><br>"
			+			"<input onclick='startRecording(this)' type='button' value='录音' /> "
			+			"<input onclick='stopRecording(this)' type='button' value='停止' /> "
			+			"<input onclick='playRecording(this)' type='button' value='播放' /> "
			+			"<input onclick='uploadAudio(this)' type='button' value='提交' /> "
			+			"<input onclick='handleUp(this)' type='button'  value='导入' /> "
			+		"</p>"
			+		"<input type='hidden' name='voice' value='' />"
			+	"</td>"
			+"</tr><br>";
	$("#lx").find("tr[name='choice']:last").after(ch);
	var an = "";
	if(len != 0 && len%5 == 0){
		an = "<br> <label><span>"+letter+":</span><input type='checkBox' value='"+letter+"' name='standardAnswer' /></label>";
	}else{
		an = " <label><span>"+letter+":</span><input type='checkBox' value='"+letter+"' name='standardAnswer' /></label>";
	}
	$("#lx").find("td[name='choiceAnswer']").append(an);
}

//添加选项
function delChoice(t){
	var letter = $(t).parent('td').attr("name");
	letter = letter.substring(2,3);
	$(t).parent('td').parent('tr').remove();
	var trArg = $("#lx").find("tr[name='choice']");
	for( var i = 0; i < trArg.length; i++ ){
		$(trArg[i]).find("td").eq(0).html( sort.charAt(i)+"：" );
		$(trArg[i]).find("td").eq(1).find('input').attr("name","choice"+sort.charAt(i));
		$(trArg[i]).find("td").eq(1).find("a[name='delBtn']").attr("href","javascript:delChoice(this);");
		$(trArg[i]).find("td").eq(1).attr("name","td"+sort.charAt(i));
	}
	$("#lx").find("td[name='choiceAnswer']").find('br').remove();
	$("#lx").find("td[name='choiceAnswer']").find('label').eq(sort.indexOf(letter)).remove();
	$("#lx").find("td[name='choiceAnswer']").find('label').each(function(i,item){
		$(this).find('span').html(sort.charAt(i)+":");
		$(this).find('input').val(sort.charAt(i));
		if(i != 0 && (i+1)%5 == 0){
			$(this).after("<br>");
		}
	});
}

// 添加问答项
function addFill(t){
	var len = $("#lx").find("tr[name='fillTr']").length;
	var tr = "<tr name='fillTr' str='"+len+"'>"
			+	"<td><input name='prompt' type='text' style='width:120px;height:22px;'/></td>"
			+	"<td><input name='standardAnswer' type='text' style='width:100px;height:22px;'/></td>"
			+	"<td><input name='score' type='number' style='width:60px;height:22px;'/>"
			+		" <button onclick='addFill(this);'>添加</button>"
			+		" <button onclick='delFill(this);'>删除</button>"
			+	"</td>"
			+"</tr>";
			
	$("#lx").find("tr[name='fillTr']:last").after(tr);
}

// 添加选项
function addFilld(){
	var len = $("#lx").find("tr[name='fillTr']").length;
	var tr = "<tr name='fillTr' str='"+len+"'>"
			+	"<td></td>"
			+	"<td><input name='standardAnswer' type='text' style='width:100px;height:22px;'/></td>"
			+	"<td><input name='score' type='number' style='width:60px;height:22px;'/>"
			+		" <button onclick='addFilld(this);'>添加</button>"
			+		" <button onclick='delFill(this);'>删除</button>"
			+	"</td>"
			+"</tr>";
			
	$("#lx").find("tr[name='fillTr']:last").after(tr);
}


// 删除问答项
function delFill(t){
	$(t).parent('td').parent('tr').remove();
}


// topic 表单
function doTopic(li){
	$("#topicContent").css("display","block");
	$("#lx").css("display","none");
	$("#sx").css("display","none");
	if(currLi!=""){
		currLi.removeClass("active");
	}
	$(li).addClass("active");
	currLi=$(li);
	
	$("select[name='questionType']").hide();
}

// 显示topicTitle
function changeTitle(t){
	var title = $(t).val();
	if(title != null && title != "" && typeof(title) != "undefined" ){
		$("li[name='leftTitle']").html(title);
	}else{
		$("li[name='leftTitle']").html("呼叫业务训练题");
	}
}

function showP(t){
	var txt = $(t).text();
	var newTxt = "";
	if(txt == '展开'){
		newTxt = "关闭"
	}else{
		newTxt = "展开"
	}
	$(t).text(newTxt);
	$(t).next("br").next('p').toggle(300);
}

// 修改topic类型
function changeTopic(t){
	var topicType = $(t).val();
	if(topicType == "实训营地"){// 实训
		stepData = [];
		snum=0;
		currLi="";
		$("li[class='question']").remove();
		$("li[class='xh']").remove();
		$("select[name='questionType']").empty();
		jQuery("#select_id").empty();
		var opt = "<option value='' style='color:#999'>请选择题型</option>"
			+ "<option value='practical'>实训营地</option>";
		$("select[name='questionType']").html(opt);
	}else{
		stepData=[];
		practicalData = "";
		snum=0;
		currLi="";
		$("li[class='question']").remove();
		$("li[class='xh']").remove();
		$("select[name='questionType']").empty();
		var opt = "<option value='' style='color:#999'>请选择题型</option>"
			+ "<option value='selectMode'>单选题</option>"
			+ "<option value='selectsMode'>多选题</option>"
			+ "<option value='answerMode'>问答题</option>"
			+ "<option value='judgeMode'>判断题</option>"
			+ "<option value='clozeMode'>听打录入</option>"
			+ "<option value='clozedMode'>填空题</option>"
			+ "<option value='repeatMode'>复读陈述题</option>"
			+ "<option value='stateMode'>应答陈述题</option>"
			+ "<option value='voiceMode'>语音提示题</option>";
		$("select[name='questionType']").html(opt);
	}
}
//***********************base end********************

//***********************append start****************

String.prototype.startWith=function(str){
	var reg=new RegExp("^"+str);
	return reg.test(this);
}


function getPractical(){
	var obj = new Object();
	var result = "";
	var u1 = UE.getEditor('editor1').getContent();// 题干
	var u2 = UE.getEditor('editor2').getContent();// 答案
	/* u1 = u1.replace(/\%/g,"%25").replace(/\#/g,"%23").replace(/\&/g,"%26").replace(/\+/g,"%2B");*/
	//u1 = encodeURI(u1 );
	/*u2 = u2.replace(/\%/g,"%25").replace(/\#/g,"%23").replace(/\&/g,"%26").replace(/\+/g,"%2B");*/
	//u2 = encodeURI(u2 ); 
	//result = "param="+u1+"&standardAnswer="+u2+"&stepTtitle="+title;
	var title = $("#sx").find("input[name='title']").val();
	obj.stepTtitle = title;
	obj.param = u1;
	obj.standardAnswer = u2;
	obj.stepXmls = new Array();
	return obj;
}

// 获取数据
function getDataArg(){
	var stepList = stepData;
	var stepXmls = new Array();
	if( stepList != null && stepList != "" && typeof(stepList) != "undefined" ){
		for( var i = 0; i < stepList.length; i++ ){
			var step = stepList[i];
			if(step != null && step != "" && typeof(step) != "undefined"){
				var questionType = step.find("input[name='questionType']").val();
				var stepXml = "";
				// 选择则题
				if(questionType == "selectMode" || questionType == "selectsMode"){// 选择则题
					stepXml = doSelect(step);
				} 
				if(questionType == "answerMode" || questionType == "repeatMode"
						|| questionType == "stateMode" || questionType == "voiceMode"){
					stepXml = doAskAnswer(step,questionType);
				}
				if(questionType == "clozeMode" ){// 填空题
					stepXml = doFill(step);
				}
				if(questionType == "judgeMode" ){// 判断题
					stepXml = doJudge(step);
				}
				
				
				
				if(stepXml != ""){
					stepXmls.push(stepXml);
				}
			}
		}
	}
	return stepXmls;
}

// 获取topic form数据
function getTopicForm(){
	
	var topicForm = $("#topicContent");
	var title = topicForm.find("input[name='title']").val();
	var type = topicForm.find("select[name='type']").val();
	var difficulty = topicForm.find("input[name='difficulty']").val();
	var msg = ""
	if($.trim(title) == null || $.trim(title) == ""){
		msg += "标题";
	}
	if($.trim(type) == null || $.trim(type) == ""){
		msg += " 类型";
	}
	if($.trim(difficulty) == null || $.trim(difficulty) == ""){
		msg += " 难度";
	}
	if(msg != ""){
		alert(msg+" 不允许为空！");
		return false;
	}else{
		return topicForm;
	}
}

// 拼接选择题
function doSelect(step){
	var sort="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	var prompt = step.find("textarea[name='prompt']").val();// 题干
	if(prompt != null && typeof(prompt) != "undefined"){
		prompt = prompt.replace(/,/g , "，");
	}
	var promptVoice = step.find("input[name='promptVoice']").val();// 题干录音
	var title = step.find("input[name='title']").val();// 标题
	var choice = "";
	step.find("tr[name='choice']").each(function(i,item){
		var voice = $(this).find("input[name='voice']").val();
		var content = $(this).find("input[name='choice"+sort.charAt(i)+"']").val();
		choice = choice + "<choice id=\""+sort.charAt(i)+"\" audio=\""+voice+"\" >"+content+"</choice>"
		
	});
	var standardAnswer = "";// 答案
	step.find("input[name='standardAnswer']").each(function(i,item){
		if($(item).is(':checked')){
			if(standardAnswer == ""){
				standardAnswer = standardAnswer + sort.charAt(i);
			}else{
				standardAnswer = standardAnswer + "@@" + sort.charAt(i);
			}
		}
	});
	var score = step.find("input[name='score']").val();// 得分
	var choiceType = "choice_single";
	if(standardAnswer.length > 1){
		choiceType = "choice_multi";
	}
	var stepXml = "<step type=\"question\">"
					+	"<title>"+title+"</title>"
					+	"<score>"+score+"</score>"
					+	"<prompt audio=\""+promptVoice+"\" repeat=\"\">"+prompt+"</prompt>"
					+	"<answer type=\""+choiceType+"\">"
					+	choice
					+	"</answer>"
					+	"<standard_answer>"+standardAnswer+"</standard_answer>"
					+"</step>";
	return stepXml;
}

// 拼接问答题
function doAskAnswer(step,type){
	var stepType="";
	var fileParam = "";
	if(type == "stateMode"){
		stepType = "state";
	}
	if(type == "repeatMode"){
		stepType = "repeat";
	}
	if(type == "voiceMode"){
		stepType = "voice";
	}
	if(type == "answerMode"){
		stepType = "question";
		var origiName = "origiName=\""+step.find("input[name='origiName']").val()+"\"";// 文件原名称;
		var fileName = "fileName=\"" + step.find("input[name='fileName']").val()+"\"";// 文件原名称;
		fileParam = "   "+origiName +"   "+fileName;
	}
	var answerType = "";
	if(stepType == "question"){
		answerType = "answer";
	}
	var title = step.find("input[name='title']").val();// 标题
	var prompt = step.find("textarea[name='prompt']").val();// 题干
	if(prompt == null || typeof(prompt) == "undefined"){
		prompt = "";
	}else{
		prompt = prompt.replace(/,/g, "，");
	}
	var promptVoice = step.find("input[name='promptVoice']").val();// 题干录音
	if(promptVoice == null ||  typeof(promptVoice) == "undefined"){
		promptVoice = "";
	}
	var standardAnswer = step.find("textarea[name='standardAnswer']").val();// 答案
	if(standardAnswer != null && typeof(standardAnswer) != "undefined"){
		standardAnswer = standardAnswer.replace(/,/g, "，");
	}
	
	var score = step.find("input[name='score']").val();// 得分
	var stepXml = "<step type=\""+stepType+"\">"
				+	"<title>"+title+"</title>"
				+	"<score>"+score+"</score>"
				+	"<prompt audio=\""+promptVoice+"\" repeat=\"\" "+fileParam+" >"+prompt+"</prompt>"
				+	"<answer type=\""+answerType+"\" length=\"\" />"
				+	"<standard_answer>"+standardAnswer+"</standard_answer>"
				+"</step>";
	return stepXml;
}

// 拼接填空题
function doFill(step){
	var title = step.find("input[name='title']").val();// 标题
	var promptArg = step.find("input[name='prompt']");// 题干
	var standardAnswerArg = step.find("input[name='standardAnswer']");// 答案
	var scoreArg = step.find("input[name='score']");// 得分
	var promptVoice = step.find("input[name='promptVoice']").val();// 题干录音
	var prompt = "";
	if(promptVoice != null && promptVoice != "" && typeof(promptVoice) != "undefined" ){
		if(promptArg != null && promptArg != "" && typeof(promptArg) != "undefined"){
			for( var i = 0; i < promptArg.length; i++ ){
				var pro = $(promptArg[i]).val();
				if(pro != null && pro != "" && typeof(pro) != "undefined"){
					prompt = prompt+pro+"\[]"
				}
			}
		}
	}else{
		prompt = step.find("textarea[name='prompt']").val();// 题干
		if(prompt != null && prompt != "" && typeof(prompt) != "undefined"){
			prompt = prompt.replace(/,/g, "，");
			promptVoice = "";
		}
	}
	
	var standardAnswer = "";
	if(standardAnswerArg != null && standardAnswerArg != "" && typeof(standardAnswerArg) != "undefined"){
		for( var s = 0; s < standardAnswerArg.length; s++ ){
			var stan =$( standardAnswerArg[s]).val();
			if(stan != null && stan != "" && typeof(stan) != "undefined"){
				if(standardAnswer != ""){
					standardAnswer = standardAnswer+"@@"+stan;
				}else{
					standardAnswer = stan;
				}
			}
		}
	}
	var score = "";
	if(scoreArg != null && scoreArg != "" && typeof(scoreArg) != "undefined"){
		for( var j = 0; j < scoreArg.length; j++ ){
			var sco = $(scoreArg[j]).val();
			if(j == 0){
				score = sco;
			}else{
				score = score +"@@"+sco;
			}
		}
	}
	var stepXml = "<step type=\"question\">"
				+	"<title>"+title+"</title>"
				+	"<score>"+score+"</score>"
				+	"<prompt audio=\""+promptVoice+"\" repeat=\"\">"+prompt+"</prompt>"
				+	"<answer type=\"cloze\" length=\"\" />"
				+	"<standard_answer>"+standardAnswer+"</standard_answer>"
				+"</step>";
	return stepXml;
}

// 拼接判断题
function doJudge(step){
	var prompt = step.find("textarea[name='prompt']").val();// 题干
	if(prompt == null || typeof(prompt) == "undefined" ){
		prompt = "";
	}else{
		prompt = prompt.replace(/,/g, "，");
	}
	var voice = step.find("input[name='promptVoice']").val();// 题干
	if(voice == null || typeof(voice) == "undefined" ){
		voice = "";
	}
	var title = step.find("input[name='title']").val();// 标题
	if(title != null && typeof(title) != "undefined"){
		title = title.replace(/,/g, "，");
	}
	var standardAnswer = "";// 答案
	step.find("input[name='standardAnswer']").each(function(i,item){
		if($(item).is(':checked')){
			if(i == 0){
				standardAnswer = "true";
			}else{
				standardAnswer = "false";
			}
		}
	});
	
	var score = step.find("input[name='score']").val();// 得分
	var stepXml = "<step type=\"question\">"
				+	"<title>"+title+"</title>"
				+	"<score>"+score+"</score>"
				+	"<prompt audio=\""+voice+"\" repeat=\"\">"+prompt+"</prompt>"
				+	"<answer type=\"judge\"/>"
				+	"<standard_answer>"+standardAnswer+"</standard_answer>"
				+"</step>";
	return stepXml;
}
//**********************append 结束**********************
	
//**********************voice start*******************
function playVedio(elem) {
	//var audio = $(elem).next("audio");
	var audio = elem;
	audio = audio.get(0);
	var audioState = audio.paused;
	if(audioState) {
		audio.play();
	}else{
		audio.pause();
	}
}

//上传录音
var recorder = new Array();
var recIdx = 0;
function startRecording(t) {
	var audio = $(t).parent('p').find('audio').eq(0);
	var idx=audio.attr("idx");
	if(idx==""||idx==undefined){
		audio.attr("idx",recIdx);
		idx=recIdx;
		recIdx++;
	}
	HZRecorder.get(function(rec) {
		recorder[idx] = {'rec':rec};
		rec.start();
	});
}

function stopRecording(t) {
	var audio = $(t).parent('p').find('audio').eq(0);
	var idx=audio.attr("idx");
	if(recorder[idx] != null && typeof(recorder[idx]) != "undefined" 
		&& recorder[idx].rec != null && typeof(recorder[idx].rec) != "undefined"){
		audio.get(0).pause();
		recorder[idx].rec.stop();
	}else{
		audio.get(0).pause();
	}
}

function playRecording(t) {
	var audio = $(t).parent('p').find('audio').eq(0);
	var idx=audio.attr("idx");
	if(recorder[idx] != null && typeof(recorder[idx]) != "undefined" 
			&& recorder[idx].rec != null && typeof(recorder[idx].rec) != "undefined"){
		recorder[idx].rec.play(audio.get(0));
		audio.get(0).play();
	}else{
		audio.get(0).pause();
		playVedio(audio);
	}
}

function uploadAudio(t) {
	var audio = $(t).parent('p').find('audio').eq(0);
	var idx=audio.attr("idx");
	var btn = $(t);
	recorder[idx].rec.upload("/"+projectName+"/paper/question/uploadAudio.action?bankId="+bankIdParam+"&topicId="+topicIdParam, function(state, resultStr) {
		switch (state) {
		case 'uploading':
			break;
		case 'ok':
			var result = JSON.parse(resultStr);
			if(result.success){
				btn.parent("p").next("input").val(result.fullName);
				btn.parent("p").find('audio').eq(0).get(0).src = result.fileUrl;
				alert("上传成功");
			}else{
				
			}
			break;
		case 'error':
			alert("上传失败");
			break;
		case 'cancel':
			alert("上传被取消");
			break;
		}
	});
}
//**********************voice end*******************
	
function handleUp(t){
	var btn = $(t);
	$("#handleUpForm").find("input[name='file']").unbind();
	$("#handleUpForm").find("input[name='file']").change(function(){
		var fil = $("#handleUpForm").find("input[name='file']").val();
		if(fil != ""){
			$("#handleUpForm").find("input[name='bankId']").val(bankIdParam);
			$("#handleUpForm").find("input[name='topicId']").val(topicIdParam);
			$("#handleUpForm").ajaxSubmit({
				url: '/${projectName}/paper/question/handleUploadVoice.action', 
				type: "post", 
				dataType: "json", 
				success: function (result) {
					if(result.success){
						btn.parent("p").next("input").val(result.fullName);
						btn.parent("p").find('audio').eq(0).get(0).src = result.fileUrl;
					}else{
					}
					$("#handleUpForm").find("input[name='file']").unbind();
					$("#handleUpForm").find("input[name='file']").val("");
				},
			});
		}
	});
	$("#handleUpForm").find("input[name='file']").trigger('click');
}

// 上传实操题压缩包
function handleZipUp(t){
	var btn = $(t);
	$("#handleZipUpForm").find("input[name='zipFile']").unbind();
	$("#handleZipUpForm").find("input[name='zipFile']").change(function(){
		var fil = $("#handleZipUpForm").find("input[name='zipFile']").val();
		if(fil != ""){
			$("#handleZipUpForm").find("input[name='bankId']").val(bankIdParam);
			$("#handleZipUpForm").find("input[name='topicId']").val(topicIdParam);
			$("#handleZipUpForm").ajaxSubmit({
				url: '/${projectName}/paper/question/handleUploadZip.action', 
				type: "post", 
				dataType: "json", 
				success: function (result) {
					if(result.success){
						btn.parent("td").find("input[name='origiName']").val(result.origiName);
						btn.parent("td").find("input[name='fileName']").val(result.fullName);
						btn.parent("td").find("a[name='downloadZipA']").remove();
						btn.parent("td").append("<a name='downloadZipA' href='javascript:downloadZip(\""+result.fullName+"\",\""+result.origiName+"\")'>"+result.origiName +"</a>");
					}
					$("#handleZipUpForm").find("input[name='file']").unbind();
					$("#handleZipUpForm").find("input[name='file']").val("");
				},
			});
		}
	});
	$("#handleZipUpForm").find("input[name='zipFile']").trigger('click');
}

// 下载zip
function downloadZip(fileName,origiName){
	var param = "fileName="+fileName+"&origiName="+origiName+"&bankId="+bankIdParam+"&topicId="+topicIdParam;
	window.location.href="/${projectName}/paper/question/downloadZip.action?"+param;
}


</script>
</head>
<body>
<!-- 页面显示内容 -->
<div style="display:flex;width:100%;height:100%;position:absolute;">
	<div style="padding:20px 0;overflow-y:scroll;flex:1;">
		<ul id="indexIco">
			<li></li>
			<li>o</li>
			<li class="end1">o</li>
		</ul>
		<ul id="sortable">
			<li class="question" name="leftTitle" onclick="doTopic(this);">呼叫业务训练题</li>
			<li class="static">开始</li>
			<li class="static end2">结束</li>
		</ul>
		<div id="addQuestion" style="background-color: #5bb75b !important;">+</div>
	</div>
	<div style="flex:1 1 0%;padding:20px 0px;">
		<select name="questionType" onchange="changeQuestion(this)" style="margin-bottom: 10px;">
			<option value="" style="color:#999">请选择题型</option>
			<option value="selectMode">单选题</option>
			<option value="selectsMode">多选题</option>
			<option value="answerMode">问答题</option>
			<option value="judgeMode">判断题</option>
			<option value="clozeMode">听打录入题</option>
			<option value="clozedMode">填空题</option>
			<option value="repeatMode">复读陈述题</option>
			<option value="stateMode">应答陈述题</option>
			<option value="voiceMode">语音提示题</option>
			<option value="practical">实训营地</option>
		</select>
		<div id="questionPane">
			<div id="lx">
			
			</div>
			
			<div id="topicContent" style="display:none;">
					<form id="_form">
						<input type="hidden" name='topicId' id='topicId' />
						<table id="topicTable">
							<tr>
								<td class='label w'>标题：</td>
								<td><input id="title" name="title"  value="" onchange="changeTitle(this);" />&nbsp;&nbsp;<font color="red">*</font></td>
							</tr>
							<tr>
								<td class='label w'>类型：</td>
								<td>
									<select id="topicType" name="type" onchange='changeTopic(this);' style="width:155px;height:25px;">
										<option value="">请选择</option>
										<option value="1">单选题</option>
										<option value="2">多选题</option>
										<option value="3">问答题</option>
										<option value="4">判断题</option>
										<option value="5">听打录入</option>
										<option value="9">填空题</option>
										<option value="6">复读陈述题</option>
										<option value="7">应答陈述题</option>
										<option value="8">语音提示题</option>
										<option value="10">综合题型</option>
										<option value="实训营地">实训营地</option>
									</select>&nbsp;&nbsp;<font color="red">*</font>
								</td>
							</tr>
							<!-- <tr>
								<td class='label w'>级别：</td>
								<td><input id="level" name="level" value=""/></td>
							</tr> -->
							<tr>
								<td class='label w'>行业：</td>
								<td><dc:insertTree name="profession" dictId="ZD_REPOSITORY" style="synch_single"/></td>
								<!-- <td><input id="profession" name="profession" value=""/></td> -->
							</tr>
							<tr>
								<td class='label w'>关键字：</td>
								<td><input id="keyWords" name="keyWords" value="" /></td>
							</tr>
							<tr>
								<td class='label w'>话术：</td>
								<td><input id="talkSkill" name="talkSkill" value="" /></td>
							</tr>
							<tr>
								<td class='label w'>难度：</td>
								<!-- <td><input id="difficulty" name="difficulty"  value=""/></td> -->
								<td><dc:insertTree name="difficulty" dictId="ZD_DIFFICULT" style="synch_single"/><font color="red">*</font></td>
							</tr>
							<tr>
								<td class='label w'>总分：</td>
								<td><input id="totalScore" name="totalScore" readonly="readonly" value=""/></td>
							</tr>
							<tr>
								<td class='label w'>章节：</td>
								<td>
								<dc:insertTree name="bookId" dictId="CC_BOOK" style="synch_single"/>
								<!-- <input id="bookId" name="bookId"  value=""/> --></td>
							</tr>
							<tr>
								<td class='label w'>备注：</td>
								<td><textarea name="remark" style="width:300px;height:58px;"></textarea>
									<input type="hidden" name="param" value="" />
									<input type="hidden" name="standardAnswer" value="" />
								</td>
							</tr>
						</table>
					</form>
			</div>
			
			<div id="sx" style="display:none">
				<div class="practical ">
					<input type="hidden" name="title" value=""/>
					<hr>
					<span style="vertical-align:top;">&nbsp;题干：</span>
					<script id="editor1" type="text/plain" style="width:97%;height:150px;">
						</script>
					<br>
					<span style="vertical-align:top;">&nbsp;答案：</span>
					<script id="editor2" type="text/plain" style="width:97%;height:150px;">
					</script>
				</div>
			</div>
		</div>
		<!-- <div class="btnbox" style="margin-top:60px;">
		 		<input type="button" value="确定" class="btn btn-default" onclick="newPage()">
		</div> -->
	</div>
</div>

<!-- 弹出内容 -->
<div id="newQuestDialog" style="z-index:999;display:none;">
	<a href="javascript:$('#newQuestDialog').hide();$('#mask').fadeOut()" class="close" style="display:block;float:right;text-decoration:none;color:#555;font-size: 14px;font-weight:bold;">x</a>
	<div style="margin: 50px 20px;line-height:28px;">
		<label>步骤标题：</label>
		<input type="text" name="name" style="width:60%;height:28px;border-radius:2px;border:silver 1px solid;font-size:14px;padding:0px 10px;" maxlength="10"/><br/>
	</div>
	<div class="btnbox" style="margin-top:0px;">
		<input type="button" value="确定" class="btn btn-default" onclick="newQuest()">
	</div>
</div>
 
 <!-- 遮罩 -->
<div id="mask"></div>

<!-- 手动导入录音form表单 -->
<form id="handleUpForm" action="" enctype="multipart/form-data" style="display:none;">
	<input type="file" name="file" value="" style="display:"/>
	<input type="hidden" name='bankId' value="" />
	<input type="hidden" name='topicId' value="" />
</form>

<!-- 手动导入压缩包form表单 -->
<form id="handleZipUpForm" action="" enctype="multipart/form-data" style="display:none;">
	<input type="file" name="zipFile" value="" style="display:"/>
	<input type="hidden" name='bankId' value="" />
	<input type="hidden" name='topicId' value="" />
</form>


<!-- 题目模板 -->
<div style="display:none;">
	<!-- 单选题(语音) -->
	<jsp:include page="./template/select-temp.jsp" />
	<!-- 选择题(语音) -->
	
	<!-- 多选题(语音) -->
	<jsp:include page="./template/selects-temp.jsp" />
	<!-- 选择题(语音) -->
	
	<!-- 听打录入 -->
	<jsp:include page="./template/cloze-temp.jsp" />
	<!-- 听打录入 -->
	
	<!-- 填空题 -->
	<jsp:include page="./template/clozed-temp.jsp" />
	<!-- 填空题 -->
	
	<!-- 问答题（语音） -->
	<jsp:include page="./template/answer-temp.jsp" />
	<!-- 问答题（语音） -->
	
	<!-- 应答题（语音） -->
	<jsp:include page="./template/state-temp.jsp" />
	<!-- 应答题（语音） -->
	
	<!-- 语音提示题（语音） -->
	<jsp:include page="./template/voice-temp.jsp" />
	<!-- 语音提示题（语音） -->
	
	<!-- 复读陈述题（语音） -->
	<jsp:include page="./template/repeat-temp.jsp" />
	<!-- 复读陈述题（语音） -->
								
	<!-- 判断题(语音) -->
	<jsp:include page="./template/judge-temp.jsp" />
	<!-- 判断题(语音) -->
	
</div>
</body>
</html>
