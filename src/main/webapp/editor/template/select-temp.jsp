<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<table class="selectMode model">
		<input type="hidden" name="questionType" value="selectMode"/>
		<input type="hidden" name="title" />
		<tr name="choiceRoot">
			<td class="label">题干：</td>
			<td>
				<textarea name="prompt" style="height:50px;"></textarea>
				<p>
					<audio controls="controls"><source src='' type="audio/mpeg"/></audio><br>
					<input onclick="startRecording(this)" type="button" value="录音" /> 
					<input onclick="stopRecording(this)" type="button" value="停止" /> 
					<input onclick="playRecording(this)" type="button" value="播放" /> 
					<input onclick="uploadAudio(this)" type="button" value="提交" /> 
					<input onclick="handleUp(this)" type="button"  value="导入" /> 
				</p>
				<input type="hidden" name="promptVoice" />
			</td>
		</tr>
		<tr name="choice">
			<td class="label">A：</td>
			<td >
				<input name="choiceA" type="text" value="" />
				<button onclick="addChoiceVoice(this);" >添加</button>
				<button onclick="showP(this);";>展开</button>
				<br>
				<p style="display:none">
					<audio controls="controls"><source src='' type="audio/mpeg"/></audio><br>
					<input onclick="startRecording(this)" type="button" value="录音" /> 
					<input onclick="stopRecording(this)" type="button" value="停止" /> 
					<input onclick="playRecording(this)" type="button" value="播放" /> 
					<input onclick="uploadAudio(this)" type="button" value="提交" /> 
					<input onclick="handleUp(this)" type="button"  value="导入" /> 
				</p>
				<input type="hidden" name="voice" value="" />
			</td>
		</tr>
		<tr name="choice">
			<td class="label">B：</td>
			<td >
				<input name="choiceB" type="text" value="" />
				<button onclick="addChoiceVoice(this);" >添加</button>
				<button onclick="showP(this);";>展开</button><br>
				<p style="display:none">
					<audio controls="controls"><source src='' type="audio/mpeg"/></audio><br>
					<input onclick="startRecording(this)" type="button" value="录音" /> 
					<input onclick="stopRecording(this)" type="button" value="停止" /> 
					<input onclick="playRecording(this)" type="button" value="播放" /> 
					<input onclick="uploadAudio(this)" type="button" value="提交" /> 
					<input onclick="handleUp(this)" type="button"  value="导入" /> 
				</p>
				<input type="hidden" name="voice" value="" />
			</td>
		</tr>
		<tr>
			<td class="label">正确答案：</td>
			<td name="choiceAnswer">
				<label>A:<input type="radio" value="A" name="standardAnswer" /></label>
				<label>B:<input type="radio" value="B" name="standardAnswer" /></label>
			</td>
		</tr>
		<tr>
			<td class="label">得分：</td>
			<td><input name="score" type="number" style="height:22px;"/></td>
		</tr>
	</table>