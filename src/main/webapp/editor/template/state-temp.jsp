<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<table class="stateMode model">
		<input type="hidden" name="questionType" value="stateMode"/>
		<input type="hidden" name="title" />
		<tr>
			<td class="label">问题：</td>
			<td>
				<textarea name="prompt" style="height:50px;"></textarea>
				<p>
					<audio controls><source src="" type="audio/mpeg"/></audio><br>
					<input onclick="startRecording(this)" type="button" value="录音" /> 
					<input onclick="stopRecording(this)" type="button" value="停止" /> 
					<input onclick="playRecording(this)" type="button" value="播放" /> 
					<input onclick="uploadAudio(this)" type="button" value="提交"/> 
					<input onclick="handleUp(this)" type="button"  value="导入" /> 
				</p>
				<input type="hidden" name="promptVoice" />
			</td>
		</tr>
		<tr >
			<td class="label">正确答案：</td>
			<td><textarea name="standardAnswer" style="height:100px;"></textarea></td>
		</tr>
		<tr>
			<td class="label">得分：</td>
			<td><input name="score" type="number" style="height:22px;"/></td>
		</tr>
	</table>