<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<%@include file="/common/include.jsp" %>
<title>监听</title>
<style>

/* 通话列表 */
.monitorCon {
    padding: 0 20px;
    font-size: 0;
    overflow: hidden;
}

.monitorCon .list {
    position: relative;
    display: inline-block;
    width: 19%;
    margin: 0 8px 8px 0;
    border: silver 1px dashed;
    overflow: hidden;
}

.monitorCon .list:nth-child(5n) {
	margin-right: 0;
}

.monitorCon .list dt {
     position: absolute;
    top: -200px;
    left: 0;
    z-index: 5;
    width: 100%;
    height: 100%;
    font-size: 14px;
     background-color: rgba(0,0,0,0.6);
    transition: all .2s;
	-moz-transition: all .2s;	/* Firefox 4 */
	-webkit-transition: all .2s;	/* Safari 和 Chrome */
	-o-transition: all .2s;
}

.monitorCon .list dt span {
	display: block;
	width: 80px;
	height:28px;
	line-height:28px;
	text-align: center;
	font-size: 12px;
	color: #fff;
	margin: 10px auto;
	background: #f36501;
	border-radius: 4px;
    cursor: pointer;
}

.monitorCon .list:hover dt{
	top: 0;
}

.monitorCon .list dd {
    font-size: 14px;
    display: flex;
    padding: 20px 5px;
}

.monitorCon .list dd p {
    line-height: 24px;
    text-align: center;
    flex: 1;
}

.monitorCon .list dd span {
	display: block;
	width: 60px;
	overflow: hidden;
	text-overflow: ellipsis;
	white-space: nowrap;
}
.monitorCon .list dd i {
    margin: 10px;
    color: #003b93;
    font-size: 20px;
}
</style>
<script type="text/javascript">
//**************************************socket  start****************************
/* var agentId = "${CURRENT_USER.agentId}";
 var websocket = null;
	//判断当前浏览器是否支持WebSocket
	if ('WebSocket' in window) {
		if(agentId != null && agentId != "" && typeof(agentId) != "undefined"){
			websocket = new WebSocket("wss://"+document.location.host+"/${projectName}/websocket?agentId=" + agentId);
		}
	}
	else {
		alert('当前浏览器 Not support websocket')
	}
if(websocket != null) {
	//连接发生错误的回调方法
	websocket.onerror = function () {
		setMessageInnerHTML("WebSocket连接发生错误");
		if(agentId != null && agentId != "" && typeof(agentId) != "undefined"){
			websocket = new WebSocket("wss://"+document.location.host+"/${projectName}/websocket?agentId=" + agentId);
		}
	};
	
	//连接成功建立的回调方法
	websocket.onopen = function () {
		setMessageInnerHTML("WebSocket连接成功");
		heartCheck.start();
	}

	//接收到消息的回调方法
	websocket.onmessage = function (event) {
		setMessageInnerHTML(event.data);
		heartCheck.reset();
	}
	
	//连接关闭的回调方法
	websocket.onclose = function () {
		setMessageInnerHTML("WebSocket连接关闭");
	}

	//监听窗口关闭事件，当窗口关闭时，主动去关闭websocket连接，防止连接还没断开就关闭窗口，server端会抛异常。
	window.onbeforeunload = function () {
		closeWebSocket();
	}

	//将消息显示在网页上
	function setMessageInnerHTML(innerHTML) {
		//alert(innerHTML);
		if(innerHTML == "change"){
			$("#show").empty();
			var html = '<div class="monitorBox"><span style="text-align: center;display: block;font-size:20px;padding: 20px 0;">通话列表</span>';
			html += '<div class="monitorCon">';
			$("#show").append(html);
			clearTimeout(intervalID);
			var intervalID =  setTimeout(loadStudCalling(),1000);
		}
	}

	//关闭WebSocket连接
	function closeWebSocket() {
		websocket.close();
	}

	//发送消息
	function send() {
		websocket.send(message);
	} 

	// 心跳
	var heartCheck = {
		timeout: 10000,//5ms
		timeoutObj: null,
		reset: function(){
			clearTimeout(this.timeoutObj);
			this.start();	
		},
		start: function(){
			this.timeoutObj = setTimeout(function(){
				websocket.send("HeartBeat");
			}, this.timeout)
		}
	}
} */
//**************************socket end*************************************

	$(function(){
		$("#show").empty();
		var html = '<div class="monitorBox"><span style="text-align: center;display: block;font-size:20px;padding: 20px 0;">通话列表</span><div class="monitorCon"></div><div>';
		$("#show").append(html);
		$.ajax({
			url:"/${projectName }/dialog/schStudCalling.action",
			dataType : "json",
			success: function(data){
				var personList = data;
				if (personList != null && personList !=undefined && personList != '' ) {
					var html = '';
						$.each(personList, function(i, obj) {
							var toperson = obj.to;
							var agentid = obj.agentId;
							html += '<dl class="list"><dt><span><a onclick="eavesdrop('+agentid+')">监听</a></span><span><a onclick="threeway('+agentid+')">加入通话</a></span></dt>';
							html += '<dd><p><span>'+ obj.name +'</span>'+obj.agentId+'</p><i>→</i>';
							html += '<p>'+toperson[0] +'<br>'+toperson[2] +'</p></dd></dl>';
						});
				}
				$(".monitorCon").append(html);
			},
			error: function(e) {
				console.log(e);
			}
		});
		
	});
	
	/* function loadStudCalling() {
		$("#show").empty();
		$.ajax({
			url:"/${projectName }/dialog/schStudCalling.action",
			dataType : "json",
			success: function(data){
				var html = "<tr><th>拨打者</th><th>来电号码</th><th>接听者</th><th>坐席号</th><th>操作</th>";
				console.log(data);
				var personList = data;
				if (personList != null && personList !=undefined && personList != '' ) {
						$.each(personList, function(i, obj) {
							var toperson = obj.to;
							html += '<tr><td>'+ obj.name +'</td>';
							html += '<td>'+ obj.agentId +'</td>';
							html += '<td>'+ toperson[0] +'</td>';
							html += '<td>'+ toperson[2] +'</td>';
							var agentid = obj.agentId;
							html += '<td><a onclick="eavesdrop('+agentid+')">监听</a>&nbsp;<a onclick="threeway('+agentid+')">加入通话</a>';
							html += '</td></tr>';
						});
				}
				$("#show").append(html);
			},
			error: function(e) {
				console.log(e);
			}
		});
	}  */
	function loadStudCalling() {
		$("#show").empty();
		var html = '<div class="monitorBox"><span style="text-align: center;display: block;font-size:20px;padding: 20px 0;">通话列表</span><div class="monitorCon"></div><div>';
		$("#show").append(html);
		$.ajax({
			url:"/${projectName }/dialog/schStudCalling.action",
			dataType : "json",
			success: function(data){
				var personList = data;
				if (personList != null && personList !=undefined && personList != '' ) {
					var html = '';
						$.each(personList, function(i, obj) {
							var toperson = obj.to;
							var agentid = obj.agentId;
							html += '<dl class="list"><dt><span><a onclick="eavesdrop('+agentid+')">监听</a></span><span><a onclick="threeway('+agentid+')">加入通话</a></span></dt>';
							html += '<dd><p><span>'+ obj.name +'</span>'+obj.agentId+'</p><i>→</i>';
							html += '<p>'+toperson[0] +'<br>'+toperson[2] +'</p></dd></dl>';
						});
				}
				$(".monitorCon").append(html);
			},
			error: function(e) {
				console.log(e);
			}
		});
	} 
	
	function eavesdrop(agentId) {
		$.ajax({
			url:"/${projectName }/dialog/eavesdrop.action",
			type:"post",
			data : {"agentId" : agentId},
			success: function(data){
				data = eval("(" + data + ")");
				if(data.result) {
					if(data.result == "failed") {
						alert("请该学生使用正确坐席号");
					}else if(data.result == "success"){
						alert("请接听电话开始监听");
					}else{
						alert(data.result);
					}
				}
			},
			error:function(e) {
				alert("服务器内部错误");
			}
		});
	} 
	
	function threeway(agentId) {
		$.ajax({
			url:"/${projectName }/dialog/threeway.action",
			type:"post",
			data : {"agentId" : agentId},
			success: function(data){
				data = eval("(" + data + ")");
				if(data.result) {
					if(data.result == "failed") {
						alert("请该学生使用正确坐席号");
					}else if(data.result == "success"){
						alert("请接听电话开始加入通话");
					}else{
						alert(data.result);
					}
				}
			},
			error:function(e) {
				alert("服务器内部错误");
			}
		});
	}
</script>
</head>
<body>
<div id="show">
<div class="monitorBox"><span style="text-align: center;display: block;font-size:20px;padding: 20px 0;">通话列表</span>
<div>
</div>


</body>
</html>