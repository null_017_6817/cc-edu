<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>查看答题步骤 </title>
	<%@include file="/common/include.jsp" %>
<script>
function load(id){
	$('#_form').form('load','/${projectName}/examination/exerStep/vieExerStep.action?toL=true&id='+id);
};
</script>
</head>
<body>
<form id="_form">
	<input type="hidden" name='id' value=""/>
	<table style="margin: 0 auto;">
		<tr><td class='label'>练习编号：</td><td><input type='text' name='eid' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>考题编号：</td><td><input type='text' name='etid' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>步骤标识：</td><td><input type='text' name='step' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>步骤类型：</td><td><input type='text' name='type' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>标题：</td><td><input type='text' name='title' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>分值：</td><td><input type='text' name='score' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>步骤内容：</td><td><input type='text' name='stepinfo' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>标准答案：</td><td><input type='text' name='standardAnswer' class='viewbox' readonly='readonly'/></td></tr>
	</table>

</form>
</body>
</html>
