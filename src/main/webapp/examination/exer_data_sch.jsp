<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>个人完成情况</title>
<script src="/${projectName}/easyui1_5/echarts.min.js"></script>
<script src="/${projectName}/easyui1_5/jquery.min.js"></script>
<script src="/${projectName}/easyui1_5/themes/custom/jquery.easyui.min.js"></script>
<link rel="stylesheet" type="text/css" href="/${projectName}/easyui1_5/themes/custom/uimaker/easyui.css" id="easyuiTheme" >
<script src="/${projectName}/easyui1_5/qct.js"></script>
<style type="text/css">
	.exer{
		width: 225px;
		height: 20px
	}
	table,table tr th, table tr td{
		 border:1px solid #0094ff;
		 border-collapse:collapse;
	}
	#searchForm select{
		margin:0 30px 0 8px;
		width:225px;
		height:36px;
		line-height:36px;
		padding:0 10px;
		outline:none;
		border:0;
		border:1px solid #ccc;
	}
	#searchForm #exer{
		width:200px;
		margin-right: 10px;
	}
	#searchForm .search_input{
		width:94px;
		height:36px;
		line-height:34px;
		text-align:center;
		background:none;
		border:0;
		border:1px solid #ccc;
		border-radius:5px;
		cursor:pointer;
		outline:none;
	}
	.completionList{
		border:1px solid #ccc;
	}
	.completionList thead th{
		height:34px;
		line-height:34px;
		border:1px solid #ddd;
		font-weight:400;
		color:#333;
		background:#efefef;
	}
	.completionList tr td{
		height:30px;
		line-height:30px;
		border:1px solid #eee;
		text-align:center;
	}
	#searchForm .exer-select{
	   width:200px;
	}
	.keyWorlds{
	    height: 36px;
	    line-height: 36px;
	    padding: 0 10px;
	    border: 1px solid #ccc;
	    margin-right: 23px;
	}
	/*修饰滚动条*/
::-webkit-scrollbar-track-piece{background-color:#fff;-webkit-border-radius:0}
::-webkit-scrollbar{width:1px;height:1px}
::-webkit-scrollbar-thumb{height:50px;background-color:#b8b8b8;-webkit-border-radius:6px;outline:2px solid #fff;
outline-offset:-2px;border:2px solid #fff;filter:alpha(opacity=50);-moz-opacity:0.5;-khtml-opacity:0.5;opacity:0.5}
::-webkit-scrollbar-thumb:hover{height:50px;background-color:#878987;-webkit-border-radius:6px}
::-moz-scrollbar-track-piece{background-color:#fff;-webkit-border-radius:0}
::-moz-scrollbar{width:1px;height:1px}
::-moz-scrollbar-thumb{height:50px;background-color:#b8b8b8;-webkit-border-radius:6px;outline:2px solid #fff;
outline-offset:-2px;border:2px solid #fff;filter:alpha(opacity=50);-moz-opacity:0.5;-khtml-opacity:0.5;opacity:0.5}
::-moz-scrollbar-thumb:hover{height:50px;background-color:#878987;-webkit-border-radius:6px}
</style>
<script type="text/javascript">
var kw="";
$(function() {
	 getClasses();
})
function getClasses(){
	$.ajax({
		 async:false,
		 type:"GET",
		 url:"/${projectName}/show/getClasses.action",
		 dataType:"json",
		 data:{
			 
		 },
		 success: function (response) {
	   		if(response.length>0){
	   			var tjsHtml='';
	   			tjsHtml+='<option value="-1">--请选择班级--</option>';
	   			for(var i=0;i<response.length;i++){
	   				var tj=response[i];
	   				tjsHtml+='<option value="'+tj.id+'">'+tj.label+'</option>';
	   			}
	   			$("#bj").html(tjsHtml);
	   		}
		 }

	  });
}

function selectBj(classid){
	if(classid==-1){
		$("#exer").html(tjsHtml);
	}else{
		schExer(classid,kw);
	}
}
function searchClass(){
	var bj=$("#bj").val();
	if(bj==-1){
		$.messager.alert('敬告','请选择班级！');
		return;
	}
	var exer=$("#exer").val();
	if(exer==-1){
		$.messager.alert('敬告','请选择练习！');
		return;
	}
	var wcqk=$("#wcqk").val();
	$.ajax({
		 async:false,
		 type:"GET",
		 url:"/${projectName}/show/schPersonalCompletion.action",
		 dataType:"json",
		 data:{
			 eid:exer,
			 classid:bj,
			 wcqk:wcqk
		 },
		 success: function (response) {
	   		if(response.rows.length>0){
	   			var tjsHtml='';
	   			for(var i=0;i<response.rows.length;i++){
	   				var n=i+1;
	   				var tj=response.rows[i];
	   				tjsHtml+='<tr><td>'+n+'</td><td>'+tj.studid+'</td><td>'+tj.stuName+'</td><td>'+tj.top+'</td><td>'+tj.zts+'</td>';
	   				tjsHtml+='<td>'+tj.wcqk+'</td><td>'+tj.wcl+'</td><tr>';
	   			}
	   			$("#completion").html(tjsHtml);
	   		}
		 }

	  });
	  
	$.ajax({
		url:"/${projectName}/show/schTaskStatistics.action?getType=json",
		async:false,
		data:{
			 eid:exer,
			 classid:bj
		 },
		success:function(data){
			var json = eval("("+data+")");
			showPie("jzg-mz",json);
		}
	}) 
}

function keyWorldsSearch(){
	var bj=$("#bj").val();
	if(bj==-1){
		$("#exer").html(tjsHtml);
	}else{
		kw=$(".keyWorlds").val();
		schExer(bj,kw);
	}
}

function schExer(_classid,_name){
	var tjsHtml='';
	tjsHtml+='<option value="-1">--请选择练习--</option>';
	$.ajax({
		 async:false,
		 type:"GET",
		 url:"/${projectName}/show/schRExerStudBjExer.action",
		 dataType:"json",
		 data:{
			 classid:_classid,
			 name:_name
		 },
		 success: function (response) {
	   		if(response.rows.length>0){
	   			for(var i=0;i<response.rows.length;i++){
	   				var tj=response.rows[i];
	   				tjsHtml+='<option value="'+tj.eid+'">'+tj.name+'</option>';
	   			}
	   		}
	   		$("#exer").html(tjsHtml);
		 }

	  });
}
</script>
<style type="text/css">
</style>
</head>
<body>
	<div style="width: 100%; height: 400px; clear: both;">
		<div style="margin-bottom:20px">
			<form id="searchForm">
				班级: <select class="exer-select" id="bj" onchange="selectBj(this.options[this.options.selectedIndex].value)"></select>
				练习:<select  id="exer" class="exer exer-select">
						<option value="-1">--请选择练习--</option>
					</select>
					<input type="text" class="keyWorlds" placeholder="请输入关键字" onchange="keyWorldsSearch()"/>
				完成情况: <select id="wcqk" class="exer-select">
					<option value="">全部</option>
					<option value="已完成">已完成</option>
					<option value="未完成">未完成</option>
			
					</select>
					<input type="button" onclick="searchClass()" value="统计" class="search_input" /> 
			</form>
		</div>
		<div class="exerdata" style="width: 67%;height: 38px; float: left;overflow-y:scroll">
			<table class="completionList" style="width: 100%">
			  <colgroup>
			      <col width="10%">
			      <col width="20%">
			      <col width="20%">
			      <col width="10%">
			      <col width="10%">
			      <col width="10%">
			      <col>
			    </colgroup>
				<thead>
					<tr>
						<th>序号</th>
						<th>学号</th>
						<th>姓名</th>
						<th>题目数</th>
						<th>做题数</th>
						<th>完成情况</th>
						<th>完成率</th>
					</tr>
				</thead>
		    </table>
		</div>
		<div class="exerdata" style="width: 67%;height: 705px; float: left;overflow: scroll;">
			<table class="completionList" style="width: 100%;margin-bottom: 80px;">	
			    <colgroup>
			      <col width="10%">
			      <col width="20%">
			      <col width="20%">
			      <col width="10%">
			      <col width="10%">
			      <col width="10%">
			      <col>
			    </colgroup>			
				<tbody	id="completion">
			   </tbody>
		    </table>
		</div>
		<div id="jzg-mz" style="width: 28%;height: 400px; float: right;padding-left:5%;"></div>
	</div>
</body>
<script type="text/javascript">
function showPie(divname,json) {
	var pieV={
		dom:document.getElementById(divname),
		title:json.title,
		legend:json.legend,
		xAxis:[],
	    series: [{
	         data: json.series.data
	    }]
	};
	pie(pieV);
}
</script>
</html>