<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>答题步骤 查询</title>
	<%@include file="/common/include.jsp" %>

<script>
$(function(){
	<%-- 为分页行设置"添加"和"删除"按钮 --%>
	var pager = $('#exerStepGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});			
})

<%-- 执行查询，点击查询按钮时调用 --%>
function doSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#exerStepGrid').datagrid('load',{
		step:$('#step').val(),
		type:$('#type').val(),
		title:$('#title').val(),
		score:$('#score').val()
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatOper(val,row,index){  
    return "<span onclick='updExerStep("+val+")' style='text-decoration:underline;cursor:pointer;'>修改</span> "
    			+"<span onclick='vieExerStep("+val+")' style='text-decoration:underline;cursor:pointer;'>查看</span>";
} 

<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
function updExerStep(id){
	QMoveDialog($("#exerStepEditWin"));
	$('#exerStepViewWin').dialog('close');
	$('#exerStepEditWin').dialog('open');
	$('#exerStepEditWin').get(0).contentWindow.load(id);
} 

<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
function vieExerStep(id){
	QMoveDialog($("#exerStepViewWin"));
	$('#exerStepEditWin').dialog('close');
	$('#exerStepViewWin').dialog('open');
	$('#exerStepViewWin').get(0).contentWindow.load(id);
}

<%-- 删除记录 --%>
function delExerStep(){
	var checkedItems = $('#exerStepGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/examination/exerStep/delExerStep.action",{ids:ids},function(data){
				$('#exerStepGrid').datagrid('reload');
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#exerStepEditWin').dialog('close');
	$('#exerStepGrid').datagrid('reload');
}
</script>
</head>
<body>
<table id="exerStepGrid" title="答题步骤 " fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#exerStepGridToolbar',collapsible:true,url:'/${projectName}/examination/exerStep/schExerStep.action',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" ></th>
			<th data-options="field:'eid',width:25,align:'center',sortable:true">练习编号</th>
			<th data-options="field:'etid',width:25,align:'center',sortable:true">考题编号</th>
			<th data-options="field:'step',width:25,align:'center',sortable:true">步骤标识</th>
			<th data-options="field:'type',width:25,align:'center',sortable:true">步骤类型</th>
			<th data-options="field:'title',width:25,align:'center',sortable:true">标题</th>
			<th data-options="field:'score',width:25,align:'center',sortable:true">分值</th>
			<th data-options="field:'stepinfo',width:25,align:'center',sortable:true">步骤内容</th>
			<th data-options="field:'standardAnswer',width:25,align:'center',sortable:true">标准答案</th>

			<th data-options="field:'_oper',align:'center',formatter:formatOper">操作</th>
		</tr>
	</thead>
</table>
<%-- 查询条件设定栏 --%>
<div id="exerStepGridToolbar">
	<div style="margin-bottom:5px">
		<a href="javascript:updExerStep(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true">添加</a>
		<a href="javascript:delExerStep()" class="easyui-linkbutton" iconCls="icon-remove" plain="true">删除</a>
	</div>
	<form id="_form">
	步骤标识：<input name='step' id="step" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	步骤类型：<input name='type' id="type" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	标题：<input name='title' id="title" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	分值：<input name='score' id="score" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>

	<a href="#" class="easyui-linkbutton" iconCls="icon-search" onclick="doSearch()">查询</a>
	</form>
</div>


<%-- 修改or添加面板 --%>
<iframe id="exerStepEditWin" src="/${projectName}/examination/exerStep/page.action?page=exerStep_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#exerStepEditButton'" title="修改答题步骤 " style="width:500px;height:500px;padding:5px;top:5000px" scrolling="no" frameborder="0"></iframe>
<div id="exerStepEditButton">
	<a href="javascript:void(0)" onclick="$('#exerStepEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#exerStepEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >取 消</a>
</div>
<%-- 查看面板 --%>
<iframe id="exerStepViewWin" src="/${projectName}/examination/exerStep/page.action?page=exerStep_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#exerStepViewButton'" title="查看答题步骤 " style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="exerStepViewButton">
	<a href="javascript:$('#exerStepViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >关 闭</a>
</div>
</body>
</html>
