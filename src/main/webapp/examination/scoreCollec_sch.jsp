<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>成绩汇总</title>
	<%@include file="/common/include.jsp" %>

<script>
var eid = "";
$(function(){
	<%-- 为分页行设置"添加"和"删除"按钮 --%>
	var pager = $('#exerGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});			
})

<%-- 执行查询，点击查询按钮时调用 --%>
function doSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#exerGrid').datagrid('load',{
		name:$('#name').val(),
		shortcut:$('#shortcut').val(),
		department:$('#department').val(),
		teacher:$('#teacher').val(),
		bankId:$('#bankId').val(),
		bankName:$('#bankName').val()
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatOper(val,row,index){
	return "<a href='javascript:void(0)' onclick='addtopic("+val+")' class='_oper' title='成绩单'>&nbsp;<i class='icon-list-alt'></i>&nbsp;</a>&emsp;"; 
} 

<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
function updExer(id){
	QMoveDialog($("#exerEditWin"));
// 	$('#exerViewWin').dialog('close');
	$('#exerEditWin').dialog('open');
	$('#exerEditWin').get(0).contentWindow.load(id);
} 

<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
// function vieExer(id){
// 	QMoveDialog($("#exerViewWin"));
// 	$('#exerEditWin').dialog('close');
// 	$('#exerViewWin').dialog('open');
// 	$('#exerViewWin').get(0).contentWindow.load(id);
// }

<%-- 删除记录 --%>
function delExer(){
	var checkedItems = $('#exerGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/examination/exer/delExer.action",{ids:ids},function(data){
				$('#exerGrid').datagrid('reload');
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#exerEditWin').dialog('close');
	$('#exerGrid').datagrid('reload');
}

// 查看成绩单
function addtopic(id){
	window.location.href="/${projectName}/examination/scoreCollec/page.action?page=sReportCard_sch&id="+id;
}

</script>
</head>
<body>
<table id="exerGrid" title="成绩汇总" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#exerGridToolbar',collapsible:true,url:'/${projectName}/examination/exer/schExerByCollec.action',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" ></th>
			<th data-options="field:'name',width:25,sortable:true">试卷名称</th>
			<th data-options="field:'start_time',width:25,sortable:true">开始时间</th>
			<th data-options="field:'end_time',width:25,sortable:true">结束时间</th>
			<!-- <th data-options="field:'shortcut',sortable:true">分类目录</th>
			<th data-options="field:'department',sortable:true">教研室</th>
			<th data-options="field:'teacher',sortable:true">发起教师</th>
			<th data-options="field:'bankId',sortable:true">题库编号</th>
			<th data-options="field:'bankName',sortable:true">题库名</th>
			<th data-options="field:'createTime',sortable:true">创建时间</th>
			<th data-options="field:'expireTime',sortable:true">有效期</th>
			<th data-options="field:'totalNum',sortable:true">题数</th>
			<th data-options="field:'totalScore',sortable:true">总分值</th>
			<th data-options="field:'passScore',sortable:true">及格分值</th>
			<th data-options="field:'showStandard',sortable:true">是否显示标准答案</th>
			<th data-options="field:'canRepeat',sortable:true">是否可重复做题</th>
			<th data-options="field:'remark',sortable:true">备注</th> -->
			<th data-options="field:'_oper',align:'center',formatter:formatOper">操作</th>
		</tr>
	</thead>
</table>
<%-- 查询条件设定栏 --%>
<div id="exerGridToolbar">
	<div class="opline">
	</div>
	<form id="_form" class="shline">
		试卷名称：<input name='name' id="name" class='easyui-textbox' value='' data-options="iconWidth:16,
			icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
			}]"/>
		<!-- 分类目录：<input name='shortcut' id="shortcut" class='easyui-textbox' value='' data-options="iconWidth:16,
			icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
			}]"/>
		教研室：<input name='department' id="department" class='easyui-textbox' value='' data-options="iconWidth:16,
			icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
			}]"/>
		发起教师：<input name='teacher' id="teacher" class='easyui-textbox' value='' data-options="iconWidth:16,
			icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
			}]"/>
		题库编号：<input name='bankId' id="bankId" class='easyui-textbox' value='' data-options="iconWidth:16,
			icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
			}]"/>
		题库名：<input name='bankName' id="bankName" class='easyui-textbox' value='' data-options="iconWidth:16,
			icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
			}]"/> -->
		<a href="javascript:void(0)" onclick="doSearch()" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>
	</form>
</div>

<%-- 修改or添加面板 --%>
<iframe id="exerEditWin" src="/${projectName}/examination/exer/page.action?page=exer_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#exerEditButton'" title="修改练习" style="width:700px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="exerEditButton">
	<a href="javascript:void(0)" onclick="$('#exerEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#exerEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >取 消</a>
</div>
<%-- 查看面板 --%>
<%-- <iframe id="exerViewWin" src="/${projectName}/examination/exer/page.action?page=exer_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#exerViewButton'" title="查看练习" style="width:700px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="exerViewButton">
	<a href="javascript:$('#exerViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >关 闭</a>
</div> --%>
</body>
</html>