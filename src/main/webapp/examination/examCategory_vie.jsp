<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.qtrmoon.com/tags-dictionary" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>查看</title><!--examCategory-->
	<%@include file="/common/include.jsp" %>
<script>
function load(id){
	$('#_form').form('load','/${projectName}/examination/examCategory/vieExamCategory.action?toL=true&id='+id);
};
</script>
</head>
<body>
<form id="_form">
	<input type="hidden" name='id' value=""/>
	<table>
		<tr><td class='label'>父ID：</td><td><input type='text' name='pid' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>名称：</td><td><input type='text' name='name' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>类型：</td><td><input type='text' name='type' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>排序：</td><td><input type='text' name='ord' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>说明：</td><td><input type='text' name='info' class='viewbox' readonly='readonly'/></td></tr>
	</table>

</form>
</body>
</html>
