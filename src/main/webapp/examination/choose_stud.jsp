<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>老师阅卷</title>
	<%@include file="/common/include.jsp" %>
	
<!-- <link rel="stylesheet" type="text/css" href="css/style.css"/> -->
</head>
<body>

<!-- 选择学生 -->
	<div id="w" class="easyui-window" title="选择学生" inline="true"
	data-options="modal:true,iconCls:'icon-save'" style="width:1400px;height:700px;top:30px;">
		<div class="easyui-layout" fit="true">
			<div data-options="region:'north',border:false" style="height:40px;" >
				<a href="javascript:history.back();"  class="btn btn-orange"><span class="icon-undo" style="font-size:16px"></span> <span style="font-size:14px">返回</span></a>
			</div>
		
			<div region="west" split="true" style="width:45%;">
				<table id="bj" title="班级" class="easyui-datagrid"
					pagination="true" ,
					toolbar = "#winClassesToolbar",
					pageSize = "10",
					fitColumns = "true",
					fit = "true",
					fitColumns="true",
					singleSelect="true"
					data-options="collapsible:true,region:'center',border:false" style="overflow: hidden;">
					<thead>
						<tr>
							<th data-options="field:'checkbox',checkbox:true" ></th>
							<th data-options="field:'number',width:25,align:'center',sortable:true">班级号</th>
							<th data-options="field:'name',width:25,align:'center',sortable:true">班级名称</th>
							<th data-options="field:'createTime',width:25,align:'center',sortable:true">创建时间</th>
							<th data-options="field:'remark',width:25,align:'center',sortable:true">备注</th>
						</tr>
					</thead>
				</table>
			</div>
			<div region="center" border="true" style="width:55%;">
				<table id="xs" title="学生列表" class="easyui-datagrid"
					pagination="true" ,
					toolbar = "#winStuToolbar",
					pageSize = "10",
					fitColumns = "true",
					fit = "true",
					fitColumns="true",
					singleSelect="true"
					data-options="region:'center',border:false" style="overflow: hidden;">
					<thead>
						<tr>
							<th data-options="field:'checkbox',checkbox:true" ></th>
							<th data-options="field:'number',width:25,align:'center',sortable:true">学号</th>
							<th data-options="field:'cnumber',width:25,align:'center',sortable:true">班级</th>
							<th data-options="field:'name',width:25,align:'center',sortable:true">姓名</th>
							<th data-options="field:'sex',width:25,align:'center',sortable:true">性别</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
	<div id="toStudent" class="easyui-window" title="学生试卷" inline="true"
	data-options="modal:true,iconCls:'icon-save'" style="width:1200px;height:700px;top:35px;"
	closed="true">
		<div class="easyui-layout" fit="true">
			<div region="west" border="true" split="true" style="width:75%;">
					<span class="index"></span>
					
					<div class="question" name="questionDiv">
						
					</div>
					<div align="center">
						<input type="button" value="完成阅卷" onclick="teacherMark()">
					</div>
					<div>
						<input type="button" value="上一题" id="prevTopic" onclick="prevTopicX()">
						<input type="button" value="上一步" id="prevStep" onclick="prevStepX()">
						<input type="button" value="下一步" id="nextStep" onclick="nextStepX()">
						<input type="button" value="下一题" id="nextTopic" onclick="nextTopicX()">
					</div>
         	</div>
	         <div region="east" border="true" style="width:25%;">
				 <div  id="tabs-basic">
	                <ul id="train">
	       				
	                </ul>
		         </div>
	         </div>
		</div>
	</div>
<div id="winStuToolbar" style="padding:5px;height:auto">
	<div>
	<form id="searchForm2">
		 学号:<input name="number" style="width:80px">
		 姓名:<input name="name"  style="width:80px">
		<a href="javascript:void(0)" onclick="searchClass2()" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>
	</form>
	</div>
</div>

<div id="winClassesToolbar" style="padding:5px;height:auto">
	<div>
	<form id="searchForm1">
		班级号:<input name="number"  style="width:80px">
		班级名称:<input name="name" style="width:80px">
		<a href="javascript:void(0)" onclick="searchClass1()" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>
	</form>
	</div>
</div>

<script>
function searchClass1() {
	var opts = $('#bj').datagrid("options");
	opts.url = '/${projectName}/manager/classes/schClasses.action';
	$('#bj').datagrid('load',{
		name:$("#searchForm1").find("input[name='name']").val(),
		number:$("#searchForm1").find("input[name='number']").val()
	});
}

// 查询学生
function searchClass2(){
	var row = $('#bj').datagrid('getSelected');
	var opts = $('#xs').datagrid("options");
	opts.url = '/${projectName}/manager/stud/schStud.action';
	$('#xs').datagrid('load',{
		cnumber:row.number,
		name:$("#searchForm2").find("input[name='name']").val(),
		number:$("#searchForm2").find("input[name='number']").val()
	});
}
//传入参数***************
var topicArr = [];
var stepArr=new Array();

var currTopicIdx=0;
var currStepIdx=0;
var topicStepIdsMap ="";
var stepId = "";
var eid = "${eid}";
var index = 0;
var studid = "";
var esscore = {};
$(function(){
	<%-- 为分页行设置"添加"和"删除"按钮 --%>
	$('#bj').datagrid({
		url : '/${projectName}/manager/classes/schClasses.action',
		onSelect:function(rowIndex, rowData){
			var row = $('#bj').datagrid('getSelected');
			$('#xs').datagrid({
				url : '/${projectName}/examination/mark/schStudLis.action',
				queryParams:{
					classid:row.number,
					eid:eid
				},
				onSelect:function(rowIndex, rowData){
					currTopicIdx=0;
					currStepIdx=0;
					index=0;
					var row = $('#xs').datagrid('getSelected');
					studid = row.number;
					$("#toStudent").window('open');
					var ul = $("#train");
					ul.empty();
					$(".question").eq(0).empty();
					$.ajax({
				    	async: false,
				    	url:"/${projectName}/examination/exerTopic/schExerTopicTeacher.action",
				    	data: {eid:eid},
				    	success: function(data) {
				    		data = eval("(" + data + ")");
							data = data.rows;
							var html = "";
							for(var i=0; i<data.length; i++){
									
									if(i == 0) {
				   						html += "<li><a class='active' data-index="+i+" title='"+data[i].id+"' onclick='loadActive(this)'>"+data[i].title+"</a></li>";
									}else{
										html += "<li><a data-index="+i+" title='"+data[i].id+"' onclick='loadActive(this)'>"+data[i].title+"</a></li>";
									}
							}
							ul.append(html);
							initExam();
				    	}
				    });
					
				}
			});
		}
	});	
	$("#prevStep").prop("type", "hidden");
	$("#nextStep").prop("type", "hidden");
	$("#nextTopic").prop("type", "hidden");
	$("#prevTopic").prop("type", "hidden");
});

function initExam(){
	topicStepIdsMap = "";
	topicArr = [];
	var etid = "${etid}";
	stepArr=new Array();
	$.ajax({
		async: false,
		url:"/${projectName}/examination/exerTopic/getExerStepTeacherByEid.action",
		data: {eid:"${eid}"},
		success:function(data){
			topicStepIdsMap = data;
			
			$.each(topicStepIdsMap, function(key, values) {
				topicArr[index] = key;
				
				if(key == etid) {
					currTopicIdx = index;
				}
				stepArr[index] = values;
				index++;
			});
			stepId = stepArr[currTopicIdx][currStepIdx];	
		}
	});
	loadTopic();
}

function loadActive(active) {
	var li = $("#train li");
	li.each(function() {
		$(this).find('a:eq(0)').prop("class", "");
	});
	$(active).prop("class", "active");
	etid = $(active).prop("title");
	index = 0;
	saveAnswers();
	$.each(topicStepIdsMap, function(key, values) {
		topicArr[index] = key;
		if(key == etid) {
			currTopicIdx = index;
		}
		stepArr[index] = values;
		index++;
	});
	currStepIdx=0;
	stepId = stepArr[currTopicIdx][currStepIdx];
	loadTopic();
}

//加载当前题目内容
function loadTopic(){
	$("div[name='questionDiv']").empty();
	var etid=topicArr[currTopicIdx];
	var esid=stepArr[currTopicIdx][currStepIdx];
	$(".index").text("第"+(currTopicIdx+1)+"题   共"+topicArr.length+"题");
	var audioIndex = 0;
	var areaIndex = 0;
	for(var i= 0; i<stepArr[currTopicIdx].length; i++ ) {
		$.ajax({
			async:false,
			type:"post",
			url:"/${projectName}/examination/exerStep/schExerStepById.action",
			data:{stepid:stepArr[currTopicIdx][i]},
			success:function(stepData) {
				var step = eval("(" + stepData + ")");
				eid = step.eid;
				var standardAanswer = step.standardAnswer;
				score=step.score;
				$.ajax({
					async:false,
					//url: "/${projectName}/export/eid"+eid+"/" + step.etid + "_" + step.id + ".html",
					url: "/${projectName}/paper/question/appendHtmlUrl.action?eid="+eid+"&etid="+step.etid+"&etsid="+step.id,
					success: function(htmldata) {
						var html = "";
						
						
						
						//alert(htmldata);
						$("div[name='questionDiv']").append(htmldata);
						if(htmldata.indexOf("value=\"singlechoice\"") > -1 || htmldata.indexOf("value=\"multichoice\"") > -1
								|| htmldata.indexOf("value=\"judge\"") > -1){
							$("div[name='questionDiv']").find("div[class='question']").find("input[name='templateanswer']").attr("name","templateanswer"+stepArr[currTopicIdx][i]);
						}
						$.ajax({
							async:false,
							url:"/${projectName}/examination/exerAnswer/schExerAnswerByEtssId.action",
							data:{"eid":eid, "etid":etid, "esid":stepArr[currTopicIdx][i], "studNumber": studid},
							success: function(data){
								if(data!=""&&data!=undefined){
									data = eval("(" + data + ")");
								}
								var editscore = data.score;
								if(score > 0) {
									html = "<div class='score'>";
									html += "得分:<select name='score' id='select"+step.id+"'>";
									for(var j=0; j<=score; j=j+0.5) {
										html += "<option ";
										if(editscore == j) {
											html += "selected";
										}
										html +=	"  value="+j+" >"+((j+"").length > 1 ? j : (j+".0"))+"分</option>";
									}
									html += "</select></div>";
									
								}
								$("div[name='questionDiv']").find("div[class='question']").eq(i).append(html);
								$("div.operateBtn").each(function(i,obj){
									$(this).hide();
								});
								
								
								$.ajax({
									async:false,
									url:"/${projectName}/examination/exerAnswer/schExerAnswerByEtssId.action",
									data:{"eid":eid, "etid":etid, "esid":stepArr[currTopicIdx][i], "studNumber": studid},
									success: function(data){
										if(data!=""&&data!=undefined){
											data = eval("(" + data + ")");
											$("#select"+stepArr[currTopicIdx][i]).val(data.score);
											if(htmldata.indexOf("value=\"state\"") > -1 || htmldata.indexOf("value=\"repeat\"") > -1) {
												Exam.showAudioAnswer({
													
													element:$("div[name='questionDiv']").find("div[class='question']").eq(i),
													answer:data.answer,
													answerIndex:audioIndex,
													esid:stepArr[currTopicIdx][i],
													standardAanswer:standardAanswer
												});
												audioIndex++;
											}
											if(htmldata.indexOf("textarea") > -1) {
												Exam.showAnswer({
													element:$("div[name='questionDiv']").find("div[class='question']").eq(i),
													answer:data.answer,
													answerIndex:areaIndex,
													standardAanswer:standardAanswer
												});
												areaIndex++;
											}
											
											if(htmldata.indexOf("value=\"cloze\"") > -1){
												Exam.showClozeAnswer({
													element:$("div[name='questionDiv']").find("div[class='question']").eq(i),
													answer:data.answer,
													answerIndex:areaIndex,
													standardAanswer:standardAanswer
												});
												areaIndex++;
											}
											
											if(htmldata.indexOf("value=\"singlechoice\"") > -1 || htmldata.indexOf("value=\"multichoice\"") > -1
													|| htmldata.indexOf("value=\"judge\"") > -1){
												Exam.showChooseAnswer({
													element:$("div[name='questionDiv']").find("div[class='question']").eq(i),
													answer:data.answer,
													answerIndex:areaIndex,
													stepId:stepArr[currTopicIdx][i],
													standardAanswer:standardAanswer
												});
												areaIndex++;
											}
										}
										
										$("button:button").hide();
										$("button[name='questionBtn']").show();
										$("button[name='answerBtn']").show();
									}
								});
								
								
							}
						});
						
						//alert(stepArr[currTopicIdx][i])
						
						
					}
				});
			}
		});
	}
	
	if(currTopicIdx==0){
		if(topicArr.length==1){
			$("#prevTopic").attr("disabled",true);
			$("#nextTopic").attr("disabled",true);
		}else{
			$("#prevTopic").attr("disabled",true);
			$("#nextTopic").attr("disabled",false);
		}
	}
}
var Exam={
	showAudioAnswer:function(obj){
		var element=obj.element;
		var answer=obj.answer;
		var answerIndex = obj.answerIndex;
		var esid = obj.esid;
		var filename = "/" + eid +"/" + studid +"/" + topicArr[currTopicIdx] + "@@" + esid + ".wav";
		//$("audio[name='templateanswer']:eq("+answerIndex+")").find("source").prop("src", "/${projectName}/examination/exerAnswer/appendUrl.action?filename=" + filename );
		$("audio[name='templateanswer']:eq("+answerIndex+")").append("<source src='/${projectName}/examination/exerAnswer/appendUrl.action?filename=" + filename +"' type='audio/mpeg'>");
		$(".question:eq("+answerIndex+")").append("<p>标准答案："+obj.standardAanswer+"</p>");
		return answer;
	},
	showAnswer:function(obj){
		var element=obj.element;
		var answer=obj.answer;
		var answerIndex = obj.answerIndex;
		$("textarea[name='templateanswer']:eq("+answerIndex+")").val(answer);
		appendAnswer(answerIndex,obj.standardAanswer,element);
		return answer;
	},
	
	showClozeAnswer:function(obj){
		var element=obj.element;
		var answer = obj.answer
		var answerArr = answer.split("@@@");
		var answerIndex = obj.answerIndex;
		element.find("input[name='templateanswer']").each(function(i,item){
			if(answerArr.length > i){
				$(this).val(answerArr[i]);
			}
		});
		appendAnswer(answerIndex,obj.standardAanswer,element);
		return answer;
	},
	showChooseAnswer:function(obj){
		var element=obj.element;
		var answer = obj.answer;
		var stepId = obj.stepId;
		if(answer != null && typeof(answer) != "undefined" ){
			if( answer.indexOf("@@@") > -1){
				var answerArr = answer.split("@@@");
				var answerIndex = obj.answerIndex;
				for( var i = 0 ; i < answerArr.length; i++ ){
					element.find("input[name='templateanswer"+stepId+"'][value='" + answerArr[i] + "']").prop("checked", "checked");
				}
			}else{
				element.find("input[name='templateanswer"+stepId+"'][value='" + answer + "']").prop("checked", "checked");
			}
		}
		appendAnswer(answerIndex,obj.standardAanswer,element);
		return answer;
	}
	
	
}

// 显示答案
function appendAnswer(answerIndex,standardAanswer,element){
	element.append("<p name='standardAanswerP'>标准答案："+standardAanswer+"</p>");
	var len = element.find("p[name='standardAanswerP']").length;
	if(len > 1){
		element.find("p[name='standardAanswerP']").eq(1).remove();
	}
}

function prevTopicX(){
	if(currTopicIdx<=0){
	}else{
		saveAnswer();
		currTopicIdx--;
		loadTopic();
	}
	if(currTopicIdx==0){
		$("#prevTopic").attr("disabled",true);
	}
	$("#nextTopic").attr("disabled",false);
} 

function nextTopicX(){
	if(currTopicIdx>topicArr.length-1){
	}else{
		saveAnswer();
		currTopicIdx++;
		loadTopic();
	}
	if(currTopicIdx==topicArr.length-1){
		
		$("#nextTopic").attr("disabled",true);
	}
	$("#prevTopic").attr("disabled",false);
}

//保存答题***************

//新添加保存方法
function saveAnswers(){
	var paramArr = [];

	var steparr = stepArr[currTopicIdx];
	var index = 0;
	$("select[name='score'] option:selected").each(function(i, obj){
		var stepid = steparr[index];
		var param = {};
		param.esid = stepid;
		param.eid = eid;
		param.etid = topicArr[currTopicIdx];
		param.studNumber = studid;
		param.score = $(this).val();
		paramArr[index] = param;
		index ++;
	});
	
	$.ajax({
		async:false,
		type:"post",
		url:"/${projectName}/examination/exerAnswer/updExerAnswerByEtIdd.action",
		data:JSON.stringify(paramArr),
		contentType:"application/json",
		success: function(data){
			//alert("保存成功");
		}
	});
	
}


function saveAnswer(){
	var paramArr = [];
	
	var steparr = stepArr[currTopicIdx];
	var index = 0;
	$("select[name='score'] option:selected").each(function(i, obj){
		var stepid = steparr[index];
		var param = {};
		param.esid = stepid;
		param.eid = eid;
		param.etid = topicArr[currTopicIdx];
		param.studNumber = studid;
		param.score = $(this).val();
		paramArr[index] = param;
		index ++;
	});
	$.ajax({
		async:false,
		type:"post",
		url:"/${projectName}/examination/exerAnswer/updExerAnswerByEtId.action",
		data:JSON.stringify(paramArr),
		contentType:"application/json",
		success: function(data){
			//alert("保存成功");
		}
	});
}

function downAnswer(){
	var param = "number="+studid+"&eid="+eid+"&etid="+topicArr[currTopicIdx]+"&esid="+stepArr[currTopicIdx][currStepIdx];
	var str = "/${projectName}/paper/question/downZipAnswer.action?"+param;
	window.open(str);
}

function downloadZip(fileName,origiName,bankId,topicId){
	var param = "fileName="+fileName+"&origiName="+origiName+"&bankId="+bankId+"&topicId="+topicId;
	window.location.href="/${projectName}/paper/question/downloadZip.action?"+param;
}

function teacherMark() {
	saveAnswers();
	var param = {"eid":eid, "studid":studid};
	$.ajax({
		async:false,
		type:"post",
		url:"/${projectName}/examination/mark/sumTeacherMark.action",
		data: param,
		success: function(data){
			alert("阅卷成功");
			$("#toStudent").window('close');
		}
	});
}
</script>
</body>
</html>





