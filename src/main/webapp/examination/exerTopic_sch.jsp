<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>习题查询</title>
	<%@include file="/common/include.jsp" %>

<script>
$(function(){
	<%-- 为分页行设置"添加"和"删除"按钮 --%>
	var pager = $('#exerTopicGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});			
})

<%-- 执行查询，点击查询按钮时调用 --%>
function doSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#exerTopicGrid').datagrid('load',{
		bankId:$('#bankId').val(),
		topicId:$('#topicId').val(),
		type:$('#type').val(),
		level:$('#level').val(),
		profession:$('#profession').val(),
		difficulty:$('#difficulty').val(),
		author:$('#author').val(),
		beginStepId:$('#beginStepId').val()
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatOper(val,row,index){  
    return "<span onclick='updExerTopic("+val+")' style='text-decoration:underline;cursor:pointer;'>修改</span> "
    			+"<span onclick='vieExerTopic("+val+")' style='text-decoration:underline;cursor:pointer;'>查看</span>";
} 

<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
function updExerTopic(id){
	QMoveDialog($("#exerTopicEditWin"));
	$('#exerTopicViewWin').dialog('close');
	$('#exerTopicEditWin').dialog('open');
	$('#exerTopicEditWin').get(0).contentWindow.load(id);
} 

<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
function vieExerTopic(id){
	QMoveDialog($("#exerTopicViewWin"));
	$('#exerTopicEditWin').dialog('close');
	$('#exerTopicViewWin').dialog('open');
	$('#exerTopicViewWin').get(0).contentWindow.load(id);
}

<%-- 删除记录 --%>
function delExerTopic(){
	var checkedItems = $('#exerTopicGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/examination/exerTopic/delExerTopic.action",{ids:ids},function(data){
				$('#exerTopicGrid').datagrid('reload');
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#exerTopicEditWin').dialog('close');
	$('#exerTopicGrid').datagrid('reload');
}
</script>
</head>
<body>
<table id="exerTopicGrid" title="习题" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#exerTopicGridToolbar',collapsible:true,url:'/${projectName}/examination/exerTopic/schExerTopic.action',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" ></th>
			<th data-options="field:'eid',width:25,align:'center',,sortable:true">练习编号</th>
			<th data-options="field:'orderNo',width:25,align:'center',sortable:true">在练习中的序号</th>
			<th data-options="field:'bankId',width:25,align:'center',sortable:true">题库编号</th>
			<th data-options="field:'topicId',width:25,align:'center',sortable:true">考题编号</th>
			<!-- <th data-options="field:'remark',sortable:true">备注</th> -->
			<th data-options="field:'title',width:25,align:'center',sortable:true">标题</th>
			<!-- <th data-options="field:'type',sortable:true">类型</th>
			<th data-options="field:'level',sortable:true">级别</th>
			<th data-options="field:'profession',sortable:true">行业</th>
			<th data-options="field:'keyWords',sortable:true">关键字</th>
			<th data-options="field:'bookmodul',sortable:true">能力类型</th>
			<th data-options="field:'difficulty',sortable:true">难度</th>
			<th data-options="field:'author',sortable:true">作者</th>
			<th data-options="field:'creattime',sortable:true">时间</th>
			<th data-options="field:'mergeVoice',sortable:true">合并录音</th>
			<th data-options="field:'customer',sortable:true">客户资料</th>
			<th data-options="field:'talkSkill',sortable:true">话术</th>
			<th data-options="field:'totalScore',sortable:true">总分</th>
			<th data-options="field:'passScore',sortable:true">及格分</th>
			<th data-options="field:'beginStepId',sortable:true">开始步骤</th>
			<th data-options="field:'createTime',sortable:true">创建时间</th> -->
			<th data-options="field:'status',width:25,align:'center',sortable:true">答题状态</th>
			<!-- <th data-options="field:'capability',sortable:true">能力类型</th>
			<th data-options="field:'docATitle',sortable:true">文档A标题</th>
			<th data-options="field:'docBTitle',sortable:true">文档B标题</th>
			<th data-options="field:'docCTitle',sortable:true">文档C标题</th>
			<th data-options="field:'docDTitle',sortable:true">文档D标题</th>
			<th data-options="field:'docETitle',sortable:true">文档E标题</th>
			<th data-options="field:'docAText',sortable:true">文档A答案</th>
			<th data-options="field:'docBText',sortable:true">文档B答案</th>
			<th data-options="field:'docCText',sortable:true">文档C答案</th>
			<th data-options="field:'docDText',sortable:true">文档D答案</th>
			<th data-options="field:'docEText',sortable:true">文档E答案</th> -->

			<th data-options="field:'_oper',align:'center',formatter:formatOper">操作</th>
		</tr>
	</thead>
</table>
<%-- 查询条件设定栏 --%>
<div id="exerTopicGridToolbar">
	<div style="margin-bottom:5px">
		<a href="javascript:updExerTopic(0)" class="easyui-linkbutton" data-options="selected:true" iconCls="icon-add" plain="true">添加</a>
		<a href="javascript:delExerTopic()" class="easyui-linkbutton" data-options="selected:true" iconCls="icon-remove" plain="true">删除</a>
	</div>
	<form id="_form">
	题库编号：<input name='bankId' id="bankId" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	考题编号：<input name='topicId' id="topicId" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	题型：<input name='type' id="type" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	<!-- 级别：<input name='level' id="level" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/> -->
	行业：<input name='profession' id="profession" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	<!-- 难度：<input name='difficulty' id="difficulty" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	作者：<input name='author' id="author" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/> -->
	开始步骤：<input name='beginStepId' id="beginStepId" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>

	<a href="#" class="easyui-linkbutton" iconCls="icon-search" onclick="doSearch()">查询</a>
	</form>
</div>


<%-- 修改or添加面板 --%>
<iframe id="exerTopicEditWin" src="/${projectName}/examination/exerTopic/page.action?page=exerTopic_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#exerTopicEditButton'" title="修改习题" style="width:700px;height:500px;padding:5px;top:5000px" scrolling="no" frameborder="0"></iframe>
<div id="exerTopicEditButton">
	<a href="javascript:void(0)" onclick="$('#exerTopicEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#exerTopicEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >取 消</a>
</div>
<%-- 查看面板 --%>
<iframe id="exerTopicViewWin" src="/${projectName}/examination/exerTopic/page.action?page=exerTopic_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#exerTopicViewButton'" title="查看习题" style="width:700px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="exerTopicViewButton">
	<a href="javascript:$('#exerTopicViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >关 闭</a>
</div>
</body>
</html>
