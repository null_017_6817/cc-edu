<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>练习与班级关系表查询</title><!--rExerStud-->
	<%@include file="/common/include.jsp" %>
	<link rel="stylesheet" href="/${projectName}/book/js/layui/js/lib/layui2.3/css/layui.css">
	<link rel="stylesheet" type="text/css"	href="/${projectName}/book/js/layui/css/style.css">
	
	<script src="/${projectName}/book/js/layui/js/lib/layui2.3/layui.all.js"></script>
	<script>
$(function(){
	<%-- 为分页行设置"添加"和"删除"按钮 --%>
	var pager = $('#rExerStudGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});	
	// 初始化统计信息（全部考试人数，已完成阅卷人数，未完成阅卷人数）
	initCountData();
})

<%-- 执行查询，点击查询按钮时调用 --%>
function doSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#rExerStudGrid').datagrid('load',{
		eid:$('#eid').val(),
		markStatus:markStatusDict.getValue(),
		studName:$('#studName').val(),
		classid:classidDict.getValue(),
		type:$('#type').val(),
		name:$('#name').val(),
		status:$('#status').val()
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatOper(val,row,index){
	var stuNum = row.studid;
	if('${isCouldMark}'=='true'){
		return	"<a href='javascript:void(0)' onclick=\"mark('"+stuNum+"')\" class='_oper' title='阅卷' ><i class='icon-edit'></i></a> ";
	}
} 

var openIndex;
function mark(stuNum){
	openIndex = layer.open({
		type: 2,
		shadeClose: false, //点击遮罩关闭
		skin: 'layui-layer-lan', //样式类名
		closeBtn: 0, //不显示关闭按钮
		area: ['1000px', '700px'],
		anim: 5,
		title: false,
		content: '/${projectName}/examination/mark/page.action?page=exer_mark&eid=${eid}&stuNum='+stuNum,
	});
}

<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
function updRExerStud(id){
	QMoveDialog($("#rExerStudEditWin"));
	$('#rExerStudViewWin').dialog('close');
	$('#rExerStudEditWin').dialog('open');
	$('#rExerStudEditWin').get(0).contentWindow.load(id);
} 

<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
function vieRExerStud(id){
	QMoveDialog($("#rExerStudViewWin"));
	$('#rExerStudEditWin').dialog('close');
	$('#rExerStudViewWin').dialog('open');
	$('#rExerStudViewWin').get(0).contentWindow.load(id);
}

<%-- 删除记录 --%>
function delRExerStud(){
	var checkedItems = $('#rExerStudGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/examination/rExerStud/delRExerStud.action",{ids:ids},function(data){
				$('#rExerStudGrid').datagrid('reload');
			});
		}
	});
}

//显示进度条
function progressOper(val,row,index){
	var progress = row.markProgress+"%";
	var htmlstr = "<div class='easyui-progressbar progressbar easyui-fluid' style='width: 90%; height: 15px;border-radius: 9px;margin:0 auto;'>"
						+ "<div class='progressbar-value' style='width: 100%; height: 15px; line-height: 15px;'> "
							+ "<div class='progressbar-text' style='width: "+progress+"; height: 15px;line-height: 15px;background-color: #d4c20b;'>"+progress+"</div>"
						+ "</div>"
					+ "</div>";
	return htmlstr;
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#rExerStudEditWin').dialog('close');
	$('#rExerStudGrid').datagrid('reload');
}

// 
function initCountData(){
	$.ajax({
		type:"POST",
		url:'/${projectName}/examination/rExerStud/getCountData.action',
		data:{"eid":"${eid}"},
		async: false,
		dataType:'json',
		success:function(result){
			if(result.success){
				$("span[name='totalNum']").html(result.total);
				$("span[name='finishNum']").html(result.finish);
				$("span[name='ingNum']").html(result.ing);
				var exer = result.exer;
				$(".exer_title").html(exer.name);
			}
		}
	});
}

function okLayer(){
	cancelLayer();
	layer.msg("操作成功");
}
function cancelLayer(){
	layer.close(openIndex);
	initCountData();
	reloadData();
}

</script>
</head>
<body>
<table id="rExerStudGrid" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#rExerStudGridToolbar',collapsible:true,url:'/${projectName}/examination/rExerStud/schMarkRExerStud.action?eid=${eid}',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'classidCode',width:15,align:'center',sortable:true">班级号</th>
			<th data-options="field:'classNum',width:15,align:'center',sortable:true">班级名称</th>
			<th data-options="field:'studid',width:15,align:'center',sortable:true">学号</th>
			<th data-options="field:'studidName',width:15,align:'center',sortable:true">学生姓名</th>
			<th data-options="field:'status',width:15,align:'center',sortable:true">完成情况</th>
			<th data-options="field:'markProgress',width:20,align:'center',sortable:true,formatter:progressOper">评分进度</th>
			<th data-options="field:'mark',width:10,align:'center',sortable:true">得分</th>
			<th data-options="field:'_oper',align:'center',formatter:formatOper">操作</th>
		</tr>
	</thead>
</table>
<%-- 查询条件设定栏 --%>
<div id="rExerStudGridToolbar">
	<div style="margin-bottom:5px" class="exer_header">
		<p class="exer_titl">考试名称：<span class="exer_title"></span></p>
		<ul class="exer_lists">
			<li>
				<p><img src="/${projectName}/book/js/layui/img/people.png"/></p>
				<p>发卷人数：<span name="totalNum">0</span>人</p>
			</li>
			<li>
				<p><img src="/${projectName}/book/js/layui/img/ping.png"/></p>
				<p>已评分：<span name="finishNum">0</span>人</p>
			</li>
			<li>
				<p><img src="/${projectName}/book/js/layui/img/weip.png"/></p>
				<p>评分中：<span name="ingNum">0</span>人</p>
			</li>
		</ul>
	</div>
	<form id="_form">
		班级：<dc:insertList name="classid" dictId="CC_CLASSES" style="combo_normal" />
		学生：<input name='studName' id="studName" class='easyui-textbox' value='' data-options="iconWidth:16,
			icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
		}]"/>
		
		阅卷状态：<dc:insertList name="markStatus" dictId="ZD_MARK_STATUS" style="combo_normal"/>
		<a href="javascript:void(0)" onclick="doSearch()" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>
		&emsp;<a href="javascript:history.back();"  class="btn btn-orange"><span class="icon-undo" style="font-size:16px"></span> <span style="font-size:14px">返回</span></a>
	</form>
</div>


<%-- 修改or添加面板 --%>
<iframe id="rExerStudEditWin" src="/${projectName}/examination/rExerStud/page.action?page=rExerStud_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#rExerStudEditButton'" title="修改练习与班级关系表" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="rExerStudEditButton">
	<a href="javascript:void(0)" onclick="$('#rExerStudEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#rExerStudEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >取 消</a>
</div>
<%-- 查看面板 --%>
<iframe id="rExerStudViewWin" src="/${projectName}/examination/rExerStud/page.action?page=rExerStud_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#rExerStudViewButton'" title="查看练习与班级关系表" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="rExerStudViewButton">
	<a href="javascript:$('#rExerStudViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >关 闭</a>
</div>
</body>
</html>
