<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>修改练习与班级关系表</title><!--rExerStud-->
	<%@include file="/common/include.jsp" %>
<script>
<%-- 查询页点击修改时调用此方法加载记录详情 --%>
function load(id){
	$('#_form').form('load','/${projectName}/examination/rExerStud/vieRExerStud.action?id='+id);
};

<%-- 点击保存按钮时调用此方法。(注:保存按钮在查询列表页) --%>
function submitForm(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#_form').form('submit',{
      success:function(data) {
      	parent.reloadData();
      }
   });
}
</script>
</head>
<body>
<form id="_form" method="post" action="/${projectName}/examination/rExerStud/updRExerStud.action">
	<input type="hidden" name='id' id='id'/>
	<table>
		<tr><td class='label' style="width: 168px">练习编号：</td><td><input id="eid" name="eid" class="easyui-textbox" data-options="required:true,validType:['int']"/></td></tr>
		<tr><td class='label'>学生：</td><td><input id="studid" name="studid" class="easyui-textbox" data-options="required:true,validType:['int']"/></td></tr>
		<tr><td class='label'>班级：</td><td><input id="classid" name="classid" class="easyui-textbox" data-options="required:true,validType:['int']"/></td></tr>
		<tr><td class='label'>类型：</td><td><input id="type" name="type" class="easyui-textbox" data-options="required:true,validType:['int']"/></td></tr>
		<tr><td class='label'>标题：</td><td><input id="name" name="name" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label'>阅卷老师：</td><td><input id="markTeacher" name="markTeacher" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label'>阅卷状态：</td><td><input id="status" name="status" class="easyui-textbox" data-options="required:true,validType:['int']"/></td></tr>
		<tr><td class='label'>得分：</td><td><input id="mark" name="mark" class="easyui-textbox" data-options="required:true,validType:['int']"/></td></tr>
		<tr><td class='label'>备注：</td><td><input id="remark" name="remark" class="easyui-textbox" data-options="required:true"/></td></tr>
	</table>

</form>
</body>
</html>
