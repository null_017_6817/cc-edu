<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>背景信息</title>
	<%@include file="/common/include.jsp" %>
	<link rel="stylesheet" href="/${projectName}/book/js/layui/js/lib/layui2.3/css/layui.css">
	<link rel="stylesheet" href="/${projectName}/book/js/layui/fonts/font.css">
	<link rel="stylesheet" href="/${projectName}/book/js/layui/css/common.css">
	<link rel="stylesheet" href="/${projectName}/book/js/layui/css/style.css">
	
	<script type="text/javascript" charset="utf-8" src="/${projectName}/ueditor/ueditor.config.js"></script>
	<script type="text/javascript" charset="utf-8" src="/${projectName}/ueditor/ueditor.all.js"> </script>
	<script type="text/javascript" charset="utf-8" src="/${projectName}/ueditor/lang/zh-cn/zh-cn.js"></script>
	
<script>
$(function(){
	var id ="${param.id}";
	initUE('remark',id);
})

function initTopic(id){
	$.ajax({
		url : "/${projectName}/examination/exer/schExerById.action" , 
		type : "post" , 
		data : {id:id} ,
		success : function(result) {
			var jasonData = eval("("+result+")");
			console.log("ssss"+jasonData);
			UE.getEditor('remark').setContent(jasonData.answerReq);
		}
	});
}

//初始化富文本按钮
function initUE(uid,id){
		// 标准答案编辑器
	var ue = UE.getEditor(uid, {
		toolbars : [ [ 
		'simpleupload','source', /*源代码*/'undo', /*撤销*/'redo', /*重做*/'formatmatch', /*格式刷*/'bold', /*加粗*/
		'indent', /*首行缩进*/'italic', /*斜体*/'underline', /*下划线*/'strikethrough', /*删除线*/'time', /*时间*/
		'date', /*日期*/'justifyleft', /*居左对齐*/'justifyright', /*居右对齐*/'justifycenter', /*居中对齐*/'justifyjustify', /*两端对齐*/
		 /*全屏*/'autotypeset', /*自动排版*/'touppercase', /*字母大写*/'tolowercase', /*字母小写*/'forecolor', /* 字体颜色*/
		'rowspacingtop',/*段前*/'rowspacingbottom',/*段前*/'lineheight',/*行间距*/
		'fontfamily', /* 字体*/'fontsize', /* 字体大小*/'inserttable', /*插入表格*/'insertrow', /*前插入行*/'insertcol', /*前插入列*/
		'mergeright', /*右合并单元格*/'mergedown', /*下合并单元格*/'deleterow', /*删除行*/'deletecol', /*删除列*/'splittorows', /*拆分成行*/
		'splittocols', /*拆分成列*/'splittocells', /*完全拆分单元格*/'deletecaption', /*删除表格标题*/'inserttitle', /*插入标题*/
		'mergecells', /*合并多个单元格*/'deletetable', /*删除表格*/'backcolor', /*背景色*/'fullscreen', //全屏
		] ] , autoHeightEnabled : false , autoFloatEnabled : true , elementPathEnabled : false
	});
	
	setTimeout(function (){
		initTopic(id);
	}, 500);
}
//绑定富文本图片上传方法
UE.Editor.prototype._bkGetActionUrl = UE.Editor.prototype.getActionUrl;  
UE.Editor.prototype.getActionUrl = function(action){  
	if(action == 'uploadimage' || action == 'uploadscrawl' || action == 'uploadimage'){  
		return '/${projectName}/uploaditemimage.action';  
	}else{  
		return this._bkGetActionUrl.call(this, action);  
	}
}

// 保存数据
function saveData(id){
	var answerReq = UE.getEditor('remark').getContent();
	
	$.ajax({
		url : "/${projectName}/examination/exer/updAnswerReq.action" , 
		type : "post" , 
		dataType : "json" ,
		data : {"id":id,"answerReq":answerReq} ,
		success : function(result) {
			parent.closeAnswerReqDia(result.msg);
		}
	});
}

</script>
</head>
<body style="background-color: #F2F2F2;">
	<script id='remark' type='text/plain' style='width:100%;height:350px;'></script>
</body>
</html>




