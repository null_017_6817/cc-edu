<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>背景信息</title>
	<%@include file="/common/include.jsp" %>
	<link rel="stylesheet" href="/${projectName}/book/js/layui/js/lib/layui2.3/css/layui.css">
	<link rel="stylesheet" href="/${projectName}/book/js/layui/fonts/font.css">
	<link rel="stylesheet" href="/${projectName}/book/js/layui/css/common.css">
	<link rel="stylesheet" href="/${projectName}/book/js/layui/css/style.css">
	
	<script type="text/javascript" charset="utf-8" src="/${projectName}/ueditor/ueditor.config.js"></script>
	<script type="text/javascript" charset="utf-8" src="/${projectName}/ueditor/ueditor.all.js"> </script>
	<script type="text/javascript" charset="utf-8" src="/${projectName}/ueditor/lang/zh-cn/zh-cn.js"></script>
	
<script>
$(function(){
	var id ="${param.id}";
	initTopic(id);
})

function initTopic(id){
	$.ajax({
		url : "/${projectName}/examination/exer/schExerById.action" , 
		type : "post" , 
		data : {id:id} ,
		success : function(result) {
			var jasonData = eval("("+result+")");
			$("div[name='content']").html(jasonData.answerReq);
		}
	});
}

</script>
</head>
<body style="background-color: #ffffff;align:center;">
	<div name="content" style="padding:18px;"></div>
</body>
</html>




