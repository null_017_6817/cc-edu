<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>试卷预览</title>
	<%@include file="/common/include.jsp" %>
	<link rel="stylesheet" type="text/css"	href="/${projectName}/book/css/common.css" />
	<link rel="stylesheet" type="text/css"	href="/${projectName}/book/css/fonts.css" />
	<link rel="stylesheet" type="text/css"	href="/${projectName}/book/css/style.css" />
	<script type="text/javascript"	src="/${projectName}/book/js/sweetAlert/sweetalert2.min.js"></script>
	<link rel="stylesheet" type="text/css"	href="/${projectName}/book/js/sweetAlert/sweetalert2.min.css" />
	<script type="text/javascript" src="/${projectName}/book/js/lib/jquery-1.9.1.min.js"></script>
	<script type="text/javascript"	src="/${projectName}/examination/templateView/js/showStep.js"></script>
	<style>
	
	span[name='stepTitleNum']{
		width: 20px;
		height: 20px;
		background-color: #38de38;
		display: block;
		text-align: center;
		font-size: 10px;
		margin-left: 0px;
		border-radius: 6px;
		vertical-align: middle;
		line-height: 19px;
	}
	
	</style>
<script>
/* var sort="ABCDEFGHIJKLMNOPQRSTUVWXYZ"; */
$(function(){
	var eit = "${exerId}";// 获取试卷ID
	initExer(eit);
})


function initExer(eid){
	$("#_form").html("");
	$.ajax({
		type:"POST",
		url:'/${projectName}/examination/exer/previewExer.action',
		data:{"id":eid},
		 async:false, 
		dataType:'json',
		success:function(result){
			if(result.success){
				var exer = result.exer;// 试卷信息
				if(exer != null ){
					var exerName = exer.name;
					var exerTeacher = exer.teacherName;
					var startTime = exer.startTime;
					var endTime = exer.endTime;
					$("h4[name='exerName']").html(exerName);
					/* $("li[name='exerTimeRange']").html(startTime+"--"+endTime); */
					$("li[name='exerTeacher']").html("创建者："+exerTeacher);
					
				}
				var diff = result.diff;
				$("li[name='exerTimeDiff']").html("共："+diff+"分钟");
				
				var topicTypeMap = result.topicTypeMap;
				var stepTypeMap = result.stepTypeMap;
				
				var topics = result.topics;// 题目集合
				var steps = result.steps;// 步骤集合
				if(topics != null){
					for( var i = 0; i < topics.length; i++ ){
						var topic = topics[i];
						
						var topicTypeName = topicTypeMap[topic.type];
						if(topicTypeName != "" && topicTypeName != null && typeof(topicTypeName) != "undefined"){
							topicTypeName = "题型【"+topicTypeName+"】";
						}else{
							topicTypeName = '';
						}
						
						var topicDiv = "<div id='"+topic.id+"'>"
											+"<h3>"+"第【"+(i+1)+"】题&nbsp;&nbsp;"+topicTypeName+" &nbsp;&nbsp;"+topic.title+"</h3>"
											+"<div id='viewStep"+topic.id+"' class='viewStep'></div>"
										+"</div>";
						$("#_form").append(topicDiv);
						$("#_form").append("<br>");
						showStepList(steps[topic.id],topic.id);
					}
				}
			}else{
				alert(result.msg);
			}
		}
	});
}

// 拼接步骤
function appendStep(elementId){
	$("#viewStep"+elementId).html("");
	if(stepData != null){
		for(var i = 0; i < stepData.length; i++){
			stepData[i].find("span[name='stepTitleNum']").parents("table").attr("class","");
			stepData[i].find("span[name='stepTitleNum']").html((i+1));
			$("#viewStep"+elementId).append(stepData[i]);
			$("#viewStep"+elementId).append("<hr style='border: 1px dashed #cdcfd0;'>");
		}
	}
	
	$("#viewStep"+elementId).find("tr[name='downStudTr']").each(function(){
		$(this).hide();
	});
	
	$("#viewStep"+elementId).find("audio").each(function(){
		var src = $(this).find("source").attr("src");
		if( src != null && src != '' && typeof(src) != "undefined" ){
			if(src.indexOf('.wav') != -1 || src.indexOf('.WAV') != -1 
					|| src.indexOf('.m4a') != -1 || src.indexOf('.M4A') != -1
					|| src.indexOf('.mp3') != -1 || src.indexOf('.MP3') != -1){
				$(this).parent("p").parent("td").find("button[name='audButton']").trigger("click");
			}
		}
	}); 
	$("#viewStep"+elementId).find("button").hide().next("br").remove();
	stepData= [];
}

//下载zip
function downloadZip(fileName,origiName,bankId,topicId){
	var param = "fileName="+fileName+"&origiName="+origiName+"&bankId="+bankId+"&topicId="+topicId;
	window.open("/${projectName}/paper/question/downloadZip.action?"+param);
}


// 展开关闭效果
function showP(t){
	var txt = $(t).text();
	var newTxt = "";
	if(txt == '展开'){
		newTxt = "关闭"
	}else{
		newTxt = "展开"
	}
	$(t).text(newTxt);
	$(t).next("br").next('p').toggle(300);
}

</script>
</head>
<body>
	<div class="wrap">
		<jsp:include page="../book/hy_header.jsp" />
		<div class="banner bg height">
			<div class="inner padding">
				<img src="/${projectName}/book/img/exampreview.png" />
				<div class="examtTime">
					<h4 name="exerName" ></h4>
					<ul>
						<li name="exerTeacher"></li>
						<!-- <li name="exerTimeRange"></li> -->
						<li name="exerTimeDiff"></li>
					</ul>
				</div>
			</div>
		</div>

		<div class="main bg" style="padding: 0;">
			<div class="inner">
				<div class="tabsBox clearfix" id="tabs-basic">
					<div class="tabsInfo">
						<div class="con tab-content-active">
							<div id="_form" style="width: 100%;min-height:700px;padding: 20px;margin-bottom:120px;">
							</div>
						
							<div id="noData" style="text-align:center;display:none;height:500px;padding-top: 50px;">
								<img height="350" width="320" src="/${projectName}/imgs/common/nodata.png" />
							</div>
						</div>
					</div>
				</div>
			</div>
		<jsp:include page="../book/_footer.jsp" />
		</div>
	
		<div style="display:none;">
			<!-- 单选题(语音) -->
			<jsp:include page="./templateView/select-temp.jsp" />
			<!-- 选择题(语音) -->
			
			<!-- 多选题(语音) -->
			<jsp:include page="./templateView/selects-temp.jsp" />
			<!-- 选择题(语音) -->
			
			<!-- 听打录入 -->
			<jsp:include page="./templateView/cloze-temp.jsp" />
			<!-- 听打录入 -->
			
			<!-- 填空题 -->
			<jsp:include page="./templateView/clozed-temp.jsp" />
			<!-- 填空题 -->
			
			<!-- 问答题（语音） -->
			<jsp:include page="./templateView/answer-temp.jsp" />
			<!-- 问答题（语音） -->
			
			<!-- 应答题（语音） -->
			<jsp:include page="./templateView/state-temp.jsp" />
			<!-- 应答题（语音） -->
			
			<!-- 语音提示题（语音） -->
			<jsp:include page="./templateView/voice-temp.jsp" />
			<!-- 语音提示题（语音） -->
			
			<!-- 复读陈述题（语音） -->
			<jsp:include page="./templateView/repeat-temp.jsp" />
			<!-- 复读陈述题（语音） -->
										
			<!-- 判断题(语音) -->
			<jsp:include page="./templateView/judge-temp.jsp" />
			<!-- 判断题(语音) -->
			
		</div>
	</div>
</body>
</html>
