<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>习题查询</title>
	<%@include file="/common/include.jsp" %>

<script>
$(function(){
	<%-- 为分页行设置"添加"和"删除"按钮 --%>
	var pager = $('#exerTopicGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});			
})

<%-- 执行查询，点击查询按钮时调用 --%>
function doSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#exerTopicGrid').datagrid('load',{
		bankId:$('#bankId').val(),
		topicId:$('#topicId').val(),
		type:$('#type').val(),
		level:$('#level').val(),
		profession:$('#profession').val(),
		difficulty:$('#difficulty').val(),
		author:$('#author').val(),
		beginStepId:$('#beginStepId').val()
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatOper(val,row,index){
	return "<a href='/${projectName}/examination/exerTopic/page.action?page=exerTopicExam_vie&id=" + val 
    				+ "&eid=" + row.eid
    				+ " '>开始考试</a>";
} 

</script>
</head>
<body>
<table id="exerTopicGrid" title="习题" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#exerTopicGridToolbar',collapsible:true,url:'/${projectName}/examination/exerTopic/schExerTopic.action',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" ></th>
			<th data-options="field:'eid',width:25,align:'center',sortable:true">练习编号</th>
			<th data-options="field:'orderNo',width:25,align:'center',sortable:true">在练习中的序号</th>
			<th data-options="field:'bankId',width:25,align:'center',sortable:true">题库编号</th>
			<th data-options="field:'topicId',width:25,align:'center',sortable:true">考题编号</th>
			<!-- <th data-options="field:'remark',sortable:true">备注</th> -->
			<th data-options="field:'title',width:25,align:'center',sortable:true">标题</th>
			<!-- <th data-options="field:'type',sortable:true">类型</th>
			<th data-options="field:'level',sortable:true">级别</th>
			<th data-options="field:'profession',sortable:true">行业</th>
			<th data-options="field:'keyWords',sortable:true">关键字</th>
			<th data-options="field:'bookmodul',sortable:true">能力类型</th>
			<th data-options="field:'difficulty',sortable:true">难度</th>
			<th data-options="field:'author',sortable:true">作者</th>
			<th data-options="field:'creattime',sortable:true">时间</th>
			<th data-options="field:'mergeVoice',sortable:true">合并录音</th>
			<th data-options="field:'customer',sortable:true">客户资料</th>
			<th data-options="field:'talkSkill',sortable:true">话术</th>
			<th data-options="field:'totalScore',sortable:true">总分</th>
			<th data-options="field:'passScore',sortable:true">及格分</th>
			<th data-options="field:'beginStepId',sortable:true">开始步骤</th>
			<th data-options="field:'createTime',sortable:true">创建时间</th> -->
			<th data-options="field:'status',width:25,align:'center',sortable:true">试卷状态</th>
			<!-- <th data-options="field:'statusReady',sortable:true">是否答完</th> -->
			<!-- <th data-options="field:'capability',sortable:true">能力类型</th>
			<th data-options="field:'docATitle',sortable:true">文档A标题</th>
			<th data-options="field:'docBTitle',sortable:true">文档B标题</th>
			<th data-options="field:'docCTitle',sortable:true">文档C标题</th>
			<th data-options="field:'docDTitle',sortable:true">文档D标题</th>
			<th data-options="field:'docETitle',sortable:true">文档E标题</th>
			<th data-options="field:'docAText',sortable:true">文档A答案</th>
			<th data-options="field:'docBText',sortable:true">文档B答案</th>
			<th data-options="field:'docCText',sortable:true">文档C答案</th>
			<th data-options="field:'docDText',sortable:true">文档D答案</th>
			<th data-options="field:'docEText',sortable:true">文档E答案</th> -->

			<th data-options="field:'_oper',align:'center',formatter:formatOper">操作</th>
		</tr>
	</thead>
</table>
</body>
</html>
