<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="../book/css/fonts.css"/>
	<link rel="stylesheet" type="text/css" href="../book/css/style.css"/>
	<script type="text/javascript" src="/${projectName}/book/js/lib/jquery-1.9.1.min.js"></script>
	<script src="/${projectName}/common/artDialogs/artDialog.source.js?skin=default" type="text/javascript"></script>
	<script src="/${projectName}/common/artDialogs/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script type="text/javascript" src="/${projectName}/book/js/jqueryForm/jquery.form.js"></script>
	<script type="text/javascript" src="/${projectName}/js/record/HZRecorder.js"></script>
		
<style>
/* .question input{
	border: 1px solid #e5e5e5;
	outline: none;
} */
input[name='templateanswer']{
	border: 1px solid #e5e5e5;
	outline: none;
	autocomplete:off;
}


</style>

</head>
<body>

<form id="_form" style="font-size:16px">
	<div class="index"></div>
		
	<div class="question" style="height:300px;overflow-y: overlay;"></div>
	
	<div class="answer"></div>
	
	<div class="operateBtn">
		<div class="left" >
			<input type="button" value="上一步" id="prevStep" onclick="prevStepX()" />
			<input type="button" style="background-color: #3bcd80;" value="下一步" id="nextStep" onclick="nextStepX()" />
			<input type="button" style="background-color: #bcccc3;;" value="背景" id="bj" onclick="showBj()" />
		</div>
		<div class="right" >
			<input type="button" value="上一题" id="prevTopic" onclick="prevTopicX()" />
			<input type="button" value="下一题" id="nextTopic" onclick="nextTopicX()" />
		</div>
	</div>
	<div class="operateBtn">
		<input type="button" name="autoMarkBtn" class="sub" value="交卷" onclick="autoMark()" />
		<input type="button" class="sub" style="display:none;" name="saveStuAnsBtn"  value="保存" onclick="saveAnswerAndHasDone()">
	</div>	
</form>
<script>

//上传录音
var recorder;

//传入参数***************
var topicArr = [];
var stepArr=new Array();

var currTopicIdx=0;
var currStepIdx=0;
var topicStepIdsMap ="";
var stepId = "";
var index = 0;

$(function(){
	if("exam"=="${param.type}"){
		initExam();
		$("input[name='autoMarkBtn']").show();
		//$(".sub").attr("value","交卷")
	}else{
		//$("#prevTopic").prop("type", "hidden");
		//$("#nextTopic").prop("type", "hidden");
		initExer();
		$("input[name='autoMarkBtn']").hide();
		//$(".sub").attr("value","保存")
		
	}
});
function initExam(){
	$.ajax({
		async: false,
		url:"/${projectName}/examination/exerTopic/getExerStepByEid.action",
		data: {eid:"${param.eid}"},
		success:function(data){
			topicStepIdsMap = data;
			for (var i = 0; i < topicStepIdsMap.length; i++) {  
				$.each(topicStepIdsMap[i], function(key, values) {
					topicArr[index] = key;
					
					if(key == "${param.etid}") {
						currTopicIdx = index;
					}
					stepArr[index] = values;
					index++;
				});
				stepId = stepArr[currTopicIdx][currStepIdx];
			}
				
			loadTopic();
		}
	});
}
function initExer(){
	$.ajax({
		async: false,
		url:"/${projectName}/examination/exerTopic/getExerStepByBook.action",
		data: {bookId:"${param.bookId}"},
		success:function(data){
			topicStepIdsMap = data;
			
			$.each(topicStepIdsMap, function(key, values) {
				topicArr[index] = key;
				
				if(key == "${param.etid}") {
					currTopicIdx = index;
				}
				stepArr[index] = values;
				index++;
			});
			stepId = stepArr[currTopicIdx][currStepIdx];	
			loadTopic();
		}
	});
}
function loadActive(etid) {
	index = 0;
	$.each(topicStepIdsMap, function(key, values) {
		topicArr[index] = key;
		if(key == etid) {
			currTopicIdx = index;
		}
		stepArr[index] = values;
		index++;
	});
	currStepIdx=0;
	stepId = stepArr[currTopicIdx][currStepIdx];
	loadTopic();
}

//加载当前题目内容
function loadTopic(){
	var etid=topicArr[currTopicIdx];
	var esid=stepArr[currTopicIdx][currStepIdx];
	
	$(".index").text("第"+(currTopicIdx+1)+"题/共"+topicArr.length+"题    第"+(currStepIdx+1)+"步/共"+stepArr[currTopicIdx].length+"步");
	
	appendOpt();
	
	$.get("/${projectName}/examination/exerStep/schExerStepById.action",{stepid:esid},function(stepData){
		var step = eval("(" + stepData + ")");
		eid = step.eid;
		var stepType = step.type;
		
		$.get("/${projectName}/paper/question/appendHtmlUrl.action?eid="+eid+"&etid="+step.etid+"&etsid="+step.id+"&stepType="+stepType,function(data){
			$(".question").eq(0).html(data);
		
			// 判断录音播放器src 属性的值，如果不为空且不存在appendUrl.action说明：此步骤不是听打录入题，则录音可以拖拽进度条
			// 如果是听打录入题，则不允许拖拽录音进度条（java方法直接返回文件流是不能拖拽录音的进度条的）
			var sr = $(".question-Q").find("audio").find("source").attr("src");
			if(sr != null && typeof(sr) != "undefined" && sr.indexOf("appendUrl.action") < 0){
				var aa = document.location.host;
				var bb = aa.split(":")[0];
				var nginxHost = "http://"+bb+":9801/"+sr;
				$(".question-Q").find("audio").find("source").attr("src",nginxHost);
			}
				
			$.ajax({
				url:"/${projectName}/examination/exerAnswer/schExerAnswerByEtsId.action",
				data:{"eid":eid, "etid":etid, "esid":esid},
				success: function(data){
					if(data!=""&&data!=undefined){
						data = eval("(" + data + ")");
						Exam.showAnswer({
							element:$('.question').eq(0),
							answer:data.answer,
							studid:data.studNumber
						});
						
						var ori = data.fileOriginalName
						showStuZip(ori);
					}
				}
			});
			
			// 是否显示正确答案
			var showStandAnswer = "";
			$.ajax({
				type:"POST",
				url:'/${projectName}/examination/exer/schExerById.action',
				data:{'id':eid},
				async: false,
				dataType:'json',
				success:function(result){
					if(result != null){
						showStandAnswer = result.showStandard;
					}
				}
			});
			
			// 是:显示，否:不显示
			if(showStandAnswer == "是"){
				var showIt = "";
				var it = step.standardAnswer;
				showIt = it;
				if(it == "true" ){
					showIt = "是"
				}
				if(it == "false" ){
					showIt = "否"
				}
				$(".answer").eq(0).html("<span>正确答案</span><hr class='hr-dashed'>"+showIt);
			}
			
			if(currTopicIdx==0){
				if(topicArr.length==1){
					$("#prevTopic").attr("disabled",true);
					$("#nextTopic").attr("disabled",true);
				}else{
					$("#prevTopic").attr("disabled",true);
					$("#nextTopic").attr("disabled",false);
				}
			}
			if(currStepIdx==0){
				if(stepArr[currTopicIdx].length==1){
					$("#prevStep").attr("disabled",true);
					$("#nextStep").attr("disabled",true);
					var display =$("input[name='autoMarkBtn']").css('display');
					if(display == "none"){
						$("input[name='saveStuAnsBtn']").show();
					}else{
						$("input[name='saveStuAnsBtn']").hide();
					}
				}else{
					$("#prevStep").attr("disabled",true);
					$("#nextStep").attr("disabled",false);
					var display =$("input[name='autoMarkBtn']").css('display');
					if(display == "none"){
						$("input[name='saveStuAnsBtn']").show();
					}else{
						$("input[name='saveStuAnsBtn']").hide();
					}
				}
			}
			
			/* ########################
			 * 防止前面步骤的录音到下一个步骤来播放
			 * ########################
			 */ 
			$("audio").each(function(){
				var src = $(this).attr("src");
				if(src != null && typeof(src) != "undefined" && src.startsWith("blob")){
					$(this).attr("src","");
				}
 			});
		});
	});
}

var Exam={
	getAnswer:function(obj){
		var element=obj.element;
		var type=element.find("input[name='type']");
		var answer = "";
		if(type.val()=="singlechoice"){
			answer=$("input[name='templateanswer']:checked").val();
		}else if(type.val()=="multichoice"){
			$("input[name='templateanswer']:checked").each(function(){
				answer = answer + $(this).val() + "@@@";
			});
			answer = answer.substring(0, answer.length - 3)
		}else if(type.val() == "judge") {
			answer=$("input[name='templateanswer']:checked").val();
		}else if(type.val() == "answer") {
			answer=$("textarea[name='templateanswer']").val();
		}else if(type.val() == "cloze") {
			$("input[name='templateanswer']").each(function(){
				if($(this).val() == "" ){
					$(this).val() == " ";
				}
				answer = answer + $(this).val() + "@@@";
			});
			answer = answer.substring(0, answer.length - 3)
		}
		return answer;
	},
	showAnswer:function(obj){
		var element=obj.element;
		var answer=obj.answer;
		var studid = obj.studid;
		var type=element.find("input[name='type']");
		if(type.val() == "state" || type.val() == "repeat") {
			var filename = "/" + eid +"/" + studid +"/" + topicArr[currTopicIdx] + "@@" + stepArr[currTopicIdx][currStepIdx] + ".wav";
			
			$.ajax({
				type:"POST",
				url:'/${projectName}/examination/exerAnswer/appendUrl.action',
				data:{"studid":studid,"eid":eid,"etid":topicArr[currTopicIdx],"esid":stepArr[currTopicIdx][currStepIdx]},
				async: false,
				dataType:'json',
				success:function(result){
					if(result.success){
						path = result.path;
						$("#record").append("<source src='/${projectName}/"+path+"' type='audio/mpeg'>");
					}
				}
			});
		}
		if(answer == "" || answer == undefined) {
			return;
		}
		if(type.val()=="singlechoice"){
			element.find("input[name='templateanswer'][value='"+answer+"']").attr("checked", true);
		}else if(type.val()=="multichoice"){
			answer = answer.split("@@@");
			$.each(answer, function(i, val){
				element.find("input[name='templateanswer'][value='"+val+"']").attr("checked", true);
			});
		}else if(type.val() == "judge") {
			element.find("input[name='templateanswer'][value='"+answer+"']").attr("checked", true);
		}else if(type.val() == "answer") {
			element.find("textarea[name='templateanswer']").val(answer);
		}else if(type.val() == "cloze") {
			answer = answer.split("@@@");
			$("input[name='templateanswer']").each(function(i, obj){
				$(this).val(answer[i]);
			});
		}
		return answer;
	}
}

// 上一步
function prevStepX(){
	if(currStepIdx<=0){
		
	}else{
		saveAnswer(topicArr[currTopicIdx],stepArr[currTopicIdx][currStepIdx]);
		currStepIdx--;
		loadTopic();
	}
	if(currStepIdx==0){
		$("#prevStep").attr("disabled",true);
	} 
	$("#nextStep").attr("disabled",false);
	
	recorder.empty();
} 

// 下一步
function nextStepX(){
	if(currStepIdx<stepArr[currTopicIdx].length - 1){
		saveAnswer(topicArr[currTopicIdx],stepArr[currTopicIdx][currStepIdx]);
		currStepIdx++;
		loadTopic();
	}else if(currStepIdx==stepArr[currTopicIdx].length-1){
		saveAnswer(topicArr[currTopicIdx],stepArr[currTopicIdx][currStepIdx]);
		$("#nextStep").attr("disabled",true);
	}
	$("#prevStep").attr("disabled",false);
	recorder.empty();
}

// 上一题
function prevTopicX(){
	if("exam"=="${param.type}"){
		if(currTopicIdx<=0){
		}else{
			saveAnswer(topicArr[currTopicIdx],stepArr[currTopicIdx][currStepIdx]);
			currTopicIdx--;
			currStepIdx=0;
			loadTopic();
		}
		if(currTopicIdx==0){
			$("#prevTopic").attr("disabled",true);
		}
		$("#nextTopic").attr("disabled",false);
	}else{
		saveStuAnswer();
		setTimeout(function(){ parent.preTopic(); }, 1000);
	}
	recorder.empty();
} 

// 下一题
function nextTopicX(){
	if("exam"=="${param.type}"){
		if(currTopicIdx>=topicArr.length-1){
		}else{
			var flg = saveAnswer(topicArr[currTopicIdx],stepArr[currTopicIdx][currStepIdx]);
			if(flg){
				art.dialog({
					left: 480,
					top: "35%",
					time:1,
					content: '自动保存成功'
				});
			}else{
				art.dialog({
					left: 480,
					top: "35%",
					time:1,
					content: '自动保存失败'
				});
			}
			currTopicIdx++;
			currStepIdx=0;
			loadTopic();
		}
		if(currTopicIdx==topicArr.length-1){
			
			$("#nextTopic").attr("disabled",true);
		}
		$("#prevTopic").attr("disabled",false);
	}else{
		saveStuAnswer();
		setTimeout(function(){ parent.nextTopic(); }, 1000);
	}
	recorder.empty();
}

//手动保存学生答案
function saveStuAnswer(){
	var cetid = topicArr[currTopicIdx];
	var cesid = stepArr[currTopicIdx][currStepIdx];
	var answer = Exam.getAnswer({element:$('.question').eq(0)});
	
	var user = "${CURRENT_USER}";
	if( user != null && typeof(user) != "undefined" && user != ""){
		$.ajax({
			url:"/${projectName}/examination/exerAnswer/updExerAnswerByEtsId.action",
			data:{"eid":eid, "etid":cetid, "esid":cesid, "answer": answer},
			async: false,
			success: function(data){
				art.dialog({
					left: 330,
					top: "35%",
					time:2,
					content: '保存成功'
				});
				// parent.taskAddFlg();
			},
			error: function (jqXHR, textStatus, errorThrown) {
				art.dialog({
					left: 330,
					top: "35%",
					time:2,
					content: '保存失败'
				});
			}
		});
	}else{
		art.dialog({
			left: 320,
			top: "35%",
			time:2,
			content: '请先登录!'
		});
	}
}

//手动保存学生答案及是否做完标志
function saveAnswerAndHasDone(){
	var cetid = topicArr[currTopicIdx];
	var cesid = stepArr[currTopicIdx][currStepIdx];
	var answer = Exam.getAnswer({element:$('.question').eq(0)});
	
	var user = "${CURRENT_USER}";
	if( user != null && typeof(user) != "undefined" && user != ""){
		$.ajax({
			url:"/${projectName}/examination/exerAnswer/updHasDoneByEtsId.action",
			data:{"eid":eid, "etid":cetid, "esid":cesid, "answer": answer},
			async: false,
			success: function(data){
				art.dialog({
					left: 330,
					top: "35%",
					time:2,
					content: '保存成功'
				});
				parent.taskAddFlg();
			},
			error: function (jqXHR, textStatus, errorThrown) {
				art.dialog({
					left: 330,
					top: "35%",
					time:2,
					content: '保存失败'
				});
			}
		});
	}else{
		art.dialog({
			left: 320,
			top: "35%",
			time:2,
			content: '请先登录!'
		});
	}
}

//保存答题***************
function saveAnswer(cetid, cesid){
	var flg = false;
	var answer=Exam.getAnswer({element:$('.question').eq(0)});
	$.ajax({
		url:"/${projectName}/examination/exerAnswer/updExerAnswerByEtsId.action",
		async:false, 
		data:{"eid":eid, "etid":cetid, "esid":cesid, "answer": answer},
		success: function(data){
			flg = true;
		}
	});
	return flg;
}

function autoMark() {
	//saveAnswer(topicArr[currTopicIdx],stepArr[currTopicIdx][currStepIdx]);
	var cetid = topicArr[currTopicIdx];
	var cesid = stepArr[currTopicIdx][currStepIdx];
	var answer=Exam.getAnswer({element:$('.question').eq(0)});
	$.ajax({
		url:"/${projectName}/examination/exerAnswer/updExerAnswerByEtsId.action",
		data:{"eid":eid, "etid":cetid, "esid":cesid, "answer": answer},
		success: function(data){
			
			$.ajax({
				url:"/${projectName}/examination/automark/autoMark.action",
				data:{"eid":eid},
				dataType:'json',
				success: function(data) {
					if(data.success){
						window.parent.swal({title:"交卷成功！稍后窗口将自动关闭",
							type:"success"});
						
						setTimeout("closeWindow()",3000);
						
					}else{
						window.parent.swal({title:data.msg,
							type:"error"});
					}
				}
			});
		}
	});
	
	
	
}

// 关闭当前窗口
function closeWindow(){
	parent.window.opener=null;
	parent.window.open('','_self');
	parent.window.close();
}

// 播放音频文件
function playVedio(elem) {
	var audio = $(elem).next("audio");
	audio = audio.get(0);
	var audioState = audio.paused;
	if(audioState) {
		audio.play();
	}else{
		audio.pause();
	}
}
function playVedio1(audio) {
	if(audio instanceof jQuery) {
		audio = audio.get(0);
	}
	var audioState = audio.paused;
	if(audioState) {
		audio.play();
	}else{
		audio.pause();
	}
}



// 开始录音
function startRecording(t) {
	HZRecorder.get(function(rec) {
		recorder = rec;
		recorder.start();
	});
	// 显示录音gif图标
	parent.showAudioGif();
}

// 停止录音
function stopRecording(t) {
	parent.closeAll();
	var audio = $("#record").get(0);
	if(recorder != null && recorder != undefined) {
		audio.pause();
		recorder.stop();
	}else{
		audio.pause();
	}
}

// 播放录音
function playRecording(t) {
	var audio1 = $("#record");
	if(recorder != null && recorder != undefined) {
		recorder.play(audio1.get(0));
		audio1.get(0).play();
	}else{
		audio1.get(0).pause();
		playVedio1(audio1);		
	}
}

// 上传录音文件
function uploadAudio(t) {
	recorder.upload("/${projectName}/examination/exerStep/uploadVoice.action?eid="+eid+"&etid="+topicArr[currTopicIdx]+"&esid="+stepArr[currTopicIdx][currStepIdx], function(state, e) {
		switch (state) {
		case 'uploading':
			//var percentComplete = Math.round(e.loaded * 100 / e.total) + '%';
			break;
		case 'ok':
			//alert(e.target.responseText);
			window.parent.swal({title:"上传成功",
				type:"success"});
			break;
		case 'error':
			alert("上传失败");
			break;
		case 'cancel':
			alert("上传被取消");
			break;
		}
	});
}

// 下载压缩包
function downloadZip(fileName,origiName,bankId,topicId){
	var param = "fileName="+fileName+"&origiName="+origiName+"&bankId="+bankId+"&topicId="+topicId;
	window.open("/${projectName}/paper/question/downloadZip.action?"+param);
}

//上传实操题压缩包
function uploadZip(t){
	
	saveAnswer(topicArr[currTopicIdx],stepArr[currTopicIdx][currStepIdx]);
	
	$("#handleZipUpForm").find("input[name='eid']").val(eid);
	$("#handleZipUpForm").find("input[name='etid']").val(topicArr[currTopicIdx]);
	$("#handleZipUpForm").find("input[name='esid']").val(stepArr[currTopicIdx][currStepIdx]);
	
	var btn = $(t);
	$("#handleZipUpForm").find("input[name='zipFile']").unbind();
	$("#handleZipUpForm").find("input[name='zipFile']").change(function(){
		
		var fil = $("#handleZipUpForm").find("input[name='zipFile']").val();
		if(fil != ""){
			$("#handleZipUpForm").ajaxSubmit({
				url: '/${projectName}/paper/question/uploadZipAnswer.action', 
				type: "post", 
				dataType: "json", 
				beforeSend: function(){
					// 显示loading
					showLoading();
				},
				success: function (result) {
					// 关闭loading
					closeLoading();
					if(result.success){
						alert(result.msg);
						showStuZip(result.origiName);
					}else{
						alert(result.msg);
					}
					$("#handleZipUpForm").find("input[name='zipFile']").val("");
				},
				error:function(XMLHttpRequest, textStatus, errorThrown){
					// 关闭loading
					closeLoading();
					alert("上传异常，请重试")
					$("#handleZipUpForm").find("input[name='zipFile']").val("");
				}
			});
			// 关闭loading
			// closeLoading();
		}
	});
	$("#handleZipUpForm").find("input[name='zipFile']").trigger('click');
}

// 显示背景
function showBj(){
	parent.showRemark(topicArr[currTopicIdx]);
}

// 显示学生上传的附件
function showStuZip(zipName){
	// 清除原文件名，然后再拼接
	$("button[name='stuZipBtn']").remove();
	var upBtnLen = $("button[name='upzipBtn']").length;
	var name = "";
	if(zipName != null && zipName != "" && typeof(zipName) != "undefined"){
		name = zipName
	}
	if(upBtnLen > 0){
		$("button[name='upzipBtn']").after("<button name='stuZipBtn' title='点击下载' "
				+ "type='button' style='margin-left:10px;cursor:pointer;width:300px;height:30px;background-color: #ffffff;' "
				+ "onclick='javascript:downStuZip();'>"+name+"</button>")
	}
}

// 下载学生附件
function downStuZip(){
	var eid = "${param.eid}";
	var etid = topicArr[currTopicIdx];
	var esid = stepArr[currTopicIdx][currStepIdx];
	var studNum = "${CURRENT_USER.number}";
	// 下载附件（实操题）
	window.open('/${projectName}/paper/question/downZipAnswer.action?eid='+eid+"&etid="+etid+"&esid="+esid+"&studNum="+studNum);
}

// 显示loading
function showLoading(){
	/* 调用父页面方法显示loading */
	parent.showLoading();
}

// 关闭loading
function closeLoading(){
	/* 调用父页面方法关闭loading */
	parent.closeLoading();
}

// 拼接跳转 业务系统 知识库
function appendOpt(){
	var src = "<div style='float:right;padding-right:20px;'>"
			+	"<a href='javascript:openPractice();'>业务系统</a>&nbsp;&nbsp;"
			+	"<a href='javascript:openKnowledge();'>知识库</a>"
		+	"</div>";
	$(".index").append(src);
}

function openPractice(){
	window.open("/${projectName}/book/book/page.action?page=entrance","entrance");
}

function openKnowledge(){
	window.open("/${projectName}/book/book/page.action?page=knowledge_only&bookId=1&bookRoot=1","knowledge");
}


</script>

<!-- 手动导入压缩包form表单 -->
<form id="handleZipUpForm" action="" enctype="multipart/form-data" style="display:none;">
	<input type="file" name="zipFile" value="" />
	<input type="hidden" name='eid' value="" />
	<input type="hidden" name='etid' value="" />
	<input type="hidden" name='esid' value="" />
</form>
</body>
</html>
