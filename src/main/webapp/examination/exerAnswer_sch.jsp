<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>学生答题卡查询</title>
	<%@include file="/common/include.jsp" %>

<script>
$(function(){
	<%-- 为分页行设置"添加"和"删除"按钮 --%>
	var pager = $('#exerAnswerGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});			
})

<%-- 执行查询，点击查询按钮时调用 --%>
function doSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#exerAnswerGrid').datagrid('load',{
		eid:$('#eid').val(),
		etid:$('#etid').val(),
		esid:$('#esid').val(),
		studNumber:$('#studNumber').val()
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatOper(val,row,index){  
    return "<span onclick='updExerAnswer("+val+")' style='text-decoration:underline;cursor:pointer;'>修改</span> "
    			+"<span onclick='vieExerAnswer("+val+")' style='text-decoration:underline;cursor:pointer;'>查看</span>";
} 

<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
function updExerAnswer(id){
	QMoveDialog($("#exerAnswerEditWin"));
	$('#exerAnswerViewWin').dialog('close');
	$('#exerAnswerEditWin').dialog('open');
	$('#exerAnswerEditWin').get(0).contentWindow.load(id);
} 

<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
function vieExerAnswer(id){
	QMoveDialog($("#exerAnswerViewWin"));
	$('#exerAnswerEditWin').dialog('close');
	$('#exerAnswerViewWin').dialog('open');
	$('#exerAnswerViewWin').get(0).contentWindow.load(id);
}

<%-- 删除记录 --%>
function delExerAnswer(){
	var checkedItems = $('#exerAnswerGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/examination/exerAnswer/delExerAnswer.action",{ids:ids},function(data){
				$('#exerAnswerGrid').datagrid('reload');
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#exerAnswerEditWin').dialog('close');
	$('#exerAnswerGrid').datagrid('reload');
}
</script>
</head>
<body>
<table id="exerAnswerGrid" title="学生答题卡" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#exerAnswerGridToolbar',collapsible:true,url:'/${projectName}/examination/exerAnswer/schExerAnswer.action',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" ></th>
			<th data-options="field:'eid',width:25,align:'center',sortable:true">练习编号</th>
			<th data-options="field:'etid',width:25,align:'center',sortable:true">考题编号</th>
			<th data-options="field:'esid',width:25,align:'center',sortable:true">步骤编号</th>
			<th data-options="field:'studNumber',width:25,align:'center',sortable:true">学号</th>
			<th data-options="field:'remark',width:25,align:'center',sortable:true">备注</th>
			<th data-options="field:'answer',width:25,align:'center',sortable:true">答题内容</th>
			<th data-options="field:'score',width:25,align:'center',sortable:true">步骤得分</th>

			<th data-options="field:'_oper',align:'center',formatter:formatOper">操作</th>
		</tr>
	</thead>
</table>
<%-- 查询条件设定栏 --%>
<div id="exerAnswerGridToolbar">
	<div style="margin-bottom:5px">
		<a href="javascript:updExerAnswer(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true">添加</a>
		<a href="javascript:delExerAnswer()" class="easyui-linkbutton" iconCls="icon-remove" plain="true">删除</a>
	</div>
	<form id="_form">
	练习编号：<input name='eid' id="eid" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	考题编号：<input name='etid' id="etid" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	步骤编号：<input name='esid' id="esid" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	学号：<input name='studNumber' id="studNumber" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>

	<a href="#" class="easyui-linkbutton" iconCls="icon-search" onclick="doSearch()">查询</a>
	</form>
</div>


<%-- 修改or添加面板 --%>
<iframe id="exerAnswerEditWin" src="/${projectName}/examination/exerAnswer/page.action?page=exerAnswer_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#exerAnswerEditButton'" title="修改学生答题卡" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="no" frameborder="0"></iframe>
<div id="exerAnswerEditButton">
	<a href="javascript:void(0)" onclick="$('#exerAnswerEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#exerAnswerEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >取 消</a>
</div>
<%-- 查看面板 --%>
<iframe id="exerAnswerViewWin" src="/${projectName}/examination/exerAnswer/page.action?page=exerAnswer_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#exerAnswerViewButton'" title="查看学生答题卡" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="exerAnswerViewButton">
	<a href="javascript:$('#exerAnswerViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >关 闭</a>
</div>
</body>
</html>
