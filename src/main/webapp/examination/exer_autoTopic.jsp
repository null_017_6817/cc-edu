<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>习题查询</title>
	<%@include file="/common/include.jsp" %>
<style>

.countTh{
	text-align: center;
	background: #f0f0f0;
	height: 25px;
	border: 1px white solid;
}

.countTd{
	text-align: center;
	height: 25px;
	border: 1px #f0f0f0 solid;
}
.countTdNum{
	text-align: center;
	height: 25px;
	border: 1px #f0f0f0 solid;
}
.countTdScore{
	text-align: center;
	height: 25px;
	border: 1px #f0f0f0 solid;
}
.countTd:nth-child(2){
	width:20px;
}
.countTd:nth-child(5){
	text-align: center;
}
.countTd:nth-child(6){
	text-align: center;
}
.countTd:nth-child(7){
	text-align: center;
}
.countTd:nth-child(5) .optNum{
	margin-left:0px;
}
.countTd:nth-child(6) .optNum{
	margin-left:0px;
}
.countTd:nth-child(7) .optNum{
	margin-left:0px;
}
.draggable-element {
	display: inline-block;
	width: 70px;
	height: 30px;
	background: white;
	border: 1px solid rgb(196, 196, 196);
	line-height: 30px;
	text-align: center;
	margin: 10px;
	color: rgb(51, 51, 51);
	font-size: 12px;
	cursor: move;
	-webkit-touch-callout: none; /* iOS Safari */
	-webkit-user-select: none; /* Chrome/Safari/Opera */
	-khtml-user-select: none; /* Konqueror */
	-moz-user-select: none; /* Firefox */
	-ms-user-select: none; /* Internet Explorer/Edge */
	user-select: none; /* Non-prefixed version, currently
	not supported by any browser */
}

.bankItem{
	min-width: 70px;
	display: inline-block;
	text-align: center;
	height: 20px;
	vertical-align: middle;
	background-color: #f0f0f0;
	border: 1px #6bd26b solid;
	line-height: 20px;
	font-weight: unset;
	cursor: pointer;
	-webkit-border-radius: 20px;
	margin: 5px;
}
</style>
<script type="text/javascript" src="/${projectName}/js/jquery-sortable/drag-arrange.js"></script> 
<script>
var sort="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
var stuNum = "${stuNum}";
$(function(){
	$('#exer_form').form('load','/${projectName}/examination/exer/vieExer.action?id=${exerId}');
	$('#exer_form').form({
		onLoadSuccess:function(data){ 
			// 回显题库
			var banks = data.bankIdsStr;
			if(banks != null && banks != "" && typeof(banks) != "undefined"){
				banks = eval("(" + banks + ")");
				var str = "";
				bankObjArray = new Object();
				for (var i= 0; i < banks.length; i++){
					str += "<a class='bankItem' onclick='removeBank(this);' title='点击删除此项' dataid='"+banks[i].bankId+"' dataname='"+banks[i].bankName+"' >"+banks[i].bankName+"</a>";
					bankObjArray[banks[i].bankId] = banks[i].bankName;
				}
				$("div[name='bank']").html(str);
				countTopicNum();
			}
			
			// 回显排序
			var typeSort = data.typeSortStr;
			if( typeSort != null && typeSort != "" && typeof(typeSort) != "undefined" ){
				$("#sortDiv").html("");
				var typeSortArray = typeSort.split(',');
				for( var i = 0; i < typeSortArray.length; i++ ){
					var item = $("#sortTemp").find("div[data-type='"+typeSortArray[i]+"']").clone()
					$("#sortDiv").append(item);
				}
			}
			
			// 拖拽排序
			if (stuNum == 0) {
				$('.draggable-element').arrangeable();
			}
		
			
			// 回显选择数量
			var numParamStr = data.numParamStr;
			if(numParamStr != null){
				numParamStr = eval("(" + numParamStr + ")");
				for (var item in numParamStr){
					if(numParamStr[item] == "" || numParamStr[item] == null || typeof(numParamStr[item]) == "undefined"){
						numParamStr[item] = 0;
					}
					$("input[name='"+item+"']").val(numParamStr[item]); //根据id选中行   
				}
			}
			
			// .optNum绑定change方法
			$(".optNum").change(function(){
				var tdTotalNum = $(this).parent().siblings(".countTdNum").html();
				var tdTotalScore = $(this).parent().siblings(".countTdScore").html();
				console.log(tdTotalNum);
				if (stuNum > 0) {
					var othTdNum = 0;
					$(this).parent().siblings(".countTd").each(function(i, item) {
						var itemName = $(item).find("input").attr("name");
						var itemVal = $(item).find("input").val();
						var itemValue = parseInt(itemVal);
						if (itemName != undefined) {
							if (itemName.indexOf("judgeNum") != -1 || itemName.indexOf("singleNum") != -1
									|| itemName.indexOf("multiNum") != -1 || itemName.indexOf("clozedNum") != -1
									|| itemName.indexOf("answerNum") != -1) {
								othTdNum += itemValue;
							}
						}
					});
					var thisName = $(this).attr("name");
					var thisValue = 0;
					if (thisName.indexOf("judgeNum") != -1 || thisName.indexOf("singleNum") != -1
							|| thisName.indexOf("multiNum") != -1 || thisName.indexOf("clozedNum") != -1
							|| thisName.indexOf("answerNum") != -1) {
						thisValue = parseInt(tdTotalNum) - othTdNum;
					}
					if (thisName.indexOf("singleScore") != -1 || thisName.indexOf("multiScore") != -1
							|| thisName.indexOf("judgeScore") != -1 || thisName.indexOf("clozedScore") != -1 
							|| thisName.indexOf("answerScore") != -1) {
						if (parseInt(tdTotalNum) == 0) {
							thisValue = 0;
						} else {
							thisValue = parseInt(tdTotalScore) / parseInt(tdTotalNum);
						}
					}
					$(this).val(thisValue);
					$.messager.alert('提示','已发卷试卷不允许重新维护','warning');
					return;
				}
				var inputNum = $(this).val();
				var spanNum = $(this).next("span").html();
				var spanNum = parseInt(spanNum);
				var inputNum = parseInt(inputNum);
				if(inputNum > spanNum){
					$(this).val(spanNum)
				}
				$(this).tooltip('destroy');
				countChooseNum();
			});
			
			// 汇总选择的数量
			countChooseNum();
		}
	});
	if (stuNum == 0) {
		$('.draggable-element').arrangeable();
	}
	// $('.draggable-element').arrangeable();
})

function countChooseNum(){
	var totalNum = 0
	$(".optNum").each(function(i,item){
		var flg = $(item).attr("data-flg");
		if(flg != "score"){
			var num = $(this).val();
			if(num == null || num == "" || typeof(num) == "undefined"){
				num = 0;
			}
			totalNum += parseInt(num);
		}
	});
	$("span[name='chooseNumSpan']").html(totalNum);
	countByType();
}

// 汇总总数
function countTotalNum(){
	var totalNum = 0
	$(".optSP").each(function(i,item){
		var num = $(this).html();
		var inp = $(this).prev("input[type=number]");
		if(num > 0){
			totalNum += parseInt(num);
		} 
	});
	$("span[name='totalNumSpan']").html(totalNum);
}

// 根据不同类型统计数量
function countByType(){
	$(".countTdNum").each(function(i,item){
		var totalNum = 0;
		$(this).parent("tr").find("input[class='optNum']").each(function(n,obj){
			var flg = $(obj).attr("data-flg");
			if(flg != "score"){
				var num = $(obj).val();
				if(num > 0){
					totalNum += parseInt(num);
				}
			}
		});
		$(this).html(totalNum);
		
		var typeScore = 0;
		var score = $(this).parent("tr").find("input[data-flg='score']").val();
		var typeNum = $(this).parent("tr").find("td[class='countTdNum']").html();
		if(score != null && typeof(score) != "undefined" && typeNum != null && typeof(typeNum) != "undefined" ){
			typeScore = parseInt(typeNum)*parseInt(score);
		}
		$(this).parent("tr").find("td[class='countTdScore']").html(typeScore);
		
		var allTypeScore = 0;
		$("td[class='countTdScore']").each(function(i,item){
			var typeScore = $(item).html();
			if(typeScore != null && typeof(typeScore) != "undefined" && typeScore != '' ){
				allTypeScore += parseInt(typeScore);
			}
		});
		$("span[name='allTypeScore']").html(allTypeScore);
	});
}

function ajaxLoading(){   
	$("<div class=\"datagrid-mask\"></div>").css({display:"block",width:"100%",height:$(window).height()}).appendTo("body");   
	$("<div class=\"datagrid-mask-msg\"></div>").html("正在处理，请稍候。。。").appendTo("body").css({display:"block",left:($(document.body).outerWidth(true) - 190) / 2,top:($(window).height() - 45) / 2});   
}
function ajaxLoadEnd(){   
	$(".datagrid-mask").remove();
	$(".datagrid-mask-msg").remove();
}

// 初始化所选题库对象{bankId:bankName,bankId:bankName。。。}
var bankObjArray = new Object();
// 显示题库列表
function showBankList(){
	if (stuNum > 0) {
		$.messager.alert('提示','已发卷试卷不允许重新选择','warning');
		return;
	}
	bankObjArray = new Object();
	$("div[name='bank']").find("a[class='bankItem']").each(function(i,item){
		var bankId = $(this).attr("dataid");
		var bankName = $(this).attr("dataname");
		bankObjArray[bankId] = bankName;
	});
	$("#chooseBankDiv").window("open");	
	$("#bankTable").datagrid({
		url : '/${projectName}/manager/bank/schBank.action?isTheory=1&status=1',
		onLoadSuccess:function(data){
			letSelect();// 选中列表
		}, 
		onClickCell: function (rowIndex, field, value) {
			//点击数据行时触发
		},
		onSelect: function (rowIndex, rowData) {
			bankObjArray[rowData.bankId] = rowData.bankName;
		},
		onUnselect: function (rowIndex, rowData) {
			delete(bankObjArray[rowData.bankId]);
		},
		onUncheckAll:function(rows){
			for( var i = 0; i < rows.length; i++ ){
				var rowData = rows[i];
				delete(bankObjArray[rowData.bankId]);
			}
		},
		onSelectAll:function(rows){
			for( var i = 0; i < rows.length; i++ ){
				var rowData = rows[i];
				bankObjArray[rowData.bankId] = rowData.bankName;
			}
		}
	});
	
	// 分页回调
	var pg = $("#bankTable").datagrid("getPager");  
	if(pg){  
		$(pg).pagination({  
			onRefresh:function(pageNumber,pageSize){  
				for (var item in bankObjArray){
					$('#bankTable').datagrid('selectRecord', item); //根据id选中行   
				}
			},
			onChangePageSize:function(){  
				for (var item in bankObjArray){
					$('#bankTable').datagrid('selectRecord', item); //根据id选中行   
				}
			},
			onSelectPage:function(pageNumber,pageSize){  
				getDataByPage(pageNumber,pageSize);
			}
		}); 
	}
}

// 根据分页获取数据
function getDataByPage(pageNumber, pageSize) {
	$.ajax({
		type:"POST",
		url:'/${projectName}/manager/bank/schBank.action?isTheory=1&status=1',
		data: { "page": pageNumber, "pageSize": pageSize },
		dataType:'json',
		success:function(result){
			$("#bankTable").datagrid("loadData", result);
			var rows = $("#bankTable").datagrid("getRows");
			
			// 修改序号
			var rowNumbers = $('.datagrid-cell-rownumber');
			var start = (pageNumber - 1) * pageSize; 
			var end = start + pageSize; 
			$(rowNumbers).each(function(index){
				var row = parseInt($(rowNumbers[index]).html()) + parseInt(start);
				$(rowNumbers[index]).html("");
				$(rowNumbers[index]).html(row);
			});
			
			// 选中列表
			letSelect();
		}
	});
}

// 选择题库查询方法
function searchBanks(){
	var bankName = $("#searchBankForm").find("input[name='bankName']").val();
	var profession = $("#searchBankForm").find("input[name='profession']").val();
	$("#bankTable").datagrid('load',{
		bankName: bankName,
		profession:profession
	});
	// 选中列表
	letSelect();
}

// 触发选中
function letSelect(){
	var rows = $("#bankTable").datagrid("getRows"); 
	for (var item in bankObjArray){
		for(var i=0; i<rows.length;i++){
			var row = rows[i];
			if(item == row.bankId){
				$("#bankTable").datagrid("selectRow", i);
			}
		}
	}
}

// 保存选中的题库
function saveChooseBank(){
	var str = "";
	for (var item in bankObjArray){
		str += "<a class='bankItem' onclick=\"removeBank(this);\" title='点击删除此项' dataid='"+item+"' dataname='"+bankObjArray[item]+"' >"+bankObjArray[item]+"</a>";
	}
	$("div[name='bank']").html(str);
	$("#chooseBankDiv").window("close");
	// 保存选择的题库后计算各题型题目数量
	countTopicNum();
}

// 展示统计数量
function countTopicNum(){
	
	// 从缓存变量中拼装参数
	var bankIds = new Array();
	for (var item in bankObjArray){
		bankIds.push(item);
	}
	if(bankIds.length > 0){
		var param = JSON.stringify(bankIds);
		$.ajax({
			type:"POST",
			contentType : "application/json;charset=UTF-8",
			url:'/${projectName}/manager/bankStep/countTopicNum.action',
			data: param ,
			dataType:'json',
			beforeSend: ajaxLoading, 
			success:function(result){
				ajaxLoadEnd();// 关闭遮罩
				if(result.success){
					var data = result.data;
					
					var totalNum = 0;
					// 循环展示题目数量（与span的name对应）
					for (var item in data){
						var topicType = data[item];
						for(var diffNum in topicType){
							$("span[name='"+diffNum+"']").html(topicType[diffNum]);
						}
					}
					countTotalNum();
				}
			}
		});
	}else{
		$(".optSP").each(function(i,item){
			$(this).html(0);
		});
	}
}

// 删除题库
function removeBank(t){
	if (stuNum > 0) {
		$.messager.alert('提示','已发卷试卷不允许删除','warning');
		return;
	}
	var bankId = $(t).attr("dataid");
	delete(bankObjArray[bankId]);
	$(t).remove();
	// 删除题库后重新计算数目
	countTopicNum();
}

// 清空选择的题库
function clearAllBank(){
	if (stuNum > 0) {
		$.messager.alert('提示','已发卷试卷不允许清空','warning');
		return;
	}
	bankObjArray = new Object();
	$("div[name='bank']").find("a[class='bankItem']").remove();
	// 删除题库后重新计算数目
	countTopicNum();
}

//保存试卷和topic
function saveExerAndTopic(){
	if(!$("#exer_form").form("validate")){
		return;
	}
	var numFlg = true;
	$(".optNum").each(function( i,item ){
		var inputNum = $(this).val();
		var spanNum = $(this).next("span").html();
		var spanNum = parseInt(spanNum);
		var inputNum = parseInt(inputNum);
		if(inputNum > spanNum){
			$(this).tooltip({
				position: 'right',
				content: '<span style="color:#fff">您填写的数值过大</span>',
				onShow: function(){
					$(this).tooltip('tip').css({
						backgroundColor: '#ff8400',
						borderColor: '#ff8400'
					});
				}
			});
			$(this).tooltip('show');
			numFlg = false;
		}
	});
	if(!numFlg){
		return;
	}
	
	// 组装参数(form 表单)
	var obj = new Object();
	$("#exer_form").find("input[type='hidden']").each(function(i,item){
		var nameStr= $(this).attr("name");
		if(nameStr != 'topicIds'){
			obj[nameStr] = $(this).val();
		}
	});
	
	// 题型排序
	var typeSort = new Array();
	var typeSortStr = "";
	$("#sortDiv").find(".draggable-element").each(function( i,item ){
		var dataType = $(this).attr("data-type");
		if(dataType != null && dataType != '' && typeof(dataType) != "undefined"){
			typeSort.push(dataType);
			if(typeSortStr != ''){
				typeSortStr = typeSortStr+","+dataType;
			}else{
				typeSortStr = ""+dataType+"";
			}
		}
	});
	obj.typeSort=typeSort;
	obj.typeSortStr=typeSortStr;
	
	// 题型数量
	var numParam = new Object();
	$(".optNum").each(function( i,item ){
		var key = $(this).attr("name");
		var value = $(this).val();
		numParam[key]=value;
	});
	obj.numParam = numParam;
	
	// 所选择的题库
	var bankIds = new Array();
	var bankIdsStr = "";
	$("div[name='bank']").find("a").each(function(i,item){
		var dataid = $(this).attr("dataid");
		if(dataid != null && dataid != '' && typeof(dataid) != "undefined"){
			bankIds.push(dataid);
			if(bankIdsStr != ''){
				bankIdsStr = bankIdsStr+",'"+dataid+"'";
			}else{
				bankIdsStr = "'"+dataid+"'";
			}
		}
	});
	obj.bankIds = bankIds;
	obj.bankIdsStr = bankIdsStr;
	
	// object对象转换为json字符串
	var param = JSON.stringify(obj);
	// 执行ajax
	$.ajax({
		type:"POST",
		contentType : "application/json;charset=UTF-8",
		url:'/${projectName}/examination/exer/saveExerAndTopic.action',
		data: param ,
		dataType:'json',
		beforeSend: ajaxLoading, 
		success:function(result){
			if(result.success){
				ajaxLoadEnd();// 结束遮罩
				//调用主页面提示方法(title,msg,width,height,time)
				parent.mainShow('提示','保存成功',250,90,1500);
				window.location.href="/${projectName}/examination/exer/page.action?page=exer_sch";
			}else{
				$.messager.alert('提示',result.msg,'warning');
			}
		},
		error:function(XMLHttpRequest, textStatus, errorThrown){
			ajaxLoadEnd();// 结束遮罩
		}
	}); 
}

//保存试卷和topic
function cancelExerAndTopic(){
	var id = "${exerId}";
	$.ajax({
		type:"POST",
		url:'/${projectName}/examination/exer/cancelExerAndTopic.action',
		data: {"id":id},
		dataType:'json',
		success:function(result){
			if(result.success){
				window.location.href="/${projectName}/examination/exer/page.action?page=exer_sch";
			}else{
				$.messager.alert('提示',result.msg);
			}
		}
	});
}

</script>
</head>
<body>
	<center style="margin-top:15px;">
		<%-- 查询条件设定栏 --%>
		<form id="exer_form" method="post" action="/${projectName}/examination/exer/updExer.action">
			<input type="hidden" name='id' id='id'/>
			<input type="hidden" name='topicIds' id='topicIds'/>
			<table >
				<tr>
					<td class='label' style="min-width:64px;">试卷名称：</td>
					<td style="min-width:173px;"><input id="name" name="name" class="easyui-textbox" data-options="required:true"/><span color="red">*</span></td>
					<!-- <td class='label'>及格分值：</td>
					<td><input id="passScore" name="passScore" class="easyui-textbox"  data-options="validType:['int']"/></td> -->
					<td class='label' style="min-width:64px;">教研室：</td>
					<td style="min-width:173px;"><input id="department" name="department" class="easyui-textbox" data-options=""/></td>
					<td class='label' style="min-width:64px;">显示答案：</td>
					<td style="min-width:188px;"><dc:insertList name="showStandard" dictId="ZD_SFBZ" style="combo_normal" required="true"/><span color="red">*</span></td>
					<td colspan="4"></td>
				</tr>
				<tr>
					<td class='label' style="min-width:64px;">开始时间：</td>
					<td style="min-width:173px;"><input id="startTimestr" name="startTimestr" class="easyui-datetimebox" data-options="required:true"/><span color="red">*</span></td>
					<td class='label' style="min-width:64px;">结束时间：</td>
					<td style="min-width:173px;"><input id="endTimestr" name="endTimestr" class="easyui-datetimebox" data-options="required:true"/><span color="red">*</span></td>
					<!-- <td class='label'>总分值：</td>
					<td><input id="totalScore" name="totalScore" class="easyui-textbox" data-options="validType:['int']"/></td> -->
					<td class='label' style="min-width:64px;">分类目录：</td>
					<td style="min-width:188px;"><dc:insertTree name="shortcut" dictId="ZD_EXAM_CATEGORY" style="synch_single" required="true"/><span color="red">*</span></td>
					<td colspan="4"></td>
				</tr>
				<tr>
					<td class='label'><span onclick="" >选择题库</span>：</td>
					<td colspan="9">
						<a href="javascript:showBankList();" class="btn"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">选择</span></a>
						<a href="javascript:clearAllBank();" class="btn"><span class="icon-cancel" style="font-size:16px"></span> <span style="font-size:14px">清空</span></a>
					</td>
				</tr>
				<tr>
					<td class='label'></td>
					<td colspan="9"><div id="bank" name="bank" style="width:887px;min-height:60px;border: 1px #cfcfcf solid;" ></div></td>
				</tr>
				<tr>
					<td class='label'>排序：</td>
					<td colspan="9"><div id="sortDiv" name="sort" style="position: relative;" >
						<div class="draggable-element" data-type='1' >单选题 </div>
						<div class="draggable-element" data-type='2' >多选题 </div>
						<div class="draggable-element" data-type='4' >判断题 </div>
						<div class="draggable-element" data-type='3' >问答题 </div>
						<div class="draggable-element" data-type='9' >填空题</div>
					</div></td>
				</tr>
				<tr>
					<td class="countTh" style="width:10%">题型</td>
					<td class="countTh" style="width:10%">总分数</td>
					<td class="countTh" style="width:10%">已选题目</td>
					<td class="countTh" style="width:10%">单题分数</td>
					<td colspan="2" class="countTh" style="width:20%">易</td>
					<td colspan="2" class="countTh" style="width:20%">中</td>
					<td colspan="2" class="countTh" style="width:20%;">难</td>
				</tr>
				<tr name="optNumTr">
					<td class="countTd">单选</td>
					<td class="countTdScore">0</td>
					<td class="countTdNum">0</td>
					<td class="countTd"><input type="number" data-flg="score" class="optNum" min="0" name="singleScore" style="width: 50px;" /></td>
					<td colspan="2" class="countTd"><input type="number" class="optNum" min="0" name="singleNumY" style="width: 50px;" />/<span class="optSP" name='singleY'>0</span></td>
					<td colspan="2" class="countTd"><input type="number" class="optNum" min="0" name="singleNumZ" style="width: 50px;" />/<span class="optSP" name='singleZ'>0</span></td>
					<td colspan="2" class="countTd"><input type="number" class="optNum" min="0" name="singleNumN" style="width: 50px;" />/<span class="optSP" name='singleN'>0</span></td>
				</tr>
				<tr name="optNumTr">
					<td class="countTd">多选</td>
					<td class="countTdScore">0</td>
					<td class="countTdNum">0</td>
					<td class="countTd"><input type="number" data-flg="score" class="optNum" name="multiScore" style="width: 50px;" /></td>
					<td colspan="2" class="countTd"><input type="number" class="optNum" min="0" name='multiNumY' style="width: 50px;" />/<span class="optSP" name='multiY'>0</span></td>
					<td colspan="2" class="countTd"><input type="number" class="optNum" min="0" name='multiNumZ' style="width: 50px;" />/<span class="optSP" name='multiZ'>0</span></td>
					<td colspan="2" class="countTd"><input type="number" class="optNum" min="0" name='multiNumN' style="width: 50px;" />/<span class="optSP" name='multiN'>0</span></td>
				</tr>
				<tr name="optNumTr">
					<td class="countTd">判断</td>
					<td class="countTdScore">0</td>
					<td class="countTdNum">0</td>
					<td class="countTd"><input type="number" data-flg="score" class="optNum" min="0" name="judgeScore" style="width: 50px;" /></td>
					<td colspan="2" class="countTd"><input type="number" class="optNum" min="0" name='judgeNumY' style="width: 50px;" />/<span class="optSP" name='judgeY'>0</span></td>
					<td colspan="2" class="countTd"><input type="number" class="optNum" min="0" name='judgeNumZ' style="width: 50px;" />/<span class="optSP" name='judgeZ'>0</span></td>
					<td colspan="2" class="countTd"><input type="number" class="optNum" min="0" name='judgeNumN' style="width: 50px;" />/<span class="optSP" name='judgeN'>0</span></td>
				</tr>
				<tr name="optNumTr">
					<td class="countTd">填空</td>
					<td class="countTdScore">0</td>
					<td class="countTdNum">0</td>
					<td class="countTd"><input type="number" data-flg="score" class="optNum" min="0" name="clozedScore" style="width: 50px;" /></td>
					<td colspan="2" class="countTd"><input type="number" class="optNum" min="0" name='clozedNumY' style="width: 50px;" />/<span class="optSP" name='clozedY'>0</span></td>
					<td colspan="2" class="countTd"><input type="number" class="optNum" min="0" name='clozedNumZ' style="width: 50px;" />/<span class="optSP" name='clozedZ'>0</span></td>
					<td colspan="2" class="countTd"><input type="number" class="optNum" min="0" name='clozedNumN' style="width: 50px;" />/<span class="optSP" name='clozedN'>0</span></td>
				</tr>
				<tr name="optNumTr">
					<td class="countTd">问答</td>
					<td class="countTdScore">0</td>
					<td class="countTdNum">0</td>
					<td class="countTd"><input type="number" data-flg="score" class="optNum" min="0" name="answerScore" style="width: 50px;" /></td>
					<td colspan="2" class="countTd"><input type="number" class="optNum" min="0" name='answerNumY' style="width: 50px;" />/<span class="optSP" name='answerY'>0</span></td>
					<td colspan="2" class="countTd"><input type="number" class="optNum" min="0" name='answerNumZ' style="width: 50px;" />/<span class="optSP" name='answerZ'>0</span></td>
					<td colspan="2" class="countTd"><input type="number" class="optNum" min="0" name='answerNumN' style="width: 50px;" />/<span class="optSP" name='answerN'>0</span></td>
				</tr>
				<tr >
					<td colspan="10" style="text-align:right;" >
						<div style="font-size:18px;position: fixed;">共：<span name="allTypeScore"></span>分</div>
						<div style="font-size:18px">共：<span name="totalNumSpan"></span> 道题目，已选：<span name="chooseNumSpan">10</span> 道题目</div>
					</td>
				</tr>
			</table>
		</form>
		<br><br>
		<div style="margin-bottom:5px">
			<!-- <a href="javascript:showBankList();" class="btn btn-orange"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">选择题库</span></a>&emsp; -->
			<a href="javascript:saveExerAndTopic();" class="btn"><span class="icon-ok" style="font-size:16px"></span> <span style="font-size:14px">保存</span></a>&emsp;
			<a href="javascript:cancelExerAndTopic();" class="btn"><span class="icon-cancel" style="font-size:16px"></span> <span style="font-size:14px">取消</span></a>
		</div>
	</center>
</body>

<!-- 选择题库的弹出框 -->
<div id="chooseBankDiv" class="easyui-window" title="选择题库" inline="true"
	data-options="modal:true,iconCls:'icon-save'" style="width:600px;height:600px;top:30px;"
	closed="true">
	<div class="easyui-layout" fit="true">
		<div region="center" border="false" border="false" style="width:99%;">
			<table id="bankTable"  class="easyui-datagrid" fitcolumns=true
			data-options="iconCls:'icon-user',singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#winToolbar',collapsible:true,method:'get'">
				<thead>
					<tr>
						<th field="ck" checkbox="true"></th>
						<th data-options="field:'bankName',width:25,align:'center',sortable:true" >题库名称</th>
						<th data-options="field:'bankId',width:25,align:'center',sortable:true">题库编号</th>
						<th data-options="field:'profession',width:25,align:'center',sortable:true" >行业</th>
						<th data-options="field:'level',width:25,align:'center',sortable:true" >级别</th>
						<th data-options="field:'isTheory',width:25,align:'center',sortable:true" >理论题库</th>
					</tr>
				</thead>
			</table>
		</div>
		<div region="south" border="false"
			style="text-align:center;height:30px;line-height:30px;">
			<a class="easyui-linkbutton" icon="icon-ok" id="choseTkBtn" href="javascript:void(0)" onclick="saveChooseBank();">确认选择</a>
			<a class="easyui-linkbutton" icon="icon-cancel"
				href="javascript:$('#chooseBankDiv').window('close')">取消</a>
		</div>
	</div>
</div>
<!-- 选择题库弹出框搜索工具栏 -->
<div id="winToolbar" style="padding:5px;height:auto ;">
	<div>
	<form id="searchBankForm">
		题库名称:<input name="bankName"  style="width:80px">
		行业:<input name="profession" style="width:80px">
		<a href="javascript:void(0);" onclick="searchBanks();" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>
	</form>
	</div>
</div>

<div id="sortTemp" style="display:none;">
	<div class="draggable-element" data-type='1' >单选题 </div>
	<div class="draggable-element" data-type='2' >多选题 </div>
	<div class="draggable-element" data-type='4' >判断题 </div>
	<div class="draggable-element" data-type='3' >问答题 </div>
	<div class="draggable-element" data-type='9' >填空题</div>
</div>

</html>

