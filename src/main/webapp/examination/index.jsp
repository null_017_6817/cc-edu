<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Basic Panel - jQuery EasyUI Demo</title>
	<%@include file="/common/include.jsp" %>
	<script>
	function addTab(title, url){
		if ($('#tabdiv').tabs('exists', title)){
			$('#tabdiv').tabs('select', title);
		} else {
			var content = '<iframe scrolling="no" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
			$('#tabdiv').tabs('add',{
				title:title,
				content:content,
				closable:true
			});
		}
	}
	</script>
	<style>
	.panel-body{overflow:hidden}
	</style>
</head>
<body class="easyui-layout">
	<div data-options="region:'west',split:true" style="width:200px;padding:0px;">
		<div class="easyui-accordion" data-options="fit:true,border:false">
			<div title="Title1"  data-options="selected:true"  style="padding:10px;">
				<a href="javascript:addTab('测试练习','/${projectName}/examination/exer/page.action?page=exer_sch')">测试练习</a><br/>

				<a href="javascript:addTab('测试学生答题卡','/${projectName}/examination/exerAnswer/page.action?page=exerAnswer_sch')">测试学生答题卡</a><br/>

				<a href="javascript:addTab('测试学生答题试卷','/${projectName}/examination/exerPaper/page.action?page=exerPaper_sch')">测试学生答题试卷</a><br/>

				<a href="javascript:addTab('测试练习与班级关系表','/${projectName}/examination/rExerStud/page.action?page=rExerStud_sch')">测试练习与班级关系表</a><br/>

				<a href="javascript:addTab('测试习题','/${projectName}/examination/exerTopic/page.action?page=exerTopic_sch')">测试习题</a><br/>

				<a href="javascript:addTab('测试答题步骤 ','/${projectName}/examination/exerStep/page.action?page=exerStep_sch')">测试答题步骤 </a><br/>

				<a href="javascript:addTab('测试开始考试','/${projectName}/examination/exerTopic/page.action?page=exerTopicExam_sch')">测试开始考试</a><br/>

				<a href="javascript:addTab('测试成绩汇总','/${projectName}/examination/scoreCollec/page.action?page=scoreCollec_sch')">测试成绩汇总</a><br/>

				<a href="javascript:addTab('老师阅卷','/${projectName}/examination/mark/page.action?page=teacher_mark')">老师阅卷</a><br/>
			
			</div>
		</div>
	</div>
	<div data-options="region:'center'">
		<div id="tabdiv" class="easyui-tabs" data-options="fit:true,border:false,plain:true" fit="true">
		</div>
	</div>
</body>
</html>
