<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>发卷/撤卷</title>
	<%@include file="/common/include.jsp" %>
<style>

.teacherItem{
	min-width: 70px;
	display: inline-block;
	text-align: center;
	height: 20px;
	vertical-align: middle;
	background-color: #ffffff;
	border: 1px #6bd26b solid;
	line-height: 20px;
	font-weight: unset;
	cursor: pointer;
	-webkit-border-radius: 20px;
	margin: 5px;
	border-radius: 3px;
}

.studItem{
	margin-bottom: 10px;
	margin: 5px;
	display: inline-block;
	width: 60px;
	border: 1px #cfcfcf solid;
	text-align: center;
	height: 20px;
	line-height: 20px;
	border-radius: 3px;
	background: #ffffff;
}
.studDiv{
	border: 1px dashed #cfcfcf;
	margin: 5px;
	margin-top: 10px;
}

.claItem{
	font-size:16px;
	font-weight:600;
	margin-bottom:-5px;
	margin-top:6px;
	margin-left: 6px;
}

</style>
<script>

// JQ初始化
$(function(){
	var exerId = "${exerId}";
	
	initData(exerId);
});

// 全部班级集合，由初始化方法赋值
var classAll = new Object();
// 初始化所选题库对象{bankId:bankName,bankId:bankName。。。}
var teacherObjArray = new Object();
// 班级对象参数{班级：{学号：姓名,学号:姓名,。。。。}}
var bjObjArray = new Object();

var bjArray = new Array();
// 初始化数据
function initData(exerId){
	
	$.ajax({
		type:"POST",
		url:'/${projectName}/examination/rExerStud/batchGetExerAndStud.action?ids='+exerId,
		data: {'id': ""},
		dataType:'json',
		success:function(result){
			if(result.success){
				var exerName = result.exerName;// 试卷
				var teachersMap = result.teachersMap;// 老师集合
				var classMap = result.classMap;// 班级
				var rStus = result.rStus;// 学生列表
				classAll = result.classAll;//全部班级
				
				// 试卷名称
				$("#exerName").html(exerName);
				
				
				// saveStudent();
			}else{
				alert(result.msg);
			}
		}
	});
}

Array.prototype.indexOf = function (val) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == val) return i;
    }
    return -1;
};

Array.prototype.remove = function (val) {
    var index = this.indexOf(val);
    if (index > -1) {
        this.splice(index, 1);
    }
};

/***************************************************************************
	选择阅卷老师部分，相关操作开始
	method:		showTeacherList 显示老师列表(显示弹出页)
	method:		getTeacherDataByPage 根据分页获取数据
	method:		saveChooseTeacher 选择老师的弹出页面保存按钮按下
	method:		removeTeacher 删除选中的老师
	method:		teacherSelect 触发老师列表选中
	method:		searchTeacher 老师弹出页检索功能
 **************************************************************************/

/**************************************
	显示老师列表(显示弹出页)
***************************************/
function showTeacherList(){
	
	teacherObjArray = new Object();
	$("div[name='teachers']").find("a[class='teacherItem']").each(function(i,item){
		var id = $(this).attr("dataid");
		var name = $(this).attr("dataname");
		teacherObjArray[id] = name;
	});
	
	$("#chooseTeacherDiv").window("open");	
	$("#teacherTable").datagrid({
		url : '/${projectName}/sysmanage/user/schUser.action',
		onLoadSuccess:function(data){
			teacherSelect();
		}, 
		onClickCell: function (rowIndex, field, value) {
			//点击数据行时触发
		},
		onSelect: function (rowIndex, rowData) {
			teacherObjArray[rowData.id] = rowData.name;
		},
		onUnselect: function (rowIndex, rowData) {
			delete(teacherObjArray[rowData.id]);
		},
		onUncheckAll:function(rows){
			for( var i = 0; i < rows.length; i++ ){
				var rowData = rows[i];
				delete(teacherObjArray[rowData.id]);
			}
		},
		onSelectAll:function(rows){
			for( var i = 0; i < rows.length; i++ ){
				var rowData = rows[i];
				teacherObjArray[rowData.id] = rowData.name;
			}
		}
	});
	
	// 分页回调
	var pg = $("#teacherTable").datagrid("getPager");  
	if(pg){  
		$(pg).pagination({  
			onRefresh:function(pageNumber,pageSize){  
				for (var item in teacherObjArray){
					$('#teacherTable').datagrid('selectRecord', item); //根据id选中行   
				}
			},
			onChangePageSize:function(){  
				for (var item in teacherObjArray){
					$('#teacherTable').datagrid('selectRecord', item); //根据id选中行   
				}
			},
			onSelectPage:function(pageNumber,pageSize){  
				getTeacherDataByPage(pageNumber,pageSize);
			}
		}); 
	}
}

/**************************************
	根据分页获取数据
	param:    pageNumber(页码)
	param:    pageSize(一页条数)
***************************************/
function getTeacherDataByPage(pageNumber, pageSize) {
	var name = $("#searchTeacehrForm").find("input[name='name']").val();
	$.ajax({
		type:"POST",
		url:'/${projectName}/sysmanage/user/schUser.action',
		data: { "page": pageNumber, "pageSize": pageSize , "name": name},
		dataType:'json',
		success:function(result){
			$("#teacherTable").datagrid("loadData", result);
			var rows = $("#teacherTable").datagrid("getRows");
			
			// 修改序号
			var rowNumbers = $('.datagrid-cell-rownumber');
			var start = (pageNumber - 1) * pageSize; 
			var end = start + pageSize; 
			$(rowNumbers).each(function(index){
				var row = parseInt($(rowNumbers[index]).html()) + parseInt(start);
				$(rowNumbers[index]).html("");
				$(rowNumbers[index]).html(row);
			});
			
			// 选中列表
			teacherSelect();
		}
	});
}

/**************************************
	选择老师的弹出页面保存按钮按下
***************************************/
function saveChooseTeacher(){
	var str = "";
	for (var item in teacherObjArray){
		str += "<a class='teacherItem' dataid='"+item+"' dataname='"+teacherObjArray[item]+"' >"+teacherObjArray[item]+"</a>";
	}
	$("div[name='teachers']").html(str);
	$("#chooseTeacherDiv").window("close");
	
}

/************************************
 	 删除当前选中的老师
 ***********************************/
function removeTeacher(t){
	var id = $(t).attr("dataid");
	delete(teacherObjArray[id]);
	$(t).remove();
}

/**************************************
	触发选中
***************************************/
function teacherSelect(){
	var rows = $("#teacherTable").datagrid("getRows"); 
	for (var item in teacherObjArray){
		for(var i=0; i<rows.length;i++){
			var row = rows[i];
			if(item == row.id){
				$("#teacherTable").datagrid("selectRow", i);
			}
		}
	}
}

/**************************************
	选择老师的弹出页面点击检索
***************************************/
function searchTeacher(){
	var name = $("#searchTeacehrForm").find("input[name='name']").val();
	$('#teacherTable').datagrid('load',{
		name: name,
	});
}

/***************************************************************************
	选择阅卷老师部分，相关操作结束
**************************************************************************/


/***************************************************************************
	选择学生部分，相关操作开始
**************************************************************************/

// 显示学生列表
function showClass(){
	$("#toStudent").window("open");	
	$('#xs').datagrid('loadData', { total: 0, rows: [] }); 
	$('#bj').datagrid({
		url : '/${projectName}/manager/classes/schClasses.action',
		onLoadSuccess:function(data){
			$(".datagrid-header-check").html("");
			classSelect();
		}, 
		onClickCell: function (rowIndex, field, value) {
			//点击数据行时触发
		},
		onSelect: function (rowIndex, rowData) {
			var studObj = bjObjArray[rowData.number];
			//bjArray.push(stuObj);
			ajaxGetStud(rowData.number);
		},
		onUnselect: function (rowIndex, rowData) {
			delete(bjObjArray[rowData.number]);
			bjArray.remove(rowData.number);
			schStudList();
		}
	});
}



// 选择班级
function classSelect(){
	var rows = $("#bj").datagrid("getRows"); 
	for (var item in bjObjArray){
		for(var i=0; i<rows.length;i++){
			var row = rows[i];
			if(item == row.number){
				$("#bj").datagrid("selectRow", i);
				break;
			}
		}
	}
	schStudList();
}

// 新选择班级时查询学生
function ajaxGetStud(cnumber){
	$.ajax({
		type:"POST",
		url:'/${projectName}/manager/stud/schStudListByCnumber.action',
		data: { "cnumber" : cnumber },
		dataType:'json',
		async: false,
		success:function(result){
			if(result.success){
				var studs = result.data;
				if(studs != null && typeof(studs) != "undefined"){
					var stuObj = bjObjArray[cnumber];
					if(stuObj == null || typeof(stuObj) == "undefined"){
						var obj = new Object();
						for( var i = 0; i < studs.length; i++ ){
							var stu = studs[i];
							if(stu != null && typeof(stu) != "undefined"){
								obj[stu.studNumber]=stu.studName;
							}
						}
						bjObjArray[cnumber] = obj;
						bjArray.push(cnumber);
					}
				}
			}
			schStudList();
			studSelect();
		}
	});
	
}

// 查询学生列表数据
function schStudList(){
	var cnumbers = "";
/* 	for( item in bjObjArray){
		if(item != null && item != "" && typeof(item) != "undefined" ){
			if(cnumbers != ""){
				cnumbers = cnumbers +",'"+item+"'";
			}else{
				cnumbers = "'"+item+"'";
			}
		}
	} */
	bjArray.forEach(function(item, index, a){
		if(item != null && item != "" && typeof(item) != "undefined" ){
			if(cnumbers != ""){
				cnumbers = cnumbers +",'"+item+"'";
			}else{
				cnumbers = "'"+item+"'";
			}
		}
	});
	
	if( cnumbers != "" ){
		$('#xs').datagrid({
			url : '/${projectName}/manager/stud/schStudExertoPerson.action?cnumbers='+cnumbers,
			onLoadSuccess:function(data){
				studSelect();
			}, 
			onSelect: function (rowIndex, rowData) {
				var cla = rowData.cnumber;
				var stuNumber = rowData.number;
				var stuName = rowData.name;
				var stuObjArr = bjObjArray[cla];
				if(stuObjArr == null || typeof(stuObjArr) == "undefined" ){
					stuObjArr = new Object();
				}
				stuObjArr[stuNumber] = stuName;
			},
			onUnselect: function (rowIndex, rowData) {
				var cla = rowData.cnumber;
				var stuNumber = rowData.number;
				var stuName = rowData.name;
				var stuObjArr = bjObjArray[cla];
				if(stuObjArr != null && typeof(stuObjArr) != "undefined" ){
					delete(stuObjArr[stuNumber]);
				}
			},
			onUncheckAll:function(rows){
				for(var i = 0; i < rows.length; i++ ){
					var rowData = rows[i];
					var cla = rowData.cnumber;
					var stuNumber = rowData.number;
					var stuName = rowData.name;
					var stuObjArr = bjObjArray[cla];
					if(stuObjArr != null && typeof(stuObjArr) != "undefined" ){
						delete(stuObjArr[stuNumber]);
					}
				}
			},
			onSelectAll:function(rows){
				for(var i = 0; i < rows.length; i++ ){
					var rowData = rows[i];
					var cla = rowData.cnumber;
					var stuNumber = rowData.number;
					var stuName = rowData.name;
					var stuObjArr = bjObjArray[cla];
					if(stuObjArr == null || typeof(stuObjArr) == "undefined" ){
						stuObjArr = new Object();
					}
					stuObjArr[stuNumber] = stuName;
				}
			}
		});
	}else{
		$('#xs').datagrid('loadData', { total: 0, rows: [] }); 
	}
}

// 学生选中
function studSelect(){
	var rows = $("#xs").datagrid("getRows"); 
	for(var i=0; i<rows.length;i++){
		var row = rows[i];
		var cnumber = row.cnumber;
		var studsObj = bjObjArray[cnumber];
		if(studsObj != null && typeof(studsObj) != "undefined"){
			var rowNumber = row.number;
			var stuName = studsObj[rowNumber];
			if( stuName != null && typeof(stuName) != "undefined"){
				$("#xs").datagrid("selectRow", i);
			}
		}
	}
}

// 保存学生到页面
function saveStudent(){
	$("#toStudent").window("close");
	$("#studs").html("");
	if(bjObjArray != null && typeof(bjObjArray) != "undefined"){
		for(bj in bjObjArray){
			
			var studObj = bjObjArray[bj];
			var htm="";
			var num = 0;
			for(xs in studObj){
				htm += "<p class='studItem' data-number='"+xs+"' data-name='"+studObj[xs]+"' data-cla='"+bj+"' >"+studObj[xs]+"</p>";
				num++;
			}
			var html = "<div class='studDiv'><p class='claItem' data-number='"+bj+"'>"+classAll[bj]+" (已选："+num+")</p><br>"+htm+"</div>";
			$("#studs").append(html);
		}
	}
	var total = $(".studItem").length;
	$("#totalP").html("已选: "+total+" 位同学");
}

/***************************************************************************
	选择学生部分，相关操作结束
**************************************************************************/

// 保存
function saveToPerson(){
	var teachers_ = $("#teachers").html();
	var students_ = $("#studs").html();
	console.log(teachers_);
	console.log(students_);
	if (teachers_ == null || teachers_ == "") {
		$.messager.alert('敬告','请选择阅卷老师？');
		return;
	}
	
	var studArr = new Array();
	$(".studItem").each(function(i,item){
		var number= $(this).attr("data-number");
		studArr.push(number);
	});
	var param = JSON.stringify(studArr);
	
	var teachers = "";
	$("div[name='teachers']").find("a").each(function(){
		var id = $(this).attr("dataid");
		if(id != null && id != "" && typeof(id) != "undefined"){
			if(teachers != ""){
				teachers += id+",";
			}else{
				teachers = ","+id+",";
			}
		}
	});
	
	$.ajax({
		type:"POST",
		contentType : "application/json;charset=UTF-8",
		url:'/${projectName}/examination/rExerStud/batchGetDiffStud.action?eid=${exerId}&teachers='+teachers,
		data: param,
		dataType:'json',
		success:function(result){
			
			if(result.success){
				parent.mainShow('提示','操作成功',250,90,1500);
				window.location.href="/${projectName}/examination/exer/page.action?page=exer_sch";
			}else{
				alert(result.msg);
			}
			
			/* if(result.success){
				var addList = result.addList;
				var delList = result.delList;
				
				var addStr = "";
				if(addList != null){
					for( var i = 0; i < addList.length; i++ ){
						var addNum = addList[i];
						addStr = addStr + addNum + ",";
					}
				}
				var delStr = "";
				if(delList != null){
					for( var i = 0; i < delList.length; i++ ){
						var delNum = delList[i];
						delStr = delStr + delNum + ",";
					}
				}
				//addAndRemove(addStr,delStr);
			}else{
				alert(result.msg);
			} */
		}
	});
}

// 发卷和撤卷
function addAndRemove(addStr ,delStr){
	
	var teachers = "";
	$("div[name='teachers']").find("a").each(function(){
		var id = $(this).attr("dataid");
		if(id != null && id != "" && typeof(id) != "undefined"){
			if(teachers != ""){
				teachers += id+",";
			}else{
				teachers = ","+id+",";
			}
		}
	});
	
	$.ajax({
		type:"POST",
		url:'/${projectName}/examination/rExerStud/addAndRemove.action',
		data: {"addStr":addStr,"delStr":delStr,"eid":'${exerId}',"teachers":teachers},
		dataType:'json',
		success:function(result){
			if(result.success){
				parent.mainShow('提示','操作成功',250,90,1500);
				window.location.href="/${projectName}/examination/exer/page.action?page=exer_sch";
			}else{
				alert(result.msg);
			}
		}
	});
}

// 取消
function cancel(){
	window.location.href="/${projectName}/examination/exer/page.action?page=exer_sch";
}

</script>
</head>
<body>
	<div style="width:1100px;">
		<%-- 查询条件设定栏 --%>
		<form id="exer_form" method="post" action="/${projectName}/examination/exer/updExer.action">
			<input type="hidden" name='id' id='id'/>
			<input type="hidden" name='topicIds' id='topicIds'/>
			<table>
				<tr>
					<td class='label'>试卷名称：</td>
					<td colspan="7"><span id="exerName"></span></td>
				</tr>
				<tr>
					<td class='label'><span >阅卷老师</span>：</td>
					<td colspan="7"><div onclick="showTeacherList();" id="teachers" name="teachers" style="cursor: pointer;width:887px;min-height:32px;border: 1px #cfcfcf solid;" ></div></td>
				</tr>
				<tr>
					<td class='label'><span >选择学生</span>：</td>
					<td colspan="7">
						<div id="studs" onclick="showClass();" name="studs" style="cursor: pointer;width:887px;height:400px;border: 1px #cfcfcf solid;overflow: scroll;" ></div>
						<p id="totalP" style="text-align:right;padding-right:5px;padding-top:5px;">共：</p>
					</td>
				</tr>
			</table>
		</form>
		<br><br>
		<div style="margin-bottom:5px;text-align:center;">
			<a href="javascript:saveToPerson();" class="btn"><span class="icon-ok" style="font-size:16px"></span> <span style="font-size:14px">保存</span></a>&emsp;
			<a href="javascript:cancel();" class="btn"><span class="icon-cancel" style="font-size:16px"></span> <span style="font-size:14px">取消</span></a>
		</div>
	</div >
</body>

<!-- 选择老师的弹出框 -->
<div id="chooseTeacherDiv" class="easyui-window" title="选择老师" inline="true"
	data-options="modal:true,iconCls:'icon-save'" style="width:600px;height:600px;top:30px;"
	closed="true">
	<div class="easyui-layout" fit="true">
		<div region="center" border="false" border="false" style="width:99%;">
			<table id="teacherTable"  class="easyui-datagrid" fitcolumns=true
			data-options="iconCls:'icon-user',singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#teacherToolbar',collapsible:true,method:'get'">
				<thead>
					<tr>
						<th field="ck" checkbox="true"></th>
						<th data-options="field:'name',sortable:true">姓名</th>
						<th data-options="field:'cnumberName',width:80,sortable:true">班级</th>
					</tr>
				</thead>
			</table>
		</div>
		<div region="south" border="false"
			style="text-align:center;height:30px;line-height:30px;">
			<a class="easyui-linkbutton" icon="icon-ok" id="choseTkBtn" href="javascript:void(0)" onclick="saveChooseTeacher();">确认选择</a>
			<a class="easyui-linkbutton" icon="icon-cancel"
				href="javascript:$('#chooseTeacherDiv').window('close')">取消</a>
		</div>
	</div>
</div>
<!-- 选择题库弹出框搜索工具栏 -->
<div id="teacherToolbar" style="padding:5px;height:auto ;">
	<div>
	<form id="searchTeacehrForm">
		姓名：<input class="easyui-textbox" name="name" id="name" data-options="
						iconWidth:16,
						icons: [{iconCls:'icon-clear',
							handler: function(e){$(e.data.target).textbox('setValue', '');}
						}]"/>
		<a href="javascript:void(0);" onclick="searchTeacher();" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>
	</form>
	</div>
</div>

<!-- 选择学生 -->
<div id="toStudent" class="easyui-window" title="学生列表" inline="true"
	data-options="modal:true,iconCls:'icon-save'" style="width:1200px;height:700px;top:35px;"
	closed="true">
	<div class="easyui-layout" fit="true">
		<div region="west" border="true" split="true" style="width:45%;">
			<table id="bj" title="班级" class="easyui-datagrid" 
				data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,
				fitColumns:true,striped:true,region:'center',border:false" style="overflow: hidden;">
				<thead>
					<tr>
						<th data-options="field:'checkbox',checkbox:true" ></th>
						<th data-options="field:'number',width:25,align:'center',sortable:true">班级号</th>
						<th data-options="field:'name',width:25,align:'center',sortable:true">班级名称</th>
						<th data-options="field:'createTime',width:25,align:'center',sortable:true">创建时间</th>
					</tr>
				</thead>
			</table>
		</div>
		<div region="center" border="true" style="width:55%;">
			<table id="xs" title="学生列表" class="easyui-datagrid"
				data-options="singleSelect:false,rownumbers:true,fit:true,
				fitColumns:true,striped:true,region:'center',border:false" style="overflow: hidden;">
				<thead>
					<tr>
						<th data-options="field:'checkbox',checkbox:true" ></th>
						<th data-options="field:'number',width:25,align:'center',sortable:true">学号</th>
						<th data-options="field:'cnumber',width:25,align:'center',sortable:true">班级</th>
						<th data-options="field:'name',width:25,align:'center',sortable:true">姓名</th>
						<th data-options="field:'sex',width:25,align:'center',sortable:true">性别</th>
					</tr>
				</thead>
			</table>
		</div>
		<div region="south" border="true"
			style="text-align:center;height:30px;line-height:30px;">
			<a class="easyui-linkbutton" icon="icon-ok" href="javascript:void(0)" onclick="saveStudent();">确认选择</a>
			<a class="easyui-linkbutton" icon="icon-cancel"
				href="javascript:$('#toStudent').window('close');">取消</a>
		</div>
	</div>
</div>

</html>

