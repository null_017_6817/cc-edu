<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>修改练习</title>
	<%@include file="/common/include.jsp" %>
	<style>
		td > span {
			width: 155px !important;
		}
	</style>
<script>
<%-- 查询页点击修改时调用此方法加载记录详情 --%>
function load(id){
	$('#_form').form('load','/${projectName}/examination/exer/vieExer.action?id='+id);
};

<%-- 点击保存按钮时调用此方法。(注:保存按钮在查询列表页) --%>
function submitForm(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#_form').form('submit',{
		success:function(data) {
			parent.reloadData();
		}
	});
}
</script>
</head>
<body>
<form id="_form" method="post" action="/${projectName}/examination/exer/updExer.action">
	<input type="hidden" name='id' id='id'/>
	<table>
		<tr>
			<td class='label' style="width: 150px">
			试卷名称：
			</td>
			<td>
			<input id="name" name="name" class="easyui-textbox" data-options="required:true"/><span color="red">*</span>
			</td>
			<td class='label' style="width: 150px">
			分类目录：
			</td>
			<td>
				<input id="shortcut" name="shortcut" class="easyui-textbox" data-options="required:true"/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			教研室：
			</td>
			<td>
			<input id="department" name="department" class="easyui-textbox" data-options=""/>
			</td>
			<td class='label'>
			发起教师：
			</td>
			<td>
				<input id="teacher" name="teacher" class="easyui-textbox" data-options=""/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			题库编号：
			</td>
			<td>
			<input id="bankId" name="bankId" class="easyui-textbox" data-options=""/>
			</td>
			<td class='label'>
			题库名：
			</td>
			<td>
				<input id="bankName" name="bankName" class="easyui-textbox" data-options=""/>
			</td>
		</tr>
		<tr>
			
			<td class='label'>
			练习开始时间：
			</td>
			<td>
				<input id="startTimestr" name="startTimestr" class="easyui-datetimebox" data-options=""/><span color="red">*</span>
			</td>
			<td class='label'>
			练习结束时间：
			</td>
			<td>
			<input id="endTimestr" name="endTimestr"  class="easyui-datetimebox" data-options=""/><span color="red">*</span>
			</td>
			<!-- <td class='label'>
			考试时间：
			</td>
			<td>
				<input id="expireTime" name="expireTime" editable="false"  data-options="required:true"/>
			</td> -->
		</tr>
		<tr>
			<td class='label'>
			题数：
			</td>
			<td>
			<input id="totalNum" name="totalNum" class="easyui-textbox" data-options="validType:['int']"/>
			</td>
			<td class='label'>
			总分值：
			</td>
			<td>
				<input id="totalScore" name="totalScore" class="easyui-textbox" data-options="validType:['int']"/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			及格分值：
			</td>
			<td>
			<input id="passScore" name="passScore" class="easyui-textbox" data-options="validType:['int']"/>
			</td>
			<td class='label'>
			是否显示标准答案：
			</td>
			<td>
				<dc:insertList name="showStandard" dictId="ZD_SFBZ" style="combo_normal" required="true"/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			备注：
			</td>
			<td>
				<input id="remark" name="remark" class="easyui-textbox" data-options=""/>
			</td>
		</tr>
	</table>

</form>
<script type="text/javascript">
$(function(){
	$("#expireTime").timespinner({    
		showSeconds:true
	});   
});
/* function ww3(date){  
    var h = date.getHours();  
    var min = date.getMinutes();  
    var sec = date.getSeconds();  
    var str = (h<10?('0'+h):h)+':'+(min<10?('0'+min):min)+':'+(sec<10?('0'+sec):sec);  
    return str;  
}  
function w3(s){  
	 var reg=/(:)/  //利用正则表达式分隔  
     var ss = (s.split(reg));  
     var h = parseInt(ss[0],10);
     var min = parseInt(ss[1], 10);
     var sec = parseInt(ss[2], 10);
} */  
</script>
</body>
</html>
