<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>学生答题试卷查询</title><!--exerPaper-->
	<%@include file="/common/include.jsp" %>

<script>
$(function(){
	<%-- 为分页行设置"添加"和"删除"按钮 --%>
	var pager = $('#exerPaperGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});			
})

<%-- 执行查询，点击查询按钮时调用 --%>
function doSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#exerPaperGrid').datagrid('load',{
		exerId:$('#exerId').val(),
		studNumber:$('#studNumber').val(),
		score:$('#score').val()
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatOper(val,row,index){  
    return "<span onclick='updExerPaper("+val+")' style='text-decoration:underline;cursor:pointer;'>修改</span> "
    			+"<span onclick='vieExerPaper("+val+")' style='text-decoration:underline;cursor:pointer;'>查看</span>";
} 

<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
function updExerPaper(id){
	QMoveDialog($("#exerPaperEditWin"));
	$('#exerPaperViewWin').dialog('close');
	$('#exerPaperEditWin').dialog('open');
	$('#exerPaperEditWin').get(0).contentWindow.load(id);
} 

<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
function vieExerPaper(id){
	QMoveDialog($("#exerPaperViewWin"));
	$('#exerPaperEditWin').dialog('close');
	$('#exerPaperViewWin').dialog('open');
	$('#exerPaperViewWin').get(0).contentWindow.load(id);
}

<%-- 删除记录 --%>
function delExerPaper(){
	var checkedItems = $('#exerPaperGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/examination/exerPaper/delExerPaper.action",{ids:ids},function(data){
				$('#exerPaperGrid').datagrid('reload');
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#exerPaperEditWin').dialog('close');
	$('#exerPaperGrid').datagrid('reload');
}
</script>
</head>
<body>
<table id="exerPaperGrid" title="学生答题试卷" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#exerPaperGridToolbar',collapsible:true,url:'/${projectName}/examination/exerPaper/schExerPaper.action',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" ></th>
			<th data-options="field:'exerId',width:25,align:'center',sortable:true">练习编号</th>
			<th data-options="field:'studNumber',width:25,align:'center',sortable:true">学号</th>
			<th data-options="field:'score',width:25,align:'center',sortable:true">得分</th>
			<th data-options="field:'scoredetail',width:25,align:'center',sortable:true">得分详情</th>

			<th data-options="field:'_oper',align:'center',formatter:formatOper">操作</th>
		</tr>
	</thead>
</table>
<%-- 查询条件设定栏 --%>
<div id="exerPaperGridToolbar">
	<div style="margin-bottom:5px">
		<a href="javascript:updExerPaper(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true">添加</a>
		<a href="javascript:delExerPaper()" class="easyui-linkbutton" iconCls="icon-remove" plain="true">删除</a>
	</div>
	<form id="_form">
	练习编号：<input name='exerId' id="exerId" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	学号：<input name='studNumber' id="studNumber" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	得分：<input name='score' id="score" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>

	<a href="#" class="easyui-linkbutton" iconCls="icon-search" onclick="doSearch()">查询</a>
	</form>
</div>


<%-- 修改or添加面板 --%>
<iframe id="exerPaperEditWin" src="/${projectName}/examination/exerPaper/page.action?page=exerPaper_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#exerPaperEditButton'" title="修改学生答题试卷" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="exerPaperEditButton">
	<a href="javascript:void(0)" onclick="$('#exerPaperEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#exerPaperEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >取 消</a>
</div>
<%-- 查看面板 --%>
<iframe id="exerPaperViewWin" src="/${projectName}/examination/exerPaper/page.action?page=exerPaper_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#exerPaperViewButton'" title="查看学生答题试卷" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="exerPaperViewButton">
	<a href="javascript:$('#exerPaperViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >关 闭</a>
</div>
</body>
</html>
