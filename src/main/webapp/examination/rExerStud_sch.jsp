<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>练习与班级关系表查询</title><!--rExerStud-->
	<%@include file="/common/include.jsp" %>

<script>
$(function(){
	<%-- 为分页行设置"添加"和"删除"按钮 --%>
	var pager = $('#rExerStudGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});			
})

<%-- 执行查询，点击查询按钮时调用 --%>
function doSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#rExerStudGrid').datagrid('load',{
		eid:$('#eid').val(),
		studid:$('#studid').val(),
		classid:$('#classid').val(),
		type:$('#type').val(),
		name:$('#name').val(),
		status:$('#status').val()
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatOper(val,row,index){  
    return "<span onclick='updRExerStud("+val+")' style='text-decoration:underline;cursor:pointer;'>修改</span> "
    			+"<span onclick='vieRExerStud("+val+")' style='text-decoration:underline;cursor:pointer;'>查看</span>";
} 

<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
function updRExerStud(id){
	QMoveDialog($("#rExerStudEditWin"));
	$('#rExerStudViewWin').dialog('close');
	$('#rExerStudEditWin').dialog('open');
	$('#rExerStudEditWin').get(0).contentWindow.load(id);
} 

<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
function vieRExerStud(id){
	QMoveDialog($("#rExerStudViewWin"));
	$('#rExerStudEditWin').dialog('close');
	$('#rExerStudViewWin').dialog('open');
	$('#rExerStudViewWin').get(0).contentWindow.load(id);
}

<%-- 删除记录 --%>
function delRExerStud(){
	var checkedItems = $('#rExerStudGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/examination/rExerStud/delRExerStud.action",{ids:ids},function(data){
				$('#rExerStudGrid').datagrid('reload');
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#rExerStudEditWin').dialog('close');
	$('#rExerStudGrid').datagrid('reload');
}
</script>
</head>
<body>
<table id="rExerStudGrid" title="练习与班级关系表" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#rExerStudGridToolbar',collapsible:true,url:'/${projectName}/examination/rExerStud/schRExerStud.action',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" ></th>
			<th data-options="field:'eid',width:25,align:'center',sortable:true">练习编号</th>
			<th data-options="field:'studid',width:25,align:'center',sortable:true">学生</th>
			<th data-options="field:'classid',width:25,align:'center',sortable:true">班级</th>
			<th data-options="field:'type',width:25,align:'center',sortable:true">类型</th>
			<th data-options="field:'name',width:25,align:'center',sortable:true">标题</th>
			<th data-options="field:'markTeacher',width:25,align:'center',sortable:true">阅卷老师</th>
			<th data-options="field:'status',width:25,align:'center',sortable:true">阅卷状态</th>
			<th data-options="field:'mark',width:25,align:'center',sortable:true">得分</th>
			<th data-options="field:'remark',width:25,align:'center',sortable:true">备注</th>

			<th data-options="field:'_oper',align:'center',formatter:formatOper">操作</th>
		</tr>
	</thead>
</table>
<%-- 查询条件设定栏 --%>
<div id="rExerStudGridToolbar">
	<div style="margin-bottom:5px">
		<a href="javascript:updRExerStud(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true">添加</a>
		<a href="javascript:delRExerStud()" class="easyui-linkbutton" iconCls="icon-remove" plain="true">删除</a>
	</div>
	<form id="_form">
	练习编号：<input name='eid' id="eid" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	学生：<input name='studid' id="studid" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	班级：<dc:insertList name="classid" dictId="CC_CLASSES" style="combo_normal" />
	类型：<input name='type' id="type" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	标题：<input name='name' id="name" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	阅卷状态：<dc:insertList name="status" dictId="ZD_SFBZ" style="combo_normal" />

	<a href="#" class="easyui-linkbutton" iconCls="icon-search" onclick="doSearch()">查询</a>
	</form>
</div>


<%-- 修改or添加面板 --%>
<iframe id="rExerStudEditWin" src="/${projectName}/examination/rExerStud/page.action?page=rExerStud_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#rExerStudEditButton'" title="修改练习与班级关系表" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="rExerStudEditButton">
	<a href="javascript:void(0)" onclick="$('#rExerStudEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#rExerStudEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >取 消</a>
</div>
<%-- 查看面板 --%>
<iframe id="rExerStudViewWin" src="/${projectName}/examination/rExerStud/page.action?page=rExerStud_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#rExerStudViewButton'" title="查看练习与班级关系表" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="rExerStudViewButton">
	<a href="javascript:$('#rExerStudViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >关 闭</a>
</div>
</body>
</html>
