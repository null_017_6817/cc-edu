<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<%@include file="/common/include.jsp" %>
	<link rel="stylesheet" type="text/css"	href="/${projectName}/book/css/common.css" />
	<link rel="stylesheet" type="text/css"	href="/${projectName}/book/css/fonts.css" />
	<link rel="stylesheet" type="text/css"	href="/${projectName}/book/css/style.css" />
	<script type="text/javascript" src="/${projectName}/book/js/lib/jquery-1.9.1.min.js"></script>
	<script type="text/javascript"	src="/${projectName}/book/js/sweetAlert/sweetalert2.min.js"></script>
	<link rel="stylesheet" type="text/css"	href="/${projectName}/book/js/sweetAlert/sweetalert2.min.css" />
	<link rel="stylesheet" href="/${projectName}/book/js/layui/js/lib/layui2.3/css/layui.css">
	<link rel="stylesheet" type="text/css"	href="/${projectName}/book/js/layui/css/style.css">
	<script src="/${projectName}/book/js/layui/js/lib/layui2.3/layui.all.js"></script>
	<script src="/${projectName}/common/artDialogs/artDialog.source.js?skin=default" type="text/javascript"></script>
<script src="/${projectName}/common/artDialogs/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script>
	//banner - 书本名称
$(function(){
	var bookRootId = '${bookRoot.id}';
	//clickTask("${bookId}");
	//var t = setTimeout("clickTask(${bookId})", 200);
});
	
	var projectName = '${projectName}';
	 //iframe自适应高度的函数
        var oTime = null;
        function resize() {
            if (oTime) {
                clearTimeout(oTime);
            }
            oTime = setTimeout(reset, 200);
        }
 
        //iframe自适应高度的函数
        function reset() {
            var frame = document.getElementById("iframe"),
                outHeight = frame.offsetHeight,
                inHeight = frame.contentWindow.document.body.scrollHeight,
                h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight,
                ifHeight = h-($(".header").height() + $(".footer").height() + 207);
                
            if (outHeight < inHeight) {
                frame.style.height = (inHeight + 10) + "px";
            }else if(inHeight>650){
                frame.style.height=(inHeight + 10) + "px";
            }else{
                frame.style.height= ifHeight + "px";
            }
        }
	
	function showAudioGif(){
		layer.open({
			title:'正在录音'
			,type: 1
			,closeBtn :0
			,anim: 1
			,offset: 'auto' //具体配置参考：http://www.layui.com/doc/modules/layer.html#offset
			,id: 'layerDemo'//防止重复弹出
			,content: "<img width='200px' src='/${projectName}/book/img/audio-start.gif' ></img>"
			,btn: '点击停止'
			,btnAlign: 'c' //按钮居中
			,shade: 0.05 //不显示遮罩
			,yes: function(){
				var childWindow = $("#iframe")[0].contentWindow; //表示获取了嵌入在iframe中的子页面的window对象。  []将JQuery对象转成DOM对象，用DOM对象的contentWindow获取子页面window对象。
				childWindow.stopRecording();  //调用子页面中的subFunction方法。
			},cancel : function(){
				alert("取消");
			}
		});
	}

	
	function closeAll(){
		layer.closeAll();
	}
	
	function showRemark(id){
		//多窗口模式，层叠置顶
	    /* layer.open({
	      type: 2 //此处以iframe举例
	      ,title: '背景信息'
	      ,area: ['500px', '400px']
	      ,shade: 0
	      ,maxmin: true
	      ,content: '/${projectName }/examination/topicPremise.jsp?id='+id
	      ,btn: '关闭背景窗口' //只是为了演示
	      ,yes: function(){
	        layer.closeAll();
	      }
	      ,zIndex: layer.zIndex //重点1
	    }); */
		
		
	    layer.open({
			title:'背景信息'
			,type: 2
			,closeBtn :0
			,area: ['500px', '400px']
			,anim: 1
			,offset: 'auto' //具体配置参考：http://www.layui.com/doc/modules/layer.html#offset
			,id: 'layerDemo'//防止重复弹出
			,content: '/${projectName }/examination/topicPremise.jsp?id='+id
			,btn: '关闭背景信息'
			,btnAlign: 'c' //按钮居中
			,shade: 0 //不显示遮罩
			,yes: function(){
				var childWindow = $("#iframe")[0].contentWindow; //表示获取了嵌入在iframe中的子页面的window对象。  []将JQuery对象转成DOM对象，用DOM对象的contentWindow获取子页面window对象。
				childWindow.stopRecording();  //调用子页面中的subFunction方法。
			},cancel : function(){
				alert("取消");
			}
		});
		
		
	}

	window.aat;
	function showLoading(){
		aat =art.dialog({opacity:0.1,
			title:"上传中...",
			lock:true,
			content:$("#loadDiv").html(),
			cancel:false
		});
	}
	
	function closeLoading(){
		window.aat.close();
	}
	
	
</script>
</head>
<body>
<div id="loadDiv" style="display:none" >
	<div><img src="/${projectName}/book/img/loading.gif" /></div>
</div>
	<div class="wrap">
		
		<jsp:include page="../book/hy_header.jsp" />

		<div class="banner bg height">
			<div class="inner padding">
				<img src="/${projectName}/book/img/exambg.png" />
				<div class="examtTime">
					<h4>${exer.name}</h4>
					<span id="examtime"></span>
					<ul>
						<li>发起人：${exer.teacherName}</li>
						<li>考试时间：${diff}分钟</li>
					</ul>
				</div>
			</div>
		</div>

		<div class="main bg" style="padding: 0;">
			<div class="inner">
				<div class="tabsBox clearfix" id="tabs-basic">
					<div class="tabsInfo">
						<div class="con tab-content-active">
							<form id="_form">
								<iframe id="iframe" src="/${projectName }/examination/doexer.jsp?type=exam&eid=${eid}&etid=${etid}"
									 scrolling="auto" frameborder="0" style="width: 100%;min-height:700px;"></iframe>
							</form>
						</div>
						
						<div id="noData" style="text-align:center;display:none;height:500px;padding-top: 50px;">
							<img height="350" width="320" src="/${projectName}/imgs/common/nodata.png" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<jsp:include page="../book/_footer.jsp" />
	</div>
	
<script>
var topicStepIdsMap = "";
var eid = "${eid}";
var etid = "${etid}";
var studId = "${studId}";

$(function(){
	$.ajax({
		url:"/${projectName }/examination/exer/schExerById.action?id=" + eid,
		success: function(data){
			//console.log(eid);
			data = eval("(" + data + ")");
			var serverTime = new Date(data.serverTime.replace(/-/g, "/"));
			var endTime = new Date(data.endTime.replace(/-/g, "/"));
			console.log("结束" + endTime);
			var alarm = new Alarm(serverTime, endTime, function (second, minute, hour, day) { //计时钟
				$("#examtime").empty();
				if(day == 0) {
					$("#examtime").append(hour + ": " + minute + ": " + second);
				}else{
					$("#examtime").append(day + "天" + hour + ": " + minute + ": " + second);
				}
            }, function () { //倒计时结束
           	 	window.location.href =  "/${projectName }/book/index.jsp";
            });
			alarm.start();
		}
	});
});

var Alarm = function (startime, endtime, countFunc, endFunc) {
	
    this.time = Math.floor((endtime - startime) / 1000); //时间
    this.countFunc = countFunc; //计时函数
    this.endFunc = endFunc; //结束函数
    this.flag = 't' + Date.parse(new Date()); //
};
Alarm.prototype.start = function () {
    var self = this;
	
    self.flag = setInterval(function () {
        if (self.time < 0) {
            clearInterval(self.flag);
            self.endFunc();
        } else {
            var minute, hour, day, second;
            day = Math.floor(self.time / 60 / 60 / 24) < 10 ? '0' + Math.floor(self.time / 60 / 60 / 24) : Math.floor(self.time / 60 / 60 / 24);
            hour = Math.floor(self.time / 60 / 60 % 24) < 10 ? '0' + Math.floor(self.time / 60 / 60 % 24) : Math.floor(self.time / 60 / 60 % 24);
            minute = Math.floor(self.time / 60 % 60) < 10 ? '0' + Math.floor(self.time / 60 % 60) : Math.floor(self.time / 60 % 60);
            second = Math.floor(self.time % 60) < 10 ? '0' + Math.floor(self.time % 60) : Math.floor(self.time % 60);
            //倒计时执行函数
            self.countFunc(second, minute, hour, day);
            self.time--;
        }
    }, 1000);
}
</script>

</body>
</html>
