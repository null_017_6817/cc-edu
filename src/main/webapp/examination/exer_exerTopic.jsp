<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>习题查询</title>
	<%@include file="/common/include.jsp" %>
	<style>
	#topicTable{
		width: 100%;
	}
	#topicTable td.label {
		width: 50px;
	}
	#topicTable td {
		height: 26px;
		line-height: 20px;
	}
	#viewStep table {
		width:100%;
	}
	#viewStep td {
		padding: 15px 10px;	
	}
	/* #viewStep tr {
		border-bottom: 1px #333 dashed;
	} */
	#viewStep audio {
		width: 260px;
		margin-top: 8px;
	}
	#viewStep textarea {
		width: 99%;
		outline: none;
	}
	span[name='stepTitleNum']{
		width: 20px;
		height: 20px;
		background-color: #38de38;
		display: block;
		text-align: center;
		font-size: 10px;
		margin-left: 35px;
		border-radius: 6px;
		vertical-align: middle;
		line-height: 19px;
	}
	
	</style>
<script type="text/javascript" src="/${projectName}/easyui1_5/datagrid-dnd.js"></script>
<script type="text/javascript"	src="/${projectName}/examination/templateView/js/showStep.js"></script>
<script src="/${projectName}/book/js/layui/js/lib/layui2.3/layui.all.js"></script>
<script>
var projectName = "${projectName}";
var stuNum = "${stuNum}";
$(function(){
	<%-- 为分页行设置"添加"和"删除"按钮 --%>
	var pager = $('#exerTopicGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});
	$('#exer_form').form('load','/${projectName}/examination/exer/vieExer.action?id=${exerId}');
	
	//可选-绑定dnd的触发事件
	$('#exerTopicGrid').datagrid({
		onDrop:function(targetRow, sourceRow, point) {
			var tarId = targetRow.id;
			var souId = sourceRow.id;
		}
	})
})

<%-- 执行查询，点击查询按钮时调用 --%>
function doSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#exerTopicGrid').datagrid('load',{
		title:$("#title").val(),
		profession:professionDict.getValue(),
	});
}

<%-- 打开修改页面，点击记录行的"修改"链接时调用 (exerId为练习ID) --%>
function updExerTopic(exerId){
	if (stuNum > 0) {
		$.messager.alert('提示','已发卷试卷不允许重新维护','warning');
		return;
	}
	
	$("#tChoose").layout('collapse','east');
	$('#w').window('open');	
	$('#dg1').datagrid('loadData', { total: 0, rows: [] }); 
	$('#dg').datagrid({
		url : '/${projectName}/manager/bank/schBank.action?status=1',
		onSelect:function(rowIndex, rowData){
			var row = $('#dg').datagrid('getSelected');
			var opts = $('#dg1').datagrid("options");
			opts.url = '/${projectName}/manager/bankTopic/schBankTopicNotExer.action';
			$('#dg1').datagrid('load',{
				exerId:exerId,
				bankId:row.bankId
			});
						
			$('#dg1').datagrid({
				onClickRow:function(rowIndex, rowData){
					$.post('/${projectName}/manager/bankStep/schBankStep.action', {
						bankId : rowData.bankId,
						topicId: rowData.topicId
					}, function(result) {
						
					}, 'json');}
			});
		}
	});
} 

<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
function vieExerTopic(id){
	QMoveDialog($("#exerTopicViewWin"));
	$('#exerTopicEditWin').dialog('close');
	$('#exerTopicViewWin').dialog('open');
	$('#exerTopicViewWin').get(0).contentWindow.load(id);
}

<%-- 删除记录 --%>
function delExerTopic(id){
	if (stuNum > 0) {
		$.messager.alert('提示','已发卷试卷不允许重新维护','warning');
		return;
	}
	
	var ids = [];
	ids.push(id);
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/examination/exerTopic/delExerTopicAndStep.action",{ids:ids},function(data){
				$('#exerTopicGrid').datagrid('reload');
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#exerTopicEditWin').dialog('close');
	$('#exerTopicGrid').datagrid('reload');
}

// 查询题库
function searchClass1() {
	var opts = $('#dg').datagrid("options");
	opts.url = '/${projectName}/manager/bank/schBank.action?status=1';
	$('#dg').datagrid('load',{
		bankName:$("#searchForm1").find("input[name='bankName']").val(),
		profession:$("#searchForm1").find("input[name='profession']").val()
	});
}

// 查询题
function searchClass2(exerId){
	var row = $('#dg').datagrid('getSelected');
	var opts = $('#dg1').datagrid("options");
	opts.url = '/${projectName}/manager/bankTopic/schBankTopicNotExer.action';
	$('#dg1').datagrid('load',{
		exerId:exerId,
		bankId:row.bankId,
		bankName:$("#searchForm2").find("input[name='bankName']").val(),
		profession:$("#searchForm2").find("input[name='profession']").val(),
		title:$("#searchForm2").find("input[name='title']").val()
	});
}


function searchByAllBank(exerId){
	var rows = $("#dg").datagrid("getRows");
	for( var i = 0 ; i < rows.length; i++ ){
		$('#dg').datagrid('unselectRow',i)
	}
	var opts = $('#dg1').datagrid("options");
	opts.url = '/${projectName}/manager/bankTopic/schBankTopicNotExer.action';
	$('#dg1').datagrid('load',{
		exerId:exerId,
		profession:$("#searchForm2").find("input[name='profession']").val(),
		title:$("#searchForm2").find("input[name='title']").val()
	});
}

// 保存Topic数据
function saveExamTopic(){
	var row = $('#dg1').datagrid('getSelections');
	if(row != null && typeof(row) != "undefined" && row.length > 0){
		var ids = "";
		for( var i = 0; i < row.length; i++ ){
			ids = ids + "," + row[i].topicId;
		}
		$.ajax({
			type:"POST",
			url:'/${projectName}/examination/exerTopic/copyTopic.action',
			data:{"ids":ids,"eid":'${exerId}'},
			dataType:'json',
			async:false, 
			success:function(result){
				if(result != null){
					$('#w').window('close');	
					$.messager.show({
						title : ' ',
						msg : result.msg
					});
				}
				$('#exerTopicGrid').datagrid('reload');
			}
		});
	} else {
		$('#w').window('close');
		$.messager.show({
			title : '未选择',
			msg : '未选择数据'
		});
	}
}

// 预览按钮
function previewOpt(val,row,index){
	return "&emsp;<a href='javascript:void(0)' onclick=\"preview('"+val+"','Bank')\" class='_oper' title='预览'>&nbsp;<i class='icon-eye-open'></i></a>&emsp;";
}

// 试卷预览
function viewExerStep(val,row,index){
	var str = "&emsp;<a href='javascript:void(0)' onclick=\"preClick('"+val+"','Exer')\" class='_oper' title='预览'>&nbsp;<i class='icon-eye-open'></i></a>&emsp;"  
		+"&emsp;<a href='javascript:void(0)' onclick=\"delExerTopic('"+val+"','"+index+"')\" class='_oper' title='删除'>&nbsp;<i class='icon-trash'></i></a>&emsp;";
	return str;
}

// 点击预览按钮（试卷）
function preClick(id,elementId){
	$('#viewTopicWindow').window('open');
	preview(id,elementId);
	$("#tChoose").layout('expand','east');
	$("#viewStepExer").html("");// 清空展示
	$("#viewStepBank").html("");// 清空展示
	$.ajax({
		type:"POST",
		url:'/${projectName}/examination/exerTopic/previewTopic.action',
		data:{"id":id},
		dataType:'json',
		success:function(result){
			if(result.success){
				var topic = result.topic;
				var topicTypeMap = result.topicTypeMap;
				var stepTypeMap = result.stepTypeMap;
				if(topic != null){
					topic.topicTypeMap = topicTypeMap;
					topic.stepTypeMap = stepTypeMap;
				}
				// 回显topic
				var stepInfoList = result.steps;
				showTopic(topic,stepInfoList,elementId);
			}else{
				alert(result.msg);
			}
		}
	});	
}

// 预览题目(题库)
/* var stepData=[]; */
function preview(id,elementId){
	stepData=[];
	$("#tChoose").layout('expand','east');
	$("#viewStepExer").html("");// 清空展示
	$("#viewStepBank").html("");// 清空展示
	$.ajax({
		type:"POST",
		url:'/${projectName}/paper/question/getTopicAndStep.action',
		data:{"topicId":id},
		dataType:'json',
		success:function(result){
			if(result.success){
				var data = result.data;
				var topic = data.topic;
				var topictype = topic.type;
				var topicTypeMap = result.topicTypeMap;
				var stepTypeMap = result.stepTypeMap;
				if(topic != null){
					topic.topicTypeMap = topicTypeMap;
					topic.stepTypeMap = stepTypeMap;
				}
				// 回显topic
				var stepInfoList = data.stepInfoList;
				showTopic(topic,stepInfoList,elementId);
			}else{
				alert(result.msg);
			}
		}
	});	
}

// 显示topic
function showTopic(topic,stepInfoList,elementId){
	if( topic != null && typeof(topic) != "undefined" ){
		var topicTypeMap = topic.topicTypeMap;
		var stepTypeMap = topic.stepTypeMap;
		var topicForm = $('#topicTable'+elementId);
		topicForm.find("span[name='title']").html(topic.title);// title
		topicForm.find("span[name='type']").html(topicTypeMap[topic.type]);// type
		topicForm.find("span[name='keyWords']").html(topic.keyWords);// keyWords
		/* topicForm.find("span[name='totalScore']").html(topic.totalScore);// totalScore */
		topicForm.find("span[name='remark']").html(topic.remark);// remark
		var topictype = topic.type;
		if(topictype == "实训营地"){
			ss = stepInfoList;
		}else{
			// 回显step
			showStepList(stepInfoList,elementId);
		}
	}
}

function sleep(n) {
    var start = new Date().getTime();
    //  console.log('休眠前：' + start);
    while (true) {
        if (new Date().getTime() - start > n) {
            break;
        }
    }
    // console.log('休眠后：' + new Date().getTime());
}

// 拼接步骤
function appendStep(elementId){
	$("#viewStep"+elementId).html("");
	if(stepData != null){
		for(var i = 0; i < stepData.length; i++){
			stepData[i].find("span[name='stepTitleNum']").parents("table").attr("class","");
			stepData[i].find("span[name='stepTitleNum']").html((i+1));
			$("#viewStep"+elementId).append(stepData[i]);
			$("#viewStep"+elementId).append("<hr style='border: 1px dashed #cdcfd0;'>");
		}
	}
	sleep(200);
	
	$("#viewStep"+elementId).find("audio").each(function(){
		var src = $(this).find("source").attr("src");
		if( src != null && src != '' && typeof(src) != "undefined" ){
			/* if(src.indexOf('.wav') != -1 || src.indexOf('.WAV') != -1 ){
				$(this).parent("p").parent("td").find("button[name='audButton']").trigger("click");
			} */
			if(src.indexOf('.wav') != -1 || src.indexOf('.WAV') != -1 || src.indexOf('.MP3') != -1
					|| src.indexOf('.mp3') != -1 || src.indexOf('.MP3') != -1){
				$(this).parent("p").parent("td").find("button[name='audButton']").trigger("click");
			}
		}
	});
	$("#viewStep"+elementId).find("tr[name='downStudTr']").each(function(){
		$(this).hide();
	});
	$("#viewStep"+elementId).find("button").hide().next("br").remove();
}

// ajax 加载中
function ajaxLoading(){   
	$("<div class=\"datagrid-mask\"></div>").css({display:"block",width:"100%",height:$(window).height()}).appendTo("body");   
	$("<div class=\"datagrid-mask-msg\"></div>").html("正在处理，请稍候。。。").appendTo("body").css({display:"block",left:($(document.body).outerWidth(true) - 190) / 2,top:($(window).height() - 45) / 2});   
}
// ajax 加载中结束
function ajaxLoadEnd(){   
	$(".datagrid-mask").remove();
	$(".datagrid-mask-msg").remove();
}

// 保存试卷和topic
function saveExerAndTopic(){
	if(!$("#exer_form").form("validate")){
		return;
	}
	var rows = $("#exerTopicGrid").datagrid("getRows"); 
	var ids = new Array();
	for(var i=0;i<rows.length;i++){
		if(rows[i] != null && typeof(rows[i]) != "undefined" 
				&& rows[i].id != null && typeof(rows[i].id) != "undefined" ){
			ids.push(rows[i].id);
		}
	}
	var obj = new Object();
	obj.topicIds = ids;
	$("#exer_form").find("input[type='hidden']").each(function(i,item){
		var nameStr= $(this).attr("name");
		if(nameStr != 'topicIds'){
			obj[nameStr] = $(this).val();
		}
	});
	obj.typeSort=new Array();
	obj.typeSortStr="";
	obj.numParam = new Object();
	obj.bankIds = new Array();
	obj.bankIdsStr = "";
	var param = JSON.stringify(obj);
	
	//var param = $("#exer_form").serialize();
	$.ajax({
		type:"POST",
		contentType : "application/json;charset=UTF-8",
		url:'/${projectName}/examination/exer/saveExerAndTopic.action',
		data: param ,
		dataType:'json',
		beforeSend: ajaxLoading, 
		success:function(result){
			if(result.success){
				ajaxLoadEnd();// 结束遮罩
				parent.mainShow('提示','保存成功',250,90,1500);
				window.location.href="/${projectName}/examination/exer/page.action?page=exer_sch";
			}else{
				$.messager.alert('提示',result.msg,'warning');
			}
		}
	}); 
}

//保存试卷和topic
function cancelExerAndTopic(){
	var id = "${exerId}";
	$.ajax({
		type:"POST",
		url:'/${projectName}/examination/exer/cancelExerAndTopic.action',
		data: {"id":id},
		dataType:'json',
		success:function(result){
			if(result.success){
				
				window.location.href="/${projectName}/examination/exer/page.action?page=exer_sch";
			}else{
				$.messager.alert('提示',result.msg);
			}
		}
	});
}

// 批量删除题录
function deleteTopics(){
	if (stuNum > 0) {
		$.messager.alert('提示','已发卷试卷不允许重新维护','warning');
		return;
	}
	
	var checkedItems = $('#exerTopicGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		//$.messager.alert('敬告','请选择删除的记录？');
		layer.msg('敬告,请选择删除的记录');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.ajax({
				type : "POST",
				url : '/${projectName}/examination/exerTopic/delExerTopicAndStep.action',
				data : {ids:ids},	
				async: false,
				dataType:'json',
				success : function(result) {
					reloadData();
				}
			});
		}
	});
}

// 
function updataBookId(){
	if (stuNum > 0) {
		$.messager.alert('提示','已发卷试卷不允许重新维护','warning');
		return;
	}
	
	var checkedItems = $('#exerTopicGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		//$.messager.alert('敬告','请选择删除的记录？');
		layer.msg('敬告,请选要修改的记录');
		return;
	}
	
	var bookId = bookIdDict.getValue();
	
	$.messager.confirm('敬告','确定要修改吗？若未选择章节则将批量更新成空',function(r){
		if(r){
			$.ajax({
				type : "POST",
				url : '/${projectName}/examination/exerTopic/updBookIdByIds.action?bookId='+bookId,
				data : {ids:ids},	
				async: false,
				dataType:'json',
				success : function(result) {
					reloadData();
				}
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#exerTopicGrid').datagrid('reload');
}

// 保存答题要求成功
function closeAnswerReqDia(msg){
	layer.msg(msg);
	layer.close(index);
}

// 弹出框ID
var index;
function showAnsReq(t){
	
	var id = $(t).parents("form").find("input[name='id']").val();
	
	//多窗口模式，层叠置顶
	index = layer.open({
		type: 2 //此处以iframe举例
		,title: '答题要求'
		,closeBtn :1
		,area: ['800px', '550px']
		,resize: true
		,maxmin: true 
		,content: '/'+projectName+'/examination/answerReq/answerReq.jsp?id='+id
		,btn: '保存'
		,btnAlign: 'c' //按钮居中
		,shade: 0.1 //不显示遮罩
		,yes: function(){
			//layer.closeAll();
			var fh = window["layui-layer-iframe" + index].saveData(id);
			$(bjButton).next('input').val(fh);
			layer.close(index);
		},cancel : function(){
			
		}
	});
	
}

</script>
</head>
<body>
	<table id="exerTopicGrid" fitcolumns=true
		data-options="singleSelect:false,pagination:false,onLoadSuccess:function(){if(stuNum == 0) {$(this).datagrid('enableDnd');}},rownumbers:true,fit:true,striped:true,toolbar:'#exerTopicGridToolbar',collapsible:true,
		url:'/${projectName}/examination/exer/schExerTopic.action?eid=${exerId}',method:'get'">
		<thead>
			<tr>
				<th data-options="field:'checkbox',checkbox:true" ></th>
				<th data-options="field:'bankId',width:25,align:'center',sortable:true">题库编号</th>
				<th data-options="field:'title',width:25,align:'center',sortable:true">题目名称</th>
				<th data-options="field:'typeLabel',width:25,align:'center',sortable:true">类型</th>
				<th data-options="field:'professionLabel',width:25,align:'center',sortable:true">行业</th>
				<th data-options="field:'difficultyLabel',width:25,align:'center',sortable:true">难度</th>
				<th data-options="field:'totalScore',width:25,align:'center',sortable:true">分值</th>
				<th data-options="field:'bookLabel',width:25,align:'center',sortable:true">章节名称</th>
				<th data-options="field:'_oper',align:'center',formatter:viewExerStep">操作</th>
			</tr>
		</thead>
	</table>
<%-- 查询条件设定栏 --%>
<div id="exerTopicGridToolbar">
	<div style="margin-bottom:5px">
		<a href="javascript:updExerTopic('${exerId}');" class="btn btn-orange"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">加入题目</span></a>&emsp;
		<a href="javascript:void(0)" onclick="saveExerAndTopic();" class="btn"><span class="icon-ok" style="font-size:16px"></span> <span style="font-size:14px">保存</span></a>&emsp;
		<a href="javascript:void(0)" onclick="cancelExerAndTopic();" class="btn"><span class="icon-cancel" style="font-size:16px"></span> <span style="font-size:14px">取消</span></a>&emsp;
		<a href="javascript:void(0)" onclick="deleteTopics();" class="btn"><span class="icon-remove" style="font-size:16px"></span> <span style="font-size:14px">批量删除题目</span></a>&emsp;
		<a href="javascript:void(0)" onclick="updataBookId();" class="btn"><span class="icon-edit" style="font-size:16px"></span> <span style="font-size:14px">选择并保存章节</span></a>
		<dc:insertTree name="bookId" dictId="CC_BOOK" style="asynch_single" value="1"/>
	</div>
	<form id="exer_form" method="post" action="/${projectName}/examination/exer/updExer.action">
		<input type="hidden" name='id' id='id'/>
		<input type="hidden" name='topicIds' id='topicIds'/>
		<table>
			<tr>
				<td class='label'>试卷名称：</td>
				<td><input id="name" name="name" class="easyui-textbox" data-options="required:true"/><span color="red">*</span></td>
				<!-- <td class='label'>及格分值：</td>
				<td><input id="passScore" name="passScore" class="easyui-textbox"  data-options="validType:['int']"/></td> -->
				<td class='label'>教研室：</td>
				<td><input id="department" name="department" class="easyui-textbox" data-options=""/></td>
				<td class='label'>显示答案：</td>
				<td><dc:insertList name="showStandard" dictId="ZD_SFBZ" style="combo_normal" required="true"/><span color="red">*</span></td>
				<td class='label'>答题要求：</td>
				<td><button type="button" onclick="showAnsReq(this);">编辑</button></td>
				
			</tr>
			<tr>
				<td class='label'>开始时间：</td>
				<td><input id="startTimestr" name="startTimestr" class="easyui-datetimebox" data-options="required:true"/><span color="red">*</span></td>
				<td class='label'>结束时间：</td>
				<td><input id="endTimestr" name="endTimestr" class="easyui-datetimebox" data-options="required:true"/><span color="red">*</span></td>
				<!-- <td class='label'>总分值：</td>
				<td><input id="totalScore" name="totalScore" class="easyui-textbox" data-options="validType:['int']"/></td> -->
				<td class='label'>分类目录：</td>
				<td><dc:insertTree name="shortcut" dictId="ZD_EXAM_CATEGORY" style="synch_single" required="true"/><span color="red">*</span></td>
			</tr>
		</table>
	</form>
</div>


<%-- 修改or添加面板 --%>
<iframe id="exerTopicEditWin" src="/${projectName}/examination/exerTopic/page.action?page=exerTopic_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#exerTopicEditButton'" title="修改习题" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="no" frameborder="0"></iframe>
<div id="exerTopicEditButton">
	<a href="javascript:void(0)" onclick="$('#exerTopicEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#exerTopicEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >取 消</a>
</div>
<%-- 查看面板 --%>
<iframe id="exerTopicViewWin" src="/${projectName}/examination/exerTopic/page.action?page=exerTopic_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#exerTopicViewButton'" title="查看习题" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="exerTopicViewButton">
	<a href="javascript:$('#exerTopicViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >关 闭</a>
</div>

<div id="winTopicToolbar" style="padding:5px;height:auto">
	<div>
	<form id="searchForm2">
		 行业:<input name="profession" style="width:80px">
		题目名称:<input name="title"  style="width:80px">
		<a href="javascript:void(0)" onclick="searchClass2(${exerId})" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>&emsp;
		<a href="javascript:void(0)" onclick="searchByAllBank(${exerId})" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">全库查询</span></a>
	</form>
	</div>
</div>

<div id="winToolbar" style="padding:5px;height:auto">
	<div>
	<form id="searchForm1">
		题库名称:<input name="bankName"  style="width:80px">
		行业:<input name="profession" style="width:80px">
		<a href="javascript:void(0)" onclick="searchClass1()" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>
	</form>
	</div>
</div>

<!-- 选择题库和题目部分 -->
<div id="w" class="easyui-window" title="选择题库" inline="true"
	data-options="modal:true,iconCls:'icon-save'" style="width:90%;height:50%;top:5%;"
	closed="true">
	<div id="tChoose" class="easyui-layout" fit="true">
		<div data-options="region:'west',split:true" title="题库列表" style="width:35%;height:30%;">
			<table id="dg" class="easyui-datagrid" 
			pagination="true" ,
			toolbar = "#winToolbar",
			pageSize = "10",
			fitColumns = "true",
			fit = "true",
			fitColumns="true",
			singleSelect="true"
			data-options="collapsible:true,region:'center',border:false" style="overflow: hidden;">
				<thead>
					<tr>
						<th field="ck" checkbox="true"></th>
						<th field="bankId" width="25">题库编号</th>
						<th field="bankName" width="25">题库名称</th>
						<th field="profession" width="25">行业</th>
						<th field="level" width="25">级别</th>
						<th field="status" width="25">是否启用</th>
						<th field="type" width="25">数据</th>
					</tr>
				</thead>
			</table>
		</div>
		<div data-options="region:'center',split:true" title="题目列表" style="width:65%;">
			<table id="dg1" class="easyui-datagrid"
				pagination="true" ,
				toolbar = "#winTopicToolbar",
				pageSize = "10",
				fitColumns = "true",
				fit = "true",
				fitColumns="true",
				data-options="region:'center',border:false" style="overflow: hidden;">
				<thead>
					<tr>
						<th field="ck" checkbox="true"></th>
						<th field="title" width="30" sortable="true">题目名称</th>
						<th field="typeName" width="25">类型</th>
						<th field="professionLabel" width="25">行业</th>
						<th field="difficultyLabel" width="15">难度</th>
						<th field="totalScore" width="15">分值</th>
						<th field="bookName" width="15">章节</th>
						<th data-options="field:'_oper',align:'center',formatter:previewOpt">操作</th>
					</tr>
				</thead>
			</table>
		</div>
		<div data-options="region:'east',split:true,collapse:true" title="预览" style="width:30%;">
			<div id="viewTopicBank">
				<table id="topicTableBank" style="border-bottom: 1px #e5e5e5 solid;">
					<tr><td class="label"><b>题目标题：</b></td><td><span name="title"></span></td></tr>
					<!-- <tr><td class="label"><b>类型：</b></td><td><span name="type"></span></td></tr>
					<tr><td class="label"><b>关键字：</b></td><td><span name="keyWords"></span></td></tr>
					<tr><td class="label"><b>备注：</b></td><td><span name="remark"></span></td></tr> -->
				</table>
			</div>
			<div id="viewStepBank">
			
			</div>
		</div>
		<div region="south" border="false"
			style="text-align:center;height:30px;line-height:30px;">
			<a class="easyui-linkbutton" icon="icon-ok" href="javascript:void(0)" onclick="saveExamTopic();">确认选择</a>
			<a class="easyui-linkbutton" icon="icon-cancel"
				href="javascript:$('#w').window('close')">取消</a>
		</div>
	</div>
</div>

<!-- 预览题目 -->
<div id="viewTopicWindow" class="easyui-window" title="预览题目" inline="true"
	data-options="modal:true,iconCls:'icon-save'" style="width:550px;height:50%;top:5%;"
	closed="true">
	<div  class="easyui-layout" fit="true">
		<div data-options="region:'center',split:true" style="width:100%;">
			<div id="viewTopicExer">
				<table id="topicTableExer" style="border-bottom: 1px #e5e5e5 solid;">
					<tr><td class="label"><b>标题：</b></td><td><span name="title"></span></td></tr>
					<tr><td class="label">类型：</td><td><span name="type"></span></td></tr>
					<tr><td class="label">关键字：</td><td><span name="keyWords"></span></td></tr>
					<tr><td class="label">备注：</td><td><span name="remark"></span></td></tr>
				</table>
			</div>
			<div id="viewStepExer">
			</div>
		</div>
		<div region="south" border="false"
			style="text-align:center;height:30px;line-height:30px;">
			<a class="easyui-linkbutton" icon="icon-cancel"
				href="javascript:$('#viewTopicWindow').window('close')">关闭</a>
		</div>
		
	</div>
</div>

<div style="display:none;">
	<!-- 单选题(语音) -->
	<jsp:include page="./templateView/select-temp.jsp" />
	<!-- 选择题(语音) -->
	
	<!-- 多选题(语音) -->
	<jsp:include page="./templateView/selects-temp.jsp" />
	<!-- 选择题(语音) -->
	
	<!-- 听打录入 -->
	<jsp:include page="./templateView/cloze-temp.jsp" />
	<!-- 听打录入 -->
	
	<!-- 填空题 -->
	<jsp:include page="./templateView/clozed-temp.jsp" />
	<!-- 填空题 -->
	
	<!-- 问答题（语音） -->
	<jsp:include page="./templateView/answer-temp.jsp" />
	<!-- 问答题（语音） -->
	
	<!-- 应答题（语音） -->
	<jsp:include page="./templateView/state-temp.jsp" />
	<!-- 应答题（语音） -->
	
	<!-- 语音提示题（语音） -->
	<jsp:include page="./templateView/voice-temp.jsp" />
	<!-- 语音提示题（语音） -->
	
	<!-- 复读陈述题（语音） -->
	<jsp:include page="./templateView/repeat-temp.jsp" />
	<!-- 复读陈述题（语音） -->
								
	<!-- 判断题(语音) -->
	<jsp:include page="./templateView/judge-temp.jsp" />
	<!-- 判断题(语音) -->
	
</div>
</body>
</html>