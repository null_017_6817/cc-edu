<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.qtrmoon.com/tags-dictionary" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>编辑</title><!--examCategory-->
	<%@include file="/common/include.jsp" %>
<script>
<%-- 查询页点击修改时调用此方法加载记录详情 --%>
function load(id){
	$('#_form').form('load','/${projectName}/examination/examCategory/vieExamCategory.action?id='+id);
};

<%-- 点击保存按钮时调用此方法。(注:保存按钮在查询列表页) --%>
function submitForm(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#_form').form('submit',{
      success:function(data) {
      	parent.reloadData();
      }
   });
}
</script>
</head>
<body>
<form id="_form" method="post" action="/${projectName}/examination/examCategory/updExamCategory.action">
	<input type="hidden" name='id' id='id'/>
	<table>
		<tr><td class='label' style="width:120px">父ID：</td><td><input id="pid" name="pid" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >名称：</td><td><input id="name" name="name" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >类型：</td><td><input id="type" name="type" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >排序：</td><td><input id="ord" name="ord" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >说明：</td><td><input id="info" name="info" class="easyui-textbox" data-options="required:true"/></td></tr>
	</table>

</form>
</body>
</html>
