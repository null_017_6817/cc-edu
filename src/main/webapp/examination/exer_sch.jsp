<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>练习查询</title>
	<%@include file="/common/include.jsp" %>
	<script src="/${projectName}/book/js/layui/js/lib/layui2.3/layui.all.js"></script>
<script>
var eid = "";
$(function(){
	<%-- 为分页行设置"添加"和"删除"按钮 --%>
	var pager = $('#exerGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});			
})

<%-- 执行查询，点击查询按钮时调用 --%>
function doSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#exerGrid').datagrid('load',{
		name:$('#name').val(),
		shortcut:shortcutDict.getValue(),
		department:$('#department').val(),
		teacher:teacherDict.getValue(),
		bankId:$('#bankId').val(),
		bankName:$('#bankName').val()
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatOper(val,row,index){
	var addType = row.addType;
	var stuNum = row.stuNum;
	var opt = "&emsp;<a href='javascript:void(0)' onclick=\"updExer('"+val+"','"+addType+"','"+stuNum+"')\" class='_oper' title='修改'>&nbsp;<i class='icon-edit'></i></a>&emsp;"
			+"&emsp;<a href='javascript:void(0)' onclick=\"previewExer('"+val+"')\" class='_oper' title='预览试卷'>&nbsp;<i class='icon-eye-open'></i></a>&emsp;";
	return opt;
} 

function previewExer(id){
	window.open("/${projectName}/examination/exer/page.action?page=exer_preview&id="+id);
}

<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
function updExer(id,addType,stuNum){
	// if(stuNum > 0 ){
		// alert("已发卷，禁止修改")
	// }else{
		if(addType == '1' ){
			window.location.href="/${projectName}/examination/exer/page.action?page=exer_autoTopic&id="+id+"&stuNum="+stuNum;
		}else{
			window.location.href="/${projectName}/examination/exer/page.action?page=exer_exerTopic&id="+id+"&stuNum="+stuNum;
		}
	// }
} 

<%-- 删除记录 --%>
function delExer(){
	var checkedItems = $('#exerGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		//$.messager.alert('敬告','请选择删除的记录？');
		layer.msg('敬告,请选择删除的记录');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.ajax({
				type : "POST",
				url : '/${projectName}/examination/exer/delExer.action',
				data : {ids:ids},	
				async: false,
				dataType:'json',
				success : function(result) {
					if(result.success){
						reloadData();
					}else{
						alert(result.msg);
					}
				}
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#exerEditWin').dialog('close');
	$('#exerGrid').datagrid('reload');
	//$('#shutcutTree').tree('reload');
}

// 手动组卷
function addtopic(){
	$.ajax({
		type : "POST",
		url : '/${projectName}/examination/exer/addDeFaultHandExer.action',
		data : {},	
		dataType:'json',
		success : function(result) {
			if(result.success){
				var exer = result.data;
				window.location.href="/${projectName}/examination/exer/page.action?page=exer_exerTopic&id="+exer.id;
			}else{
				//$.messager.alert('提示',result.msg);
				layer.msg(result.msg);
			}
		}
	});
}

// 自动组卷
function autoTopic(){
	$.ajax({
		type : "POST",
		url : '/${projectName}/examination/exer/addDeFaultAutoExer.action',
		data : {},	
		dataType:'json',
		success : function(result) {
			if(result.success){
				var exer = result.data;
				window.location.href="/${projectName}/examination/exer/page.action?page=exer_autoTopic&id="+exer.id;
			}else{
				//$.messager.alert('提示',result.msg);
				layer.msg(result.msg);
			}
		}
	});
}

// 发卷和撤卷
function toPerson(){
	var rows = $('#exerGrid').datagrid('getChecked');
	var ids = "";
	if( rows != null && typeof(rows) != 'undefined' ){
		var len = rows.length;
		if( len == 1 ){
			var id = rows[0].id;
			window.location.href="/${projectName}/examination/exer/page.action?page=exer_toPerson&id="+id;
		}else{
			/* if(len > 1){
				layer.msg("该操作只能选择一条数据");
			}else{
				layer.msg("请选择试卷");
			} */
			for(var i = 0 ; i < len;i++){
				if(ids == ""){
					ids = rows[i].id;
				}else{
					ids = ids + "," + rows[i].id;
				}
			}
			
			
			$.messager.confirm('提示','确定要批量发卷？',function(r){
				if(r){
					window.location.href="/${projectName}/examination/exer/page.action?page=exers_toPerson&id="+ids;
				}
			})
			
			
			
			
		}
	} else {
		layer.msg("请选择试卷");
	}
}

var currOrganId;
function openOrgan(json){//{id:'',text:''}
	currOrganId=json.id;
	searchTeacher(true);
}

// 点击左侧目录树
var fullShutCut ;
function treeSear(json){
	fullShutCut = "";
	var name = json.id;
	shortcutDict.select(name);
	doSearch();
}

// 递归查询父节点（拼接父节点名称）
function appendNodeName(name,target){
	var parent = $('#shutcutTree').tree('getParent', target); //获取选中节点的父节点
	if(parent != null){
		name = parent.id+"/"+name;
		appendNodeName(name,parent.target);
		return false;
	}
	fullShutCut = name;
}

var winNode = "";
$(function(){
	$('#shutcutTree').tree({
		url:'${pageContext.request.contextPath }/document/getMenuList',
		animate:false,
		dnd:true,
		onClick:function(node){//单击node事件
			winNode = node;
		},
		onContextMenu: function(e, node){ //给结点添加右键菜单 
			winNode = node;
			e.preventDefault();  //阻止右键默认事件
			$('#MenueItem').menu('show', {  
				left: e.pageX,  
				top: e.pageY  
			});
		},
		onDrop : function(target, source, point){
			var targetId = $(this).tree('getNode', target).id;
			var sourceId = source.id;
			dropNode(targetId,sourceId);
		}, 
		onBeforeEdit : function(node){
			//console.log(node.text);
			
		},
		onAfterEdit : function(node){
			var id = node.id;
			var text = node.text;
			editNode(id,text);
		},
		onDblClick : function(node){
			$(this).tree('beginEdit',node.target);
		},
	});
});

// 点击修改节点菜单，使树变成可编辑的状态
function updateNode(){
	$("#shutcutTree").tree('beginEdit',winNode.target);
}

// 拖拽树的方法
function dropNode(targetId,sourceId){
	$.ajax({
		type : "POST",
		url : '/${projectName}/examination/examCategory/dropNode.action',
		data : {"targetId":targetId,"sourceId":sourceId},
		dataType:'json',
		success : function(result) {
			if(result.success){
				reloadTree();
			}else{
				alert(result.msg);
			}
		}
	});
}

// 修改节点
function editNode(id,text){
	$.ajax({
		type : "POST",
		url : '/${projectName}/examination/examCategory/editNode.action',
		data : {"id":id,"text":text},
		dataType:'json',
		success : function(result) {
			if(result.success){
				reloadTree();
			}else{
				$.messager.alert('提示',result.msg,'error');
			}
		}
	});
}

//根据root添加节点
function addRootNode(){
	addNode(0);
}

// 添加节点
function addNodeByPid(){
	addNode(winNode.id);
}

function addNode(pid){
	$.ajax({
		type : "POST",
		url : '/${projectName}/examination/examCategory/addNode.action',
		data : {"pid":pid},
		dataType:'json',
		success : function(result) {
			if(result.success){
				var data = result.data;
				var node = new Object();
				node.id=data.id;
				node.text=data.name;
				if(pid == 0){
					$('#shutcutTree').tree('append', {
						parent:"",
						data:node
					});
				}else{
					$('#shutcutTree').tree('append', {
						parent:winNode.target,
						data:node
					});
				}
				//找到第一个元素
				var newNode = $('#shutcutTree').tree('find', data.id);
				reloadTree();
				$("#shutcutTree").tree('beginEdit',newNode.target);
			}else{
				$.messager.alert('提示',result.msg,'error');
			}
		}
	});
}

//添加节点显示添加窗口
function removeNode(){
	$.ajax({
		type : "POST",
		url : '/${projectName}/examination/examCategory/delNode.action',
		data : {"id":winNode.id},
		dataType:'json',
		success : function(result) {
			if(result.success){
				$("#shutcutTree").tree("remove",winNode.target);
				reloadTree();
			}else{
				$.messager.alert('提示',result.msg,'error');
			}
		}
	});
}

// 更新检索条件目录树
function reloadTree(){
	shortcutDict.tar.combotree("reload")
}

</script>
</head>
<body class="easyui-layout">
	<div id="TreeMenue" style="display:none;">
		<div id="MenueItem" class="easyui-menu" style="width: 120px;">
			<div onclick="addNodeByPid()" iconcls="icon-plus">添加下级目录</div>
			<div onclick="removeNode()" iconcls="icon-remove">删除目录</div>
			<div onclick="updateNode()" iconcls="icon-edit">修改目录</div>
		</div>
	</div>

	<div title="目录" style="width:180px;" data-options="iconCls:'icon-sitemap',region:'west',split:true,collapsible:true,animate: true,">
		<button onclick="addRootNode();" style="width:100%"> 添加根目录 </button>
		<dc:includeTree dictId="ZD_EXAM_CATEGORY" name="shutcutTree" style="synch_single" bind="select:treeSear"  />
	</div>
	<div id="content" region="center" style="border:0;">
		<table id="exerGrid"  title="试卷组织" fitcolumns=true
			data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#exerGridToolbar',collapsible:true,url:'/${projectName}/examination/exer/schExer.action',method:'get'">
			<thead>
				<tr>
					<th data-options="field:'checkbox',checkbox:true" ></th>
					<th data-options="field:'name',width:25,align:'left',sortable:true">试卷名称</th>
					<th data-options="field:'shortcut',width:25,left:'center',sortable:true">分类目录</th>
					<th data-options="field:'teacherName',width:25,align:'center',sortable:true">创建人</th>
					<th data-options="field:'startTime',width:25,align:'center',sortable:true">练习开始时间</th>
					<th data-options="field:'endTime',width:25,align:'center',sortable:true">练习结束时间</th>
					<th data-options="field:'totalNum',width:25,align:'center',sortable:true">题数</th> 
					<th data-options="field:'stuNum',width:25,align:'center',sortable:true">已发卷人数</th> 
					<!-- <th data-options="field:'totalScore',width:25,align:'center',sortable:true">总分值</th> 
					<th data-options="field:'passScore',width:25,align:'center',sortable:true">及格分值</th> -->
					<th data-options="field:'showStandard',width:25,align:'center',sortable:true">是否显示标准答案</th>
					<th data-options="field:'addTypeName',width:25,align:'center',sortable:true">组卷类型</th>
					<th data-options="field:'_oper',align:'center',formatter:formatOper">操作</th>
				</tr>
			</thead>
		</table>
	</div>
<%-- 查询条件设定栏 --%>
<div id="exerGridToolbar">
	<div class="opline">
		
	</div>
	<form id="_form" class="shline">
		试卷名称：<input name='name' id="name" class='easyui-textbox' value='' data-options="iconWidth:16,
			icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
			}]"/>
		分类目录：<td><dc:insertTree name="shortcut" dictId="ZD_EXAM_CATEGORY" style="synch_single"/></td>
		创建人：<dc:insertList name="teacher" dictId="ZD_TEACHER" style="combo_normal"/>
		<a href="javascript:doSearch();" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>&emsp;
		<a href="javascript:addtopic();" class="btn btn-success"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">手动组卷</span></a>&emsp;
		<a href="javascript:autoTopic();" class="btn btn-success"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">自动组卷</span></a>&emsp;
		<a href="javascript:toPerson();" class="btn btn-success"><span class="icon-share-alt" style="font-size:16px"></span> <span style="font-size:14px">发卷/撤卷</span></a>&emsp;
		<a href="javascript:delExer();" class="btn btn-danger"><span class="icon-trash" style="font-size:16px"></span> <span style="font-size:14px">删除</span></a>&emsp;
	</form>
</div>
</body>
</html>