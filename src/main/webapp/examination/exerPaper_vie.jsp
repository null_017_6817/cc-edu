<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>查看学生答题试卷</title><!--exerPaper-->
	<%@include file="/common/include.jsp" %>
<script>
function load(id){
	$('#_form').form('load','/${projectName}/examination/exerPaper/vieExerPaper.action?toL=true&id='+id);
};
</script>
</head>
<body>
<form id="_form">
	<input type="hidden" name='id' value=""/>
	<table style="margin: 0 auto;">
		<tr><td class='label'>练习编号：</td><td><input type='text' name='exerId' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>学号：</td><td><input type='text' name='studNumber' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>得分：</td><td><input type='text' name='score' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>得分详情：</td><td><input type='text' name='scoredetail' class='viewbox' readonly='readonly'/></td></tr>
	</table>

</form>
</body>
</html>
