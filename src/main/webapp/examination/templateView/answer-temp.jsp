<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<table class="answerMode model" style="display: inline-block;padding-left: 30px;">
	<input type="hidden" name="questionType" value="answerMode"/>
	<input type="hidden" name="title" />
	<input type="hidden" name="esid" value="" />
	<tr>
		<td class="label" ><span name="stepTitleNum"></span></td>
		<td><span name="stepTitle" ></span></td>
	</tr>
	<tr>
		<td class="label w" rowspan=2 style="text-align: right;vertical-align: top;">题干：</td>
		<td>
			<span name="prompt" ></span>
		</td>
	</tr>
	<tr name="choiceRoot">
		<td>
			<button name="audButton" onclick="showP(this);";>展开</button>
			<br>
			<p style="display:none">
				<audio controls="controls" ><source src='' type="audio/mpeg"/></audio>
			</p>
		</td>
	</tr>
	<tr>
		<td class="label w" style="vertical-align: top;">答案：</td>
		<td><span name="standardAnswer" style="height:100px;"></span></td>
	</tr>
	<tr>
		<td class="label w">分值：</td>
		<td><span name="score"></span>分</td>
	</tr>
	<tr name="downTr" style="display:none;">
		<td class="label">实操题：<input type="hidden" name="downFlg" value="no"></td>
		<td> 
			<input type="hidden" name="origiName" value="" />
			<input type="hidden" name="fileName" value="" />
		</td>
	</tr>
	<tr name="downStudTr" style="display:none;">
		<td class="label">学生上传：<input type="hidden" name="downFlg" value="no"></td>
		<td name="downStudTd"> 
		</td>
	</tr>
</table>
