<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<table class="stateMode model" style="display: inline-block;padding-left: 30px;">
	<input type="hidden" name="questionType" value="stateMode"/>
	<input type="hidden" name="title" />
	<input type="hidden" name="esid" value="" />
	<tr>
		<td class="label" ><span name="stepTitleNum"></span></td>
		<td><span name="stepTitle" ></span></td>
	</tr>
	<tr>
		<td class="label w" rowspan=2 style="text-align: right;vertical-align: top;">题干：</td>
		<td>
			<span name="prompt" ></span>
		</td>
	</tr>
	<tr name="choiceRoot" >
		<td>
			<button name="audButton" onclick="showP(this);";>展开</button>
			<br>
			<p style="display:none">
				<audio controls="controls" ><source src='' type="audio/mpeg"/></audio>
			</p>
		</td>
	</tr>
	<tr >
		<td class="label w" style="vertical-align: top;">答案：</td>
		<td><span name="standardAnswer" ></span></td>
	</tr>
	<tr>
		<td class="label w">分值：</td>
		<td><span name="score"></span>分</td>
	</tr>
</table>