
var stepData=[];
var sort="ABCDEFGHIJKLMNOPQRSTUVWXYZ";

//获取类型
function getType(stepInfo){
	return stepInfo.type;
}

// 显示步骤列表
function showStepList(stepInfoList,elementId){
	if(stepInfoList != null && stepInfoList.length > 0){
		for(var i = 0; i < stepInfoList.length; i++ ){
			var stepInfo = stepInfoList[i];
			var type = getType(stepInfo);
			// state-应答陈述题,repeat-复读陈述题,answer-问题-问答题,voice-语音提示题
			if(type == "state" || type == "answer" || type == "repeat" || type == "voice"
					|| type == '7' || type == '3' || type == '6' || type == '8' || type == '11'
					|| type == '12' || type == '14'){
				var cacheType = '';
				if(type == '7'){
					type = "state";
				}
				if(type == '3' || type == '11' || type == '12'){
					if(type == '11' || type == '12'){
						cacheType = 'down';
					}
					type = "answer";
				}
				if(type == '6'){
					type = "repeat";
				}
				if(type == '8'){
					type = "voice";
				}
				
				var aud = stepInfo.prompt.audio;
				$("."+type+"Mode").find("tr[name='choiceRoot']").find("audio").eq(0).find('source').attr("src","");
				$("."+type+"Mode").find("tr[name='choiceRoot']").find("audio").eq(0).find('source').attr("src",aud);
				$("."+type+"Mode").find("span[name='prompt']").html(stepInfo.prompt.context);
				$("."+type+"Mode").find("input[name='title']").val(stepInfo.title);
				$("."+type+"Mode").find("input[name='esid']").val(stepInfo.esid);
				$("."+type+"Mode").find("span[name='stepTitle']").html("");
				$("."+type+"Mode").find("span[name='stepTitle']").html(stepInfo.title);
				$("."+type+"Mode").find("span[name='score']").html(stepInfo.score);
				$("."+type+"Mode").find("span[name='standardAnswer']").html(stepInfo.standardAnswer);
				if(cacheType == "down" ){
					$("."+type+"Mode").find("tr[name='downTr']").show();
					$("."+type+"Mode").find("tr[name='downStudTr']").show();
					var origiName = stepInfo.prompt.origiName;
					var fileName = stepInfo.prompt.fileName;
					var bankId = stepInfo.bankId;
					var topicId = stepInfo.topicId;
					if(origiName != null && origiName != "" && typeof(origiName) != "undefined"){
						$("."+type+"Mode").find("input[name='origiName']").val(origiName);
						$("."+type+"Mode").find("input[name='fileName']").parent("td").find("a").remove();
						$("."+type+"Mode").find("input[name='fileName']").parent("td").append("<a name='downloadZipA' href='javascript:downloadZip(\""+fileName+"\",\""+origiName+"\",\""+bankId+"\",\""+topicId+"\");'>"+origiName +"</a>");
					}
					if( fileName != null && fileName != "" && typeof(fileName) != "undefined" ){
						$("."+type+"Mode").find("input[name='fileName']").val(fileName);
					}
				}else{
					$("."+type+"Mode").find("tr[name='downTr']").hide();
					$("."+type+"Mode").find("tr[name='downStudTr']").hide();
				}
				var askAnswerVoice = $("."+type+"Mode").clone(true);
				stepData.push(askAnswerVoice);
			}
			// 单选/多选题
			if( type == "choice_single" || type == "choice_multi" || type == '1' || type == '2' ){
				if(type == '1' ){
					type = "choice_single";
				}
				if(type == '2' ){
					type = "choice_multi"
				}
				
				var claType = "";
				var addBtn = "";
				if(type == "choice_single"){
					claType = "selectMode";
				}
				if(type == "choice_multi"){
					claType = "selectsMode";
				}
				var aud = stepInfo.prompt.audio;
				
				$("."+claType).find("tr[name='choiceRoot']").find("audio").eq(0).find('source').attr("src","");
				$("."+claType).find("tr[name='choiceRoot']").find("audio").eq(0).find('source').attr("src",aud);
				$("."+claType).find("span[name='prompt']").html(stepInfo.prompt.context);
				$("."+claType).find("span[name='stepTitle']").html("");
				$("."+claType).find("span[name='stepTitle']").html(stepInfo.title);
				$("."+claType).find("input[name='title']").val(stepInfo.title);
				$("."+claType).find("input[name='esid']").val(stepInfo.esid);
				$("."+claType).find("span[name='score']").html(stepInfo.score);
				$("."+claType).find("tr[name='choice']:gt(1)").remove();
				$("."+claType).find("td[name='choiceAnswer']").find("label:gt(1)").remove();
				var choices = stepInfo.choiceQuestionList;
				if(choices != null && typeof(choices) != "undefined"){
					for( var c = 0; c < choices.length; c++ ){
						var letter = sort.charAt(c);
						var choice = choices[c];
						var chAudio = choice.audio;
						
						if( c >= 2 ){
							// 拼接选项
							var ch = "<tr name='choice'>"
								+	"<td class='label w'>"+letter+"：</td>"
								+	"<td name='td"+letter+"'>"
								+		"<span name='choice"+letter+"' >"+choice.context+"</span>"
								+		" <button onclick='showP(this);';>展开</button><br>"
								+		"<p style='display:none'>"
								+			"<audio controls><source src='"+choice.audio+"' type='audio/mpeg'/></audio><br>"
								+		"</p>"
								+	"</td>"
								+"</tr><br>";
							$("."+claType).find("tr[name='choice']:last").after(ch);
						}else{
							$("."+claType).find("tr[name='choice']").eq(c).find("span[name='choice"+letter+"']").html(choice.context);
							$("."+claType).find("tr[name='choice']").eq(c).find("input[name='voice']").val(choice.audioHid);
							$("."+claType).find("tr[name='choice']").eq(c).find("source").eq(0).attr("src",chAudio);
						}
					}
				}
				
				var standardAnswer = stepInfo.standardAnswer;
				// 清空选中
				$("."+claType).find("td[name='choiceAnswer']").html(standardAnswer)
				// 复制表格
				var selectVoice = $("."+claType).clone(true);
				stepData.push(selectVoice);
			}
			// 填空题
			if( type == "judge" || type == '4' ){
				if(type == '4'){
					type = "judge";
				}
				
				var aud = stepInfo.prompt.audio;
				$("."+type+"Mode").find("tr[name='choiceRoot']").find("audio").eq(0).find('source').attr("src","");
				$("."+type+"Mode").find("tr[name='choiceRoot']").find("audio").eq(0).find('source').attr("src",aud);
				$("."+type+"Mode").find("span[name='prompt']").html(stepInfo.prompt.context);
				$("."+type+"Mode").find("input[name='title']").val(stepInfo.title);
				$("."+type+"Mode").find("input[name='esid']").val(stepInfo.esid);
				$("."+type+"Mode").find("span[name='stepTitle']").html("");
				$("."+type+"Mode").find("span[name='stepTitle']").html(stepInfo.title);
				$("."+type+"Mode").find("span[name='score']").html(stepInfo.score);
				$("."+type+"Mode").find("td[name='standardAnswer']").html(stepInfo.standardAnswer);
				var table = $("."+type+"Mode").clone(true);
				stepData.push(table);
			}
			// 填空题
			if( type == "cloze" || type == '5' || type == '9' ){
				if(type == '5' || type == '9'){
					type = "cloze"
				}
				
				if(stepInfo.prompt.audioHid != null && stepInfo.prompt.audioHid != '' 
					|| typeof(stepInfo.prompt.audioHid) != "undefined"){
					
					$("."+type+"Mode").find("tr[name='fillTr']:gt(0)").remove();
					var aud = stepInfo.prompt.audio;
					$("."+type+"Mode").find("tr").eq(1).find("audio").eq(0).find('source').attr("src","");
					$("."+type+"Mode").find("tr").eq(1).find("audio").eq(0).find('source').attr("src",aud);
					$("."+type+"Mode").find("input[name='title']").val(stepInfo.title);
					$("."+type+"Mode").find("input[name='esid']").val(stepInfo.esid);
					$("."+type+"Mode").find("span[name='stepTitle']").html(stepInfo.title);
					var prompts = stepInfo.prompt.context;
					promptArg = prompts.split("[]");
					var standardAnswers = stepInfo.standardAnswer;
					var standardAnswerArg = standardAnswers.split(",");
					var scores = stepInfo.score;
					var scoreArg = scores.split(",");
					
					$("."+type+"Mode").find("tr[name='fillTr']:gt(0)").remove();// 清除之前加载的脏数据
					for( var j = 0; j < promptArg.length; j++ ){
						if(promptArg[j] != null && $.trim(promptArg[j]) != "" && typeof(promptArg[j]) != "undefined" ){
							if(scoreArg[j] == null || typeof(scoreArg[j]) == "undefined"){
								scoreArg[j] = ""
							}
							if(standardAnswerArg[j] == null || typeof(standardAnswerArg[j]) == "undefined"){
								standardAnswerArg[j] = ""
							}
							if( j == 0){
								$("."+type+"Mode").find("tr[name='fillTr']").find("span[name='prompt']").html(promptArg[j]);
								$("."+type+"Mode").find("tr[name='fillTr']").find("span[name='standardAnswer']").html(standardAnswerArg[j]);
								$("."+type+"Mode").find("tr[name='fillTr']").find("span[name='score']").html(scoreArg[j]);
							}
							if( j > 0 ){
								var tr = "<tr name='fillTr'>"
										+	"<td><span name='prompt' >"+promptArg[j]+"</span></td>"
										+	"<td style='padding-left: 16px;'><span name='standardAnswer' >"+standardAnswerArg[j]+"</span></td>"
										+	"<td><span name='score' >"+scoreArg[j]+"</span>"
										+	"</td>"
										+"</tr>";
										
								$("."+type+"Mode").find("tr[name='fillTr']:last").after(tr);
							}
						}
					}
					var table = $("."+type+"Mode").clone(true);
					stepData.push(table);
				}else{
					showclozed(stepInfo);
				}
			}
		}
	}
	appendStep(elementId);
}

//显示截取一句话类型的填空题
function showclozed(stepInfo){
	var aud = stepInfo.prompt.audio;
	$(".clozedMode").find("tr").eq(1).find("audio").eq(0).find('source').attr("src","");
	$(".clozedMode").find("tr").eq(1).find("audio").eq(0).find('source').attr("src",aud);
	
	$(".clozedMode").find("span[name='prompt']").html(stepInfo.prompt.context);
	$(".clozedMode").find("input[name='title']").val(stepInfo.title);
	$(".clozedMode").find("input[name='esid']").val(stepInfo.esid);
	$(".clozedMode").find("span[name='stepTitle']").html("");
	$(".clozedMode").find("span[name='stepTitle']").html(stepInfo.title);
	var standardAnswers = stepInfo.standardAnswer;
	var standardAnswerArg = standardAnswers.split(",");
	var scores = stepInfo.score;
	var scoreArg = scores.split(",");
	$(".clozedMode").find("input[name='title']").val(stepInfo.title);
	
	$(".clozedMode").find("tr[name='fillTr']:gt(0)").remove();// 清除之前加载的脏数据
	for( var j = 0; j < standardAnswerArg.length; j++ ){
		if(standardAnswerArg[j] != null && $.trim(standardAnswerArg[j]) != "" && typeof(standardAnswerArg[j]) != "undefined" ){
			if(scoreArg[j] == null || typeof(scoreArg[j]) == "undefined"){
				scoreArg[j] = ""
			}
			if(standardAnswerArg[j] == null || typeof(standardAnswerArg[j]) == "undefined"){
				standardAnswerArg[j] = ""
			}
			if( j == 0){
				$(".clozedMode").find("tr[name='fillTr']").find("span[name='standardAnswer']").html(standardAnswerArg[j]);
				$(".clozedMode").find("tr[name='fillTr']").find("span[name='score']").html(scoreArg[j]);
			}
			if( j > 0 ){
				var tr = "<tr name='fillTr'>"
						+	"<td></td>"
						+	"<td><span name='standardAnswer' >"+standardAnswerArg[j]+"</span></td>"
						+	"<td><span name='score' "+scoreArg[j]+"</span>"
						+	"</td>"
						+"</tr>";
				$(".clozedMode").find("tr[name='fillTr']:last").after(tr);
			}
		}
	}
	var askAnswerVoice = $(".clozedMode").clone(true);
	stepData.push(askAnswerVoice);
}

//下载zip
function downloadZip(fileName,origiName,bankId,topicId){
	var param = "fileName="+fileName+"&origiName="+origiName+"&bankId="+bankId+"&topicId="+topicId;
	window.location.href= "/"+projectName +"/paper/question/downloadZip.action?"+param;
}

// 展开关闭效果
function showP(t){
	var txt = $(t).text();
	var newTxt = "";
	if(txt == '展开'){
		newTxt = "关闭"
	}else{
		newTxt = "展开"
	}
	$(t).text(newTxt);
	$(t).next("br").next('p').toggle(300);
}

