<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>查看学生答题卡</title>
	<%@include file="/common/include.jsp" %>
<script>
function load(id){
	$('#_form').form('load','/${projectName}/examination/exerAnswer/vieExerAnswer.action?toL=true&id='+id);
};
</script>
</head>
<body>
<form id="_form">
	<input type="hidden" name='id' value=""/>
	<table style="margin: 0 auto;">
		<tr><td class='label'>练习编号：</td><td><input type='text' name='eid' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>考题编号：</td><td><input type='text' name='etid' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>步骤编号：</td><td><input type='text' name='esid' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>学号：</td><td><input type='text' name='studNumber' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>备注：</td><td><input type='text' name='remark' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>答题内容：</td><td><input type='text' name='answer' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>步骤得分：</td><td><input type='text' name='score' class='viewbox' readonly='readonly'/></td></tr>
	</table>

</form>
</body>
</html>
