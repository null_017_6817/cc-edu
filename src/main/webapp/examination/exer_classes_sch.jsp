<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>班级完成情况</title>
<script src="/${projectName}/easyui1_5/echarts.min.js"></script>
<script src="/${projectName}/easyui1_5/jquery.min.js"></script>
<script src="/${projectName}/easyui1_5/themes/custom/jquery.easyui.min.js"></script>
<link rel="stylesheet" type="text/css" href="/${projectName}/easyui1_5/themes/custom/uimaker/easyui.css" id="easyuiTheme" >
<script src="/${projectName}/easyui1_5/qct.js"></script>
<style type="text/css">
	.exer{
		width: 225px;
		height: 20px
	}
	table,table tr th, table tr td{
		 border:1px solid #0094ff;
		 border-collapse:collapse;
	}
	#searchForm select{
		margin-left:8px;
		margin-right:20px;
		width:280px;
		height:36px;
		line-height:36px;
		padding:0 10px;
		outline:none;
		border:0;
		border:1px solid #ccc;
	}
	#searchForm .search_input{
		width:94px;
		height:36px;
		line-height:34px;
		text-align:center;
		background:none;
		border:0;
		border:1px solid #ccc;
		border-radius:5px;
		cursor:pointer;
		outline:none;
	}
	.form_table{
		border:1px solid #ccc;
	}
	.form_table thead th{
		height:34px;
		line-height:34px;
		border:1px solid #ddd;
		font-weight:400;
		color:#333;
		background:#efefef;
	}
	.form_table tr td{
		height:30px;
		line-height:30px;
		border:1px solid #eee;
		text-align:center;
	}
	#jzg-mz{
		display:none;
	}
	.keyWorlds{
	    height: 36px;
	    line-height: 36px;
	    padding: 0 10px;
	    border: 1px solid #ccc;
	    margin-right: 23px;
	}
	/*修饰滚动条*/
::-webkit-scrollbar-track-piece{background-color:#fff;-webkit-border-radius:0}
::-webkit-scrollbar{width:1px;height:1px}
::-webkit-scrollbar-thumb{height:50px;background-color:#b8b8b8;-webkit-border-radius:6px;outline:2px solid #fff;
outline-offset:-2px;border:2px solid #fff;filter:alpha(opacity=50);-moz-opacity:0.5;-khtml-opacity:0.5;opacity:0.5}
::-webkit-scrollbar-thumb:hover{height:50px;background-color:#878987;-webkit-border-radius:6px}
::-moz-scrollbar-track-piece{background-color:#fff;-webkit-border-radius:0}
::-moz-scrollbar{width:1px;height:1px}
::-moz-scrollbar-thumb{height:50px;background-color:#b8b8b8;-webkit-border-radius:6px;outline:2px solid #fff;
outline-offset:-2px;border:2px solid #fff;filter:alpha(opacity=50);-moz-opacity:0.5;-khtml-opacity:0.5;opacity:0.5}
::-moz-scrollbar-thumb:hover{height:50px;background-color:#878987;-webkit-border-radius:6px}
</style>
<script type="text/javascript">
var kw="";
$(function() {
	getSj(kw);
})
function getSj(_kw){
	$.ajax({
		 async:false,
		 type:"GET",
		 url:"/${projectName}/show/getTestPaperList.action",
		 dataType:"json",
		 data:{
			 kw:kw
		 },
		 success: function (response) {
	   		if(response.length>0){
	   			var tjsHtml='';
	   			tjsHtml+='<option value="-1">--请选择练习--</option>';
	   			for(var i=0;i<response.length;i++){
	   				var tj=response[i];
	   				tjsHtml+='<option value="'+tj.id+'">'+tj.label+'</option>';
	   			}
	   			$("#exer").html(tjsHtml);
	   		}
		 }
	  });
}

function keyWorldsSearch(){
	kw=$(".keyWorlds").val();
	getSj(kw);
}

function searchClass(){
	$(".form_pic").show();
	var exer=$("#exer").val();
	if(exer==-1){
		$.messager.alert('敬告','请选择练习！');
		return;
	}
	$.ajax({
		 async:false,
		 type:"GET",
		 url:"/${projectName}/show/schClassCompletion.action",
		 dataType:"json",
		 data:{
			 eid:exer
		 },
		 success: function (response) {
	   		if(response.rows.length>0){
	   			show(response.rows);
	   			var tjsHtml='';
	   			for(var i=0;i<response.rows.length;i++){
	   				var n=i+1;
	   				var tj=response.rows[i];
	   				tjsHtml+='<tr><td>'+n+'</td><td>'+tj.bjbh+'</td><td>'+tj.bjmc+'</td><td>'+tj.cjrs+'</td><td>'+tj.wcrs+'</td>';
	   				tjsHtml+='<td>'+tj.wwcrs+'</td><td>'+tj.pjwcl+'</td><tr>';
	   			}
	   			$("#completion").html(tjsHtml);
	   		}
		 }

	  });
}
</script>
<style type="text/css">
</style>
</head>
<body>
	<div style="width: 100%; height: 1000px; clear: both;">
		<div style="margin-bottom:20px">
			<form id="searchForm">
				练习:<select  id="exer" class="exer">
						<option value="-1">--请选择练习--</option>
					</select>
					<input type="text" class="keyWorlds" placeholder="请输入关键字" onchange="keyWorldsSearch()"/>
					<input type="button" onclick="searchClass()" value="统计" class="search_input" /> 
			</form>
		</div>
		<div id="jzg-mz" style="width: 67%;height: 400px; float: left;" class="form_pic"></div>
		<div style="width: 67%;height: 38px; float: left;">
			<table style="width: 100%" class="form_table">
			    <colgroup>
			      <col width="8%">
			      <col width="15%">
			      <col width="20%">
			      <col width="10%">
			      <col width="10%">
			      <col width="15%">
			      <col>
			    </colgroup>	
				<thead>
					<tr>
						<th>序号</th>
						<th>班级编号</th>
						<th>班级名称</th>
						<th>参加人数</th>
						<th>完成人数</th>
						<th>未完成人数</th>
						<th>平均题目完成率</th>
					</tr>
				</thead>
		</table>
		</div>
		<div style="width: 67%;height: 400px; float: left;overflow: auto;">
			<table style="width: 100%;margin-bottom: 80px;" class="form_table">		
			    <colgroup>
			      <col width="8%">
			      <col width="15%">
			      <col width="20%">
			      <col width="10%">
			      <col width="10%">
			      <col width="15%">
			      <col>
			    </colgroup>		
				<tbody	id="completion">
			   </tbody>
		</table>
		</div>
		
	</div>
</body>
<script type="text/javascript">
function show(tjs) {
	var myChart = echarts.init(document.getElementById('jzg-mz'));
	var bjs=[];
	var cjrs=[];
	var wcl=[];
	if(tjs.length>0){
		for(var i=0;i<tjs.length;i++){
			var tj=tjs[i];
			bjs.push(tj.bjmc);
			cjrs.push(tj.cjrs);
			wcl.push(tj.pjwcl.replace("%",""));
		}
	}
	
	var option = {
		    tooltip : {
		        trigger: 'axis'
		    },
		    toolbox: {
		        show :false,/*隐藏右上方工具*/
		        feature : {
		            mark : {show: true},
		            dataView : {show: true, readOnly: false},
		            magicType: {show: true, type: ['line', 'bar']},
		            restore : {show: true},
		            saveAsImage : {show: true}
		        }
		    },
		    calculable : true,
		    legend: {
		        data:['参加人数','完成率']
		    },
		    xAxis : [
		        {
		            type : 'category',
		            data : bjs
		        }
		    ],
		    yAxis : [
		        {
		            type : 'value',
		            name : '参加人数',
		           position: 'inside',
		            axisLabel : {
		             
		                formatter: '{value}'
		            }
		        },
		        {
		            type : 'value',
		            name : '完成率',
		           position:'top' ,
		            axisLabel : {
		             
		                formatter: '{value}% '
		            }
		        }
		    ],
		    series : [

		        {
		            name:'参加人数',
		            type:'bar',
		            barWidth:30,
		            itemStyle : { normal: {label : {show: true, position: 'inside'}}},
		            data:cjrs
		        },
		        {
		            name:'完成率',
		            type:'line',
		            yAxisIndex: 1,
		            itemStyle : { normal: {label : {show: true, position: 'top',formatter:'{c}%'}}},
		            data:wcl
		        }
		    ]
		};
	myChart.setOption(option);
}
</script>
</html>