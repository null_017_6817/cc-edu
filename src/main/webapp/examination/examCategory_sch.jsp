<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.qtrmoon.com/tags-dictionary" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>查询</title><!--examCategory-->
	<%@include file="/common/include.jsp" %>

<script>
var winToggle;
$(function(){
	winToggle=new QWinToggle().regist(["examCategoryEditWin","examCategoryViewWin"]);
	var pager = $('#examCategoryGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});			
})

<%-- 执行查询，点击查询按钮时调用 --%>
function doExamCategorySearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#examCategoryGrid').datagrid('load',{
		id:$('#id').val(),
		pid:$('#pid').val(),
		name:$('#name').val(),
		type:$('#type').val(),
		ord:$('#ord').val(),
		info:$('#info').val()
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatExamCategoryOper(val,row,index){  
    return "&emsp;<a href='javascript:void(0)' onclick='updExamCategory("+val+")' class='_oper' title='修改'>&nbsp;<i class='icon-edit'></i>&nbsp;</a>"
    			+"<a href='javascript:void(0)' onclick='vieExamCategory("+val+")' class='_oper' title='查看'>&nbsp;<i class='icon-info-sign'></i>&nbsp;</a>&emsp;"; 
} 

<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
function updExamCategory(id){
	winToggle.open("examCategoryEditWin");
	$('#examCategoryEditWin').get(0).contentWindow.load(id);
} 

<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
function vieExamCategory(id){
	winToggle.open("examCategoryViewWin");
	$('#examCategoryViewWin').get(0).contentWindow.load(id);
}

<%-- 删除记录 --%>
function delExamCategory(){
	var checkedItems = $('#examCategoryGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/examination/examCategory/delExamCategory.action",{ids:ids},function(data){
				$('#examCategoryGrid').datagrid('reload');
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#examCategoryEditWin').dialog('close');
	$('#examCategoryGrid').datagrid('reload');
}
</script>
</head>
<body>
<table id="examCategoryGrid" title="" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#examCategoryGridToolbar',collapsible:true,url:'/${projectName}/examination/examCategory/schExamCategory.action',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" ></th>
			<th data-options="field:'pid',width:80,sortable:true">父ID</th>
			<th data-options="field:'name',sortable:true">名称</th>
			<th data-options="field:'type',sortable:true">类型</th>
			<th data-options="field:'ord',sortable:true">排序</th>
			<th data-options="field:'info',sortable:true">说明</th>

			<th data-options="field:'_oper',align:'center',formatter:formatExamCategoryOper">操作</th>
		</tr>
	</thead>
</table>
<%-- 查询条件设定栏 --%>
<div id="examCategoryGridToolbar">
	<div class="opline">
		<a href="javascript:updExamCategory(0)" class="btn btn-success"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">添加</span></a>&emsp;
		<a href="javascript:delExamCategory()" class="btn btn-danger"><span class="icon-trash" style="font-size:16px"></span> <span style="font-size:14px">删除</span></a>
	</div>
	<form id="_form" class="shline">
	id：<input name='id' id="id" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	父ID：<input name='pid' id="pid" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	名称：<input name='name' id="name" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	类型：<input name='type' id="type" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	排序：<input name='ord' id="ord" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	说明：<input name='info' id="info" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>

	<a href="javascript:void(0)" onclick="doExamCategorySearch()" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>
	</form>
</div>


<%-- 修改or添加面板 --%>
<iframe id="examCategoryEditWin" src="/${projectName}/examination/examCategory/page.action?page=examCategory_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#examCategoryEditButton'" title="编辑" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="examCategoryEditButton">
	<a href="javascript:void(0)" onclick="$('#examCategoryEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#examCategoryEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >取 消</a>
</div>
<%-- 查看面板 --%>
<iframe id="examCategoryViewWin" src="/${projectName}/examination/examCategory/page.action?page=examCategory_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#examCategoryViewButton'" title="查看" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="examCategoryViewButton">
	<a href="javascript:$('#examCategoryViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >关 闭</a>
</div>
</body>
</html>
