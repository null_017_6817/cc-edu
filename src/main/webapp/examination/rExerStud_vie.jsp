<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>查看练习与班级关系表</title><!--rExerStud-->
	<%@include file="/common/include.jsp" %>
<script>
function load(id){
	$('#_form').form('load','/${projectName}/examination/rExerStud/vieRExerStud.action?toL=true&id='+id);
};
</script>
</head>
<body>
<form id="_form">
	<input type="hidden" name='id' value=""/>
	<table style="margin: 0 auto;">
		<tr><td class='label'>练习编号：</td><td><input type='text' name='eid' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>学生：</td><td><input type='text' name='studid' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>班级：</td><td><input type='text' name='classid' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>类型：</td><td><input type='text' name='type' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>标题：</td><td><input type='text' name='name' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>阅卷老师：</td><td><input type='text' name='markTeacher' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>阅卷状态：</td><td><input type='text' name='status' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>得分：</td><td><input type='text' name='mark' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>备注：</td><td><input type='text' name='remark' class='viewbox' readonly='readonly'/></td></tr>
	</table>

</form>
</body>
</html>
