<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>学习过程分析</title>
<script src="/${projectName}/easyui1_5/echarts.min.js"></script>
<script src="/${projectName}/easyui1_5/jquery.min.js"></script>
<script src="/${projectName}/easyui1_5/themes/custom/jquery.easyui.min.js"></script>
<link rel="stylesheet" type="text/css" href="/${projectName}/easyui1_5/themes/custom/uimaker/easyui.css" id="easyuiTheme" >
<script src="/${projectName}/easyui1_5/qct.js"></script>
<style type="text/css">
	.exer{
		width: 225px;
		height: 20px
	}
	table,table tr th, table tr td{
		 border:1px solid #0094ff;
		 border-collapse:collapse;
	}
	#searchForm select{
		margin:0 45px 0 8px;
		width:225px;
		height:36px;
		line-height:36px;
		padding:0 10px;
		outline:none;
		border:0;
		border:1px solid #ccc;
	}
	#searchForm #exer{
		width:300px;
	}
	#searchForm .search_input{
		width:94px;
		height:36px;
		line-height:34px;
		text-align:center;
		background:none;
		border:0;
		border:1px solid #ccc;
		border-radius:5px;
		cursor:pointer;
		outline:none;
	}
	.completionList{
		border:1px solid #ccc;
	}
	.completionList thead th{
		height:34px;
		line-height:34px;
		border:1px solid #ddd;
		font-weight:400;
		color:#333;
		background:#efefef;
	}
	.completionList tr td{
		height:30px;
		line-height:30px;
		border:1px solid #eee;
		text-align:center;
	}
	table tbody {
	/*display: block;*/
	height: 200px;
	overflow: auto;
}
/*修饰滚动条*/
::-webkit-scrollbar-track-piece{background-color:#fff;-webkit-border-radius:0}
::-webkit-scrollbar{width:1px;height:1px}
::-webkit-scrollbar-thumb{height:50px;background-color:#b8b8b8;-webkit-border-radius:6px;outline:2px solid #fff;
outline-offset:-2px;border:2px solid #fff;filter:alpha(opacity=50);-moz-opacity:0.5;-khtml-opacity:0.5;opacity:0.5}
::-webkit-scrollbar-thumb:hover{height:50px;background-color:#878987;-webkit-border-radius:6px}
::-moz-scrollbar-track-piece{background-color:#fff;-webkit-border-radius:0}
::-moz-scrollbar{width:1px;height:1px}
::-moz-scrollbar-thumb{height:50px;background-color:#b8b8b8;-webkit-border-radius:6px;outline:2px solid #fff;
outline-offset:-2px;border:2px solid #fff;filter:alpha(opacity=50);-moz-opacity:0.5;-khtml-opacity:0.5;opacity:0.5}
::-moz-scrollbar-thumb:hover{height:50px;background-color:#878987;-webkit-border-radius:6px}
</style>
<script type="text/javascript">
$(function() {
	 getClasses();
})
function getClasses(){
	$.ajax({
		 async:false,
		 type:"GET",
		 url:"/${projectName}/show/getClasses.action",
		 dataType:"json",
		 data:{
			 
		 },
		 success: function (response) {
	   		if(response.length>0){
	   			var tjsHtml='';
	   			tjsHtml+='<option value="-1">--请选择班级--</option>';
	   			for(var i=0;i<response.length;i++){
	   				var tj=response[i];
	   				tjsHtml+='<option value="'+tj.id+'">'+tj.label+'</option>';
	   			}
	   			$("#bj").html(tjsHtml);
	   		}
		 }

	  });
}

function selectBj(classid){
	var tjsHtml='';
	tjsHtml+='<option value="-1">--请选择学生--</option>';
	if(classid==-1){
		$("#stu").html(tjsHtml);
	}else{
		$.ajax({
			 async:false,
			 type:"GET",
			 url:"/${projectName}/show/getStuList.action",
			 dataType:"json",
			 data:{
				 classid:classid
			 },
			 success: function (response) {
		   		if(response.length>0){
		   			for(var i=0;i<response.length;i++){
		   				var tj=response[i];
		   				tjsHtml+='<option value="'+tj.id+'">'+tj.label+'</option>';
		   			}
		   		}
		   		$("#stu").html(tjsHtml);
			 }

		  });
	}
}
function searchClass(){
	var bj=$("#bj").val();
	if(bj==-1){
		$.messager.alert('敬告','请选择班级！');
		return;
	}
	var stu=$("#stu").val();
	if(stu==-1){
		$.messager.alert('敬告','请选择学生！');
		return;
	}
	$.ajax({
		 async:false,
		 type:"GET",
		 url:"/${projectName}/show/schLearningProcess.action",
		 dataType:"json",
		 data:{
			 studid:stu,
			 classid:bj
		 },
		 success: function (response) {
	   		if(response.rows.length>0){
	   			var tjsHtml='';
	   			for(var i=0;i<response.rows.length;i++){
	   				var n=i+1;
	   				var tj=response.rows[i];
	   				tjsHtml+='<tr><td>'+n+'</td><td>'+tj.name+'</td><td>'+tj.tms+'</td><td>'+tj.zts+'</td>';
	   				tjsHtml+='<td>'+tj.wcqk+'</td><td>'+tj.wcl+'</td><tr>';
	   			}
	   			$("#completion").html(tjsHtml);
	   		}
		 }

	  });
	  
	$.ajax({
		url:"/${projectName}/show/schOrderStatistics.action?getType=json",
		async:false,
		data:{
			studid:stu,
			classid:bj
		 },
		success:function(data){
			var json = eval("("+data+")");
			showPie("jzg-mz",json);
		}
	}) 
}
</script>
<style type="text/css">
</style>
</head>
<body>
	<div style="width: 100%; height: 1000px; clear: both;">
		<div style="margin-bottom:20px">
			<form id="searchForm">
				班级: <select  id="bj" onchange="selectBj(this.options[this.options.selectedIndex].value)"></select>
				学生:<select  id="stu">
						<option value="-1">--请选择学生--</option>
					</select>
					<input type="button" onclick="searchClass()" value="统计" class="search_input" /> 
			</form>
		</div>
		<div style="width: 67%;height: 38px; float: left;">
			<table style="width: 100%" class="completionList">
			    <colgroup>
			      <col width="10%">
			      <col width="30%">
			      <col width="10%">
			      <col width="10%">
			      <col width="20%">
			      <col>
			    </colgroup>	
				<thead>
					<tr>
						<th>序号</th>
						<th>任务名称</th>
						<th>题目数</th>
						<th>做题数</th>
						<th>完成情况</th>
						<th>完成率</th>
					</tr>
				</thead>
		</table>
	</div>
	<div style="width: 67%;height: 705px; float: left;overflow: auto;">
	    <table style="width: 100%;margin-bottom: 80px;"" class="completionList">
	            <colgroup>
			      <col width="10%">
			      <col width="30%">
			      <col width="10%">
			      <col width="10%">
			      <col width="20%">
			      <col>
			    </colgroup>	
				<tbody	id="completion">
			   </tbody>
		</table>
	</div>
	<div id="jzg-mz" style="width: 28%;height: 400px; float: right;padding-left:5%;"></div>
</div>
</body>
<script type="text/javascript">
function showPie(divname,json) {
	var pieV={
		dom:document.getElementById(divname),
		title:json.title,
		legend:json.legend,
		xAxis:[],
	    series: [{
	         data: json.series.data
	    }]
	};
	pie(pieV);
}
</script>
</html>