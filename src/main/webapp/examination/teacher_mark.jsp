<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>老师阅卷</title>
	<%@include file="/common/include.jsp" %>

<script>
var eid = "";
$(function(){
	<%-- 为分页行设置"添加"和"删除"按钮 --%>
	var pager = $('#exerGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});			
})

<%-- 执行查询，点击查询按钮时调用 --%>
function doSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#exerGrid').datagrid('load',{
		name:$('#name').val(),
		markStatus:markStatusDict.getValue()
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatOper(val,row,index){
	var stepNum = row.stepNum;
	var opt = "&emsp;<a href='javascript:void(0)' onclick=\"viwStu('"+val+"','"+row.endTime+"')\" class='_oper' title='查看考生'>&nbsp;<i class='icon-user-md'></i></a>&emsp;";
	var myDate = new Date();
	var endDate = new Date(row.endTime)
	if(stepNum > 0 && myDate>endDate){
		opt = opt+"&emsp;<a href='javascript:void(0)' onclick=\"downLoadAll('"+val+"');\" class='_oper' title='批量下载考生实操题答案'>&nbsp;<span class='icon-download-alt' style='font-size:16px'></span></a>&emsp;";
	}
	return opt;
} 

function downLoadAll(val){
	window.open("/${projectName}/examination/mark/downAllAnswer.action?eid="+val);
}

// 显示进度条
function progressOper(val,row,index){
	var progress = row.progress+"%";
	var htmlstr = "<div class='easyui-progressbar progressbar easyui-fluid' style='width: 80%; height: 15px;border-radius: 9px;margin-left: 22px;'>"
					+ "<div class='progressbar-value' style='width: 100%; height: 15px; line-height: 15px;'> "
						+ "<div class='progressbar-text' style='width: "+progress+"; height: 15px;line-height: 15px;background-color: #d4c20b;'>"+progress+"</div>"
					+ "</div>"
				+ "</div>";
	return htmlstr;
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#exerEditWin').dialog('close');
	$('#exerGrid').datagrid('reload');
}

// 弹出学生选择页面
function viwStu( eid,endTime ){
	window.location.href = "/${projectName}/examination/mark/page.action?page=exer_stud&eid="+eid+"&endTime="+endTime;
}

</script>
</head>
<body>
<table id="exerGrid" title="试卷列表" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#exerGridToolbar',collapsible:true,url:'/${projectName}/examination/mark/schTeacherMark.action',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" ></th>
			<th data-options="field:'name',width:40,sortable:true">试卷名称</th>
			<th data-options="field:'totalScore',width:20,align:'center',sortable:true">总分值</th>
			<th data-options="field:'passScore',width:20,align:'center',sortable:true">及格分值</th>
			<th data-options="field:'startTime',width:30,align:'center',sortable:true">练习开始时间</th>
			<th data-options="field:'endTime',width:30,align:'center',sortable:true">练习结束时间</th>
			<th data-options="field:'showStandard',width:10,align:'center',sortable:true">是否显示答案</th>
			<th data-options="field:'markProgress',width:10,align:'center',sortable:true">进度</th>
			<th data-options="field:'_oper',align:'center',formatter:formatOper">操作</th>
		</tr>
	</thead>
</table>
<%-- 查询条件设定栏 --%>
<div id="exerGridToolbar">
	<div class="opline">
	</div>
	<form id="_form" class="shline">
		试卷名称：<input name='name' id="name" class='easyui-textbox' value='' data-options="iconWidth:16,
			icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
			}]"/>
		
		阅卷状态：<dc:insertList name="markStatus" dictId="ZD_MARK_STATUS" style="combo_normal"/>
		<a href="javascript:void(0)" onclick="doSearch()" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>
	</form>
</div>
</body>
</html>




