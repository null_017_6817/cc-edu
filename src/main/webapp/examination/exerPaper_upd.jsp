<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>修改学生答题试卷</title><!--exerPaper-->
	<%@include file="/common/include.jsp" %>
<script>
<%-- 查询页点击修改时调用此方法加载记录详情 --%>
function load(id){
	$('#_form').form('load','/${projectName}/examination/exerPaper/vieExerPaper.action?id='+id);
};

<%-- 点击保存按钮时调用此方法。(注:保存按钮在查询列表页) --%>
function submitForm(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#_form').form('submit',{
      success:function(data) {
      	parent.reloadData();
      }
   });
}
</script>
</head>
<body>
<form id="_form" method="post" action="/${projectName}/examination/exerPaper/updExerPaper.action">
	<input type="hidden" name='id' id='id'/>
	<table>
		<tr><td class='label' style="width: 168px">练习编号：</td><td><input id="exerId" name="exerId" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label'>学号：</td><td><input id="studNumber" name="studNumber" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label'>得分：</td><td><input id="score" name="score" class="easyui-textbox" data-options="required:true,validType:['intOrFloat']"/></td></tr>
		<tr><td class='label'>得分详情：</td><td><input id="scoredetail" name="scoredetail" class="easyui-textbox" data-options="required:true"/></td></tr>
	</table>

</form>
</body>
</html>
