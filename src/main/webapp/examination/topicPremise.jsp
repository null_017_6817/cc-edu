<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>背景信息</title>
	<%@include file="/common/include.jsp" %>
	<link rel="stylesheet" href="/${projectName}/book/js/layui/js/lib/layui2.3/css/layui.css">
	<link rel="stylesheet" href="/${projectName}/book/js/layui/fonts/font.css">
	<link rel="stylesheet" href="/${projectName}/book/js/layui/css/common.css">
	<link rel="stylesheet" href="/${projectName}/book/js/layui/css/style.css">
<style>
.layui-card-body img {
	max-width: 100%;
}


</style>
<script>
$(function(){
	var id ="${param.id}";
	initTopic(id)
})

function initTopic(id){
	$.ajax({
		url : "/${projectName}/examination/exerTopic/previewTopic.action" , 
		type : "post" , 
		data : {id:id} ,
		dataType:"json",
		success : function(result) {
			if(result.success){
				var remark = result.topic.remark;
				var title = result.topic.title;
				
				$(".layui-card-header").html("背景【"+title+"】");
				$(".layui-card-body").html(remark);
			}
		}
	});
}

</script>
</head>
<body style="background-color: #ffffff;">
<div style="padding: 20px; background-color: #ffffff;">
  <div class="layui-row layui-col-space15">
    <div class="layui-col-md12" style="padding: 0px 0px 0px 0px;">
      <div class="layui-card">
        <div class="layui-card-header">背景</div>
        <div class="layui-card-body">
        </div>
      </div>
    </div>
  </div>
</div> 
</body>
</html>




