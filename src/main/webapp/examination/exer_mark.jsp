<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>阅卷评分</title>
	<meta http-equiv="keywords" content="阅卷评分">
	<meta http-equiv="description" content="阅卷评分">
	<link rel="stylesheet" type="text/css"	href="/${projectName}/book/css/common.css" />
	<link rel="stylesheet" type="text/css"	href="/${projectName}/book/css/fonts.css" />
	<link rel="stylesheet" href="/${projectName}/book/js/layui/js/lib/layui2.3/css/layui.css">
	<link rel="stylesheet" type="text/css"	href="/${projectName}/book/js/layui/css/style.css">
	<link rel="stylesheet" type="text/css"	href="/${projectName}/book/css/style.css" />
	<script src="/${projectName}/book/js/layui/js/lib/layui2.3/layui.all.js"></script>
	<script type="text/javascript"	src="/${projectName}/book/js/lib/jquery-1.9.1.min.js"></script>
	<script type="text/javascript"	src="/${projectName}/examination/templateView/js/showStep.js"></script>
	<script type="text/javascript"	src="/${projectName}/book/js/audiojs/audio.min.js"></script>
	<script src="/${projectName}/common/artDialogs/artDialog.source.js?skin=default" type="text/javascript"></script>
<script src="/${projectName}/common/artDialogs/plugins/iframeTools.source.js" type="text/javascript"></script>
<style>

#topicTable{
		width: 100%;
	}
	#topicTable td.label {
		width: 50px;
	}
	#topicTable td {
		height: 26px;
		line-height: 20px;
	}
	#viewStep table {
		width:100%;
	}
	#viewStep td {
		padding: 15px 10px;	
	}
	/* #viewStep tr {
		border-bottom: 1px #333 dashed;
	} */
	#viewStep audio {
		width: 260px;
		margin-top: 8px;
	}
	#viewStep textarea {
		width: 99%;
		outline: none;
	}
	span[name='stepTitleNum']{
		width: 20px;
		height: 20px;
		background-color: #1792ee;
		color: #fff;
		display: block;
		text-align: center;
		font-size: 10px;
		margin-left: 35px;
		border-radius: 6px;
		vertical-align: middle;
		line-height: 19px;
	}

</style>
</head>
<body style="width:1000px;">
	<div class="yjpftcl">
		<div class="jbyhcon-title">
			<img src="/${projectName}/examination/img/yjpftc1.png">
			<p>学生试卷</p>
		</div>
		<p name="topicTitle" style="color:#333333;font-size:16px;line-height:40px;margin-left:30px;font-weight:600;"></p>
		<div id="viewStepMark" class="yjpftcl-con layui-form" style="font-size: 15px;line-height: 2;">			
		</div>
		<div class="yjpftcl-btn">
			<span onclick="nextTopic();" class="yjpftcl-conpfnl">下一题</span>
			<span onclick="preTopic();" class="yjpftcl-conpfnl">上一题</span>
			<span style="clear: both;display: block;"></span>
			<p class="yjpftcl-btn-save" onclick="saveBtn();">保存</p>
			<p class="yjpftcl-btn-close" onclick="cancelBtn();">关闭</p>
		</div>
	</div>
	<div class="yjpftcr">
		<span title="上一位考生" onclick="preStu();" class="change-s-left">&lt;</span>
		<span title="下一位考生" onclick="nextStu();" class="change-s-right">&gt;</span>
		<div class="yjpftcr-tx">
			<img name="studPhto" src="">
			<p style="text-align:center" name="stuNameP"></p>
			<p style="text-align:center" name="stuNumP">学号：${param.stuNum}</p>
		</div>
		<p class="yjpftcr-dtk">答题卡</p>
		<div class="yjpftcr-th">
		</div>
	</div>
	<div id="loadDiv" style="display:none" >
		<span><img src="/${projectName}/book/img/loading.gif" />正在查询，请稍后...</span>
	</div>
<script >
var projectName = "${projectName}";
var topics = new Array();
var teacherScoreMap = new Object();
var studAnswerMap = new Object();

var currentEtid; // 当前题目ID


/* audiojs.events.ready(function() {
    var as = audiojs.createAll();
}); */

var currentStuNum = "";
var studNumArr = [];
$(function(){
	// 渲染
	renderForm();
	var eid = "${eid}";
	currentStuNum = "${stuNum}";
	var studNumStr = "${stuNumArr}"
	studNumArr = eval("(" + studNumStr + ")");
	
	// 初始化答题卡列表
	initTopicList(eid,currentStuNum);
});
var aat;
// 初始化答题卡列表
function initTopicList(eid,studNum){
	$("#viewStepMark").html("");
	topics = new Array();// 题目列表
	teacherScoreMap = new Object();// 老师得分集合
	studAnswerMap = new Object();// 学生答案集合
	$(".yjpftcr-th").html("");
	$.ajax({
		type:"POST",
		url:'/${projectName}/examination/mark/markTopicsList.action',
		data:{"eid":eid,"studNum":studNum},
		dataType:'json',
		beforeSend:function(XMLHttpRequest){
			aat = art.dialog({
					opacity:0.1,
					title:"温馨提示",
					lock:true,
					content:$("#loadDiv").html(),
					});
		},
		success:function(result){
			aat.close();
			if(result.success){
				var stud = result.user;
				var phUrl = "/${projectName}/imgs/photos/"+stud.cnumber+"/"+stud.photoUrl;
				$("img[name='studPhto']").attr("src",phUrl);
				$("p[name='stuNumP']").html("学号:"+stud.number);
				$("p[name='stuNameP']").html("姓名:"+stud.name);
				
				topics = result.topics;// 题目列表
				teacherScoreMap = result.teacherScoreMap;// 老师得分集合
				studAnswerMap = result.studAnswerMap;// 学生答案集合
				if(topics != null && typeof(topics) != "undefined" ){
					var html = "";
					for(var i = 0 ; i < topics.length; i++){
						// 接受参数
						var topic = topics[i];
						var topicId = topic.topicId;
						var topicTitle = topic.topicTitle;
						var isFinish = topic.isFinish;
						// 判断此题是否完成阅卷
						var finishFlg = "";
						if(isFinish == 1){
							finishFlg = "yjpftcr-numed";
						}
						// 拼接html
						var index = (i+1);
						html += "<div data-id='"+topicId+"' onclick=\"clickTopicIndex('"+topicId+"','"+eid+"','"+studNum+"',this)\"" 
							 + " title='"+topicTitle+"' data-index='"+index+"' class='yjpftcr-num "+finishFlg+"'>"+index+"</div>";
					}
					$(".yjpftcr-th").html(html);
					
					$(".yjpftcr-num").eq(0).trigger("click");
				}
			}else{
				alert(result.msg);
			}
		}
	});
}

// 点击答题卡
function clickTopicIndex(topicId,eid,studNum,t){
	// 保存得分
	saveScore(eid,studNum,t);
	// 选中效果
	chooseIt(t);
	currentEtid = topicId;
	$.ajax({
		type:"POST",
		url:'/${projectName}/examination/exerTopic/previewTopic.action',
		data:{"id":topicId},
		dataType:'json',
		success:function(result){
			if(result.success){
				var topic = result.topic;
				var topicTypeMap = result.topicTypeMap;
				var stepTypeMap = result.stepTypeMap;
				if(topic != null){
					topic.topicTypeMap = topicTypeMap;
					topic.stepTypeMap = stepTypeMap;
				}
				// 回显topic
				var stepInfoList = result.steps;
				showTopic(topic,stepInfoList,"Mark");
			}else{
				alert(result.msg);
			}
		}
	});	
}

// 更新分数
function saveScore(eid,studNum,t){
	var flg = false;
	if(currentEtid != null && currentEtid != "" && typeof(currentEtid) != "undefined"){
		var JQscore = $(".mark_d");
		if(JQscore != null && typeof(JQscore) != "undefined" && JQscore.length > 0){
			var arr = new Array();
			$(".mark_d").each(function (i,item) {
				var select = $(this).find("select");
				var esid = select.attr("id");
				var score = select.val();
				var obj = new Object();
				obj.eid = eid;
				obj.etid = currentEtid;
				obj.esid = esid;
				obj.score = score;
				obj.studNumber=studNum;
				arr.push(obj);
				teacherScoreMap[esid]=score;
			});
			var param = JSON.stringify(arr);
			$.ajax({
				type:"POST",
				contentType : "application/json;charset=UTF-8",
				url:'/${projectName}/examination/exerAnswer/updScoreByTeacher.action',
				data:param,
				async: false,
				dataType:'json',
				success:function(result){
					if(result.success){
						flg = true;
					}
				}
			});
		}
	}
	return flg;
}


//显示topic
function showTopic(topic,stepInfoList,elementId){
	var title = topic.title;
	$("p[name='topicTitle']").html(title);
	
	stepData=[];
	showStepList(stepInfoList,elementId);
}

// 拼接步骤
function appendStep(elementId){
	$("#viewStep"+elementId).html("");
	if(stepData != null){
		for(var i = 0; i < stepData.length; i++){
			stepData[i].find("span[name='stepTitleNum']").parents("table").attr("class","");
			stepData[i].find("span[name='stepTitleNum']").html((i+1));
			var score = stepData[i].find("span[name='score']").html();
			var esid = stepData[i].find("input[name='esid']").val();
			// 得分处理
			var teaScore = teacherScoreMap[esid];
			if(teaScore == null || typeof(teaScore) == "undefined" || teaScore == ""){
				teaScore = 0;
			}
			
			if(studAnswer == null || typeof(studAnswer) == "undefined" ){
				studAnswer = "";
			}
			
			// 处理答案
			var eid = "${param.eid}";
			var etid = currentEtid;
			var studid = "${param.stuNum}";
			var questionType = stepData[i].find("input[name='questionType']").val();
			if(questionType == 'stateMode' || questionType == 'repeatMode' ){
				var filename = "/" + eid +"/" + studid +"/" + etid + "@@" + esid + ".wav";
				var path = getLocalAudioPath(studid,eid,etid,esid);
				var anserVoice = "<tr><td class='label w' style='text-align: right;'>学生录音：</td><td><audio controls='controls' data-file='"+filename+"'>"
								+"<source src='/${projectName}"+path+"' type='audio/mpeg'></audio></td></tr>";
				stepData[i].find("tbody").append(anserVoice);
			}else{
				if(questionType == "answerMode"){
					if(studAnswerMap[esid] != null && typeof(studAnswerMap[esid]) != "undefined"){
						var fileUrl = studAnswerMap[esid].fileUrl;
						var orgName = studAnswerMap[esid].fileOriginalName;
						var a = "<a href='javascript:void(0)' onclick=\"downFile('"+eid+"','"+etid+"','"+esid+"','"+studid+"')\"  type='audio/mpeg'>"+orgName+"</a>";
						stepData[i].find("tr[name='downStudTr']").find("td[name='downStudTd']").html(a);
					}
				}
				var studAnswer = "";
				if(studAnswerMap[esid] != null && typeof(studAnswerMap[esid]) != "undefined"
						&& studAnswerMap[esid].answer != null && typeof(studAnswerMap[esid].answer) != "undefined"){
					studAnswer = studAnswerMap[esid].answer;
				}
				studAnswer = studAnswer.replace(/@@@/g, '<br>');
				var answerHtm = "<tr><td class='label w' style='text-align: right;max-width:460px;'><span>学生答案:</span></td>"
					 + "<td style='max-width:470px;padding-left: 16px;'><span>"+studAnswer+"</span></td></tr>";
				stepData[i].find("tbody").append(answerHtm);
			}
			
			
			// 设置宽度等样式
			stepData[i].find(".label").each(function(){
				$(this).css({"text-align":"right","width": "70px;"});
			});
			stepData[i].find("span[name='standardAnswer']").parent("td").css({"max-width":"460px"});
			stepData[i].find("span[name='prompt']").parent("td").css({"max-width":"460px"});
			$("#viewStep"+elementId).append(stepData[i]);
			
			// 拼接得分项
			var opt = "";
			var qut = stepData[i].find("input[name='questionType']").val();
			/* if(qut == "clozedMode"){
				var empLen = stepData[i].find("tr[name='fillTr']").length;
				if(empLen != null && typeof(empLen) != "undefined"){
					score = score*empLen;
				}
			} */
			
			for(var j=0; j<=score; j=j+0.5){
				var select = "";
				if(j == teaScore){
					select = "selected"
				}
				opt += "<option "+select+" value='"+j+"'>"+j+"分</option>"
			}
			
			// 拼接得分
			var sc = "<div id='div"+esid+"' class='mark_d'><img src='/${projectName}/examination/img/jbyh4.png'>&nbsp;&nbsp;<p>本步骤得分</p><select id='"+esid+"' class='teacherScore' >"+opt+"</select></div>";
			$("#viewStep"+elementId).append(sc);
			$("#viewStep"+elementId).append("<hr>");
			renderForm();
		}
		
	}
	$("#viewStep"+elementId).find("audio").each(function(){
		var src = $(this).find("source").attr("src");
		if( src != null && src != '' && typeof(src) != "undefined" ){
			if(src.indexOf('.wav') != -1 || src.indexOf('.WAV') != -1 
					|| src.indexOf('.m4a') != -1 || src.indexOf('.M4A') != -1
					|| src.indexOf('.mp3') != -1 || src.indexOf('.MP3') != -1){
				$(this).parent("p").parent("td").find("button[name='audButton']").trigger("click");
			}
		}
	});
	$("#viewStep"+elementId).find("button").hide().next("br").remove();
	
	/* audiojs.events.ready(function() {
		var as = audiojs.createAll();
	}); */
	renderForm();
}

function getLocalAudioPath(studid,eid,etid,esid){
	var path = "";
	$.ajax({
		type:"POST",
		url:'/${projectName}/examination/exerAnswer/appendUrl.action',
		data:{"studid":studid,"eid":eid,"etid":etid,"esid":esid},
		async: false,
		dataType:'json',
		success:function(result){
			if(result.success){
				path = result.path;
			}
		}
	});
	return path;
}

// 下载附件（实操题）
function downFile(eid,etid,esid,studNum){
	window.open('/${projectName}/paper/question/downZipAnswer.action?eid='+eid+"&etid="+etid+"&esid="+esid+"&studNum="+studNum);
}

// 选择当前，之前的题目设置为已阅卷
function chooseIt(t){
	$(".yjpftcr-numing").addClass("yjpftcr-numed").removeClass("yjpftcr-numing")
	$(t).attr("class","yjpftcr-num").addClass("yjpftcr-numing");
}

//重新渲染表单
function renderForm(){
	layui.use('form', function(){
		var form = layui.form;
		form.render();
	});
}

function saveBtn(){
	var eid = "${eid}";
	var studNum = currentStuNum;
	var t = $(".yjpftcr-numing");
	var flg = saveScore(eid,studNum,t)
	if(flg){
		parent.okLayer(); //再执行关闭
	}else{
		layer.msg("操作失败")
	}
}

function cancelBtn(){
	parent.cancelLayer(); //再执行关闭
}

// 下一题
function nextTopic(){
	var len = $(".yjpftcr-th").find("div").length;
	if(len > 0){
		len = len-1;
		var ind = $(".yjpftcr-th").find("div[class='yjpftcr-num yjpftcr-numing']").index();
		if(ind < len){
			$(".yjpftcr-th").find("div").eq(ind+1).trigger("click");
		}else{
			$(".yjpftcr-th").find("div").eq(ind).trigger("click");
			layer.msg("已经是最后一道题目了！")
		}
	}
}

// 上一题
function preTopic(){
	var len = $(".yjpftcr-th").find("div").length;
	if(len > 0){
		len = len-1;
		var ind = $(".yjpftcr-th").find("div[class='yjpftcr-num yjpftcr-numing']").index();
		if(ind > 0){
			$(".yjpftcr-th").find("div").eq(ind-1).trigger("click");
		}else{
			$(".yjpftcr-th").find("div").eq(ind).trigger("click");
			layer.msg("已经是第一道题目了！");
		}
	}
}

// 上个学生
function preStu(){
	var curStum = currentStuNum;
	var curInd = 0;
	var len = studNumArr.length;
	if(len > 0){
		for(var i = 0; i < len; i++ ){
			if(curStum == studNumArr[i]){
				curInd = i
			}
		}
		if(curInd > 0){
			var preStum = studNumArr[curInd-1];
			currentStuNum = preStum;
			
			art.dialog.confirm('即将切换到上一位考生，是否保存该学生当前题目分数', function () {
				var eid = "${eid}";
				var studNum = currentStuNum;
				var t = $(".yjpftcr-numing");
				var flg = saveScore(eid,curStum,t);
				if(flg){
					initTopicList("${eid}",preStum);
				}else{
					layer.msg("保存当前题目失败");
				}
			}, function () {
				initTopicList("${eid}",preStum);
			});
			
		}else{
			layer.msg("已经是第一位考生了！")
		}
	}
	
}

// 下个学生
function nextStu(){
	var curStum = currentStuNum;
	var curInd = 0;
	var len = studNumArr.length;
	if(len > 0){
		for(var i = 0; i < len; i++ ){
			if(curStum == studNumArr[i]){
				curInd = i
			}
		}
		if(curInd < (len-1)){
			var nextStum = studNumArr[curInd+1];
			currentStuNum = nextStum
			art.dialog.confirm('即将切换到下一位考生，是否保存该学生当前题目分数', function () {
				var eid = "${eid}";
				var studNum = curStum;
				var t = $(".yjpftcr-numing");
				var flg = saveScore(eid,studNum,t);
				if(flg){
					initTopicList("${eid}",nextStum);
				}else{
					layer.msg("保存当前题目失败");
				}
			}, function () {
				initTopicList("${eid}",nextStum);
			});
		}else{
			layer.msg("已经是最后一位考生了！")
		}
	}
}

</script>
<div style="display:none;">
	<!-- 单选题(语音) -->
	<jsp:include page="./templateView/select-temp.jsp" />
	<!-- 选择题(语音) -->
	
	<!-- 多选题(语音) -->
	<jsp:include page="./templateView/selects-temp.jsp" />
	<!-- 选择题(语音) -->
	
	<!-- 听打录入 -->
	<jsp:include page="./templateView/cloze-temp.jsp" />
	<!-- 听打录入 -->
	
	<!-- 填空题 -->
	<jsp:include page="./templateView/clozed-temp.jsp" />
	<!-- 填空题 -->
	
	<!-- 问答题（语音） -->
	<jsp:include page="./templateView/answer-temp.jsp" />
	<!-- 问答题（语音） -->
	
	<!-- 应答题（语音） -->
	<jsp:include page="./templateView/state-temp.jsp" />
	<!-- 应答题（语音） -->
	
	<!-- 语音提示题（语音） -->
	<jsp:include page="./templateView/voice-temp.jsp" />
	<!-- 语音提示题（语音） -->
	
	<!-- 复读陈述题（语音） -->
	<jsp:include page="./templateView/repeat-temp.jsp" />
	<!-- 复读陈述题（语音） -->
								
	<!-- 判断题(语音) -->
	<jsp:include page="./templateView/judge-temp.jsp" />
	<!-- 判断题(语音) -->
	
</div>
</body>
</html>