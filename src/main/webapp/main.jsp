<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.qtrmoon.manager.pojo.Person" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>呼叫中心</title>
	<%@include file="/common/include.jsp" %>
	<script>
	$(function(){
		//加载Module
		/* $.get("/${projectName}/sysmanage/module/schMyModule.action",function(data){
			if(data!=""){
				var list=eval("("+data+")").rows;
				var res="";
				for(var i=0;i<list.length;i++){
					res+="<li onclick=\"javascript:openModule("+list[i].id+")\"><a><img src='/${projectName}"+list[i].icon+"'/><br/>"+list[i].name+"</a></li>";
				}
				$("#module").html(res);
			}
		}); */
		openModule(2);
	});
	
	function openModule(moduleid){
		$.get("/${projectName}/sysmanage/module/schMenuByModule.action",{id:moduleid},function(data){
			if(data!=""){
				var jasonData = eval("("+data+")");
				console.log(jasonData);
				if(jasonData.success){
					var list=jasonData.rows;
					var res="";
					for(var i=0;i<list.length;i++){
						var fun=list[i];
						var img="";
						if(fun.picico!=""){
							img="<img src='/${projectName}"+fun.picico+"'/>";
						}
						 
						if(fun.isleaf=="1"&&fun.level=="2"){//二级的叶子节点
							res+="<div onclick=\"javascript:addTab(this, '"+fun.name+"','/${projectName}/"+fun.link+"')\">"+img+"<a>"+fun.name+"</a></div>";
						}else if(fun.isleaf=="0"&&fun.level=="2"){//二级的非叶子节点
							res+="<div  id='"+fun.name+"' class='mendiv'><span class='menuer' onclick=\"javascript:getChildTab(this,'"+fun.name+"','"+fun.id+"')\" >"+img+"<a class='item item1'>"+fun.name+"</a></span>";
							res+="<ul class='content'>";
							res+="</ul>";
							res+="</div>";
						}else if(fun.isleaf=="1"&&fun.level=="3"){//三级的叶子节点
							/* res+="<li>";
								res+="<div onclick=\"javascript:addTab(this, '"+fun.name+"','/${projectName}/"+fun.link+"')\">"+img+"<a>"+fun.name+"</a></div>";
							res+="</li>"; */
						}
						
					}
					$("#menu").html(res);
				}else{
					window.location.href="/${projectName}/sysmanage/logout/bookLogout.action";
				}
			}
		});
	}
	function getChildTab(q,funName,funId){
		var res ="";
		$.get("/${projectName}/sysmanage/module/schChildMenuByPid.action",{pid:funId},function(data){
			if(data!=""){
				var jasonData = eval("("+data+")");
				if(jasonData.success){
					var list=jasonData.rows;
					for(var i=0;i<list.length;i++){
						var fun=list[i];
						res+="<li  onclick=\"javascript:addTab(this, '"+fun.name+"','/${projectName}/"+fun.link+"')\">";
						res+="<a class='item item2' >"+fun.name+"</a>";
						res+="</li>";
					}
					$("#"+funName+" .content").html(res);
					//debugger;
					$(q).next(".content").slideToggle();
				}
			}
		});
		
			
		
	}
	
	function addTab(t,title, url){
		if ($('#tabdiv').tabs('exists', title)){
			$('#tabdiv').tabs('select', title);
		} else {
			var content = '<iframe scrolling="no" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
			$('#tabdiv').tabs('add',{
				title:title,
				content:content,
				closable:true
			});
			// 获取最后一个选项卡
			/*
			var li = $(".tabs-wrap ul li:last-child");
			$("#close").remove();
			li.after("<li id='close'><a class='tabs-inner' href='javascript:void(0);' onClick='javascript:closeAll();'>关闭全部</a></li>");
			*/
		}
		//$(t).prevAll().attr("style","")
		//$(t).nextAll().attr("style","")
		$(".addtab").removeClass();
		$(t).addClass("addtab");
	}
	
	function cd() {
		var $easyuiTheme = $('#easyuiTheme');
		var url = $easyuiTheme.attr('href');
		$easyuiTheme.attr('href', '/${projectName}/easyui1_5/themes/default/easyui.css');
	};

	/* 关闭选项卡右键菜单 */
	$(document).ready(function() {
		$("#tabdiv").tabs({
			onContextMenu: function(e, title, index) {
				e.preventDefault();
				if(index>=0) {
					$("#closeMenu").menu(
						"show",
						{
							left: e.pageX,
							top: e.pageY
						}
					).data("tabTitle", title);
				}
			}
		});
		// 右键菜单项点击事件
		$("#closeMenu").menu({
			onClick: function(item) {
				closeTab(this, item.name);
			}
		});
	})

	function closeAll() {
		$(".tabs li").each(function(index, obj) {
			// 获取所有可关闭选项卡
			var tab = $(".tabs-closable", this).text();
			$(".easyui-tabs").tabs("close", tab);
		});
		$("#close").remove();
	}

	function closeTab(menu, type) {
		var allTabs = $("#tabdiv").tabs("tabs");
		var allTabtitle = [];
		$.each(allTabs, function(i, n) {
			var opt = $(n).panel("options");
			if(opt.closable) {
				allTabtitle.push(opt.title);
			}
		});
		var curTabTitle = $(menu).data("tabTitle");
		var curTabIndex = $("#tabdiv").tabs("getTabIndex", $("#tabdiv").tabs("getTab", curTabTitle));
		switch (type) {
			case "1":
				$(".easyui-tabs").tabs("close", curTabIndex);
				return false;
				break;
			case "2":
				for(var j = 0; j < allTabtitle.length; j++) {
					console.log(j != curTabIndex);
					if(j != curTabIndex) {
						$(".easyui-tabs").tabs("close", allTabtitle[j]);
					}
				}
				break;
			case "3":
				for(var i = 0; i < allTabtitle.length; i++) {
					$(".easyui-tabs").tabs("close", allTabtitle[i]);
				}
				break;
			default:
				break;
		}
	}
	
	// 主页面显示show消息
	function mainShow(title,msg,width,height,time){
		$.messager.show({
			title: title,msg: msg,showType: 'slide',
			width: width,height: height,timeout: time
		});
	}
	// 主页面显示alert消息
	function mainAlert(title,msg,type){
		$.messager.alert(title,msg,type);
	}
	
	</script>
	<style>
	.panel-body{overflow:hidden}
	
	.logo{
		background: url('/${projectName}/common/img/huatang.png') no-repeat;
    	width: 188px;
    	height: 63px;
    	float: left;
    	margin-top: 5px;
    	margin-left: 10px;
    	background-size: 180px auto;
    }
	
	#module{margin:0;padding:0;}
	#module li{
		width:80px;padding:10px;text-align:center;float:left;cursor:pointer;border-top-color:rgba(0,0,0,0.0);border-top-style:solid;border-top-width:5px;list-style-type:none;
		background: linear-gradient(#56545f, #35383d); /* 标准的语法 */
		transition: background 1s,border-top-color 1s;
		-moz-transition: background 1s,border-top-color 1s;	/* Firefox 4 */
		-webkit-transition: background 1s,border-top-color 1s;	/* Safari 和 Chrome */
		-o-transition: background 1s,border-top-color 1s;	/* Opera */
	}
	#module li:hover{background:#162436;border-top-color:#e43c59;}
	#module li a{color:white;text-decoration:none;}
	
	#menu{background:#44484e;border:0;}
	#menu div{line-height:50px;display:block;font-size:14px;padding-left:30px;
		border-left-color:#293c55;border-left-style:solid;cursor:pointer;boder-radius:2px;color:#ccc;
		transition: background 1s,border-left-color 1s;
		-moz-transition: background 1s,border-left-color 1s;	/* Firefox 4 */
		-webkit-transition: background 1s,border-left-color 1s;	/* Safari 和 Chrome */
		-o-transition: background 1s,border-left-color 1s;	/* Opera */
	}
	#menu div:hover{background:linear-gradient(#ffa400, #ff7c00);border-color:#ff4800;border-left-color:#ff8f00;color:white;}
	#menu .mendiv{
	   padding-left:0;
	}
	#menu .mendiv:hover{background:initial;}
	#menu img{width:20px;vertical-align:middle;margin-right:5px;}

	.tabs li.tabs-selected a {
		background-color: rgba(255, 158, 1, .65) !important;
	}
	#menu .menuer{
	    line-height:50px;display:block;font-size:14px;padding-left:30px;
		border-left-color:#293c55;border-left-style:solid;cursor:pointer;boder-radius:2px;color:#ccc;
		transition: background 1s,border-left-color 1s;
		-moz-transition: background 1s,border-left-color 1s;	/* Firefox 4 */
		-webkit-transition: background 1s,border-left-color 1s;	/* Safari 和 Chrome */
		-o-transition: background 1s,border-left-color 1s;	/* Opera */
    }
    #menu .menuer:hover,.content li:hover,.addtab{background:linear-gradient(#ffa400, #ff7c00);border-color:#ff4800;border-left-color:#ff8f00;color:white;}
	.content{
		display:none;
	}	
	.content li{
	    padding-left:60px;	
	     line-height:50px;display:block;font-size:14px;
		border-left-color:#293c55;border-left-style:solid;cursor:pointer;boder-radius:2px;color:#ccc;
		transition: background 1s,border-left-color 1s;
		-moz-transition: background 1s,border-left-color 1s;	/* Firefox 4 */
		-webkit-transition: background 1s,border-left-color 1s;	/* Safari 和 Chrome */
		-o-transition: background 1s,border-left-color 1s;	/* Opera */    
	}
	.content li a{
	    text-decoration: none;
	    color:#fff;
	}
	</style>
</head>
<body class="easyui-layout">
<%
Person user = (Person) request.getSession().getAttribute("CURRENT_USER");
String userName = "";
if(user != null){
	userName = user.getName();
}
%>
	<div data-options="region:'north',border:false" style="height:65px;background: linear-gradient(#56545f, #35383d); /* 标准的语法 */;">
		<div class="logo"></div>
		<ul id="module">
			
		</ul>
		<div style="float:right;margin-right:30px;line-height:60px; font-size: 14px;" >
			<a href="/${projectName}/sysmanage/logout/logout.action" title="退出" style="color:#FFFFFF;">退出</a>
		</div>
		<div style="float:right;margin-right:30px;line-height:60px; font-size: 14px; color:#FFFFFF;" >
			<c:if test="${!empty CURRENT_USER.photoUrl}">	
				<img src="/${projectName}/imgs/photos/${CURRENT_USER.cnumber}/${CURRENT_USER.photoUrl}" style="width:50px;vertical-align:middle;border-radius:50px;height:50px;"/>
			</c:if>
			<c:if test="${empty CURRENT_USER.photoUrl}">	
				<img src="/${projectName}/common/mico/login.png" style="width: 26px;vertical-align:middle;"/>
			</c:if>
			<span>[<%=userName%>]</span>
		</div>
	</div>
	<div data-options="region:'west',split:true" style="width:200px;padding:0px;overflow: auto;" id="menu">
		
	</div>
	<!-- <div data-options="region:'east',split:true,collapsed:true,title:'East'" style="width:100px;padding:10px;">east region</div> -->
	<div data-options="region:'south',border:false" style="height:30px;text-align:center;line-height:30px;padding-left:30px;background:#EEE;padding:0px;">Copyright 2013-2017 北京华唐中科科技集团有限公司 <a href="http://www.cnccss.cn/" target="_blank">www.cnccss.cn</a></div>
	<div data-options="region:'center'">
		<div id="tabdiv" class="easyui-tabs" data-options="fit:true,border:false,plain:true" fit="true">
		</div>
	</div>

	<div id="closeMenu" class="easyui-menu" style="width: 120px;">
		<div id="closeAll" name="1">关闭当前</div>
		<div id="closeAll" name="2">关闭其他</div>
		<div id="closeAll" name="3">关闭全部</div>
	</div>
</body>
</html>
