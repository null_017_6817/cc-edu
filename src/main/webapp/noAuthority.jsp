<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>权限过期</title>
	<style>
	</style>
</head>
<body>
	<div style="padding-top: 100px;background: #fbf3f3;padding-left: 203px;
		padding-bottom: 47px;margin-top: 142px; border-radius: 30px;">
		<img src="/${projectName}/imgs/common/warning.png" style="width:105px" />
		<h2 style="color:red;">温馨提示：您的使用权限已到期，请联系软件开发商！</h2>
		<h5>联系电话：010-1111111</h5>
		<h5>手机号码：18511111111</h5>
	</div>
</body>
</html>