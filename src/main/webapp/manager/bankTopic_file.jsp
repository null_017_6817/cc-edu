<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>修改学生</title>
	<%@include file="/common/include.jsp" %>
<script>
<%-- 查询页点击修改时调用此方法加载记录详情 --%>
/*  function load(oid){
	$('#_form').form('load','/${projectName}/manager/bank/uploadFile.action?backId='+oid);
};
 */
<%-- 点击保存按钮时调用此方法。(注:保存按钮在查询列表页) --%>
function submitForm(){
	if(!$("#_form").form("validate")){
		return; 
	}
	window.parent.openStatusDialog();
	$('#_form').form('submit',{
      success:function(data) {
    	console.log(data);
		var res = JSON.parse(data);
		console.log(res);
		var status = res.status;
		var logfile = res.logfile;
		if("1" == status) {
      		parent.reloadData();
      		window.parent.closeStatusDialog();
			$("#log").hide();
			alert("操作成功");
		}else if("0" == status) {
      		window.parent.closeStatusDialog();
			$("#logfile").val(logfile);
			$("#log").show();
		}else if("2"){
			window.parent.closeStatusDialog();
			alert("文件格式不正确！");
		}else {
      		window.parent.closeStatusDialog();
		}
      }
   });
}

function downloadExcelError() {
	var logfile = $("#logfile").val();
	window.open("/${projectName}/manager/bank/download.action?logfile="+logfile);
}
</script>
</head>
<body>
<form id="_form" method="post" action="/${projectName}/manager/bank/uploadFile.action" enctype="multipart/form-data">
			<input type="hidden" id="bankId" name='bankId' value="${bankId}"/>
 			<div id="bankTopicGrid" align="center"><br><br>
 			<input name="file" id="file" class="easyui-filebox" data-options="buttonText:'选择文件', accept:'' "/>请选择后缀为.xml, .zip, .xls或者.xlsx文件上传 
			<div id="log" style="margin-top: 15px;display:none;">
			<span id="log_tip" style="color: #b32b21;">Excel解析失败，详情请参考错误日志.</span>
			<br>
			<a id="log_error" href="javascript:void(0)" onclick="downloadExcelError();" class="btn btn-orange" style="margin-top:10px;"><span class="icon-download-alt" style="font-size:16px"></span> <span style="font-size:14px">错误日志</span></a>&emsp;
			<input id="logfile" value="日志文件" style="display:none;" />
			</div>
 			</div>
</form>
</body>
</html>
