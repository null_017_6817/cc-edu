<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>修改题库</title>
	<%@include file="/common/include.jsp" %>
<script>
<%-- 查询页点击修改时调用此方法加载记录详情 --%>
function load(id){
	$('#_form').form('load','/${projectName}/manager/bank/vieBank.action?bankId='+id);
};

<%-- 点击保存按钮时调用此方法。(注:保存按钮在查询列表页) --%>
function submitForm(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#_form').form('submit',{
      success:function(data) {
      	parent.reloadData();
      }
   });
}
</script>
</head>
<body>
<form id="_form" method="post" action="/${projectName}/manager/bank/updBank.action">
	<input type="hidden" name='bankId' id='bankId'/>
	<table>
		<tr><td class='label' style="width: 168px">分校编号：</td><td><input id="vccId" name="vccId" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label'>题库名：</td><td><input id="bankName" name="bankName" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label'>级别：</td><td><dc:insertList name="level" dictId="ZD_BANKLEVEL" style="combo_normal"/></td></tr>
		<tr><td class='label'>行业：</td><td><input id="profession" name="profession" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label'>备注：</td><td><input id="remark" name="remark" class="easyui-textbox" data-options="required:true"/></td></tr>
<!-- 		<tr><td class='label'>创建时间：</td><td><input id="createTime" name="createTime" class="easyui-datebox" data-options="required:true"/></td></tr>  -->
		<tr><td class='label'>状态：</td><td><dc:insertList name="status" dictId="ZD_BANKSTATUS" style="combo_normal"/></td></tr>
	</table>

</form>
</body>
</html>
