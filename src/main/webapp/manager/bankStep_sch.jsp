<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>答题步骤查询</title>
	<%@include file="/common/include.jsp" %>

<script>
$(function(){
	<%-- 为分页行设置"添加"和"删除"按钮 --%>
	var pager = $('#bankStepGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});			
})

<%-- 执行查询，点击查询按钮时调用 --%>
function doSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#bankStepGrid').datagrid('load',{
		bankId:$('#bankId').val(),
		topicId:$('#topicId').val(),
		stepId:$('#stepId').val(),
		type:$('#type').val(),
		title:$('#title').val(),
		score:$('#score').val()
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatOper(val,row,index){  
	 return "&emsp;<a href='javascript:void(0)' onclick='updBankStep("+val+")' class='_oper'>&nbsp;<i class='icon-edit'></i>&nbsp;</a>"
		+"<a href='javascript:void(0)' onclick='vieBankStep("+val+")' class='_oper'>&nbsp;<i class='icon-info-sign'></i>&nbsp;</a>&emsp;"; 
} 

<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
function updBankStep(id){
	QMoveDialog($("#bankStepEditWin"));
	$('#bankStepViewWin').dialog('close');
	$('#bankStepEditWin').dialog('open');
	$('#bankStepEditWin').get(0).contentWindow.load(id);
} 

<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
function vieBankStep(id){
	QMoveDialog($("#bankStepViewWin"));
	$('#bankStepEditWin').dialog('close');
	$('#bankStepViewWin').dialog('open');
	$('#bankStepViewWin').get(0).contentWindow.load(id);
}

<%-- 删除记录 --%>
function delBankStep(){
	var checkedItems = $('#bankStepGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/manager/bankStep/delBankStep.action",{ids:ids},function(data){
				$('#bankStepGrid').datagrid('reload');
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#bankStepEditWin').dialog('close');
	$('#bankStepGrid').datagrid('reload');
}
</script>
</head>
<body>
<table id="bankStepGrid" title="答题步骤" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#bankStepGridToolbar',collapsible:true,url:'/${projectName}/manager/bankStep/schBankStep.action',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" ></th>
			<th data-options="field:'vccId',width:25,align:'center',sortable:true">分校编号</th>
			<th data-options="field:'bankId',width:25,align:'center',sortable:true">题库编号</th>
			<th data-options="field:'stepId',width:25,align:'center',sortable:true">步骤编号</th>
			<th data-options="field:'type',width:25,align:'center',sortable:true">步骤类型</th>
			<th data-options="field:'title',width:25,align:'center',sortable:true">标题</th>
			<th data-options="field:'score',width:25,align:'center',sortable:true">分值</th>
			<!-- <th data-options="field:'preStepId',sortable:true">上一步</th>
			<th data-options="field:'param',sortable:true">参数</th>
			<th data-options="field:'standardAnswer',sortable:true">标准答案</th>
			<th data-options="field:'topicId',sortable:true">考题编号</th> -->

			<th data-options="field:'_oper',align:'center',formatter:formatOper">操作</th>
		</tr>
	</thead>
</table>
<%-- 查询条件设定栏 --%>
<div id="bankStepGridToolbar"> 
	<div class="opline">
	<a href="javascript:void(0)" onclick="updBankStep(0)" class="btn btn-orange"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">添加</span></a>&emsp;
			<a href="javascript:void(0)" onclick="delBankStep()" class="btn btn-orange"><span class="icon-trash" style="font-size:16px"></span> <span style="font-size:14px">删除</span></a>&emsp;
	</div>
	<form id="_form" class="shline">
	标题：<input name='title' id="title" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
分值：<input name='score' id="score" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	<!-- 题库编号：<input name='bankId' id="bankId" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	考题编号：<input name='topicId' id="topicId" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	步骤编号：<input name='stepId' id="stepId" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	步骤类型：<input name='type' id="type" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/> -->
	
	<a href="javascript:void(0)" onclick="doSearch()" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>
	</form>
</div>


<%-- 修改or添加面板 --%>
<iframe id="bankStepEditWin" src="/${projectName}/manager/bankStep/page.action?page=bankStep_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#bankStepEditButton'" title="修改答题步骤" style="width:700px;height:500px;padding:5px;top:5000px" scrolling="no" frameborder="0"></iframe>
<div id="bankStepEditButton">
	<a href="javascript:void(0)" onclick="$('#bankStepEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#bankStepEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >取 消</a>
</div>
<%-- 查看面板 --%>
<iframe id="bankStepViewWin" src="/${projectName}/manager/bankStep/page.action?page=bankStep_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#bankStepViewButton'" title="查看答题步骤" style="width:700px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="bankStepViewButton">
	<a href="javascript:$('#bankStepViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >关 闭</a>
</div>
</body>
</html>
