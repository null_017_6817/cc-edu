<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>题库查询</title>
	<%@include file="/common/include.jsp" %>

<script>
$(function(){
	<%-- 为分页行设置"添加"和"删除"按钮 --%>
	var pager = $('#bankGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});			
})

<%-- 执行查询，点击查询按钮时调用 --%>
function doSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#bankGrid').datagrid('load',{
		bankName:$('#bankName').val(),
		level:levelDict.getValue(),
		profession:$('#profession').val(),
		status:statusDict.getValue()
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatOper(val,row,index){
    return "&emsp;<a href='javascript:void(0)' onclick='updBank("+val+")' class='_oper'>&nbsp;<i class='icon-edit'></i>&nbsp;</a>";
		//+"<a href='javascript:void(0)' onclick='vieRepository("+val+")' class='_oper'>&nbsp;<i class='icon-info-sign'></i>&nbsp;</a>&emsp;"; 
} 

<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
function updBank(id){
	QMoveDialog($("#bankEditWin"));
// 	$('#bankViewWin').dialog('close');
	$('#bankEditWin').dialog('open');
	$('#bankEditWin').get(0).contentWindow.load(id);
} 

<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
// function vieBank(id){
// 	QMoveDialog($("#bankViewWin"));
// 	$('#bankEditWin').dialog('close');
// 	$('#bankViewWin').dialog('open');
// 	$('#bankViewWin').get(0).contentWindow.load(id);
// }

<%-- 删除记录 --%>
function delBank(){
	var checkedItems = $('#bankGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.bankId);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/manager/bank/delBank.action",{ids:ids},function(data){
				$('#bankGrid').datagrid('reload');
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#bankEditWin').dialog('close');
	$('#bankGrid').datagrid('reload');
}

//上传题目到题库
function topicaa(){
	var row = $('#bankGrid').datagrid('getSelected');
	if (!row && typeof(row)!="undefined" && row!=0){
		alert("请选择题库！！！");
	}else{
		window.location.href="/${projectName}/manager/bank/page.action?page=newbankTopic_sch&bankId="+row.bankId;
	}
}
</script>
</head>
<body>
<table id="bankGrid" title="题库" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#bankGridToolbar',collapsible:true,url:'/${projectName}/manager/bank/schBank.action',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" ></th>
			<th data-options="field:'bankId',width:25,align:'center',sortable:true">题库编号</th>
			<th data-options="field:'bankName',width:25,align:'center',sortable:true">题库名称</th>
			<th data-options="field:'profession',width:25,align:'center',sortable:true">行业</th>
			<th data-options="field:'createTime',width:25,align:'center',sortable:true">练习开始时间</th>
			<th data-options="field:'status',width:25,align:'center',sortable:true">状态</th>
			<th data-options="field:'level',width:25,align:'center',sortable:true">级别</th>
			<!-- <th data-options="field:'remark',sortable:true">备注</th> -->
			<th data-options="field:'_oper',align:'center',formatter:formatOper">操作</th>
		</tr>
	</thead>
</table>
<%-- 查询条件设定栏 --%>
<div id="bankGridToolbar">
	<div  class="opline">
		<a href="javascript:void(0)" onclick="updBank(0)" class="btn btn-orange"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">添加</span></a>&emsp;
		<a href="javascript:void(0)" onclick="delBank()" class="btn btn-orange"><span class="icon-trash" style="font-size:16px"></span> <span style="font-size:14px">删除</span></a>&emsp;
		<a href="javascript:void(0)" onclick="topicaa()" class="btn btn-orange"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">查看及上传题目</span></a>&emsp;
	</div>
	<form id="_form" class="shline">
	题库名称：<input name='bankName' id="bankName" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	行业：<input name='profession' id="profession" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	状态：<dc:insertList name="status" dictId="ZD_BANKSTATUS" style="combo_normal"/>
	级别：<dc:insertList name="level" dictId="ZD_BANKLEVEL" style="combo_normal"/>
	
	<a href="javascript:void(0)" onclick="doSearch()" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>
	</form>
</div>


<%-- 修改or添加面板 --%>
<iframe id="bankEditWin" src="/${projectName}/manager/bank/page.action?page=bank_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#bankEditButton'" title="修改题库" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="no" frameborder="0"></iframe>
<div id="bankEditButton">
	<a href="javascript:void(0)" onclick="$('#bankEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#bankEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >取 消</a>
</div>
<%-- 查看面板 --%>
<%-- <iframe id="bankViewWin" src="/${projectName}/manager/bank/page.action?page=bank_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#bankViewButton'" title="查看题库" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="bankViewButton">
	<a href="javascript:$('#bankViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >关 闭</a>
</div> --%>
</body>
</html>
