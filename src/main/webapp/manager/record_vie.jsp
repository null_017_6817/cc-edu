<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>查看</title><!--record-->
	<%@include file="/common/include.jsp" %>
<script>
function load(id){
	$('#_form').form('load','/${projectName}/manager/record/vieRecord.action?toL=true&id='+id);
};
</script>
</head>
<body>
<form id="_form">
	<input type="hidden" name='id' value=""/>
	<table>
		<tr><td class='label'>任务ID：</td><td><input type='text' name='taskid' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>拨打人：</td><td><input type='text' name='fromnum' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>接听人：</td><td><input type='text' name='tonum' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>录音名称：</td><td><input type='text' name='voice' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>状态：</td><td><input type='text' name='status' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>类型：</td><td><input type='text' name='type' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>创建时间：</td><td><input type='text' name='createtime' class='viewbox' readonly='readonly'/></td></tr>
	</table>

</form>
</body>
</html>
