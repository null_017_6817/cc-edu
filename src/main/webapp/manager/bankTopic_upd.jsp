<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>修改题目</title>
	<%@include file="/common/include.jsp" %>
<script>
<%-- 查询页点击修改时调用此方法加载记录详情 --%>
function load(id){
	$('#_form').form('load','/${projectName}/manager/bankTopic/vieBankTopic.action?topicId='+id);
};

<%-- 点击保存按钮时调用此方法。(注:保存按钮在查询列表页) --%>
function submitForm(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#_form').form('submit',{
      success:function(data) {
      	parent.reloadData();
      }
   });
}
</script>
</head>
<body>
<form id="_form" method="post" action="/${projectName}/manager/bankTopic/updBankTopic.action">
	<input type="hidden" name='topicId' id='topicId'/>
	<table>
		<tr>
			<td class='label' style="width: 150px">
			分校编号：
			</td>
			<td>
			<input id="vccId" name="vccId" class="easyui-textbox" data-options="required:true"/>
			</td>
			<td class='label' style="width: 150px">
			题库编号：
			</td>
			<td>
				<input id="bankId" name="bankId" class="easyui-textbox" data-options="required:true"/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			标题：
			</td>
			<td>
			<input id="title" name="title" class="easyui-textbox" data-options="required:true"/>
			</td>
			<td class='label'>
			类型：
			</td>
			<td>
				<input id="type" name="type" class="easyui-textbox" data-options="required:true"/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			级别：
			</td>
			<td>
			<input id="level" name="level" class="easyui-textbox" data-options="required:true"/>
			</td>
			<td class='label'>
			行业：
			</td>
			<td>
				<input id="profession" name="profession" class="easyui-textbox" data-options="required:true"/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			关键字：
			</td>
			<td>
			<input id="keyWords" name="keyWords" class="easyui-textbox" data-options="required:true"/>
			</td>
			<td class='label'>
			难度：
			</td>
			<td>
				<input id="difficulty" name="difficulty" class="easyui-textbox" data-options="required:true"/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			作者：
			</td>
			<td>
			<input id="author" name="author" class="easyui-textbox" data-options="required:true"/>
			</td>
			<td class='label'>
			时间：
			</td>
			<td>
				<input id="creattime" name="creattime" class="easyui-textbox" data-options="required:true"/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			备注：
			</td>
			<td>
			<input id="remark" name="remark" class="easyui-textbox" data-options="required:true"/>
			</td>
			<td class='label'>
			合并录音：
			</td>
			<td>
				<input id="mergeVoice" name="mergeVoice" class="easyui-textbox" data-options="required:true,validType:['int']"/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			客户资料：
			</td>
			<td>
			<input id="customer" name="customer" class="easyui-textbox" data-options="required:true"/>
			</td>
			<td class='label'>
			话术：
			</td>
			<td>
				<input id="talkSkill" name="talkSkill" class="easyui-textbox" data-options="required:true"/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			总分：
			</td>
			<td>
			<input id="totalScore" name="totalScore" class="easyui-textbox" data-options="required:true,validType:['int']"/>
			</td>
			<td class='label'>
			及格分：
			</td>
			<td>
				<input id="passScore" name="passScore" class="easyui-textbox" data-options="required:true,validType:['int']"/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			第一个有效步骤：
			</td>
			<td>
			<input id="beginStepId" name="beginStepId" class="easyui-textbox" data-options="required:true"/>
			</td>
			<td class='label'>
			入库时间：
			</td>
			<td>
				<input id="createTime" name="createTime" class="easyui-textbox" data-options="required:true"/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			状态：
			</td>
			<td>
			<input id="status" name="status" class="easyui-textbox" data-options="required:true,validType:['int']"/>
			</td>
			<td class='label'>
			：
			</td>
			<td>
				<input id="capability" name="capability" class="easyui-textbox" data-options="required:true"/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			课程模块：
			</td>
			<td>
			<input id="bookmodul" name="bookmodul" class="easyui-textbox" data-options="required:true"/>
			</td>
			<td class='label'>
			文档A标题：
			</td>
			<td>
				<input id="docATitle" name="docATitle" class="easyui-textbox" data-options="required:true"/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			文档A答案：
			</td>
			<td>
			<input id="docATmplt" name="docATmplt" class="easyui-textbox" data-options="required:true,validType:['int']"/>
			</td>
			<td class='label'>
			文档B标题：
			</td>
			<td>
				<input id="docBTitle" name="docBTitle" class="easyui-textbox" data-options="required:true"/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			文档B答案：
			</td>
			<td>
			<input id="docBTmplt" name="docBTmplt" class="easyui-textbox" data-options="required:true,validType:['int']"/>
			</td>
			<td class='label'>
			文档C标题：
			</td>
			<td>
				<input id="docCTitle" name="docCTitle" class="easyui-textbox" data-options="required:true"/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			文档C答案：
			</td>
			<td>
			<input id="docCTmplt" name="docCTmplt" class="easyui-textbox" data-options="required:true,validType:['int']"/>
			</td>
			<td class='label'>
			文档D标题：
			</td>
			<td>
				<input id="docDTitle" name="docDTitle" class="easyui-textbox" data-options="required:true"/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			文档D答案：
			</td>
			<td>
			<input id="docDTmplt" name="docDTmplt" class="easyui-textbox" data-options="required:true,validType:['int']"/>
			</td>
			<td class='label'>
			文档E标题：
			</td>
			<td>
				<input id="docETitle" name="docETitle" class="easyui-textbox" data-options="required:true"/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			文档E答案：
			</td>
			<td>
			<input id="docETmplt" name="docETmplt" class="easyui-textbox" data-options="required:true,validType:['int']"/>
			</td>
			<td>&nbsp;</td><td>&nbsp;</td></tr>
	</table>

</form>
</body>
</html>
