<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>修改知识库</title>
	<%@include file="/common/include.jsp" %>
	<style>
		td > span {
			width: 155px !important;
		}
	</style>
<script>
<%-- 查询页点击修改时调用此方法加载记录详情 --%>
function load(id){
	$('#_form').form('load','/${projectName}/manager/repository/vieRepository.action?id='+id);
};

<%-- 点击保存按钮时调用此方法。(注:保存按钮在查询列表页) --%>
function submitForm(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#_form').form('submit',{
      success:function(data) {
      	parent.reloadData();
      }
   });
	//$('#shortCut').val(shortCutDict.getValue());
}
</script>
</head>
<body>
<form id="_form" method="post" action="/${projectName}/manager/repository/updRepository.action" enctype="multipart/form-data">
	<input type="hidden" name='id' id='id'/>
	<table>
		<tr>
			<td class='label' style="width: 150px">
			目录：
			</td>
			<td>
			<dc:insertTree name="shortCut" dictId="ZD_REPOSITORY" style="smart_single"/>
			</td>
			<td class='label' style="width: 150px">
			标题：
			</td>
			<td>
				<input id="repoTitle" name="repoTitle" class="easyui-textbox" data-options=""/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			简要：
			</td>
			<td>
			<input id="repoSynopsis" name="repoSynopsis" class="easyui-textbox" data-options=""/>
			</td>
			<td class='label'>
			正文：
			</td>
			<td>
				<input id="repoText" name="repoText" class="easyui-textbox" data-options=""/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			附件路径：
			</td>
			<td>
			<input  id="attachPath" name="file" class="easyui-filebox" data-options="buttonText:'选择文件',prompt:'选择已有类型',accept:'application/pdf,image/jpeg,application/vnd.ms-excel,image/gif,image/png,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-powerpoint,application/msword,message/rfc822,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.openxmlformats-officedocument.presentationml.presentation'"/>
			</td>
			<td class='label'>
			上传人：
			</td>
			<td>
				<input id="uploadPeople" name="uploadPeople" class="easyui-textbox" data-options=""/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			上传时间：
			</td>
			<td>
			<input id="uploadTimestr" name="uploadTimestr" class="easyui-datebox" data-options=""/>
			</td>
			<td class='label' style="display:none;">
			浏览数：
			</td>
			<td style="display:none;">
				<input id="readCount" name="readCount" class="easyui-textbox" data-options="validType:['int']"/>
			</td>
		</tr>
		<tr style="display:none;">
			<td class='label'>
			修改人：
			</td>
			<td>
			<input id="changePeople" name="changePeople" class="easyui-textbox" data-options=""/>
			</td>
			<td class='label'>
			修改时间：
			</td>
			<td>
				<input id="changeTimestr" name="changeTimestr" class="easyui-datebox" data-options=""/>
			</td>
		</tr>
	</table>

</form>
</body>
</html>
