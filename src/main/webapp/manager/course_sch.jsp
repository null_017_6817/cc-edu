<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>课件查询</title>
	<%@include file="/common/include.jsp" %>

<script>
$(function(){
	<%-- 为分页行设置"添加"和"删除"按钮 --%>
	var pager = $('#courseGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});		
	setTimeout("collapseAll()",0.001);
})
function collapseAll(){
	booktreeDict.expand(1);
}
<%-- 执行查询，点击查询按钮时调用 --%>
function doSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#courseGrid').datagrid('load',{
		courTitle:$('#courTitle').val(),
		uploadPeople:$('#uploadPeople').val(),
		changePeople:$('#changePeople').val(),
		bookId:currBookId
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatOper(val,row,index){

	 return "&emsp;<a href='javascript:void(0)' onclick='updCourse("+val+")' class='_oper'>&nbsp;<i class='icon-edit'></i>&nbsp;</a>"
//     			+"<span onclick='vieCourse("+val+")' style='text-decoration:underline;cursor:pointer;'>查看</span>";
} 

<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
function updCourse(id){
	if(id == '0'){
		$("#courseEditWin").dialog({title: '添加课件',resizable:true});
	}
	if(id != '0'){
		$("#courseEditWin").dialog({title: '修改课件',resizable:true});
	}
	QMoveDialog($("#courseEditWin"));
// 	$('#courseViewWin').dialog('close');
	$('#courseEditWin').dialog('open');
	$('#courseEditWin').get(0).contentWindow.load(id);
} 

<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
// function vieCourse(id){
// 	QMoveDialog($("#courseViewWin"));
// 	$('#courseEditWin').dialog('close');
// 	$('#courseViewWin').dialog('open');
// 	$('#courseViewWin').get(0).contentWindow.load(id);
// }

<%-- 删除记录 --%>
function delCourse(){
	var checkedItems = $('#courseGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/manager/course/delCourse.action",{ids:ids},function(data){
				$('#courseGrid').datagrid('reload');
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#courseEditWin').dialog('close');
	$('#courseGrid').datagrid('reload');
}

var currBookId;
function openBook(json){//{id:'',text:''}
	currBookId=json.id;
	doSearch();
}

<%-- 课件预览 --%>
function courseware(val,row,index){
		return "<a href='javascript:void(0);' onclick=\"vie('"+row.attachPath+"')\" class='_open' title='查看' style='font-size:12px;color:blue;text-decoration:underline;'>课件预览</a>";
}

function vie(it){
	// window.open($("input[name='"+it+"']").val());
	window.open('/${projectName}/manager/viewCourse.jsp?attachPath='+it);
}
</script>
</head>
<body class="easyui-layout">
		<div region="west" split="true" title="课本单元" style="width:260px;">
				<dc:includeTree dictId="CC_BOOK" name="booktree" style="asynch_single" bind="select:openBook" value="1"/>
		</div>
		<div id="content" region="center" style="border:0;">
			<table id="courseGrid" title="课件" fitcolumns=true
				data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#courseGridToolbar',collapsible:true,url:'/${projectName}/manager/course/schCourse.action',method:'get'">
				<thead>
					<tr>
						<th data-options="field:'checkbox',checkbox:true" ></th>
						<th data-options="field:'bookId',width:25,align:'left',sortable:true">目录</th>
						<th data-options="field:'courTitle',width:25,align:'left',sortable:true">课件名称</th>
						<th data-options="field:'attachpath',width:25,align:'center',formatter:courseware">课件</th>
						<!-- <th data-options="field:'readCount',width:25,align:'center',sortable:true">浏览数</th> -->
						<th data-options="field:'type',width:25,align:'center',sortable:true">类型</th>
						<th data-options="field:'uploadPeople',width:25,align:'center',sortable:true">上传人</th>
						<!-- <th data-options="field:'changePeople',width:25,align:'center',sortable:true">修改人</th> -->
						<th data-options="field:'uploadTimestr',width:25,align:'center',sortable:true">上传时间</th>
						<!-- <th data-options="field:'courSynopsis',sortable:true">简要</th>
						<th data-options="field:'courText',sortable:true">正文</th>
						<th data-options="field:'attachPath',sortable:true">PDF附件</th>
						<th data-options="field:'uploadTime',sortable:true">上传时间</th> -->
						<th data-options="field:'_oper',align:'center',formatter:formatOper">操作</th>
					</tr>
				</thead>
			</table>
		</div>
		<%-- 查询条件设定栏 --%>
		<div id="courseGridToolbar">
			<!-- <div class="opline"">
				<a href="javascript:void(0)" onclick="updCourse(0)" class="btn btn-success"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">添加</span></a>&emsp;
				<a href="javascript:void(0)" onclick="delCourse()" class="btn btn-danger"><span class="icon-trash" style="font-size:16px"></span> <span style="font-size:14px">删除</span></a>&emsp;
			</div> -->
			<form class="shline">
					课件名称：<input name='courTitle' id="courTitle" class='easyui-textbox' value='' data-options="iconWidth:16,
				icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
				}]"/>
					上传人：<input name='uploadPeople' id="uploadPeople" class='easyui-textbox' value='' data-options="iconWidth:16,
				icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
				}]"/>
					<!-- 浏览数：<input name='readCount' id="readCount" class='easyui-textbox' value='' data-options="iconWidth:16,
				icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
				}]"/> -->
					<!-- 修改人：<input name='changePeople' id="changePeople" class='easyui-textbox' value='' data-options="iconWidth:16,
				icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
				}]"/> -->
					<a href="javascript:void(0)" onclick="doSearch()" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>&emsp;
						<a href="javascript:void(0)" onclick="updCourse(0)" class="btn btn-success"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">添加</span></a>&emsp;
				<a href="javascript:void(0)" onclick="delCourse()" class="btn btn-danger"><span class="icon-trash" style="font-size:16px"></span> <span style="font-size:14px">删除</span></a>
			</form>
		</div>


<%-- 修改or添加面板 --%>
<iframe id="courseEditWin" src="/${projectName}/manager/course/page.action?page=course_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#courseEditButton'" title="添加课件" style="width:700px;height:500px;padding:5px;top:5000px" scrolling="no" frameborder="0"></iframe>
<div id="courseEditButton">
	<a href="javascript:void(0)" onclick="$('#courseEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#courseEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >取 消</a>
</div>
<%-- 查看面板 --%>
<%-- <iframe id="courseViewWin" src="/${projectName}/manager/course/page.action?page=course_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#courseViewButton'" title="查看课件" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="courseViewButton">
	<a href="javascript:$('#courseViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >关 闭</a>
</div>
</body> --%>
</html>
