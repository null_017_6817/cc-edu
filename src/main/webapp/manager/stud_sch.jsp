<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>学生查询</title>
	<%@include file="/common/include.jsp" %>
	<style>
	.updpass{background-image: url(/${projectName}/imgs/updpass.png);    
		width: 16px;
	    height: 16px;
	    display: inline-block;
	    background-size: 90%;
	    background-repeat: no-repeat;
	    vertical-align: -3px;
    }
	</style>
<script>
$(function(){
	<%-- 为分页行设置"添加"和"删除"按钮 --%>
	var pager = $('#studGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});			
})

<%-- 执行查询，点击查询按钮时调用 --%>
function doSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#studGrid').datagrid('load',{
		number:$('#number').val(),
		name:$('#name').val(),
		cnumber:$('#partnerClass').val(),
		roles:$('#roles').val(),
		agentId:$('#agentId').val(),
		cnumber:cnumberDict.getValue()
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatOper(val,row,index){  
    return "&emsp;<a href='javascript:void(0)' onclick='updStud_upd("+val+")' class='_oper' title='修改'>&nbsp;<i class='icon-edit'></i>&nbsp;</a>"
    +"<a href='javascript:void(0)' onclick='resetPass("+val+")' class='_oper' title='重置密码'>&nbsp;<i class='updpass'></i>&nbsp;</a>&emsp;"; 
			//+"<span onclick='resetPass("+val+")' style='text-decoration:underline;cursor:pointer;'>重置密码</span>";
} 

<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
function updStud_upd(id){
	QMoveDialog($("#studEditWin"));
	$('#studViewWin').dialog('close');
	$('#studEditWin').dialog('open');
	$('#studEditWin').get(0).contentWindow.load(id);
}
//打开添加页面
function updStud(id){
	if(id == '0'){
		$("#studaddWin").dialog({title: '添加学生',resizable:true});
	}
	if(id != '0'){
		$("#studaddWin").dialog({title: '修改学生',resizable:true});
	}
	QMoveDialog($("#studaddWin"));
// 	$('#studViewWin').dialog('close');
	$('#studaddWin').dialog('open');
	$('#studaddWin').get(0).contentWindow.load(id);
} 

<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
// function vieStud(id){
// 	QMoveDialog($("#studViewWin"));
// 	$('#studEditWin').dialog('close');
// 	$('#studViewWin').dialog('open');
// 	$('#studViewWin').get(0).contentWindow.load(id);
// }

<%-- 删除记录 --%>
function delStud(){
	var checkedItems = $('#studGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/manager/stud/delStud.action",{ids:ids},function(data){
				$('#studGrid').datagrid('reload');
			});
		}
	});
}

function clearExamSta(){
	var checkedItems = $('#studGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择要清除的数据？');
		return;
	}
	$.messager.confirm('敬告','确定清除所选学生的考试状态码？',function(r){
		if(r){
			$.get("/${projectName}/manager/stud/clearExamSta.action",{ids:ids},function(data){
				$('#studGrid').datagrid('reload');
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#studEditWin').dialog('close');
	$('#studaddWin').dialog('close');
	//$('#studfile').dialog('close');
	$('#studGrid').datagrid('reload');
}

//重置密码
function resetPass(id){
	var ids = [id];
	$.messager.confirm('提示', '重置后将不能恢复,确认重置密码请选择确定!',
	        function(r) {
	            if (r) {
	                $.post('/${projectName}/manager/stud/resetPwd.action?',{ids:ids},
	                function(result) {
	                    if (result.success) {
	                        $('#studGrid').datagrid('reload');
	                    } else {
	                        $.messager.show({
	                            title: 'Error',
	                            msg: result.errorMsg
	                        });
	                    }
	                },
	                'json');
	                $.messager.show({
											title : "消息",
											msg : "密码已重置,默认密码为111111"
										});
	            }
	        });
	/* var checkedItems = $('#studGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
    var row = $('#studGrid').datagrid('getSelected');
    if (!row && typeof(row)!="undefined" && row!=0){
    			$.messager.alert("提示","请选中一行！！！");
			}else {
       			$.messager.confirm('提示', '重置后将不能恢复,确认重置密码请选择确定!',
        function(r) {
            if (r) {
                $.post('/${projectName}/manager/stud/resetPwd.action?',{ids:ids},
                function(result) {
                    if (result.success) {
                        $('#studGrid').datagrid('reload');
                    } else {
                        $.messager.show({
                            title: 'Error',
                            msg: result.errorMsg
                        });
                    }
                },
                'json');
                $.messager.show({
										title : "消息",
										msg : "密码已重置,默认密码为111111"
									});
            }
        });
    } */
}
//导入
function uploadStud() {
	QMoveDialog($("#studfile"));
	$('#studfile').dialog('open');
	$('#studfile').get(0).contentWindow.load();
}

// 导出模板
function exportTemp(){
	window.open("/${projectName}/manager/stud/exportTemplate.action");
}

</script>
</head>
<body>
<table id="studGrid" title="" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#studGridToolbar',collapsible:true,url:'/${projectName}/manager/stud/schStud.action',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',width:25,align:'center',checkbox:true" ></th>
			<th data-options="field:'number',width:25,align:'center',sortable:true">学号</th>
			<th data-options="field:'name',width:25,align:'center',sortable:true">姓名</th>
			<th data-options="field:'sex',width:25,align:'center',sortable:true">性别</th>
			<!-- <th data-options="field:'agentId',width:25,align:'center',sortable:true">座席号</th> -->
			<th data-options="field:'cnumber',width:25,align:'center',sortable:true">班级</th>
			<th data-options="field:'partnerClass',width:25,align:'center',sortable:true">班级名称</th>
			<!-- <th data-options="field:'info',width:25,align:'center',sortable:true">资料</th> -->
			
			<!-- <th data-options="field:'passwd',sortable:true">密码</th>
			<th data-options="field:'roles',sortable:true">学生角色</th>
			<th data-options="field:'login',sortable:true">登录</th>
			<th data-options="field:'callExpired',sortable:true">通话过期时刻</th>
			<th data-options="field:'partnerType',sortable:true">人员类型(备用)</th>
			<th data-options="field:'partnerId',sortable:true">学号/教师</th>
			<th data-options="field:'partnerAgent',sortable:true">陪练坐席号</th>
			<th data-options="field:'curExercise',sortable:true">当前做题进度</th>
			<th data-options="field:'myExercise',sortable:true">我的试卷</th>
			<th data-options="field:'remark',sortable:true">备注</th> -->

			<th data-options="field:'_oper',align:'center',formatter:formatOper">操作</th>
		</tr>
	</thead>
</table>
<%-- 查询条件设定栏 --%>
<div id="studGridToolbar">
	<div class="opline">
		
		<!-- <a href="javascript:void(0)" onclick="clearExamSta()" class="btn btn-orange"><span class="icon-download-alt" style="font-size:16px"></span> <span style="font-size:14px">清除考试状态</span></a> -->
	</div>
	<form id="_form" class="shline">
	姓名：<input name='name' id="name" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	班级：<dc:insertList name="cnumber" dictId="CC_CLASSES" style="combo_normal" />

	<!-- 座席号：<input name='agentId' id="agentId" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/> -->
	学号：<input name='number' id="number" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	<%-- 班级号：<input name='cnumber' id="cnumber" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	学生角色：<dc:insertList name="roles" dictId="ZD_PARTNER" style="combo_normal" />
	CC座席号：<input name='agentId' id="agentId" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	班级/教研室：<dc:insertList name="partnerClass" dictId="CC_CLASSES" style="combo_normal" /> --%>

	<a href="javascript:void(0)" onclick="doSearch()" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>&emsp;
	<a href="javascript:void(0)" onclick="updStud(0)" class="btn btn-success"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">添加</span></a>&emsp;
		<a href="javascript:void(0)" onclick="delStud()" class="btn btn-danger"><span class="icon-trash" style="font-size:16px"></span> <span style="font-size:14px">删除</span></a>&emsp;
		<a href="javascript:void(0)" onclick="uploadStud(${id})" class="btn btn-success"><span class="icon-cloud-upload" style="font-size:16px"></span> <span style="font-size:14px">批量导入</span></a>&emsp;
		<a href="javascript:void(0)" onclick="exportTemp()" class="btn btn-orange"><span class="icon-download-alt" style="font-size:16px"></span> <span style="font-size:14px">导出模板</span></a>&emsp;
				<!-- <a href="javascript:void(0)" onclick="resetPass()" class="btn btn-orange"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">重置密码</span></a> -->
	</form>
</div>
<%-- 修改面板 --%>
<iframe id="studEditWin" src="/${projectName}/manager/stud/page.action?page=stud_upd_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#studEditButton'" title="修改学生" style="width:600px;height:300px;padding:5px;top:5000px" scrolling="no" frameborder="0"></iframe>
<div id="studEditButton">
	<a href="javascript:void(0)" onclick="$('#studEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#studEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >取 消</a>
</div>
<%-- 添加面板 --%>
<iframe id="studaddWin" src="/${projectName}/manager/stud/page.action?page=stud_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#studaddButton'" title="添加学生" style="width:600px;height:300px;padding:5px;top:5000px" scrolling="no" frameborder="0"></iframe>
<div id="studaddButton">
	<a href="javascript:void(0)" onclick="$('#studaddWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#studaddWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >取 消</a>
</div>
<!-- 查看导出学生账号面板 -->
<iframe id="studfile" src="/${projectName}/manager/stud/page.action?page=stud_file" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#studfileButton'" title="导入" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="studfileButton">
	<a href="javascript:void(0)" onclick="$('#studfile').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">导入</a>
	<a href="javascript:void(0)" onclick="$('#studfile').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >关 闭</a>
</div>
</body>
</html>

