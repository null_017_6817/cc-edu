<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>查看学生</title>
	<%@include file="/common/include.jsp" %>
<script>
function load(id){
	$('#_form').form('load','/${projectName}/manager/stud/vieStud.action?toL=true&id='+id);
};
</script>
</head>
<body>
<form id="_form">
	<input type="hidden" name='id' value=""/>
	<table style="margin: 0 auto;">
		<tr>
			<td class='label'>
			班级：
			</td>
			<td>
			<input type='text' name='cnumber' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			学号：
			</td>
			<td>
			<input type='text' name='number' class='viewbox' readonly='readonly'/>
			</td>
			
		</tr>
		<tr>
			<td class='label'>
			座席号：
			</td>
			<td>
				<input type='text' name='agentId' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			性别：
			</td>
			<td>
				<input type='text' name='sex' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			资料：
			</td>
			<td>
			<input type='text' name='info' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		 <tr>
			<td class='label'>
			密码：
			</td>
			<td>
				<input type="password" name='passwd' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			学生角色：
			</td>
			<td>
			<input type='text' name='roles' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			姓名：
			</td>
			<td>
				<input type='text' name='name' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			登录：
			</td>
			<td>
			<input type='text' name='login' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			通话过期时刻：
			</td>
			<td>
				<input type='text' name='callExpired' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			人员类型(备用)：
			</td>
			<td>
			<input type='text' name='partnerType' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			班级/教研室：
			</td>
			<td>
				<input type='text' name='partnerClass' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			学号/教师：
			</td>
			<td>
			<input type='text' name='partnerId' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			陪练坐席号：
			</td>
			<td>
				<input type='text' name='partnerAgent' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			当前做题进度：
			</td>
			<td>
			<input type='text' name='curExercise' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			我的试卷：
			</td>
			<td>
				<input type='text' name='myExercise' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			备注：
			</td>
			<td>
			<input type='text' name='remark' class='viewbox' readonly='readonly'/>
			</td>
			<td>&nbsp;</td><td>&nbsp;</td></tr>
	</table>

</form>
</body>
</html>
