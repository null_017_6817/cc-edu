<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>知识库查询</title>
	<%@include file="/common/include.jsp" %>
</head>
<body class="easyui-layout">
	<div region="west" split="true" title="知识库" style="width:260px;">
		<dc:includeTree dictId="ZD_REPOSITORY" name="repositorytree" style="synch_single" bind="select:openRepository"/>
	</div>
		<div id="content" region="center" style="border:0;">
		<table id="repositoryGrid" title="知识库" fitcolumns=true
			data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#repositoryGridToolbar',collapsible:true,method:'get'">
			<thead>
				<tr>
					<th data-options="field:'checkbox',checkbox:true" ></th>
					<th data-options="field:'shortCut',width:25,align:'center',sortable:true">目录</th>
					<th data-options="field:'repoTitle',width:25,align:'center',sortable:true">文件名称</th>
					<th data-options="field:'attachPath',width:25,align:'center',formatter:previewOper">预览</th>
					<!-- <th data-options="field:'readCount',width:25,align:'center',sortable:true">浏览数</th> -->
					<th data-options="field:'uploadPeople',width:25,align:'center',sortable:true">上传人</th>
					<th data-options="field:'uploadTime',width:25,align:'center',sortable:true">上传时间</th>
			<!-- 		<th data-options="field:'changePeople',width:25,align:'center',sortable:true">修改人</th>
					<th data-options="field:'changeTime',width:25,align:'center',sortable:true">修改时间</th> -->
					<th data-options="field:'_oper',align:'center',formatter:formatOper">操作</th>
					<!-- <th data-options="field:'attachPath',sortable:true">附件路径</th>
					<th data-options="field:'treetrack',sortable:true"></th>
					<th data-options="field:'repoSynopsis',sortable:true">简要</th>
					<th data-options="field:'repoText',sortable:true">正文</th> -->
				</tr>
			</thead>
		</table>

	</div>
		<%-- 查询条件设定栏 --%>
	<div id="repositoryGridToolbar">
		<div class="opline">
			
		</div>
		<div class="shline">
		目录：<input name='shortCut' id="shortCut" class='easyui-textbox' value='' data-options="iconWidth:16,
	icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
	}]"/>
		文件名称：<input name='repoTitle' id="repoTitle" class='easyui-textbox' value='' data-options="iconWidth:16,
	icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
	}]"/>
		<a href="javascript:void(0)" onclick="doSearch()" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>&emsp;
		<a href="javascript:void(0)" onclick="updRepository(0)" class="btn btn-success"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">上传</span></a>&emsp;
			<a href="javascript:void(0)" onclick="delRepository()" class="btn btn-danger"><span class="icon-trash" style="font-size:16px"></span> <span style="font-size:14px">删除</span></a>
		</div>
	</div>
		<!--简要：<input name='repoSynopsis' id="repoSynopsis" class='easyui-textbox' value='' data-options="iconWidth:16,
	icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
	}]"/>
		附件路径：<input name='attachPath' id="attachPath" class='easyui-textbox' value='' data-options="iconWidth:16,
	icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
	}]"/>
		上传人：<input name='uploadPeople' id="uploadPeople" class='easyui-textbox' value='' data-options="iconWidth:16,
	icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
	}]"/>
		修改人：<input name='changePeople' id="changePeople" class='easyui-textbox' value='' data-options="iconWidth:16,
	icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
	}]"/>-->
	
<script>
$(function(){
	<%-- 为分页行设置"添加"和"删除"按钮 --%>
	var pager = $('#repositoryGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});	
	//注册回车查询
	$('#shortCut').textbox('textbox').keydown(function (e) {
       if (e.keyCode == 13) {
          doSearch();
       }
   });
   window.setTimeout(function() {
	   $("#repositoryGrid").datagrid({url:'/${projectName}/manager/repository/schRepository.action'});
   }, 1);
})

<%-- 执行查询，点击查询按钮时调用 --%>
function doSearch(single){
	var orgs=currRepositoryId;
	var treeOrgs=repositorytreeDict.getValue();
	if(!single&&treeOrgs!=""){
		orgs=treeOrgs;
	}
	$('#repositoryGrid').datagrid('load',{
		shortCut: $('#shortCut').val(),
		repoTitle: $('#repoTitle').val(),
		organids:orgs
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatOper(val,row,index){
	 return "&emsp;<a href='javascript:void(0)' onclick='updRepository("+val+")' class='_oper'>&nbsp;<i class='icon-edit'></i>&nbsp;</a>"; 
} 

function previewOper(val,row,index){
	 return "<a href='javascript:void(0);' onclick=\"preview('"+val+"')\" class='_open' title='查看' style='font-size:12px;color:blue;text-decoration:underline;'>查看</a>";
}

function preview(fileName){
	if(fileName==null || fileName=='' || fileName==undefined || fileName=='null'){
		$.messager.alert('敬告','文件不存在！');
	}else{
		window.open('/${projectName}/js/pdfjs/web/viewer.html?file=/${projectName}/manager/repository/appendUrl/'+fileName+'.action');
	}
}

<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
function updRepository(id){
	QMoveDialog($("#repositoryEditWin"));
 	$('#repositoryViewWin').dialog('close');
	$('#repositoryEditWin').dialog('open');
	$('#repositoryEditWin').get(0).contentWindow.load(id);
} 

<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
function vieRepository(id){
	QMoveDialog($("#repositoryViewWin"));
	$('#repositoryEditWin').dialog('close');
	$('#repositoryViewWin').dialog('open');
	$('#repositoryViewWin').get(0).contentWindow.load(id);
}

<%-- 删除记录 --%>
function delRepository(){
	var checkedItems = $('#repositoryGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/manager/repository/delRepository.action",{ids:ids},function(data){
				$('#repositoryGrid').datagrid('reload');
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#repositoryEditWin').dialog('close');
	$('#repositoryGrid').datagrid('reload');
}
var currRepositoryId;
function openRepository(json){//{id:'',text:''}
	currRepositoryId=json.id;
	doSearch(true);
}
</script>

<%-- 修改or添加面板 --%>
<iframe id="repositoryEditWin" src="/${projectName}/manager/repository/page.action?page=repository_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#repositoryEditButton'" title="修改知识库" style="width:700px;height:500px;padding:5px;top:5000px" scrolling="no" frameborder="0"></iframe>
<div id="repositoryEditButton">
	<a href="javascript:void(0)" onclick="$('#repositoryEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#repositoryEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >取 消</a>
</div>
<%-- 查看面板 --%>
<iframe id="repositoryViewWin" src="/${projectName}/manager/repository/page.action?page=repository_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#repositoryViewButton'" title="查看知识库" style="width:700px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="repositoryViewButton">
	<a href="javascript:$('#repositoryViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >关 闭</a>
</div> 
</body>
</html>
