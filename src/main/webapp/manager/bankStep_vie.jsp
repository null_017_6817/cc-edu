<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>查看答题步骤</title>
	<%@include file="/common/include.jsp" %>
<script>
function load(id){
	$('#_form').form('load','/${projectName}/manager/bankStep/vieBankStep.action?toL=true&id='+id);
};
</script>
</head>
<body>
<form id="_form">
	<input type="hidden" name='id' value=""/>
	<table style="margin: 0 auto;">
		<tr>
			<td class='label'>
			分校编号：
			</td>
			<td>
			<input type='text' name='vccId' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			标准答案：
			</td>
			<td>
			<input type='text' name='standardAnswer' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			题库编号：
			</td>
			<td>
			<input type='text' name='bankId' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			考题编号：
			</td>
			<td>
				<input type='text' name='topicId' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			步骤编号：
			</td>
			<td>
			<input type='text' name='stepId' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			步骤类型：
			</td>
			<td>
				<input type='text' name='type' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			标题：
			</td>
			<td>
			<input type='text' name='title' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			分值：
			</td>
			<td>
				<input type='text' name='score' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			参数：
			</td>
			<td>
			<input type='text' name='param' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			上一步：
			</td>
			<td>
				<input type='text' name='preStepId' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
	</table>

</form>
</body>
</html>
