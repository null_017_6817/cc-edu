<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>题目查询</title>
	<%@include file="/common/include.jsp" %>

<script>
$(function(){
	<%-- 为分页行设置"添加"和"删除"按钮 --%>
	var pager = $('#bankTopicGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});			
})

<%-- 执行查询，点击查询按钮时调用 --%>
function doSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#bankTopicGrid').datagrid('load',{
		title:$('#title').val(),
		type:$('#type').val(),
		level:$('#level').val(),
		profession:$('#profession').val(),
		keyWords:$('#keyWords').val(),
		author:$('#author').val()
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#topicfile').dialog('close');
	$('#bankTopicEditWin').dialog('close');
	$('#bankTopicGrid').datagrid('reload');
}

//上传题目
function uploadTopic() {
	var bankId =  "${bankId}";
	QMoveDialog($("#topicfile"));
	$('#topicfile').dialog('open');
}
</script>
</head>
<body>
<input type="hidden" id="bankId" name="bankId" value="${bankId}">
<table id="bankTopicGrid" title="上传题目" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#bankTopicGridToolbar',collapsible:true,url:'/${projectName}/manager/bankTopic/schBankTopicById.action?bankId=${bankId}',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" ></th>
			<th data-options="field:'bankId',width:25,align:'center',sortable:true">题库编号</th>
			<th data-options="field:'title',width:25,align:'center',sortable:true">标题</th>
			<th data-options="field:'type',width:25,align:'center',sortable:true">类型</th>
			<th data-options="field:'level',width:25,align:'center',sortable:true">级别</th>
			<th data-options="field:'profession',width:25,align:'center',sortable:true">行业</th>
			<th data-options="field:'difficulty',width:25,align:'center',sortable:true">难度</th>
			<th data-options="field:'bookmodul',width:25,align:'center',sortable:true">课程模块</th>
		</tr>
	</thead>
</table>
<%-- 查询条件设定栏 --%>
<div id="bankTopicGridToolbar">
	<div class="opline">
		<a href="javascript:void(0)" onclick="uploadTopic()" class="btn btn-orange"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">上传题目</span></a>&emsp; 
		<a href="javascript:history.back();"  class="btn btn-orange"><span class="icon-undo" style="font-size:16px"></span> <span style="font-size:14px">返回</span></a>
	</div>
	<form id="_form" class="shline">
	行业：<input name='profession' id="profession" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	题目名称：<input name='title' id="title" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	<a href="javascript:void(0)" onclick="doSearch()" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>
	</form>
</div>


	<!-- 类型：<input name='type' id="type" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	关键字：<input name='keyWords' id="keyWords" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	级别：<input name='level' id="level" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	作者：<input name='author' id="author" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/> -->
<%-- 修改or添加面板 --%>
<%-- <iframe id="bankTopicEditWin" src="/${projectName}/manager/bankTopic/page.action?page=bankTopic_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#bankTopicEditButton'" title="修改题目" style="width:700px;height:500px;padding:5px;top:5000px" scrolling="no" frameborder="0"></iframe>
<div id="bankTopicEditButton">
	<a href="javascript:void(0)" onclick="$('#bankTopicEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#bankTopicEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >取 消</a>
</div> --%>
<%-- 查看面板 --%>
<%-- <iframe id="bankTopicViewWin" src="/${projectName}/manager/bankTopic/page.action?page=bankTopic_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#bankTopicViewButton'" title="查看题目" style="width:700px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="bankTopicViewButton">
	<a href="javascript:$('#bankTopicViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >关 闭</a>
</div> --%>
<!-- <div id="toTopic" class="easyui-window" title="上传文件弹出窗口" inline="true"
	data-options="modal:true,iconCls:'icon-save'" style="width:500px;height:250px;"
	closed="true">
	<div style="margin-bottom:20px">
			<input class="easyui-filebox" name="file" data-options="buttonText:'选择文件'" >
		</div>
		<div>
			<a href="#" class="easyui-linkbutton" style="width:100%">Upload</a>
			<a href="#" class="easyui-linkbutton" style="width:100%">Cancle</a>
	</div>
	</div> -->
<!-- 查看上传题目弹框 -->
<iframe id="topicfile" src="/${projectName}/manager/bank/page.action?page=bankTopic_file&bankId=${bankId}" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#studfileButton'" title="导入" style="width:500px;height:200px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="studfileButton">
	<a href="javascript:void(0)" onclick="$('#topicfile').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">上传</a>
	<a href="javascript:void(0)" onclick="$('#topicfile').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >关 闭</a>
</div>
</body>
</html>
