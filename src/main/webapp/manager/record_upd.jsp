<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>修改</title><!--record-->
	<%@include file="/common/include.jsp" %>
<script>
<%-- 查询页点击修改时调用此方法加载记录详情 --%>
function load(id){
	$('#_form').form('load','/${projectName}/manager/record/vieRecord.action?id='+id);
};

<%-- 点击保存按钮时调用此方法。(注:保存按钮在查询列表页) --%>
function submitForm(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#_form').form('submit',{
      success:function(data) {
      	parent.reloadData();
      }
   });
}
</script>
</head>
<body>
<form id="_form" method="post" action="/${projectName}/manager/record/updRecord.action">
	<input type="hidden" name='id' id='id'/>
	<table>
		<tr><td class='label' style="width:120px">任务ID：</td><td><dc:insertList name="taskId" dictId="ZD_EXER_TOPIC" style="combo_normal"/></td></tr>
		<tr><td class='label' >拨打人：</td><td><input id="fromnum" name="fromnum" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >接听人：</td><td><input id="tonum" name="tonum" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >录音名称：</td><td><input id="voice" name="voice" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >状态：</td><td><input id="status" name="status" class="easyui-textbox" /></td></tr>
		<tr><td class='label' >类型：</td><td><input id="type" name="type" class="easyui-textbox" /></td></tr>
		<tr><td class='label' >创建时间：</td><td><input id="createtimestr" name="createtimestr" class="easyui-datebox" data-options="required:true"/></td></tr>
	</table>
</form>
</body>
</html>
