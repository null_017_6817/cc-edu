<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>修改答题步骤</title>
	<%@include file="/common/include.jsp" %>
<script>
<%-- 查询页点击修改时调用此方法加载记录详情 --%>
function load(id){
	$('#_form').form('load','/${projectName}/manager/bankStep/vieBankStep.action?id='+id);
};

<%-- 点击保存按钮时调用此方法。(注:保存按钮在查询列表页) --%>
function submitForm(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#_form').form('submit',{
      success:function(data) {
      	parent.reloadData();
      }
   });
}
</script>
</head>
<body>
<form id="_form" method="post" action="/${projectName}/manager/bankStep/updBankStep.action">
	<input type="hidden" name='id' id='id'/>
	<table>
		<tr>
			<td class='label' style="width: 150px">
			分校编号：
			</td>
			<td>
			<input id="vccId" name="vccId" class="easyui-textbox" data-options="required:true"/>
			</td>
			<td class='label' style="width: 150px">
			标准答案：
			</td>
			<td>
			<input id="standardAnswer" name="standardAnswer" class="easyui-textbox" data-options="required:true"/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			题库编号：
			</td>
			<td>
			<input id="bankId" name="bankId" class="easyui-textbox" data-options="required:true"/>
			</td>
			<td class='label'>
			考题编号：
			</td>
			<td>
				<input id="topicId" name="topicId" class="easyui-textbox" data-options="required:true"/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			步骤编号：
			</td>
			<td>
			<input id="stepId" name="stepId" class="easyui-textbox" data-options="required:true"/>
			</td>
			<td class='label'>
			步骤类型：
			</td>
			<td>
				<input id="type" name="type" class="easyui-textbox" data-options="required:true"/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			标题：
			</td>
			<td>
			<input id="title" name="title" class="easyui-textbox" data-options="required:true"/>
			</td>
			<td class='label'>
			分值：
			</td>
			<td>
				<input id="score" name="score" class="easyui-textbox" data-options="required:true"/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			参数：
			</td>
			<td>
			<input id="param" name="param" class="easyui-textbox" data-options="required:true"/>
			</td>
			<td class='label'>
			上一步：
			</td>
			<td>
				<input id="preStepId" name="preStepId" class="easyui-textbox" data-options="required:true"/>
			</td>
		</tr>
		
			
	</table>

</form>
</body>
</html>
