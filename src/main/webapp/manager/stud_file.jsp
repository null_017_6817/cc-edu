<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>修改学生</title>
	<%@include file="/common/include.jsp" %>
<script>
<%-- 查询页点击修改时调用此方法加载记录详情 --%>
function load(id){
	$("#mes").html("");
	$("#dataFile").filebox("setValue","");
	$('#_form').form('load','/${projectName}/manager/stud/uploadFile.action');
};

<%-- 点击保存按钮时调用此方法。(注:保存按钮在查询列表页) --%>
function submitForm(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#_form').form('submit',{
      success:function(data){
    	$("#mes").html(data);
     	parent.reloadData();
      }
   });
}
<%-- 执行查询，点击查询按钮时调用 --%>
function doSearch(){
	if(!$("#_form").form(" ")){
		return;
	}
	$('#studGrid').datagrid('load',{
		cnumber:$('#cnumber').val(),
	});
}
//重新点击时清空span便签内容
$(function(){
	$('#studGrid').click(function(){
		$("#mes").html("");
	});
});
</script>
<style type="text/css">
	#studGrid{
		padding-top: 50px;
	}
	#spanmes{
		color: red;
		margin-top: 20px;
		padding-left: 50px;
		padding-right: 50px;
	}
</style>
</head>
<body>
<form id="_form" method="post" action="/${projectName}/manager/stud/uploadFile.action" enctype="multipart/form-data">
			<input type="hidden" name='id' value=""/>
 			<div id="studGrid">
 			<table>
 			<tr><td class='label' style="width: 100px;height: 20px"></td><td><dc:insertList name="cnumber" dictId="CC_CLASSES"/>班级号</td></tr>
			<tr><td class='label'></td><td><input name="dataFile" id="dataFile" class="easyui-filebox" data-options="buttonText:'选择文件', accept:'application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' "/>请选择后缀为.xls和.xlsx的文件上传</td></tr>
 			</table>
 			</div>
 			<div id="spanmes">
 			<span id="mes"></span>
			</div>
</form>
</body>
</html>
