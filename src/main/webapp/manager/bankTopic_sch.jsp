<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>题目查询</title>
	<%@include file="/common/include.jsp" %>

<script>
$(function(){
	<%-- 为分页行设置"添加"和"删除"按钮 --%>
	var pager = $('#bankTopicGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});			
})

<%-- 执行查询，点击查询按钮时调用 --%>
function doSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#bankTopicGrid').datagrid('load',{
		title:$('#title').val(),
		type:$('#type').val(),
		level:$('#level').val(),
		profession:$('#profession').val(),
		keyWords:$('#keyWords').val(),
		author:$('#author').val()
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatOper(val,row,index){ 
	 return "&emsp;<a href='javascript:void(0)' onclick=\"updBankTopic('"+val+"')\" class='_oper'>&nbsp;<i class='icon-edit'></i>&nbsp;</a>"
		+"<a href='javascript:void(0)' onclick=\"vieBankTopic('"+val+"')\" class='_oper'>&nbsp;<i class='icon-info-sign'></i>&nbsp;</a>&emsp;"; 
}

<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
function updBankTopic(id){
	QMoveDialog($("#bankTopicEditWin"));
	$('#bankTopicViewWin').dialog('close');
	$('#bankTopicEditWin').dialog('open');
	$('#bankTopicEditWin').get(0).contentWindow.load(id);
} 

<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
function vieBankTopic(id){
	QMoveDialog($("#bankTopicViewWin"));
	$('#bankTopicEditWin').dialog('close');
	$('#bankTopicViewWin').dialog('open');
	$('#bankTopicViewWin').get(0).contentWindow.load(id);
}

<%-- 删除记录 --%>
function delBankTopic(){
	var checkedItems = $('#bankTopicGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.topicId);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/manager/bankTopic/delBankTopic.action",{ids:ids},function(data){
				$('#bankTopicGrid').datagrid('reload');
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#bankTopicEditWin').dialog('close');
	$('#bankTopicGrid').datagrid('reload');
}
</script>
</head>
<body>
<table id="bankTopicGrid" title="题目" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#bankTopicGridToolbar',collapsible:true,url:'/${projectName}/manager/bankTopic/schBankTopic.action',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" ></th>
			<th data-options="field:'bankId',width:25,align:'center',sortable:true">题库编号</th>
			<th data-options="field:'title',width:25,align:'center',sortable:true">标题</th>
			<th data-options="field:'type',width:25,align:'center',sortable:true">类型</th>
			<th data-options="field:'level',width:25,align:'center',sortable:true">级别</th>
			<th data-options="field:'profession',width:25,align:'center',sortable:true">行业</th>
			<th data-options="field:'difficulty',width:25,align:'center',sortable:true">难度</th>
			<th data-options="field:'bookmodul',width:25,align:'center',sortable:true">课程模块</th>
			<!--<th data-options="field:'keyWords',sortable:true">关键字</th>
			<th data-options="field:'author',sortable:true">作者</th>
			<th data-options="field:'totalScore',sortable:true">总分</th>
			<th data-options="field:'capability',sortable:true"></th>
			<th data-options="field:'customer',sortable:true">客户资料</th>
			<th data-options="field:'talkSkill',sortable:true">话术</th>
			<th data-options="field:'vccId',width:80,sortable:true">分校编号</th>
			<th data-options="field:'creattime',sortable:true">时间</th>
			<th data-options="field:'remark',sortable:true">备注</th>
			<th data-options="field:'mergeVoice',sortable:true">合并录音</th>
			<th data-options="field:'passScore',sortable:true">及格分</th>
			<th data-options="field:'beginStepId',sortable:true">第一个有效步骤</th>
			<th data-options="field:'createTime',sortable:true">入库时间</th>
			<th data-options="field:'status',sortable:true">状态</th>
			<th data-options="field:'docATitle',sortable:true">文档A标题</th>
			<th data-options="field:'docATmplt',sortable:true">文档A答案</th>
			<th data-options="field:'docBTitle',sortable:true">文档B标题</th>
			<th data-options="field:'docBTmplt',sortable:true">文档B答案</th>
			<th data-options="field:'docCTitle',sortable:true">文档C标题</th>
			<th data-options="field:'docCTmplt',sortable:true">文档C答案</th>
			<th data-options="field:'docDTitle',sortable:true">文档D标题</th>
			<th data-options="field:'docDTmplt',sortable:true">文档D答案</th>
			<th data-options="field:'docETitle',sortable:true">文档E标题</th>
			<th data-options="field:'docETmplt',sortable:true">文档E答案</th> -->

			<th data-options="field:'_oper',align:'center',formatter:formatOper">操作</th>
		</tr>
	</thead>
</table>
<%-- 查询条件设定栏 --%>
<div id="bankTopicGridToolbar">
	<div class="opline">
		<a href="javascript:void(0)" onclick="updBankTopic(0)" class="btn btn-orange"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">添加</span></a>&emsp;
			<a href="javascript:void(0)" onclick="delBankTopic()" class="btn btn-orange"><span class="icon-trash" style="font-size:16px"></span> <span style="font-size:14px">删除</span></a>&emsp;
	</div>
	<form id="_form" class="shline">
	行业：<input name='profession' id="profession" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	标题：<input name='title' id="title" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	<!-- 类型：<input name='type' id="type" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	关键字：<input name='keyWords' id="keyWords" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	级别：<input name='level' id="level" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	作者：<input name='author' id="author" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/> -->

	<a href="javascript:void(0)" onclick="doSearch()" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>
	</form>
</div>


<%-- 修改or添加面板 --%>
<iframe id="bankTopicEditWin" src="/${projectName}/manager/bankTopic/page.action?page=bankTopic_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#bankTopicEditButton'" title="修改题目" style="width:700px;height:500px;padding:5px;top:5000px" scrolling="no" frameborder="0"></iframe>
<div id="bankTopicEditButton">
	<a href="javascript:void(0)" onclick="$('#bankTopicEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#bankTopicEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >取 消</a>
</div>
<%-- 查看面板 --%>
<iframe id="bankTopicViewWin" src="/${projectName}/manager/bankTopic/page.action?page=bankTopic_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#bankTopicViewButton'" title="查看题目" style="width:700px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="bankTopicViewButton">
	<a href="javascript:$('#bankTopicViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >关 闭</a>
</div>
</body>
</html>
