<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>修改学生</title>
	<%@include file="/common/include.jsp" %>
	<style>
		td > span {
			width: 155px !important;
		}
	</style>
<script>
<%-- 查询页点击修改时调用此方法加载记录详情 --%>
function load(id){
	$('#_form').form('load','/${projectName}/manager/stud/vieStud.action?id='+id);
};

<%-- 点击保存按钮时调用此方法。(注:保存按钮在查询列表页) --%>
function submitForm(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#_form').form('submit',{
      success:function(data) {
      	parent.reloadData();
      }
   });
}
</script>
</head>
<body>
<form id="_form" method="post" action="/${projectName}/manager/stud/updStud_upd.action">
	<input type="hidden" name='id' id='id'/>
	<table>
		<tr>
			<td class='label' style="width: 150px">
			学号：
			</td>
			<td>
			<input id="number" name="number" class="easyui-textbox" data-options="required:true"/>
			</td>
			<td class='label' style="width: 150px">
			姓名：
			</td>
			<td>
				<input id="name" name="name" class="easyui-textbox" data-options="required:true"/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			班级：
			</td>
			<td>
			<dc:insertList name="cnumber" dictId="CC_CLASSES" style="combo_normal" />
			<!-- <input id="cnumber" name="cnumber" class="easyui-textbox" data-options="required:true"/> -->
			</td>
			<td class='label'>
			性别：
			</td>
			<td>
				<dc:insertList name="sex" dictId="ZD_XB" style="combo_normal" />
			</td>
		</tr>
		<tr>
			<!-- <td class='label'>
			<input name="roles" type="hidden" value="10" />
			CC座席号：
			</td>
			<td>
				<input id="agentId" name="agentId" class="easyui-textbox" data-options=""/>
			</td> -->
			<td class='label'>
			备注：
			</td>
			<td colspan="3">
			<!-- <input id="remark" name="remark" class="easyui-textbox" data-options=""/> -->
			<textarea id="remark" name="remark" rows="3" cols="66" class="textarea easyui-validatebox"></textarea>
			</td>
			</tr>
	</table>

</form>
</body>
</html>
