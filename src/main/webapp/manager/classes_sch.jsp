<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>班级查询</title>
	<%@include file="/common/include.jsp" %>

<script>
$(function(){
	<%-- 为分页行设置"添加"和"删除"按钮 --%>
	var pager = $('#classesGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});			
})

<%-- 执行查询，点击查询按钮时调用 --%>
function doSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#classesGrid').datagrid('load',{
		number:$('#number').val(),
		name:$('#name').val()
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatOper(val,row,index){ 
	return "&emsp;<a href='javascript:void(0)' onclick='updClasses("+val+")' class='_oper' title='修改'>&nbsp;<i class='icon-edit'></i></a>&emsp;";
//     			+"<span onclick='vieClasses("+val+")' style='text-decoration:underline;cursor:pointer;'>查看</span>";
} 

<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
function updClasses(id){
	if(id == '0'){
		$("#classesEditWin").dialog({title: '添加班级',resizable:true});
	}
	if(id != '0'){
		$("#classesEditWin").dialog({title: '修改班级',resizable:true});
	}
	QMoveDialog($("#classesEditWin"));
// 	$('#classesViewWin').dialog('close');
	$('#classesEditWin').dialog('open');
	$('#classesEditWin').get(0).contentWindow.load(id);
} 

<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
// function vieClasses(id){
// 	QMoveDialog($("#classesViewWin"));
// 	$('#classesEditWin').dialog('close');
// 	$('#classesViewWin').dialog('open');
// 	$('#classesViewWin').get(0).contentWindow.load(id);
// }

<%-- 删除记录 --%>
function delClasses(){
	var checkedItems = $('#classesGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/manager/classes/delClasses.action",{ids:ids},function(data){
				$('#classesGrid').datagrid('reload');
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#classesEditWin').dialog('close');
	$('#classesGrid').datagrid('reload');
}
</script>
</head>
<body>
<table id="classesGrid" title="班级" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#classesGridToolbar',collapsible:true,url:'/${projectName}/manager/classes/schClasses.action',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" ></th>
			<th data-options="field:'number',width:25,align:'center',sortable:true">班级号</th>
			<th data-options="field:'name',width:25,align:'center',sortable:true">班级名称</th>
			<th data-options="field:'createTime',width:25,align:'center',sortable:true">创建时间</th>
			<!-- <th data-options="field:'remark',width:25,align:'center',sortable:true">备注</th> -->

			<th data-options="field:'_oper',align:'center',formatter:formatOper">操作</th>
		</tr>
	</thead>
</table>
<%-- 查询条件设定栏 --%>
<div id="classesGridToolbar">
	<div class="opline">
		<!-- <a href="javascript:updClasses(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true">添加</a>
		<a href="javascript:delClasses()" class="easyui-linkbutton" iconCls="icon-remove" plain="true">删除</a> -->
		
	</div>
	<form id="_form" class="shline" >
	班级号：<input name='number' id="number" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	班级名称：<input name='name' id="name" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	<a href="javascript:void(0)" onclick="doSearch()" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>&emsp;
	<a href="javascript:void(0)" onclick="updClasses(0)" class="btn btn-success"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">添加</span></a>&emsp;
		<a href="javascript:void(0)" onclick="delClasses()" class="btn btn-danger"><span class="icon-trash" style="font-size:16px"></span> <span style="font-size:14px">删除</span></a>
	</form>
</div>


<%-- 修改or添加面板 --%>
<iframe id="classesEditWin" src="/${projectName}/manager/classes/page.action?page=classes_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#classesEditButton'" title="添加班级" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="no" frameborder="0"></iframe>
<div id="classesEditButton">
	<a href="javascript:void(0)" onclick="$('#classesEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#classesEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >取 消</a>
</div>
<%-- 查看面板 --%>
<%-- <iframe id="classesViewWin" src="/${projectName}/manager/classes/page.action?page=classes_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#classesViewButton'" title="查看班级" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="classesViewButton">
	<a href="javascript:$('#classesViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >关 闭</a>
</div> --%>
</body>
</html>
