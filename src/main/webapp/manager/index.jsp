<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Basic Panel - jQuery EasyUI Demo</title>
	<%@include file="/common/include.jsp" %>
	<script>
	function addTab(title, url){
		if ($('#tabdiv').tabs('exists', title)){
			$('#tabdiv').tabs('select', title);
		} else {
			var content = '<iframe scrolling="no" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
			$('#tabdiv').tabs('add',{
				title:title,
				content:content,
				closable:true
			});
		}
	}
	</script>
	<style>
	.panel-body{overflow:hidden}
	</style>
</head>
<body class="easyui-layout">
	<div data-options="region:'west',split:true" style="width:200px;padding:0px;">
		<div class="easyui-accordion" data-options="fit:true,border:false">
			<div title="Title1"  data-options="selected:true"  style="padding:10px;">
				<a href="javascript:addTab('测试班级','/${projectName}/manager/classes/page.action?page=classes_sch')">测试班级</a><br/>




<a href="javascript:addTab('测试学生','/${projectName}/manager/stud/page.action?page=stud_sch')">测试学生</a><br/>

<a href="javascript:addTab('测试知识库','/${projectName}/manager/repository/page.action?page=repository_sch')">测试知识库</a><br/>

<a href="javascript:addTab('测试课件','/${projectName}/manager/course/page.action?page=course_sch')">测试课件</a><br/>



<a href="javascript:addTab('测试题库','/${projectName}/manager/bank/page.action?page=bank_sch')">测试题库</a><br/>

<a href="javascript:addTab('测试题目','/${projectName}/manager/bankTopic/page.action?page=bankTopic_sch')">测试题目</a><br/>

<a href="javascript:addTab('测试答题步骤','/${projectName}/manager/bankStep/page.action?page=bankStep_sch')">测试答题步骤</a><br/>


<a href="javascript:addTab('测试','/${projectName}/manager/record/page.action?page=record_sch')">测试</a><br/>

<!--insert-->
			</div>
		</div>
	</div>
	<div data-options="region:'center'">
		<div id="tabdiv" class="easyui-tabs" data-options="fit:true,border:false,plain:true" fit="true">
		</div>
	</div>
</body>
</html>
