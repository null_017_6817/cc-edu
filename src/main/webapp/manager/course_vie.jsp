<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>查看课件</title>
	<%@include file="/common/include.jsp" %>
<script>
function load(id){
	$('#_form').form('load','/${projectName}/manager/course/vieCourse.action?toL=true&id='+id);
};
function vie(it){
	//window.open($("input[name='"+it+"']").val());
	window.open('/${projectName}/manager/course/appendUrl.action?filename='+$(it).val());
}
</script>
</head>
<body>
<form id="_form">
	<input type="hidden" name='id' value=""/>
	<table style="margin: 0 auto;">
		<tr>
			<td class='label'>
			目录：
			</td>
			<td>
			<input type='text' name='shortCut' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			标题：
			</td>
			<td>
				<input type='text' name='courTitle' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			简要：
			</td>
			<td>
			<input type='text' name='courSynopsis' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			正文：
			</td>
			<td>
				<input type='text' name='courText' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			PDF附件：
			</td>
			<td>
			<input type='text' name='attachPath' class='viewbox' readonly='readonly' onclick="vie(this);"/>
			</td>
			<td class='label'>
			上传人：
			</td>
			<td>
				<input type='text' name='uploadPeople' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			上传时间：
			</td>
			<td>
			<input type='text' name='uploadTime' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			浏览数：
			</td>
			<td>
				<input type='text' name='readCount' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			修改人：
			</td>
			<td>
			<input type='text' name='changePeople' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			修改时间：
			</td>
			<td>
				<input type='text' name='changeTime' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
	</table>

</form>
</body>
</html>
