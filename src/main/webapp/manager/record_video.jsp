<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="X-UA-Compatible" content="IE=8" />
	<title>查询</title><!--record-->
	<%@include file="/common/include.jsp" %>
	<link rel="stylesheet" type="text/css"	href="/${projectName}/book/css/style.css" />
	<style>
		#info .ly_p{
			display:inline-block;
			vertical-align:middle;
			width:49%;
			line-height:24px;
		}
		#vdeioTable tr th{
			line-height:26px;
			text-align:center;
			border:1px solid #ccc;
		}
		#vdeioTable tr td{
			border:1px solid #ddd;
			text-align:center;
		}
		#vdeioTable tr td:last-child{
			position:relative;
		}
		#vdeioTable tr td:last-child a{
			position:absolute;
			top:50%;
			margin-top:-10px;
			right:0;
			font-size:20px;
		}
	</style>
<script>

//显示录音列表
function load(record){
	//转换为空的值为0
	if(record.taskId =='null'){
		record.taskId=0
	}
	
	$("#vdeioTable").find("tbody").html("");
	$.ajax({
		url : "/${projectName}/manager/record/getRecords.action" , 
		type : "post" , 
		data : {"cTime":record.cTime,"taskName":record.taskName,"taskId":record.taskId,"fromNum":record.fromNum,"toNum":record.toNum,"typeName":record.typeName} , 
		success : function(result) {
			if(result.success){
				var data = result.data;
				var infodata = result.infodata;
				//console.log(data);
				console.log(infodata);
				var taskName = "";
				var typeName = "";
				var fromNum = "";
				var toNum = "";
				
				
				if(infodata.taskName != null && typeof(infodata.taskName) != 'undefined'){
					taskName = infodata.taskName;
				}
				
				if(infodata.typeName != null && typeof(infodata.typeName) != 'undefined'){
					typeName = infodata.typeName;
				}
				
				if(infodata.fromNum != null && typeof(infodata.fromNum) != 'undefined'){
					fromNum = infodata.fromNum;
				}
				
				if(infodata.toNum != null && typeof(infodata.toNum) != 'undefined'){
					toNum = infodata.toNum;
				}
				
				var infoHtml = "<p class='ly_p'>任务名称："+taskName+"</p><p class='ly_p'>录音类型："+typeName+"</p><p class='ly_p'>拨打人："+fromNum+"</p><p class='ly_p'>接听人："+toNum+"</p>";
				$("#info").html(infoHtml);
				
				if(data != null){
					var trHtml = "";
					for(var i = 0; i < data.length; i++){
						var tr = "<tr>" 
									+ "<td>"+(i+1)+"</td>"
									+ "<td>"+data[i].voice+"</td>"
									+ "<td>"+data[i].createTime+"</td>"
									+ "<td style='border: 1px #ccc solid;'><audio controls='controls' style='width:90%;''><source src='"+data[i].voicePath+"' type='audio/mpeg'/></audio>"
									+ "&emsp;<a href='javascript:void(0)' onclick=\"downVideos('"+data[i].voice+"')\" class='_oper' title='下载录音'>&nbsp;<i class='icon-download'></i>&nbsp;</a>&emsp;</td>"
								+ "</tr>";
								
								trHtml += tr;
						
					}
					$("#vdeioTable").find("tbody").html(trHtml);
				}
			}
		}
	});
}
//关闭录音列表
function closeVedioDiv(){
	$("#fade").hide();
	$("#vdeioTable").find("tbody").find("audio").each(function(){
		$(this).remove();
	})
	$("#vdeioDiv").hide();
}

function stop(){
	$("#vdeioTable").find('audio').remove();
}

//下载录音文件
function downVideos(fileName){
	window.open("/${projectName}/manager/record/downVoiceFile.action?fileName="+fileName);
}
</script>
</head>
<body>
	<div id="vdeioDiv">
		<div>
			<div id="info"> 
			</div>
			<div class="ly_main" style="width:100%;">
				<table id="vdeioTable" style="text-align:center;width:100%;margin: 2% auto 0;" fitcolumns=true>
					<thead>
						<tr>
							<th style='width:10%;'>序号</th>
							<th style='width:20%;'>录音名称</th>
							<th style='width:20%;'>创建时间</th>
							<th style='width:50%;'>操作</th>
						</tr> 
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
	</div>
</body>
</html>
