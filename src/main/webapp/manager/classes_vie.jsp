<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>查看班级</title>
	<%@include file="/common/include.jsp" %>
<script>
function load(id){
	$('#_form').form('load','/${projectName}/manager/classes/vieClasses.action?toL=true&id='+id);
};
</script>
</head>
<body>
<form id="_form">
	<input type="hidden" name='id' value=""/>
	<table style="margin: 0 auto;">
		<tr><td class='label'>班级号：</td><td><input type='text' name='number' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>班级名称：</td><td><input type='text' name='name' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>创建时间：</td><td><input type='text' name='createTime' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>备注：</td><td><input type='text' name='remark' class='viewbox' readonly='readonly'/></td></tr>
	</table>

</form>
</body>
</html>
