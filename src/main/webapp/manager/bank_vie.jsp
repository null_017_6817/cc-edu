<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>查看题库</title>
	<%@include file="/common/include.jsp" %>
<script>
function load(id){
	$('#_form').form('load','/${projectName}/manager/bank/vieBank.action?toL=true&bankId='+id);
};
</script>
</head>
<body>
<form id="_form">
	<input type="hidden" name='bankId' value=""/>
	<table style="margin: 0 auto;">
		<tr><td class='label'>分校编号：</td><td><input type='text' name='vccId' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>题库名：</td><td><input type='text' name='bankName' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>级别：</td><td><input type='text' name='level' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>行业：</td><td><input type='text' name='profession' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>备注：</td><td><input type='text' name='remark' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>创建时间：</td><td><input type='text' name='createTime' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>状态：</td><td><input type='text' name='status' class='viewbox' readonly='readonly'/></td></tr>
	</table>

</form>
</body>
</html>
