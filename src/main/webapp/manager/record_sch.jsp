<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="X-UA-Compatible" content="IE=8" />
	<title>查询</title><!--record-->
	<%@include file="/common/include.jsp" %>
	<link rel="stylesheet" type="text/css"	href="/${projectName}/book/css/style.css" />

<script>
$(function(){
	var pager = $('#recordGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});			
})

<%-- <%-- 执行查询，点击查询按钮时调用 --%> 
function doRecordSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	
	$('#recordGrid').datagrid('load',{
		taskName:$('#taskName').val(),
		fromNum:$('#fromnum').val(),
		toNum:$('#tonum').val(),
		voice:$('#voice').val(),
		status:$('#status').val(),
		type:$('#type').val()
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatRecordOper(val,row,index){  
	console.log(row);
	return "&emsp;<a href='javascript:void(0)' onclick=\"showVideos('"+row.taskId+"','"+row.taskName+"','"+row.fromNum+"','"+row.toNum+"','"+row.cTimestr+"','"+row.typeName+"')\" class='_oper' title='播放录音'>&nbsp;<i class='icon-play'></i>&nbsp;</a>&nbsp;"
			+ "&emsp;<a href='javascript:void(0)' onclick=\"downVideos('"+row.voice+"')\" class='_oper' title='下载录音'>&nbsp;<i class='icon-download'></i>&nbsp;</a>&emsp;";
}

// 下载录音文件
function downVideos(fileName){
	window.open("/${projectName}/manager/record/downVoiceFile.action?fileName="+fileName);
}

 <%-- 打开修改页面，点击记录行的"修改"链接时调用 --%> 
function updRecord(id){
	QMoveDialog($("#recordEditWin"));
	$('#recordViewWin').dialog('close');
	$('#recordEditWin').dialog('open');
	$('#recordEditWin').get(0).contentWindow.load(id);
} 

 <%-- 打开查看页面，点击记录行的"查看"链接时调用 --%> 
function vieRecord(id){
	QMoveDialog($("#recordViewWin"));
	$('#recordEditWin').dialog('close');
	$('#recordViewWin').dialog('open');
	$('#recordViewWin').get(0).contentWindow.load(id);
}

 <%-- 删除记录 --%> 
function delRecord(){
	var checkedItems = $('#recordGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/manager/record/delRecord.action",{ids:ids},function(data){
				$('#recordGrid').datagrid('reload');
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%> 
function reloadData(){
	$('#recordEditWin').dialog('close');
	$('#recordGrid').datagrid('reload');
}

//显示录音列表
function showVideos(taskId,taskName,fromNum,toNum,cTime,typeName){
	//alert(typeName);
	var record ={taskId:taskId,
			fromNum:fromNum,
			toNum:toNum,
			cTime:cTime,
			taskName:taskName,
			typeName:typeName
	}
	QMoveDialog($("#recordVideo"));
	$('#recordVideo').dialog('close');
	$('#recordVideo').dialog('open');
	$('#recordVideo').get(0).contentWindow.load(record);
}

//关闭录音列表
function closeVedioDiv(){
	$("#fade").hide();
	$("#vdeioTable").find("tbody").find("audio").each(function(){
		$(this).remove();
	})
	$("#vdeioDiv").hide();
}
function closeVid(){
	//$('#recordVideo').get(0).contentWindow.stop();
	$('#recordVideo').dialog('close');
}

function closeVideo(){
	$('#recordVideo').get(0).contentWindow.stop();
}

</script>
</head>
<body>
<table id="recordGrid" title="" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#recordGridToolbar',collapsible:true,url:'/${projectName}/manager/record/schRecord.action',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" ></th>
			<!-- <th data-options="field:'id',width:80,sortable:true">id</th> -->
			<th data-options="field:'taskName',width:80,sortable:true">任务</th>
			<th data-options="field:'fromNum',sortable:true">拨打人学号</th>
			<th data-options="field:'toNum',sortable:true">接听人学号</th>
			<th data-options="field:'fromAgent',sortable:true">拨打人分机号</th>
			<th data-options="field:'toAgent',sortable:true">接听人分机号</th>
			
<!-- 			<th data-options="field:'voice',sortable:true">录音名称</th> -->
			<!-- <th data-options="field:'status',sortable:true">状态</th>
			<th data-options="field:'type',sortable:true">类型</th> -->
			<th data-options="field:'cTimestr',sortable:true">创建时间</th>
			<th data-options="field:'_oper',align:'center',formatter:formatRecordOper">操作</th>
		</tr>
	</thead>
</table>
<%-- 查询条件设定栏 --%>
 <div id="recordGridToolbar">
 	<div class="opline">
 		<a href="javascript:delRecord()" class="btn btn-danger"><span class="icon-trash" style="font-size:16px"></span> <span style="font-size:14px">删除</span></a>
 	</div>
 	<form id="_form" class="shline">
 	任务：<input name='taskName' id="taskName" class='easyui-textbox' value='' data-options="iconWidth:16,
 icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
 }]"/>
 	拨打人：<input name='fromnum' id="fromnum" class='easyui-textbox' value='' data-options="iconWidth:16,
 icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
 }]"/>
 	接听人：<input name='tonum' id="tonum" class='easyui-textbox' value='' data-options="iconWidth:16,
 icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
 }]"/>
<!--  	录音名称：<input name='voice' id="voice" class='easyui-textbox' value='' data-options="iconWidth:16,
 icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
 }]"/> -->
 <!-- 	状态：<input name='status' id="status" class='easyui-textbox' value='' data-options="iconWidth:16,
 icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
 }]"/>
 	类型：<input name='type' id="type" class='easyui-textbox' value='' data-options="iconWidth:16,
 icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
 }]"/> -->
	<a href="javascript:void(0)" onclick="doRecordSearch()" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>
	</form>
	</div>
	<%-- 录音列表 --%>
	<iframe id="recordVideo" src="/${projectName}/manager/record/page.action?page=record_video" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#recordViewButton',onBeforeClose:closeVideo" title="播放录音" style="width:700px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
	
	<div id="recordViewButton">
		<a href="javascript:closeVid();" class="easyui-linkbutton" iconCls="icon-remove" >关 闭</a>
	</div>
	</div>	
	
	<div id="fade" class="black_overlay"></div>
	<div id="vdeioDiv" class="white_content" style="left:45%;width:400px;height:300px;">
		<h5>通话列表</h5>
		<a href="javascript:void(0)" onclick="closeVedioDiv();" style="float:right;margin-top:0px;" ><font size="5">×</font></a>
		<hr class="hr-dashed">
		<!-- <div>
			<table id="vdeioTable" style="text-align:center;width:20%;">
				<thead>
					<tr style='border: 1px #ccc solid;'>
						<th style='border: 1px #ccc solid;'>录音</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div> -->
	</div>
</body>
</html>
