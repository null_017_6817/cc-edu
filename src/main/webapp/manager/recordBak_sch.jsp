<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>查询</title><!--record-->
	<%@include file="/common/include.jsp" %>

<script>
$(function(){
	var pager = $('#recordGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});			
})

<%-- 执行查询，点击查询按钮时调用 --%>
function doRecordSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#recordGrid').datagrid('load',{
		fromnum:$('#fromnum').val(),
		tonum:$('#tonum').val(),
		voice:$('#voice').val(),
		status:$('#status').val(),
		type:$('#type').val()
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
// function formatRecordOper(val,row,index){  
//     return "&emsp;<a href='javascript:void(0)' onclick='updRecord("+val+")' class='_oper' title='修改'>&nbsp;<i class='icon-edit'></i>&nbsp;</a>"
//     			+"<a href='javascript:void(0)' onclick='vieRecord("+val+")' class='_oper' title='查看'>&nbsp;<i class='icon-info-sign'></i>&nbsp;</a>&emsp;"; 
// } 

<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
function updRecord(id){
	QMoveDialog($("#recordEditWin"));
	$('#recordViewWin').dialog('close');
	$('#recordEditWin').dialog('open');
	$('#recordEditWin').get(0).contentWindow.load(id);
} 

<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
function vieRecord(id){
	QMoveDialog($("#recordViewWin"));
	$('#recordEditWin').dialog('close');
	$('#recordViewWin').dialog('open');
	$('#recordViewWin').get(0).contentWindow.load(id);
}

<%-- 删除记录 --%>
function delRecord(){
	var checkedItems = $('#recordGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/manager/record/delRecord.action",{ids:ids},function(data){
				$('#recordGrid').datagrid('reload');
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#recordEditWin').dialog('close');
	$('#recordGrid').datagrid('reload');
}
</script>
</head>
<body>
<table id="recordGrid" title="" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#recordGridToolbar',collapsible:true,url:'/${projectName}/manager/record/schRecord.action',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" ></th>
			<th data-options="field:'id',width:80,sortable:true">id</th>
			<th data-options="field:'taskid',sortable:true">任务ID</th>
			<th data-options="field:'fromnum',sortable:true">拨打人</th>
			<th data-options="field:'tonum',sortable:true">接听人</th>
			<th data-options="field:'voice',sortable:true">录音名称</th>
<!-- 			<th data-options="field:'voice',align:'center',width:25,formatter:formatPlay">录音名称</th> -->
			<th data-options="field:'status',sortable:true">状态</th>
			<th data-options="field:'type',sortable:true">类型</th>
			<th data-options="field:'createtime',sortable:true">创建时间</th>

<!-- 			<th data-options="field:'_oper',align:'center',formatter:formatRecordOper">操作</th> -->
		</tr>
	</thead>
</table>
<%-- 查询条件设定栏 --%>
<div id="recordGridToolbar">
	<div class="opline">
<!-- 		<div id="audioId" style="display:none;"> -->
<%-- 			<audio controls><source src="/${projectName}/soundrecording/ivr_voice-3004651-1479102953145455.wav" type="audio/mpeg"/></audio> --%>
<!-- 		</div> -->
<!-- 		<a href="javascript:updRecord(0)" class="btn btn-success"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">添加</span></a>&emsp; -->
		<a href="javascript:delRecord()" class="btn btn-danger"><span class="icon-trash" style="font-size:16px"></span> <span style="font-size:14px">删除</span></a>
	</div>
	<form id="_form" class="shline">
	拨打人：<input name='fromnum' id="fromnum" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	接听人：<input name='tonum' id="tonum" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	录音名称：<input name='voice' id="voice" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	状态：<input name='status' id="status" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	类型：<input name='type' id="type" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>

	<a href="javascript:void(0)" onclick="doRecordSearch()" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>
	</form>
</div>


<%-- 修改or添加面板 --%>
<%-- <iframe id="recordEditWin" src="/${projectName}/manager/record/page.action?page=record_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#recordEditButton'" title="修改" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe> --%>
<!-- <div id="recordEditButton"> -->
<!-- 	<a href="javascript:void(0)" onclick="$('#recordEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a> -->
<!-- 	<a href="javascript:void(0)" onclick="$('#recordEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >取 消</a> -->
<!-- </div> -->
<%-- 查看面板 --%>
<%-- <iframe id="recordViewWin" src="/${projectName}/manager/record/page.action?page=record_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#recordViewButton'" title="查看" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe> --%>
<!-- <div id="recordViewButton"> -->
<!-- 	<a href="javascript:$('#recordViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >关 闭</a> -->
<!-- </div> -->
</body>
</html>
