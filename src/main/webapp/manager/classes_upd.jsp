<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>修改班级</title>
	<%@include file="/common/include.jsp" %>
<script>
<%-- 查询页点击修改时调用此方法加载记录详情 --%>
function load(id){
	$('#_form').form('load','/${projectName}/manager/classes/vieClasses.action?id='+id);
};

<%-- 点击保存按钮时调用此方法。(注:保存按钮在查询列表页) --%>
function submitForm(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#_form').form('submit',{
      success:function(data) {
      	parent.reloadData();
      }
   });
}
</script>
</head>
<body>
<form id="_form" method="post" action="/${projectName}/manager/classes/updClasses.action">
	<input type="hidden" name='id' id='id'/>
	<table>
		<tr><td class='label' style="width: 168px">班级号：</td><td><input id="number" name="number" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label'>班级名称：</td><td><input id="name" name="name" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label'>创建时间：</td><td><input id="createTimestr" name="createTimestr" class="easyui-datebox" data-options="required:true"/></td></tr>
		<tr><td class='label'>备注：</td><td><textarea id="remark" name="remark" rows=6 cols="35" class="textarea easyui-validatebox"></textarea></td></tr>
	</table>

</form>
</body>
</html>
