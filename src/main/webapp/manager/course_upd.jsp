<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>修改课件</title>
	<%@include file="/common/include.jsp" %>
	<style>
		td > span {
			width: 155px !important;
		}
	</style>
<script>
<%-- 查询页点击修改时调用此方法加载记录详情 --%>
function load(id){
	$('#_form').form('load','/${projectName}/manager/course/vieCourse.action?id='+id);
	if(id == 0){
		$("#updTr").hide("speed");
	}else{
		$("#updTr").show("speed");
	}
};

<%-- 点击保存按钮时调用此方法。(注:保存按钮在查询列表页) --%>
function submitForm(){
	if(!$("#_form").form("validate")){
		return;
	}
	var bo = bookIdDict.getValue();
	if("" == bo || null == bo || typeof(bo) == "undefined"){
		$.messager.alert("温馨提示", "请选择目录");
		return;
	}
	var typeVal = typeDict.getValue();
	console.log(typeVal);
	var file = $("#attachPath").filebox("getValue");
	console.log(file);
	if("" == file || null == file || typeof(file) == "undefined") {
		if("1" == typeVal) {
			$.messager.alert("温馨提示", "请上传swf文件");
			return;
		}
		if("2" == typeVal) {
			$.messager.alert("温馨提示", "请上传pdf或swf文件");
			return;
		}
	}
	//$('#shortCut').val(bookTreeDict.getValue());
	$('#_form').form('submit',{
      success:function(data) {
      	parent.reloadData();
      }
   });
}
</script>
</head>
<body>
<form id="_form" method="post" action="/${projectName}/manager/course/updCourse.action" enctype="multipart/form-data">
	<input type="hidden" name='id' id='id'/>
	<table>
		<tr>
			<td class='label' style="width: 150px">
			目录：
			</td>
			<td>
			<dc:insertTree name="bookId" dictId="CC_BOOK" style="synch_single"/>
			</td>
			<td class='label' style="width: 150px">
			课件名称：
			</td>
			<td>
				<input id="courTitle" name="courTitle" class="easyui-textbox" data-options=""/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			附件：
			</td>
			<td>
			<input id="attachPath" name="file" class="easyui-filebox" data-options="buttonText:'选择文件'"/>
			</td>
			<td class='label'>
			上传人：
			</td>
			<td>
				<input id="uploadPeople" name="uploadPeople" class="easyui-textbox" data-options=""/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			上传时间：
			</td>
			<td>
			<input id="uploadTimestr" name="uploadTimestr" class="easyui-datebox" data-options=""/>
			</td>
			<!-- <td class='label'>
			浏览数：
			</td>
			<td>
				<input id="readCount" name="readCount" class="easyui-textbox" data-options="required:true,validType:['int']"/>
			</td> -->
			<td class='label'>
			类型：
			</td>
			<td>
				<dc:insertList name="type" dictId="ZD_COURSETYPE" style="enu_radio" />
			</td>
		</tr>
		<!-- <tr id="updTr">
			<td class='label'>
			修改人：
			</td>
			<td>
			<input id="changePeople" name="changePeople" class="easyui-textbox" data-options=""/>
			</td>
			<td class='label'>
			修改时间：
			</td>
			<td>
				<input id="changeTimestr" name="changeTimestr" class="easyui-datebox" data-options=""/>
			</td>
		</tr> -->
		<tr>
			<td class='label'>
			简要：
			</td>
			<td colspan="3">
			<!-- <input id="courSynopsis" name="courSynopsis" class="easyui-textbox" data-options=""/> -->
			<textarea id="courSynopsis" name="courSynopsis" rows="3" cols="66" class="textarea easyui-validatebox"></textarea>
			</td>
		</tr>
		<tr>
			<td class='label'>
			正文：
			</td>
			<td colspan="3">
				<!-- <input id="courText" name="courText" class="easyui-textbox" data-options=""/> -->
				<textarea id="courText" name="courText" rows="3" cols="66" class="textarea easyui-validatebox"></textarea>
			</td>
		</tr>
	</table>

</form>
</body>
</html>
