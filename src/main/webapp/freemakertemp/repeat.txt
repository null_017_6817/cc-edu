<div class="question">
	<h3><#if step.title?exists>${step.title}</#if>[复读陈述题]</h3>
	<div class="question-Q">
	<!-- begin步骤模板套用内容 -->
		<#if step.prompt.context?exists>
			<span>${step.prompt.context}</span>
		</#if>
		<#if step.prompt.audio?exists>
			<audio controls="controls">
				<source src="${step.prompt.audio}" type="audio/mpeg"/>
			</audio>
		</#if>
	</div>
	<div class="question-A">
		<div class="operateBtn audioBtn">
			<button type="button" title="录音" onclick="startRecording(this)"><i class="icon-record"></i></button> 
			<button type="button" title="暂停" onclick="stopRecording(this)"><i class="icon-stop"></i></button> 
			<button type="button" title="播放" onclick="playRecording(this)"><i class="icon-play"></i></button> 
		</div>
		<audio controls id="record" name="templateanswer"><source src="" type="audio/mpeg"/></audio>
		<div class="operateBtn audioBtn">
			<button class="upload-audio" type="button" title="上传" onclick="uploadAudio(this)"><i class="icon-sub"></i></button>
		</div>
	</div>
</div>	
	<input type="hidden" name="type" value="repeat"/>
                                                                                                                                                                                                                                                                                                                                                                                                                                    