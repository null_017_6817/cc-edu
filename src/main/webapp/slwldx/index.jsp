<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title></title>
	<%@include file="/common/include.jsp" %>
	<script>
	function addTab(title, url){
		if ($('#tabdiv').tabs('exists', title)){
			$('#tabdiv').tabs('select', title);
		} else {
			var content = '<iframe scrolling="no" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
			$('#tabdiv').tabs('add',{
				title:title,
				content:content,
				closable:true
			});
		}
	}
	</script>
	<style>
	.panel-body{overflow:hidden}
	</style>
</head>
<body class="easyui-layout">
	<div data-options="region:'west',split:true" style="width:200px;padding:0px;">
		<div class="easyui-accordion" data-options="fit:true,border:false">
			<div title="Title1"  data-options="selected:true"  style="padding:10px;">
				<a href="javascript:addTab('测试航班信息','/${projectName}/slwldx/hbxx_sch.jsp')">测试航班信息</a><br/>

				<a href="javascript:addTab('测试机票订单信息','/${projectName}/slwldx/jpddxx/page.action?page=jpddxx_sch')">测试机票订单信息</a><br/>

<a href="javascript:addTab('测试物流资费信息','/${projectName}/slwldx/wlzfxx/page.action?page=wlzfxx_sch')">测试物流资费信息</a><br/>

<a href="javascript:addTab('测试物流订单信息','/${projectName}/slwldx/wlddxx/page.action?page=wlddxx_sch')">测试物流订单信息</a><br/>

<a href="javascript:addTab('测试物流流转信息','/${projectName}/slwldx/wllzxx/page.action?page=wllzxx_sch')">测试物流流转信息</a><br/>

<a href="javascript:addTab('测试套餐信息','/${projectName}/slwldx/tcxx/page.action?page=tcxx_sch')">测试套餐信息</a><br/>

<a href="javascript:addTab('测试个人套餐用量信息','/${projectName}/slwldx/grtcylxx/page.action?page=grtcylxx_sch')">测试个人套餐用量信息</a><br/>













<!--insert-->
			</div>
		</div>
	</div>
	<div data-options="region:'center'">
		<div id="tabdiv" class="easyui-tabs" data-options="fit:true,border:false,plain:true" fit="true">
		</div>
	</div>
</body>
</html>
