<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>修改物流订单信息</title><!--wlddxx-->
	<%@include file="/common/include.jsp" %>
<script>
<%-- 查询页点击修改时调用此方法加载记录详情 --%>
function load(id){
	$('#_form').form('load','/${projectName}/slwldx/wlddxx/vieWlddxx.action?id='+id);
};

<%-- 点击保存按钮时调用此方法。(注:保存按钮在查询列表页) --%>
function submitForm(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#_form').form('submit',{
      success:function(data) {
      	parent.reloadData();
      }
   });
}
</script>
</head>
<body>
<form id="_form" method="post" action="/${projectName}/slwldx/wlddxx/updWlddxx.action">
	<input type="hidden" name='id' id='id'/>
	<table>
		<tr><td class='label' style="width:120px">物流资费：</td>
		<td>
		<dc:insertList name="wlzfid" dictId="CCQ_WLZF" style="combo_grid:{WLGS:物流公司,SFCS:始发城市,MDCS:目的城市,JG:价格(元)}" cssWidth="300px" />
		</td>
		</tr>
		<tr><td class='label' >订单号：</td><td><input id="ddh" name="ddh" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >订单价格(元)：</td><td><input id="ddjg" name="ddjg" class="easyui-textbox" data-options="required:true,validType:['int']"/></td></tr>
		<tr><td class='label' >寄件人：</td><td><input id="jjr" name="jjr" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >收件人：</td><td><input id="sjr" name="sjr" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >寄件人电话：</td><td><input id="jjrdh" name="jjrdh" class="easyui-textbox" data-options="required:true,validType:['tel']"/></td></tr>
		<tr><td class='label' >收件人电话：</td><td><input id="sjrdh" name="sjrdh" class="easyui-textbox" data-options="required:true,validType:['tel']"/></td></tr>
		<tr><td class='label' >始发城市：</td><td><dc:insertTree name="sfcs" dictId="CCQ_QQCS" style="asynch_single" /></td></tr>
		<tr><td class='label' >目的城市：</td><td><dc:insertTree name="mdcs" dictId="CCQ_QQCS" style="asynch_single" /></td></tr>
		<tr><td class='label' >始发城市详细地址：</td><td><input id="sfcsxxdz" name="sfcsxxdz" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >目的城市详细地址：</td><td><input id="mdcsxxdz" name="mdcsxxdz" class="easyui-textbox" data-options="required:true"/></td></tr>
		<!-- <tr><td class='label' >寄件时间：</td><td><input id="jjsj" name="jjsj" class="easyui-textbox" data-options="required:true"/></td></tr> -->
		<tr><td class='label' >收件时间：</td><td><input id="sjsj" name="sjsj" class="easyui-datetimebox"/></td></tr>
		<tr><td class='label' >订单状态：</td><td><dc:insertList name="ddzt" dictId="CCQ_WLDDZT" style="combo_normal" /></td></tr>
	</table>

</form>
</body>
</html>
