<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>查看机票订单信息</title><!--jpddxx-->
	<%@include file="/common/include.jsp" %>
<script>
function load(id){
	$('#_form').form('load','/${projectName}/slwldx/jpddxx/vieJpddxx.action?toL=true&id='+id);
};
</script>
</head>
<body>
<form id="_form">
	<input type="hidden" name='id' value=""/>
	<table>
		<tr><td class='label'>订单号：</td><td><input type='text' name='ddh' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>航班号：</td><td><input type='text' name='hbh' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>航班日期：</td><td><input type='text' name='hbrq' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>订票人姓名：</td><td><input type='text' name='dprxm' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>订票人性别：</td><td><input type='text' name='dprxb' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>订票时间：</td><td><input type='text' name='dpsj' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>身份证号：</td><td><input type='text' name='sfzh' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>电话：</td><td><input type='text' name='dh' class='viewbox' readonly='readonly'/></td></tr>
		<!-- <tr><td class='label'>订单状态：</td><td><input type='text' name='ddzt' class='viewbox' readonly='readonly'/></td></tr> -->
	</table>

</form>
</body>
</html>
