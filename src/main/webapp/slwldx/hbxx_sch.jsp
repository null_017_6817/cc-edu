<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>航班信息查询</title><!--hbxx-->
	<%@include file="/common/include.jsp" %>

<script>
$(function(){
	var pager = $('#hbxxGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});			
})

<%-- 执行查询，点击查询按钮时调用 --%>
function doSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#hbxxGrid').datagrid('load',{
		rq:$("input[name='rq']").val(),
		cfcs:cfcsDict.getValue(),
		ddcs:ddcsDict.getValue()
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatOper(val,row,index){  
    return "&emsp;<a href='javascript:void(0)' onclick='updHbxx("+val+")' class='_oper' title='修改'>&nbsp;<i class='icon-edit'></i>&nbsp;</a>"
//     			+"<a href='javascript:void(0)' onclick='vieHbxx("+val+")' class='_oper' title='查看'>&nbsp;<i class='icon-info-sign'></i>&nbsp;</a>&emsp;" 
    			+"<a href='javascript:void(0)' onclick='reshbxx("+val+")' class='_oper' title='订票'>&nbsp;<i class='icon-info-sign'></i>&nbsp;</a>&emsp;"; 
} 

<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
function updHbxx(id){
	QMoveDialog($("#hbxxEditWin"));
// 	$('#hbxxViewWin').dialog('close');
	$('#hbxxEditWin').dialog('open');
	$('#hbxxEditWin').get(0).contentWindow.load(id);
} 

<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
/* function vieHbxx(id){
	QMoveDialog($("#hbxxViewWin"));
	$('#hbxxEditWin').dialog('close');
	$('#hbxxViewWin').dialog('open');
	$('#hbxxViewWin').get(0).contentWindow.load(id);
} */

<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
function reshbxx(){
	var row = $('#hbxxGrid').datagrid('getSelected');
	window.location.href="/${projectName}/slwldx/hbxx/page.action?page=hbxx_res&id="+row.id;
}

<%-- 删除记录 --%>
function delHbxx(){
	var checkedItems = $('#hbxxGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/slwldx/hbxx/delHbxx.action",{ids:ids},function(data){
				$('#hbxxGrid').datagrid('reload');
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#hbxxEditWin').dialog('close');
	$('#hbxxGrid').datagrid('reload');
}
</script>
</head>
<body>
<table id="hbxxGrid" title="" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#hbxxGridToolbar',collapsible:true,url:'/${projectName}/slwldx/hbxx/schHbxx.action',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" ></th>
			<th data-options="field:'hkgs',width:10,sortable:true">航空公司</th>
			<th data-options="field:'hbh',width:5,sortable:true">航班号</th>
			<th data-options="field:'qfsj',sortable:true">起飞时间</th>
			<th data-options="field:'ddsj',sortable:true">到达时间</th>
			<!-- <th data-options="field:'rq',sortable:true">日期</th> -->
			<th data-options="field:'cfcs',sortable:true">出发城市</th>
			<th data-options="field:'ddcs',sortable:true">达到城市</th>
			<th data-options="field:'qfjc',width:10,sortable:true">起飞机场</th>
			<th data-options="field:'ddjc',width:10,sortable:true">抵达机场</th>
			<th data-options="field:'jg',sortable:true">价格(元)</th>
			<th data-options="field:'zdl',sortable:true">准点率(%)</th>

			<th data-options="field:'_oper',align:'center',formatter:formatOper">操作</th>
		</tr>
	</thead>
</table>
<%-- 查询条件设定栏 --%>
<div id="hbxxGridToolbar">
	<div class="opline">
		<a href="javascript:updHbxx(0)" class="btn btn-success"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">添加</span></a>&emsp;
		<a href="javascript:delHbxx()" class="btn btn-danger"><span class="icon-trash" style="font-size:16px"></span> <span style="font-size:14px">删除</span></a>
	</div>
	<form id="_form" class="shline">
	日期：<input name='rq' id="rq" class='easyui-datebox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).datebox('setValue', '');}
}]"/>
	出发城市：<dc:insertList name="cfcs" dictId="CCQ_CS" style="combo_smart" />
	达到城市：<dc:insertList name="ddcs" dictId="CCQ_CS" style="combo_smart" />

	<a href="javascript:void(0)" onclick="doSearch()" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>
	</form>
</div>


<%-- 修改or添加面板 --%>
<iframe id="hbxxEditWin" src="/${projectName}/slwldx/hbxx/page.action?page=hbxx_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#hbxxEditButton'" title="修改航班信息" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="hbxxEditButton">
	<a href="javascript:void(0)" onclick="$('#hbxxEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#hbxxEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >取 消</a>
</div>
<%-- 查看面板 --%>
<%-- <iframe id="hbxxViewWin" src="/${projectName}/slwldx/hbxx/page.action?page=hbxx_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#hbxxViewButton'" title="查看航班信息" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="hbxxViewButton">
	<a href="javascript:$('#hbxxViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >关 闭</a>
</div> --%>
</body>
</html>
