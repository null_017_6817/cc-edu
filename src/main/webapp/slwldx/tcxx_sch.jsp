<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>套餐信息查询</title><!--tcxx-->
	<%@include file="/common/include.jsp" %>

<script>
$(function(){
	var pager = $('#tcxxGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});			
})

<%-- 执行查询，点击查询按钮时调用 --%>
function doSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#tcxxGrid').datagrid('load',{
		tcxy:tcxyDict.getValue(),
		tcmc:$('#tcmc').val()
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatOper(val,row,index){  
    return "&emsp;<a href='javascript:void(0)' onclick='updTcxx("+val+")' class='_oper' title='修改'>&nbsp;<i class='icon-edit'></i>&nbsp;</a>"
    			+"<a href='javascript:void(0)' onclick='vieTcxx("+val+")' class='_oper' title='查看'>&nbsp;<i class='icon-info-sign'></i>&nbsp;</a>&emsp;"; 
} 

<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
function updTcxx(id){
	QMoveDialog($("#tcxxEditWin"));
	$('#tcxxViewWin').dialog('close');
	$('#tcxxEditWin').dialog('open');
	$('#tcxxEditWin').get(0).contentWindow.load(id);
} 

<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
function vieTcxx(id){
	QMoveDialog($("#tcxxViewWin"));
	$('#tcxxEditWin').dialog('close');
	$('#tcxxViewWin').dialog('open');
	$('#tcxxViewWin').get(0).contentWindow.load(id);
}

<%-- 删除记录 --%>
function delTcxx(){
	var checkedItems = $('#tcxxGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/slwldx/tcxx/delTcxx.action",{ids:ids},function(data){
				$('#tcxxGrid').datagrid('reload');
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#tcxxEditWin').dialog('close');
	$('#tcxxGrid').datagrid('reload');
}
</script>
</head>
<body>
<table id="tcxxGrid" title="" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#tcxxGridToolbar',collapsible:true,url:'/${projectName}/slwldx/tcxx/schTcxx.action',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" ></th>
			<th data-options="field:'tcxy',width:80,sortable:true">套餐行业</th>
			<th data-options="field:'tcmc',sortable:true">套餐名称</th>
			<th data-options="field:'zf',sortable:true">资费（元）</th>
			<th data-options="field:'dx',sortable:true">短信（条数）</th>
			<th data-options="field:'gnll',sortable:true">国内流量（M）</th>
			<th data-options="field:'bdll',sortable:true">本地流量（M）</th>
			<th data-options="field:'bdth',sortable:true">本地通话(分钟)</th>
			<th data-options="field:'qgth',sortable:true">全国通话（分钟）</th>
			<th data-options="field:'xxsm',sortable:true">详细说明</th>

			<th data-options="field:'_oper',align:'center',formatter:formatOper">操作</th>
		</tr>
	</thead>
</table>
<%-- 查询条件设定栏 --%>
<div id="tcxxGridToolbar">
	<div class="opline">
		<a href="javascript:updTcxx(0)" class="btn btn-success"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">添加</span></a>&emsp;
		<a href="javascript:delTcxx()" class="btn btn-danger"><span class="icon-trash" style="font-size:16px"></span> <span style="font-size:14px">删除</span></a>
	</div>
	<form id="_form" class="shline">
	套餐行业：<dc:insertList name="tcxy" dictId="CCQ_TCHY" style="combo_normal" />
	套餐名称：<input name='tcmc' id="tcmc" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>

	<a href="javascript:void(0)" onclick="doSearch()" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>
	</form>
</div>


<%-- 修改or添加面板 --%>
<iframe id="tcxxEditWin" src="/${projectName}/slwldx/tcxx/page.action?page=tcxx_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#tcxxEditButton'" title="修改套餐信息" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="tcxxEditButton">
	<a href="javascript:void(0)" onclick="$('#tcxxEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#tcxxEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >取 消</a>
</div>
<%-- 查看面板 --%>
<iframe id="tcxxViewWin" src="/${projectName}/slwldx/tcxx/page.action?page=tcxx_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#tcxxViewButton'" title="查看套餐信息" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="tcxxViewButton">
	<a href="javascript:$('#tcxxViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >关 闭</a>
</div>
</body>
</html>
