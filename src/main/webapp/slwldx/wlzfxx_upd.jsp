<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>修改物流资费信息</title><!--wlzfxx-->
	<%@include file="/common/include.jsp" %>
<script>
<%-- 查询页点击修改时调用此方法加载记录详情 --%>
function load(id){
	$('#_form').form('load','/${projectName}/slwldx/wlzfxx/vieWlzfxx.action?id='+id);
};

<%-- 点击保存按钮时调用此方法。(注:保存按钮在查询列表页) --%>
function submitForm(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#_form').form('submit',{
      success:function(data) {
      	parent.reloadData();
      }
   });
}
</script>
</head>
<body>
<form id="_form" method="post" action="/${projectName}/slwldx/wlzfxx/updWlzfxx.action">
	<input type="hidden" name='id' id='id'/>
	<table>
		<tr><td class='label' >物流公司：</td><td><dc:insertList name="wlgs" dictId="CCQ_WLGS" style="combo_normal" /></td></tr>
		<tr><td class='label' style="width:120px">始发城市：</td><td><dc:insertTree name="sfcs" dictId="CCQ_QQCS" style="asynch_single" /></td></tr>
		<tr><td class='label' >目的城市：</td><td><dc:insertTree name="mdcs" dictId="CCQ_QQCS" style="asynch_single" /></td></tr>
		<tr><td class='label' >价格(元)：</td><td><input id="jg" name="jg" class="easyui-textbox" data-options="required:true,validType:['int']"/></td></tr>
	</table>

</form>
</body>
</html>
