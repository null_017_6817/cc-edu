<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>物流资费信息查询</title><!--wlzfxx-->
	<%@include file="/common/include.jsp" %>

<script>
$(function(){
	var pager = $('#wlzfxxGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});			
})

<%-- 执行查询，点击查询按钮时调用 --%>
function doSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#wlzfxxGrid').datagrid('load',{
		sfcs:sfcsDict.getValue(),
		mdcs:mdcsDict.getValue(),
		wlgs:wlgsDict.getValue()
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatOper(val,row,index){  
    return "&emsp;<a href='javascript:void(0)' onclick='updWlzfxx("+val+")' class='_oper' title='修改'>&nbsp;<i class='icon-edit'></i>&nbsp;</a>"
    			+"<a href='javascript:void(0)' onclick='vieWlzfxx("+val+")' class='_oper' title='查看'>&nbsp;<i class='icon-info-sign'></i>&nbsp;</a>&emsp;"; 
} 

<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
function updWlzfxx(id){
	QMoveDialog($("#wlzfxxEditWin"));
	$('#wlzfxxViewWin').dialog('close');
	$('#wlzfxxEditWin').dialog('open');
	$('#wlzfxxEditWin').get(0).contentWindow.load(id);
} 

<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
function vieWlzfxx(id){
	QMoveDialog($("#wlzfxxViewWin"));
	$('#wlzfxxEditWin').dialog('close');
	$('#wlzfxxViewWin').dialog('open');
	$('#wlzfxxViewWin').get(0).contentWindow.load(id);
}

<%-- 删除记录 --%>
function delWlzfxx(){
	var checkedItems = $('#wlzfxxGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/slwldx/wlzfxx/delWlzfxx.action",{ids:ids},function(data){
				$('#wlzfxxGrid').datagrid('reload');
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#wlzfxxEditWin').dialog('close');
	$('#wlzfxxGrid').datagrid('reload');
}
</script>
</head>
<body>
<table id="wlzfxxGrid" title="" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#wlzfxxGridToolbar',collapsible:true,url:'/${projectName}/slwldx/wlzfxx/schWlzfxx.action',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" ></th>
			<th data-options="field:'wlgs',width:40,sortable:true">物流公司</th>
			<th data-options="field:'sfcs',width:20,sortable:true">始发城市</th>
			<th data-options="field:'mdcs',width:20,sortable:true">目的城市</th>
			<th data-options="field:'jg',sortable:true">价格(元)</th>

			<th data-options="field:'_oper',align:'center',formatter:formatOper,width:20">操作</th>
		</tr>
	</thead>
</table>
<%-- 查询条件设定栏 --%>
<div id="wlzfxxGridToolbar">
	<div class="opline">
		<a href="javascript:updWlzfxx(0)" class="btn btn-success"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">添加</span></a>&emsp;
		<a href="javascript:delWlzfxx()" class="btn btn-danger"><span class="icon-trash" style="font-size:16px"></span> <span style="font-size:14px">删除</span></a>
	</div>
	<form id="_form" class="shline">
	物流公司：<dc:insertList name="wlgs" dictId="CCQ_WLGS" style="combo_normal" />
	始发城市：<dc:insertTree name="sfcs" dictId="CCQ_QQCS" style="asynch_single" />
	目的城市：<dc:insertTree name="mdcs" dictId="CCQ_QQCS" style="asynch_single" />
	
	<a href="javascript:void(0)" onclick="doSearch()" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>
	</form>
</div>


<%-- 修改or添加面板 --%>
<iframe id="wlzfxxEditWin" src="/${projectName}/slwldx/wlzfxx/page.action?page=wlzfxx_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#wlzfxxEditButton'" title="修改物流资费信息" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="wlzfxxEditButton">
	<a href="javascript:void(0)" onclick="$('#wlzfxxEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#wlzfxxEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >取 消</a>
</div>
<%-- 查看面板 --%>
<iframe id="wlzfxxViewWin" src="/${projectName}/slwldx/wlzfxx/page.action?page=wlzfxx_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#wlzfxxViewButton'" title="查看物流资费信息" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="wlzfxxViewButton">
	<a href="javascript:$('#wlzfxxViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >关 闭</a>
</div>
</body>
</html>
