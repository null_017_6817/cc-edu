<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>物流订单信息查询</title><!--wlddxx-->
	<%@include file="/common/include.jsp" %>

<script>
$(function(){
	var pager = $('#wlddxxGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});			
})

<%-- 执行查询，点击查询按钮时调用 --%>
function doSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#wlddxxGrid').datagrid('load',{
		ddh:$('#ddh').val(),
		jjr:$('#jjr').val(),
		jjrdh:$('#jjrdh').val(),
		sfcs:sfcsDict.getValue(),
		mdcs:mdcsDict.getValue(),
		jjsj:$('#jjsj').val()
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatOper(val,row,index){  
    return "&emsp;<a href='javascript:void(0)' onclick='updWlddxx("+val+")' class='_oper' title='修改'>&nbsp;<i class='icon-edit'></i>&nbsp;</a>"
    			+"<a href='javascript:void(0)' onclick='vieWlddxx("+val+")' class='_oper' title='查看'>&nbsp;<i class='icon-info-sign'></i>&nbsp;</a>&emsp;"; 
} 

<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
function updWlddxx(id){
	QMoveDialog($("#wlddxxEditWin"));
	$('#wlddxxViewWin').dialog('close');
	$('#wlddxxEditWin').dialog('open');
	$('#wlddxxEditWin').get(0).contentWindow.load(id);
} 

<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
function vieWlddxx(id){
	QMoveDialog($("#wlddxxViewWin"));
	$('#wlddxxEditWin').dialog('close');
	$('#wlddxxViewWin').dialog('open');
	$('#wlddxxViewWin').get(0).contentWindow.load(id);
}

<%-- 删除记录 --%>
function delWlddxx(){
	var checkedItems = $('#wlddxxGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/slwldx/wlddxx/delWlddxx.action",{ids:ids},function(data){
				$('#wlddxxGrid').datagrid('reload');
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#wlddxxEditWin').dialog('close');
	$('#wlddxxGrid').datagrid('reload');
}
</script>
</head>
<body>
<table id="wlddxxGrid" title="" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#wlddxxGridToolbar',collapsible:true,url:'/${projectName}/slwldx/wlddxx/schWlddxx.action',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" ></th>
			<th data-options="field:'wlzfid',width:10,sortable:true" dictId="CCQ_WLZF">物流</th>
			<th data-options="field:'ddh',sortable:true">订单号</th>
			<th data-options="field:'ddjg',sortable:true">订单价格(元)</th>
			<th data-options="field:'jjr',sortable:true">寄件人</th>
			<th data-options="field:'sjr',sortable:true">收件人</th>
			<th data-options="field:'jjrdh',sortable:true">寄件人电话</th>
			<th data-options="field:'sjrdh',sortable:true">收件人电话</th>
			<th data-options="field:'sfcs',sortable:true">始发城市</th>
			<th data-options="field:'mdcs',sortable:true">目的城市</th>
			<th data-options="field:'sfcsxxdz',width:10,sortable:true">始发城市详细地址</th>
			<th data-options="field:'mdcsxxdz',width:10,sortable:true">目的城市详细地址</th>
			<th data-options="field:'jjsj',sortable:true">寄件时间</th>
			<th data-options="field:'sjsj',sortable:true">收件时间</th>
			<th data-options="field:'ddzt',sortable:true">订单状态</th>

			<th data-options="field:'_oper',align:'center',formatter:formatOper">操作</th>
		</tr>
	</thead>
</table>
<%-- 查询条件设定栏 --%>
<div id="wlddxxGridToolbar">
	<div class="opline">
		<a href="javascript:updWlddxx(0)" class="btn btn-success"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">添加</span></a>&emsp;
		<a href="javascript:delWlddxx()" class="btn btn-danger"><span class="icon-trash" style="font-size:16px"></span> <span style="font-size:14px">删除</span></a>
	</div>
	<form id="_form" class="shline">
	订单号：<input name='ddh' id="ddh" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	寄件人：<input name='jjr' id="jjr" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	
	始发城市：<dc:insertList name="sfcs" dictId="CCQ_CS" style="combo_normal" />
	目的城市：<dc:insertList name="mdcs" dictId="CCQ_CS" style="combo_normal" />
	

	<a href="javascript:void(0)" onclick="doSearch()" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>
	</form>
</div>


<%-- 修改or添加面板 --%>
<iframe id="wlddxxEditWin" src="/${projectName}/slwldx/wlddxx/page.action?page=wlddxx_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#wlddxxEditButton'" title="修改物流订单信息" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="wlddxxEditButton">
	<a href="javascript:void(0)" onclick="$('#wlddxxEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#wlddxxEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >取 消</a>
</div>
<%-- 查看面板 --%>
<iframe id="wlddxxViewWin" src="/${projectName}/slwldx/wlddxx/page.action?page=wlddxx_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#wlddxxViewButton'" title="查看物流订单信息" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="wlddxxViewButton">
	<a href="javascript:$('#wlddxxViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >关 闭</a>
</div>
</body>
</html>
