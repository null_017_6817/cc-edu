<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>查看物流流转信息</title><!--wllzxx-->
	<%@include file="/common/include.jsp" %>
<script>
function load(id){
	$('#_form').form('load','/${projectName}/slwldx/wllzxx/vieWllzxx.action?toL=true&id='+id);
};
</script>
</head>
<body>
<form id="_form">
	<input type="hidden" name='id' value=""/>
	<table>
		<tr><td class='label'>订单号：</td><td><input type='text' name='ddh' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>经办人姓名：</td><td><input type='text' name='jbrxm' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>经办人电话：</td><td><input type='text' name='jbrdh' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>当前地址：</td><td><input type='text' name='dqdz' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>送达时间：</td><td><input type='text' name='sdsj' class='viewbox' readonly='readonly'/></td></tr>
	</table>

</form>
</body>
</html>
