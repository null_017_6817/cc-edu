<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>查看物流资费信息</title><!--wlzfxx-->
	<%@include file="/common/include.jsp" %>
<script>
function load(id){
	$('#_form').form('load','/${projectName}/slwldx/wlzfxx/vieWlzfxx.action?toL=true&id='+id);
};
</script>
</head>
<body>
<form id="_form">
	<input type="hidden" name='id' value=""/>
	<table>
		<tr><td class='label'>物流公司：</td><td><input type='text' name='wlgs' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>始发城市：</td><td><input type='text' name='sfcs' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>目的城市：</td><td><input type='text' name='mdcs' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>价格：</td><td><input type='text' name='jg' class='viewbox' readonly='readonly'/></td></tr>
	</table>

</form>
</body>
</html>
