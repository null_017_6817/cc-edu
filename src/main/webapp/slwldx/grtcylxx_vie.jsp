<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>查看个人套餐用量信息</title><!--grtcylxx-->
	<%@include file="/common/include.jsp" %>
<script>
function load(id){
	$('#_form').form('load','/${projectName}/slwldx/grtcylxx/vieGrtcylxx.action?toL=true&id='+id);
};
</script>
</head>
<body>
<form id="_form">
	<input type="hidden" name='id' value=""/>
	<table>
		<tr><td class='label'>手机号：</td><td><input type='text' name='sjh' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>姓名：</td><td><input type='text' name='xm' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>套餐：</td><td><input type='text' name='tcid' class='viewbox' readonly='readonly' style="width:180px"/></td></tr>
		<tr><td class='label'>短信剩余（条数）：</td><td><input type='text' name='dxsy' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>国内流量剩余（M）：</td><td><input type='text' name='gnllsy' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>本地流量剩余（M）：</td><td><input type='text' name='bdllsy' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>本地通话剩余(分钟)：</td><td><input type='text' name='bdthsy' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>全国通话剩余（分钟）：</td><td><input type='text' name='qgthsy' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>话费余额（元）：</td><td><input type='text' name='hfye' class='viewbox' readonly='readonly'/></td></tr>
	</table>

</form>
</body>
</html>
