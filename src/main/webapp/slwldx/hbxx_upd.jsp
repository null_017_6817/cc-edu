<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>修改航班信息</title><!--hbxx-->
	<%@include file="/common/include.jsp" %>
<script>
<%-- 查询页点击修改时调用此方法加载记录详情 --%>
function load(id){
	$('#_form').form('load','/${projectName}/slwldx/hbxx/vieHbxx.action?id='+id);
};

<%-- 点击保存按钮时调用此方法。(注:保存按钮在查询列表页) --%>
function submitForm(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#_form').form('submit',{
      success:function(data) {
      	parent.reloadData();
      }
   });
}
</script>
</head>
<body>
<form id="_form" method="post" action="/${projectName}/slwldx/hbxx/updHbxx.action">
	<input type="hidden" name='id' id='id'/>
	<table>
		<tr><td class='label' style="width:120px">航空公司：</td><td><input id="hkgs" name="hkgs" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >航班号：</td><td><input id="hbh" name="hbh" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >起飞时间：</td><td><input id="qfsj" name="qfsj" class="easyui-timespinner" data-options="required:true"/></td></tr>
		<tr><td class='label' >到达时间：</td><td><input id="ddsj" name="ddsj" class="easyui-timespinner" data-options="required:true"/></td></tr>
		<!-- <tr><td class='label' >日期：</td><td><input id="rq" name="rq" class="easyui-datebox" data-options="required:true"/></td></tr> -->
		<tr><td class='label' >出发城市：</td><td><dc:insertList name="cfcs" dictId="CCQ_CS" style="combo_smart" /></td></tr>
		<tr><td class='label' >达到城市：</td><td><dc:insertList name="ddcs" dictId="CCQ_CS" style="combo_smart" /></td></tr>
		<tr><td class='label' >起飞机场：</td><td><input id="qfjc" name="qfjc" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >抵达机场：</td><td><input id="ddjc" name="ddjc" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >价格：</td><td><input id="jg" name="jg" class="easyui-textbox" data-options="required:true,validType:['int']"/>元</td></tr>
		<tr><td class='label' >准点率：</td><td><input id="zdl" name="zdl" class="easyui-textbox" data-options="required:true,validType:['int']"/>%</td></tr>
	</table>

</form>
</body>
</html>
