<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>机票订单信息查询</title><!--jpddxx-->
	<%@include file="/common/include.jsp" %>

<script>
$(function(){
	var pager = $('#jpddxxGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});			
})

<%-- 执行查询，点击查询按钮时调用 --%>
function doSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#jpddxxGrid').datagrid('load',{
		ddh:$('#ddh').val(),
		dprxm:$('#dprxm').val(),
		dpsj:$("input[name='dpsj']").val()
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatOper(val,row,index){  
    return "&emsp;<a href='javascript:void(0)' onclick='updJpddxx("+val+")' class='_oper' title='修改'>&nbsp;<i class='icon-edit'></i>&nbsp;</a>"
    			+"<a href='javascript:void(0)' onclick='vieJpddxx("+val+")' class='_oper' title='查看'>&nbsp;<i class='icon-info-sign'></i>&nbsp;</a>&emsp;"; 
} 

<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
function updJpddxx(id){
	QMoveDialog($("#jpddxxEditWin"));
	$('#jpddxxViewWin').dialog('close');
	$('#jpddxxEditWin').dialog('open');
	$('#jpddxxEditWin').get(0).contentWindow.load(id);
} 

<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
function vieJpddxx(id){
	QMoveDialog($("#jpddxxViewWin"));
	$('#jpddxxEditWin').dialog('close');
	$('#jpddxxViewWin').dialog('open');
	$('#jpddxxViewWin').get(0).contentWindow.load(id);
}

<%-- 删除记录 --%>
function delJpddxx(){
	var checkedItems = $('#jpddxxGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/slwldx/jpddxx/delJpddxx.action",{ids:ids},function(data){
				$('#jpddxxGrid').datagrid('reload');
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#jpddxxEditWin').dialog('close');
	$('#jpddxxGrid').datagrid('reload');
}
</script>
</head>
<body>
<table id="jpddxxGrid" title="" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#jpddxxGridToolbar',collapsible:true,url:'/${projectName}/slwldx/jpddxx/schJpddxx.action',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" ></th>
			<th data-options="field:'ddh',width:10,sortable:true">订单号</th>
			<th data-options="field:'hbh',width:5,sortable:true">航班号</th>
			<th data-options="field:'hbrq',width:2,sortable:true">航班日期</th>
			<th data-options="field:'dprxm',sortable:true">订票人姓名</th>
			<th data-options="field:'dprxb',sortable:true" dictId="ZD_XB">订票人性别</th>
			<th data-options="field:'dpsj',sortable:true">订票时间</th>
			<th data-options="field:'sfzh',width:5,sortable:true" >身份证号</th>
			<th data-options="field:'dh',width:3,sortable:true">电话</th>
			<!-- <th data-options="field:'ddzt',sortable:true">订单状态</th> -->

			<th data-options="field:'_oper',align:'center',formatter:formatOper">操作</th>
		</tr>
	</thead>
</table>
<%-- 查询条件设定栏 --%>
<div id="jpddxxGridToolbar">
	<div class="opline">
		<a href="javascript:updJpddxx(0)" class="btn btn-success"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">添加</span></a>&emsp;
		<a href="javascript:delJpddxx()" class="btn btn-danger"><span class="icon-trash" style="font-size:16px"></span> <span style="font-size:14px">删除</span></a>
	</div>
	<form id="_form" class="shline">
	订单号：<input name='ddh' id="ddh" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	订票人姓名：<input name='dprxm' id="dprxm" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	订票时间：<input name='dpsj' id="dpsj" class='easyui-datebox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).datebox('setValue', '');}
}]"/>

	<a href="javascript:void(0)" onclick="doSearch()" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>
	</form>
</div>


<%-- 修改or添加面板 --%>
<iframe id="jpddxxEditWin" src="/${projectName}/slwldx/jpddxx/page.action?page=jpddxx_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#jpddxxEditButton'" title="修改机票订单信息" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="jpddxxEditButton">
	<a href="javascript:void(0)" onclick="$('#jpddxxEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#jpddxxEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >取 消</a>
</div>
<%-- 查看面板 --%>
<iframe id="jpddxxViewWin" src="/${projectName}/slwldx/jpddxx/page.action?page=jpddxx_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#jpddxxViewButton'" title="查看机票订单信息" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="jpddxxViewButton">
	<a href="javascript:$('#jpddxxViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >关 闭</a>
</div>
</body>
</html>
