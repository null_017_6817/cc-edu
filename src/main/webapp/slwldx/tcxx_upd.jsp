<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>修改套餐信息</title><!--tcxx-->
	<%@include file="/common/include.jsp" %>
<script>
<%-- 查询页点击修改时调用此方法加载记录详情 --%>
function load(id){
	$('#_form').form('load','/${projectName}/slwldx/tcxx/vieTcxx.action?id='+id);
};

<%-- 点击保存按钮时调用此方法。(注:保存按钮在查询列表页) --%>
function submitForm(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#_form').form('submit',{
      success:function(data) {
      	parent.reloadData();
      }
   });
}
</script>
</head>
<body>
<form id="_form" method="post" action="/${projectName}/slwldx/tcxx/updTcxx.action">
	<input type="hidden" name='id' id='id'/>
	<table>
		<tr><td class='label' style="width:120px">套餐行业：</td><td><dc:insertList name="tcxy" dictId="CCQ_TCHY" style="combo_normal" /></td></tr>
		<tr><td class='label' >套餐名称：</td><td><input id="tcmc" name="tcmc" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >资费（元）：</td><td><input id="zf" name="zf" class="easyui-textbox" data-options="required:true,validType:['int']"/></td></tr>
		<tr><td class='label' >短信（条数）：</td><td><input id="dx" name="dx" class="easyui-textbox" data-options="required:true,validType:['int']"/></td></tr>
		<tr><td class='label' >国内流量（M）：</td><td><input id="gnll" name="gnll" class="easyui-textbox" data-options="required:true,validType:['int']"/></td></tr>
		<tr><td class='label' >本地流量（M）：</td><td><input id="bdll" name="bdll" class="easyui-textbox" data-options="required:true,validType:['int']"/></td></tr>
		<tr><td class='label' >本地通话(分钟)：</td><td><input id="bdth" name="bdth" class="easyui-textbox" data-options="required:true,validType:['int']"/></td></tr>
		<tr><td class='label' >全国通话（分钟）：</td><td><input id="qgth" name="qgth" class="easyui-textbox" data-options="required:true,validType:['int']"/></td></tr>
		<tr><td class='label' >详细说明：</td>
		<td>
		<!-- 	<input id="xxsm" name="xxsm" class="easyui-textbox" data-options="required:true"/> -->
		<textarea id="" id="xxsm" name="xxsm" rows=5 cols="45" class="textarea easyui-validatebox"></textarea>
		</td>
		</tr>
	</table>

</form>
</body>
</html>
