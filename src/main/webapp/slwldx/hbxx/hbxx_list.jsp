<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<head>
<meta charset="utf-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>呼叫中心-电信列表</title>
<link rel="stylesheet" type="text/css" href="/${projectName}/slwldx/aadx/css/normalize.css" />
<link rel="stylesheet" type="text/css" href="/${projectName}/slwldx/aadx/css/fonts.css" />
<link rel="stylesheet" type="text/css" href="/${projectName}/slwldx/aadx/css/style.css" />
<link type="text/css" rel="stylesheet" href="/${projectName}/slwldx/hbxx/css/time/flatpickr.min.css">

<script type="text/javascript" src="/${projectName}/slwldx/hbxx/js/lib/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="/${projectName}/slwldx/hbxx/js/lib/time/flatpickr.js" async onload="fp_ready()"></script>
<script type="text/javascript" src="/${projectName}/slwldx/hbxx/js/hbxxList.js"></script>
<script async>
	
	//出发时间
	function fp_ready() {
		//Regular flatpickr
		document.getElementsByClassName("calendar").flatpickr();
		var calendars = document.getElementsByClassName("flatpickr").flatpickr();
	}
	
	//查询数据路径
	var projectName = "/${projectName}/slwldx/jpddxx/schJpddxxForJsp.action";

	//退票
	function refund(obj) {
		var cid = $(obj).parent().attr("dataId");//获取起飞时间
		if(confirm('确定退票？')==true){
			window.location = "/${projectName}/slwldx/jpddxx/refund.action?cid="+ cid;
		}else{
			return;
		}
	}

</script>
</head>

<body>
	<div class="wrap">
		<jsp:include page="../../book/hy_header.jsp" />

		<div class="main">
			<div class="listBox inner">
				<!-- 搜索 -->
				<form class="clearfix">
					<div class="item">
						<label>订单号：</label> <input type="text" name="ddh" id="ddh" value="" />
					</div>
					<div class="item">
						<label>订票人姓名：</label> <input type="text" name="dprxm" id="dprxm" value="" />
					</div>
					<div class="item">
						<label>订票时间：</label> <input type="text" name="dpsj" id="dpsj" value="" />
					</div>
					<div class="item btn">
						<i class="icon-search"></i> <input type="button" onclick="search()" value="搜索" />
					</div>
				</form>

				<!-- 搜索结果 -->
				<div class="resultLIst">
					<table>
						<thead>
							<tr>
								<td>订单号</td>
								<td>航班号</td>
								<td>航班日期</td>
								<td>订票人姓名</td>
								<td>订票人性别</td>
								<td>订票时间</td>
								<td>身份证号</td>
								<td>电话</td>
								<td>操作</td>
							</tr>
						</thead>
					
						<tbody id="grtcylxxList">
						</tbody>
					</table>
				</div>

				<!-- 分页 -->
				<div class="fydiv">
					<ul class="fenye"></ul>
				</div>
			</div>
		</div>

		<jsp:include page="../../book/_footer.jsp" />
		
		<!-- 改签弹层 -->
		<div class="pop-con" id="ticketchang">
			<h5>改签信息</h5>
			<span class="close" onclick="returnBack()">×</span>
			<form id="_form" method="post" action="/${projectName}/slwldx/jpddxx/ticketchang.action">
				<input type='hidden' name='id' id='id'/>
				<input type='hidden' name='dprxb' id='dprxb'/>
				<table id="totable">
				</table>
				<input value="改期" class="submit" type="submit"  />
			</form>
		</div>
	</div>
</body>
</html>
