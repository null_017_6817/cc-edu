<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<head>
<meta charset="utf-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>呼叫中心</title>

<link rel="stylesheet" type="text/css" href="/${projectName}/slwldx/hbxx/css/normalize.css" />
<link rel="stylesheet" type="text/css" href="/${projectName}/slwldx/hbxx/css/kuCity.css" />
<link rel="stylesheet" type="text/css" href="/${projectName}/slwldx/hbxx/css/fonts.css" />
<link type="text/css" rel="stylesheet" href="/${projectName}/slwldx/hbxx/css/time/flatpickr.min.css">
<link rel="stylesheet" type="text/css" href="/${projectName}/slwldx/hbxx/css/index.css" />

<script type="text/javascript" src="/${projectName}/slwldx/hbxx/js/lib/jquery-1.9.1.min.js"></script>
<script src="/${projectName}/common/artDialogs/artDialog.source.js?skin=default" type="text/javascript"></script>
<script src="/${projectName}/common/artDialogs/plugins/iframeTools.source.js" type="text/javascript"></script>
<%-- <script type="text/javascript" src="/${projectName}/slwldx/hbxx/js/lib/time/flatpickr.js" async onload="fp_ready()"></script> --%>
<script type="text/javascript" charset="utf-8"	src="/${projectName}/js/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="/${projectName}/slwldx/hbxx/js/lib/kuCity.js"></script>
<script type="text/javascript" src="/${projectName}/slwldx/hbxx/js/tab.js"></script>
<script type="text/javascript" src="/${projectName}/slwldx/hbxx/js/lib/jquery.validate.min.js"></script>
<script type="text/javascript" src="/${projectName}/slwldx/hbxx/js/hbxx_sch.js"></script>
<script type="text/javascript" src="/${projectName}/slwldx/hbxx/js/form.js"></script>

<script async>
//出发时间
	/* function fp_ready() {
		//Regular flatpickr
		document.getElementsByClassName("calendar").flatpickr();
		var calendars = document.getElementsByClassName("flatpickr").flatpickr();
	} */
	$(function(){
		/* document.getElementsByClassName("calendar").flatpickr();
		var calendars = document.getElementsByClassName("flatpickr").flatpickr(); */
	})
	//订票方法
	function reshbxx(obj) {
		var nowDate = new Date();//获取系统时间
		var calendar = $("#oranger").find(".active").find(".calendar").html();//订票弹出日期和星期
		var spcalendar = calendar.split("");
		var caltime = calendar.substring(0,spcalendar.length-3);//当前日期
		var newDate = new Date(Date.parse(caltime.replace(/-/g, "/")));
		var qfsj = $(obj).parent().parent().find("#qfsj").html();//获取起飞时间
		var qfsjs = new Array(); //定义一数组 
		qfsjs = qfsj.split(":"); //字符分割 
		for (var j = 0; j < qfsjs.length; j++) {
			newDate.setHours(qfsjs[0], qfsjs[1], 0, 0);//设置当前时间为起飞时间
		}
		var hbxxId = $(obj).parent().find("span").html();
		//$("#buy").show();
		var money = $("#oranger").find(".active").find(".money").text();
		window.location.href = "/${projectName}/slwldx/hbxx/success.jsp?hbxxId="+hbxxId+"&calendar="+calendar+"&money="+money; 
	}
</script>


</head>
<body>
	<div class="wrap">
		<jsp:include page="../../book/hy_header.jsp" />

		<div class="main">
			<div class="inner">
				<!-- 搜索 -->
				<form id="searchBox" class="searchBox">
					<div class="item">
						<span class="icon-plane1"></span> <input type="text" name="cfcs"
							placeholder="出发城市" class="DepartureCity" />
					</div>
					<span class="change"></span>
					<div class="item">
						<span class="icon-plane2"></span> <input type="text" name="ddcs"
							placeholder="到达城市" class="ArrivalCity" />
					</div>
					<div class="item">
						<span class="icon-date"></span>
						<input type="text" name="rq" class="Wdate" onclick="WdatePicker({isShowClear:false,readOnly:true,dateFmt:'yyyy-MM-dd',minDate:'%y-%M-%d'})" placeholder="出发日期">
					</div>
					<div class="item">
						<input type="button" id="searchbtn" value="搜索" onclick="doSearch()" />
					</div>
				</form>

				<!-- 时间切换 -->
				<div class="timeBox" id="week_Time">
					<ul id="oranger">
						<li class="active"><span class="calendar" id="today"></span>
							<span class="money" id="firstm"></span></li>
						<li><span class="calendar" id="secondDay"></span> <span
							class="money" id="secondm"></span></li>
						<li><span class="calendar" id="thirdDay"></span> <span
							class="money" id="thirdm"></span></li>
						<li><span class="calendar" id="fourthDay"></span> <span
							class="money" id="fourthm"></span></li>
						<li><span class="calendar" id="fiveDay"></span> <span
							class="money" id="fifthm"></span></li>
						<li><span class="calendar" id="sixDay"></span> <span
							class="money" id="sixthm"></span></li>
						<li><span class="calendar" id="seventhDay"></span> <span
							class="money" id="seventhm"></span></li>
					</ul>
				</div>

				<div class="sortControls">
					<ul class="clearfix">
						<li class="li1">航班信息</li>
						<li class="li2">起飞时间</li>
						<li class="li3">到达时间</li>
						<li class="li4">准点率</li>
						<li class="li6">优惠</li>
						<li class="li5">价格</li>
					</ul>
				</div>

				<!-- 切换结果 -->
				<div class="resultBox" id="tablea">
					<div class="resultCon">
						<div class="search_list">
							<table cellspacing="0" cellpadding="0" border="0" id="first_list">
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
					<div class="resultCon">
						<div class="search_list">
							<table cellspacing="0" cellpadding="0" border="0" id="second_list">
								<tbody>
								</tbody>
							</table>
						</div>
					</div>

					<div class="resultCon">
						<div class="search_list">
							<table cellspacing="0" cellpadding="0" border="0" id="third_list">
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
					<div class="resultCon">
						<div class="search_list">
							<table cellspacing="0" cellpadding="0" border="0" id="fourth_list">
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
					<div class="resultCon">
						<div class="search_list">
							<table cellspacing="0" cellpadding="0" border="0" id="fifth_list">
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
					<div class="resultCon">
						<div class="search_list">
							<table cellspacing="0" cellpadding="0" border="0" id="sixth_list">
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
					<div class="resultCon">
						<div class="search_list">
							<table cellspacing="0" cellpadding="0" border="0" id="seventh_list">
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

		<jsp:include page="../../book/_footer.jsp" />

		<!-- 购票弹层 -->
		<div class="pop" id="buy">
			<div class="pop-con" style="overflow-y:scroll; height: 587px;" >
				<div class="flight-info" id="top_pop"></div>
				<div class="flight-detail" id="centre_pop"></div>
				<form id="_form" method="post" action="/${projectName}/slwldx/hbxx/reshbxx.action">
				<div class="passenger">乘客信息</div>
					<input type="hidden" id="hbxxid" name="hbxxid" value="${hbxxId}">
					<input type="hidden" id="hbrq" name="hbrq" value="${calendar}"> 
					<div class="flight-detail">
						<ul>
							<li><label>姓名</label> <input type="text" name="dprxm"
								class="required dprxm" placeholder="请与登记证件姓名保持一致" /></li>
							<li><label>身份证</label> <input type="text" name="sfzh"
								class="required sfzh" placeholder="登机证件号码" /></li>
							<li><label>中国大陆区号86</label> <input type="text" name="dh"
								class="required dh" placeholder="乘机手机号，用来接收航变信息" /></li>
							<li><label>性别</label> <input type="radio" value="0" name="dprxb0" checked /><i>男</i>
								 <input type="radio" value="1" name="dprxb0" /><i>女</i></li>
						</ul>
					</div>
					<div id="addPassengers"></div>
					<input value="订票" class="submit"  type="submit" />
				</form>
				<!-- <button onclick="addPassengers();">添加乘客</button>   onclick="saveData();" -->
			</div>
		</div>

		<!--购票成功弹层 -->
		<div class="pop" id="success">
			<div class="pop-con">
				<dl>
					<dt>
						<img src="img/success.png">
					</dt>
					<dd>
						订票成功 <span>还剩3秒自动跳转......</span>
					</dd>
				</dl>
			</div>
		</div>
	</div>
	<div id="loadDiv" style="display:none" >
		<span><img src="/${projectName}/book/img/loading.gif" />正在查询，请稍后...</span>
	</div>
	<script>
		//地区选择
		$('.DepartureCity').kuCity();
		$('.ArrivalCity').kuCity();
		var projectName = "/${projectName}/slwldx/hbxx/schHbxx.action";
	</script>
</body>
</html>
