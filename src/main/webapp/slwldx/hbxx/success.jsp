<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<head>
<meta charset="utf-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>呼叫中心-购票信息</title>
<link rel="stylesheet" type="text/css" href="/${projectName}/slwldx/hbxx/css/normalize.css" />
<link rel="stylesheet" type="text/css" href="/${projectName}/slwldx/hbxx/css/fonts.css" />
<script type="text/javascript" src="/${projectName}/slwldx/hbxx/js/lib/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="/${projectName}/slwldx/hbxx/js/lib/jquery.validate.min.js"></script>
<script type="text/javascript" src="/${projectName}/slwldx/hbxx/js/form.js"></script>
<script type="text/javascript"	src="/${projectName}/book/js/sweetAlert/sweetalert2.min.js"></script>
<link rel="stylesheet" type="text/css"	href="/${projectName}/book/js/sweetAlert/sweetalert2.min.css" />
<script>
$(function() {
	ok();
 })

//添加乘客
function addPassengers() {
	var len = $(".pop-con").find("input[name='dprxm']").length;	
	$('#addPassengers').append("<div class='passenger'><h5>乘客信息</h5><span class='close'>× 删除</span><div class='flight-detail'>"
	+"<ul><li><label>姓名</label><input type='text' value='' name='dprxm'	class='required dprxm' placeholder='请与登记证件姓名保持一致'  /></li>"
	+"<li><label>身份证</label><input type='text' name='sfzh' value='' class='required sfzh' placeholder='登机证件号码' /></li>"
	+"<li><label>中国大陆区号86</label><input type='text' name='dh' value='' class='required dh' placeholder='乘机手机号，用来接收航变信息' /></li>"
	+"<li><label>性别</label><input type='radio' value='0' name='dprxb["+len+"]' id='man' checked /><label for='man'>男</label>&emsp;&emsp;<input type='radio' value='1'"
	+"name='dprxb["+len+"]' id='woman' /><label for='woman'>女</label></li></ul></div></div>");
	
	
	//关闭乘客信息
	$('.passenger .close').each(function () {
		$(this).click(function () {
			$(this).parent().remove();
		})
	})
}

//将form表单数据  传到后台
function saveData(){
	var objs = "hbxxid="+$("#_form").find("input[name='hbxxid']").val();
	objs += "&ddhbrq="+$("#_form").find("input[name='hbrq']").val();
	$("#_form").find("input[name='dprxm']").each(function(i,item){
		objs += "&jpddList["+i+"].dprxm="+$(this).val();
		objs += "&jpddList["+i+"].sfzh="+$("#_form").find("input[name='sfzh']").eq(i).val();
		objs += "&jpddList["+i+"].dh="+$("#_form").find("input[name='dh']").eq(i).val();
		objs += "&jpddList["+i+"].dprxb="+$("#_form").find("input[name='dprxb0']+").val();
	})
	$.ajax({														//ajax传数据
		type : 'post',
		url : '/${projectName}/slwldx/hbxx/reshbxx.action',         
		dataType:'json',
		data : objs,
		success : function(a) {
			swal({ 
				title: '恭喜您，预定成功！', 
				text: '',
				type: "success",
				showCancelButton: false,
				showCloseButton:false,
				confirmButtonColor: '#3085d6',
				confirmButtonText: '返回', 
			}).then(function(confirm){
				if(confirm.value){
					window.location.href='/${projectName}/slwldx/hbxx/hbxx_sch.jsp';
				}else{
					window.location.href='/${projectName}/slwldx/hbxx/hbxx_sch.jsp';
				}
			}) 
		},
	});
}

function ok(){
	$("#addPassengers").html("");//每次点击订票清除上次添加乘客信息
	$("#top_pop").html("");
	$("#centre_pop").html("");
	var calendar = $("#hbrq").val();
	var jg = $("#jg").val();
	$.ajax({
		url : "/${projectName}/slwldx/hbxx/schHbxx.action",
		dataType : "json",
		success : function(data) {
			data = data.rows;
			for (var i = 0; i < data.length; i++) {
				var jg = Math.floor((Math.random() * 20 + 80)* (data[i].jg) * 0.01);//取整
				if (data[i].id == $("#hbxxid").val()) {    
					$('flight-info').append("<div class='flight-city'>"
							+ "<div class='flt-date'>10-17 &nbsp;<span class='week'> 周二</span></div>"
							+ "<div class='flt-depart '>北京</div>&nbsp;"
							+ "<div class='flt-arrow'>"
							+ "<i class='ico-arrow'></i>"
							+ "	</div>&nbsp;"
							+ "	<div class='flt-arrive '>上海</div>"
							+ "</div>"
							+ "<div class='flight-tit'>"
							+ "<span class='flt-airline'>"
							+ "	<img alt='' height='16' width='16' src=''> 南方航空 CZ6412"
							+ "</span>"
							+ "<span class='plane-type'>空中客车 A321</span>&nbsp;&nbsp;"
							+ "<span class='flt-seat'>经济舱</span>"
							+ "	</div>");
					$("#centre_pop").append(
						"<div class='flt-depart'><span class='time'>"
							+ data[i].qfsj
							+ "</span> <span class='airport'>"
							+ data[i].qfjc
							+ "</span></div><div class='flt-arrow flt-non-stop'><div class='timeline'><i class='ico-airport'></i> <i class='dot'></i> <i class='dot dot-end'></i></div></div><div class='flt-arrive'><span class='time'>"
							+ data[i].ddsj
							+ "</span><div id='arriveTime_1_1' style='display: none'>到达时间为第2天，2017年10月17日08时40分</div><span class='airport'>"
							+ data[i].ddjc
							+ "</span></div><div class='flt-config'><span>机场建设费</span><span><i class='icon-money'></i>50</span><span>燃油费</span><span>免费</span></div><span class='money'> <i class='icon-money'></i>"
							+ jg             		
							+ "</span>");
				}
			}
			$(this).addClass("done");
		}
	});
}

</script>
</head>
<body>
	<div class="wrap"> 
		<jsp:include page="../../book/hy_header.jsp" />

		<div class="main">
			<!-- 购票弹层 -->
			<div class="inner clearfix" id="buybox">
				<div class="pull-left">
					<ul class="hint">
						<li>目前该仓仅剩下3个座位，请尽快完成预订。</li>
						<li>您预定的产品仅限预定成人、儿童票，不可使用港澳通行证预定。</li>
						<li>航班预计最晚于起飞前150分钟完成出票，保障出行；</li>
					</ul>
										
					<form id="_form" method="post" action="/${projectName}/slwldx/hbxx/reshbxx.action">
						<div class="passenger">
							<h5>乘客信息</h5>
							<input type="hidden" id="hbxxid" name="hbxxid" value="${param.hbxxId}">
							<input type="hidden" id="hbrq" name="hbrq" value="${param.calendar}"> 
							<input type="hidden" id="jg" name="jg" value="${param.money}"> 
							<div class="flight-detail">
								<ul>
									<li><label>姓名</label> <input type="text" name="dprxm"	class="required dprxm" placeholder="请与登记证件姓名保持一致" /></li>
									<li><label>身份证</label> <input type="text" name="sfzh"	class="required sfzh" placeholder="登机证件号码" /></li>
									<li><label>中国大陆区号86</label> <input type="text" name="dh"	class="required dh" placeholder="乘机手机号，用来接收航变信息" /></li>
									<li><label>性别</label> <input type="radio" value="0" name="dprxb0" id="man" checked /><label for="man">男</label>&emsp;&emsp;
									<input type="radio" value="1" name="dprxb0" id="woman" /><label for="woman">女</label></li>
								</ul>
							</div>
						</div>
						
						<div id="addPassengers"></div>
						<input value="订票" class="submit" onclick="saveData()" type="button" />
					</form>
				<button class="addbtn" onclick="addPassengers();">+ 添加乘客</button>
			</div>
			
			<div class="pull-right">
				<div class="flight-info" id ="flight-info"></div>
				<div class="flight-detail" id="centre_pop"></div>
			</div>
		</div>
	</div>
</div>

<jsp:include page="../../book/_footer.jsp" />
	
<!--购票成功弹层 -->
<div class="pop" id="success">
	<div class="pop-con">
		<dl>
			<dt>
				<img src="img/success.png">
			</dt>
			<dd>
				订票成功 <span>还剩3秒自动跳转......</span>
			</dd>
		</dl>
	</div>
</div>
</body>
</html>
