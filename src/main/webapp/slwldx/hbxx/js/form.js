$(function () {

	//表单验证
	initCardTypeFrmValidate();

	//表单验证
	function initCardTypeFrmValidate() {
		$("#_form").validate({
			rules: {
				//姓名
				dprxm: "required",
				//身份证
				sfzh: {
					required: true,
					sfzh: true
				},
				//手机号
				dh: {
					required: true,
					dh: true
				}
			},
			messages: {
				//姓名
				dprxm: "姓名不能为空",
				//身份证
				sfzh: {
					required: "身份证不能为空",
					sfzh: "身份证输入错误"
				},
				//手机号
				dh: {
					required: "手机号码不能为空",
					dh: "手机号码输入错误"
				}
			}
		});
		
		//航班搜索
		$("#searchBox").validate({
			rules: {
				//出发城市
				cfcs: "required",
				//到达城市
				ddcs: "required",
				//出发日期
				rq: {
					required: true,
					rqReg: true
				}
			},
			messages: {
				//出发城市
				cfcs: "请选择出发城市",
				//到达城市
				ddcs: {
					required: "请选择到达城市"
				},
				//出发日期
				rq: {
					required: "请选择出发日期"
				}
			}
		});

		//身份證
		$.validator.addMethod("sfzh", function (value, element, params) {
			var sfzh = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
			return this.optional(element) || (sfzh.test(value));
		}, "身份证输入错误");
		
		//手机号码
		$.validator.addMethod("dh", function (value, element, params) {
			var dh = /^1[34578]\d{9}$/;
			return this.optional(element) || (dh.test(value));
		}, "手机号码输入错误");
		
		//日期
		$.validator.addMethod("rqReg", function (value, element, params) {
			var rqReg = /\d{4}-\d{2}-\d{2}/;
			return this.optional(element) || (rqReg.test(value));
		}, "日期输入错误");
	}
	
	
});
