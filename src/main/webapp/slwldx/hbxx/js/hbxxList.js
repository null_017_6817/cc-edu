//初始化方法
$(function() {
	search(1);
	dataticket();
})

//搜索栏的标题的模糊查询
function search(pnum) {
	$("#grtcylxxList").html('');
	if (pnum == null || pnum == "undefined") {
		pnum = 1;
	}
	$.ajax({
		url : projectName,
		type : "post",
		data : {
			"ddh" : $("input[name='ddh']").val(),
			"dprxm" : $("input[name='dprxm']").val(),
			"dpsj" : $("input[name='dpsj']").val(),
			"page" : pnum
		},
		dataType : "json",
		success : function(data) {
			getPage(pnum, data);
		}
	});
}

function getPage(pn, data) {
	var dataCount = data.total;//总数据条数
	var pageSize = data.pageSize;//每页显示条数
	data = data.rows;
	var pageCount = Math.ceil(dataCount / pageSize);//总页数
	if (pn == 0 || pn > pageCount) {
		return;
	}
	var ul = $(".listul");
	ul.empty();
	paintPage(pageCount, pn); //绘制页码
	var startPage = pageSize * (pn - 1);
	for (var j = 0; j < data.length; j++) {
		var html = "<tr><td>"
				+ data[j].ddh
				+ "</td><td>"
				+ data[j].hbh
				+ "</td><td>"
				+ data[j].hbrq
				+ "</td><td>"
				+ data[j].dprxm
				+ "</td><td><span id='ddprxb'>"
				+ data[j].dprxb
				+ "</span></td><td>"
				+ data[j].dpsj
				+ "</td><td>"
				+ data[j].sfzh
				+ "</td><td>"
				+ data[j].dh
				+ "</td><td dataId='" + data[j].id + "'><span onclick='refund(this)'>退票</span> <span onclick=' ticketchang(this)'>改签</span></td></tr>"
		$("#grtcylxxList").append(html);
	}
}

//绘制页码
function paintPage(number, currNum){ //number 总页数,currNum 当前页  
	var pageUl = $(".fenye");
	pageUl.empty();
	var ulDetail = "";
	if (number == 1) {
		ulDetail = "<li class=\"start\"><a href=\"javascript:void(0)\">首页</a></li>"
				+ "<li class=\"prev\"><a href=\"javascript:void(0)\">上一页</a></li>"
				+ "<li class=\"numb choose\"><a href=\"javascript:search(1)\">1</a></li>"
				+ "<li class=\"next\"><a href=\"javascript:void(0)\">下一页</a></li>"
				+ "<li class=\"end\"><a href=\"javascript:void(0)\">尾页</a></li>";
	} else if (number == 2) {
		ulDetail = "<li class=\"start\"><a href=\"javascript:search(1)\">首页</a></li>"
				+ "<li class=\"prev\"><a href=\"javascript:search(1)\">上一页</a></li>"
				+ "<li class=\"numb"
				+ choosele(currNum, 1)
				+ "\"><a href=\"javascript:search(1)\">1</a></li>"
				+ "<li class=\"numb"
				+ choosele(currNum, 2)
				+ "\"><a href=\"javascript:search(2)\">2</a></li>"
				+ "<li class=\"next\"><a href=\"javascript:search(2)\">下一页</a></li>"
				+ "<li class=\"end\"><a href=\"javascript:search(2)\">尾页</a></li>";
	} else if (number == 3) {
		ulDetail = "<li class=\"start\"><a href=\"javascript:search(1)\">首页</a></li>"
				+ "<li class=\"prev\"><a href=\"javascript:search("
				+ parseInt(currNum - 1)
				+ ")\">上一页</a></li>"
				+ "<li class=\"numb"
				+ choosele(currNum, 1)
				+ "\"><a href=\"javascript:search(1)\">1</a></li>"
				+ "<li class=\"numb"
				+ choosele(currNum, 2)
				+ "\"><a href=\"javascript:search(2)\">2</a></li>"
				+ "<li class=\"numb"
				+ choosele(currNum, 3)
				+ "\"><a href=\"javascript:search(3)\">3</a></li>"
				+ "<li class=\"next\"><a href=\"javascript:search("
				+ parseInt(currNum + 1)
				+ ")\">下一页</a></li>"
				+ "<li class=\"end\"><a href=\"javascript:search(3)\">尾页</a></li>";
	} else if (number == currNum && currNum > 3) {
		ulDetail = "<li class=\"start\"><a href=\"javascript:search(1)\">首页</a></li>"
				+ "<li class=\"prev\"><a href=\"javascript:search("
				+ parseInt(currNum - 1)
				+ ")\">上一页</a></li>"
				+ "<li class=\"numb\"><a href=\"javascript:search("
				+ parseInt(currNum - 2)
				+ ")\">"
				+ parseInt(currNum - 2)
				+ "</a></li>"
				+ "<li class=\"numb\"><a href=\"javascript:search("
				+ parseInt(currNum - 1)
				+ ")\">"
				+ parseInt(currNum - 1)
				+ "</a></li>"
				+ "<li class=\"numb choose\"><a href=\"javascript:search("
				+ currNum
				+ ")\">"
				+ currNum
				+ "</a></li>"
				+ "<li class=\"next\"><a href=\"javascript:search("
				+ currNum
				+ ")\">下一页</a></li>"
				+ "<li class=\"end\"><a href=\"javascript:search("
				+ number + ")\">尾页</a></li>";
	} else if (currNum == 1 && number > 3) {
		ulDetail = "<li class=\"start\"><a href=\"javascript:search(1)\">首页</a></li>"
				+ "<li class=\"prev\"><a href=\"javascript:void(0)\">上一页</a></li>"
				+ "<li class=\"numb choose\"><a href=\"javascript:void(0)\">1</a></li>"
				+ "<li class=\"numb\"><a href=\"javascript:search(2)\">2</a></li>"
				+ "<li class=\"numb\"><a href=\"javascript:search(3)\">3</a></li>"
				+ "<li class=\"next\"><a href=\"javascript:search(2)\">下一页</a></li>"
				+ "<li class=\"end\"><a href=\"javascript:search("
				+ number + ")\">尾页</a></li>";
	} else {
		ulDetail = "<li class=\"start\"><a href=\"javascript:search(1)\">首页</a></li>"
				+ "<li class=\"prev\"><a href=\"javascript:search("
				+ parseInt(currNum - 1)
				+ ")\">上一页</a></li>"
				+ "<li class=\"numb\"><a href=\"javascript:search("
				+ parseInt(currNum - 1)
				+ ")\">"
				+ parseInt(currNum - 1)
				+ "</a></li>"
				+ "<li class=\"numb choose\"><a href=\"javascript:search("
				+ currNum
				+ ")\">"
				+ currNum
				+ "</a></li>"
				+ "<li class=\"numb\"><a href=\"javascript:search("
				+ parseInt(currNum + 1)
				+ ")\">"
				+ parseInt(currNum + 1)
				+ "</a></li>"
				+ "<li class=\"next\"><a href=\"javascript:search("
				+ parseInt(currNum + 1)
				+ ")\">下一页</a></li>"
				+ "<li class=\"end\"><a href=\"javascript:search("
				+ number + ")\">尾页</a></li>";
	}
	$(".fenye").append(ulDetail);
}

//选择
function choosele(num, cur) {
	if (num == cur) {
		return " choose";
	} else {
		return "";
	}
}

//关闭购买套餐弹层
function returnBack() {
	$('#ticketchang').hide();
	$('.pop').hide();
}

//升舱
function upgrade(){
	if(confirm('升舱需补足差价？')==true){
		alert("升舱成功,如果不修改日期请点击'×'退出");
	}
}

//改签
function ticketchang(obj) {
	$("#totable").html("");
	var cid = $(obj).parent().attr("dataId");//获取起飞时间
	$('.pop').show();
	$('#ticketchang').show();
	$("#id").val(cid);//给当前改签的id赋值
	if($(obj).parent().parent().find("#ddprxb").html()=='男'){
		$("#dprxb").val(0);
	}
	if($(obj).parent().parent().find("#ddprxb").html()=='女'){
		$("#dprxb").val(1);
	}
	dataticket(cid);
}

//改签数据
 function dataticket(cid){
	$("#totable").html("");
	$.ajax({
		url : projectName,
		dataType : "json",
		success : function(data) {
			data = data.rows;
			for (var i = 0; i < data.length; i++) {
				if(data[i].id == cid){
					$("#totable").append("<tr><td><label>订单号：</label><input id='ddh' name='ddh' value='"+data[i].ddh+"' readonly='readonly'/></td><td><label>航班号：</label><input id='hbh' name='hbh' value='"+data[i].hbh+"' readonly='readonly'/></td></tr><tr><td><label>航班日期：</label><input type='text' name='hbrq' id='hbrq' class='flatpickr' data-min-date='today' value='"+data[i].hbrq+"' /></td><td><label>订票人姓名：</label><input id='dprxm' name='dprxm' value='"+data[i].dprxm+"' readonly='readonly'/></td></tr><tr><td><label>订票时间：</label><input id='dpsj' name='dpsj' value='"+data[i].dpsj+"' readonly='readonly'/></td><td><label>身份证号：</label><input id='sfzh' name='sfzh' value='"+data[i].sfzh+"' readonly='readonly'/></td></tr><tr><td><label>电话：</label><input id='dh' name='dh' value='"+data[i].dh+"' readonly='readonly'/></td><td><label>是否升舱：</label><input name='sc' value='' type='radio' id='yes' onclick='upgrade();' readonly='readonly'/><label for='yes'>是</label>&emsp;&emsp;<input name='sc' value='' id='no' type='radio' readonly='readonly' checked/><label for='no'>否</label></td></tr>");
				}	
			}
			$(this).addClass("done");
		}
	});
}


