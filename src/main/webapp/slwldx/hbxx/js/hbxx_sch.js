//初始化方法
$(function() {
	loadFirstList();
	/*loadSecondList();
	loadThirdList();
	loadFourthList();
	loadFifthList();
	loadSixthList();
	loadSeventhList();*/
	week();
 })

//今天的日期
function week() {
	var rq = $("input[name='rq']").val();
	if (rq != null && rq != "") {
		var show_day = new Array('星期日', '星期一', '星期二', '星期三', '星期四', '星期五',
				'星期六'), time = StringToDate(rq), year = time.getFullYear(), month = time
				.getMonth() + 1, date = time.getDate(), day = time.getDay();
		$('#today').html(year + '-' + month + '-' + date + '' + show_day[day % 7]);
		var date2 = GetDateStrs(1);
		var date3 = GetDateStrs(2);
		var date4 = GetDateStrs(3);
		var date5 = GetDateStrs(4);
		var date6 = GetDateStrs(5);
		var date7 = GetDateStrs(6);
		$('#secondDay').html(date2 + '  ' + show_day[(day + 1) % 7]);
		$('#thirdDay').html(date3 + '  ' + show_day[(day + 2) % 7]);
		$('#fourthDay').html(date4 + '  ' + show_day[(day + 3) % 7]);
		$('#fiveDay').html(date5 + '  ' + show_day[(day + 4) % 7]);
		$('#sixDay').html(date6 + '  ' + show_day[(day + 5) % 7]);
		$('#seventhDay').html(date7 + '  ' + show_day[(day + 6) % 7]);
	} else {
		var show_day = new Array('星期日', '星期一', '星期二', '星期三', '星期四', '星期五',
				'星期六'), time = new Date(), year = time.getFullYear(), month = time
				.getMonth() + 1, date = time.getDate(), day = time.getDay();
		$('#today').html(year + '-' + month + '-' + date + '' + show_day[day % 7]);
		var date2 = GetDateStr(1);
		var date3 = GetDateStr(2);
		var date4 = GetDateStr(3);
		var date5 = GetDateStr(4);
		var date6 = GetDateStr(5);
		var date7 = GetDateStr(6);
		$('#secondDay').html(date2 + '  ' + show_day[(day + 1) % 7]);
		$('#thirdDay').html(date3 + '  ' + show_day[(day + 2) % 7]);
		$('#fourthDay').html(date4 + '  ' + show_day[(day + 3) % 7]);
		$('#fiveDay').html(date5 + '  ' + show_day[(day + 4) % 7]);
		$('#sixDay').html(date6 + '  ' + show_day[(day + 5) % 7]);
		$('#seventhDay').html(date7 + '  ' + show_day[(day + 6) % 7]);
	}
}

//获取今后几天的日期方法
function GetDateStr(AddDayCount) {
	var dd = new Date();
	dd.setDate(dd.getDate() + AddDayCount);//获取AddDayCount天后的日期
	var y = dd.getFullYear();
	var m = dd.getMonth() + 1;//获取当前月份的日期
	var d = dd.getDate();
	return y + "-" + m + "-" + d;
}

//从搜索日期开始一周日期
function GetDateStrs(AddDayCount) {
	var rq = $("input[name='rq']").val();
	var dd = StringToDate(rq)
	dd.setDate(dd.getDate() + AddDayCount);//获取AddDayCount天后的日期
	var y = dd.getFullYear();
	var m = dd.getMonth() + 1;//获取当前月份的日期
	var d = dd.getDate();
	return y + "-" + m + "-" + d;
}

//字符串转换为日期  new Date();
function StringToDate(DateStr) {
	var converted = Date.parse(DateStr);
	var myDate = new Date(converted);
	if (isNaN(myDate)) {
		var arys = DateStr.split('-');
		myDate = new Date(arys[0], --arys[1], arys[2]);
	}
	return myDate;
}
var aat;
var dataList = new Array();
//第一个内容
function loadFirstList() {
	aat = art.dialog({
		opacity:0.1,
		title:"温馨提示",
		lock:true,
		content:$("#loadDiv").html(),
		});
	$("#first_list tbody").html("");
	$.ajax({
		url : projectName+"?cfcs="+$("input[name='cfcs']").val()+"&ddcs="+$("input[name='ddcs']").val(),
		dataType : "json",
		async: true,
		beforeSend:function(XMLHttpRequest){
			
		}, 
		success : function(data) {
			aat.close();
			data = data.rows;
			for (var i = 0; i < data.length; i++) {
				if ($("input[name='cfcs']").val() == data[i].cfcs && $("input[name='ddcs']").val() == data[i].ddcs) {
					var nowDate = new Date();//获取系统时间
					nowDate.setHours(0, 0, 0, 0);
					var rq = $("input[name='rq']").val();//搜索框输入时间
					var newDate = new Date(Date.parse(rq.replace(/-/g, "/")));
					if (nowDate <= newDate) {
						var jg = Math.floor((Math.random() * 20 + 80)* (data[i].jg) * 0.01);
						$("#firstm").html("<i class='icon-money'></i>" + jg);
						$("#first_list tbody").append(
							"<tr><td class=\"logo\"><div class=\"title\"><strong class=\"pubFlights_sc\">"
									+ data[i].hkgs
									+ "</strong><span>"
									+ data[i].hbh
									+ "</span></div><div class='name'>波音 737-800(中型)</div></td><td class='right'><div><strong class='time' id='qfsj'>"
									+ data[i].qfsj
									+ "</strong></div><div>"
									+ data[i].qfjc
									+ "</div></td><td class='center'><div class='arrow'></div></td> <td class='left'><div><strong class='time'>"
									+ data[i].ddsj
									+ "</strong></div><div>"
									+ data[i].ddjc
									+ "</div></td><td class='service'><div class='service-item'>准点率<br><span>"
									+ data[i].zdl
									+ "</div></td><td class='service'>"
									+ data[i].zdl
									+ "</div></td><div><span>"+data[i].discount+"<span></div><td class='price'><span class='base_price02'><i class='icon-money'></i>"
									+ data[i].jg
									+ "</span></td><td class='book'><button onclick='reshbxx(this)'>订票 <i class='icon-down'></i></button><span class='hbxxId' style='display: none'>"
									+ data[i].id
									+ "</span></td><td class='special'></td></tr>");
					} else {
						alert("请选择大于或等于今天的订票日期。");
						return;
					}
				}
	
				if ($("input[name='cfcs']").val() == "" && i < 3) {
					$("#first_list tbody").append(
						"<tr><td class=\"logo\"><div class=\"title\"><strong class=\"pubFlights_sc\">"
								+ data[i].hkgs
								+ "</strong><span>"
								+ data[i].hbh
								+ "</span></div><div class='name'>波音 737-800(中型)</div></td><td class='right'><div><strong class='time' id='qfsj'>"
								+ data[i].qfsj
								+ "</strong></div><div>"
								+ data[i].qfjc
								+ "</div></td><td class='center'><div class='arrow'></div></td> <td class='left'><div><strong class='time'>"
								+ data[i].ddsj
								+ "</strong></div><div>"
								+ data[i].ddjc
								+ "</div></td><td class='service'><div class='service-item'>准点率<br><span>"
								+ data[i].zdl
								+ "</span></div></td><td class='price'><span class='base_price02'><i class='icon-money'></i>"
								+ data[i].jg
								+ "</span></td><td class='book'><button onclick='resStyle(this)'>订票 <i class='icon-down'></i></button><span class='hbxxId' style='display: none'>"
								+ data[i].id
								+ "</span></td><td class='special'></td></tr>");
				}
			}
			$(this).addClass("done");
			dataList=data;
			loadSecondList();
			loadThirdList();
			loadFourthList();
			loadFifthList();
			loadSixthList();
			loadSeventhList();
		}
	});
}

//第二个内容
function loadSecondList() {
	$("#second_list tbody").html("");
	/*$.ajax({
		url : projectName+"?cfcs="+$("input[name='cfcs']").val()+"&ddcs="+$("input[name='ddcs']").val(),
		dataType : "json",
		success : function(data) {
			data = data.rows;*/
			var data = dataList;
			for (var i = 0; i < data.length; i++) {
				if ($("input[name='cfcs']").val() == data[i].cfcs && $("input[name='ddcs']").val() == data[i].ddcs) {
					var nowDate = new Date();//获取系统时间
					nowDate.setHours(0, 0, 0, 0);
					var rq = $("input[name='rq']").val();//搜索框输入时间
					var newDate = new Date(Date.parse(rq.replace(/-/g, "/")));
					if (nowDate <= newDate) {
					var jg = Math.floor((Math.random() * 20 + 80) * (data[i].jg) * 0.01);
					$("#secondm").html("<i class='icon-money'></i>" + jg);
					$("#second_list tbody").append(
						"<tr><td class=\"logo\"><div class=\"title\"><strong class=\"pubFlights_sc\">"
								+ data[i].hkgs
								+ "</strong><span>"
								+ data[i].hbh
								+ "</span></div><div class='name'>波音 737-800(中型)</div></td><td class='right'><div><strong class='time' id='qfsj' >"
								+ data[i].qfsj
								+ "</strong></div><div>"
								+ data[i].qfjc
								+ "</div></td><td class='center'><div class='arrow'></div></td> <td class='left'><div><strong class='time'>"
								+ data[i].ddsj
								+ "</strong></div><div>"
								+ data[i].ddjc
								+ "</div></td><td class='service'><div class='service-item'>准点率<br><span>"
								+ data[i].zdl
								+ "</span></div></td><td class='price'><span class='base_price02'><i class='icon-money'></i>"
								+ data[i].jg
								+ "</span></td><td class='book'><button onclick='reshbxx(this)'>订票 <i class='icon-down'></i></button><span style='display: none'>"
								+ data[i].id
								+ "</span></td><td class='special'></td></tr>");
				}else {
					return;
				}
				}
				if ($("input[name='cfcs']").val() == "" && i < 3) {
					$("#second_list tbody").append(
						"<tr><td class=\"logo\"><div class=\"title\"><strong class=\"pubFlights_sc\">"
								+ data[i].hkgs
								+ "</strong><span>"
								+ data[i].hbh
								+ "</span></div><div class='name'>波音 737-800(中型)</div></td><td class='right'><div><strong class='time' id='qfsj' >"
								+ data[i].qfsj
								+ "</strong></div><div>"
								+ data[i].qfjc
								+ "</div></td><td class='center'><div class='arrow'></div></td> <td class='left'><div><strong class='time'>"
								+ data[i].ddsj
								+ "</strong></div><div>"
								+ data[i].ddjc
								+ "</div></td><td class='service'><div class='service-item'>准点率<br><span>"
								+ data[i].zdl
								+ "</span></div></td><td class='price'><span class='base_price02'><i class='icon-money'></i>"
								+ data[i].jg
								+ "</span></td><td class='book'><button onclick='resStyle(this)'>订票 <i class='icon-down'></i></button><span style='display: none'>"
								+ data[i].id
								+ "</span></td><td class='special'></td></tr>");
				}
			}
			$(this).addClass("done");
		/*}
	});*/
}

//第三个内容
function loadThirdList() {
	$("#third_list tbody").html("");
	/*$.ajax({
		url : projectName+"?cfcs="+$("input[name='cfcs']").val()+"&ddcs="+$("input[name='ddcs']").val(),
		dataType : "json",
		success : function(data) {
			data = data.rows;*/
			var data = dataList;
			for (var i = 0; i < data.length; i++) {
				if ($("input[name='cfcs']").val() == data[i].cfcs && $("input[name='ddcs']").val() == data[i].ddcs) {
					var nowDate = new Date();//获取系统时间
					nowDate.setHours(0, 0, 0, 0);
					var rq = $("input[name='rq']").val();//搜索框输入时间
					var newDate = new Date(Date.parse(rq.replace(/-/g, "/")));
					if (nowDate <= newDate) {
					var jg = Math.floor((Math.random() * 20 + 80) * (data[i].jg) * 0.01);
					$("#thirdm").html("<i class='icon-money'></i>" + jg);
					$("#third_list tbody").append(
						"<tr><td class=\"logo\"><div class=\"title\"><strong class=\"pubFlights_sc\">"
								+ data[i].hkgs
								+ "</strong><span>"
								+ data[i].hbh
								+ "</span></div><div class='name'>波音 737-800(中型)</div></td><td class='right'><div><strong class='time' id='qfsj' >"
								+ data[i].qfsj
								+ "</strong></div><div>"
								+ data[i].qfjc
								+ "</div></td><td class='center'><div class='arrow'></div></td> <td class='left'><div><strong class='time'>"
								+ data[i].ddsj
								+ "</strong></div><div>"
								+ data[i].ddjc
								+ "</div></td><td class='service'><div class='service-item'>准点率<br><span>"
								+ data[i].zdl
								+ "</span></div></td><td class='price'><span class='base_price02'><i class='icon-money'></i>"
								+ data[i].jg
								+ "</span></td><td class='book'><button onclick='reshbxx(this)'>订票 <i class='icon-down'></i></button><span style='display: none'>"
								+ data[i].id
								+ "</span></td><td class='special'></td></tr>");
					}else {
						return;
					}
					}
				if ($("input[name='cfcs']").val() == "" && i < 3) {
					$("#third_list tbody").append(
						"<tr><td class=\"logo\"><div class=\"title\"><strong class=\"pubFlights_sc\">"
								+ data[i].hkgs
								+ "</strong><span>"
								+ data[i].hbh
								+ "</span></div><div class='name'>波音 737-800(中型)</div></td><td class='right'><div><strong class='time' id='qfsj' >"
								+ data[i].qfsj
								+ "</strong></div><div>"
								+ data[i].qfjc
								+ "</div></td><td class='center'><div class='arrow'></div></td> <td class='left'><div><strong class='time'>"
								+ data[i].ddsj
								+ "</strong></div><div>"
								+ data[i].ddjc
								+ "</div></td><td class='service'><div class='service-item'>准点率<br><span>"
								+ data[i].zdl
								+ "</span></div></td><td class='price'><span class='base_price02'><i class='icon-money'></i>"
								+ data[i].jg
								+ "</span></td><td class='book'><button onclick='resStyle(this)'>订票 <i class='icon-down'></i></button><span style='display: none'>"
								+ data[i].id
								+ "</span></td><td class='special'></td></tr>");
				}
			}
			$(this).addClass("done");
		/*}
	});*/
}

//第四个内容
function loadFourthList() {
	$("#fourth_list tbody").html("");
	/*$.ajax({
		url : projectName+"?cfcs="+$("input[name='cfcs']").val()+"&ddcs="+$("input[name='ddcs']").val(),
		dataType : "json",
		success : function(data) {
			data = data.rows;*/
			var data = dataList;
			for (var i = 0; i < data.length; i++) {
				if ($("input[name='cfcs']").val() == data[i].cfcs && $("input[name='ddcs']").val() == data[i].ddcs) {
					var nowDate = new Date();//获取系统时间
					nowDate.setHours(0, 0, 0, 0);
					var rq = $("input[name='rq']").val();//搜索框输入时间
					var newDate = new Date(Date.parse(rq.replace(/-/g, "/")));
					if (nowDate <= newDate) {
					var jg = Math.floor((Math.random() * 20 + 80) * (data[i].jg) * 0.01);
					$("#fourthm").html("<i class='icon-money'></i>" + jg);
					$("#fourth_list tbody").append(
						"<tr><td class=\"logo\"><div class=\"title\"><strong class=\"pubFlights_sc\">"
								+ data[i].hkgs
								+ "</strong><span>"
								+ data[i].hbh
								+ "</span></div><div class='name'>波音 737-800(中型)</div></td><td class='right'><div><strong class='time' id='qfsj' >"
								+ data[i].qfsj
								+ "</strong></div><div>"
								+ data[i].qfjc
								+ "</div></td><td class='center'><div class='arrow'></div></td> <td class='left'><div><strong class='time'>"
								+ data[i].ddsj
								+ "</strong></div><div>"
								+ data[i].ddjc
								+ "</div></td><td class='service'><div class='service-item'>准点率<br><span>"
								+ data[i].zdl
								+ "</span></div></td><td class='price'><span class='base_price02'><i class='icon-money'></i>"
								+ data[i].jg
								+ "</span></td><td class='book'><button onclick='reshbxx(this)'>订票 <i class='icon-down'></i></button><span style='display: none'>"
								+ data[i].id
								+ "</span></td><td class='special'></td></tr>");
					}else {
						return;
					}
					}
				if ($("input[name='cfcs']").val() == "" && i < 3) {
					$("#fourth_list tbody").append(
						"<tr><td class=\"logo\"><div class=\"title\"><strong class=\"pubFlights_sc\">"
								+ data[i].hkgs
								+ "</strong><span>"
								+ data[i].hbh
								+ "</span></div><div class='name'>波音 737-800(中型)</div></td><td class='right'><div><strong class='time' id='qfsj' >"
								+ data[i].qfsj
								+ "</strong></div><div>"
								+ data[i].qfjc
								+ "</div></td><td class='center'><div class='arrow'></div></td> <td class='left'><div><strong class='time'>"
								+ data[i].ddsj
								+ "</strong></div><div>"
								+ data[i].ddjc
								+ "</div></td><td class='service'><div class='service-item'>准点率<br><span>"
								+ data[i].zdl
								+ "</span></div></td><td class='price'><span class='base_price02'><i class='icon-money'></i>"
								+ data[i].jg
								+ "</span></td><td class='book'><button onclick='resStyle(this)'>订票 <i class='icon-down'></i></button><span style='display: none'>"
								+ data[i].id
								+ "</span></td><td class='special'></td></tr>");
				}
			}
			$(this).addClass("done");
		/*}
	});*/
}
//第五个内容
function loadFifthList() {
	$("#fifth_list tbody").html("");
	/*$.ajax({
		url : projectName+"?cfcs="+$("input[name='cfcs']").val()+"&ddcs="+$("input[name='ddcs']").val(),
		dataType : "json",
		success : function(data) {
			data = data.rows;*/
			var data = dataList;
			for (var i = 0; i < data.length; i++) {
				if ($("input[name='cfcs']").val() == data[i].cfcs && $("input[name='ddcs']").val() == data[i].ddcs) {
					var nowDate = new Date();//获取系统时间
					nowDate.setHours(0, 0, 0, 0);
					var rq = $("input[name='rq']").val();//搜索框输入时间
					var newDate = new Date(Date.parse(rq.replace(/-/g, "/")));
					if (nowDate <= newDate) {
						var jg = Math.floor((Math.random() * 20 + 80) * (data[i].jg) * 0.01);
						$("#fifthm").html("<i class='icon-money'></i>" + jg);
						$("#fifth_list tbody").append(
							"<tr><td class=\"logo\"><div class=\"title\"><strong class=\"pubFlights_sc\">"
								+ data[i].hkgs
								+ "</strong><span>"
								+ data[i].hbh
								+ "</span></div><div class='name'>波音 737-800(中型)</div></td><td class='right'><div><strong class='time' id='qfsj' >"
								+ data[i].qfsj
								+ "</strong></div><div>"
								+ data[i].qfjc
								+ "</div></td><td class='center'><div class='arrow'></div></td> <td class='left'><div><strong class='time'>"
								+ data[i].ddsj
								+ "</strong></div><div>"
								+ data[i].ddjc
								+ "</div></td><td class='service'><div class='service-item'>准点率<br><span>"
								+ data[i].zdl
								+ "</span></div></td><td class='price'><span class='base_price02'><i class='icon-money'></i>"
								+ data[i].jg
								+ "</span></td><td class='book'><button onclick='reshbxx(this)'>订票 <i class='icon-down'></i></button><span style='display: none'>"
								+ data[i].id
								+ "</span></td><td class='special'></td></tr>");
						}else {
					return;
				}
			}
			if ($("input[name='cfcs']").val() == "" && i < 3) {
				$("#fifth_list tbody").append(
					"<tr><td class=\"logo\"><div class=\"title\"><strong class=\"pubFlights_sc\">"
						+ data[i].hkgs
						+ "</strong><span>"
						+ data[i].hbh
						+ "</span></div><div class='name'>波音 737-800(中型)</div></td><td class='right'><div><strong class='time' id='qfsj' >"
						+ data[i].qfsj
						+ "</strong></div><div>"
						+ data[i].qfjc
						+ "</div></td><td class='center'><div class='arrow'></div></td> <td class='left'><div><strong class='time'>"
						+ data[i].ddsj
						+ "</strong></div><div>"
						+ data[i].ddjc
						+ "</div></td><td class='service'><div class='service-item'>准点率<br><span>"
						+ data[i].zdl
						+ "</span></div></td><td class='price'><span class='base_price02'><i class='icon-money'></i>"
						+ data[i].jg
						+ "</span></td><td class='book'><button onclick='resStyle(this)'>订票 <i class='icon-down'></i></button><span style='display: none'>"
						+ data[i].id
						+ "</span></td><td class='special'></td></tr>");
			}
			}
			$(this).addClass("done");
		/*}
	});*/
}
//第六个内容
function loadSixthList() {
	$("#sixth_list tbody").html("");
	/*$.ajax({
		url : projectName+"?cfcs="+$("input[name='cfcs']").val()+"&ddcs="+$("input[name='ddcs']").val(),
		dataType : "json",
		success : function(data) {
			data = data.rows;*/
			var data = dataList;
			for (var i = 0; i < data.length; i++) {
				if ($("input[name='cfcs']").val() == data[i].cfcs && $("input[name='ddcs']").val() == data[i].ddcs) {
					var nowDate = new Date();//获取系统时间
					nowDate.setHours(0, 0, 0, 0);
					var rq = $("input[name='rq']").val();//搜索框输入时间
					var newDate = new Date(Date.parse(rq.replace(/-/g, "/")));
					if (nowDate <= newDate) {
					var jg = Math.floor((Math.random() * 20 + 80) * (data[i].jg) * 0.01);
					$("#sixthm").html("<i class='icon-money'></i>" + jg);
					$("#sixth_list tbody").append(
						"<tr><td class=\"logo\"><div class=\"title\"><strong class=\"pubFlights_sc\">"
							+ data[i].hkgs
							+ "</strong><span>"
							+ data[i].hbh
							+ "</span></div><div class='name'>波音 737-800(中型)</div></td><td class='right'><div><strong class='time' id='qfsj' >"
							+ data[i].qfsj
							+ "</strong></div><div>"
							+ data[i].qfjc
							+ "</div></td><td class='center'><div class='arrow'></div></td> <td class='left'><div><strong class='time'>"
							+ data[i].ddsj
							+ "</strong></div><div>"
							+ data[i].ddjc
							+ "</div></td><td class='service'><div class='service-item'>准点率<br><span>"
							+ data[i].zdl
							+ "</span></div></td><td class='price'><span class='base_price02'><i class='icon-money'></i>"
							+ data[i].jg
							+ "</span></td><td class='book'><button onclick='reshbxx(this)'>订票 <i class='icon-down'></i></button><span style='display: none'>"
							+ data[i].id
							+ "</span></td><td class='special'></td></tr>");
					}else {
						return;
					}
				}
				if ($("input[name='cfcs']").val() == "" && i < 3) {
					$("#sixth_list tbody").append(
						"<tr><td class=\"logo\"><div class=\"title\"><strong class=\"pubFlights_sc\">"
							+ data[i].hkgs
							+ "</strong><span>"
							+ data[i].hbh
							+ "</span></div><div class='name'>波音 737-800(中型)</div></td><td class='right'><div><strong class='time' id='qfsj' >"
							+ data[i].qfsj
							+ "</strong></div><div>"
							+ data[i].qfjc
							+ "</div></td><td class='center'><div class='arrow'></div></td> <td class='left'><div><strong class='time'>"
							+ data[i].ddsj
							+ "</strong></div><div>"
							+ data[i].ddjc
							+ "</div></td><td class='service'><div class='service-item'>准点率<br><span>"
							+ data[i].zdl
							+ "</span></div></td><td class='price'><span class='base_price02'><i class='icon-money'></i>"
							+ data[i].jg
							+ "</span></td><td class='book'><button onclick='resStyle(this)'>订票 <i class='icon-down'></i></button><span style='display: none'>"
							+ data[i].id
							+ "</span></td><td class='special'></td></tr>");
				}
			}
			$(this).addClass("done");
		/*}
	});*/
}
//第七个内容
function loadSeventhList() {
	$("#seventh_list tbody").html("");
	/*$.ajax({
		url : projectName+"?cfcs="+$("input[name='cfcs']").val()+"&ddcs="+$("input[name='ddcs']").val(),
		dataType : "json",
		success : function(data) {
			data = data.rows;*/
			var data = dataList;
			for (var i = 0; i < data.length; i++) {
				if ($("input[name='cfcs']").val() == data[i].cfcs && $("input[name='ddcs']").val() == data[i].ddcs) {
					var nowDate = new Date();//获取系统时间
					nowDate.setHours(0, 0, 0, 0);
					var rq = $("input[name='rq']").val();//搜索框输入时间
					var newDate = new Date(Date.parse(rq.replace(/-/g, "/")));
					if (nowDate <= newDate) {
					var jg = Math.floor((Math.random() * 20 + 80) * (data[i].jg) * 0.01);
					$("#seventhm").html("<i class='icon-money'></i>" + jg);
					$("#seventh_list tbody").append(
						"<tr><td class=\"logo\"><div class=\"title\"><strong class=\"pubFlights_sc\">"
							+ data[i].hkgs
							+ "</strong><span>"
							+ data[i].hbh
							+ "</span></div><div class='name'>波音 737-800(中型)</div></td><td class='right'><div><strong class='time' id='qfsj' >"
							+ data[i].qfsj
							+ "</strong></div><div>"
							+ data[i].qfjc
							+ "</div></td><td class='center'><div class='arrow'></div></td> <td class='left'><div><strong class='time'>"
							+ data[i].ddsj
							+ "</strong></div><div>"
							+ data[i].ddjc
							+ "</div></td><td class='service'><div class='service-item'>准点率<br><span>"
							+ data[i].zdl
							+ "</span></div></td><td class='price'><span class='base_price02'><i class='icon-money'></i>"
							+ data[i].jg
							+ "</span></td><td class='book'><button onclick='reshbxx(this)'>订票 <i class='icon-down'></i></button><span style='display: none'>"
							+ data[i].id
							+ "</span></td><td class='special'></td></tr>");
				}else {
					return;
				}
			}
			if ($("input[name='cfcs']").val() == "" && i < 3) {
				$("#seventh_list tbody").append(
					"<tr><td class=\"logo\"><div class=\"title\"><strong class=\"pubFlights_sc\">"
						+ data[i].hkgs
						+ "</strong><span>"
						+ data[i].hbh
						+ "</span></div><div class='name'>波音 737-800(中型)</div></td><td class='right'><div><strong class='time' id='qfsj' >"
						+ data[i].qfsj
						+ "</strong></div><div>"
						+ data[i].qfjc
						+ "</div></td><td class='center'><div class='arrow'></div></td> <td class='left'><div><strong class='time'>"
						+ data[i].ddsj
						+ "</strong></div><div>"
						+ data[i].ddjc
						+ "</div></td><td class='service'><div class='service-item'>准点率<br><span>"
						+ data[i].zdl
						+ "</span></div></td><td class='price'><span class='base_price02'><i class='icon-money'></i>"
						+ data[i].jg
						+ "</span></td><td class='book'><button onclick='resStyle(this)'>订票 <i class='icon-down'></i></button><span style='display: none'>"
						+ data[i].id
						+ "</span></td><td class='special'></td></tr>");
			}
		}
		$(this).addClass("done");
		/*}
	});*/
}

function resStyle(obj){
	alert("请选择出发城市、到达城市和出发日期。");
}

//点击搜索 查询  重新加载页面
function doSearch() {
	if ($('#searchBox').valid()) {
		$(function() {
			loadFirstList();
			/*loadSecondList();
			loadThirdList();
			loadFourthList();
			loadFifthList();
			loadSixthList();
			loadSeventhList();*/
			week();
		})
	}
	return false;//永远禁止页面表单提交
}

//关闭订票
function returnBack() {
	$(".pop").css({display : "none"});
}
	
//提交表单
$('.submit').click(function () {
	if ($('#_form').valid()) {
		//通过执行的动作
		$('#buy').hide();
		$("#success").show();
		//两秒后关闭
		setTimeout(function(){//两秒后跳转  
			$("#success").hide();
        },2000);  
	}
	//return false;//永远禁止页面表单提交
});
