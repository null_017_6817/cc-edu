<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>查看航班信息</title><!--hbxx-->
	<%@include file="/common/include.jsp" %>
<script>
function load(id){
	$('#_form').form('load','/${projectName}/slwldx/hbxx/vieHbxx.action?toL=true&id='+id);
};
</script>
</head>
<body>
<form id="_form">
	<input type="hidden" name='id' value=""/>
	<table>
		<tr><td class='label'>航空公司：</td><td><input type='text' name='hkgs' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>航班号：</td><td><input type='text' name='hbh' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>起飞时间：</td><td><input type='text' name='qfsj' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>到达时间：</td><td><input type='text' name='ddsj' class='viewbox' readonly='readonly'/></td></tr>
		<!-- <tr><td class='label'>日期：</td><td><input type='text' name='rq' class='viewbox' readonly='readonly'/></td></tr> -->
		<tr><td class='label'>出发城市：</td><td><input type='text' name='cfcs' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>达到城市：</td><td><input type='text' name='ddcs' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>起飞机场：</td><td><input type='text' name='qfjc' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>抵达机场：</td><td><input type='text' name='ddjc' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>价格(元)：</td><td><input type='text' name='jg' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>准点率(%)：</td><td><input type='text' name='zdl' class='viewbox' readonly='readonly' style="width:20px"/>%</td></tr>
	</table>

</form>
</body>
</html>
