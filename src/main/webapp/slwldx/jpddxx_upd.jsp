<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>修改机票订单信息</title><!--jpddxx-->
	<%@include file="/common/include.jsp" %>
<script>
<%-- 查询页点击修改时调用此方法加载记录详情 --%>
function load(id){
	$('#_form').form('load','/${projectName}/slwldx/jpddxx/vieJpddxx.action?id='+id);
};

<%-- 点击保存按钮时调用此方法。(注:保存按钮在查询列表页) --%>
function submitForm(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#_form').form('submit',{
      success:function(data) {
      	parent.reloadData();
      }
   });
}

</script>
</head>
<body>
<form id="_form" method="post" action="/${projectName}/slwldx/jpddxx/updJpddxx.action">
	<input type="hidden" name='id' id='id'/>
	<table>
		<tr><td class='label' style="width:120px">订单号：</td><td><input id="ddh" name="ddh" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >航班号：</td><td><input id="hbh" name="hbh" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >航班日期：</td><td><input id="hbrq" name="hbrq" class="easyui-datebox" data-options="required:true"/></td></tr>
		<tr><td class='label' >订票人姓名：</td><td><input id="dprxm" name="dprxm" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >订票人性别：</td><td><dc:insertList name="dprxb" dictId="ZD_XB" style="combo_normal" /></td></tr>
		<!-- <tr><td class='label' >订票时间：</td><td><input id="dpsj" name="dpsj" class="easyui-datetimebox" data-options="required:true"/></td></tr> -->
		<tr><td class='label' >身份证号：</td><td><input id="sfzh" name="sfzh" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >电话：</td><td><input id="dh" name="dh" class="easyui-textbox" data-options="required:true,validType:['tel']"/></td></tr>
		<%-- <tr><td class='label' >订单状态：</td><td><dc:insertList name="ddzt" dictId="CCQ_JPDDZT" style="combo_normal" /></td></tr> --%>
	</table>

</form>
</body>
</html>
