<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>呼叫中心-物流搜索</title>

    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/slwldx/wlxx/css/normalize.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/slwldx/wlxx/css/fonts.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/slwldx/wlxx/css/style.css"/>

    <script type="text/javascript" src="${pageContext.request.contextPath }/slwldx/wlxx/js/lib/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/slwldx/wlxx/js/logistics.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/slwldx/wlxx/js/tab.js"></script>

    <script type="text/javascript">
 		var wlgsPath = "${pageContext.request.contextPath}/slwldx/web/wlxx/schWlgs.action";
 		var wlddxxByDdhPath = "${pageContext.request.contextPath}/slwldx/web/wlddxx/schWlddxxByDdh.action";
 		var wlddxxByPhonePath = "${pageContext.request.contextPath}/slwldx/web/wlddxx/schWlddxxByPhone.action";
 		var schPullPath = "${pageContext.request.contextPath}/slwldx/web/wlxx/schPull.action";
 	</script>
</head>

<body>
<div class="wrap">
    <jsp:include page="../../book/hy_header.jsp" />

    <div class="main">
        <div class="inner">
            <!-- 选项切换 -->
            <ul class="tabNav clearfix" id="oranger">
                <li class="active">搜索单号</li>
                <li>搜索手机号</li>
                <div class="logistics">
                    <label>物流公司：</label>
                    <input type="text" name="ReturnDate" id="logistics" value="" />
                    <div class="logisticsPop">
                        <span title="关闭" class="close">×</span>
                        <span class="text">支持中文输入</span>
                        <div class="logisticsList"></div>
                    </div>
                </div>
            </ul>

            <!-- 搜索结果 -->
            <div class="resultBox">
                <div class="resultCon">
                    <form>
                        <div class="item">
                            <input id="wlddh" type="text" name="ArrivalCity" value="" placeholder="请输入快递单号" />
                        </div>
                        <div class="item btn">
                            <i class="icon-search"></i>
                            <input type="button" value="搜索" onclick="submitDdhForm()"/>
                            <tbody id="content_table_body">
                     			<!-- 动态数据在这里显示 -->
                			 </tbody> 
                        </div>
                    </form>

                    <ul class="title width">
                        <li><i class="icon-logistics"></i>物流公司</li>
                        <li><i class="icon-code"></i>订单号</li>
                        <li><i class="icon-time"></i>发件时间</li>
                        <li><i class="icon-user"></i>寄件人</li>
                        <li><i class="icon-user"></i>收件人</li>
                        <li><i class="icon-phone2"></i>寄件人电话</li>
                        <li><i class="icon-phone2"></i>收件人电话</li>
                        <!-- <li><i class="icon-city"></i>始发城市</li>
                        <li><i class="icon-city"></i>目的城市</li> -->
                        <li><i class="icon-city"></i>物流轨迹</li>
                    </ul>

                    <ul id="wlddxxByDdh_info" class="list width">
                        
                    </ul>
                </div>
                <div class="resultCon">
                    <form>
                        <div class="item">
                            <input id="phoneNum" type="text" size="50" name="ArrivalCity" value="" placeholder="请输入手机号码" />
                        </div>
                        <div class="item btn">
                            <i class="icon-search"></i>
                            <input type="button" value="搜索" onclick="submitPnumForm()"/>
                        </div>
                    </form>

                    <ul class="title width">
                        <li><i class="icon-logistics"></i>物流公司</li>
                        <li><i class="icon-code"></i>订单号</li>
                        <li><i class="icon-time"></i>发件时间</li>
                        <li><i class="icon-user"></i>寄件人</li>
                        <li><i class="icon-user"></i>收件人</li>
                        <li><i class="icon-phone2"></i>寄件人电话</li>
                        <li><i class="icon-phone2"></i>收件人电话</li>
                        <!-- <li><i class="icon-city"></i>始发城市</li>
                        <li><i class="icon-city"></i>目的城市</li> -->
                        <li><i class="icon-city"></i>物流轨迹</li>
                    </ul>

                    <ul id="wlddxxByPhone_info" class="list width">
                    </ul>
                </div>
            </div>
        </div>
    </div>

   <jsp:include page="../../book/_footer.jsp" />

    <!-- 弹层 -->
    <div class="pop">
        <div class="pop-con">
            <span class="close">×</span>
            <div class="title clearfix">
                <img src="${pageContext.request.contextPath }/slwldx/wlxx/img/success.png" width=20px height=90px alt="商品图" />
                <p>
                    <span>快递已签收</span>
                    <span id="wllzxx_ddh"></span>
                </p>
            </div> 
            <div class="line">
                <ul  id="wllzxx_info">
                </ul>
            </div>
        </div>
    </div>
</div>
</body>
</html>
