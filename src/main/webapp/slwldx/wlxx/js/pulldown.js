function insertContent(content) {
	clearContent();
	setLocation();
	var json = JSON.parse(content);
	var len = json.length;
	for (var i = 0; i < len; i++) {
		var value = json[i];
		var tr = document.createElement("tr");
		var td = document.createElement("td");
		td.setAttribute("bgcolor", "#FFFAFA");
		td.setAttribute("border", "0");
		td.onmouseover = function() {
			this.className = 'mouseOver';
		};
		td.onmouseout = function() {
			this.className = 'mouseOut';
		};
		td.onclick = function() {
			// document.getElementById("keyword").value=this.;
		};
		var text = document.createTextNode(value);
		td.appendChild(text);
		tr.appendChild(td);
		document.getElementById("content_table_body").appendChild(tr);
	}
}

function clearContent() {
	var popNode = document.getElementById("popDiv");
	popNode.style.border = "none";
	var contentNode = document.getElementById("content_table_body");
	var len = contentNode.childNodes.length;
	for (var i = len - 1; i >= 0; i--) {
		contentNode.removeChild(contentNode.childNodes[i]);
	}
}

function setLocation(){
	var inputNode = document.getElementById("keyword");
	var width = inputNode.offsetWidth;
	var left = inputNode["offsetLeft"];
	var top = inputNode.offsetHeight+inputNode["offsetTop"];
	var popNode = document.getElementById("popDiv");
	popNode.style.border = "gray 0.5px solid";
	popNode.style.width = width+"px";
	popNode.style.top = top+"px";
	popNode.style.left = left+"px";
	document.getElementById("content_table").style.width=width+"px";
}