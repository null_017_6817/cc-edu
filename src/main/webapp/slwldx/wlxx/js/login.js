$(function(){  
	//显示登录弹层
	$('.login').click(function () {
		$('#loginBox').show();
		$('body').css('overflow', 'hidden');
	});
	
	//关闭登录弹层
	$('.login-con span').click(function () {
		$(this).parent('div').parent('div').hide();
		$('body').css('overflow', 'auto');
	});
	
	$("#loginBtn").click(function(){
		
		userLogin();
	});
	
	$("#changePwBtn").click(function(){
		changePw();
	});	
});


// 学生,老师登录
function userLogin(){
	
	var number = $("#login_Form").find("input[name='number']").val();
	var password = $("#login_Form").find("input[name='password']").val();
	if(number == "" || password == ""){
		
		alert(123);
		return false;
	}
	
	var param = $("#login_Form").serialize();
	
	$.ajax({
		type : "POST",
		url : '/'+projectName+'/sysmanage/login/bookLogin.action',
		data : param,	
		dataType:'json',
		success : function(result) {
			if(result.success){
				var person = result.person;
				if(person != null){
					var manageUrl = ""
					if(person.state != "3"){
						manageUrl = "<li><a href='javascript:toManager();'>后台管理</a></li>" 
					}
					
					var htm = "<span class='icon-exam'></span><span class='icon-phone'></span><div class='user'><span class=' icon-logins'></span><i class='icon-down'></i>"
						+"<ul>" 
						+"<li><a href=\"/"+projectName+"/sysmanage/logout/bookLogout.action\">注销</a></li>"
						+"<li><a href='javascript:showChangePw();'>修改密码</a></li>" 
						+manageUrl
						+"</ul></div>";				
					
					$(".loginDiv").html(htm);
					
					//显示user
					$('.loginDiv .user').hover(function () {
						$('.loginDiv ul').css({'opacity':'1', 'visibility':'visible'});
					}, function () {
						$('.loginDiv ul').css({'opacity':'0', 'visibility':'hidden'});
					});
					
					$('.login-con span').trigger('click');
					// 如果为实训营地页面则触发判断里的方法
					if(typeof(isSXYD) != "undefined" && isSXYD){
						$("input[name='currUser']").val(person.agentId);
						doChange(person.state);
						showAnswer(person.state,person);
					}
					
					// 如果为训练题库则刷新页面中iframe部分
					if(typeof(isTest) != "undefined" && isTest){
						iframe.window.location.reload()
					}
					
					if(typeof(mainJsp) != "undefined" && mainJsp){
						showExam(person.state);
					}
				}
			}else{
				swal({ 
					title: result.msg, 
					type: "error"});
			}
		}
	});
}

// 显示修改密码的弹出框
function showChangePw(){
	$('#changePw').find("input[type='password']").val('');
	$('#changePw').show();
	$('body').css('overflow', 'hidden');
}

// 修改密码
function changePw(){
	
	var param = $("#changePw_Form").serialize();
	
	$.ajax({
		type : "POST",
		url : '/'+projectName+'/sysmanage/changePw/changePw.action',
		data : param,	
		success : function(result) {
			if(result.success){
				$('#changePw').find('.login-con span').trigger('click');
				swal({ 
					title: "修改成功", 
					type: "success"});
			}else{
				swal({ 
					title: result.msg, 
					type: "error"});
			}
		}
	});
}

function newAlert(msg,type){
	swal({ title: msg,
		type: type});
}

function toManager(){
	window.open('/'+projectName+'/sysmanage/login/page.action?page=main');
}

document.onkeydown=function(event){
	var e = event || window.event || arguments.callee.caller.arguments[0];
	
	if(e && e.keyCode==13){ // enter 键
		//要做的事情
		if($('#loginBox').css('display') == "block"){
			userLogin();
		}
		if($('#changePw').css('display') == "block"){
			changePw();
		}
	}
};

