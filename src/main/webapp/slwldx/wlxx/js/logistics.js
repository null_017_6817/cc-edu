$(function(){
	//显示物流
	function SeLogistics () {
		$.ajax({
			   async: false,
			   type: "POST",
			   url: wlgsPath,
			   dataType: "json",
			   success: function (data) {
				   //console.log(data[5]);
				   $('.logisticsList').html('');
					for (var i = 0; i < data.length; i++) {
						if("华唐速递" == data[i]){
							var html = '<span>' + data[i] + '</span>';
							$('.logisticsList').append(html);
						}	
					}

					$('.logisticsList span').click(function () {
						var spanHtml = $(this).html();
						$('#logistics').val(spanHtml);
						$('.logisticsPop').hide();
					});
			   }
		   });
		//关闭弹层
		$('.logisticsPop .close').click(function () {
			$('.logisticsPop').hide();
		});
	}
	//显示弹层
	$('#logistics').click(function (e) {
		e.stopPropagation();  //去除默认事件

		$('.logisticsPop').show();
		SeLogistics(this,e);
	});
	//弹层本身去除默认事件
	$('.logisticsPop').click(function (e) {
		e.stopPropagation();  //去除默认事件
	});

	$(document).click(function () {
		$('.logisticsPop').hide();
	});
});

//wlzfxx_sch.jsp发送根据订单号查询ajax的请求
function submitDdhForm(){
		var randomdate = getRandom();//加载随机时间
		$("#wlddxxByDdh_info").empty();
		 var wlddxxTemp="<li>"+"<span id='wlddxxByDdh_wlgs'>"+"</span>"
						  +"<span id='wlddxxByDdh_ddh'>"+"</span>"
						  +"<span id='wlddxxByDdh_ddjg'>"+"</span>"
					      +"<span id='wlddxxByDdh_jjr'>"+"</span>"
						  +"<span id='wlddxxByDdh_sjr'>"+"</span>"
						  +"<span id='wlddxxByDdh_jjrdh'>"+"</span>"
						  +"<span id='wlddxxByDdh_sjrdh'>"+"</span>"
						/*  +"<span id='wlddxxByDdh_sfcs'>"+"</span>"*/
						/*  +"<span id='wlddxxByDdh_mdcs'>"+"</span>"*/
						  +"<span id='wlddxxByDdh_wlgj' class='detailBtn'>"+"</span>"
					+"</li>";
       $("#wlddxxByDdh_info").append(wlddxxTemp);
		if($('#logistics').val() != "" && $('#wlddh').val() != ""){
			 $.ajax({
		        type : 'post',
		        url : wlddxxByDdhPath,
		        dataType : 'json',
		        data : {
		        	'toL' : 'true',
		            'ddh' : $('#wlddh').val(),
		            'wlgs' : $('#logistics').val()
		        },
		        success : function(data) {
		        		if(data.state == "success"){
				  			$("#wlddxxByDdh_wlgs").html(data.wlddxx.wlgs);
				  			$("#wlddxxByDdh_ddh").html(data.wlddxx.ddh);
				  			$("#wlddxxByDdh_ddjg").html(randomdate);
				  			$("#wlddxxByDdh_jjr").html(data.wlddxx.jjr);
				  			$("#wlddxxByDdh_sjr").html(data.wlddxx.sjr);
				  			$("#wlddxxByDdh_jjrdh").html(data.wlddxx.jjrdh);
				  			$("#wlddxxByDdh_sjrdh").html(data.wlddxx.sjrdh);
				  			/*$("#wlddxxByDdh_sfcs").html(data.wlddxx.sfcs);
				  			$("#wlddxxByDdh_mdcs").html(data.wlddxx.mdcs);*/
				  			var xqStr ="详情";
				  			$("#wlddxxByDdh_wlgj").text(xqStr);
				  			$("#wllzxx_ddh").html("运单号码：" + data.wlddxx.ddh);
				  			$("#wllzxx_info").empty();
				  			var connectStr = "<span class='border-left'></span>";
			  				$("#wllzxx_info").append(connectStr);
			  				for(var i = 0; i< data.wllzxx.length;i++){
			  					var dd = data.wllzxx[i];
			  					var sdsj = dd.sdsj;
			  					var dqdz = dd.dqdz;
				  				var jbrxm = dd.jbrxm;
				  				var temp = '';
				  				if(i==data.wllzxx.length-1){
				  					temp="<li>"+"<i class='icon-coord'>"+"</i>"+sdsj+"，到达"+dqdz+"<li>";
				  				}else {
				  					temp="<li>"+"<i class='icon-arrows-down'>"+"</i>"+sdsj+"，到达"+dqdz+"<li>";
				  				}
				  				$("#wllzxx_info").append(temp); 
			  				}
				  			
				  			//展示物流详情弹层
				  			$('#wlddxxByDdh_wlgj').click(function () {
				  				$('.pop').show();
				  			});

				  			$('.pop-con .close').click(function () {
				  				$('.pop').hide();
				  			})
				  			
				  			}
				  			else if(data.state == "error"){
		        				alert("没有查询到相关信息！");
		        			}
		        },
		        error : function() {
		            alert("请求查询失败！");
		        }
		    });			   
		}else{
			alert("请填全查询条件：物流公司和快递单号！");
	}
};

//wlzfxx_sch.jsp发送根据手机号查询ajax的请求
function submitPnumForm(){
	var randomdate = getRandom();//加载随机时间
	$("#wlddxxByPhone_info").empty();
	var wlddxxTemp="<li>"+"<span id='wlddxxByPhone_wlgs'>"+"</span>"
						 +"<span id='wlddxxByPhone_ddh'>"+"</span>"
						 +"<span id='wlddxxByPhone_ddjg'>"+"</span>"
					     +"<span id='wlddxxByPhone_jjr'>"+"</span>"
						 +"<span id='wlddxxByPhone_sjr'>"+"</span>"
						 +"<span id='wlddxxByPhone_jjrdh'>"+"</span>"
						 +"<span id='wlddxxByPhone_sjrdh'>"+"</span>"
						/* +"<span id='wlddxxByPhone_sfcs'>"+"</span>"
						 +"<span id='wlddxxByPhone_mdcs'>"+"</span>"*/
						 +"<span id='wlddxxByPhone_wlgj' class='detailBtn'>"+"</span>"
						 +"</li>";
	$("#wlddxxByPhone_info").append(wlddxxTemp);
	
	if($('#logistics').val() != "" && $('#phoneNum').val() != ""){
    $.ajax({
        type : 'post',
        url : wlddxxByPhonePath,
        dataType : 'json',
        data : {
        	'toL' : 'true',
            'phoneNum' : $('#phoneNum').val(),
            'wlgs' : $('#logistics').val()
        },
        success : function(data) {
        		if(data.state == "success"){
        			$("#wlddxxByPhone_wlgs").html(data.wlddxx.wlgs);
		  			$("#wlddxxByPhone_ddh").html(data.wlddxx.ddh);
		  			$("#wlddxxByPhone_ddjg").html(randomdate);
		  			$("#wlddxxByPhone_jjr").html(data.wlddxx.jjr);
		  			$("#wlddxxByPhone_sjr").html(data.wlddxx.sjr);
		  			$("#wlddxxByPhone_jjrdh").html(data.wlddxx.jjrdh);
		  			$("#wlddxxByPhone_sjrdh").html(data.wlddxx.sjrdh);
		  		/*	$("#wlddxxByPhone_sfcs").html(data.wlddxx.sfcs);
		  			$("#wlddxxByPhone_mdcs").html(data.wlddxx.mdcs); */
		  			var xqStr ="详情";
			  		$("#wlddxxByPhone_wlgj").text(xqStr);
		  			$("#wllzxx_ddh").html("运单号码 ：" + data.wlddxx.ddh);
		  			$("#wllzxx_info").empty();
		  			var connectStr = "<span class='border-left'></span>";
	  				$("#wllzxx_info").append(connectStr);
	  				
	  				for(var i = 0; i< data.wllzxx.length;i++){
	  					var dd = data.wllzxx[i];
	  					var sdsj = dd.sdsj;
	  					var dqdz = dd.dqdz;
		  				var jbrxm = dd.jbrxm;
		  				var temp = '';
		  				if(i==data.wllzxx.length-1){
		  					temp="<li>"+"<i class='icon-coord'>"+"</i>"+sdsj+"，到达"+dqdz+"<li>";
		  				}else {
		  					temp="<li>"+"<i class='icon-arrows-down'>"+"</i>"+sdsj+"，到达"+dqdz+"<li>";
		  				}
		  				$("#wllzxx_info").append(temp); 
	  				}
	  				
	
		  			$('#wlddxxByPhone_wlgj').click(function () {
		  				$('.pop').show();
		  			});

		  			$('.pop-con .close').click(function () {
		  				$('.pop').hide();
		  			})
		  			
        		}
	  			else if(data.state == "error"){
        			alert("没有查询到相关结果");
        		}
        },
        error : function() {
        	alert("请求查询失败！");
        }
    });
	}else{
		alert("请填全查询条件：物流公司和手机号码！");
	}
};

// wlzfxx_sch.jsp页面发送ajax的请求
function submitForm(){
	$("#wlzfxx_info").empty();
	if($('#DepartureCity').val() != "" && $('#ArrivalCity').val() != "" && $('#logistics').val() != ""){
		 $.ajax({
		        type : 'post',
		        url : wlzfxxPath,
		        dataType : 'json',
		        data : {
		        	'toL' : 'true',
		            'sfcs' : $("#hproper").attr('data-id'), 
		            'mdcs' : $('#city').attr('data-id'),
		            'wlgs' : $('#logistics').val()
		        },
		        success : function(data) {
		        			console.log(data);
		        			if(data.state == "success"){
		                    	var wlddxxTemp="<li>"+"<span>"+data.wlgs+"</span>"
				                 				  +"<span>"+data.sfcs+"</span>"
				                 				  +"<span>"+data.mdcs+"</span>"
				                 				  +"<span>"+data.jg+"元</span>"
				                 				  +"<span>"+(data.jg)/2+"元/公斤</span>"
							                 	  +"</li>";
							    $("#wlzfxx_info").append(wlddxxTemp);
		        			}
		        			else if(data.state == "error"){
		        			alert("没有查询到相关结果");
		        			}
		        },
		        error : function() {
		           		alert("请求失败！");
		        }
		    });
		}else {
			alert("请填全查询条件：始发城市，目的城市，物流公司！");
	}
};

//获取当前时间前一周内的随机时间
function getRandom(){
	var now = new Date();
	var date = new Date(now.getTime() - Math.round(Math.random()*9+1) * 24 * 3600 * 1000);
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	var hour = date.getHours();
	var minute = date.getMinutes();
	var second = date.getSeconds();
	return year + '-' + month + '-' + day  + ' ' + hour + ':' + minute + ':' + second;
}
