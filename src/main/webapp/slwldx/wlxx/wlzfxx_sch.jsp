<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>呼叫中心-物流</title>

    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/slwldx/wlxx/css/normalize.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/slwldx/wlxx/css/fonts.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/slwldx/wlxx/css/style.css"/>

    <script type="text/javascript" src="${pageContext.request.contextPath }/slwldx/wlxx/js/lib/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/easyui1_5/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/slwldx/wlxx/js/lib/city/Popt.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/slwldx/wlxx/js/lib/city/cityJson.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/slwldx/wlxx/js/lib/city/citySet.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/slwldx/wlxx/js/logistics.js"></script>
    <script type="text/javascript">
    //全局变量,logistics.js文件调用
    var wlgsPath = "${pageContext.request.contextPath}/slwldx/web/wlxx/schWlgs.action";
 	var wlzfxxPath = "${pageContext.request.contextPath}/slwldx/web/wlzfxx/schWlzfxx.action";
    </script>
</head>
<body>
<div class="wrap">
    <jsp:include page="../../book/hy_header.jsp" />

    <div class="main">
        <div class="inner">
            <!-- 搜索 -->
            <form class="searchBox" >
            		<input type="hidden" name="toL" value="true">
                <div class="item">
                    <label>始发城市：</label>
                    <input type="text" name="sfcs" id="DepartureCity" value="" />
                </div>
                <div class="item">
                    <label>目的城市：</label>
                    <input type="text" name="mdcs" id="ArrivalCity" value="" />
                </div>
                <div class="item logistics">
                    <label>物流公司：</label>
                    <input type="text" name="wlgs" id="logistics" value="" />
                    <div class="logisticsPop">
                        <span title="关闭" class="close">×</span>
                        <span class="text">支持中文输入</span>
                        <div class="logisticsList"></div>
                    </div>
                </div>
                <div class="item btn">
                    <i class="icon-search"></i>
                    <input type="button" value="搜索" onclick="submitForm()"/>
                </div>
            </form>

            <!-- 搜索结果 -->
            <div class="resultBox">
                <ul class="title">
                    <li><i class="icon-logistics"></i>物流公司</li>
                    <li><i class="icon-city"></i>始发城市</li>
                    <li><i class="icon-city"></i>目的城市</li>
                    <li><i class="icon-price"></i>价格</li>
                    <li><i class="icon-price"></i>续重价格</li>
                </ul>
                <ul id="wlzfxx_info" class="list">
                </ul>
            </div>
        </div>
    </div>

    <jsp:include page="../../book/_footer.jsp" />
</div>

<!-- 搜索城市 -->
<script type="text/javascript">
    //始发城市
	$("#DepartureCity").click(function (e) {
		SelDepartureCity(this,e);
		console.log("input",$(this).val(),new Date())
	});

    //目的城市
    $("#ArrivalCity").click(function (e) {
	    SelArrivalCity(this,e);
	    console.log("input",$(this).val(),new Date())
    });
</script>
</body>
</html>
