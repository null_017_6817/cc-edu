<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>修改物流流转信息</title><!--wllzxx-->
	<%@include file="/common/include.jsp" %>
<script>
<%-- 查询页点击修改时调用此方法加载记录详情 --%>
function load(id){
	$('#_form').form('load','/${projectName}/slwldx/wllzxx/vieWllzxx.action?id='+id);
};

<%-- 点击保存按钮时调用此方法。(注:保存按钮在查询列表页) --%>
function submitForm(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#_form').form('submit',{
      success:function(data) {
      	parent.reloadData();
      }
   });
}
</script>
</head>
<body>
<form id="_form" method="post" action="/${projectName}/slwldx/wllzxx/updWllzxx.action">
	<input type="hidden" name='id' id='id'/>
	<table>
		<tr><td class='label' style="width:120px">订单号：</td><td><input id="ddh" name="ddh" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >经办人姓名：</td><td><input id="jbrxm" name="jbrxm" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >经办人电话：</td><td><input id="jbrdh" name="jbrdh" class="easyui-textbox" data-options="required:true,validType:['tel']"/></td></tr>
		<tr><td class='label' >当前地址：</td><td><input id="dqdz" name="dqdz" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >送达时间：</td><td><input id="sdsj" name="sdsj" class="easyui-datetimebox" data-options="required:true"/></td></tr>
	</table>

</form>
</body>
</html>
