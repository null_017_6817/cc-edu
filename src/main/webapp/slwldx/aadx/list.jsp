<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>呼叫中心-电信列表</title>

<link rel="stylesheet" type="text/css" href="/${projectName}/slwldx/aadx/css/normalize.css" />
<link rel="stylesheet" type="text/css" href="/${projectName}/slwldx/aadx/css/fonts.css" />
<link rel="stylesheet" type="text/css" href="/${projectName}/slwldx/aadx/css/style.css" />

<script type="text/javascript" src="/${projectName}/slwldx/aadx/js/lib/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="/${projectName}/slwldx/aadx/js/tcxxList.js"></script>
<script>
	//详情
	function details(obj) {
		var cid = $(obj).attr("dataId");//获取起飞时间
		window.location = "/${projectName}/slwldx/grtcylxx/setDetail.action?cid="+cid;
	}
	
	var projectName  = "/${projectName}/slwldx/grtcylxx/schGrtcylxxForJsp.action";
</script>
</head>

<body>
	<div class="wrap">
		<jsp:include page="../../book/hy_header.jsp" />

		<div class="main">
			<div class="listBox inner">
				<!-- 搜索 -->
				<form class="clearfix">
					<div class="item">
						<label>手机号：</label> <input type="text" name="sjh" id="sjh" value="" />
					</div>
					<div class="item">
						<label>姓名：</label> <input type="text" name="xm" id="xm" value="" />
					</div>
					<div class="item btn">
						<i class="icon-search"></i> <input type="button" onclick="search()" value="搜索" />
					</div>
				</form>

				<!-- 搜索结果 -->
				<div class="resultLIst">
					<table>
						<thead>
							<tr>
								<td>手机号</li>
								<td>姓名</li>
								<td>套餐</li>
								<td>国内通话时长</li>
								<td>本地通话时长</li>
								<td>国内套餐剩余</li>
								<td>本地套餐剩余</li>
								<td>话费剩余</li>
								<td>操作</li>
							</tr>
						</thead>
					
						<tbody class="list" id="grtcylxxList">
						</tbody>
					</table>
				</div>

				<!-- 分页 -->
				<div class="fydiv">
					<ul class="fenye"></ul>
				</div>
			</div>
		</div>

		<jsp:include page="../../book/_footer.jsp" />
	</div>
</body>
</html>
