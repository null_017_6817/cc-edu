<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>呼叫中心-电信</title>

<link rel="stylesheet" type="text/css" href="/${projectName}/slwldx/aadx/css/normalize.css" />
<link rel="stylesheet" type="text/css" href="/${projectName}/slwldx/aadx/css/fonts.css" />
<link rel="stylesheet" type="text/css" href="/${projectName}/slwldx/aadx/css/style.css" />

<script type="text/javascript" src="/${projectName}/slwldx/aadx/js/lib/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="/${projectName}/slwldx/aadx/js/lib/jquery.validate.min.js"></script>
<script type="text/javascript" src="/${projectName}/slwldx/aadx/js/tab.js"></script>
<script type="text/javascript" src="/${projectName}/slwldx/aadx/js/form.js"></script>
<script type="text/javascript" src="/${projectName}/slwldx/aadx/js/tcxxIndex.js"></script>

<script>
	var urlTcxx = "/${projectName}/slwldx/tcxx/schTcxxMinMax.action";
	var urlSchTcxx = "/${projectName}/slwldx/tcxx/schTcxx.action";
</script>

</head>

<body>
	<div class="wrap">
		<jsp:include page="../../book/hy_header.jsp" />

		<div class="main">
			<div class="telecomBox inner">
				<h5>套餐筛选</h5>
				<div class="searchBox clearfix">
					<span class="pull-left">套餐分类：</span>
					<ul class="pull-left" id="oranger">
						<li class="active">全部</li>
						<li>移动</li>
						<li>电信</li>
						<li>联通</li>
					</ul>
					<div class="search pull-right">
						<input type="text" name="searchTop" placeholder="请输入关键字" /> <label class="icon-search" onclick="doSearch()"></label>
					</div>
				</div>

				<div class="resultBox">
					<!-- 全部  -->
					<div class="resultCon clearfix" id="grtcylxxList"></div>
					<!-- 移动  -->
					<div class="resultCon clearfix" id="mobileList"></div>
					<!-- 联通  -->
					<div class="resultCon clearfix" id="telecomList"></div>
					<!-- 电信  -->
					<div class="resultCon clearfix" id="unicornList"></div>
				</div>
			</div>
		</div>

		<jsp:include page="../../book/_footer.jsp" />

		<!-- 购买弹层 -->
		<div class="pop" id="buyCon">
			<div class="pop-con">
				<span class="close" onclick="returnBack()">×</span>
				<h5>购买信息</h5>
				<div class="clearfix">
					<div class="pull-left" id="poptcxy"></div>
					<div class="pull-right">
						<form id="_form" method="post" action="/${projectName}/slwldx/tcxx/buyTc.action">
							<h6 id="buyTitle"></h6>
							<input type="hidden" id="tcmc" name="tcmc"> 
							<input type="hidden" id="tid" name="tid">
							<input type="hidden" id="xzlltc" name="xzlltc"> 
							<input type="hidden" id="gjctsj" name="gjctsj">
							<ul>
								<li><label>适用客户:</label>动感地带 神州行 全球通</li>
								<li><label>生效时间:</label>次月</li>
								<li><label>功能:</label></li>
								<li id="buyzf"></li>
								<li class="form-info"><label>资&nbsp;&nbsp;&nbsp;费:</label> <input type="text" name="selectzf" id="selectzf" required="required" value="" readonly /></li>
								<li class="form-info"><label>姓&nbsp;&nbsp;&nbsp;名:</label> <input type="text" name="xm" class="xm" id="xm" required="required" /></li>
								<li class="form-info"><label>电&nbsp;&nbsp;&nbsp;话:</label> <input type="text" name="sjh" class="sjh" id="sjh" required="required" /></li>
								<li class="form-info"><label>身份证:</label> <input type="text" name="sfzh" class="sfzh" id="sfzh" required="required" /></li>
							</ul>
							<button id="subBtn">立即办理</button>
							<input type="checkbox" id="longcall">开通国际长途
							<input type="checkbox" id="dataplan">购买流量包
							
						</form>
					</div>
				</div>
				<!-- 国际长途 -->
				<!-- <div class="idd" id="idd">
					<div class="imgcall">
						<img src="img/longcall.png">
					</div>
					<div>
						<h6>国际漫游/港台漫游功能</h6>
						<ul>
							<li><label>资费:</label><span>0</span>元</li>
							<li class="btns"><label>套餐类型:</label><span onclick="longcall(this)">长期</span><span onclick="longcall(this)">180</span><span onclick="longcall(this)">30</span><span id="type"></span></li>
							<li><label>业务介绍:</label>为中国国际/港澳台漫游出访客户提供的漫游通信服务</li>
						</ul>
					</div>
				</div> -->
				<!-- 流量套餐  -->
				<!-- <div class="dp" id="dp">
					<div class="dataplan">
						<img src="img/dataplan.png">
					</div>
					<div>
						<h6>流量可选包</h6>
						<ul>
							<li><label>适用客户:</label>动感地带 神州行 全球通</li>
							<li><label>生效日期:</label>立即</li>
							<li class="btns"><label>功能:</label><span onclick="package(this)">10-元包100M</span><span onclick="package(this)">20-元包300M</span><span onclick="package(this)">30-元包500M</span><span onclick="package(this)">40-元包700M</span><span onclick="package(this)">50-元包1G</span><span onclick="package(this)">70-元包2G</span>
							<span onclick="package(this)">100-元包3G</span><span onclick="package(this)">130-元包4G</span><span onclick="package(this)">180-元包6G</span><span onclick="package(this)">280-元包11G</span></li>
							<li><label>资费:</label><span id="dataplanzf"></span>元/月</li>
						</ul>
					</div>
				</div> -->
			</div>
		</div>
	</div>
</body>
</html>
