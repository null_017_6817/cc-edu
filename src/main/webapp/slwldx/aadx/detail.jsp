<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>呼叫中心-电信详情</title>

<link rel="stylesheet" type="text/css" href="/${projectName}/slwldx/aadx/css/normalize.css" />
<link rel="stylesheet" type="text/css" href="/${projectName}/slwldx/aadx/css/fonts.css" />
<link rel="stylesheet" type="text/css" href="/${projectName}/slwldx/aadx/css/style.css" />

<script type="text/javascript" src="/${projectName}/slwldx/aadx/js/lib/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="/${projectName}/slwldx/aadx/js/plan.js"></script>
</head>

<body>
	<div class="wrap">
		<jsp:include page="../../book/hy_header.jsp" />

		<div class="main">
			<div class="detailBox inner">
				<h5>套餐余量查询</h5>
				<div class="detailCon" id="detailCon">

					<h6>${tcxx.tcmc}</h6>
					<dl>
						<dt>国内2/3/4G融合流量共<span class="totalNum">${tcxx.gnll}</span>MB</dt>
						<dd>
							<div class='planBox'>
								<span><strong></strong></span>
							</div>
							剩余<i class='plan-pink surplusNum'>${grtcylxx2.gnllsy}</i>MB
						</dd>
					</dl>
					<dl>
						<dt>本地2/3/4G融合流量共<span class="totalNum">${tcxx.bdll}</span>MB</dt>
						<dd>
							<div class='planBox'>
								<span><strong></strong></span>
							</div>
							剩余<i class='plan-pink surplusNum'>${grtcylxx2.bdllsy}</i>MB
						</dd>
					</dl>
					<dl>
						<dt>全国语音资源共<span class="totalNum">${tcxx.qgth}</span>分钟</dt>
						<dd>
							<div class='planBox'>
								<span><strong></strong></span>
							</div>
							剩余<i class='plan-pink surplusNum'>${grtcylxx2.qgthsy}</i>分钟
						</dd>
					</dl>
					<dl>
						<dt>本地语音资源共<span class="totalNum">${tcxx.bdth}</span>分钟</dt>
						<dd>
							<div class='planBox'>
								<span> <strong></strong></span>
							</div>
							剩余<i class='plan-pink surplusNum'>${grtcylxx2.bdthsy}</i>分钟
						</dd>
					</dl>
				</div>
			</div>
		</div>

		<jsp:include page="../../book/_footer.jsp" />
	</div>
</body>
</html>
