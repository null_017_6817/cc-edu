$(function () {
	planFun();
	//初始化方法
	function planFun () {
		//先获取总长度和剩余长度
		var totalWidth = $('.planBox').width();
		//遍历当前页面中dl个数
		$('#detailCon dl').each (function () {
			var surplusNum = $(this).find($('.surplusNum')).html();
			var totalNum = $(this).find($('.totalNum')).html();
			if(surplusNum!=0){
				var newWidth = parseInt(surplusNum / totalNum * totalWidth);
				var newNum = parseInt(surplusNum / totalNum * 100);
				$(this).find($('.planBox span')).width(newWidth);
				$(this).find($('.planBox strong')).html(newNum + '%');
			}else{
				$(this).find($('.planBox span')).width(0);
				$(this).find($('.planBox strong')).html(0 + '%');
			}
			//判断颜色
			if (newNum === 100) {
				$(this).find($('.planBox span')).addClass('plan-green');
				$(this).find($('.planBox strong')).addClass('plan-green');
			}else if (newNum >= 50) {
				$(this).find($('.planBox span')).addClass('plan-yellow');
				$(this).find($('.planBox strong')).addClass('plan-yellow');
			}else {
				$(this).find($('.planBox span')).addClass('plan-pink');
				$(this).find($('.planBox strong')).addClass('plan-pink');
			}
		})
	}
})	
	
