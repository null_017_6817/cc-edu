//初始化方法
$(function() {
	search(1);
})

//搜索栏的标题的模糊查询
function search(pnum) {
	$("#grtcylxxList").html('');
	if (pnum == null || pnum == "undefined") {
		pnum = 1;
	}
	$.ajax({
		url : projectName,
		type : "post",
		data : {
			"sjh" : $("input[name='sjh']").val(),
			"xm" : $("input[name='xm']").val(),
			"page" : pnum
		},
		dataType : "json",
		success : function(data) {
			getPage(pnum, data);
		}
	});
}

function getPage(pn, data) {
	var dataCount = data.total;//总数据条数
	var pageSize = data.pageSize;//每页显示条数
	data = data.rows;
	var pageCount = Math.ceil(dataCount / pageSize);//总页数
	if (pn == 0 || pn > pageCount) {
		return;
	}
	var ul = $(".listul");
	ul.empty();
	paintPage(pageCount, pn); //绘制页码
	var startPage = pageSize * (pn - 1);
	for (var j = 0; j < data.length; j++) {
		var html = "<tr><td>"
				+ data[j].sjh
				+ "</td><td>"
				+ data[j].xm
				+ "</td><td>"
				+ data[j].tcid
				+ "</td><td>"
				+ data[j].qgthsy
				+ "</td><td>"
				+ data[j].bdthsy
				+ "</td><td>"
				+ data[j].gnllsy
				+ "</td><td>"
				+ data[j].bdllsy
				+ "</td><td>"
				+ data[j].hfye
				+ "</td><td><span onclick='details(this)' dataId='" + data[j].id + "'>详情</span></td></tr>"
		$("#grtcylxxList").append(html);
	}
}

//绘制页码
function paintPage(number, currNum){ //number 总页数,currNum 当前页  
	var pageUl = $(".fenye");
	pageUl.empty();
	var ulDetail = "";
	if (number == 1) {
		ulDetail = "<li class=\"start\"><a href=\"javascript:void(0)\">首页</a></li>"
				+ "<li class=\"prev\"><a href=\"javascript:void(0)\">上一页</a></li>"
				+ "<li class=\"numb choose\"><a href=\"javascript:search(1)\">1</a></li>"
				+ "<li class=\"next\"><a href=\"javascript:void(0)\">下一页</a></li>"
				+ "<li class=\"end\"><a href=\"javascript:void(0)\">尾页</a></li>";
	} else if (number == 2) {
		ulDetail = "<li class=\"start\"><a href=\"javascript:search(1)\">首页</a></li>"
				+ "<li class=\"prev\"><a href=\"javascript:search(1)\">上一页</a></li>"
				+ "<li class=\"numb"
				+ choosele(currNum, 1)
				+ "\"><a href=\"javascript:search(1)\">1</a></li>"
				+ "<li class=\"numb"
				+ choosele(currNum, 2)
				+ "\"><a href=\"javascript:search(2)\">2</a></li>"
				+ "<li class=\"next\"><a href=\"javascript:search(2)\">下一页</a></li>"
				+ "<li class=\"end\"><a href=\"javascript:search(2)\">尾页</a></li>";
	} else if (number == 3) {
		ulDetail = "<li class=\"start\"><a href=\"javascript:search(1)\">首页</a></li>"
				+ "<li class=\"prev\"><a href=\"javascript:search("
				+ parseInt(currNum - 1)
				+ ")\">上一页</a></li>"
				+ "<li class=\"numb"
				+ choosele(currNum, 1)
				+ "\"><a href=\"javascript:search(1)\">1</a></li>"
				+ "<li class=\"numb"
				+ choosele(currNum, 2)
				+ "\"><a href=\"javascript:search(2)\">2</a></li>"
				+ "<li class=\"numb"
				+ choosele(currNum, 3)
				+ "\"><a href=\"javascript:search(3)\">3</a></li>"
				+ "<li class=\"next\"><a href=\"javascript:search("
				+ parseInt(currNum + 1)
				+ ")\">下一页</a></li>"
				+ "<li class=\"end\"><a href=\"javascript:search(3)\">尾页</a></li>";
	} else if (number == currNum && currNum > 3) {
		ulDetail = "<li class=\"start\"><a href=\"javascript:search(1)\">首页</a></li>"
				+ "<li class=\"prev\"><a href=\"javascript:search("
				+ parseInt(currNum - 1)
				+ ")\">上一页</a></li>"
				+ "<li class=\"numb\"><a href=\"javascript:search("
				+ parseInt(currNum - 2)
				+ ")\">"
				+ parseInt(currNum - 2)
				+ "</a></li>"
				+ "<li class=\"numb\"><a href=\"javascript:search("
				+ parseInt(currNum - 1)
				+ ")\">"
				+ parseInt(currNum - 1)
				+ "</a></li>"
				+ "<li class=\"numb choose\"><a href=\"javascript:search("
				+ currNum
				+ ")\">"
				+ currNum
				+ "</a></li>"
				+ "<li class=\"next\"><a href=\"javascript:search("
				+ currNum
				+ ")\">下一页</a></li>"
				+ "<li class=\"end\"><a href=\"javascript:search("
				+ number + ")\">尾页</a></li>";
	} else if (currNum == 1 && number > 3) {
		ulDetail = "<li class=\"start\"><a href=\"javascript:search(1)\">首页</a></li>"
				+ "<li class=\"prev\"><a href=\"javascript:void(0)\">上一页</a></li>"
				+ "<li class=\"numb choose\"><a href=\"javascript:void(0)\">1</a></li>"
				+ "<li class=\"numb\"><a href=\"javascript:search(2)\">2</a></li>"
				+ "<li class=\"numb\"><a href=\"javascript:search(3)\">3</a></li>"
				+ "<li class=\"next\"><a href=\"javascript:search(2)\">下一页</a></li>"
				+ "<li class=\"end\"><a href=\"javascript:search("
				+ number + ")\">尾页</a></li>";
	} else {
		ulDetail = "<li class=\"start\"><a href=\"javascript:search(1)\">首页</a></li>"
				+ "<li class=\"prev\"><a href=\"javascript:search("
				+ parseInt(currNum - 1)
				+ ")\">上一页</a></li>"
				+ "<li class=\"numb\"><a href=\"javascript:search("
				+ parseInt(currNum - 1)
				+ ")\">"
				+ parseInt(currNum - 1)
				+ "</a></li>"
				+ "<li class=\"numb choose\"><a href=\"javascript:search("
				+ currNum
				+ ")\">"
				+ currNum
				+ "</a></li>"
				+ "<li class=\"numb\"><a href=\"javascript:search("
				+ parseInt(currNum + 1)
				+ ")\">"
				+ parseInt(currNum + 1)
				+ "</a></li>"
				+ "<li class=\"next\"><a href=\"javascript:search("
				+ parseInt(currNum + 1)
				+ ")\">下一页</a></li>"
				+ "<li class=\"end\"><a href=\"javascript:search("
				+ number + ")\">尾页</a></li>";
	}
	$(".fenye").append(ulDetail);
}

function choosele(num, cur) {
	if (num == cur) {
		return " choose";
	} else {
		return "";
	}
}