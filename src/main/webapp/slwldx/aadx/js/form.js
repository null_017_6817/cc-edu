$(function () {

	//表单验证
	initCardTypeFrmValidate();

	//表单验证
	function initCardTypeFrmValidate() {
		$("#_form").validate({
			rules: {
				//姓名
				xm: "required",
				//手机号
				sjh: {
					required: true,
					sjh: true
				},
				//身份证号
				sfzh: {
					required: true,
					sfzh: true
				},
				//套餐ka
				selectzf: "required"
			},
			messages: {
				//姓名
				xm: "姓名不能为空",
				//手机号
				sjh: {
					required: "手机号码不能为空",
					sjh: "手机号码输入错误"
				},
				sfzh: {
					required: "身份证码不能为空",
					sfzh: "身份证号码输入错误"
				},
				//套餐ka
				selectzf: "请选择套餐功能"
			}
		});
		
		
		//手机号码
		$.validator.addMethod("sjh", function (value, element, params) {
			var sjh = /^1[34578]\d{9}$/;
			return this.optional(element) || (sjh.test(value));
		}, "手机号码输入错误");
		
		$.validator.addMethod("sfzh", function (value, element, params) {
			var sfzh = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
			return this.optional(element) || (sfzh.test(value));
		}, "身份证输入错误");
	}
	
	
});
