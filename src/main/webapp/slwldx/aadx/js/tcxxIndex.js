//初始化方法
$(function() {
	grtcylxxList();
	mobileList();
	telecomList();
	unicornList();
	$(".idd").hide();
	$(".dp").hide();
})

//加载全部套餐用量数据
function grtcylxxList() {
	$("#grtcylxxList").html("");
	$.ajax({
		url : urlTcxx,
		dataType : "json",
		data : {"tcmc" : $("input[name='searchTop']").val()},
		success : function(data) {
			data = data.rows;
			for (var i = 0; i < data.length; i++) {
				if (data[i].tcxy == "移动") {
					$("#grtcylxxList").append(
						"<dl><dt><img src='img/combo1.png' /></dt><dd><h5 id='onloadtcmc'>"
								+ data[i].tcmc
								+ "</h5><p>你可以自己选择你想要的。。。</p><i>"
								+ data[i].minzf
								+ "-"
								+ data[i].maxzf
								+ "</i><a href='###' onclick='showBuy(this);' dataTcxy = '"+data[i].tcxy+"'>购买</a></dd></dl>");
				}
				if(data[i].tcxy == "电信"){
					$("#grtcylxxList").append(
						"<dl><dt><img src='img/combo2.png' /></dt><dd><h5 id='onloadtcmc'>"
								+ data[i].tcmc
								+ "</h5><p>你可以自己选择你想要的。。。</p><i>"
								+ data[i].minzf
								+ "-"
								+ data[i].maxzf
							+ "</i><a href='###' onclick='showBuy(this);' dataTcxy = '"+data[i].tcxy+"'>购买</a></dd></dl>");
				}
				if(data[i].tcxy == "联通"){
					$("#grtcylxxList").append(
						"<dl><dt><img src='img/combo3.png' /></dt><dd><h5 id='onloadtcmc'>"
								+ data[i].tcmc
								+ "</h5><p>你可以自己选择你想要的。。。</p><i>"
								+ data[i].minzf
								+ "-"
								+ data[i].maxzf
								+ "</i><a href='###' onclick='showBuy(this);' dataTcxy = '"+data[i].tcxy+"'>购买</a></dd></dl>");
				}
			}
			$(this).addClass("done");
		}
	});
}

//移动
function mobileList() {
	$("#mobileList").html("");
	$.ajax({
		url : urlTcxx,
		dataType : "json",
		data : {"tcmc" : $("input[name='searchTop']").val()},
		success : function(data) {
			data = data.rows;
			for (var i = 0; i < data.length; i++) {
				if (data[i].tcxy == "移动") {
					$("#mobileList").append(
						"<dl><dt><img src='img/combo1.png' /></dt><dd><h5 id='onloadtcmc'>"
							+ data[i].tcmc
							+ "</h5><p>你可以自己选择你想要的。。。</p><i>"
							+ data[i].minzf
							+ "-"
							+ data[i].maxzf
							+ "</i><a href='###' onclick='showBuy(this);' dataTcxy = '"+data[i].tcxy+"'>购买</a></dd></dl>");
				}
			}
			$(this).addClass("done");
		}
	});
}

//电信
function telecomList() {
	$("#telecomList").html("");
	$.ajax({
		url : urlTcxx,
		dataType : "json",
		data : {"tcmc" : $("input[name='searchTop']").val()},
		success : function(data) {
			data = data.rows;
			for (var i = 0; i < data.length; i++) {
				if (data[i].tcxy == "电信") {
					$("#telecomList").append(
						"<dl><dt><img src='img/combo3.png' /></dt><dd><h5 id='onloadtcmc'>"
							+ data[i].tcmc
							+ "</h5><p>你可以自己选择你想要的。。。</p><i>"
							+ data[i].minzf
							+ "-"
							+ data[i].maxzf
							+ "</i><a href='###' onclick='showBuy(this);' dataTcxy = '"+data[i].tcxy+"'>购买</a></dd></dl>");
				}
			}
			$(this).addClass("done");
		}
	});
}
//联通
function unicornList() {
	$("#unicornList").html("");
	$.ajax({
		url : urlTcxx,
		dataType : "json",
		data : {"tcmc" : $("input[name='searchTop']").val()},
		success : function(data) {
			data = data.rows;
			for (var i = 0; i < data.length; i++) {
				if (data[i].tcxy == "联通") {
					$("#unicornList").append(
						"<dl><dt><img src='img/combo2.png' /></dt><dd><h5 id='onloadtcmc'>"
							+ data[i].tcmc
							+ "</h5><p>你可以自己选择你想要的。。。</p><i>"
							+ data[i].minzf
							+ "-"
							+ data[i].maxzf
							+ "</i><a href='###' onclick='showBuy(this);' dataTcxy = '"+data[i].tcxy+"'>购买</a></dd></dl>");
				}
			}
			$(this).addClass("done");
		}
	});
}

//点击搜索
function doSearch() {
	$(function() {
		grtcylxxList();
		mobileList();
		telecomList();
		unicornList();
	})
}

//加载全部套餐用量数据
function buyList() {
	$("#buyzf").html("");
	$.ajax({
		url : urlSchTcxx,
		dataType : "json",
		success : function(data) {
			data = data.rows;
			var fortcmc = $("input[name='tcmc']").val();
			for (var i = 0; i < data.length; i++) {
				if (data[i].tcmc == fortcmc) {
					$("#buyzf").append(
							"<span onclick='zf(this)' name='zf' dataId='"
									+ data[i].id + "'>" + fortcmc + "-"
									+ data[i].zf + "元 </span>");
				}
			}
			$(this).addClass("done");
		}
	});
}

//根据套餐新增资费
function zf(obj) {
	var str = $(obj).html();
	var spstr = str.split("-");
	for (var i = 0; i < spstr.length; i++) {
		$("#selectzf").val(spstr[1]);
	}
	$("#tid").val($(obj).attr("dataId"));
}

//提交购买套餐
$('.subBtn').click(function() {
	if ($('#_form').valid()) {
		alert(111);
	}
	return false;//永远禁止页面表单提交
});

//购买套餐
function showBuy(obj) {
	$("#type").html("");//清空选中国际长途
	$("#selectzf").val("");//清空资费
	$("#poptcxy").html("");//清空img图片
	var buytcmc = $(obj).parent().find("#onloadtcmc").html();
	$("#tcmc").val(buytcmc);
	$("#buyTitle").html(buytcmc);
	var poptcxy = $(obj).attr("dataTcxy");
	if (poptcxy == "移动") {
		$("#poptcxy").append("<img src='img/combo1.png' class='buymian' />");
	}
	if (poptcxy == "电信") {
		$("#poptcxy").append("<img src='img/combo3.png' class='buymian' />");
	}
	if (poptcxy == "联通") {
		$("#poptcxy").append("<img src='img/combo2.png' class='buymian' />");
	}
	$('#buyCon').show();
	buyList();
}

//关闭购买套餐弹层
function returnBack() {
	$(".pop").hide();
}

//是否开通国际长途,是否购买流量包
$(function(){
    $('#longcall').click(function(){
    	//如果选中就显示div   否则就隐藏
        if($("#longcall").prop("checked")){  
        	$(".idd").show();
        }
        else
        	$(".idd").hide();
    });
    $('#dataplan').click(function(){
        if($("#dataplan").prop("checked")){
        	$(".dp").show();
        }
        else
        	$(".dp").hide();
    });
})

//根据套餐新增资费
function package(obj) {
	var str = $(obj).html();
	var spstr = str.split("-");
	for (var i = 0; i < spstr.length; i++) {
		$("#dataplanzf").html(spstr[0]);
		$("#xzlltc").val(spstr[0]);
	}
}

//是否开通国际长途
function longcall(obj) {
	var str = $(obj).html();
	$("#gjctsj").val(str);
	$("#type").html(str);
	$("#type").css("background-color","red");
}
