<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>查看套餐信息</title><!--tcxx-->
	<%@include file="/common/include.jsp" %>
<script>
function load(id){
	$('#_form').form('load','/${projectName}/slwldx/tcxx/vieTcxx.action?toL=true&id='+id);
};
</script>
</head>
<body>
<form id="_form">
	<input type="hidden" name='id' value=""/>
	<table>
		<tr><td class='label'>套餐行业：</td><td><input type='text' name='tcxy' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>套餐名称：</td><td><input style="width:180px" type='text' name='tcmc' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>资费（元）：</td><td><input type='text' name='zf' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>短信（条数）：</td><td><input type='text' name='dx' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>国内流量（M）：</td><td><input type='text' name='gnll' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>本地流量（M）：</td><td><input type='text' name='bdll' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>本地通话(分钟)：</td><td><input type='text' name='bdth' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>全国通话（分钟）：</td><td><input type='text' name='qgth' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>详细说明：</td><td><textarea readonly='readonly' id="" id="xxsm" name="xxsm" rows=5 cols="45" class="textarea easyui-validatebox"></textarea></td></tr>
	</table>

</form>
</body>
</html>
