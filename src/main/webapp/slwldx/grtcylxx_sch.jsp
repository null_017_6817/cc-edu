<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>个人套餐用量信息查询</title><!--grtcylxx-->
	<%@include file="/common/include.jsp" %>

<script>
$(function(){
	var pager = $('#grtcylxxGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});			
})

<%-- 执行查询，点击查询按钮时调用 --%>
function doSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#grtcylxxGrid').datagrid('load',{
		sjh:$('#sjh').val(),
		xm:$('#xm').val()
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatOper(val,row,index){  
    return "&emsp;<a href='javascript:void(0)' onclick='updGrtcylxx("+val+")' class='_oper' title='修改'>&nbsp;<i class='icon-edit'></i>&nbsp;</a>"
    			+"<a href='javascript:void(0)' onclick='vieGrtcylxx("+val+")' class='_oper' title='查看'>&nbsp;<i class='icon-info-sign'></i>&nbsp;</a>&emsp;"; 
} 

<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
function updGrtcylxx(id){
	QMoveDialog($("#grtcylxxEditWin"));
	$('#grtcylxxViewWin').dialog('close');
	$('#grtcylxxEditWin').dialog('open');
	$('#grtcylxxEditWin').get(0).contentWindow.load(id);
} 

<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
function vieGrtcylxx(id){
	QMoveDialog($("#grtcylxxViewWin"));
	$('#grtcylxxEditWin').dialog('close');
	$('#grtcylxxViewWin').dialog('open');
	$('#grtcylxxViewWin').get(0).contentWindow.load(id);
}

<%-- 删除记录 --%>
function delGrtcylxx(){
	var checkedItems = $('#grtcylxxGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/slwldx/grtcylxx/delGrtcylxx.action",{ids:ids},function(data){
				$('#grtcylxxGrid').datagrid('reload');
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#grtcylxxEditWin').dialog('close');
	$('#grtcylxxGrid').datagrid('reload');
}
</script>
</head>
<body>
<table id="grtcylxxGrid" title="" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#grtcylxxGridToolbar',collapsible:true,url:'/${projectName}/slwldx/grtcylxx/schGrtcylxx.action',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" ></th>
			<th data-options="field:'sjh',width:5,sortable:true">手机号</th>
			<th data-options="field:'xm',width:2,sortable:true">姓名</th>
			<th data-options="field:'tcid',width:5,sortable:true" dictId="CCQ_TC">套餐</th>
			<th data-options="field:'dxsy',sortable:true">短信剩余（条数）</th>
			<th data-options="field:'gnllsy',sortable:true">国内流量剩余（M）</th>
			<th data-options="field:'bdllsy',sortable:true">本地流量剩余（M）</th>
			<th data-options="field:'bdthsy',sortable:true">本地通话剩余(分钟)</th>
			<th data-options="field:'qgthsy',sortable:true">全国通话剩余（分钟）</th>
			<th data-options="field:'hfye',sortable:true">话费余额（元）</th>

			<th data-options="field:'_oper',align:'center',formatter:formatOper,width:5">操作</th>
		</tr>
	</thead>
</table>
<%-- 查询条件设定栏 --%>
<div id="grtcylxxGridToolbar">
	<div class="opline">
		<a href="javascript:updGrtcylxx(0)" class="btn btn-success"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">添加</span></a>&emsp;
		<a href="javascript:delGrtcylxx()" class="btn btn-danger"><span class="icon-trash" style="font-size:16px"></span> <span style="font-size:14px">删除</span></a>
	</div>
	<form id="_form" class="shline">
	手机号：<input name='sjh' id="sjh" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	姓名：<input name='xm' id="xm" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>

	<a href="javascript:void(0)" onclick="doSearch()" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>
	</form>
</div>


<%-- 修改or添加面板 --%>
<iframe id="grtcylxxEditWin" src="/${projectName}/slwldx/grtcylxx/page.action?page=grtcylxx_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#grtcylxxEditButton'" title="修改个人套餐用量信息" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="grtcylxxEditButton">
	<a href="javascript:void(0)" onclick="$('#grtcylxxEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#grtcylxxEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >取 消</a>
</div>
<%-- 查看面板 --%>
<iframe id="grtcylxxViewWin" src="/${projectName}/slwldx/grtcylxx/page.action?page=grtcylxx_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#grtcylxxViewButton'" title="查看个人套餐用量信息" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="grtcylxxViewButton">
	<a href="javascript:$('#grtcylxxViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >关 闭</a>
</div>
</body>
</html>
