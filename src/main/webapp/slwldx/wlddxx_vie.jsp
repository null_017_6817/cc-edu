<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>查看物流订单信息</title><!--wlddxx-->
	<%@include file="/common/include.jsp" %>
<script>
function load(id){
	$('#_form').form('load','/${projectName}/slwldx/wlddxx/vieWlddxx.action?toL=true&id='+id);
};
</script>
</head>
<body>
<form id="_form">
	<input type="hidden" name='id' value=""/>
	<table>
		<tr><td class='label'>物流：</td><td><input type='text' name='wlzfid' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>订单号：</td><td><input type='text' name='ddh' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>订单价格(元)：</td><td><input type='text' name='ddjg' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>寄件人：</td><td><input type='text' name='jjr' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>收件人：</td><td><input type='text' name='sjr' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>寄件人电话：</td><td><input type='text' name='jjrdh' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>收件人电话：</td><td><input type='text' name='sjrdh' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>始发城市：</td><td><input type='text' name='sfcs' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>目的城市：</td><td><input type='text' name='mdcs' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>始发城市详细地址：</td><td><input type='text' name='sfcsxxdz' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>目的城市详细地址：</td><td><input type='text' name='mdcsxxdz' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>寄件时间：</td><td><input type='text' name='jjsj' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>收件时间：</td><td><input type='text' name='sjsj' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>订单状态：</td><td><input type='text' name='ddzt' class='viewbox' readonly='readonly'/></td></tr>
	</table>

</form>
</body>
</html>
