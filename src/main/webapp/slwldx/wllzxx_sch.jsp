<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>物流流转信息查询</title><!--wllzxx-->
	<%@include file="/common/include.jsp" %>

<script>
$(function(){
	var pager = $('#wllzxxGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});			
})

<%-- 执行查询，点击查询按钮时调用 --%>
function doSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#wllzxxGrid').datagrid('load',{
		ddh:$('#ddh').val()
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatOper(val,row,index){  
    return "&emsp;<a href='javascript:void(0)' onclick='updWllzxx("+val+")' class='_oper' title='修改'>&nbsp;<i class='icon-edit'></i>&nbsp;</a>"
    			+"<a href='javascript:void(0)' onclick='vieWllzxx("+val+")' class='_oper' title='查看'>&nbsp;<i class='icon-info-sign'></i>&nbsp;</a>&emsp;"; 
} 

<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
function updWllzxx(id){
	QMoveDialog($("#wllzxxEditWin"));
	$('#wllzxxViewWin').dialog('close');
	$('#wllzxxEditWin').dialog('open');
	$('#wllzxxEditWin').get(0).contentWindow.load(id);
} 

<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
function vieWllzxx(id){
	QMoveDialog($("#wllzxxViewWin"));
	$('#wllzxxEditWin').dialog('close');
	$('#wllzxxViewWin').dialog('open');
	$('#wllzxxViewWin').get(0).contentWindow.load(id);
}

<%-- 删除记录 --%>
function delWllzxx(){
	var checkedItems = $('#wllzxxGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/slwldx/wllzxx/delWllzxx.action",{ids:ids},function(data){
				$('#wllzxxGrid').datagrid('reload');
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#wllzxxEditWin').dialog('close');
	$('#wllzxxGrid').datagrid('reload');
}
</script>
</head>
<body>
<table id="wllzxxGrid" title="" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#wllzxxGridToolbar',collapsible:true,url:'/${projectName}/slwldx/wllzxx/schWllzxx.action',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" ></th>
			<th data-options="field:'ddh',width:20,sortable:true">订单号</th>
			<th data-options="field:'jbrxm',sortable:true">经办人姓名</th>
			<th data-options="field:'jbrdh',sortable:true">经办人电话</th>
			<th data-options="field:'dqdz',sortable:true,width:20">当前地址</th>
			<th data-options="field:'sdsj',sortable:true">送达时间</th>

			<th data-options="field:'_oper',align:'center',formatter:formatOper,width:20">操作</th>
		</tr>
	</thead>
</table>
<%-- 查询条件设定栏 --%>
<div id="wllzxxGridToolbar">
	<div class="opline">
		<a href="javascript:updWllzxx(0)" class="btn btn-success"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">添加</span></a>&emsp;
		<a href="javascript:delWllzxx()" class="btn btn-danger"><span class="icon-trash" style="font-size:16px"></span> <span style="font-size:14px">删除</span></a>
	</div>
	<form id="_form" class="shline">
	订单号：<input name='ddh' id="ddh" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>

	<a href="javascript:void(0)" onclick="doSearch()" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>
	</form>
</div>


<%-- 修改or添加面板 --%>
<iframe id="wllzxxEditWin" src="/${projectName}/slwldx/wllzxx/page.action?page=wllzxx_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#wllzxxEditButton'" title="修改物流流转信息" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="wllzxxEditButton">
	<a href="javascript:void(0)" onclick="$('#wllzxxEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#wllzxxEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >取 消</a>
</div>
<%-- 查看面板 --%>
<iframe id="wllzxxViewWin" src="/${projectName}/slwldx/wllzxx/page.action?page=wllzxx_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#wllzxxViewButton'" title="查看物流流转信息" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="wllzxxViewButton">
	<a href="javascript:$('#wllzxxViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >关 闭</a>
</div>
</body>
</html>
