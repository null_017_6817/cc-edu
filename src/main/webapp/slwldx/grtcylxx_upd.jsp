<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>修改个人套餐用量信息</title><!--grtcylxx-->
	<%@include file="/common/include.jsp" %>
<script>
<%-- 查询页点击修改时调用此方法加载记录详情 --%>
function load(id){
	$('#_form').form('load','/${projectName}/slwldx/grtcylxx/vieGrtcylxx.action?id='+id);
};

<%-- 点击保存按钮时调用此方法。(注:保存按钮在查询列表页) --%>
function submitForm(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#_form').form('submit',{
      success:function(data) {
      	parent.reloadData();
      }
   });
}
</script>
</head>
<body>
<form id="_form" method="post" action="/${projectName}/slwldx/grtcylxx/updGrtcylxx.action">
	<input type="hidden" name='id' id='id'/>
	<table>
		<tr><td class='label' style="width:120px">手机号：</td><td><input id="sjh" name="sjh" class="easyui-textbox" data-options="required:true,validType:['mobile']"/></td></tr>
		<tr><td class='label' >姓名：</td><td><input id="xm" name="xm" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >套餐：</td><td>
		<dc:insertList name="tcid" dictId="CCQ_TC" style="combo_grid:{TCXY:套餐类型,TCMC:套餐名称,ZF:资费(元)}" cssWidth="300px"/></td></tr>
		<tr><td class='label' >短信剩余（条数）：</td><td><input id="dxsy" name="dxsy" class="easyui-numberbox" data-options="required:true,validType:['int']"/></td></tr>
		<tr><td class='label' >国内流量剩余（M）：</td><td><input id="gnllsy" name="gnllsy" class="easyui-numberbox" data-options="required:true,validType:['int']"/></td></tr>
		<tr><td class='label' >本地流量剩余（M）：</td><td><input id="bdllsy" name="bdllsy" class="easyui-numberbox" data-options="required:true,validType:['int']"/></td></tr>
		<tr><td class='label' >本地通话剩余(分钟)：</td><td><input id="bdthsy" name="bdthsy" class="easyui-numberbox" data-options="required:true,validType:['int']"/></td></tr>
		<tr><td class='label' >全国通话剩余（分钟）：</td><td><input id="qgthsy" name="qgthsy" class="easyui-numberbox" data-options="required:true,validType:['int']"/></td></tr>
		<tr><td class='label' >话费余额（元）：</td><td><input id="hfye" name="hfye" class="easyui-textbox" data-options="required:true"/></td></tr>
	</table>

</form>
</body>
</html>
