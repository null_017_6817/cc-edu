<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<script type="text/javascript" src="/${projectName}/easyui1_5/jquery.min.js"></script>
	<style>
	body{padding:0;margin:0;background:#35383d;}
	.use-flex{display:flex;flex-direction:column;height:100%;position:absolute;width:100%}
	.use-flex div{}
	
	.top{
		flex:none;height:100px;
		background: -webkit-linear-gradient(top,#55535e 0,#35383d 100%);
		background: -moz-linear-gradient(top,#55535e 0,#35383d 100%);
		background: -o-linear-gradient(top,#55535e 0,#35383d 100%);
		background: linear-gradient(to bottom,#55535e 0,#35383d 100%);
	}
	
	.top div span {
		display: inline-block;
    	line-height: 30px;
    	padding-left: 6px;
    	vertical-align: sub;
    	border-left: 2px #ccc solid;
    	color: #ccc;
	}
	
	.center{
		background:url('imgs/login/bk1.png') no-repeat;
		background-size:100%;
		flex:1;
	}
	
	.oper{margin:30% 0 0 34%;}
	.oper div{margin:0 0 0 0;width:12%;height:50px;float:left;cursor:pointer;
		opacity:.8;
	}
	.oper .btn1{background:url('imgs/login/btn.png') 0 0 no-repeat; margin-right: 22px;}
	.oper .btn2{background:url('imgs/login/btn.png') 0 0 no-repeat;}
	.oper .btn3{display: none;background:url('imgs/login/btn.png') 0 0 no-repeat;}
	.oper div:hover{opacity:1}
	</style>
	<script type="text/javascript">
	$(function(){
		setSize();
		
		$('.oper').find('div').click(function(){
			window.location.href = $(this).attr('url');
		});
		
	});
	$(window).resize(setSize);
	function setSize(){
		console.log($(".oper div").width());
		$(".oper div").css({"height":$(".oper div").width(),"background-size":$(".oper div").width()*3});
		$(".oper .btn2").css("background-position",$(".oper div").width()*(-1)+"px 0");
		$(".oper .btn3").css("background-position",$(".oper div").width()*(-2)+"px 0");
	}
	
	function toUrl(url){
		window.location.href = url;
	}
	
	</script>
</head>
<body>
	<div class="use-flex">
		<div class="top">
			<div style="margin:15px 0 0 25%;color:#ccc;">
				<img src="imgs/login/huatang.png" style="vertical-align:middle;"/>
				<span> 全国服务外包及联络中心/实训平台与专业建设的领航者</span>
			</div>
		</div>
		<div class="center">
			<div class="oper">
				<div class="btn1" url='/${projectName}/sysmanage/login/page.action?page=login&state=1' ></div>
				<div class="btn2" url='/${projectName}/sysmanage/login/page.action?page=login&state=2' ></div>
				<div class="btn3" url='/${projectName}/sysmanage/login/page.action?page=login&state=3' ></div>
			</div>
		</div>
	</div>
</body>
</html>