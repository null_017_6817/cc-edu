<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>问卷查询</title>
	<%@include file="/common/include.jsp" %>

<script>
$(function(){
	<%-- 为分页行设置"添加"和"删除"按钮 --%>
	var pager = $('#questGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});			
})

<%-- 执行查询，点击查询按钮时调用 --%>
function doSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#questGrid').datagrid('load',{
		title:$('#title').val(),
		totalscore:$('#totalscore').val(),
		effectivescore:$('#effectivescore').val()
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatOper(val,row,index){  
    return "<span onclick='updQuest("+val+")' style='text-decoration:underline;cursor:pointer;'>修改</span> "
    			+"<span onclick='vieQuest("+val+")' style='text-decoration:underline;cursor:pointer;'>查看</span>";
} 

<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
function updQuest(id){
	QMoveDialog($("#questEditWin"));
	$('#questViewWin').dialog('close');
	$('#questEditWin').dialog('open');
	$('#questEditWin').get(0).contentWindow.load(id);
} 

<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
function vieQuest(id){
	QMoveDialog($("#questViewWin"));
	$('#questEditWin').dialog('close');
	$('#questViewWin').dialog('open');
	$('#questViewWin').get(0).contentWindow.load(id);
}

<%-- 删除记录 --%>
function delQuest(){
	var checkedItems = $('#questGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/practical/quest/delQuest.action",{ids:ids},function(data){
				$('#questGrid').datagrid('reload');
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#questEditWin').dialog('close');
	$('#questGrid').datagrid('reload');
}
</script>
</head>
<body>
<table id="questGrid" title="问卷" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#questGridToolbar',collapsible:true,url:'/${projectName}/practical/quest/schQuest.action',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" ></th>
			<th data-options="field:'title',width:25,align:'center',sortable:true">标题</th>
			<th data-options="field:'totalscore',width:25,align:'center',sortable:true">总分</th>
			<th data-options="field:'effectivescore',width:25,align:'center',sortable:true">有效分值</th>
			<th data-options="field:'remark',width:25,align:'center',sortable:true">备注</th>

			<th data-options="field:'_oper',align:'center',formatter:formatOper">操作</th>
		</tr>
	</thead>
</table>
<%-- 查询条件设定栏 --%>
<div id="questGridToolbar">
	<div style="margin-bottom:5px">
		<a href="javascript:updQuest(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true">添加</a>
		<a href="javascript:delQuest()" class="easyui-linkbutton" iconCls="icon-remove" plain="true">删除</a>
	</div>
	<form id="_form">
	标题：<input name='title' id="title" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	总分：<input name='totalscore' id="totalscore" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	有效分值：<input name='effectivescore' id="effectivescore" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>

	<a href="#" class="easyui-linkbutton" iconCls="icon-search" onclick="doSearch()">查询</a>
	</form>
</div>


<%-- 修改or添加面板 --%>
<iframe id="questEditWin" src="/${projectName}/practical/quest/page.action?page=quest_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#questEditButton'" title="修改问卷" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="no" frameborder="0"></iframe>
<div id="questEditButton">
	<a href="javascript:void(0)" onclick="$('#questEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#questEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >取 消</a>
</div>
<%-- 查看面板 --%>
<iframe id="questViewWin" src="/${projectName}/practical/quest/page.action?page=quest_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#questViewButton'" title="查看问卷" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="questViewButton">
	<a href="javascript:$('#questViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >关 闭</a>
</div>
</body>
</html>
