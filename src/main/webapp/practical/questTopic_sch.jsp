<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>问卷题目查询</title>
	<%@include file="/common/include.jsp" %>

<script>
$(function(){
	<%-- 为分页行设置"添加"和"删除"按钮 --%>
	var pager = $('#questTopicGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});			
})

<%-- 执行查询，点击查询按钮时调用 --%>
function doSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#questTopicGrid').datagrid('load',{
		title:$('#title').val(),
		content:$('#content').val(),
		score:$('#score').val()
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatOper(val,row,index){  
    return "<span onclick='updQuestTopic("+val+")' style='text-decoration:underline;cursor:pointer;'>修改</span> "
    			+"<span onclick='vieQuestTopic("+val+")' style='text-decoration:underline;cursor:pointer;'>查看</span>";
} 

<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
function updQuestTopic(id){
	QMoveDialog($("#questTopicEditWin"));
	$('#questTopicViewWin').dialog('close');
	$('#questTopicEditWin').dialog('open');
	$('#questTopicEditWin').get(0).contentWindow.load(id);
} 

<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
function vieQuestTopic(id){
	QMoveDialog($("#questTopicViewWin"));
	$('#questTopicEditWin').dialog('close');
	$('#questTopicViewWin').dialog('open');
	$('#questTopicViewWin').get(0).contentWindow.load(id);
}

<%-- 删除记录 --%>
function delQuestTopic(){
	var checkedItems = $('#questTopicGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/practical/questTopic/delQuestTopic.action",{ids:ids},function(data){
				$('#questTopicGrid').datagrid('reload');
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#questTopicEditWin').dialog('close');
	$('#questTopicGrid').datagrid('reload');
}
</script>
</head>
<body>
<table id="questTopicGrid" title="问卷题目" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#questTopicGridToolbar',collapsible:true,url:'/${projectName}/practical/questTopic/schQuestTopic.action',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" ></th>
			<th data-options="field:'title',width:25,align:'center',sortable:true">标题</th>
			<th data-options="field:'content',width:25,align:'center',sortable:true">内容</th>
			<th data-options="field:'score',width:25,align:'center',sortable:true">得分</th>

			<th data-options="field:'_oper',align:'center',formatter:formatOper">操作</th>
		</tr>
	</thead>
</table>
<%-- 查询条件设定栏 --%>
<div id="questTopicGridToolbar">
	<div style="margin-bottom:5px">
		<a href="javascript:updQuestTopic(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true">添加</a>
		<a href="javascript:delQuestTopic()" class="easyui-linkbutton" iconCls="icon-remove" plain="true">删除</a>
	</div>
	<form id="_form">
	标题：<input name='title' id="title" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	内容：<input name='content' id="content" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	得分：<input name='score' id="score" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>

	<a href="#" class="easyui-linkbutton" iconCls="icon-search" onclick="doSearch()">查询</a>
	</form>
</div>


<%-- 修改or添加面板 --%>
<iframe id="questTopicEditWin" src="/${projectName}/practical/questTopic/page.action?page=questTopic_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#questTopicEditButton'" title="修改问卷题目" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="no" frameborder="0"></iframe>
<div id="questTopicEditButton">
	<a href="javascript:void(0)" onclick="$('#questTopicEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#questTopicEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >取 消</a>
</div>
<%-- 查看面板 --%>
<iframe id="questTopicViewWin" src="/${projectName}/practical/questTopic/page.action?page=questTopic_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#questTopicViewButton'" title="查看问卷题目" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="questTopicViewButton">
	<a href="javascript:$('#questTopicViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >关 闭</a>
</div>
</body>
</html>
