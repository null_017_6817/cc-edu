<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>查看问卷题目</title>
	<%@include file="/common/include.jsp" %>
<script>
function load(id){
	$('#_form').form('load','/${projectName}/practical/questTopic/vieQuestTopic.action?toL=true&id='+id);
};
</script>
</head>
<body>
<form id="_form">
	<input type="hidden" name='id' value=""/>
	<table style="margin: 0 auto;">
		<tr><td class='label'>标题：</td><td><input type='text' name='title' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>内容：</td><td><input type='text' name='content' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>得分：</td><td><input type='text' name='score' class='viewbox' readonly='readonly'/></td></tr>
	</table>

</form>
</body>
</html>
