<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>客户工单</title>
	<%@include file="/common/include.jsp" %>
<style>
.black_overlay{ 
	display: none; 
	position: absolute; 
	top: 0%; 
	left: 0%; 
	width: 100%; 
	height: 100%; 
	background-color: black; 
	z-index:101; 
	-moz-opacity: 0.3; 
	opacity:0.3; 
	filter: alpha(opacity=88); 
} 
.white_content {
	display: none; 
	position: absolute; 
	top: 20%; 
	left: 40%; 
	width: 13%; 
	height: 35%; 
	padding: 20px; 
	border: 5px solid grey; 
	background-color: white; 
	z-index:102; 
	overflow: auto; 
}
</style>


<script>

$(function(){
	//弹出按钮
	$("#orderId").click(function(){
		$("#fade").show();
		$("#orderDiv").show();
		loadData($("select[name='profession']").val());
	});
	//关闭按钮
	$("#closeId").click(function(){
		$("#orderDiv").hide();
		$("#fade").hide();
	});
	// 下拉框绑定方法
	$("select[name='profession']").change(function(){
		var profession = $(this).val();
		if(profession != ""){
			loadData($(this).val());
			$("#dataTable").show();
		}else{
			$("#dataTable").hide();
		}
	});
	//保存
	$("#saveId").click(function(){
		saveData();
	});
	// 拨号
	$("#callId").click(function(){
		preCall();
	});
	
});

// 加载数据
function loadData( profession ){
	$.ajax({
		type:"POST",
		url:'/${projectName}/practical/workorder/serchCusOrder.action',
		data:{"profession":profession},
		dataType:'json',
		success:function(result){
			if(result != null && result.data != null){
				var entity = result.data;
				$("input[name='id']").val(entity.id);
				$("input[name='userid']").val(entity.userid);
				$("input[name='name']").val(entity.name);
				$("input[name='sex']").val(entity.sex);
				$("input[name='telphone']").val(entity.telphone);
				$("input[name='company']").val(entity.company);
				$("input[name='profession']").val(entity.profession);
				$("input[name='addr']").val(entity.addr);
				$("input[name='zipcode']").val(entity.zipcode);
				$("input[name='email']").val(entity.email);
				$("input[name='belong']").val(entity.belong);
			}
		}
	});
}

// 保存数据
function saveData(){
	var param = $("#orderForm").serialize();
	$.ajax({
		type:"POST",
		url:'/${projectName}/practical/workorder/saveClient.action',
		data:param,
		dataType:'json',
		success:function(result){
			if(result != null && result.success){
				var id = $("input[name='id']").val();
				if(id == null || id == ""){
					$("input[name='id']").val(result.id);
				}
				alert(result.msg);
			}
		}
	});
}

// 拨号
/* function preCall(){
	var profession = $("select[name='profession']").val();
	var agentId = $("input[name='agentId']").val();
	var id = $("input[name='id']").val();
	if(profession == null || profession == ""){
		alert("请选择则行业");
		return false;
	}
	if(agentId == null || agentId == ""){
		alert("请填写您要拨打的坐席号");
		return false;
	}
	if(id == null || id == ""){
		alert("请先保存您的数据");
		return false;
	}
	$.ajax({
		type:"POST",
		url:'/${projectName}/practical/workorder/preCall.action',
		data:{"profession":profession,"agentId":agentId},
		dataType:'json',
		success:function(result){
			if(result != null ){
				if( result.success ){
					
				}else{
					
				}
				alert(result.msg);
			}
		}
	});
} */

</script>
</head>
<body >
<div id="fade" class="black_overlay"></div> 
	<div align="center">
		<input name="agentId" type="text" value="1102" placeholder="请输入坐席号码"/>
		<button id="orderId">工单 </button>
		<!-- <button id="callIdBtn">拨打 </button> -->
	</div>
<hr>
	<div id="orderDiv" class="white_content" >
		<span style="float:left "><font size='3'>客户信息</font></span>
		<br/><hr>
		<form id="orderForm" action="/${projectName}/" method="post">
			<input type="hidden" name="id" />
			<input type="hidden" name="userid" />
			<div align="center">
			行业 : <select name="profession" >
				<option value="">请选择则</option>
				<option value="1">测试行业1</option>
				<option value="2">测试行业2</option>
				<option value="3">测试行业3</option>
			</select>
			</div>
			<br/><hr>
			<table align="center" id="dataTable" style="display:none;" border="1" style="border:1px solid #00F">
				<tr ><td style="text-align:right;"> 姓名 : </td><td><input type="text" name="name" value=""/></td></tr>
				<tr ><td style="text-align:right;"> 性别 : </td><td><input type="text" name="sex" name="" value=""/></td></tr>
				<tr ><td style="text-align:right;"> 电话 : </td><td><input type="text" name="telphone" value=""/></td></tr>
				<tr ><td style="text-align:right;"> 公司 : </td><td><input type="text" name="company" value=""/></td></tr>
				<tr ><td style="text-align:right;"> 地址 : </td><td><input type="text" name="addr" value=""/></td></tr>
				<tr ><td style="text-align:right;"> 邮编 : </td><td><input type="text" name="zipcode" value=""/></td></tr>
				<tr ><td style="text-align:right;"> 邮箱 : </td><td><input type="text" name="email" value=""/></td></tr>
				<tr ><td style="text-align:right;"> 归属地 : </td><td><input type="text" name="belong" value=""/></td></tr>
			</table>
		</form>
		<hr>
		<div align="center">
			<button type="button" id="saveId">保存 </button>
			<button type="button" id="callId">拨号</button>
			<button type="button" id="closeId">关闭 </button>
		</div>
	</div>
</body>
</html>