<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>客户卡片查询</title>
	<%@include file="/common/include.jsp" %>

<script>
$(function(){
	<%-- 为分页行设置"添加"和"删除"按钮 --%>
	var pager = $('#trainClientGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});			
})

<%-- 执行查询，点击查询按钮时调用 --%>
function doSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#trainClientGrid').datagrid('load',{
		name:$('#name').val(),
		telphone:$('#telphone').val(),
		company:$('#company').val(),
		profession:$('#profession').val(),
		addr:$('#addr').val(),
		zipcode:$('#zipcode').val(),
		email:$('#email').val(),
		belong:$('#belong').val()
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatOper(val,row,index){  
    return "<span onclick='updTrainClient("+val+")' style='text-decoration:underline;cursor:pointer;'>修改</span> "
    			+"<span onclick='vieTrainClient("+val+")' style='text-decoration:underline;cursor:pointer;'>查看</span>";
} 

<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
function updTrainClient(id){
	QMoveDialog($("#trainClientEditWin"));
	$('#trainClientViewWin').dialog('close');
	$('#trainClientEditWin').dialog('open');
	$('#trainClientEditWin').get(0).contentWindow.load(id);
} 

<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
function vieTrainClient(id){
	QMoveDialog($("#trainClientViewWin"));
	$('#trainClientEditWin').dialog('close');
	$('#trainClientViewWin').dialog('open');
	$('#trainClientViewWin').get(0).contentWindow.load(id);
}

<%-- 删除记录 --%>
function delTrainClient(){
	var checkedItems = $('#trainClientGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/practical/trainClient/delTrainClient.action",{ids:ids},function(data){
				$('#trainClientGrid').datagrid('reload');
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#trainClientEditWin').dialog('close');
	$('#trainClientGrid').datagrid('reload');
}
</script>
</head>
<body>
<table id="trainClientGrid" title="客户卡片" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#trainClientGridToolbar',collapsible:true,url:'/${projectName}/practical/trainClient/schTrainClient.action',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" ></th>
			<th data-options="field:'userid',width:25,align:'center',sortable:true">用户</th>
			<th data-options="field:'name',width:25,align:'center',sortable:true">姓名</th>
			<th data-options="field:'sex',width:25,align:'center',sortable:true">性别</th>
			<th data-options="field:'telphone',width:25,align:'center',sortable:true">电话</th>
			<th data-options="field:'company',width:25,align:'center',sortable:true">公司名称</th>
			<th data-options="field:'profession',width:25,align:'center',sortable:true">行业类别</th>
			<th data-options="field:'addr',width:25,align:'center',sortable:true">地址</th>
			<th data-options="field:'zipcode',width:25,align:'center',sortable:true">邮编</th>
			<th data-options="field:'email',width:25,align:'center',sortable:true">电子邮箱</th>
			<th data-options="field:'belong',width:25,align:'center',sortable:true">归属地</th>

			<th data-options="field:'_oper',align:'center',formatter:formatOper">操作</th>
		</tr>
	</thead>
</table>
<%-- 查询条件设定栏 --%>
<div id="trainClientGridToolbar">
	<div style="margin-bottom:5px">
		<a href="javascript:updTrainClient(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true">添加</a>
		<a href="javascript:delTrainClient()" class="easyui-linkbutton" iconCls="icon-remove" plain="true">删除</a>
	</div>
	<form id="_form">
	姓名：<input name='name' id="name" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	电话：<input name='telphone' id="telphone" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	公司名称：<input name='company' id="company" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	行业类别：<input name='profession' id="profession" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	地址：<input name='addr' id="addr" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	邮编：<input name='zipcode' id="zipcode" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	电子邮箱：<input name='email' id="email" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	归属地：<input name='belong' id="belong" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>

	<a href="#" class="easyui-linkbutton" iconCls="icon-search" onclick="doSearch()">查询</a>
	</form>
</div>


<%-- 修改or添加面板 --%>
<iframe id="trainClientEditWin" src="/${projectName}/practical/trainClient/page.action?page=trainClient_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#trainClientEditButton'" title="修改客户卡片" style="width:700px;height:500px;padding:5px;top:5000px" scrolling="no" frameborder="0"></iframe>
<div id="trainClientEditButton">
	<a href="javascript:void(0)" onclick="$('#trainClientEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#trainClientEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >取 消</a>
</div>
<%-- 查看面板 --%>
<iframe id="trainClientViewWin" src="/${projectName}/practical/trainClient/page.action?page=trainClient_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#trainClientViewButton'" title="查看客户卡片" style="width:700px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="trainClientViewButton">
	<a href="javascript:$('#trainClientViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >关 闭</a>
</div>
</body>
</html>
