<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>工单查询</title>
	<%@include file="/common/include.jsp" %>

<script>
$(function(){
	<%-- 为分页行设置"添加"和"删除"按钮 --%>
	var pager = $('#trainBillGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});			
})

<%-- 执行查询，点击查询按钮时调用 --%>
function doSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#trainBillGrid').datagrid('load',{
		number:$('#number').val(),
		userid:$('#userid').val(),
		dialing:$('#dialing').val(),
		contact:$('#contact').val(),
		customer:$('#customer').val(),
		business:$('#business').val()
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatOper(val,row,index){  
    return "<span onclick='updTrainBill("+val+")' style='text-decoration:underline;cursor:pointer;'>修改</span> "
    			+"<span onclick='vieTrainBill("+val+")' style='text-decoration:underline;cursor:pointer;'>查看</span>";
} 

<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
function updTrainBill(id){
	QMoveDialog($("#trainBillEditWin"));
	$('#trainBillViewWin').dialog('close');
	$('#trainBillEditWin').dialog('open');
	$('#trainBillEditWin').get(0).contentWindow.load(id);
} 

<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
function vieTrainBill(id){
	QMoveDialog($("#trainBillViewWin"));
	$('#trainBillEditWin').dialog('close');
	$('#trainBillViewWin').dialog('open');
	$('#trainBillViewWin').get(0).contentWindow.load(id);
}

<%-- 删除记录 --%>
function delTrainBill(){
	var checkedItems = $('#trainBillGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/practical/trainBill/delTrainBill.action",{ids:ids},function(data){
				$('#trainBillGrid').datagrid('reload');
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#trainBillEditWin').dialog('close');
	$('#trainBillGrid').datagrid('reload');
}
</script>
</head>
<body>
<table id="trainBillGrid" title="工单" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#trainBillGridToolbar',collapsible:true,url:'/${projectName}/practical/trainBill/schTrainBill.action',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" ></th>
			<th data-options="field:'number',width:25,align:'center',sortable:true">工单号</th>
			<th data-options="field:'userid',width:25,align:'center',sortable:true">用户</th>
			<th data-options="field:'tm',width:25,align:'center',sortable:true">呼入时间</th>
			<th data-options="field:'dialing',width:25,align:'center',sortable:true">主叫</th>
			<th data-options="field:'contact',width:25,align:'center',sortable:true">联系电话</th>
			<th data-options="field:'customer',width:25,align:'center',sortable:true">客户名称</th>
			<th data-options="field:'business',width:25,align:'center',sortable:true">业务类型</th>
			<th data-options="field:'result',width:25,align:'center',sortable:true">处理结果</th>
			<th data-options="field:'descrip',width:25,align:'center',sortable:true">描述</th>

			<th data-options="field:'_oper',align:'center',formatter:formatOper">操作</th>
		</tr>
	</thead>
</table>
<%-- 查询条件设定栏 --%>
<div id="trainBillGridToolbar">
	<div style="margin-bottom:5px">
		<a href="javascript:updTrainBill(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true">添加</a>
		<a href="javascript:delTrainBill()" class="easyui-linkbutton" iconCls="icon-remove" plain="true">删除</a>
	</div>
	<form id="_form">
	工单号：<input name='number' id="number" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	用户：<input name='userid' id="userid" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	主叫：<input name='dialing' id="dialing" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	联系电话：<input name='contact' id="contact" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	客户名称：<input name='customer' id="customer" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	业务类型：<input name='business' id="business" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>

	<a href="#" class="easyui-linkbutton" iconCls="icon-search" onclick="doSearch()">查询</a>
	</form>
</div>


<%-- 修改or添加面板 --%>
<iframe id="trainBillEditWin" src="/${projectName}/practical/trainBill/page.action?page=trainBill_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#trainBillEditButton'" title="修改工单" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="no" frameborder="0"></iframe>
<div id="trainBillEditButton">
	<a href="javascript:void(0)" onclick="$('#trainBillEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#trainBillEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >取 消</a>
</div>
<%-- 查看面板 --%>
<iframe id="trainBillViewWin" src="/${projectName}/practical/trainBill/page.action?page=trainBill_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#trainBillViewButton'" title="查看工单" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="trainBillViewButton">
	<a href="javascript:$('#trainBillViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >关 闭</a>
</div>
</body>
</html>
