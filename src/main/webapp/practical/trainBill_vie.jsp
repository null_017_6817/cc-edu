<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>查看工单</title>
	<%@include file="/common/include.jsp" %>
<script>
function load(id){
	$('#_form').form('load','/${projectName}/practical/trainBill/vieTrainBill.action?toL=true&id='+id);
};
</script>
</head>
<body>
<form id="_form">
	<input type="hidden" name='id' value=""/>
	<table style="margin: 0 auto;">
		<tr><td class='label'>工单号：</td><td><input type='text' name='number' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>用户：</td><td><input type='text' name='userid' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>呼入时间：</td><td><input type='text' name='tm' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>主叫：</td><td><input type='text' name='dialing' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>联系电话：</td><td><input type='text' name='contact' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>客户名称：</td><td><input type='text' name='customer' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>业务类型：</td><td><input type='text' name='business' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>处理结果：</td><td><input type='text' name='result' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>描述：</td><td><input type='text' name='descrip' class='viewbox' readonly='readonly'/></td></tr>
	</table>

</form>
</body>
</html>
