<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>修改问卷</title>
	<%@include file="/common/include.jsp" %>
<script>
<%-- 查询页点击修改时调用此方法加载记录详情 --%>
function load(id){
	$('#_form').form('load','/${projectName}/practical/quest/vieQuest.action?id='+id);
};

<%-- 点击保存按钮时调用此方法。(注:保存按钮在查询列表页) --%>
function submitForm(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#_form').form('submit',{
      success:function(data) {
      	parent.reloadData();
      }
   });
}
</script>
</head>
<body>
<form id="_form" method="post" action="/${projectName}/practical/quest/updQuest.action">
	<input type="hidden" name='id' id='id'/>
	<table>
		<tr><td class='label' style="width: 168px">标题：</td><td><input id="title" name="title" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label'>总分：</td><td><input id="totalscore" name="totalscore" class="easyui-textbox" data-options="required:true,validType:['int']"/></td></tr>
		<tr><td class='label'>有效分值：</td><td><input id="effectivescore" name="effectivescore" class="easyui-textbox" data-options="required:true,validType:['int']"/></td></tr>
		<tr><td class='label'>备注：</td><td><input id="remark" name="remark" class="easyui-textbox" data-options="required:true"/></td></tr>
	</table>

</form>
</body>
</html>
