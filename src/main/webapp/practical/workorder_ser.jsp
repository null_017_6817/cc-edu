<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>客服工单</title>
	<%@include file="/common/include.jsp" %>
	
	
<style>
.black_overlay{ 
	display: none; 
	position: absolute; 
	top: 0%; 
	left: 0%; 
	width: 100%; 
	height: 100%; 
	background-color: black; 
	z-index:101; 
	-moz-opacity: 0.3; 
	opacity:0.3; 
	filter: alpha(opacity=88); 
} 
.white_content { 
	display: none; 
	position: absolute; 
	top: 15%; 
	left: 30%; 
	width: 40%; 
	height: 60%; 
	padding: 20px; 
	border: 5px solid grey; 
	background-color: white; 
	z-index:102; 
	overflow: auto; 
}
.highDiv {
	display: none; 
	position: absolute; 
	top: 20%; 
	left: 40%; 
	width: 200px; 
	height: 200px; 
	padding: 20px; 
	border: 5px solid grey; 
	background-color: white; 
	z-index:102; 
	overflow: auto; 
}

table, th, td
{
	border: 2px solid grey;
	height:25px;
}

table{
	width:100%;
}

.labelText{
	text-align:right;
	background-color:#9BCD9B;
	width:100px;
}
th{
	background-color:#9BCD9B;
	text-align:center;
}
</style>
<script>
$(function(){
	//弹出按钮
	$("#orderId").click(function(){
		loadData();
	});
	
	//关闭按钮
	$("#closeId").click(function(){
		$("#orderDiv").hide();
		$("#fade").hide();
	});
	//添加bill
	$("#add").click(function(){
		var id = $("input[name='id']").val();
		if(id != null && id != "" && typeof(id) != "undefined"){
			$("#addBill").show();
			$("#orderDiv").hide();
		}else{
			alert('禁止添加');
		}
	});
	//保存bill
	$("#billSave").click(function(){
		saveBill();
	});
	// 挂断电话
	$("#hungId").click(function(){
		preHungUp();
	});
	
	$("#billClose").click(function(){
		$("#addBill").hide();
		$("#orderDiv").show();
	});
	
});

// 加载数据
function loadData(){
	$.ajax({
		type:"POST",
		url:'/${projectName}/practical/workorder/serSearchCus.action',
		data:{},
		dataType:'json',
		success:function(result){
			
			if(result != null && result.data != null){
				$("#orderDiv").show();
				$("#fade").show();
				
				var entity = result.data;
				if(entity.profession != null){
					$("#cusTable").find("select[name='profession']").val(entity.profession);
				}
				if(entity.id != null){
					$("#cusTable").find("input[name='id']").val(entity.id);
				}
				if( entity.userid != null){
					$("#cusTable").find("input[name='userid']").val(entity.userid);
				}
				if(entity.name != null){
					$("#cusTable").find("td[name='name']").text(entity.name);
				}
				if(entity.sex != null){
					$("#cusTable").find("td[name='sex']").text(entity.sex);
				}
				if(entity.telphone != null){
					$("#cusTable").find("td[name='telphone']").text(entity.telphone);
				}
				if(entity.company != null){
					$("#cusTable").find("td[name='company']").text(entity.company);
				}
				if(entity.profession != null){
					$("#cusTable").find("td[name='profession']").text(entity.profession);
				}
				if(entity.addr != null){
					$("#cusTable").find("td[name='addr']").text(entity.addr);
				}
				if(entity.zipcode != null){
					$("#cusTable").find("td[name='zipcode']").text(entity.zipcode);
				}
				if(entity.email != null){
					$("#cusTable").find("td[name='email']").text(entity.email);
				}
				if(entity.belong != null){
					$("#cusTable").find("td[name='belong']").text(entity.belong);
				}
			}else{
				/* $("#orderDiv").hide();
				$("#fade").hide(); */
				alert("没有电话");
			}
		}
	});
	// 加载bill数据
	loadBill();
}

// 加载bill数据
function loadBill(){
	// 清空tbody
	$("#billTable").find('tbody').empty();
	$.ajax({
		type:"POST",
		url:'/${projectName}/practical/workorder/serchBill.action',
		data:{},
		dataType:'json',
		success:function(result){
			if(result != null && result.rows != null){
				var trHtml = "";
				var rows = result.rows;
				for( var i = 0; i < rows.length; i++ ){
					var tr = "<tr style='border:1px solid #00F;'>"
							+	"<td>"+rows[i].number+"</td>"
							+	"<td>"+rows[i].dialing+"</td>"
							+	"<td>"+rows[i].tm+"</td>"
							+	"<td>"+rows[i].business+"</td>"
							+	"<td>"+rows[i].descrip+"</td>"
							+	"<td>"+rows[i].userid+"</td>"
							+"</tr>";
					trHtml += tr;
				}
				$("#billTable").find('tbody').html(trHtml);
			}
		}
	});
}

function saveBill(){
	var param = $("#billAddForm").serialize();
	$.ajax({
		type:"POST",
		url:'/${projectName}/practical/workorder/saveBill.action',
		data:param,
		dataType:'json',
		success:function(result){
			if(result.success){
				$("#addBill").hide();
				$("#orderDiv").show();
				loadBill();
			}else{
				alert(result.msg);
			}
		}
	});
	
}
// 挂断电话
function preHungUp(){
	
	$.ajax({
		type:"POST",
		url:'/${projectName}/practical/workorder/preHungUp.action',
		data:{},
		dataType:'json',
		success:function(result){
			if(result.success){
				$("#addBill").hide();
				$("#orderDiv").hide();
				$("#fade").hide();
			}else{
				alert(result.msg);
			}
		}
	});
}

</script>
</head>
<body >
	
	<div id="fade" class="black_overlay"></div> 
	
	<div align="center">
		<!-- <input name="agentId" type="text" value="1102" placeholder="请输入坐席号码"/> -->
		<button id="orderId">接电话 </button>
		<!-- <button id="callIdBtn">拨打 </button> -->
	</div>
	<hr>
	<div id="orderDiv" class="white_content" >
		<span style="float:left "><font size='3'>客户信息</font></span>
		<span style="float:right "><a id="closeId" href="javascript:void(0)"><font size='4'>×</font></a></span>
		<br/><hr>
			<!-- 客户信息table -->
			<table id="cusTable" >
				<input type="hidden" name="id" />
				<input type="hidden" name="userid" />
				<tr>	
					<td class="labelText">行业: </td>
					<td> <select name="profession" >
						<option value="">请选择则</option>
						<option value="1">测试行业1</option>
						<option value="2">测试行业2</option>
						<option value="3">测试行业3</option>
						</select>
					</td>
				</tr>
				<tr >
					<td class="labelText"> 姓名 : </td><td name="name"></td>
					<td class="labelText"> 性别 : </td><td name="sex"></td>
				</tr>
				<tr >
					<td class="labelText"> 电话 : </td><td name="telphone"></td>
					<td class="labelText"> 公司 : </td><td name="company"></td>
				</tr>
				<tr >
					<td class="labelText"> 地址 : </td><td name="addr"></td>
					<td class="labelText"> 邮编 : </td><td name="zipcode"></td>
				</tr>
				<tr >
					<td class="labelText"> 邮箱 : </td><td name="email"></td>
					<td class="labelText"> 归属地 : </td><td name="belong"></td>
				</tr>
			</table>
			<br/>
			<span style="float:left "><font size='3'>记录信息</font></span>
			<br/>
			<hr>
			<!-- bill列表 -->
			<table id="billTable" style="text-align:center;width:100%;border:1px solid #00F">
				<thead>
					<tr>
						<th>工单号</th>
						<th>主叫</th>
						<th>时间</th>
						<th>处理类型</th>
						<th>描述</th>
						<th>受理人</th>
					</tr>
				</thead>
				<tbody style="border:1px solid #00F;">
				</tbody>
			</table>
		<div align="center">
			<button type="button" id="add">添加</button>
			<button type="button" id="hungId">挂断</button>
		</div>
	</div>
	
	<!-- 添加bill -->
	<div id="addBill" align="center" class="highDiv">
		<span style="float:left "><font size='3'>添加</font></span>
		<br/><hr>
		<form id="billAddForm">
			<table id="addBillTable">
				<tr><td class="labelText">类型 :</td><td> <input  type="text" name="business"/> </td></tr>
				<tr><td class="labelText">单号 :</td><td> <input  type="text" name="number"/> </td></tr>
				<tr><td class="labelText">电话 :</td><td> <input  type="text" name="contact"/> </td></tr>
				<tr><td class="labelText">结果 :</td><td> <input  type="text" name="result"/> </td></tr>
				<tr><td class="labelText">描述 :</td><td> <input  type="text" name="descrip"/> </td></tr>
			</table>
		</form>
		<hr>
		<button id="billSave" type="button">保存</button>
		<button id="billClose" type="button">取消</button>
	</div>
</body>
</html>