<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>查看客户卡片</title>
	<%@include file="/common/include.jsp" %>
<script>
function load(id){
	$('#_form').form('load','/${projectName}/practical/trainClient/vieTrainClient.action?toL=true&id='+id);
};
</script>
</head>
<body>
<form id="_form">
	<input type="hidden" name='id' value=""/>
	<table style="margin: 0 auto;">
		<tr>
			<td class='label'>
			用户：
			</td>
			<td>
			<input type='text' name='userid' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			姓名：
			</td>
			<td>
				<input type='text' name='name' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			性别：
			</td>
			<td>
			<input type='text' name='sex' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			电话：
			</td>
			<td>
				<input type='text' name='telphone' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			公司名称：
			</td>
			<td>
			<input type='text' name='company' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			行业类别：
			</td>
			<td>
				<input type='text' name='profession' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			地址：
			</td>
			<td>
			<input type='text' name='addr' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			邮编：
			</td>
			<td>
				<input type='text' name='zipcode' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			电子邮箱：
			</td>
			<td>
			<input type='text' name='email' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			归属地：
			</td>
			<td>
				<input type='text' name='belong' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
	</table>

</form>
</body>
</html>
