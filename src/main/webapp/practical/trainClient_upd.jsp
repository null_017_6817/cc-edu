<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>修改客户卡片</title>
	<%@include file="/common/include.jsp" %>
<script>
<%-- 查询页点击修改时调用此方法加载记录详情 --%>
function load(id){
	$('#_form').form('load','/${projectName}/practical/trainClient/vieTrainClient.action?id='+id);
};

<%-- 点击保存按钮时调用此方法。(注:保存按钮在查询列表页) --%>
function submitForm(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#_form').form('submit',{
      success:function(data) {
      	parent.reloadData();
      }
   });
}
</script>
</head>
<body>
<form id="_form" method="post" action="/${projectName}/practical/trainClient/updTrainClient.action">
	<input type="hidden" name='id' id='id'/>
	<table>
		<tr>
			<td class='label' style="width: 150px">
			用户：
			</td>
			<td>
			<input id="userid" name="userid" class="easyui-textbox" data-options="required:true,validType:['int']"/>
			</td>
			<td class='label' style="width: 150px">
			姓名：
			</td>
			<td>
				<input id="name" name="name" class="easyui-textbox" data-options="required:true"/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			性别：
			</td>
			<td>
			<input id="sex" name="sex" class="easyui-textbox" data-options="required:true"/>
			</td>
			<td class='label'>
			电话：
			</td>
			<td>
				<input id="telphone" name="telphone" class="easyui-textbox" data-options="required:true,validType:['tel']"/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			公司名称：
			</td>
			<td>
			<input id="company" name="company" class="easyui-textbox" data-options="required:true"/>
			</td>
			<td class='label'>
			行业类别：
			</td>
			<td>
				<input id="profession" name="profession" class="easyui-textbox" data-options="required:true"/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			地址：
			</td>
			<td>
			<input id="addr" name="addr" class="easyui-textbox" data-options="required:true"/>
			</td>
			<td class='label'>
			邮编：
			</td>
			<td>
				<input id="zipcode" name="zipcode" class="easyui-textbox" data-options="required:true,validType:['zip']"/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			电子邮箱：
			</td>
			<td>
			<input id="email" name="email" class="easyui-textbox" data-options="required:true,validType:['email']"/>
			</td>
			<td class='label'>
			归属地：
			</td>
			<td>
				<input id="belong" name="belong" class="easyui-textbox" data-options="required:true"/>
			</td>
		</tr>
	</table>

</form>
</body>
</html>
