<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>查看问卷</title>
	<%@include file="/common/include.jsp" %>
<script>
function load(id){
	$('#_form').form('load','/${projectName}/practical/quest/vieQuest.action?toL=true&id='+id);
};
</script>
</head>
<body>
<form id="_form">
	<input type="hidden" name='id' value=""/>
	<table style="margin: 0 auto;">
		<tr><td class='label'>标题：</td><td><input type='text' name='title' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>总分：</td><td><input type='text' name='totalscore' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>有效分值：</td><td><input type='text' name='effectivescore' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>备注：</td><td><input type='text' name='remark' class='viewbox' readonly='readonly'/></td></tr>
	</table>

</form>
</body>
</html>
