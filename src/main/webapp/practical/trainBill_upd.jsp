<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>修改工单</title>
	<%@include file="/common/include.jsp" %>
<script>
<%-- 查询页点击修改时调用此方法加载记录详情 --%>
function load(id){
	$('#_form').form('load','/${projectName}/practical/trainBill/vieTrainBill.action?id='+id);
};

<%-- 点击保存按钮时调用此方法。(注:保存按钮在查询列表页) --%>
function submitForm(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#_form').form('submit',{
      success:function(data) {
      	parent.reloadData();
      }
   });
}
</script>
</head>
<body>
<form id="_form" method="post" action="/${projectName}/practical/trainBill/updTrainBill.action">
	<input type="hidden" name='id' id='id'/>
	<table>
		<tr><td class='label' style="width: 168px">工单号：</td><td><input id="number" name="number" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label'>用户：</td><td><input id="userid" name="userid" class="easyui-textbox" data-options="required:true,validType:['int']"/></td></tr>
		<tr><td class='label'>呼入时间：</td><td><input id="tmstr" name="tmstr" class="easyui-datebox" data-options="required:true"/></td></tr>
		<tr><td class='label'>主叫：</td><td><input id="dialing" name="dialing" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label'>联系电话：</td><td><input id="contact" name="contact" class="easyui-textbox" data-options="required:true,validType:['tel']"/></td></tr>
		<tr><td class='label'>客户名称：</td><td><input id="customer" name="customer" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label'>业务类型：</td><td><input id="business" name="business" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label'>处理结果：</td><td><input id="result" name="result" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label'>描述：</td><td><input id="descrip" name="descrip" class="easyui-textbox" data-options="required:true"/></td></tr>
	</table>

</form>
</body>
</html>
