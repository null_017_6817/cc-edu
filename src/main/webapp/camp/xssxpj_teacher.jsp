<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>教师评价</title>
<!--trainCamp-->
<link rel="stylesheet" href="/${projectName}/book/js/lib/layui2.3/css/layui.css">
<link rel="stylesheet" href="/${projectName}/book/css/style.css">
</head>
<style>
   .height10{
      height:6px;
   }
   .layui-table .xss-td{
      padding:9px 0;
   }
</style>
<body>
	<h2 class="teac_title">教师评价</h2>
	
		<form class="layui-form teac_form" action="">
			<div class="layui-form-item">
			    <label class="layui-form-label">选择班级：</label>
			    <div class="layui-input-block">
			        <select name="classes" lay-verify="required"  id="bj" lay-filter="classes">
				        <option value="-1"></option>
			        </select>
			    </div>
			</div>
			<div class="layui-form-item">
			    <label class="layui-form-label">评分状态：</label>
			    <div class="layui-input-block">
			        <select name="status" lay-verify="required" lay-filter="status" class="status">
				        <option value="-1">全部</option>
				        <option value="0">未评分</option>
				        <option value="1">已评分</option>
			        </select>
			    </div>
			</div>
		</form>
		<div class="teac_table">
			<table class="layui-table">
			    <colgroup>
				    <col>
				    <col>
				    <col>
				    <col>
				    <col>
				    <col>
				    <col>
				    <col>
				    <col>
			    </colgroup>
			    <tbody class="stus">
				    
				    <%-- <tr>
				        <td>
				        	<div class="teac_img_head">
								<img src="/${projectName}/book/img/teac_head.png" alt="">
				        	</div>
				        </td>
				        <td>李四李四</td>
				        <td>1000000000</td>
				        <td><span class="teac_p">已评分</span></td>
				        <td>
							<span class="teac_score_left">100</span>
							<span class="teac_score_right">分</span>
				        </td>
				        <td>
							<a href="#" class="teac_tz">导出脚本</a>
				        </td>
				        <td>
							<a href="#" class="teac_tz">录音（）</a>
				        </td>
				        <td>
							<a href="#" class="teac_tz">工单（）</a>
				        </td>
				    </tr> --%>
			    </tbody>
			</table>
		</div>
	
</body>
<script type="text/javascript" src="/${projectName}/book/js/lib/jquery.min.js"></script>
<script type="text/javascript" src="/${projectName}/book/js/lib/layui2.3/layui.all.js"></script>
<script type="text/javascript">
var topicId=0;
$(function(){
	showClasses();
	topicId=parent.reTopicId();
})	
	
//显示班级
function showClasses(){
	$.ajax({
        url:"/${projectName}/camp/trainCamp/schMyClasses.action",
        type:"GET",
       	dataType: "json",
        success:function(data){
        	if(data.classes.length>0){
        		for(var i=0;i<data.classes.length;i++){
        			var bj=data.classes[i];
        			$("#bj").append("<option  value="+bj.number+">"+bj.name+"</option>");
        			renderForm();
        		}
        	}
		 }
	});	
}

//重新渲染表单
function renderForm(){
  layui.use('form', function(){
   var form = layui.form;//高版本建议把括号去掉，有的低版本，需要加()
   form.render();
  });
 }
 
 
layui.use(['form'], function() {
	var form=layui.form;
	
	form.on('select(classes)', function(data){  
		var status=null;
		tanchu(); 
		if($(".status").val()!=-1){
			status=$(".status").val();
		}
	    var id=data.value;
	$.ajax({
		url:"/${projectName}/camp/trainCamp/schTeacherEvaluate.action",
		type:"post",
		data:{
			"classid":id,
			"bookId":topicId,
			"status":status
			},
		dataType: "json",
		success:function(data){
			$(".stus").find("tbody").html('');
			var stuHtml='';
			$.each(data,function(i,obj){
				var size=getRecordsSize(obj.studid,topicId);
				var billSize=serchBillSzie(obj.studid,topicId);
				var score=obj.jsonInDict.teacherscores;
				if(score==null){
					score='';
				}
				if(obj.photo_url!=null&&obj.photo_url!=''){
					stuHtml+='<tr><td><div class="teac_img_head" onclick=\'openTcp('+obj.studid+','+obj.teacherscorestatus+',"'+obj.teacherid+'")\'><img src="/${projectName}/imgs/photos/'+obj.classid+'/'+obj.photo_url+'" alt="" style="cursor:pointer;"></div></td>';
				}else{
					stuHtml+='<tr><td><div class="teac_img_head" onclick=\'openTcp('+obj.studid+','+obj.teacherscorestatus+',"'+obj.teacherid+'")\'><img src="/${projectName}/book/img/icon/touxiang.png" alt="" style="cursor:pointer;"></div></td>';
				}
				if(obj.teacherscorestatus==0){
					stuHtml+='<td>'+obj.name+'</td><td>'+obj.studid+'</td><td><span class="teac_ping">'+obj.jsonInDict.teacherscorestatus+'</span></td><td><span class="teac_score_left">'+score+'</span>';
				}else{
					stuHtml+='<td>'+obj.name+'</td><td>'+obj.studid+'</td><td><span class="teac_p">'+obj.jsonInDict.teacherscorestatus+'</span></td><td><span class="teac_score_left">'+score+'</span>';
				}
				stuHtml+='<span class="teac_score_right">分</span></td><td class="xss-td"><a href="#" class="teac_tz" onclick="exportFile('+obj.studid+')">导出脚本</a></td>';
				stuHtml+='<td class="xss-td"><a href="#" class="teac_tz" onclick="showVideos('+obj.studid+')">录音（'+size+'）</a></td><td class="xss-td"><a href="#" class="teac_tz" onclick="showOrderList('+obj.studid+')">工单（'+billSize+'）</a></td></tr><tr class="height10"></tr>';
			});
			$(".stus").html(stuHtml);
			tanchuclose();
		},
	});
	
	});
	
	form.on('select(status)', function(data){
		var bj=$("#bj").val();
		if(bj!=-1){
			tanchu();
		}else{
			return;
		}
	    var status=data.value;
		if(status==-1){
			status=null;
		}
	$.ajax({
		url:"/${projectName}/camp/trainCamp/schTeacherEvaluate.action",
		type:"post",
		data:{
			"classid":bj,
			"bookId":topicId,
			"status":status
			},
		dataType: "json",
		success:function(data){
			$(".stus").find("tbody").html('');
			var stuHtml='';
			$.each(data,function(i,obj){
				var size=getRecordsSize(obj.studid,topicId);
				var billSize=serchBillSzie(obj.studid,topicId);
				var score=obj.jsonInDict.teacherscores;
				if(score==null){
					score='';
				}
				stuHtml+='<tr><td><div class="teac_img_head" onclick=\'openTcp('+obj.studid+','+obj.teacherscorestatus+',"'+obj.teacherid+'")\'><img src="/${projectName}/book/img/teac_head.png" alt=""></div></td>';
				if(obj.teacherscorestatus==0){
					stuHtml+='<td>'+obj.name+'</td><td>'+obj.studid+'</td><td><span class="teac_ping">'+obj.jsonInDict.teacherscorestatus+'</span></td><td><span class="teac_score_left">'+score+'</span>';
				}else{
					stuHtml+='<td>'+obj.name+'</td><td>'+obj.studid+'</td><td><span class="teac_p">'+obj.jsonInDict.teacherscorestatus+'</span></td><td><span class="teac_score_left">'+score+'</span>';
				}
				stuHtml+='<span class="teac_score_right">分</span></td><td><a href="#" class="teac_tz" onclick="exportFile('+obj.studid+')">导出脚本</a></td>';
				stuHtml+='<td><a href="#" class="teac_tz" onclick="showVideos('+obj.studid+')">录音（'+size+'）</a></td><td><a href="#" class="teac_tz" onclick="showOrderList('+obj.studid+')">工单（'+billSize+'）</a></td></tr>';
			});
			$(".stus").html(stuHtml);
			tanchuclose();
		},
	});
	
	});
});

function getRecords(){
	var status=null;
	if($(".status").val()!=-1){
		status=$(".status").val();
	}
	var bj=$("#bj").val();
	if(status!=null || bj!=-1){
		tanchu();
	}else{
		return;
	}
	
	$.ajax({
		url:"/${projectName}/camp/trainCamp/schTeacherEvaluate.action",
		type:"post",
		data:{
			"classid":bj,
			"bookId":topicId,
			"status":status
			},
		dataType: "json",
		success:function(data){
			$(".stus").find("tbody").html('');
			var stuHtml='';
			$.each(data,function(i,obj){
				var size=getRecordsSize(obj.studid,topicId);
				var billSize=serchBillSzie(obj.studid,topicId);
				var score=obj.jsonInDict.teacherscores;
				if(score==null){
					score='';
				}
				if(obj.photo_url!=null&&obj.photo_url!=''){
					stuHtml+='<tr><td><div class="teac_img_head" onclick=\'openTcp('+obj.studid+','+obj.teacherscorestatus+',"'+obj.teacherid+'")\'><img src="/${projectName}/imgs/photos/'+obj.classid+'/'+obj.photo_url+'" alt="" style="cursor:pointer;"></div></td>';
				}else{
					stuHtml+='<tr><td><div class="teac_img_head" onclick=\'openTcp('+obj.studid+','+obj.teacherscorestatus+',"'+obj.teacherid+'")\'><img src="/${projectName}/book/img/icon/touxiang.png" alt="" style="cursor:pointer;"></div></td>';
				}
				if(obj.teacherscorestatus==0){
					stuHtml+='<td>'+obj.name+'</td><td>'+obj.studid+'</td><td><span class="teac_ping">'+obj.jsonInDict.teacherscorestatus+'</span></td><td><span class="teac_score_left">'+score+'</span>';
				}else{
					stuHtml+='<td>'+obj.name+'</td><td>'+obj.studid+'</td><td><span class="teac_p">'+obj.jsonInDict.teacherscorestatus+'</span></td><td><span class="teac_score_left">'+score+'</span>';
				}
				stuHtml+='<span class="teac_score_right">分</span></td><td class="xss-td"><a href="#" class="teac_tz" onclick="exportFile('+obj.studid+')">导出脚本</a></td>';
				stuHtml+='<td class="xss-td"><a href="#" class="teac_tz" onclick="showVideos('+obj.studid+')">录音（'+size+'）</a></td><td class="xss-td"><a href="#" class="teac_tz" onclick="showOrderList('+obj.studid+')">工单（'+billSize+'）</a></td></tr><tr class="height10"></tr>';
			});
			$(".stus").html(stuHtml);
			tanchuclose();
		},
	});
}

/* 查询录音条数 */
function getRecordsSize(_xh,_pfrw){
	var size=0;
	$.ajax({
		async:false,
		type:"GET",
		url:'/${projectName}/camp/trainCamp/getRecordsSize.action',
		dataType:"json",
		data:{
			taskId:_pfrw, 
			toNum:_xh
		},
		success:function(response){
			size=response.data;
		}
	});
	return size;
}

/* 查询工单条数 */
function serchBillSzie(_xh,_pfrw){
	var size=0;
	$.ajax({
		async:false,
		type:"POST",
		url:'/${projectName}/practical/workorder/serchBillSzie.action',
		dataType:"json",
		data:{
			stuNum:_xh,
			taskId:_pfrw,
			type:1
		},
		success:function(response){
			size=response.size;
		}
	});
	return size;
}

//导出学生脚本
function exportFile(number){
	var taskId=parent.reTopicId();

	window.parent.open("/${projectName}/examination/mark/exportSxAnswerToWord.action?taskId="+taskId+"&number="+number);
}

//展示
function showVideos(stuNum){
	var taskId=parent.reTopicId();
	window.parent.showVideos(taskId,stuNum);
}

function showOrderList(stuNum){
	var taskId=parent.reTopicId();
	window.parent.showOrderList(taskId,stuNum);
}

function openTcp(stuid,statu,teacher){
	if(statu==0){
		parent.updTcp(stuid,-1,teacher);
	}else{
		parent.updTcp(stuid,0,teacher);
	}
}
//弹出遮罩
function  tanchu(){
	layui.use('layer', function(){
		var layer = layui.layer;
		var index = layer.load(1, {
	    	shade: [0.1,'#fff'] //0.1透明度的白色背景
		});
   }); 
}
//关闭遮罩
function  tanchuclose(){
	layui.use('layer', function(){
	    var layer = layui.layer;
	    layer.close(layer.index);
	}); 
}
</script>
</html>
