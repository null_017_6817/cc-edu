<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.qtrmoon.com/tags-dictionary" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>查看评分表</title><!--score-->
	<%@include file="/common/include.jsp" %>
<script>
function load(id){
	$('#_form').form('load','/${projectName}/camp/score/vieScore.action?toL=true&id='+id);
};
</script>
</head>
<body>
<form id="_form">
	<input type="hidden" name='id' value=""/>
	<table>
		<tr><td class='label'>模板ID：</td><td><input type='text' name='mbid' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>评分学生学号：</td><td><input type='text' name='pfxsxh' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>被评分学生学号：</td><td><input type='text' name='bpfxsxh' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>评分类型（自评、互评、教师评）：</td><td><input type='text' name='pflx' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>评分结果：</td><td><input type='text' name='pfjg' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>总分：</td><td><input type='text' name='zf' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>评分日期：</td><td><input type='text' name='pfrq' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>评分任务：</td><td><input type='text' name='pfrw' class='viewbox' readonly='readonly'/></td></tr>
	</table>

</form>
</body>
</html>
