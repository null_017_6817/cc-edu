<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
<title>教师测评</title>
<!--trainCamp-->
<style>
#_table thead {
	background-color: #f0f0f0;
}

#_table thead th {
	padding: 8px 0;
	text-align: center;
}

#_table  td {
	height: 40px;
    line-height: 40px;
    padding: 0 5px;
    text-align: center;
    border-bottom: 1px #f3f3f3 solid;
}

#aa {
    display: block;
    padding: 5px 15px;
    margin: 30px auto 0;
    color: #fff;
    background: #f36501;
    border: none;
    border-radius: 4px;
    cursor: pointer;
    outline: none;
}

.abc {
	width: 88px;
    height: 24px;
    padding: 0 4px;
    outline: none;
}

.hr-dashed {
    height: 1px;
    border: 0;
    border-top: silver 1px dashed;
}


</style>
<%@include file="/common/include.jsp"%>
</head>
<body>
		<!-- 通话列表 -->
		<!-- <div class="monitorBox">
			<span style="text-align: center;display: block;font-size:20px;padding: 20px 0;">通话列表</span>
			<div class="monitorCon">
				<dl class="list">
					<dt><span>监听</span><span>加入通话</span></dt>
					<dd>
						<p><span>小李子</span>100</p>
						<i>→</i>
						<p>小林子<br>200</p>
					</dd>
				</dl>
				<dl class="list">
					<dt><span>监听</span><span>加入通话</span></dt>
					<dd>
						<p><span>小李子</span>100</p>
						<i>→</i>
						<p>小林子<br>200</p>
					</dd>
				</dl>
				<dl class="list">
					<dt><span>监听</span><span>加入通话</span></dt>
					<dd>
						<p><span>小李子</span>100</p>
						<i>→</i>
						<p>小林子<br>200</p>
					</dd>
				</dl>
				<dl class="list">
					<dt><span>监听</span><span>加入通话</span></dt>
					<dd>
						<p><span>小李子</span>100</p>
						<i>→</i>
						<p>小林子<br>200</p>
					</dd>
				</dl>
				<dl class="list">
					<dt><span>监听</span><span>加入通话</span></dt>
					<dd>
						<p><span>小李子</span>100</p>
						<i>→</i>
						<p>小林子<br>200</p>
					</dd>
				</dl>
				<dl class="list">
					<dt><span>监听</span><span>加入通话</span></dt>
					<dd>
						<p><span>小李子你好</span>1000</p>
						<i>→</i>
						<p>小林子<br>200</p>
					</dd>
				</dl>
			</div>
		</div>
		
		<hr class="hr-dashed"> -->
		
		<span style="text-align: center;display: block;font-size:20px;padding: 20px 0;">教师评价</span>
		<div style="font-size: 14px;margin-bottom:20px;padding: 0 20px;">
			班级选择：<select id="se" name="classid" style="display: inline-block; width: 130px;height: 28px;line-height: 28px;
				vertical-align: middle;outline: none; margin-left: 4px;">
				<option>请选择</option>
			</select>
		</div>

	<form id="_form" style="padding: 0 20px;font-size:14px;">
		<table id="_table" style="width:100%;">
			<thead>
				<tr>
					<th>客服学号</th>
					<th>学生姓名</th>
					<th>脚本内容</th>
					<th>任务完成</th>
					<th>小组合作</th>
					<th>操作</th>
				</tr>
			</thead>
		</table>
		<input id="aa" type="button" value="保存" onclick="javascript:updTeachers()">
	</form>
</body>
<script type="text/javascript">	 
// 事件一  当页面加载结束后，会调用 showClasses函数
	$(function(){
		showClasses();
 	})	

//事件二 当班级值进行改变时，触发changeClassAddTable()函数
    $("#se").change(changeClassAddTable());
    
//事件三 当点击保存按钮时会触发updTeachers()方法

//显示班级
	function showClasses(){
		$.ajax({
		        url:"/${projectName}/camp/trainCamp/schClasses.action",
		        type:"post",
		       	dataType: "json",
		        success:function(data){
		            $.each(data,function(i,obj){
			           $("#se").append("<option  value="+obj.json.number+">"+obj.json.name+"</option>")
		         });
				 }
		});
	}
// 班级id的change事件，表格后面追加数据
	function changeClassAddTable(){
	 	$("#se").change(function(){
	 		var id = $("option:selected").attr("value");
	 		$.ajax({
	 			url:"/${projectName}/camp/trainCamp/schTeacherEvaluate.action",
	 			type:"post",
	 			data:{
	 				"classid":id,
	 				"bookId":'${bookId}'
	 				},
	 			dataType: "json",
	 			success:function(data){
	 				$("#_table").find("tbody").html('');
	 				$.each(data,function(i,obj){
	 					var a=obj.teacherscore1;
	 					if(a==undefined){a=0};
	 					var b=obj.teacherscore2;
	 					if(b==undefined){b=0};
	 					var c=obj.teacherscore3;
	 					if(c==undefined){c=0};
	 					var paramId=obj.id;
	 					if(paramId==undefined){paramId=""};
	 					var paramClassid=obj.classid;
	 					if(paramClassid==undefined){paramClassid=""};
	 					var d = obj.num;
	 						var html = "<tr name='dataTr'>"
	 							+	"<td id='a'><input name='studid' type='text' readonly='readonly' style='border:none;width:100px; text-align:center;' value="+obj.studid+" ></td>"
	 							+	"<td>"+obj.name
	 							+		"<input type='hidden' name='id' value='"+paramId+"' />"
	 							+		"<input type='hidden' name='bookid' value='${bookId}' />"
	 							+		"<input type='hidden' name='classid' value='"+paramClassid+"' />"
	 							+	"</td>"
	 							+	"<td><input class='abc' type='text' name='teacherscore1' value="+a+"></td>"
	 							+	"<td><input class='abc' type='text' name='teacherscore2' value="+b+"></td>"
	 							+	"<td><input class='abc' type='text' name='teacherscore3' value="+c+"></td>"
	 							+	"<td>"
	 							/* +		"<a href='javascript:void(0);' onclick=\"exportFile('${bookId}','"+obj.studid+"');\" >导出脚本</a>&nbsp;&nbsp;" */
	 							/* +		"<a href='javascript:void(0);' onclick=\"showVideos('${bookId}','"+obj.studid+"');\" >录音</a>&nbsp;&nbsp;" */
	 							+		"<a href='javascript:void(0);' onclick=\"showOrderList('${bookId}','"+obj.studid+"');\" >工单</a>&nbsp;&nbsp;"
	 							+		"<a href='javascript:void(0);' onclick=\"openTcp('"+obj.studid+"');\" >评分</a>"
	 							+		"<a href='javascript:void(0);' onclick=\"openUpdTcp('"+obj.studid+"','"+23+"');\" >修改评分</a>"
	 							+	"</td>"
	 							+"</tr>";
	 					/* if(d!=null){
	 					}else{
	 						var html = "<tr name='dataTr'>"
	 							+	"<td id='a'><input name='studid' type='text' readonly='readonly' style='border:none;width:100px; text-align:center;' value="+obj.studid+" ></td>"
	 							+	"<td>"+obj.name
	 							+		"<input type='hidden' name='id' value='"+paramId+"' />"
	 							+		"<input type='hidden' name='bookid' value='${bookId}' />"
	 							+		"<input type='hidden' name='classid' value='"+paramClassid+"' />"
	 							+	"</td>"
	 							+	"<td><input class='abc' type='text' name='teacherscore1' value="+a+" disabled='disabled'></td>"
	 							+	"<td><input class='abc' type='text' name='teacherscore2' value="+b+" disabled='disabled'></td>"
	 							+	"<td><input class='abc' type='text' name='teacherscore3' value="+c+" disabled='disabled'></td>"
	 							+	"<td><a href='javascript:void(0);' onclick=\"exportFile('${bookId}','"+obj.studid+"');\" >导出脚本</a>&nbsp;"
	 							+		 "<a href='javascript:void(0);' onclick=\"showVideos('${bookId}','"+obj.studid+"');\" >录音</a></td>"
	 							+"</tr>";
	 					} */
	 				
	 					$("#_table").append(html);
	 				});
	 				window.parent.cahngeHeight($(document).height());
	 			},
	 		});
	   });
	}
	
	// 展示
	function showVideos(taskId,stuNum){
		window.parent.showVideos(taskId,stuNum);
	}
	
	function showOrderList(taskId,stuNum){
		window.parent.showOrderList(taskId,stuNum);
	}
	
	
	// 保存按钮，实现更新功能
	 function updTeachers(){
         
        var jsonStr= $("#_form").serializeArrayToJson();
		 $.ajax({
			 url: "/${projectName}/camp/trainCamp/updTeacher.action",
			 data:jsonStr,
			 type:"post",
			 contentType:"application/json",
			 success:function(result){
				if(result != null){
					for(var i = 0; i < result.length; i++){
						$("tr[name='dataTr']").find("input[name='id']").eq(i).val(result[i].id);
					}
				}
				window.parent.swal({title:"保存成功！",
					type:"success"});
				
			 },
			 error:function(){
				 window.parent.swal({title:"服务器异常！",
						type:"error"});
					
			 }
		 });
	 }
	 
	function reloadData(){
			$('#_table').datagrid('reload');
	}
	
	
	//多个表单转json数组
	$.fn.extend({
		//多个表单转json数组
		serializeArrayToJson : function() {
	        var a = this.serializeArray();
	        var b = [];
	        var o = {};
	        $.each(a, function() {
	        	var name = this.name;
	            if (o[name]) {
	            	//如果o[name]没有嵌套在{}内
	            	if(!o[name].push) {
	            		b.push(o);
	            		o = {};
	            		o[name] = this.value || '';
	            	}
	            } else {
	                o[name] = this.value || '';
	            }
	        });
	        b.push(o);
	        return JSON.stringify(b);
		}
	});
	
	// 导出学生脚本
	function exportFile(taskId,number){
		//location.href="/${projectName}/examination/mark/exportSxAnswerToWord.action?taskId="+taskId+"&number="+number;
		window.parent.open("/${projectName}/examination/mark/exportSxAnswerToWord.action?taskId="+taskId+"&number="+number);
	}
	
	function openTcp(stuid){
		parent.updTcp(stuid,-1);
	}
	
	function openUpdTcp(stuid,tpid){
		parent.updTcp(stuid,tpid);
	}
</script>
</html>
