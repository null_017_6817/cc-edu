<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>查看实训营地评分</title><!--trainCamp-->
	<%@include file="/common/include.jsp" %>
<script>
function load(id){
	$('#_form').form('load','/${projectName}/camp/trainCamp/vieTrainCamp.action?toL=true&id='+id);
};
</script>
</head>
<body>
<form id="_form">
	<input type="hidden" name='id' value=""/>
	<table>
		<tr>
			<td class='label'>
			任务：
			</td>
			<td>
			<input type='text' name='bookid' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			学生：
			</td>
			<td>
				<input type='text' name='studid' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			组队学生：
			</td>
			<td>
			<input type='text' name='partnerid' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			评分老师：
			</td>
			<td>
				<input type='text' name='teacherid' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			知识掌握自评：
			</td>
			<td>
			<input type='text' name='personalscore1' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			任务完成互评：
			</td>
			<td>
				<input type='text' name='personalscore2' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			小组合作教师评：
			</td>
			<td>
			<input type='text' name='personalscore3' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			知识掌握：
			</td>
			<td>
				<input type='text' name='partnerscore1' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			任务完成互评：
			</td>
			<td>
			<input type='text' name='partnerscore2' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			小组合作教师评：
			</td>
			<td>
				<input type='text' name='partnerscore3' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			知识掌握：
			</td>
			<td>
			<input type='text' name='teacherscore1' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			任务完成互评：
			</td>
			<td>
				<input type='text' name='teacherscore2' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			小组合作教师评：
			</td>
			<td>
			<input type='text' name='teacherscore3' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			修改时间：
			</td>
			<td>
				<input type='text' name='tm' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			总分：
			</td>
			<td>
			<input type='text' name='score' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			班级id：
			</td>
			<td>
				<input type='text' name='classid' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
	</table>

</form>
</body>
</html>
