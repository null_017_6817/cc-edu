<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.qtrmoon.com/tags-dictionary" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>编辑</title><!--hytopic-->
	<%@include file="/common/include.jsp" %>
<script>
<%-- 查询页点击修改时调用此方法加载记录详情 --%>
function load(id){
	$('#_form').form('load','/${projectName}/camp/hytopic/vieHytopic.action?id='+id);
};

<%-- 点击保存按钮时调用此方法。(注:保存按钮在查询列表页) --%>
function submitForm(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#_form').form('submit',{
      success:function(data) {
      	parent.reloadData();
      }
   });
}
</script>
</head>
<body>
<form id="_form" method="post" action="/${projectName}/camp/hytopic/updHytopic.action">
	<input type="hidden" name='id' id='id'/>
	<input type="hidden" name='createtime' id='createtime'/>
	<input type="hidden" name='createby' id='createby'/>
	<input type="hidden" name='updatetime' id='updatetime'/>
	<input type="hidden" name='updateby' id='updateby'/>
	<table>
		<tr><td class='label' style="width:120px">关联id：</td><td><input id="rid" name="rid" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >标题：</td><td><input id="title" name="title" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >内容：</td><td><input id="content" name="content" class="easyui-textbox" data-options="required:true"/></td></tr>
	</table>

</form>
</body>
</html>
