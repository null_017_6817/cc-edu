<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.qtrmoon.com/tags-dictionary" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>编辑</title><!--hytopic-->
	<%@include file="/common/include.jsp" %>
<style>

.questionDiv {
	padding: 10px 0;
	margin-top: 5px;
	border-bottom: 1px solid #cacaca;
}

.question-edit .datagrid-header-check {
	height: 16px;
}

.question-edit select,
.question-edit input[type="text"] {
	width: 156px;
    height: 26px;
}


.question-edit input[type="text"] {
	padding: 0 2px;
}

.question-edit .rel {
	position: relative;
}

.question-edit .rel button {
	position: absolute;
	top: 50%;
	right: 0;
	margin-top: -13px;
	font-size: 18px;
	background: 0;
	border: 0;
}

.question-edit .questionDiv table {
	margin: 0 auto;
}

.question-edit .questionDiv td {
 padding: 4px 2px;
}

.question-edit .questionDiv textarea {
	width: 440px;
	max-width: 440px;
}

.question-edit .l-btn:hover {
	background: none;
	color: 
}



</style>
<script>
<%-- 查询页点击修改时调用此方法加载记录详情 --%>
function load(id){
	$(".content").html("");
	$("input[name='rid']").val(id);
	$.ajax({
		url:'/${projectName}/camp/dxwj/getDxwj.action',
		data:{rid:id,id:id},
		type: "post",
		dataType:'json',
		success:function(result){
			if(result.success){
				var data = result.data;
				if(data != null){
					$("input[name='id']").val(data.id);
					var con = data.content;
					if(con != null){
						var dxwjJsons = con.dxwjJsons;
						if(dxwjJsons != null ){
							for(var i = 0; i < dxwjJsons.length; i++){
								var quest = dxwjJsons[i];
								var type = quest.type;
								addQuestion();
								var qDiv = $(".content").find(".questionDiv").eq(i);
								qDiv.find("select[name='type']").val(quest.type);
								qDiv.find("select[name='type']").trigger("change");
								qDiv.find("textarea[name='question']").val(quest.question);
								if(type == 1 || type == 2){
									var choices = quest.choices
									if(choices != null){
										for( var j = 0 ; j < choices.length; j++){
											var choice = choices[j];
											var html = "<tr name='choiceTr'><td></td>"
												+ "<td>选项</td>"
												+ "<td name='choiceTd'><input type='text' value='"+choice.chooseTxt+"'/>&nbsp;&nbsp;<button type='button' onclick='removeChoice(this);'>删除选项</button> </td>"
												+ "</tr>";
											qDiv.find("table[name='questionTable']").append(html);
										}
									}
								}
							}
						}
					}
				}
			}else{
				alert(result.msg);
			}
		},
	});
};

<%-- 点击保存按钮时调用此方法。(注:保存按钮在查询列表页) --%>
function submitForm(){
	var id = $("input[name='id']").val();
	var rid = $("input[name='rid']").val();
	var cont = "id="+id+"&rid="+rid;
	$(".content").find('.questionDiv').each(function(i,item){
		var type = $(this).find("select[name='type']").val();
		cont += "&content.dxwjJsons["+i+"].type="+type;
		if(type == 1 || type == 2){
			$(this).find("tr[name='choiceTr']").each(function(j,item){
				cont += "&content.dxwjJsons["+i+"].choices["+j+"].chooseTxt="+$(this).find("input[type=text]").val();
			});
		}
		cont += "&content.dxwjJsons["+i+"].question="+$(this).find("textarea[name='question']").val();
	});
	$.ajax({
		url:'/${projectName}/camp/dxwj/saveDxwj.action',
		data:encodeURI(cont),
		type: "post",
		dataType:'json',
		success:function(result){					
			if(result.success){
				parent.closeEditDxWj();
			}else{
				alert(result.msg);
			}
		},
	});
}

// 添加题目
function addQuestion(){
	var commonHtml = $("div[name='temp']").find('.question').html();
	commonHtml = "<div class='questionDiv'>"+commonHtml+"</div>";
	$(".content").append(commonHtml);
}

// 删除题目
function remove(t){
	$(t).parents('.questionDiv').remove();
}

// 题型修改
function changeType(t){
	var type = $(t).val();
	if(type != "" && type != null && type != "undefined" ){
		var table = $(t).parents("table[name='questionTable']").find('tr:gt(0)').remove();
		var question = $("div[name='temp']").find('.question'+type).find("table tbody").html();
		$(t).parents("table[name='questionTable']").append(question);
	}
}
// 添加选项
function addChoice(t){
	var html = "<tr name='choiceTr'><td></td>"
			+ "<td>选项</td>"
			+ "<td name='choiceTd'><input type='text' value=''/>&nbsp;&nbsp;<button type='button' onclick='removeChoice(this);'>删除选项</button> </td>"
			+ "</tr>";
	$(t).parents("table[name='questionTable']").append(html);
}

// 删除选项
function removeChoice(t){
	$(t).parents("tr[name='choiceTr']").remove();
}

function clearChoice(t){
	$(t).parents("table[name='questionTable']").find("tr[name='choiceTr']").remove();
}
</script>
</head>
<body>
<form id="_form" class="question-edit" method="post" action="/${projectName}/camp/hytopic/updHytopic.action">
	<input type="hidden" name="id" value=""/>
	<input type="hidden" name="rid" value=""/>
	<div class="opt">
		<button class="btn btn-success" type="button" onclick="addQuestion();" >添加</button>
	</div>
	<div class="content">
	</div>
</form>

<div name="temp" style="display:none">
	<!-- 单选题 -->
	<jsp:include page="./template/select-temp.jsp" />
	<!-- 选择题 -->
	<!-- 多选题 -->
	<jsp:include page="./template/selects-temp.jsp" />
	<!-- 选择题 -->
	<!-- 填空题 -->
	<jsp:include page="./template/answer-temp.jsp" />
	<!-- 选择题 -->
	<!-- 空题 -->
	<jsp:include page="./template/common-temp.jsp" />
	<!-- 选择题 -->
</div>
</body>
</html>
