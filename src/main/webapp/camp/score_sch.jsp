<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.qtrmoon.com/tags-dictionary" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>评分表查询</title><!--score-->
	<%@include file="/common/include.jsp" %>

<script>
var winToggle;
$(function(){
	winToggle=new QWinToggle().regist(["scoreEditWin","scoreViewWin"]);
	var pager = $('#scoreGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});			
})

<%-- 执行查询，点击查询按钮时调用 --%>
function doScoreSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#scoreGrid').datagrid('load',{
		mbid:$('#mbid').val(),
		pfxsxh:$('#pfxsxh').val(),
		bpfxsxh:$('#bpfxsxh').val(),
		pflx:$('#pflx').val(),
		zf:$('#zf').val(),
		pfrqBeg: $('#pfrqBeg').val(),
		pfrqEnd: $('#pfrqEnd').val(),
		pfrw:$('#pfrw').val()
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatScoreOper(val,row,index){  
    return "&emsp;<a href='javascript:void(0)' onclick='updScore("+val+")' class='_oper' title='修改'>&nbsp;<i class='icon-edit'></i>&nbsp;</a>"
    			+"<a href='javascript:void(0)' onclick='vieScore("+val+")' class='_oper' title='查看'>&nbsp;<i class='icon-info-sign'></i>&nbsp;</a>&emsp;"; 
} 

<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
function updScore(id){
	winToggle.open("scoreEditWin");
	$('#scoreEditWin').get(0).contentWindow.load(id);
} 

<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
function vieScore(id){
	winToggle.open("scoreViewWin");
	$('#scoreViewWin').get(0).contentWindow.load(id);
}

<%-- 删除记录 --%>
function delScore(){
	var checkedItems = $('#scoreGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/camp/score/delScore.action",{ids:ids},function(data){
				$('#scoreGrid').datagrid('reload');
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#scoreEditWin').dialog('close');
	$('#scoreGrid').datagrid('reload');
}
</script>
</head>
<body>
<table id="scoreGrid" title="" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#scoreGridToolbar',collapsible:true,url:'/${projectName}/camp/score/schScore.action',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" ></th>
			<th data-options="field:'mbid',width:80,sortable:true">模板ID</th>
			<th data-options="field:'pfxsxh',sortable:true">评分学生学号</th>
			<th data-options="field:'bpfxsxh',sortable:true">被评分学生学号</th>
			<th data-options="field:'pflx',sortable:true">评分类型（自评、互评、教师评）</th>
			<th data-options="field:'pfjg',sortable:true">评分结果</th>
			<th data-options="field:'zf',sortable:true">总分</th>
			<th data-options="field:'pfrq',sortable:true">评分日期</th>
			<th data-options="field:'pfrw',sortable:true">评分任务</th>

			<th data-options="field:'_oper',align:'center',formatter:formatScoreOper">操作</th>
		</tr>
	</thead>
</table>
<%-- 查询条件设定栏 --%>
<div id="scoreGridToolbar">
	<div class="opline">
		<a href="javascript:updScore(0)" class="btn btn-success"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">添加</span></a>&emsp;
		<a href="javascript:delScore()" class="btn btn-danger"><span class="icon-trash" style="font-size:16px"></span> <span style="font-size:14px">删除</span></a>
	</div>
	<form id="_form" class="shline">
	模板ID：<input name='mbid' id="mbid" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	评分学生学号：<input name='pfxsxh' id="pfxsxh" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	被评分学生学号：<input name='bpfxsxh' id="bpfxsxh" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	评分类型（自评、互评、教师评）：<input name='pflx' id="pflx" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	总分：<input name='zf' id="zf" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	评分日期：<input name="pfrqBeg" id="pfrqBeg" class="easyui-datebox" style="width:100px;" data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
&gt;&gt;
	<input name="pfrqEnd" id="pfrqEnd" class="easyui-datebox" style="width:100px;" data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}}],
validType:['compareDate[pfrqBeg]']"/>
	评分任务：<input name='pfrw' id="pfrw" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>

	<a href="javascript:void(0)" onclick="doScoreSearch()" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>
	</form>
</div>


<%-- 修改or添加面板 --%>
<iframe id="scoreEditWin" src="/${projectName}/camp/score/page.action?page=score_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#scoreEditButton'" title="编辑评分表" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="scoreEditButton">
	<a href="javascript:void(0)" onclick="$('#scoreEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#scoreEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >取 消</a>
</div>
<%-- 查看面板 --%>
<iframe id="scoreViewWin" src="/${projectName}/camp/score/page.action?page=score_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#scoreViewButton'" title="查看评分表" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="scoreViewButton">
	<a href="javascript:$('#scoreViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >关 闭</a>
</div>
</body>
</html>
