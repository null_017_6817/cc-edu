<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>修改实训营地评分</title><!--trainCamp-->
	<%@include file="/common/include.jsp" %>
<script>
<%-- 查询页点击修改时调用此方法加载记录详情 --%>
function load(id){
	$('#_form').form('load','/${projectName}/camp/trainCamp/vieTrainCamp.action?id='+id);
};

<%-- 点击保存按钮时调用此方法。(注:保存按钮在查询列表页) --%>
function submitForm(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#_form').form('submit',{
      success:function(data) {
      	parent.reloadData();
      }
   });
}
</script>
</head>
<body>
<form id="_form" method="post" action="/${projectName}/camp/trainCamp/updTrainCamp.action">
	<input type="hidden" name='id' id='id'/>
	<table>
		<tr>
			<td class='label'>
			任务：
			</td>
			<td>
			<input id="bookid" name="bookid" class="easyui-textbox" data-options="required:true"/>
			</td>
			<td class='label'>
			学生id：
			</td>
			<td>
				<input id="studid" name="studid" class="easyui-textbox" data-options="required:true"/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			组队学生：
			</td>
			<td>
			<input id="partnerid" name="partnerid" class="easyui-textbox" data-options="required:true"/>
			</td>
			<td class='label'>
			评分老师：
			</td>
			<td>
				<input id="teacherid" name="teacherid" class="easyui-textbox" data-options="required:true"/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			知识掌握自评：
			</td>
			<td>
			<input id="personalscore1" name="personalscore1" class="easyui-textbox" data-options="required:true"/>
			</td>
			<td class='label'>
			任务完成互评：
			</td>
			<td>
				<input id="personalscore2" name="personalscore2" class="easyui-textbox" data-options="required:true"/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			小组合作教师评：
			</td>
			<td>
			<input id="personalscore3" name="personalscore3" class="easyui-textbox" data-options="required:true"/>
			</td>
			<td class='label'>
			知识掌握：
			</td>
			<td>
				<input id="partnerscore1" name="partnerscore1" class="easyui-textbox" data-options="required:true"/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			任务完成互评：
			</td>
			<td>
			<input id="partnerscore2" name="partnerscore2" class="easyui-textbox" data-options="required:true"/>
			</td>
			<td class='label'>
			小组合作教师评：
			</td>
			<td>
				<input id="partnerscore3" name="partnerscore3" class="easyui-textbox" data-options="required:true"/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			知识掌握：
			</td>
			<td>
			<input id="teacherscore1" name="teacherscore1" class="easyui-textbox" data-options="required:true"/>
			</td>
			<td class='label'>
			任务完成互评：
			</td>
			<td>
				<input id="teacherscore2" name="teacherscore2" class="easyui-textbox" data-options="required:true"/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			小组合作教师评：
			</td>
			<td>
			<input id="teacherscore3" name="teacherscore3" class="easyui-textbox" data-options="required:true"/>
			</td>
			<td class='label'>
			时间：
			</td>
			<td>
				<input id="tmstr" name="tmstr" class="easyui-datebox" data-options="required:true"/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			总分：
			</td>
			<td>
			<input id="score" name="score" class="easyui-textbox" data-options="required:true"/>
			</td>
			<td class='label'>
			班级id：
			</td>
			<td>
				<input id="classid" name="classid" class="easyui-textbox" data-options="required:true"/>
			</td>
		</tr>
	</table>

</form>
</body>
</html>
