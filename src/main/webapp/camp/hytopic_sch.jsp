<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.qtrmoon.com/tags-dictionary" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>查询</title><!--hytopic-->
	<%@include file="/common/include.jsp" %>
<script>
var winToggle;
$(function(){
	winToggle=new QWinToggle().regist(["hytopicEditWin","hytopicViewWin"]);
	var pager = $('#hytopicGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});			
})

<%-- 执行查询，点击查询按钮时调用 --%>
function doHytopicSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#hytopicGrid').datagrid('load',{
		title:$('#title').val(),
		rid:bookIdParamDict.getValue()
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatHytopicOper(val,row,index){  
	var opt = "&emsp;<a href='javascript:void(0)' onclick='showCj("+val+")' class='_oper' title='修改'>&nbsp;<i class='icon-edit'></i>&nbsp;</a>";
	if($.trim(row.rid) == '电信'){
		opt += "&emsp;<a href='javascript:void(0)' onclick='question("+val+")' class='_oper' title='问卷'>&nbsp;<i class='icon-file-alt'></i>&nbsp;</a>";
	} 
	return opt;
}

// 问卷
function question(id){
	QMoveDialog($("#editDxWj"));
	$('#editDxWj').dialog('close');
	$('#editDxWj').dialog('open');
	$('#editDxWj').get(0).contentWindow.load(id);
}

// 添加topic数据
function addHytopic(){
	currentId = "";
	$("#goalDIV").find("#id").val("");
	UE.getEditor('editor').setContent("");
	$("#goalDIV").show();
}

function closeEditDxWj(){
	$('#editDxWj').dialog('close');
}

var currentId;

//问卷
function showCj(id){
	if(id == '0'){
		$("#editHyCj").dialog({title: '添加场景',resizable:true});
	}
	if(id != '0'){
		$("#editHyCj").dialog({title: '修改场景',resizable:true});
	}
	QMoveDialog($("#editHyCj"));
	$('#editHyCj').dialog('close');
	$('#editHyCj').dialog('open');
	$('#editHyCj').get(0).contentWindow.load(id);
}

function closeCj(){
	$('#editHyCj').dialog('close');
	reloadData();
}


<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
function vieHytopic(id){
	winToggle.open("hytopicViewWin");
	$('#hytopicViewWin').get(0).contentWindow.load(id);
}

<%-- 删除记录 --%>
function delHytopic(){
	var checkedItems = $('#hytopicGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/camp/hytopic/delHytopic.action",{ids:ids},function(data){
				$('#hytopicGrid').datagrid('reload');
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#hytopicEditWin').dialog('close');
	$('#hytopicGrid').datagrid('reload');
}
</script>
</head>
<body>
<table id="hytopicGrid" title="" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#hytopicGridToolbar',collapsible:true,url:'/${projectName}/camp/hytopic/schHytopic.action',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" ></th>
			<th data-options="field:'title',width:80,sortable:true">标题</th>
			<th data-options="field:'rid',sortable:true">行业</th>
			<th data-options="field:'createby',sortable:true">创建人</th>
			<th data-options="field:'createtime',sortable:true">创建时间</th>
			<th data-options="field:'updateby',sortable:true">修改人</th>
			<th data-options="field:'updatetime',sortable:true">修改时间</th>
			<th data-options="field:'_oper',align:'center',formatter:formatHytopicOper">操作</th>
		</tr>
	</thead>
</table>
<%-- 修改or添加面板 --%>
<iframe id="editDxWj" src="/${projectName}/camp/hytopic/page.action?page=hytopic_dxwjupd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#exerEditButton'" title="编辑问卷" style="width:650px;height:600px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="exerEditButton">
	<a href="javascript:void(0)" onclick="$('#editDxWj').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#editDxWj').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >取 消</a>
</div>

<iframe id="editHyCj" src="/${projectName}/camp/hytopic/page.action?page=hytopic_cj" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#hycjButton'" title="编辑场景" style="width:850px;height:650px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="hycjButton">
	<a href="javascript:void(0)" onclick="$('#editHyCj').get(0).contentWindow.saveGoal()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#editHyCj').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >取 消</a>
</div>

<%-- 查询条件设定栏 --%>
<div id="hytopicGridToolbar">
	<div class="opline">
		
	</div>
	<form id="_form" class="shline">
	标题：<input name='title' id="title" class='easyui-textbox' value='' data-options="iconWidth:16,
				icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
			}]"/>
	行业：
	<dc:insertList  name="bookIdParam" dictId="ZD_TRAININDUSTY" style="combo_normal" />
	<a href="javascript:void(0)" onclick="doHytopicSearch()" class="btn btn-info">
		<span class="icon-search" style="font-size:16px"></span>
		<span style="font-size:14px">查询</span>
	</a>&emsp;
	<a href="javascript:showCj(0)" class="btn btn-success"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">添加</span></a>&emsp;
		<a href="javascript:delHytopic()" class="btn btn-danger"><span class="icon-trash" style="font-size:16px"></span> <span style="font-size:14px">删除</span></a>
	</form>
</div>
</body>
</html>
