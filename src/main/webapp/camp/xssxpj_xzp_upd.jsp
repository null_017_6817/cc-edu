<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>小组评分</title>
<!--trainCamp-->
<link rel="stylesheet" href="/${projectName}/book/js/lib/layui2.3/css/layui.css">
<link rel="stylesheet" href="/${projectName}/book/css/style.css">
<link rel="stylesheet" href="/${projectName}/book/css/common_score.css">
</head>

<body style="background:#f1f5f8;overflow-x:auto;">
	<div class="jbyhcon-title">
		<img src="/${projectName}/book/img/jbyh2.png">
		<p>编辑小组评分表</p>
	</div>
	<form class="layui-form" action="">
		<div style="margin:10px 0;">
    		<label class="layui-form-label student_name" style="font-size:16px;margin-left:25px;"><strong class="xsxx">小组成员：</strong></label>
		</div>
	</form>
<script src="/${projectName}/book/js/lib/jquery.min.js"></script>
<script src="/${projectName}/book/js/lib/layui2.3/layui.all.js"></script>
<script src="/${projectName}/book/js/common.js"></script>
<script src="/${projectName}/book/js/circleChart.min.js"></script>
<script src="/${projectName}/book/js/jbyh.js"></script>
<script type="text/javascript">
var id=null;
var khfl=0;
var bpfxsxh=0;
var pfid='${pfid}';
var templateId='${templateId}';
$(function(){
	schGroupScore();
	khys(khfl);
});

function schGroupScore(){
	$.ajax({
		async:false,
		type:"GET",
		url:'/${projectName}/camp/score/schScoreById.action',
		dataType:"json",
		data:{
			pfid:pfid,
			templateId:templateId
		},
		success:function(response){
			bpfxsxh=response.result.bpfxsxh;
			var xm=response.result.bpfxsxm;
			$(".xsxx").text(xm+','+bpfxsxh);
			id=response.result.id;
			var score=response.score.split(",");
			var zpxs="";
			var zpjg=eval('(' + response.result.pfjg + ')'); 
			if(zpjg.length>0){
				for(var i=0;i<zpjg.length;i++){
					khfl=zpjg.length;
					var zpx=zpjg[i];
					var xb=i+1;
					var bh=1;
					if(xb%2==0){
						bh=2;
					}
					zpxs+='<div class="jbyhpf-con"><div class="jbyhpf-con1"><p class="jbyhpf-con1-title">分类</p><p class="jbyhpf-con1-title2" name="'+zpx.classify+'">'+zpx.classifyName+'</p>';
					zpxs+='<div class="circleChart'+bh+'" id="circleChart" data-value="'+Number(zpx.proportion)+'" style="color:#1792ee;margin-bottom:10px;"></div></div>';
					if(zpx.targets.length>0){
						zpxs+='<div class="jbyhpf-con2"><p class="jbyhpf-con1-title">考核指标</p>';
						for(var j=0;j<zpx.targets.length;j++){
							zpxs+='<p class="target" name="'+zpx.target[j]+'"><span></span>'+zpx.targets[j]+'</p>';
						}
						zpxs+='</div>';
					}
					
					zpxs+='<div class="jbyhpf-con3"><p class="jbyhpf-con1-title" style="text-align:center;margin-left:-5px;">评分</p>';
				
					if(zpx.score.length>0){
						for(var k=0;k<zpx.score.length;k++){
							zpxs+='<div class="jbyhpf-con3pf"><img src="/${projectName}/book/img/jbyhpf2.png"><select class="zscore" name="zpf" >';
							if(score.length>0){
								for(var s=0;s<score.length;s++){
									if(zpx.score[k]==score[s]){
										zpxs+='<option value="'+score[s]+'" selected>'+score[s]+'</option>';
									}else{
										zpxs+='<option value="'+score[s]+'">'+score[s]+'</option>';
									}
									
								}
							}
							zpxs+='</select></div>';
						}
					}
					
					zpxs+='</div><div class="clear"></div></div>';
				}
				zpxs+='<div class="jbyhpf-texa"><label>有待改进之处：</label><textarea rows="" cols="" id="ydgjzc" name="ydgjzc">'+response.result.ydgjzc+'</textarea></div><div class="jbyhpf-btn"><div class="jbyhpf-btn1" lay-submit="" lay-filter="jbyhXzp" onclick="save()">保存</div>';
				zpxs+='<div class="jbyhpf-btn1" onclick="cancel()">取消</div></div>';
				$(".layui-form").append(zpxs);
				renderForm();//表单重新渲染，要不然添加完显示不出来新的option
			}
		}
	});
}

function save(){
	var pfxsxh='${studid}';
	var pflx=2;/* 评分类型（小组评） */
	var pfrw='${topicId}';
	var ydgjzc=$("#ydgjzc").val();
	var zf=0;
	var scoreMsg = {};  //全局变量
	var modelArr=[];
	  $(".layui-form .jbyhpf-con").each(function(m){    //遍历分类
		  
	      var scoreObj=[];
	  	  var fl=$(this).find(".jbyhpf-con1-title2").attr("name");
	  	  var zb=$(this).find("#circleChart").attr("data-value");
	  	  var zfs=0;
	  	  var target=[];
	      $(this).find(".target").each(function(n){    //遍历指标
	    	  target.push($(this).attr("name"));
	      });
	      var grade=[];
	      var i=0;
	      $(this).find(".layui-input").each(function(n){ 
	    	  grade.push($(this).val());
	    	  zfs+=Number($(this).val());
	    	  i++;
	      });
	      modelArr.push({"classify":fl,"target":target,"score":grade});
	      zfs=zfs/i*zb/100;
	      zf+=Number(zfs);			  
	      
	  });
	  scoreMsg = JSON.stringify(modelArr);
	 
	  var pfjg=scoreMsg;
      
      $.post("/${projectName}/camp/score/updScore.action",{id:id,pfxsxh:pfxsxh,bpfxsxh:bpfxsxh,pflx:pflx,pfjg:pfjg,zf:zf,pfrw:pfrw,templateId:templateId,ydgjzc:ydgjzc},function(data){
    	  parent.reload();
	  }); 
}

function khys(khs){
	if(khs>0){
		for(var i=1;i<=khs;i++){
			if(i%2==0){
				$(".circleChart2").circleChart({
			        color: "#33cd98",
					backgroundColor: "#c2eee0",
					background: true,
					speed: 2000,
					widthRatio: 0.1,
					unit: 'percent',
					counterclockwise: false,
					size: 110,
					startAngle: 75,
					animate: true,
					backgroundFix: true,
					lineCap: "round",
					animation: "easeInOutCubic",
					text: 0,
					redraw: false,
					cAngle: 0,
					textCenter: true,
					textSize: 30,
					textWeight: 'normal',
					textFamily: 'sans-serif',
					relativeTextSize: 1 / 7,
					autoCss: true,
					onDraw: function(el, circle) {
			            circle.text(Math.round(circle.value) + "%");
			        }   
			    });
			}else{
				$(".circleChart1").circleChart({
			        color: "#1792ee",
					backgroundColor: "#b9defa",
					background: true,
					speed: 2000,
					widthRatio: 0.1,
					unit: 'percent',
					counterclockwise: false,
					size: 110,
					startAngle: 75,
					animate: true,
					backgroundFix: true,
					lineCap: "round",
					animation: "easeInOutCubic",
					text: 0,
					redraw: false,
					cAngle: 0,
					textCenter: true,
					textSize: 30,
					textWeight: 'normal',
					textFamily: 'sans-serif',
					relativeTextSize: 1 / 7,
					autoCss: true,
					onDraw: function(el, circle) {
			            circle.text(Math.round(circle.value) + "%");
			        }   
			    });
			}
		}
	}
}

function cancel(){
	parent.closepj();
}
//重新渲染表单
function renderForm(){
  layui.use('form', function(){
   var form = layui.form;//高版本建议把括号去掉，有的低版本，需要加()
   form.render();
  });
 }
</script>
</body>
</html>