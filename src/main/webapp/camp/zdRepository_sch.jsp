<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.qtrmoon.com/tags-dictionary" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>查询</title><!--repository-->
	<%@include file="/common/include.jsp" %>

<script>
var winToggle;
$(function(){
	winToggle=new QWinToggle().regist(["repositoryEditWin","repositoryViewWin"]);
	var pager = $('#repositoryGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});			
})
var currRepositoryId;
function openRepository(json){//{id:'',text:''}
	currRepositoryId=json.id;
	doRepositorySearch(true);
}
<%-- 执行查询，点击查询按钮时调用 --%>
function doRepositorySearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	var treeOrgs=repositorytreeDict.getValue();
	$('#repositoryGrid').datagrid('load',{
		id:treeOrgs,
		label:$("#label").val()
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatRepositoryOper(val,row,index){  
    return "&emsp;<a href='javascript:void(0)' onclick='updRepository("+row.id+", "+row.bookId+")' class='_oper' title='修改'>&nbsp;<i class='icon-edit'></i>&nbsp;</a>"
    			+"<a href='javascript:void(0)' onclick='vieRepository("+row.id+")' class='_oper' title='查看'>&nbsp;<i class='icon-info-sign'></i>&nbsp;</a>&emsp;"; 
} 

<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
function updRepository(id, bookId){
	if (bookId != null) {
		// $.messager.alert('敬告','课本关联的知识无法修改！');
		$.messager.show({title: '敬告', msg: '课本关联的知识无法修改！'});
		return;
	}
	if(id == '0'){
		if (currRepositoryId == undefined) {
			// $.messager.alert('敬告','请选择父级知识目录！');
			$.messager.show({title: '敬告', msg: '请选择父级知识目录！'});
			return;
		}
		console.log(currRepositoryId)
		$.get("/${projectName}/camp/repository/getRepositoryById.action",{id:currRepositoryId},function(data){
			var res = JSON.parse(data);
			var status = res.code;
			var zdRepo = res.data;
			if (status == '1') {
				if (zdRepo.bookId != null) {
					$.messager.show({title: '敬告', msg: '课本关联的知识无法修改！'});
					// return;
				} else {
					$("#repositoryEditWin").dialog({title: '添加行业',resizable:true});
					winToggle.open("repositoryEditWin");
					$('#repositoryEditWin').get(0).contentWindow.load(id,currRepositoryId);
				}
			} else {
				$("#repositoryEditWin").dialog({title: '添加行业',resizable:true});
				winToggle.open("repositoryEditWin");
				$('#repositoryEditWin').get(0).contentWindow.load(id,currRepositoryId);
			}	
		});
	}
	if(id != '0'){
		$("#repositoryEditWin").dialog({title: '修改行业',resizable:true});
		winToggle.open("repositoryEditWin");
		$('#repositoryEditWin').get(0).contentWindow.load(id,currRepositoryId);
	}
	//winToggle.open("repositoryEditWin");
	//$('#repositoryEditWin').get(0).contentWindow.load(id,currRepositoryId);
} 

<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
function vieRepository(id){
	winToggle.open("repositoryViewWin");
	$('#repositoryViewWin').get(0).contentWindow.load(id);
}

<%-- 删除记录 --%>
function delRepository(){
	var checkedItems = $('#repositoryGrid').datagrid('getChecked');
	console.log(checkedItems.length);
	var canDel = true;
	for (var i = 0; i < checkedItems.length; i++) {
		var checkedItem = checkedItems[i];
		if (checkedItem.bookId != null) {
			canDel = false;
		}
	}
	if (!canDel) {
		// $.messager.alert('敬告','课本关联的知识无法删除！');
		$.messager.show({title: '敬告', msg: '课本关联的知识无法删除！'});
		return;
	}
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		// $.messager.alert('敬告','请选择删除的记录？');
		$.messager.show({title: '敬告', msg: '请选择删除的记录？'});
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/camp/repository/delRepository.action",{ids:ids},function(data){
				$('#repositoryGrid').datagrid('reload');
				$('#repositorytree').tree('reload');
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	var src = $("#repositoryEditWin").attr("src");
	$("#repositoryEditWin").attr("src","");
	$("#repositoryEditWin").attr("src",src);
	$('#repositoryEditWin').dialog('close');
	$('#repositoryGrid').datagrid('reload');
	$('#repositorytree').tree('reload');
}
</script>
</head>
<body class="easyui-layout">
<div region="west" split="true" title="知识库" style="width:260px;">
		<dc:includeTree dictId="ZD_REPOSITORY" name="repositorytree" style="synch_single" bind="select:openRepository"/>
</div>
<div id="content" region="center" style="border:0;">
	<table id="repositoryGrid" title="" fitcolumns=true
		data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#repositoryGridToolbar',collapsible:true,url:'/${projectName}/camp/repository/schRepository.action',method:'get'">
		<thead>
			<tr>
				<th data-options="field:'checkbox',checkbox:true" ></th>
				<th data-options="field:'label',width:80,sortable:true">名称</th>
				<th data-options="field:'pidLabel',sortable:true">父节点</th>
	
				<th data-options="field:'_oper',align:'center',formatter:formatRepositoryOper">操作</th>
			</tr>
		</thead>
	</table>
</div>
<%-- 查询条件设定栏 --%>
<div id="repositoryGridToolbar">
	<div class="opline">
		
	</div>
	<form id="_form" class="shline">
		 名称：<input name='label' id="label" class='easyui-textbox' value='' data-options="iconWidth:16,
		icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}}]"/> 
	<a href="javascript:void(0)" onclick="doRepositorySearch()" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>&emsp;
	<a href="javascript:updRepository(0)" class="btn btn-success"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">添加</span></a>&emsp;
		<a href="javascript:delRepository()" class="btn btn-danger"><span class="icon-trash" style="font-size:16px"></span> <span style="font-size:14px">删除</span></a>
	</form>
</div>


<%-- 修改or添加面板 --%>
<iframe id="repositoryEditWin" src="/${projectName}/camp/repository/page.action?page=zdRepository_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#repositoryEditButton'" title="编辑" style="width:450px;height:350px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="repositoryEditButton">
	<a href="javascript:void(0)" onclick="$('#repositoryEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#repositoryEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >取 消</a>
</div>
<%-- 查看面板 --%>
<iframe id="repositoryViewWin" src="/${projectName}/camp/repository/page.action?page=zdRepository_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#repositoryViewButton'" title="查看" style="width:400px;height:200px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="repositoryViewButton">
	<a href="javascript:$('#repositoryViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >关 闭</a>
</div>
</body>
</html>
