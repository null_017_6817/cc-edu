<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.qtrmoon.com/tags-dictionary" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>查看</title><!--hyanswer-->
	<%@include file="/common/include.jsp" %>
<script>
function load(id){
	$('#_form').form('load','/${projectName}/camp/hyanswer/vieHyanswer.action?toL=true&id='+id);
};
</script>
</head>
<body>
<form id="_form">
	<input type="hidden" name='id' value=""/>
	<input type="hidden" name='fromagentid' value=""/>
	<input type="hidden" name='toagentid' value=""/>
	<input type="hidden" name='type' value=""/>
	<table>
		<tr><td class='label'>拨打人学号：</td><td><input type='text' name='fromno' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>接听人学号：</td><td><input type='text' name='tono' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>任务id（电信行业场景id）：</td><td><input type='text' name='taskid' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>创建时间：</td><td><input type='text' name='createtime' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>修改时间：</td><td><input type='text' name='updatetime' class='viewbox' readonly='readonly'/></td></tr>
	</table>

</form>
</body>
</html>
