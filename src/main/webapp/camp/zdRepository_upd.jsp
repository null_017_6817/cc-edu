<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.qtrmoon.com/tags-dictionary" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>编辑</title><!--repository-->
	<%@include file="/common/include.jsp" %>
	<style>
		td > span {
			width: 155px !important;
		}
	</style>
<script>
<%-- 查询页点击修改时调用此方法加载记录详情 --%>
function load(id,pid){
	if(pid>0){
		$('#_form').form('load','/${projectName}/camp/repository/vieRepository.action?id='+id+'&pid='+pid);
	}else{
		$('#_form').form('load','/${projectName}/camp/repository/vieRepository.action?id='+id);
	}
	$('#_form').form({
	    onLoadSuccess : function(data) {
	      	$("#label").textbox('textbox').css("width","141px");
	    }
	});
};

<%-- 点击保存按钮时调用此方法。(注:保存按钮在查询列表页) --%>
function submitForm(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#_form').form('submit',{
      success:function(data) {
      	parent.reloadData();
      }
   });
}
</script>
</head>
<body>
<form id="_form" method="post" action="/${projectName}/camp/repository/updRepository.action">
	<input type="hidden" name='id' id='id'/>
	<table>
		<tr><td class='label' style="width:120px">父节点：</td><td><dc:insertTree name="pid" dictId="ZD_REPOSITORY" style="synch_single"/></td></tr>
		<tr><td class='label' >名称：</td><td><input id="label" name="label" class="easyui-textbox" data-options="required:true" /></td></tr>
	</table>

</form>
</body>
</html>
