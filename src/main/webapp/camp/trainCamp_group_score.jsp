<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>查看实训营地评分</title><!--trainCamp-->
	<%@include file="/common/include.jsp" %>
<script>
var bookid =0,studid = 0,xzpfrid = 0;
function load(_bookid,_studid,_xzpfrid){
	bookid = _bookid;
	studid = _studid;
	if(_xzpfrid!=undefined){
		xzpfrid = _xzpfrid;
	}
	$('#_form').form('load','/${projectName}/camp/trainCamp/viewGroupScoreDetail.action?bookid='+_bookid+'&studid='+_studid+'&xzpfrid='+xzpfrid);
	
	$('#_form').form({
		onLoadSuccess : function(data) {
			$('#ratio').val(data.scoreVo.ratio+'%');
			$('#finalScore').val(data.scoreVo.finalScore);
			var scoreDetails = data.scoreVo.scoreDetails;
			var memberScores = data.scoreVo.memberScores;
			$('#ydgjzc').val(data.scoreVo.ydgjzc);
			
			//渲染小组成员评分
			rendGroupMemberScores(memberScores);
			
			//渲染评分明细
			rendScoreDetails(scoreDetails);
		
		}
	});
}

//渲染评分明细
function rendScoreDetails(scoreDetails){
	var tables = '';
	for(var i = 0 ;i < scoreDetails.length;i++){
		var scoreDetail = scoreDetails[i];
		tables+='<table class="zhibiao"><thead>';
		tables+='<tr align="center"><td style="width:5%">分类</td>';
		tables+='<td style="width:65%">考核指标</td><td style="width:5%">评分</td>';
		tables+='</tr></thead>';
		var indexScores = scoreDetail.indexScores;
		for(var k = 0 ; k <indexScores.length;k++ ){
			var indexScore = indexScores[k];
			if(k==0) {
				tables+='<tr><td rowspan="4">'+scoreDetail.category+'（'+scoreDetail.ratio+'%）</td>';
				tables+='<td>'+indexScore.indexContent+'</td><td>'+indexScore.score+'</td></tr>';
			}else {
				tables+='<td>'+indexScore.indexContent+'</td><td>'+indexScore.score+'</td></tr>';
			}
		}
		
		tables+='</table>';
	}
	$('#pfmx').html(tables);
}


//渲染小组成员评分
function rendGroupMemberScores(memberScores){
	var tables = '';
	tables+='<table class="zhibiao"><thead>';
	tables+='<tr align="center"><td style="width:25%">小组成员</td>';
	tables+='<td style="width:25%">学号</td><td style="width:25%">评分</td>';
	tables+='<td style="width:25%">查看评分表</td>';
	tables+='</tr></thead>';
	for(var i = 0 ;i < memberScores.length;i++){
		var memberScore = memberScores[i];
		tables+='<tr><td>'+memberScore.pfxsxm+'</td>';
		tables+='<td>'+memberScore.pfxsxh+'</td>';
		tables+='<td>'+memberScore.zf+'</td>';
		var checked = "";
		if(i==0){
			checked = "checked";
		}else {
			checked = memberScore.pfxsxh == xzpfrid ? "checked":"";
		}
		
		tables+='<td><input '+checked+' type="radio" name="preview" onclick="load('+bookid+','+studid+','+memberScore.pfxsxh+');"/>查看</td>';
		tables+='</tr>';
	}
	tables+='</table>';
	$('#members').html(tables);
}
</script>
<style>
	thead {background: #f2f2f2}
	.zhibiao{margin-left:12px;margin-top:14px;width:96%;}
	.zhibiao td {height: 27px!important;padding-left: 3px;border:1px solid #fcfcfc;text-align: center}
	.tab td {width: 25%;height:24px!important}
	.label {width:10%!important}
	.viewbox {width:100%!important}
	.group-tab {width:98% }
	.pfmx-td {vertical-align: top;padding-top:10px;width:3.5%!important;}
</style>
</head>
<body>
<form id="_form">
	<input type="hidden" name='id' value=""/>
	<table class="tab">
		<tr>
			<td class='label'>
			互评占比：
			</td>
			<td>
			<input type='text' id='ratio' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			最终得分：
			</td>
			<td>
				<input type='text' id='finalScore' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
	</table>
	<table class="group-tab">
		<tr>
			<td style="width:10%"></td>
			<td style="width:88%" id="members">
			</td>
		</tr>
	</table>
	
	<table class="tab group-tab">
		<tr>
			<td class='label pfmx-td'>评分明细：</td>
			<td style="border:1px solid #ccc" id="pfmx">
				<!-- <table class="zhibiao">
					<thead>
						<tr align="center">
							<td style="width:5%">分类</td>
							<td style="width:65%">考核指标</td>
							<td style="width:5%">评分</td>
						</tr>
					</thead>
					<tr>
							<td rowspan="4">问卷设计（40%）</td>
							<td>问卷设计111111</td>
							<td>80</td>
						</tr>
						<tr>
							<td>问卷设计111112</td>
							<td>80</td>
						</tr>
						<tr>
							<td>问卷设计111113</td>
							<td>80</td>
						</tr>
						<tr>
							<td>问卷设计111114</td>
							<td>80</td>
						</tr>
				</table>	 -->
			</td>
		</tr>
		<tr>
			<td class='label'>有待改进之处：</td>
			<td colspan="3">
				<textarea type='text' id='ydgjzc' class='viewbox' readonly='readonly' style="outline:none;vertical-align: middle;resize: none;margin-top: 19px;"/>
			</td>
		</tr>
	</table>
	

</form>
</body>
</html>
