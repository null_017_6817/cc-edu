<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>修改实训营地评分</title>
<!--trainCamp-->
<link rel="stylesheet" href="/${projectName}/book/js/lib/layui2.3/css/layui.css">
<link rel="stylesheet" href="/${projectName}/book/css/style.css">
<link rel="stylesheet" href="/${projectName}/book/css/common_score.css">
</head>

<body style="background:#f1f5f8;overflow-x:auto;">
	<div class="jbyhcon-title">
		<img src="/${projectName}/book/img/jbyh1.png">
		<p class="addorupdzp"></p>
	</div>
	<div class="jbyhcon-title2">
		<p style="margin-left:40px;"><strong>自评学生：</strong>${stuName}<strong style="margin-left:100px;">学号：</strong>${studid}</p>
	</div>
	<form class="layui-form" action="">
	
	</form>
<script src="/${projectName}/book/js/lib/jquery.min.js"></script>
<script src="/${projectName}/book/js/lib/layui2.3/layui.all.js"></script>
<script src="/${projectName}/book/js/common.js"></script>
<script src="/${projectName}/book/js/circleChart.min.js"></script>
<script type="text/javascript">
var khfl=0;
var zpf='${zpf}';
var templateId='${templateId}';
var id=null;
$(function(){
	if(zpf==null||zpf==""){
		$(".addorupdzp").text("填写自评表");
		schTemplate();
		khys(khfl);
	}else{
		$(".addorupdzp").text("编辑自评表");
		schZpxx();
		khys(khfl);
	}
});

	function cancel(){
		parent.closepj();
	}
	
	function save(){
		var pfxsxh='${studid}';
		var bpfxsxh='${studid}';
		var ydgjzc=$("#ydgjzc").val();
		var pflx=1;/* 评分类型（自评） */
		var pfrw='${topicId}';
		var zf=0;
		var scoreMsg = {};  //全局变量
		var modelArr=[];
		  $(".layui-form .jbyhpf-con").each(function(m){    //遍历分类
			  
		      var scoreObj=[];
		  	  var fl=$(this).find(".jbyhpf-con1-title2").attr("name");
		  	  var zb=$(this).find("#circleChart").attr("data-value");
		  	  var zfs=0;
		  	  var target=[];
		      $(this).find(".target").each(function(n){    //遍历指标
		    	  target.push($(this).attr("name"));
		      });
		      var grade=[];
		      var i=0;
		      $(this).find(".zscore").each(function(n){ 
		    	  grade.push($(this).val());
		    	  zfs+=Number($(this).val());
		    	  i++;
		      });
		      modelArr.push({"classify":fl,"target":target,"score":grade});
		      zfs=zfs/i*zb/100;
		      zf+=Number(zfs);			  
		      
		  });
		  scoreMsg = JSON.stringify(modelArr);
		 
		  var pfjg=scoreMsg;
	      
	      $.post("/${projectName}/camp/score/updScore.action",{id:id,pfxsxh:pfxsxh,bpfxsxh:bpfxsxh,pflx:pflx,pfjg:pfjg,zf:zf,pfrw:pfrw,templateId:templateId,ydgjzc:ydgjzc},function(data){
	    	  parent.reload();
		  });
	}
	
	function khys(khs){
		if(khs>0){
			for(var i=1;i<=khs;i++){
				if(i%2==0){
					$(".circleChart2").circleChart({
				        color: "#33cd98",
						backgroundColor: "#c2eee0",
						background: true,
						speed: 2000,
						widthRatio: 0.1,
						unit: 'percent',
						counterclockwise: false,
						size: 110,
						startAngle: 75,
						animate: true,
						backgroundFix: true,
						lineCap: "round",
						animation: "easeInOutCubic",
						text: 0,
						redraw: false,
						cAngle: 0,
						textCenter: true,
						textSize: 30,
						textWeight: 'normal',
						textFamily: 'sans-serif',
						relativeTextSize: 1 / 7,
						autoCss: true,
						onDraw: function(el, circle) {
				            circle.text(Math.round(circle.value) + "%");
				        }   
				    });
				}else{
					$(".circleChart1").circleChart({
				        color: "#1792ee",
						backgroundColor: "#b9defa",
						background: true,
						speed: 2000,
						widthRatio: 0.1,
						unit: 'percent',
						counterclockwise: false,
						size: 110,
						startAngle: 75,
						animate: true,
						backgroundFix: true,
						lineCap: "round",
						animation: "easeInOutCubic",
						text: 0,
						redraw: false,
						cAngle: 0,
						textCenter: true,
						textSize: 30,
						textWeight: 'normal',
						textFamily: 'sans-serif',
						relativeTextSize: 1 / 7,
						autoCss: true,
						onDraw: function(el, circle) {
				            circle.text(Math.round(circle.value) + "%");
				        }   
				    });
				}
			}
		}
	}
	
	function schTemplate(){
		$.ajax({
			async:false,
			type:"GET",
			url:'/${projectName}/camp/score/schTemplateCategory.action',
			dataType:"json",
			data:{
				templateId:templateId
			},
			success:function(response){
				var zpxs="";
				var score=response.score.split(",");
				if(response.rows.length>0){
					for(var i=0;i<response.rows.length;i++){
						khfl=response.rows.length;
						var zpx=response.rows[i];
						var xb=i+1;
						var bh=1;
						if(xb%2==0){
							bh=2;
						}
						zpxs+='<div class="jbyhpf-con"><div class="jbyhpf-con1"><p class="jbyhpf-con1-title">分类</p><p class="jbyhpf-con1-title2" name="'+zpx.id+'">'+zpx.classificationName+'</p>';
						zpxs+='<div class="circleChart'+bh+'" id="circleChart" data-value="'+zpx.calculationProportion+'" style="color:#1792ee;margin-bottom:10px;"></div></div>';
						
						var khzb=eval('(' + zpx.indexContent + ')');
						
						if(khzb!=null&&khzb.length>0){
							zpxs+='<div class="jbyhpf-con2"><p class="jbyhpf-con1-title">考核指标</p>';
							for(var j=0;j<khzb.length;j++){
								zpxs+='<p class="target" name="'+khzb[j].serialNumber+'"><span></span>'+khzb[j].indexContent+'</p>';
							}
							zpxs+='</div>';
							
							zpxs+='<div class="jbyhpf-con3"><p class="jbyhpf-con1-title" style="text-align:center;margin-left:-5px;">评分</p>';
							for(var k=0;k<khzb.length;k++){
								zpxs+='<div class="jbyhpf-con3pf"><img src="/${projectName}/book/img/jbyhpf2.png"><select class="zscore" name="zpf" >';
								if(score.length>0){
									for(var s=0;s<score.length;s++){
										zpxs+='<option value="'+score[s]+'">'+score[s]+'</option>';
									}
								}
								zpxs+='</select></div>';
							}
							
							zpxs+='</div><div class="clear"></div></div>';
						} 
					}
				}
				zpxs += '<div class="jbyhpf-texa"><label>有待改进之处：</label>'+
					'<textarea rows="" cols="" id="ydgjzc" name="ydgjzc"></textarea></div><div class="jbyhpf-btn">'+
					'<div class="jbyhpf-btn1" lay-submit="" lay-filter="jbyhZp" onclick="save()">保存</div>'+
					'<div class="jbyhpf-btn1" onclick="cancel()">取消</div></div>';
				$(".layui-form").append(zpxs);
				renderForm();
			}
		});
	}
	
	function schZpxx(){
		var pfxsxh='${studid}';
		var pfrw='${topicId}';
		$.ajax({
			async:false,
			type:"GET",
			url:'/${projectName}/camp/score/schSelfScore.action',
			dataType:"json",
			data:{
				pfxsxh:pfxsxh,
				pfrw:pfrw,
				pflx:1,
				templateId:templateId
				
			},
			success:function(response){
				if(response.sfzp==true){
					id=response.result.id;
					var score=response.score.split(",");
					var zpxs="";
					var zpjg=eval('(' + response.result.pfjg + ')'); 
					if(zpjg.length>0){
						for(var i=0;i<zpjg.length;i++){
							khfl=zpjg.length;
							var zpx=zpjg[i];
							var xb=i+1;
							var bh=1;
							if(xb%2==0){
								bh=2;
							}
							zpxs+='<div class="jbyhpf-con"><div class="jbyhpf-con1"><p class="jbyhpf-con1-title">分类</p><p class="jbyhpf-con1-title2" name="'+zpx.classify+'">'+zpx.classifyName+'</p>';
							zpxs+='<div class="circleChart'+bh+'" id="circleChart" data-value="'+Number(zpx.proportion)+'" style="color:#1792ee;margin-bottom:10px;"></div></div>';
							if(zpx.targets.length>0){
								zpxs+='<div class="jbyhpf-con2"><p class="jbyhpf-con1-title">考核指标</p>';
								for(var j=0;j<zpx.targets.length;j++){
									zpxs+='<p class="target" name="'+zpx.target[j]+'"><span></span>'+zpx.targets[j]+'</p>';
								}
								zpxs+='</div>';
							}
							
							zpxs+='<div class="jbyhpf-con3"><p class="jbyhpf-con1-title" style="text-align:center;margin-left:-5px;">评分</p>';
						
							if(zpx.score.length>0){
								for(var k=0;k<zpx.score.length;k++){
									zpxs+='<div class="jbyhpf-con3pf"><img src="/${projectName}/book/img/jbyhpf2.png"><select class="zscore" name="zpf" >';
									if(score.length>0){
										for(var s=0;s<score.length;s++){
											if(zpx.score[k]==score[s]){
												zpxs+='<option value="'+score[s]+'" selected>'+score[s]+'</option>';
											}else{
												zpxs+='<option value="'+score[s]+'">'+score[s]+'</option>';
											}
										}
									}
									zpxs+='</select></div>';
								}
							}
							
							zpxs+='</div><div class="clear"></div></div>';
						}
						zpxs += '<div class="jbyhpf-texa"><label>有待改进之处：</label>'+
						'<textarea rows="" cols="" id="ydgjzc" name="ydgjzc">'+response.result.ydgjzc+'</textarea></div><div class="jbyhpf-btn">'+
						'<div class="jbyhpf-btn1" lay-submit="" lay-filter="jbyhZp" onclick="save()">保存</div>'+
						'<div class="jbyhpf-btn1" onclick="cancel()">取消</div></div>';
						$(".layui-form").prepend(zpxs);
						renderForm();
					}
				}
			}
		});
	}
	
	//重新渲染表单
	function renderForm(){
	  layui.use('form', function(){
	   var form = layui.form;//高版本建议把括号去掉，有的低版本，需要加()
	   form.render();
	  });
	 }
	
</script>
<script src="/${projectName}/book/js/jbyh.js"></script>
</body>
</html>
