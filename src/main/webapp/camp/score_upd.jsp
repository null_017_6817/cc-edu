<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.qtrmoon.com/tags-dictionary" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>编辑评分表</title><!--score-->
	<%@include file="/common/include.jsp" %>
<script>
<%-- 查询页点击修改时调用此方法加载记录详情 --%>
function load(id){
	$('#_form').form('load','/${projectName}/camp/score/vieScore.action?id='+id);
};

<%-- 点击保存按钮时调用此方法。(注:保存按钮在查询列表页) --%>
function submitForm(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#_form').form('submit',{
      success:function(data) {
      	parent.reloadData();
      }
   });
}
</script>
</head>
<body>
<form id="_form" method="post" action="/${projectName}/camp/score/updScore.action">
	<input type="hidden" name='id' id='id'/>
	<table>
		<tr><td class='label' style="width:120px">模板ID：</td><td><input id="mbid" name="mbid" class="easyui-textbox" data-options="required:true,validType:['int']"/></td></tr>
		<tr><td class='label' >评分学生学号：</td><td><input id="pfxsxh" name="pfxsxh" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >被评分学生学号：</td><td><input id="bpfxsxh" name="bpfxsxh" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >评分类型（自评、互评、教师评）：</td><td><input id="pflx" name="pflx" class="easyui-textbox" data-options="required:true,validType:['int']"/></td></tr>
		<tr><td class='label' >评分结果：</td><td><input id="pfjg" name="pfjg" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >总分：</td><td><input id="zf" name="zf" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >评分日期：</td><td><input id="pfrqstr" name="pfrqstr" class="easyui-datebox" data-options="required:true"/></td></tr>
		<tr><td class='label' >评分任务：</td><td><input id="pfrw" name="pfrw" class="easyui-textbox" data-options="required:true,validType:['int']"/></td></tr>
	</table>

</form>
</body>
</html>
