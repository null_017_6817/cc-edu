<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>修改实训营地评分</title>
<!--trainCamp-->
<%@include file="/common/include.jsp"%>

<style>
#_table thead {
	background-color: #f0f0f0;
}

#_table thead th {
	padding: 8px 0;
	text-align: center;
}

#_table  td {
	height: 40px;
    line-height: 40px;
    padding: 0 5px;
    text-align: center;
    border-bottom: 1px #f3f3f3 solid;
}

#aa {
    display: block;
    padding: 5px 15px;
    margin: 30px auto 0;
    color: #fff;
    background: #f36501;
    border: none;
    border-radius: 4px;
    cursor: pointer;
    outline: none;
}

.abc {
	width: 88px;
    height: 24px;
    padding: 0 4px;
    outline: none;
}

.hr-dashed {
    height: 1px;
    border: 0;
    border-top: silver 1px dashed;
}

</style>
</head>

<body>
<span style="text-align: center;display: block;font-size:20px;padding: 20px 0;">小组评价与自评</span>
	<div style="font-size: 14px;margin-bottom:20px;padding: 0 20px;">
			学生选择：<select id="cc" name="classid" style="display: inline-block; width: 130px;height: 28px;line-height: 28px;
				vertical-align: middle;outline: none; margin-left: 4px;">
				<option>请选择</option>
			</select>
		</div>
	<form id="_form" style="padding: 0 20px;font-size:14px;">
		<table id="_table" style="width:100%;">
			<thead>
				<tr>
					<th>客服学号</th>
					<th>学生姓名</th>
					<th>脚本内容</th>
					<th>任务完成</th>
					<th>小组合作</th>
					<th>操作</th>
				</tr>
			</thead>
		</table>
		<input id="aa" type="button" value="保存" onclick="javascript:updTeachers()">
	</form>
		
</body>

<script type="text/javascript">	
$(function(){
	var cnum = $("#classid").val();
	var url = "/${projectName}/camp/trainCamp/schStud.action?cnum=${cnum}";
	$.getJSON(url, function(data) {
		/* $('#cc').combobox({
			data : json.rows , valueField : 'number' , textField : 'name' , onSelect : function() {
				var partnerid = $("#cc").combobox('getValue');
				$("#partnerid").textbox('setValue', partnerid);
			} , onLoadSuccess : function() {
			}
		}); */
		$.each(data.rows, function(i, obj){
			$("#cc").append("<option  value="+obj.number+">"+obj.name+"</option>")
		});
	});
});
$("#cc").change(changeClassAddTable());
function changeClassAddTable(){
 	$("#cc").change(function(){
 		var id = $("option:selected").attr("value");
 		$.ajax({
 			url:"/${projectName}/camp/trainCamp/schStudentEvaluate.action",
 			type:"post",
 			data:{
 				"classid":'${cnum}',
 				"bookId":'${bookId}',
 				"studid":id
 				},
 			dataType: "json",
 			success:function(data){
 				$("#_table").find("tbody").html('');
 				$.each(data,function(i,obj){
 					var currstu = ${CURRENT_USER.number};
 					if(currstu == id) {
	 					var a=obj.personalscore1;
	 					if(a==undefined){a=0};
	 					var b=obj.personalscore2;
	 					if(b==undefined){b=0};
	 					var c=obj.personalscore3;
	 					if(c==undefined){c=0};
	 					var paramId=obj.id;
	 					if(paramId==undefined){paramId=""};
	 					var paramClassid=obj.classid;
	 					if(paramClassid==undefined){paramClassid=""};
	 					var d = obj.num;
						var html = "<tr name='dataTr'>"
							+	"<td id='a'><input name='studid' type='text' readonly='readonly' style='border:none;width:100px; text-align:center;' value="+obj.studid+" ></td>"
							+	"<td>"+obj.name
							+		"<input type='hidden' name='id' value='"+paramId+"' />"
							+		"<input type='hidden' name='bookid' value='${bookId}' />"
							+		"<input type='hidden' name='classid' value='"+paramClassid+"' />"
							+	"</td>"
							+	"<td><input class='abc' type='text' name='personalscore1' value="+a+"></td>"
							+	"<td><input class='abc' type='text' name='personalscore2' value="+b+"></td>"
							+	"<td><input class='abc' type='text' name='personalscore3' value="+c+"></td>"
							+	"<td>"
							+		"<a href='javascript:void(0);' onclick=\"exportFile('${bookId}','"+obj.studid+"');\" >导出脚本</a>&nbsp;&nbsp;"
							+		"<a href='javascript:void(0);' onclick=\"showVideos('${bookId}','"+obj.studid+"');\" >录音</a>&nbsp;&nbsp;"
							+		"<a href='javascript:void(0);' onclick=\"showOrderList('${bookId}','"+obj.studid+"');\" >工单</a>"
							+	"</td>"
							+"</tr>";
 					}else{
 						var a=obj.partnerscore1;
	 					if(a==undefined){a=0};
	 					var b=obj.partnerscore2;
	 					if(b==undefined){b=0};
	 					var c=obj.partnerscore3;
	 					if(c==undefined){c=0};
	 					var paramId=obj.id;
	 					if(paramId==undefined){paramId=""};
	 					var paramClassid=obj.classid;
	 					if(paramClassid==undefined){paramClassid=""};
 						var html = "<tr name='dataTr'>"
							+	"<td id='a'><input name='studid' type='text' readonly='readonly' style='border:none;width:100px; text-align:center;' value="+obj.studid+" ></td>"
							+	"<td>"+obj.name
							+		"<input type='hidden' name='id' value='"+paramId+"' />"
							+		"<input type='hidden' name='bookid' value='${bookId}' />"
							+		"<input type='hidden' name='classid' value='"+paramClassid+"' />"
							+	"</td>"
							+	"<td><input class='abc' type='text' name='partnerscore1' value="+a+"></td>"
							+	"<td><input class='abc' type='text' name='partnerscore2' value="+b+"></td>"
							+	"<td><input class='abc' type='text' name='partnerscore3' value="+c+"></td>"
							+	"<td>"
							+		"<a href='javascript:void(0);' onclick=\"exportFile('${bookId}','"+obj.studid+"');\" >导出脚本</a>&nbsp;&nbsp;"
							+		"<a href='javascript:void(0);' onclick=\"showVideos('${bookId}','"+obj.studid+"');\" >录音</a>&nbsp;&nbsp;"
							+		"<a href='javascript:void(0);' onclick=\"showOrderList('${bookId}','"+obj.studid+"');\" >工单</a>"
							+	"</td>"
							+"</tr>";
 					}
 					/* if(d!=null){
 					}else{
 						var html = "<tr name='dataTr'>"
 							+	"<td id='a'><input name='studid' type='text' readonly='readonly' style='border:none;width:100px; text-align:center;' value="+obj.studid+" ></td>"
 							+	"<td>"+obj.name
 							+		"<input type='hidden' name='id' value='"+paramId+"' />"
 							+		"<input type='hidden' name='bookid' value='${bookId}' />"
 							+		"<input type='hidden' name='classid' value='"+paramClassid+"' />"
 							+	"</td>"
 							+	"<td><input class='abc' type='text' name='teacherscore1' value="+a+" disabled='disabled'></td>"
 							+	"<td><input class='abc' type='text' name='teacherscore2' value="+b+" disabled='disabled'></td>"
 							+	"<td><input class='abc' type='text' name='teacherscore3' value="+c+" disabled='disabled'></td>"
 							+	"<td><a href='javascript:void(0);' onclick=\"exportFile('${bookId}','"+obj.studid+"');\" >导出脚本</a>&nbsp;"
 							+		 "<a href='javascript:void(0);' onclick=\"showVideos('${bookId}','"+obj.studid+"');\" >录音</a></td>"
 							+"</tr>";
 					} */
 				
 					$("#_table").append(html);
 				});
 				window.parent.cahngeHeight($(document).height());
 			},
 		});
   });
}

// 展示
function showVideos(taskId,stuNum){
	window.parent.showVideos(taskId,stuNum);
}

function showOrderList(taskId,stuNum){
	window.parent.showOrderList(taskId,stuNum);
}

// 保存按钮，实现更新功能
 function updTeachers(){
	 var id = $("option:selected").attr("value");
    var jsonStr= $("#_form").serializeArrayToJson();
	 $.ajax({
		 url: "/${projectName}/camp/trainCamp/updStudCore.action?stuNum="+id,
		 data:jsonStr,
		 type:"post",
		 contentType:"application/json",
		 success:function(result){
			if(result != null){
				for(var i = 0; i < result.length; i++){
					$("tr[name='dataTr']").find("input[name='id']").eq(i).val(result[i].id);
				}
			}
			window.parent.swal({title:"保存成功！",
				type:"success"});
			
		 },
		 error:function(){
			 window.parent.swal({title:"服务器异常！",
					type:"error"});
				
		 }
	 });
 }
 
function reloadData(){
		$('#_table').datagrid('reload');
}


//多个表单转json数组
$.fn.extend({
	//多个表单转json数组
	serializeArrayToJson : function() {
        var a = this.serializeArray();
        var b = [];
        var o = {};
        $.each(a, function() {
        	var name = this.name;
            if (o[name]) {
            	//如果o[name]没有嵌套在{}内
            	if(!o[name].push) {
            		b.push(o);
            		o = {};
            		o[name] = this.value || '';
            	}
            } else {
                o[name] = this.value || '';
            }
        });
        b.push(o);
        return JSON.stringify(b);
	}
});

// 导出学生脚本
function exportFile(taskId,number){
	//location.href="/${projectName}/examination/mark/exportSxAnswerToWord.action?taskId="+taskId+"&number="+number;
	window.parent.open("/${projectName}/examination/mark/exportSxAnswerToWord.action?taskId="+taskId+"&number="+number);
}
</script>
</html>
