<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>查看实训营地评分</title><!--trainCamp-->
	<%@include file="/common/include.jsp" %>
<script>
function load(_bookid,_studid){
	$('#_form').form('load','/${projectName}/camp/trainCamp/viewPersonScoreDetail.action?bookid='+_bookid+'&studid='+_studid);
	
	$('#_form').form({
		onLoadSuccess : function(data) {
			$('#ratio').val(data.scoreVo.ratio+'%');
			$('#totalScore').val(data.scoreVo.totalScore);
			$('#finalScore').val(data.scoreVo.finalScore);
			$('#ydgjzc').val(data.scoreVo.ydgjzc);
			var scoreDetails = data.scoreVo.scoreDetails;
		
			rendScoreDetails(scoreDetails);
		
		}
	});
}
function rendScoreDetails(scoreDetails){
	var tables = '';
	for(var i = 0 ;i < scoreDetails.length;i++){
		var scoreDetail = scoreDetails[i];
		tables+='<table class="zhibiao"><thead>';
		tables+='<tr align="center"><td style="width:5%">分类</td>';
		tables+='<td style="width:65%">考核指标</td><td style="width:5%">评分</td>';
		tables+='</tr></thead>';
		var indexScores = scoreDetail.indexScores;
		for(var k = 0 ; k <indexScores.length;k++ ){
			var indexScore = indexScores[k];
			if(k==0) {
				tables+='<tr><td rowspan="4">'+scoreDetail.category+'（'+scoreDetail.ratio+'%）</td>';
				tables+='<td>'+indexScore.indexContent+'</td><td>'+indexScore.score+'</td></tr>';
			}else {
				tables+='<td>'+indexScore.indexContent+'</td><td>'+indexScore.score+'</td></tr>';
			}
		}
		
		tables+='</table>';
	}
	$('#pfmx').html(tables);
}
</script>
<style>
	thead {background: #f2f2f2}
	.zhibiao{margin-left:12px;margin-top:14px;width:96%;}
	.zhibiao td {height: 27px!important;padding-left: 3px;border:1px solid #fcfcfc;text-align: center}
	td {width: 25%;height:24px!important}
	.label {width:10%!important}
	.viewbox {width:100%!important}
</style>
</head>
<body>
<form id="_form">
	<input type="hidden" name='id' value=""/>
	<table>
		<tr>
			<td class='label'>
			自评学生：
			</td>
			<td>
			<input type='text' name='stuName' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			学号：
			</td>
			<td>
				<input type='text' name='stuNo' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			自评总分：
			</td>
			<td>
			<input type='text' id='totalScore' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			自评占比：
			</td>
			<td>
				<input type='text' id="ratio" name='ratio' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			最终得分：
			</td>
			<td>
			<input type='text' id='finalScore' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		
		<tr>
			<td class='label' style="vertical-align: top;padding-top:10px;">评分明细：</td>
			<td style="border:1px solid #ccc" colspan="3" id='pfmx'>
				
			</td>
		</tr>
		<tr>
			<td class='label'>有待改进之处：</td>
			<td colspan="3">
				<textarea type='text' id='ydgjzc' class='viewbox' readonly='readonly' style="outline:none;vertical-align: middle;resize: none;margin-top: 19px;"/>
			</td>
		</tr>
	</table>
	

</form>
</body>
</html>
