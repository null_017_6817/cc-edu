<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>实训营地评分查询</title><!--trainCamp-->
	<%@include file="/common/include.jsp" %>

<script>
$(function(){
	<%-- 为分页行设置"添加"和"删除"按钮 --%>
	var pager = $('#trainCampGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});			
})

<%-- 执行查询，点击查询按钮时调用 --%>
function doSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	
	$('#trainCampGrid').datagrid('load',{
		/* id:$('#id').val(), */
		taskName:$('#taskName').val(),
		zjId:$("input[name='bookId']").val(),
		/* studid:$('#studid').val(),
		partnerid:$('#partnerid').val(),
		teacherid:$('#teacherid').val(),
		personalscore1:$('#personalscore1').val(),
		personalscore2:$('#personalscore2').val(),
		personalscore3:$('#personalscore3').val(),
		partnerscore1:$('#partnerscore1').val(),
		partnerscore2:$('#partnerscore2').val(),
		partnerscore3:$('#partnerscore3').val(),
		teacherscore1:$('#teacherscore1').val(),
		teacherscore2:$('#teacherscore2').val(),
		teacherscore3:$('#teacherscore3').val(),
		tm:$('#tm').val(),
		score:$('#score').val(), */
		classid:$("input[name='classid']").val()
	});
}
<%-- 执行查询，点击查询按钮时调用 --%>
function doWeightSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$.ajax({
		url:"/${projectName}/camp/trainCamp/weightscore.action",
		type:"post",
		data:{"personweight" : $('#personweight').val(), "partnerweight" : $('#partnerweight').val(), "teacherweight" : $('#teacherweight').val()},
		success:function(data) {
			//alert("计算成功");
			doSearch();
		}
	});
	
	
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatOper(val,row,index){  
    //return /* "<span onclick='updTrainCamp("+val+")' style='text-decoration:underline;cursor:pointer;'>修改</span> " */
    return "<span onclick='vieTrainCampTeacherScore("+val+")' style='text-decoration:underline;cursor:pointer;'>查看</span>";
} 

<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
/* function updTrainCamp(id){
	QMoveDialog($("#trainCampEditWin"));
	$('#trainCampViewWin').dialog('close');
	$('#trainCampEditWin').dialog('open');
	$('#trainCampEditWin').get(0).contentWindow.load(id);
}  */

<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
/* function vieTrainCamp(id){
	QMoveDialog($("#trainCampViewWin"));
	$('#trainCampEditWin').dialog('close');
	$('#trainCampViewWin').dialog('open');
	$('#trainCampViewWin').get(0).contentWindow.load(id);
} */


<%-- 打开查看自评分页面 --%>
function vieTrainCampSelfScore(_bookid,_studid){
	QMoveDialog($("#trainCampSelfScoreWin"));
	$('#trainCampGroupScoreWin').dialog('close');
	$('#trainCampTeacherScoreWin').dialog('close');
	$('#trainCampSelfScoreWin').dialog('open');
	$('#trainCampSelfScoreWin').get(0).contentWindow.load(_bookid,_studid);
}


<%-- 打开查看互评分页面 --%>
function vieTrainCampGroupScore(_bookid,_studid){
	QMoveDialog($("#trainCampGroupScoreWin"));
	$('#trainCampGroupScoreWin').dialog('open');
	$('#trainCampTeacherScoreWin').dialog('close');
	$('#trainCampSelfScoreWin').dialog('close');
	$('#trainCampGroupScoreWin').get(0).contentWindow.load(_bookid,_studid);
}

<%-- 打开查看教师评分页面 --%>
function vieTrainCampTeacherScore(_bookid,_studid,_teacherid){
	QMoveDialog($("#trainCampTeacherScoreWin"));
	$('#trainCampGroupScoreWin').dialog('close');
	$('#trainCampTeacherScoreWin').dialog('open');
	$('#trainCampSelfScoreWin').dialog('close');
	$('#trainCampTeacherScoreWin').get(0).contentWindow.load(_bookid,_studid,_teacherid);
}

<%-- 删除记录 --%>
function delTrainCamp(){
	var checkedItems = $('#trainCampGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/camp/trainCamp/delTrainCamp.action",{ids:ids},function(data){
				$('#trainCampGrid').datagrid('reload');
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#trainCampEditWin').dialog('close');
	$('#trainCampGrid').datagrid('reload');
}

 <%--自评分格式化 --%>
function formatPersonScore(val,row,index){ 
	if(val==null || val ==0){
		return '';
	}
	 return "<a href='javascript:void(0)' onclick=vieTrainCampSelfScore('"+row.bookid+"',"+row.studid+")>"+val+"</a>";
} 


<%--教师评分格式化 --%>
function formatTeacherScore(val,row,index){ 
	if(val==null || val ==0){
		return '';
	}
	 return "<a href='javascript:void(0)' onclick=vieTrainCampTeacherScore('"+row.bookid+"',"+row.studid+",'"+row.teacherid+"')>"+val+"</a>";
} 


<%--小组评分格式化 --%>
function formatGroupScore(val,row,index){ 
	if(val==null || val ==0){
		return '';
	}
	 return "<a href='javascript:void(0)' onclick=vieTrainCampGroupScore('"+row.bookid+"',"+row.studid+")>"+val+"</a>";
} 


</script>
</head>
<body>
<table id="trainCampGrid" title="实训营地评分" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#trainCampGridToolbar',collapsible:true,url:'/${projectName}/camp/trainCamp/schTrainCamp.action',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" ></th>
			<!-- <th data-options="field:'id',width:80,sortable:true">主键</th> -->
			<th data-options="field:'zhangjieId',width:6,sortable:true">课程章节</th>
			<th data-options="field:'booktitle',width:4,sortable:true">任务名称</th>
			<th data-options="field:'studname',width:2,sortable:true">学生</th>
			<th data-options="field:'classid',width:4,sortable:true">班级名称</th>
			<th data-options="field:'teachername',sortable:true">评分老师</th>
			<th data-options="field:'personalscore1',sortable:true,formatter:formatPersonScore">自评分</th>
			<th data-options="field:'partnerscore1',sortable:true,formatter:formatGroupScore">小组评分</th>
			<th data-options="field:'teacherscore1',sortable:true,formatter:formatTeacherScore">教师评分</th>
			<!-- <th data-options="field:'personalscore1',sortable:true">知识掌握(自评)</th>
			<th data-options="field:'personalscore2',sortable:true">任务完成(自评)</th>
			<th data-options="field:'personalscore3',sortable:true">小组合作(自评)</th>
			<th data-options="field:'partnerscore1',sortable:true">知识掌握(互评)</th>
			<th data-options="field:'partnerscore2',sortable:true">任务完成(互评)</th>
			<th data-options="field:'partnerscore3',sortable:true">小组合作(互评)</th>
			<th data-options="field:'teacherscore1',sortable:true">知识掌握(师评)</th>
			<th data-options="field:'teacherscore2',sortable:true">任务完成(师评)</th>
			<th data-options="field:'teacherscore3',sortable:true">小组合作(师评)</th> -->
			<!-- <th data-options="field:'tm',sortable:true">修改时间</th> -->
			<th data-options="field:'score',sortable:true">总分</th>
			<th data-options="field:'tm',width:4,sortable:true">任务完成日期</th>

			<!--  <th data-options="field:'_oper',align:'center',formatter:formatOper">操作</th>  -->
		</tr>
	</thead>
</table>
<%-- 查询条件设定栏 --%>
<div id="trainCampGridToolbar">
	<div style="margin-bottom:5px">
		<!-- <a href="javascript:updTrainCamp(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true">添加</a>
		<a href="javascript:delTrainCamp()" class="easyui-linkbutton" iconCls="icon-remove" plain="true">删除</a> -->
	</div>
	<form id="_form">
		<!-- 主键：<input name='id' id="id" class='easyui-textbox' value='' data-options="iconWidth:16,
		icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
		}]"/> -->
			
			课程章节：
			<dc:insertTree name="bookId" dictId="CC_BOOK" style="synch_single"/>
			
			任务：<input name='taskName' id="taskName" class='easyui-textbox' value='' data-options="iconWidth:16,
		icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
		}]"/>
			<!-- 学生id：<input name='studid' id="studid" class='easyui-textbox' value='' data-options="iconWidth:16,
		icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
		}]"/>
			组队学生：<input name='partnerid' id="partnerid" class='easyui-textbox' value='' data-options="iconWidth:16,
		icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
		}]"/>
			评分老师：<input name='teacherid' id="teacherid" class='easyui-textbox' value='' data-options="iconWidth:16,
		icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
		}]"/>
			知识掌握自评：<input name='personalscore1' id="personalscore1" class='easyui-textbox' value='' data-options="iconWidth:16,
		icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
		}]"/>
			任务完成互评：<input name='personalscore2' id="personalscore2" class='easyui-textbox' value='' data-options="iconWidth:16,
		icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
		}]"/>
			小组合作教师评：<input name='personalscore3' id="personalscore3" class='easyui-textbox' value='' data-options="iconWidth:16,
		icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
		}]"/>
			知识掌握：<input name='partnerscore1' id="partnerscore1" class='easyui-textbox' value='' data-options="iconWidth:16,
		icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
		}]"/>
			任务完成互评：<input name='partnerscore2' id="partnerscore2" class='easyui-textbox' value='' data-options="iconWidth:16,
		icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
		}]"/>
			小组合作教师评：<input name='partnerscore3' id="partnerscore3" class='easyui-textbox' value='' data-options="iconWidth:16,
		icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
		}]"/>
			知识掌握：<input name='teacherscore1' id="teacherscore1" class='easyui-textbox' value='' data-options="iconWidth:16,
		icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
		}]"/>
			任务完成互评：<input name='teacherscore2' id="teacherscore2" class='easyui-textbox' value='' data-options="iconWidth:16,
		icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
		}]"/>
			小组合作教师评：<input name='teacherscore3' id="teacherscore3" class='easyui-textbox' value='' data-options="iconWidth:16,
		icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
		}]"/>
			时间：<input name='tm' id="tm" class="easyui-datebox" style="width:150px;" data-options="iconWidth:16,
		icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
		}]"/>
			总分：<input name='score' id="score" class='easyui-textbox' value='' data-options="iconWidth:16,
		icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
		}]"/> -->
			班级：
			<dc:insertList dictId="CC_CLASSES" name="classid" style="combo_normal"/>
			<!-- 自评权重：<input name='personweight' id="personweight" class='easyui-numberspinner' value='3' data-options="iconWidth:16,
		icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
		}]"/> -->
			<!-- 互评权重：<input name='partnerweight' id="partnerweight" class='easyui-numberspinner' value='3' data-options="iconWidth:16,
		icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
		}]"/>
			老师权重：<input name='teacherweight' id="teacherweight" class='easyui-numberspinner' value='4' data-options="iconWidth:16,
		icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
		}]"/> -->
	
		<a href="#" class="easyui-linkbutton btn-info" iconCls="icon-search" onclick="doSearch()">查询</a>
	<!-- 	<a href="#" class="easyui-linkbutton btn-info" iconCls="icon-search" onclick="doWeightSearch()">按权重计算总分</a> -->
	</form>
</div>


<%-- 修改or添加面板 --%>
<%-- <iframe id="trainCampEditWin" src="/${projectName}/camp/trainCamp/page.action?page=trainCamp_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#trainCampEditButton'" title="修改实训营地评分" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="trainCampEditButton">
	<a href="javascript:void(0)" onclick="$('#trainCampEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#trainCampEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >取 消</a>
</div> --%>
<%-- 查看面板 --%>
<%-- <iframe id="trainCampViewWin" src="/${projectName}/camp/trainCamp/page.action?page=trainCamp_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#trainCampViewButton'" title="查看实训营地评分" style="width:550px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="trainCampViewButton">
	<a href="javascript:$('#trainCampViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >关 闭</a>
</div>
 --%>

<%-- 查看自评分 --%>
<iframe id="trainCampSelfScoreWin" src="/${projectName}/camp/trainCamp/page.action?page=trainCamp_self_score" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#trainCampSelfScoreViewButton'" title="查看自评分" style="width:750px;height:700px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="trainCampSelfScoreViewButton">
	<a href="javascript:$('#trainCampSelfScoreWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >关 闭</a>
</div>

<%-- 查看互评分 --%>
<iframe id="trainCampGroupScoreWin" src="/${projectName}/camp/trainCamp/page.action?page=trainCamp_group_score" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#trainCampGroupScoreViewButton'" title="查看小组评分" style="width:750px;height:700px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="trainCampGroupScoreViewButton">
	<a href="javascript:$('#trainCampGroupScoreWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >关 闭</a>
</div>

<%-- 查看师评分 --%>
<iframe id="trainCampTeacherScoreWin" src="/${projectName}/camp/trainCamp/page.action?page=trainCamp_teacher_score" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#trainCampTeacherScoreViewButton'" title="查看教师评分" style="width:750px;height:700px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="trainCampTeacherScoreViewButton">
	<a href="javascript:$('#trainCampTeacherScoreWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >关 闭</a>
</div>

</body>
</html>
