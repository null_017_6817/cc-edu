<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>修改实训营地评分</title>
<!--trainCamp-->
<%@include file="/common/include.jsp"%>
<script>
	
<%-- 点击保存按钮时调用此方法。(注:保存按钮在查询列表页) --%>
	function submitForm() {
		if (!$("#_form").form("validate") ) {
			return;
		}
		$('#_form').form('submit', {
			success : function(data) {
				window.parent.swal({title:"保存成功！",
					type:"success"});
				}
			
		});
	}

	function clearForm() {
		$('#_form').form('clear');
	}
<%-- 页面加载时调用 --%>
	$(function() {
		
<%-- 自我评价分数累计 --%>
	$("input", $("#personalscore1,#personalscore2,#personalscore3").next("span")).blur(function() {
			if ($("#personalscore1").val() == null || $("#personalscore1").val() == '' ) {
				$("#personalscore1").textbox('setValue', 0);
			}
			if ($("#personalscore2").val() == null || $("#personalscore2").val() == '' ) {
				$("#personalscore2").textbox('setValue', 0);
			}
			if ($("#personalscore3").val() == null || $("#personalscore3").val() == '' ) {
				$("#personalscore3").textbox('setValue', 0);
			}
			$("#score1").textbox('setValue', parseFloat($("#personalscore1").val()) + parseFloat($("#personalscore2").val()) + parseFloat($("#personalscore3").val()));
		})
<%-- 小组互评分数累计 --%>
	$("input", $("#partnerscore1,#partnerscore2,#partnerscore3").next("span")).blur(function() {
			if ($("#partnerscore1").val() == null || $("#partnerscore1").val() == '' ) {
				$("#partnerscore1").textbox('setValue', 0);
			}
			if ($("#partnerscore2").val() == null || $("#partnerscore2").val() == '' ) {
				$("#partnerscore2").textbox('setValue', 0);
			}
			if ($("#partnerscore3").val() == null || $("#partnerscore3").val() == '' ) {
				$("#partnerscore3").textbox('setValue', 0);
			}
			$("#score2").textbox('setValue', parseFloat($("#partnerscore1").val()) + parseFloat($("#partnerscore2").val()) + parseFloat($("#partnerscore3").val()));
		})
<%-- 页面加载时填充表单数据 --%>
	$('#_form').form('load', '/${projectName}/camp/trainCamp/getTrainCamp.action?studid=${studid}&bookid=${bookId}');
		var pid; //小组互评时需要的partnerid
<%-- 页面数据加载成功时调用，回显表单数据 --%>
	$('#_form').form({
			onLoadSuccess : function(data) {
				pid = data.partnerid;
				$("#score1").textbox('setValue', data.personalscore1 + data.personalscore2 + data.personalscore3);
				$("#score2").textbox('setValue', data.partnerscore1 + data.partnerscore2 + data.partnerscore3);
			}
		});
		var cnum = $("#classid").val();//从index页面传递的班级号
<%-- 加载学生下拉列表 --%>
	var url = "/${projectName}/camp/trainCamp/schStudNotMe.action?cnum=${cnum}";
		$.getJSON(url, function(json) {
			$('#cc').combobox({
				data : json.rows , valueField : 'number' , textField : 'name' , onSelect : function() {
					var partnerid = $("#cc").combobox('getValue');
					$("#partnerid").textbox('setValue', partnerid);
				} , onLoadSuccess : function() {
					$('#cc').combobox('setValue', pid);
				}
			});
		});
	})
</script>

<style>
.operateBtn {
	margin-top: 30px;
	text-align: center;
}

.operateBtn .easyui-linkbutton {
	display: inline-block;
	padding: 0 15px;
	margin-right: 10px;
	color: #fff;
	background: #f36501;
	border: none;
	border-radius: 4px;
	cursor: pointer;
	outline: none;
}
</style>
</head>

<body>
	<div style="padding: 20px 100px">
		<form id="_form" method="post" action="/${projectName}/camp/trainCamp/updTrainCamp.action">
			<input type="hidden" name='id' id='id' />
			<input type="hidden" name="classid" id="classid" value="${cnum}" />
			<input type="hidden" name='teacherscore1' id='teacherscore1' value="0" />
			<input type="hidden" name='teacherscore2' id='teacherscore2' value="0" />
			<input type="hidden" name='teacherscore3' id='teacherscore3' value="0" />
			
			<table style="width: 100%;font-size: 14px;">
			<tr style="height: 50px;">
					<td colspan="4" style="font-size:18px;text-align:center;">小组互评</td>
				</tr>
				<tr>
					<td class='label'>学生账号：</td>
					<td>
						<select id="cc" class="easyui-combobox"	style="width: 157px;"></select>
					</td>
				</tr>
				<tr style="display: none;">
					<td class='label'>组队学生：</td>
					<td>
						<input type="hidden" id="partnerid" name="partnerid" class="easyui-textbox" data-options="required:true" />
					</td>
				</tr>
				<tr>
					<td class='label'>知识掌握：</td>
					<td>
						<input id="partnerscore1" name="partnerscore1" value="0" class="easyui-textbox" data-options="required:false" />
					</td>
					<td class='label'>任务完成互评：</td>
					<td>
						<input id="partnerscore2" name="partnerscore2" value="0" class="easyui-textbox" data-options="required:false" />
					</td>
				</tr>
				<tr>
					<td class='label'>小组合作教师评：</td>
					<td>
						<input id="partnerscore3" name="partnerscore3" value="0" class="easyui-textbox" data-options="required:false" />
					</td>
					<td class='label'>总分：</td>
					<td>
						<input id="score2" name="score2" class="easyui-textbox" value="0" readonly="true" data-options="required:false" />
					</td>
				</tr>
				<tr>
					<td colspan="4" style="font-size:18px;text-align:center;padding: 10px 0 20px 0;">学生自评</td>
				</tr>
				<tr style="display: none;">
					<td class='label'>任务：</td>
					<td>
						<input type="hidden" id="bookid" name="bookid" value="${bookId}" class="easyui-textbox" data-options="required:true" />
					</td>
					<td class='label'>学生id：</td>
					<td>
						<input type="hidden" id="studid" name="studid" value="${studid}" class="easyui-textbox" data-options="required:true" />
					</td>
				</tr>
				<tr>
					<td class='label'>知识掌握自评：</td>
					<td>
						<input id="personalscore1" name="personalscore1" value="0" class="easyui-textbox" data-options="required:false" />
					</td>
					<td class='label'>任务完成互评：</td>
					<td>
						<input id="personalscore2" name="personalscore2" value="0" class="easyui-textbox" data-options="required:false" />
					</td>
				</tr>
				<tr>
					<td class='label'>小组合作教师评：</td>
					<td>
						<input id="personalscore3" name="personalscore3" value="0" class="easyui-textbox" data-options="required:false" />
					</td>
					<td class='label'>总分：</td>
					<td>
						<input id="score1" name="score1" class="easyui-textbox" value="0" readonly="true" data-options="required:false" />
					</td>
				</tr>					
			</table>
		</form>
		
		<div class="operateBtn">
			<a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()">提交</a>
			<a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearForm()">重置</a>
		</div>
	</div>

</body>
</html>
