<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.qtrmoon.com/tags-dictionary" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>编辑</title><!--hyanswer-->
	<%@include file="/common/include.jsp" %>
<script>
<%-- 查询页点击修改时调用此方法加载记录详情 --%>
function load(id){
	$('#_form').form('load','/${projectName}/camp/hyanswer/vieHyanswer.action?id='+id);
};

<%-- 点击保存按钮时调用此方法。(注:保存按钮在查询列表页) --%>
function submitForm(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#_form').form('submit',{
      success:function(data) {
      	parent.reloadData();
      }
   });
}
</script>
</head>
<body>
<form id="_form" method="post" action="/${projectName}/camp/hyanswer/updHyanswer.action">
	<input type="hidden" name='id' id='id'/>
	<input type="hidden" name='fromagentid' id='fromagentid'/>
	<input type="hidden" name='toagentid' id='toagentid'/>
	<input type="hidden" name='type' id='type'/>
	<table>
		<tr><td class='label' style="width:120px">拨打人学号：</td><td><input id="fromno" name="fromno" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >接听人学号：</td><td><input id="tono" name="tono" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >任务id（电信行业场景id）：</td><td><input id="taskid" name="taskid" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >创建时间：</td><td><input id="createtimestr" name="createtimestr" class="easyui-datebox" data-options="required:true"/></td></tr>
		<tr><td class='label' >修改时间：</td><td><input id="updatetimestr" name="updatetimestr" class="easyui-datebox" data-options="required:true"/></td></tr>
	</table>

</form>
</body>
</html>
