<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.qtrmoon.com/tags-dictionary" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>查询</title><!--hytopic-->
	<%@include file="/common/include.jsp" %>
	<script type="text/javascript" charset="utf-8"	src="/${projectName}/ueditor/ueditor.config.js"></script>
	<script type="text/javascript" charset="utf-8"	src="/${projectName}/ueditor/ueditor.all.js"></script>
<script>
var winToggle;

$(function(){
	initUE();
});


function initUE(){
	var ue = UE.getEditor('editor', {
		toolbars : [ [ 
		'simpleupload','source', //源代码
		'undo', //撤销
		'redo', //重做
		'formatmatch', //格式刷
		'bold', //加粗
		'indent', //首行缩进
		'italic', //斜体
		'underline', //下划线
		'strikethrough', //删除线
		'time', //时间
		'date', //日期
		'justifyleft', //居左对齐
		'justifyright', //居右对齐
		'justifycenter', //居中对齐
		'justifyjustify', //两端对齐
		'fullscreen', //全屏
		'autotypeset', //自动排版
		'touppercase', //字母大写
		'tolowercase', //字母小写
		'forecolor', // 字体颜色
		'fontsize', // 字体大小
		'rowspacingtop',/*段前*/
		'rowspacingbottom',/*段前*/
		'lineheight',/*行间距*/
		'inserttable', //插入表格
		'insertrow', //前插入行
		'insertcol', //前插入列
		'mergeright', //右合并单元格
		'mergedown', //下合并单元格
		'deleterow', //删除行
		'deletecol', //删除列
		'splittorows', //拆分成行
		'splittocols', //拆分成列
		'splittocells', //完全拆分单元格
		'deletecaption', //删除表格标题
		'inserttitle', //插入标题
		'mergecells', //合并多个单元格
		'deletetable', //删除表格
		'backcolor', //背景色
		] ] , autoHeightEnabled : false , autoFloatEnabled : true , elementPathEnabled : false
	}); 
	
	UE.Editor.prototype._bkGetActionUrl = UE.Editor.prototype.getActionUrl;  
	UE.Editor.prototype.getActionUrl = function(action){  
		if(action == 'uploadimage' || action == 'uploadscrawl' || action == 'uploadimage'){  
			return '/${projectName}/uploaditemimage.action';  
		}else{  
			return this._bkGetActionUrl.call(this, action);  
		}
	}
	
	
}

var currentId;
//编辑章节目标
function load(id){
	UE.getEditor('editor').setContent("");
	$("input[name='title']").val("");
	bookIdDict.setValue("");
	currentId = id;
	$.ajax({
		type : "POST",
		url : '/${projectName}/camp/hytopic/getHytopicById.action',
		data : {id:id},	
		dataType:'json',
		success : function(result) {
			if(result.success){
				if(result.data != null){
					$("#goalDIV").find("input[name='id']").val(currentId);
					$("#goalDIV").find("#bookId").combobox('setValue', result.data.rid);
					$("#goalDIV").find("input[name='title']").val(result.data.title);
					UE.getEditor('editor').setContent(result.data.content);
				}else{
					$("#goalDIV").find('#bookId').combobox('setValue', "");
					$("#goalDIV").find("input[name='title']").val("");
					UE.getEditor('editor').setContent("");
				}
			}
		}
	}); 
}

// 保存数据
function saveGoal(){
	var content = UE.getEditor('editor').getContent();// 答案
	var title = $("#goalDIV").find("input[name='title']").val();
	var bookId = bookIdDict.getValue();
	var id = currentId;
	if(id == 0){
		id=null;
	}
	$.ajax({
		type : "POST",
		url : '/${projectName}/camp/hytopic/saveHytopic.action',
		data : {"id":id,"rid":bookId,"content":content,"title":title},	
		dataType:'json',
		success : function(result) {
			if(result.success){
				parent.closeCj();
			}else{
				alert(result.msg);
			}
		}
	});
}


function resetData(){
	UE.getEditor('editor').setContent("");
}
</script>
</head>
<body>

<%-- 行业场景修改 --%>
<div id="goalDIV" title="">
	<div style="padding: 20px 10px 10px;">
		<input type="hidden" name="id" type="text" />
		<span>标题：<input name="title" type="text" style="height:22px;line-height:22px;vertical-align:middle;padding-left:5px;" /></span>
		<span>任务：
			<dc:insertList name="bookId" dictId="ZD_TRAININDUSTY" style="combo_normal" />
		</span>
		<script id="editor" type="text/plain" style="width:99%;height:300px;margin:10px;"></script>
	</div>
</div>
</body>
</html>
