<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>查看评价</title>
<!--trainCamp-->
<link rel="stylesheet" href="/${projectName}/book/js/lib/layui2.3/css/layui.css">
<link rel="stylesheet" href="/${projectName}/book/css/style.css">
<link rel="stylesheet" href="/${projectName}/book/css/common_score.css">
</head>

<body style="background:#f1f5f8;overflow-x:auto;">
	<div class="jbyhcon-title">
		<img src="/${projectName}/book/img/jbyh1.png">
		<p>查看评分表</p>
	</div>
	<div class="jbyhckpf-zbox">
		<div class="jbyhckpf-zb">
			<img src="/${projectName}/book/img/jbyh5.png">
			<p class="zpfzb"></p>
		</div>
		<div class="jbyhckpf-zb">
			<img src="/${projectName}/book/img/jbyh6.png">
			<p class="xzpfzb"></p>
		</div>
		<div class="jbyhckpf-zb">
			<img src="/${projectName}/book/img/jbyh7.png">
			<p class="jspfzb"></p>
		</div>
	</div>
	<div class="jbyhckpf-conl">

	</div>
	<div class="jbyhckpf-conr">
		
	</div>
<script src="/${projectName}/book/js/lib/jquery.min.js"></script>
<script src="/${projectName}/book/js/lib/layui2.3/layui.all.js"></script>
<script src="/${projectName}/book/js/common.js"></script>
<script src="/${projectName}/book/js/circleChart.min.js"></script>
<script src="/${projectName}/book/js/jbyh.js"></script>
<script type="text/javascript">
var pfrw='${topicId}'; //评分任务
var pfid=0; //评分id
var khfl=0;	//考核分类个数
var templateId='${templateId}';//模板id
$(function(){
	schMyScore();
});

/* 查询给我评分的记录 */
function schMyScore(){
	$.ajax({
		async:false,
		type:"GET",
		url:'/${projectName}/camp/score/schMyScore.action',
		dataType:"json",
		data:{
			pfrw:pfrw,
			templateId:templateId
		},
		success:function(response){
			var score=response.score.split(":");
			if(response.rows.length>0){
				var	pfrHtml='';
				for(var i=0;i<response.rows.length;i++){
					var pf=response.rows[i]
					if(i==0){
						pfrHtml+='<div class="jbyhckpf-conls"><div class="jbyhckpf-conlsbtn on" name="'+pf.id+'" onclick="schpf(this)">查看评分</div>';
						pfid=pf.id;
					}else{
						pfrHtml+='<div class="jbyhckpf-conls"><div class="jbyhckpf-conlsbtn" name="'+pf.id+'" onclick="schpf(this)">查看评分</div>';
					}
					pfrHtml+='<p class="jbyhckpf-conlsp1">评分人：<span class="jbyhckpf-conlsp2">'+pf.pfrxm+'</span></p>';
					pfrHtml+='<p class="jbyhckpf-conlsp1">得分：<span class="jbyhckpf-conlsp3"><strong>'+pf.zf+'</strong></span><span class="jbyhckpf-conlsp4">分</span></p></div>';
				}
				$(".jbyhckpf-conl").html(pfrHtml);
			}
			if(score.length>0){
				for(var j=0;j<score.length;j++){
					if(j==0){
						$(".zpfzb").text('自评分占比：'+score[0]+'%');
					}else if(j==1){
						$(".xzpfzb").text('小组评分占比：'+score[1]+'%');
					}else{
						$(".jspfzb").text('教师评分占比：'+score[2]+'%');
					}
				}
			}
		}
	});
	schScoreById(pfid);
}

function schpf(obj){
	var id=$(obj).attr("name"); 
	$(".jbyhckpf-conlsbtn").each(function(){
		$(this).removeClass("on");
	})
	$(obj).attr("class","jbyhckpf-conlsbtn on");
	schScoreById(id);
}

/* 根据评分id查询评分信息并显示 */
function schScoreById(id){
	$.ajax({
		async:false,
		type:"GET",
		url:'/${projectName}/camp/score/schScoreById.action',
		dataType:"json",
		data:{
			pfid:id
		},
		success:function(response){
			var zpxs="";
			var zpjg=eval('(' + response.result.pfjg + ')'); 
			if(zpjg.length>0){
				for(var i=0;i<zpjg.length;i++){
					khfl=zpjg.length;
					var zpx=zpjg[i];
					var xb=i+1;
					var bh=1;
					if(xb%2==0){
						bh=2;
					}
					zpxs+='<div class="jbyhckpf-conr-title"><div class="jbyhckpf-conr-title1" style="width:110px;"><p>分类</p></div>';
					zpxs+='<div class="jbyhckpf-conr-title1" style="width:220px;"><p>考核指标</p></div><div class="jbyhckpf-conr-title1" style="width:70px;"><p>评分</p></div></div>';
					zpxs+='<div class="jbyhckpf-conrbox"><div class="jbyhckpf-conrpf1"><p class="jbyhckpf-conrxbt">'+zpx.classifyName+'</p>';
					zpxs+='<div class="circleChart'+bh+'" id="circleChart" data-value="'+Number(zpx.proportion)+'" style="color:#1792ee;margin-bottom:10px;"></div></div>';
					zpxs+='<div class="jbyhckpf-conrpf2">';
					for(var j=0;j<zpx.targets.length;j++){
						zpxs+='<p><span></span>'+zpx.targets[j]+'</p>';
					}
					zpxs+='</div><div class="jbyhckpf-conrpf3">';
					if(zpx.score.length>0){
						for(var k=0;k<zpx.score.length;k++){
							zpxs+='<div class="jbyhckpf-conrpf3-num"><img src="/${projectName}/book/img/jbyhpf2.png">';
							zpxs+='<input type="text" name="title" disabled="disabled" value="'+zpx.score[k]+'"></div>';
						}
					}
					zpxs+='</div><div class="clear"></div></div>';
				}
			}
			zpxs+='<div class="jbyhpf-texa2"><label style="font-weight: 600;">有待改进之处：</label><span>'+response.result.ydgjzc+'</span></div>';
			$(".jbyhckpf-conr").html(zpxs);
		}
	});
	khys(khfl);
}

/* 给评分分类加颜色  */
function khys(khs){
	if(khs>0){
		for(var i=1;i<=khs;i++){
			if(i%2==0){
				$(".circleChart2").circleChart({
			        color: "#33cd98",
					backgroundColor: "#c2eee0",
					background: true,
					speed: 2000,
					widthRatio: 0.1,
					unit: 'percent',
					counterclockwise: false,
					size: 110,
					startAngle: 75,
					animate: true,
					backgroundFix: true,
					lineCap: "round",
					animation: "easeInOutCubic",
					text: 0,
					redraw: false,
					cAngle: 0,
					textCenter: true,
					textSize: 30,
					textWeight: 'normal',
					textFamily: 'sans-serif',
					relativeTextSize: 1 / 7,
					autoCss: true,
					onDraw: function(el, circle) {
			            circle.text(Math.round(circle.value) + "%");
			        }   
			    });
			}else{
				$(".circleChart1").circleChart({
			        color: "#1792ee",
					backgroundColor: "#b9defa",
					background: true,
					speed: 2000,
					widthRatio: 0.1,
					unit: 'percent',
					counterclockwise: false,
					size: 110,
					startAngle: 75,
					animate: true,
					backgroundFix: true,
					lineCap: "round",
					animation: "easeInOutCubic",
					text: 0,
					redraw: false,
					cAngle: 0,
					textCenter: true,
					textSize: 30,
					textWeight: 'normal',
					textFamily: 'sans-serif',
					relativeTextSize: 1 / 7,
					autoCss: true,
					onDraw: function(el, circle) {
			            circle.text(Math.round(circle.value) + "%");
			        }   
			    });
			}
		}
	}
}
</script>
</body>
</html>
