<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>修改实训营地评分</title>
<!--trainCamp-->
<link rel="stylesheet" href="/${projectName}/book/js/lib/layui2.3/css/layui.css">
<link rel="stylesheet" href="/${projectName}/book/css/style.css">
<link rel="stylesheet" href="/${projectName}/book/css/common_score.css">
<style>
   .jbyhcon1{
      margin:93px 0;
   }
   #xzpf{
       height: 354px;
       margin: 90px 0 0 0;
   }
   .jbyhcon-right{
        height: 444px;
        overflow:hidden;
   }
   .jbyhcon-btn{
      margin:50px auto 70px;
   }
   .byhcon1-midqp{
      margin-top: 15px;
   }
</style>
</head>

<body style="background:#eff3f5;padding:7px;box-sizing:border-box;">
	<div class="jbyh-box">
		<div class="jbyhcon">
			<div class="jbyhcon-title">
				<img src="/${projectName}/book/img/jbyh1.png">
				<p>自评分</p>
			</div>
			<div class="jbyhcon1">
				<div class="jbyhcon1-mid">
					<div class="byhcon1-midq">
					<!-- 是否已打分样式 -->
						<img class="jbyhzpfimg pfnone" id="zpf" src="/${projectName}/book/img/jbyh3.png" onclick="openzp()">
						<p class="jbyhzpfp1"></p>
						<p class="jbyhzpfp2">分</p>
					</div>
					<p class="byhcon1-midqp">自评得分</p>
				</div>
				<div class="jbyhcon1-mid">
					<div class="byhcon1-midq2">
					<!-- 是否已打分样式 -->
						<img class="jbyhzdfimg" src="/${projectName}/book/img/jbyh3.png">
						<p class="jbyhzdfp1 pfnone">99</p>
						<p class="jbyhzdfp2 pfnone">分</p>
					</div>
					<p class="byhcon1-midqp">总得分</p>
				</div>	
			</div>
			<div class="jbyhcon-btn">
				<div class="jbyhcon-btn1" onclick="exportFile(${studid})">导出脚本</div>
				<div class="jbyhcon-btn1" onclick="showVideos(${studid})" id="videos"></div>
				<div class="jbyhcon-btn1" onclick="showOrderList(${studid})" id="orders"></div>
			</div>
			<div class="jbyhcon-btn2" onclick="openzp()">
				<img src="/${projectName}/book/img/jbyh4.png">
				<p>评分</p>
			</div>
		</div>
		<div class="jbyhcon" style="margin-left:2%;">
			<div class="jbyhcon-title">
				<img src="/${projectName}/book/img/jbyh2.png">
				<p>小组评分</p>
			</div>
			<!-- 有无评分样式 -->
			<div class="jbyhcon1 pfnone  jbyhcon-right" id="xzpf">
				<img class="jbyhcon1img" src="/${projectName}/book/img/nocon.png" style="margin-top:10px;">
				<p class="jbyhcon1p">暂无评分</p>
			</div>
			<div class="jbyhcon2  jbyhcon-right">	
			</div>
			 <div class="jbyhcon-btn">
			</div>
			<div class="jbyhcon-btn2" onclick="openxzp()">
				<img src="/${projectName}/book/img/jbyh4.png">
				<p>评分</p>
			</div>
		</div>
	</div>
<script src="/${projectName}/book/js/lib/jquery.min.js"></script>
<script src="/${projectName}/book/js/lib/layui2.3/layui.all.js"></script>
<script src="/${projectName}/book/js/common.js"></script>
<script src="/${projectName}/book/js/jbyh.js"></script>
<script>
var stus="";
$(function(){
	var topicId=parent.reTopicId();
	var pfxsxh='${studid}';
	var pfrw=topicId;
	schSelfScore(pfxsxh,pfrw);
	var videosSize=getRecordsSize(pfxsxh,pfrw);
	var billSize=serchBillSzie(pfxsxh,pfrw);
	$("#videos").html("录音（"+videosSize+"）");
	$("#orders").html("工单（"+billSize+"）");
	schGroup(pfxsxh,pfrw);
	schMyTotalScore(pfxsxh,pfrw);
	
})
function schSelfScore(_xh,_pfrw){
	$.ajax({
		async:false,
		type:"GET",
		url:'/${projectName}/camp/score/schSelfScore.action',
		dataType:"json",
		data:{
			pfxsxh:_xh,
			pfrw:_pfrw,
			pflx:1
			
		},
		success:function(response){
			if(response.sfzp==true){
				$(".jbyhzpfp1").text(response.result.zf);
				$(".byhcon1-midq").attr("onclick","openzp()");
				
			}else{
				$("#zpf").attr("class","jbyhzdfimg");
				$(".jbyhzpfp2").text("");
			}
		}
	});
}
function schGroup(_xh,_pfrw){
	$.ajax({
		async:false,
		type:"GET",
		url:'/${projectName}/camp/score/schSelfScore.action',
		dataType:"json",
		data:{
			pfxsxh:_xh,
			pfrw:_pfrw,
			pflx:2
			
		},
		success:function(response){
			if(response.rows.length>0){
				var hpHtml='';
				for(var i=0;i<response.rows.length;i++){
					var hp=response.rows[i];
					var j=i+1;
					var size=getRecordsSize(hp.bpfxsxh,_pfrw);
					var billSize=serchBillSzie(hp.bpfxsxh,_pfrw);
					hpHtml+='<div class="jbyhxz"><div class="jbyhxz1"><p>'+j+'</p></div>';
					if(hp.bpfxstx!=null&&hp.bpfxstx!=''){
						hpHtml+='<div class="jbyhxz2"><div class="jbyhxz2-img" onclick="openXzpUpd('+hp.id+')"><img src="/${projectName}/imgs/photos/'+hp.bpfxsbj+'/'+hp.bpfxstx+'"></div>';
					}else{
						hpHtml+='<div class="jbyhxz2"><div class="jbyhxz2-img" onclick="openXzpUpd('+hp.id+')"><img src="/${projectName}/book/img/tx.png"></div>';
					}
					hpHtml+='<div class="jbyhxz2-p"><p class="jbyhxz2-p3">'+hp.zf+'分</p><p class="jbyhxz2-p1">'+hp.bpfxsxm+'</p>';
					hpHtml+='<p class="jbyhxz2-p2">学号：'+hp.bpfxsxh+'</p><div class="jbyhxz2-ps"><p style="margin-left:0px;" onclick="showOrderList('+hp.bpfxsxh+')">工单（'+billSize+'）</p>';
					hpHtml+='<p onclick="showVideos('+hp.bpfxsxh+')">录音（'+size+'）</p><p onclick="exportFile('+hp.bpfxsxh+')">导出脚本</p></div></div></div><div class="clear"></div></div>';
					stus+=hp.bpfxsxh;
					if(i<response.rows.length-1){
						stus+=",";
					}
				}
				$(".jbyhcon2").html(hpHtml);
			}else{
				$("#xzpf").attr("class","jbyhcon1");
				$(".jbyhcon-right").css("display","none")
			}
		}
	});
}

function schMyTotalScore(_xh,_pfrw){
	$.ajax({
		async:false,
		type:"GET",
		url:'/${projectName}/camp/score/schMyTotalScore.action',
		dataType:"json",
		data:{
			bpfxsxh:_xh,
			pfrw:_pfrw,
		},
		success:function(response){
			var totalScore=response.totalScore;
			if(response.complete==true){
				$(".byhcon1-midq2").attr("onclick","openckzf()");
				$(".byhcon1-midq2 img").attr("class","jbyhzpfimg pfnone");
				$(".byhcon1-midq2 p").eq(0).attr("class","jbyhzdfp1");
				$(".byhcon1-midq2 p").eq(0).text(totalScore);
				$(".byhcon1-midq2 p").eq(1).attr("class","jbyhzdfp2");
			}else{
				$(".byhcon1-midq2 img").attr("class","jbyhzpfimg");
				$(".byhcon1-midq2 p").eq(0).attr("class","jbyhzdfp1 pfnone");
				$(".byhcon1-midq2 p").eq(1).attr("class","jbyhzdfp2 pfnone");
			}
		}
	});
}
/* 查询录音条数 */
function getRecordsSize(_xh,_pfrw){
	var size=0;
	$.ajax({
		async:false,
		type:"GET",
		url:'/${projectName}/camp/trainCamp/getRecordsSize.action',
		dataType:"json",
		data:{
			taskId:_pfrw, 
			toNum:_xh
		},
		success:function(response){
			size=response.data;
		}
	});
	return size;
}

/* 查询工单条数 */
function serchBillSzie(_xh,_pfrw){
	var size=0;
	$.ajax({
		async:false,
		type:"POST",
		url:'/${projectName}/practical/workorder/serchBillSzie.action',
		dataType:"json",
		data:{
			stuNum:_xh,
			taskId:_pfrw,
			type:1
		},
		success:function(response){
			size=response.size;
		}
	});
	return size;
}

/* 打开自评页面 */
function openzp(){
	var zpf=$(".jbyhzpfp1").text();
	parent.updZp(zpf);
}
/* 打开小组评页面 */
function openxzp(){
	parent.updXzp(-1,stus);
}
/* 打开修改小组评页面 */
function openXzpUpd(hpid){
	parent.updXzp(hpid,stus);
}
/* 打开查看评价页面 */
function openckzf(){
	parent.viePj();
}

	// $(function () {
	// 	var show_day=new Array('星期日','星期一','星期二','星期三','星期四','星期五','星期六'),
	// 		time=new Date(),
	// 		year=time.getFullYear(),
	// 		month=time.getMonth()+1, 
	// 		date=time.getDate(), 
	// 		day=time.getDay(); 
			
	// 	//2017年09月08日  &nbsp; 星期五
	// 	$('.info').find($('.date')).html(year+'年'+month+'月'+date+'日'+'  '+show_day[day]);
	// }) 
	
// 导出学生脚本
function exportFile(number){
	var taskId=parent.reTopicId();

	window.parent.open("/${projectName}/examination/mark/exportSxAnswerToWord.action?taskId="+taskId+"&number="+number);
}

//展示
function showVideos(stuNum){
	var taskId=parent.reTopicId();
	window.parent.showVideos(taskId,stuNum);
}

function showOrderList(stuNum){
	var taskId=parent.reTopicId();
	window.parent.showOrderList(taskId,stuNum);
}
</script>
</body>
</html>
