<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.qtrmoon.com/tags-dictionary" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>查看电信问卷</title><!--dxwj-->
	<%@include file="/common/include.jsp" %>
<script>
function load(id){
	$('#_form').form('load','/${projectName}/camp/dxwj/vieDxwj.action?toL=true&id='+id);
};
</script>
</head>
<body>
<form id="_form">
	<input type="hidden" name='id' value=""/>
	<input type="hidden" name='content' value=""/>
	<input type="hidden" name='createtime' value=""/>
	<input type="hidden" name='createby' value=""/>
	<input type="hidden" name='updatetime' value=""/>
	<input type="hidden" name='updateby' value=""/>
	<table>
		<tr><td class='label'>关联id：</td><td><input type='text' name='rid' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>标题：</td><td><input type='text' name='title' class='viewbox' readonly='readonly'/></td></tr>
	</table>

</form>
</body>
</html>
