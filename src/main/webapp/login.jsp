<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<script type="text/javascript" src="/${projectName}/easyui1_5/jquery.min.js"></script>
	<style>
	body{padding:0;margin:0;background:#35383d;}
	.use-flex{display:flex;flex-direction:column;height:100%;position:absolute;width:100%}
	.use-flex div{}
	
	.top{
		flex:none;height:100px;
		background: -webkit-linear-gradient(top,#55535e 0,#35383d 100%);
		background: -moz-linear-gradient(top,#55535e 0,#35383d 100%);
		background: -o-linear-gradient(top,#55535e 0,#35383d 100%);
		background: linear-gradient(to bottom,#55535e 0,#35383d 100%);
	}
	
	.top div span {
		display: inline-block;
    	line-height: 30px;
    	padding-left: 6px;
    	vertical-align: sub;
    	border-left: 2px #ccc solid;
    	color: #ccc;
	}
	
	.center{
		background:url('/${projectName}/imgs/login/bk2.png') no-repeat;
		background-size:100%;
		flex:1;
	}
	
	.oper{margin:20% 0 0 30%; padding-top: 20px;}
	.oper .btn1{
		margin: 0;
		width:12%;
		height:50px;
		float:left;
		cursor:pointer;
		opacity:.8;
	}
	.oper .btn1{background:url('/${projectName}/imgs/login/btnlg.png') 0 0 no-repeat;}
	.oper .btn1:hover{opacity:1}
	
	.frm{color:white;font-size:20px;line-height:50px;width:400px;float:left;text-align:center;}
	.frm input{
		width: 60%;
		border-radius: 4px;
		height: 32px;
		line-height: 32px;
		padding: 0 6px;
	}
	</style>
	<script type="text/javascript">
	$(function(){
		setSize();
		$('.btn1').click(function(){
			$("#loginForm").submit();
		});
	});
	$(window).resize(setSize);
	function setSize(){
		console.log($(".btn1").width()/3);
		$(".oper .btn1").css({"height":$(".oper .btn1").width(),"background-size":$(".oper .btn1").width()});
		$(".frm").css({"width":$(".btn1").width()*3,"height":$(".btn1").width(),"line-height":$(".btn1").width()/3+"px","font-size":$(".btn1").width()/3+"px"});
	}
	</script>
</head>
<body>
	<div class="use-flex">
		<div class="top">
			<div style="margin:15px 0 0 25%;color:#ccc;">
				<img src="/${projectName}/imgs/login/huatang.png" style="vertical-align:middle;"/>
				<span> 全国服务外包及联络中心/实训平台与专业建设的领航者</span>
			</div>
		</div>
		<div class="center">
			<div class="oper">
				<div class="frm">
					<form id="loginForm" method="post" action="/${projectName}/sysmanage/login/login.action">
						<input type="text" name="number" placeholder="请输入登录名"/><br/>
						<input type="password" name="password" placeholder="请输入密码"/><br/>
						<input type="hidden" name="state" value="2"/>
					</form>
				</div> 
				<div class="btn1"></div>
			</div>
		</div>
	</div>
</body>
</html>