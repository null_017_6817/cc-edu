<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.qtrmoon.com/tags-dictionary" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>附件表（邮件用）查询</title><!--attac-->
	<%@include file="/common/include.jsp" %>

<script>
var winToggle;
$(function(){
	winToggle=new QWinToggle().regist(["attacEditWin","attacViewWin"]);
	var pager = $('#attacGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});			
})

<%-- 执行查询，点击查询按钮时调用 --%>
function doAttacSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#attacGrid').datagrid('load',{
		type:$('#type').val(),
		url:$('#url').val(),
		name:$('#name').val(),
		rid:$('#rid').val(),
		createTimeBeg: $('#createTimeBeg').val(),
		createTimeEnd: $('#createTimeEnd').val()
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatAttacOper(val,row,index){  
    return "&emsp;<a href='javascript:void(0)' onclick='updAttac("+val+")' class='_oper' title='修改'>&nbsp;<i class='icon-edit'></i>&nbsp;</a>"
    			+"<a href='javascript:void(0)' onclick='vieAttac("+val+")' class='_oper' title='查看'>&nbsp;<i class='icon-info-sign'></i>&nbsp;</a>&emsp;"; 
} 

<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
function updAttac(id){
	winToggle.open("attacEditWin");
	$('#attacEditWin').get(0).contentWindow.load(id);
} 

<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
function vieAttac(id){
	winToggle.open("attacViewWin");
	$('#attacViewWin').get(0).contentWindow.load(id);
}

<%-- 删除记录 --%>
function delAttac(){
	var checkedItems = $('#attacGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/mail/attac/delAttac.action",{ids:ids},function(data){
				$('#attacGrid').datagrid('reload');
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#attacEditWin').dialog('close');
	$('#attacGrid').datagrid('reload');
}
</script>
</head>
<body>
<table id="attacGrid" title="" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#attacGridToolbar',collapsible:true,url:'/${projectName}/mail/attac/schAttac.action',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" ></th>
			<th data-options="field:'type',width:80,sortable:true">文件类型</th>
			<th data-options="field:'url',sortable:true">文件路径</th>
			<th data-options="field:'name',sortable:true">文件名称</th>
			<th data-options="field:'rid',sortable:true">关联ID</th>
			<th data-options="field:'createTime',sortable:true">创建时间</th>

			<th data-options="field:'_oper',align:'center',formatter:formatAttacOper">操作</th>
		</tr>
	</thead>
</table>
<%-- 查询条件设定栏 --%>
<div id="attacGridToolbar">
	<div class="opline">
		<a href="javascript:updAttac(0)" class="btn btn-success"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">添加</span></a>&emsp;
		<a href="javascript:delAttac()" class="btn btn-danger"><span class="icon-trash" style="font-size:16px"></span> <span style="font-size:14px">删除</span></a>
	</div>
	<form id="_form" class="shline">
	文件类型：<input name='type' id="type" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	文件路径：<input name='url' id="url" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	文件名称：<input name='name' id="name" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	关联ID：<input name='rid' id="rid" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	创建时间：<input name="createTimeBeg" id="createTimeBeg" class="easyui-datebox" style="width:100px;" data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
&gt;&gt;
	<input name="createTimeEnd" id="createTimeEnd" class="easyui-datebox" style="width:100px;" data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}}],
validType:['compareDate[createTimeBeg]']"/>

	<a href="javascript:void(0)" onclick="doAttacSearch()" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>
	</form>
</div>


<%-- 修改or添加面板 --%>
<iframe id="attacEditWin" src="/${projectName}/mail/attac/page.action?page=attac_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#attacEditButton'" title="编辑附件表（邮件用）" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="attacEditButton">
	<a href="javascript:void(0)" onclick="$('#attacEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#attacEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >取 消</a>
</div>
<%-- 查看面板 --%>
<iframe id="attacViewWin" src="/${projectName}/mail/attac/page.action?page=attac_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#attacViewButton'" title="查看附件表（邮件用）" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="attacViewButton">
	<a href="javascript:$('#attacViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >关 闭</a>
</div>
</body>
</html>
