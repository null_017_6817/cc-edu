<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title></title>
	<%@include file="/common/include.jsp" %>
	<script>
	function addTab(title, url){
		if ($('#tabdiv').tabs('exists', title)){
			$('#tabdiv').tabs('select', title);
		} else {
			var content = '<iframe scrolling="no" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
			$('#tabdiv').tabs('add',{
				title:title,
				content:content,
				closable:true
			});
		}
	}
	</script>
	<style>
	.panel-body{overflow:hidden}
	</style>
</head>
<body class="easyui-layout">
	<div data-options="region:'west',split:true" style="width:200px;padding:0px;">
		<div class="easyui-accordion" data-options="fit:true,border:false">
			<div title="Title1"  data-options="selected:true"  style="padding:10px;">
				<a href="javascript:addTab('测试邮件接收表','/${projectName}/mail/mailReceive/page.action?page=mailReceive_sch')">测试邮件接收表</a><br/>

				<a href="javascript:addTab('测试邮件接收表','/${projectName}/mail/mailSend/page.action?page=mailSend_sch')">测试邮件接收表</a><br/>

<a href="javascript:addTab('测试附件表（邮件用）','/${projectName}/mail/attac/page.action?page=attac_sch')">测试附件表（邮件用）</a><br/>

<a href="javascript:addTab('测试短信实训表','/${projectName}/mail/message/page.action?page=message_sch')">测试短信实训表</a><br/>

<a href="javascript:addTab('测试个性签名表','/${projectName}/mail/autograph/page.action?page=autograph_sch')">测试个性签名表</a><br/>

<!--insert-->
			</div>
		</div>
	</div>
	<div data-options="region:'center'">
		<div id="tabdiv" class="easyui-tabs" data-options="fit:true,border:false,plain:true" fit="true">
		</div>
	</div>
</body>
</html>
