<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.qtrmoon.com/tags-dictionary" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>编辑邮件接收表</title><!--mailSend-->
	<%@include file="/common/include.jsp" %>
<script>
<%-- 查询页点击修改时调用此方法加载记录详情 --%>
function load(id){
	$('#_form').form('load','/${projectName}/mail/mailSend/vieMailSend.action?id='+id);
};

<%-- 点击保存按钮时调用此方法。(注:保存按钮在查询列表页) --%>
function submitForm(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#_form').form('submit',{
      success:function(data) {
      	parent.reloadData();
      }
   });
}
</script>
</head>
<body>
<form id="_form" method="post" action="/${projectName}/mail/mailSend/updMailSend.action">
	<input type="hidden" name='id' id='id'/>
	<table>
		<tr><td class='label' style="width:120px">发送人学号：</td><td><input id="sendNum" name="sendNum" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >接收人学号：</td><td><input id="reveiveNum" name="reveiveNum" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >删除标识（1-已删除，0-未删除）：</td><td><input id="isDelete" name="isDelete" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >发送状态（1：已发送，0：未发送-草稿 ）：</td><td><input id="isSend" name="isSend" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >邮件标题：</td><td><input id="title" name="title" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >邮件内容：</td><td><input id="content" name="content" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >邮件发送时间：</td><td><input id="sendTimestr" name="sendTimestr" class="easyui-datebox" data-options="required:true"/></td></tr>
		<tr><td class='label' >创建时间：</td><td><input id="createTimestr" name="createTimestr" class="easyui-datebox" data-options="required:true"/></td></tr>
		<tr><td class='label' >更新时间（置顶用）：</td><td><input id="updateTimestr" name="updateTimestr" class="easyui-datebox" data-options="required:true"/></td></tr>
	</table>

</form>
</body>
</html>
