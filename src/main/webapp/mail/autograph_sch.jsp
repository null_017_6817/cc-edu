<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.qtrmoon.com/tags-dictionary" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>个性签名表查询</title><!--autograph-->
	<%@include file="/common/include.jsp" %>

<script>
var winToggle;
$(function(){
	winToggle=new QWinToggle().regist(["autographEditWin","autographViewWin"]);
	var pager = $('#autographGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});			
})

<%-- 执行查询，点击查询按钮时调用 --%>
function doAutographSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#autographGrid').datagrid('load',{
		number:$('#number').val(),
		content:$('#content').val()
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatAutographOper(val,row,index){  
    return "&emsp;<a href='javascript:void(0)' onclick='updAutograph("+val+")' class='_oper' title='修改'>&nbsp;<i class='icon-edit'></i>&nbsp;</a>"
    			+"<a href='javascript:void(0)' onclick='vieAutograph("+val+")' class='_oper' title='查看'>&nbsp;<i class='icon-info-sign'></i>&nbsp;</a>&emsp;"; 
} 

<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
function updAutograph(id){
	winToggle.open("autographEditWin");
	$('#autographEditWin').get(0).contentWindow.load(id);
} 

<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
function vieAutograph(id){
	winToggle.open("autographViewWin");
	$('#autographViewWin').get(0).contentWindow.load(id);
}

<%-- 删除记录 --%>
function delAutograph(){
	var checkedItems = $('#autographGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/mail/autograph/delAutograph.action",{ids:ids},function(data){
				$('#autographGrid').datagrid('reload');
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#autographEditWin').dialog('close');
	$('#autographGrid').datagrid('reload');
}
</script>
</head>
<body>
<table id="autographGrid" title="" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#autographGridToolbar',collapsible:true,url:'/${projectName}/mail/autograph/schAutograph.action',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" ></th>
			<th data-options="field:'number',width:80,sortable:true">学号</th>
			<th data-options="field:'content',sortable:true"></th>

			<th data-options="field:'_oper',align:'center',formatter:formatAutographOper">操作</th>
		</tr>
	</thead>
</table>
<%-- 查询条件设定栏 --%>
<div id="autographGridToolbar">
	<div class="opline">
		<a href="javascript:updAutograph(0)" class="btn btn-success"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">添加</span></a>&emsp;
		<a href="javascript:delAutograph()" class="btn btn-danger"><span class="icon-trash" style="font-size:16px"></span> <span style="font-size:14px">删除</span></a>
	</div>
	<form id="_form" class="shline">
	学号：<input name='number' id="number" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	：<input name='content' id="content" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>

	<a href="javascript:void(0)" onclick="doAutographSearch()" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>
	</form>
</div>


<%-- 修改or添加面板 --%>
<iframe id="autographEditWin" src="/${projectName}/mail/autograph/page.action?page=autograph_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#autographEditButton'" title="编辑个性签名表" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="autographEditButton">
	<a href="javascript:void(0)" onclick="$('#autographEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#autographEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >取 消</a>
</div>
<%-- 查看面板 --%>
<iframe id="autographViewWin" src="/${projectName}/mail/autograph/page.action?page=autograph_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#autographViewButton'" title="查看个性签名表" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="autographViewButton">
	<a href="javascript:$('#autographViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >关 闭</a>
</div>
</body>
</html>
