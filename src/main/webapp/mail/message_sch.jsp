<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.qtrmoon.com/tags-dictionary" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>短信实训表查询</title><!--message-->
	<%@include file="/common/include.jsp" %>

<script>
var winToggle;
$(function(){
	winToggle=new QWinToggle().regist(["messageEditWin","messageViewWin"]);
	var pager = $('#messageGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});
	// 添加联动效果
	$("#cnumber").combobox({
		onChange: function (n,o) {
			
			loadStudBySend(n,o);
		}
	});
})


function loadStudBySend(n,o){
	$('#reveiveNum').combobox("loadData", []);
	$('#sendNum').combobox("loadData", []);
	$.ajax({
		url:"/${projectName}/manager/stud/getStudDictByCnumber.action" ,
		type : "post" , 
		data : {"cnumber":n} ,
		success : function(result){
			if(result.success){
				var datas = result.data;
				var array = [];
				array.push({"id":"","text":""});
				for(var i = 0; i < datas.length; i++){
					var dd = datas[i];
					array.push({'id':dd.number,'text':dd.name});  
				}
				$('#sendNum').combobox("loadData", array);
				$('#sendNum').combobox('setValue', "");
				
				$('#receiveNum').combobox("loadData", array);
				$('#receiveNum').combobox('setValue', "");
			}else{
				
				
			}
		}
	}); 
}


<%-- 执行查询，点击查询按钮时调用 --%>
function doMessageSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#messageGrid').datagrid('load',{
		id:$('#id').val(),
		sendNum:sendNumDict.getValue(),
		receiveNum:receiveNumDict.getValue(),
		createTimeBeg: $('#createTimeBeg').datebox("getValue"),
		createTimeEnd: $('#createTimeEnd').datebox("getValue"),
		content:$('#content').val(),
		isRead:isReadDict.getValue(),
		cnumber:cnumberDict.getValue()
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatMessageOper(val,row,index){  
	/* "&emsp;<a href='javascript:void(0)' onclick=\"updMessage('"+val+"')\" class='_oper' title='修改'>&nbsp;<i class='icon-edit'></i>&nbsp;</a>" */
	return "&emsp;<a href='javascript:void(0)' onclick=\"vieMessage('"+val+"')\" class='_oper' title='查看'>&nbsp;<i class='icon-info-sign'></i>&nbsp;</a>&emsp;"; 
} 

<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
function updMessage(id){
	winToggle.open("messageEditWin");
	$('#messageEditWin').get(0).contentWindow.load(id);
} 

<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
function vieMessage(id){
	winToggle.open("messageViewWin");
	$('#messageViewWin').get(0).contentWindow.load(id);
}

<%-- 删除记录 --%>
function delMessage(){
	var checkedItems = $('#messageGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/mail/message/delMessage.action",{ids:ids},function(data){
				$('#messageGrid').datagrid('reload');
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#messageEditWin').dialog('close');
	$('#messageGrid').datagrid('reload');
}
</script>
</head>
<body>
<table id="messageGrid" title="" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#messageGridToolbar',collapsible:true,url:'/${projectName}/mail/message/schMessage.action',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" ></th>
			<th data-options="field:'sendName',width:80,sortable:true">发送人</th>
			<th data-options="field:'receiveName',sortable:true">接收人</th>
			<th data-options="field:'createTime',sortable:true">创建时间</th>
			<!-- <th data-options="field:'content',sortable:true">信息内容</th> -->
			<th data-options="field:'isReadShow',sortable:true">已读标识</th>
			<th data-options="field:'_oper',align:'center',formatter:formatMessageOper">操作</th>
		</tr>
	</thead>
</table>
<%-- 查询条件设定栏 --%>
<div id="messageGridToolbar">
	<div class="opline">
		<!-- <a href="javascript:updMessage(0)" class="btn btn-success"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">添加</span></a>&emsp; -->
		
	</div>
	<form id="_form" class="shline">
	班级：<dc:insertList name="cnumber" dictId="CC_CLASSES" style="combo_normal" />
	发送人：<dc:insertList name="sendNum" dictId="ZD_STUD" style="combo_smart"/>
	接收人：<dc:insertList name="receiveNum" dictId="ZD_STUD" style="combo_smart" />
	创建时间：<input name="createTimeBeg" id="createTimeBeg" class="easyui-datebox" style="width:150px;" data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
&gt;&gt;
	<input name="createTimeEnd" id="createTimeEnd" class="easyui-datebox" style="width:150px;" data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}}],
validType:['compareDate[createTimeBeg]']"/>
	已读标识：<dc:insertList name="isRead" dictId="IS_READ_SHOW" style="combo_normal" />
		<!-- <input name='isRead' id="isRead" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/> -->

	<a href="javascript:void(0)" onclick="doMessageSearch()" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>&emsp;
	<a href="javascript:delMessage()" class="btn btn-danger"><span class="icon-trash" style="font-size:16px"></span> <span style="font-size:14px">删除</span></a>
	</form>
</div>


<%-- 修改or添加面板 --%>
<iframe id="messageEditWin" src="/${projectName}/mail/message/page.action?page=message_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#messageEditButton'" title="编辑短信实训表" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="messageEditButton">
	<a href="javascript:void(0)" onclick="$('#messageEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#messageEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >取 消</a>
</div>
<%-- 查看面板 --%>
<iframe id="messageViewWin" src="/${projectName}/mail/message/page.action?page=message_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#messageViewButton'" title="查看短信实训表" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="messageViewButton">
	<a href="javascript:$('#messageViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >关 闭</a>
</div>
</body>
</html>
