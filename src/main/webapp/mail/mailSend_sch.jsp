<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.qtrmoon.com/tags-dictionary" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>邮件接收表查询</title><!--mailSend-->
<script>
var winTogglee;
$(function(){
	winTogglee=new QWinToggle().regist(["mailSendEditWin","mailSendViewWin"]);
	var pager = $('#mailSendGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});
	
	// 添加联动效果
	$("#cnumber").combobox({
		onChange: function (n,o) {
			
			loadStudBySend(n,o);
		}
	});
})

function loadStudBySend(n,o){
	$('#reveiveNum').combobox("loadData", []);
	$('#sendNum').combobox("loadData", []);
	$.ajax({
		url:"/${projectName}/manager/stud/getStudDictByCnumber.action" ,
		type : "post" , 
		data : {"cnumber":n} ,
		success : function(result){
			if(result.success){
				var datas = result.data;
				var array = [];
				array.push({"id":"","text":""});
				for(var i = 0; i < datas.length; i++){
					var dd = datas[i];
					array.push({'id':dd.number,'text':dd.name});  
				}
				$('#sendNum').combobox("loadData", array);
				$('#sendNum').combobox('setValue', "");
				
				$('#reveiveNum').combobox("loadData", array);
				$('#reveiveNum').combobox('setValue', "");
			}else{
				
				
			}
		}
	}); 
}

<%-- 执行查询，点击查询按钮时调用 --%>
function doMailSendSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#mailSendGrid').datagrid('load',{
		/* id:$('#id').val(), */
		cnumber:cnumberDict.getValue(),
		sendNum:sendNumDict.getValue(),
		/* reveiveNum:reveiveNumDict.getValue(), */
		/* isDelete:$('#isDelete').val(),
		isSend:$('#isSend').val(),
		title:$('#title').val(),
		content:$('#content').val(),
		sendTimeBeg: $('#sendTimeBeg').val(),
		sendTimeEnd: $('#sendTimeEnd').val(),
		createTimeBeg: $('#createTimeBeg').val(),
		createTimeEnd: $('#createTimeEnd').val(),
		updateTimeBeg: $('#updateTimeBeg').val(),
		updateTimeEnd: $('#updateTimeEnd').val() */
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatMailSendOper(val,row,index){  
	/* "<a href='javascript:void(0)' onclick='updMailSend("+val+")' class='_oper' title='修改'>&nbsp;<i class='icon-edit'></i>&nbsp;</a>" */
	return "&emsp;<a href='javascript:void(0)' onclick=\"vieMailSend('"+val+"')\" class='_oper' title='查看'>&nbsp;<i class='icon-info-sign'></i>&nbsp;</a>&emsp;"; 
} 

<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
function updMailSend(id){
	winTogglee.open("mailSendEditWin");
	$('#mailSendEditWin').get(0).contentWindow.load(id);
} 

<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
function vieMailSend(id){
	winTogglee.open("mailSendViewWin");
	$('#mailSendViewWin').get(0).contentWindow.load(id);
}

<%-- 删除记录 --%>
function delMailSend(){
	var checkedItems = $('#mailSendGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/mail/mailSend/delMailSend.action",{ids:ids},function(data){
				$('#mailSendGrid').datagrid('reload');
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#mailSendEditWin').dialog('close');
	$('#mailSendGrid').datagrid('reload');
}
</script>
</head>
<body>
<table id="mailSendGrid" title="" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#mailSendGridToolbar',collapsible:true,url:'/${projectName}/mail/mailSend/schMailSend.action',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" ></th>
			<th data-options="field:'sendNum',width:80,sortable:true">发送人学号/工号</th>
			<th data-options="field:'receiveNumShow',sortable:true">接收人学号/工号</th>
			<th data-options="field:'isDelete',sortable:true">回收站</th>
			<th data-options="field:'isSend',sortable:true">发送状态</th>
			<th data-options="field:'title',sortable:true">邮件标题</th>
			<!-- <th data-options="field:'content',sortable:true">邮件内容</th> -->
			<th data-options="field:'sendTime',sortable:true">邮件发送时间</th>
			<th data-options="field:'createTime',sortable:true">创建时间</th>
			<!-- <th data-options="field:'updateTime',sortable:true">更新时间</th> -->

			<th data-options="field:'_oper',align:'center',formatter:formatMailSendOper">操作</th>
		</tr>
	</thead>
</table>
<%-- 查询条件设定栏 --%>
<div id="mailSendGridToolbar">
	<div class="opline">
		
	</div>
	<form id="_form" class="shline">
	班级：<dc:insertList name="cnumber" dictId="CC_CLASSES" style="combo_normal" />
	发送人学号：<dc:insertList name="sendNum" dictId="GERENWANCHENGQINGKUANG_XM" style="combo_normal" />
	<!-- <input name='sendNum' id="sendNum" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/> -->
	<%--  接收人学号：<dc:insertList name="reveiveNum" dictId="GERENWANCHENGQINGKUANG_XM" style="combo_normal" /> --%>
	<!-- <input name='reveiveNum' id="reveiveNum" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/> -->
	<!-- 删除标识（1-已删除，0-未删除）：<input name='isDelete' id="isDelete" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	发送状态（1：已发送，0：未发送-草稿 ）：<input name='isSend' id="isSend" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	邮件标题：<input name='title' id="title" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	邮件内容：<input name='content' id="content" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	邮件发送时间：<input name="sendTimeBeg" id="sendTimeBeg" class="easyui-datebox" style="width:100px;" data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
&gt;&gt;
	<input name="sendTimeEnd" id="sendTimeEnd" class="easyui-datebox" style="width:100px;" data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}}],
validType:['compareDate[sendTimeBeg]']"/>
	创建时间：<input name="createTimeBeg" id="createTimeBeg" class="easyui-datebox" style="width:100px;" data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
&gt;&gt;
	<input name="createTimeEnd" id="createTimeEnd" class="easyui-datebox" style="width:100px;" data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}}],
validType:['compareDate[createTimeBeg]']"/>
	更新时间（置顶用）：<input name="updateTimeBeg" id="updateTimeBeg" class="easyui-datebox" style="width:100px;" data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
&gt;&gt;
	<input name="updateTimeEnd" id="updateTimeEnd" class="easyui-datebox" style="width:100px;" data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}}],
validType:['compareDate[updateTimeBeg]']"/> -->

	<a href="javascript:void(0)" onclick="doMailSendSearch()" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>&emsp;
	<a href="javascript:delMailSend()" class="btn btn-danger"><span class="icon-trash" style="font-size:16px"></span> <span style="font-size:14px">删除</span></a>
	</form>
</div>


<%-- 修改or添加面板 --%>
<iframe id="mailSendEditWin" src="/${projectName}/mail/mailSend/page.action?page=mailSend_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#mailSendEditButton'" title="编辑邮件接收表" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="mailSendEditButton">
	<a href="javascript:void(0)" onclick="$('#mailSendEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#mailSendEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >取 消</a>
</div>
<%-- 查看面板 --%>
<iframe id="mailSendViewWin" src="/${projectName}/mail/mailSend/page.action?page=mailSend_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#mailSendViewButton'" title="查看邮件接收表" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="mailSendViewButton">
	<a href="javascript:$('#mailSendViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >关 闭</a>
</div>
</body>
</html>
