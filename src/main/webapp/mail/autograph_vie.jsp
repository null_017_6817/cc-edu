<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.qtrmoon.com/tags-dictionary" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>查看个性签名表</title><!--autograph-->
	<%@include file="/common/include.jsp" %>
<script>
function load(id){
	$('#_form').form('load','/${projectName}/mail/autograph/vieAutograph.action?toL=true&id='+id);
};
</script>
</head>
<body>
<form id="_form">
	<input type="hidden" name='id' value=""/>
	<table>
		<tr><td class='label'>学号：</td><td><input type='text' name='number' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>：</td><td><input type='text' name='content' class='viewbox' readonly='readonly'/></td></tr>
	</table>

</form>
</body>
</html>
