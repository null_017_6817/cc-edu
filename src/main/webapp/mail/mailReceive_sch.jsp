<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.qtrmoon.com/tags-dictionary" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>邮件接收表查询</title><!--mailReceive-->

<script>
var winToggle;
$(function(){
	winToggle=new QWinToggle().regist(["mailReceiveEditWin","mailReceiveViewWin"]);
	var pager = $('#mailReceiveGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});			
	
	// 添加联动效果
	$("#cnumbers").combobox({
		onChange: function (n,o) {
			loadStudByReceive(n,o);
		}
	});
})

function loadStudByReceive(n,o){
	$('#reveiveNums').combobox("loadData", []);
	$('#sendNums').combobox("loadData", []);
	$.ajax({
		url:"/${projectName}/manager/stud/getStudDictByCnumber.action" ,
		type : "post" , 
		data : {"cnumber":n} ,
		success : function(result){
			if(result.success){
				var datas = result.data;
				var array = [];
				array.push({"id":"","text":""});
				for(var i = 0; i < datas.length; i++){
					var dd = datas[i];
					array.push({'id':dd.number,'text':dd.name});  
				}
				$('#sendNums').combobox("loadData", array);
				$('#sendNums').combobox('setValue', "");
				
				$('#reveiveNums').combobox("loadData", array);
				$('#reveiveNums').combobox('setValue', "");
			}else{
				
				
			}
		}
	}); 
	
}


<%-- 执行查询，点击查询按钮时调用 --%>
function doMailReceiveSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#mailReceiveGrid').datagrid('load',{
		/* id:$('#id').val(), */
		cnumber:cnumbersDict.getValue(),
		sendNum:sendNumsDict.getValue(),
		/* reveiveNum:reveiveNumsDict.getValue(), */
		/* isRead:$('#isRead').val(), */
		/* isDelete:$('#isDelete').val(), */
		/* title:$('#title').val(), */
		/* content:$('#content').val(), */
		/* receiveTimeBeg: $('#receiveTimeBeg').val(),
		receiveTimeEnd: $('#receiveTimeEnd').val(),
		createTimeBeg: $('#createTimeBeg').val(),
		createTimeEnd: $('#createTimeEnd').val(),
		updateTimeBeg: $('#updateTimeBeg').val(),
		updateTimeEnd: $('#updateTimeEnd').val() */
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatMailReceiveOper(val,row,index){  
	/* "<a href='javascript:void(0)' onclick='updMailReceive("+val+")' class='_oper' title='修改'>&nbsp;<i class='icon-edit'></i>&nbsp;</a>" */
	return	"&emsp;<a href='javascript:void(0)' onclick=\"vieMailReceive('"+val+"')\" class='_oper' title='查看'>&nbsp;<i class='icon-info-sign'></i>&nbsp;</a>&emsp;"; 
} 

<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
function updMailReceive(id){
	winToggle.open("mailReceiveEditWin");
	$('#mailReceiveEditWin').get(0).contentWindow.load(id);
} 

<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
function vieMailReceive(id){
	winToggle.open("mailReceiveViewWin");
	$('#mailReceiveViewWin').get(0).contentWindow.load(id);
}

<%-- 删除记录 --%>
function delMailReceive(){
	var checkedItems = $('#mailReceiveGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/mail/mailReceive/delMailReceive.action",{ids:ids},function(data){
				$('#mailReceiveGrid').datagrid('reload');
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#mailReceiveEditWin').dialog('close');
	$('#mailReceiveGrid').datagrid('reload');
}
</script>
</head>
<body>
<table id="mailReceiveGrid" title="" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#mailReceiveGridToolbar',collapsible:true,url:'/${projectName}/mail/mailReceive/schMailReceive.action',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" ></th>
			<th data-options="field:'sendNum',width:80,sortable:true">发送人学号</th>
			<th data-options="field:'receiveNumShow',sortable:true">接收人学号</th>
			<th data-options="field:'isRead',sortable:true">已读</th>
			<th data-options="field:'isDelete',sortable:true">回收站</th>
			<th data-options="field:'title',sortable:true">邮件标题</th>
			<!-- <th data-options="field:'content',sortable:true">邮件内容</th> -->
			<th data-options="field:'receiveTime',sortable:true">接收时间</th>
			<th data-options="field:'createTime',sortable:true">创建时间</th>
			<th data-options="field:'updateTime',sortable:true">更新时间（置顶用）</th>

			<th data-options="field:'_oper',align:'center',formatter:formatMailReceiveOper">操作</th>
		</tr>
	</thead>
</table>
<%-- 查询条件设定栏 --%>
<div id="mailReceiveGridToolbar">
	<div class="opline">
	</div>
	<form id="_form" class="shline">
	班级：<dc:insertList name="cnumbers" dictId="CC_CLASSES" style="combo_normal" />
	发送人学号：<dc:insertList name="sendNums" dictId="GERENWANCHENGQINGKUANG_XM" style="combo_normal" />
	<!-- <input name='sendNum' id="sendNum" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/> -->
	<%-- 接收人学号：<dc:insertList name="reveiveNums" dictId="GERENWANCHENGQINGKUANG_XM" style="combo_normal" /> --%>
	<!-- <input name='reveiveNum' id="reveiveNum" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/> -->
	<!-- 已读标识(1-已读，0-未读)：<input name='isRead' id="isRead" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/> -->
	<!-- 删除标识（1-已删除，0-未删除）：<input name='isDelete' id="isDelete" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	邮件标题：<input name='title' id="title" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	邮件内容：<input name='content' id="content" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	接收时间：<input name="receiveTimeBeg" id="receiveTimeBeg" class="easyui-datebox" style="width:100px;" data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
&gt;&gt;
	<input name="receiveTimeEnd" id="receiveTimeEnd" class="easyui-datebox" style="width:100px;" data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}}],
validType:['compareDate[receiveTimeBeg]']"/>
	创建时间：<input name="createTimeBeg" id="createTimeBeg" class="easyui-datebox" style="width:100px;" data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
&gt;&gt;
	<input name="createTimeEnd" id="createTimeEnd" class="easyui-datebox" style="width:100px;" data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}}],
validType:['compareDate[createTimeBeg]']"/>
	更新时间（置顶用）：<input name="updateTimeBeg" id="updateTimeBeg" class="easyui-datebox" style="width:100px;" data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
&gt;&gt;
	<input name="updateTimeEnd" id="updateTimeEnd" class="easyui-datebox" style="width:100px;" data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}}],
validType:['compareDate[updateTimeBeg]']"/> -->

	<a href="javascript:void(0)" onclick="doMailReceiveSearch()" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>&emsp;
			<a href="javascript:delMailReceive()" class="btn btn-danger"><span class="icon-trash" style="font-size:16px"></span> <span style="font-size:14px">删除</span></a>
	</form>
</div>


<%-- 修改or添加面板 --%>
<iframe id="mailReceiveEditWin" src="/${projectName}/mail/mailReceive/page.action?page=mailReceive_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#mailReceiveEditButton'" title="编辑邮件接收表" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="mailReceiveEditButton">
	<a href="javascript:void(0)" onclick="$('#mailReceiveEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#mailReceiveEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >取 消</a>
</div>
<%-- 查看面板 --%>
<iframe id="mailReceiveViewWin" src="/${projectName}/mail/mailReceive/page.action?page=mailReceive_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#mailReceiveViewButton'" title="查看邮件接收表" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="mailReceiveViewButton">
	<a href="javascript:$('#mailReceiveViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >关 闭</a>
</div>
</body>
</html>
