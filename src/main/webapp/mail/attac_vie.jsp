<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.qtrmoon.com/tags-dictionary" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>查看附件表（邮件用）</title><!--attac-->
	<%@include file="/common/include.jsp" %>
<script>
function load(id){
	$('#_form').form('load','/${projectName}/mail/attac/vieAttac.action?toL=true&id='+id);
};
</script>
</head>
<body>
<form id="_form">
	<input type="hidden" name='id' value=""/>
	<table>
		<tr><td class='label'>文件类型：</td><td><input type='text' name='type' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>文件路径：</td><td><input type='text' name='url' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>文件名称：</td><td><input type='text' name='name' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>关联ID：</td><td><input type='text' name='rid' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>创建时间：</td><td><input type='text' name='createTime' class='viewbox' readonly='readonly'/></td></tr>
	</table>

</form>
</body>
</html>
