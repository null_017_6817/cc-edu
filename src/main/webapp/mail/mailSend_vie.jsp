<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.qtrmoon.com/tags-dictionary" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>查看邮件接收表</title><!--mailSend-->
	<%@include file="/common/include.jsp" %>
<script>
function load(id){
	$('#_form').form('load','/${projectName}/mail/mailSend/vieMailSend.action?toL=true&id='+id);
	// 初始化回显数据
	$('#_form').form({
		onLoadSuccess : function(data) {
			showContent(data.content);
			
		}
	});
};

function showContent(con){
	$("#_form ").html(con);
}
</script>
</head>
<body>
<form id="_form">
	<input type="hidden" name='id' value=""/>
	<table>
		<tr><td class='label'>发送人学号：</td><td><input type='text' name='sendNum' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>接收人学号：</td><td><input type='text' name='reveiveNum' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>删除标识（1-已删除，0-未删除）：</td><td><input type='text' name='isDelete' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>发送状态（1：已发送，0：未发送-草稿 ）：</td><td><input type='text' name='isSend' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>邮件标题：</td><td><input type='text' name='title' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>邮件内容：</td><td><input type='text' name='content' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>邮件发送时间：</td><td><input type='text' name='sendTime' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>创建时间：</td><td><input type='text' name='createTime' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>更新时间（置顶用）：</td><td><input type='text' name='updateTime' class='viewbox' readonly='readonly'/></td></tr>
	</table>

</form>
</body>
</html>
