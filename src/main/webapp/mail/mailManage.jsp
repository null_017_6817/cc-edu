<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.qtrmoon.com/tags-dictionary" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>邮件管理</title><!--mailSend-->
	<%@include file="/common/include.jsp" %>

<script>

</script>
</head>
<!-- easyui layout布局开始 -->
<body class="easyui-layout" fit="true">
	
	<!-- 左边（邮件发送列表） -->
	<div region="west" split="true" style="width:50%;" title="发送邮件管理">
		<jsp:include page="/mail/mailSend_sch.jsp"></jsp:include>
	</div>
	
	<!-- 右边（邮件接收列表） -->
	<div region="east" split="true" style="width:50%;" title="接收邮件管理">
		<jsp:include page="/mail/mailReceive_sch.jsp"></jsp:include>
	</div>

</body>

</html>
