<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.qtrmoon.com/tags-dictionary" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>查看短信实训表</title><!--message-->
	<%@include file="/common/include.jsp" %>
<script>
function load(id){
	
	$('#_form').form('load','/${projectName}/mail/message/vieMessage.action?toL=true&id='+id);
	
	$('#_form').form({
		onLoadSuccess : function(data) {
			
			$("td[name='msgContent']").html(data.content)
		}
	});
	
};
</script>
</head>
<body>
<form id="_form">
	<input type="hidden" name='id' value=""/>
	<table style="width:470px;" >
		<tr><td class='label'>发送人：</td><td><input type='text' name='sendName' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>接收人：</td><td><input type='text' name='receiveName' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>创建时间：</td><td><input type='text' name='createTime' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>信息内容：</td><td name="msgContent" style="width:80%">
											<textarea id="content" name="content" rows="1" cols="50" readonly='readonly' class="textarea easyui-validatebox viewbox"></textarea>
									</td></tr>
		<tr><td class='label'>已读标识：</td><td><input type='text' name='isReadShow' class='viewbox' readonly='readonly'/></td></tr>
	</table>

</form>
</body>
</html>
