<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.qtrmoon.com/tags-dictionary" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>编辑短信实训表</title><!--message-->
	<%@include file="/common/include.jsp" %>
<script>
<%-- 查询页点击修改时调用此方法加载记录详情 --%>
function load(id){
	$('#_form').form('load','/${projectName}/mail/message/vieMessage.action?id='+id);
};

<%-- 点击保存按钮时调用此方法。(注:保存按钮在查询列表页) --%>
function submitForm(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#_form').form('submit',{
      success:function(data) {
      	parent.reloadData();
      }
   });
}
</script>
</head>
<body>
<form id="_form" method="post" action="/${projectName}/mail/message/updMessage.action">
	<input type="hidden" name='id' id='id'/>
	<table>
		<tr><td class='label' style="width:120px">发送人学号：</td><td><input id="sendNum" name="sendNum" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >接收人学号：</td><td><input id="receiveNum" name="receiveNum" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >创建时间：</td><td><input id="createTimestr" name="createTimestr" class="easyui-datebox" data-options="required:true"/></td></tr>
		<tr><td class='label' >信息内容：</td><td><input id="content" name="content" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >已读标识：</td><td><input id="isRead" name="isRead" class="easyui-textbox" data-options="required:true,readonly:true"/></td></tr>
	</table>

</form>
</body>
</html>
