<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>实训行业表查询</title><!--trainindusty-->
	<%@include file="/common/include.jsp" %>

<script>
$(function(){
	var pager = $('#trainindustyGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});			
})

<%-- 执行查询，点击查询按钮时调用 --%>
function doTrainindustySearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#trainindustyGrid').datagrid('load',{
		id:$('#id').val(),
		pid:$('#pid').val(),
		name:$('#name').val(),
		type:$('#type').val()
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatTrainindustyOper(val,row,index){  
    return "&emsp;<a href='javascript:void(0)' onclick=\"updTrainindusty('"+val+"')\" class='_oper' title='修改'>&nbsp;<i class='icon-edit'></i>&nbsp;</a>"
    			+"<a href='javascript:void(0)' onclick=\"vieTrainindusty('"+val+"')\" class='_oper' title='查看'>&nbsp;<i class='icon-info-sign'></i>&nbsp;</a>&emsp;"; 
} 

<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
function updTrainindusty(id){
	if(id == '0'){
		$("#trainindustyEditWin").dialog({title: '添加实训行业',resizable:true});
	}
	if(id != '0'){
		$("#trainindustyEditWin").dialog({title: '修改实训行业',resizable:true});
	}
	QMoveDialog($("#trainindustyEditWin"));
	$('#trainindustyViewWin').dialog('close');
	$('#trainindustyEditWin').dialog('open');
	$('#trainindustyEditWin').get(0).contentWindow.load(id);
} 

<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
function vieTrainindusty(id){
	QMoveDialog($("#trainindustyViewWin"));
	$('#trainindustyEditWin').dialog('close');
	$('#trainindustyViewWin').dialog('open');
	$('#trainindustyViewWin').get(0).contentWindow.load(id);
}

<%-- 删除记录 --%>
function delTrainindusty(){
	var checkedItems = $('#trainindustyGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/book/trainindusty/delTrainindusty.action",{ids:ids},function(data){
				$('#trainindustyGrid').datagrid('reload');
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#trainindustyEditWin').dialog('close');
	$('#trainindustyGrid').datagrid('reload');
}
</script>
</head>
<body>
<table id="trainindustyGrid" title="" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#trainindustyGridToolbar',collapsible:true,url:'/${projectName}/book/trainindusty/schTrainindusty.action',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" ></th>
			<!-- <th data-options="field:'id',width:80,sortable:true">ID</th> -->
			<!-- <th data-options="field:'pid',width:80,sortable:true">预留字段</th> -->
			<th data-options="field:'name',width:80,sortable:true">实训行业名称</th>
			<th data-options="field:'type',sortable:true">实训行业类型</th>
			<th data-options="field:'createTime',sortable:true">创建时间</th>
			<th data-options="field:'orderid',sortable:true">排序</th>
			
			<th data-options="field:'_oper',align:'center',formatter:formatTrainindustyOper">操作</th>
		</tr>
	</thead>
</table>
<%-- 查询条件设定栏 --%>
<div id="trainindustyGridToolbar">
	<div class="opline">
		
	</div>
	<form id="_form" class="shline">
<!-- 	ID：<input name='id' id="id" class='easyui-textbox' value='' data-options="iconWidth:16, -->
<!-- icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');} -->
<!-- }]"/> -->
	
	实训行业名称：<input name='name' id="name" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	<a href="javascript:void(0)" onclick="doTrainindustySearch()" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>&emsp;
	<a href="javascript:updTrainindusty(0)" class="btn btn-success"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">添加</span></a>&emsp;
		<a href="javascript:delTrainindusty()" class="btn btn-danger"><span class="icon-trash" style="font-size:16px"></span> <span style="font-size:14px">删除</span></a>
	</form>
</div>


<%-- 修改or添加面板 --%>
<iframe id="trainindustyEditWin" src="/${projectName}/book/trainindusty/page.action?page=trainindusty_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#trainindustyEditButton'" title="修改实训行业表" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="trainindustyEditButton">
	<a href="javascript:void(0)" onclick="$('#trainindustyEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#trainindustyEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >取 消</a>
</div>
<%-- 查看面板 --%>
<iframe id="trainindustyViewWin" src="/${projectName}/book/trainindusty/page.action?page=trainindusty_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#trainindustyViewButton'" title="查看实训行业表" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="trainindustyViewButton">
	<a href="javascript:$('#trainindustyViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >关 闭</a>
</div>
</body>
</html>
