<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>查看实训行业表</title><!--trainindusty-->
	<%@include file="/common/include.jsp" %>
<script>
function load(id){
	$('#_form').form('load','/${projectName}/book/trainindusty/vieTrainindusty.action?toL=true&id='+id);
};
</script>
</head>
<body>
<form id="_form">
	<input type="hidden" name='id' value=""/>
	<table>
		<tr style="display:none;"><td class='label'>预留字段：</td><td><input type='text' name='pid' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>实训行业名称：</td><td><input type='text' name='name' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>实训行业类型：</td><td><input type='text' name='type' class='viewbox' readonly='readonly'/></td></tr>
		<tr ><td class='label'>创建时间：</td><td><input type='text' name='createTime' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>序号：</td><td><input type='text' name='orderid' class='viewbox' readonly='readonly'/></td></tr>
	</table>

</form>
</body>
</html>
