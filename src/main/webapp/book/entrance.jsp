<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="ie6"><![endif]-->
<!--[if IE 7]>
<html class="ie7"><![endif]-->
<!--[if IE 8]>
<html class="ie8"><![endif]-->
<!--[if (gte IE 9)]><!-->
<html>
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<title>呼叫中心实训入口页面</title>
<script type="text/javascript"	src="/${projectName}/book/js/lib/jquery-1.9.1.min.js"></script>
<link rel="stylesheet" type="text/css"	href="/${projectName}/book/css/common.css" />
<link rel="stylesheet" type="text/css"	href="/${projectName}/book/css/fonts.css" />
<link rel="stylesheet" type="text/css"	href="/${projectName}/book/css/style.css" />

<script type="text/javascript" charset="utf-8"	src="/${projectName}/ueditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8"	src="/${projectName}/ueditor/ueditor.all.js"></script>
<script type="text/javascript" charset="utf-8"	src="/${projectName}/js/My97DatePicker/WdatePicker.js"></script>
<script src="/${projectName}/book/js/layui/js/lib/layui2.3/layui.all.js"></script>
<!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
<!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
<script type="text/javascript" charset="utf-8"	src="/${projectName}/ueditor/lang/zh-cn/zh-cn.js"></script>
<style>
.questionDiv {
	border: 1px solid #000000;
	margin-top: 5px;
}
</style>
<script>
var projectName = '${projectName}';
var bookId; // 
var type; // 
var indexParam;
var dataArry = new Array();
var stateParam;
var topicIdParam;

$(function(){
	var user = "${CURRENT_USER}";
	if(user == null || user == "" || typeof(user) == "undefined"){
		window.location.href= "/"+projectName+"/"
	}
	
	cusInit();
	serInit();
	var state = '${state}';
	if(state != null){
		stateParam = state;
		doChange(state);
	}
	// 初始化title
	initTitle();
});

// 初始化title
function initTitle(){
	$.ajax({
		type:'post',
		url:'/${projectName}/book/trainindusty/schAllTrainindusty.action',
		data:{},
		dataType:'json',
		success:function(data){
			var bars=$('#bars');
			bars.empty();
			bars.append("<li><a href='/${projectName}/'><i class='icon-home'></i>首页</a></li>");
			for(var i=0;i<data.rows.length;i++){
				bars.append("<li><a href='javascript:void(0);' onclick=\"clickNavigationBar('"+data.rows[i].id+"','"+data.rows[i].type+"',this)\" ><i class='"+data.rows[i].icon+"'></i>"+data.rows[i].name+"</a></li>");
			}
			bars.find("li").eq(1).find("a").trigger('click');
		}
	})
}

//登录成功后显示老师评价或者学生评价
function doChange(state){
	if(state == "3"){
		$("#saveBtn").show();
	}else{
		$("#saveBtn").hide();
	}
}

var currentTopic;
//显示主内容
function getContent(topicId,t) {
	$(t).parent().siblings("li").removeClass("active");
	$(t).parent().addClass("active");
	currentTopic = topicId;
	if (topicId != null && topicId != "") {
		$(".tab-content-active").css("display", "block");
		$("#noData").css("display", "none");
		topicIdParam = topicId;
		$.ajax({
			url : "/${projectName}/camp/hytopic/getTopicById.action" , 
			type : "post" , 
			data : {"id" : topicId} , 
			success : function(result) {
				if (result.success ) {
					var topic = result.data;
					$("input[name='hytopicId']").val(topic.id);
					$("#question").html(topic.content);
				}else{
					alert(result.msg);
				}
			}
		});
	}
}

var currentNavi = "";
var currentIndustryType = "";
//点击导航栏时调用
function clickNavigationBar(bid,ty ,t){
	$(t).parent().siblings("li").removeClass("active");
	$(t).parent().addClass("active");
	currentNavi = bid;
	currentIndustryType = ty;
	var link1=$('#link1');
	var link2=$('#link2');
	link1.empty();
	link2.empty();
	link1.attr("href","javascript:void(0);");
	link2.attr("href","javascript:void(0);");
	// 设置行业
	$("#orderForm").find("select[name='profession']").val(currentIndustryType);
	$("#orderForm").find("select[name='profession']").trigger("change");
	if(ty == 'industry_type_10001'){
		link1.attr("onclick","openWindow('/${projectName}/slwldx/hbxx/hbxx_sch.jsp');");
		link1.append("查看机票信息");
		link2.hide();
		$(".entrance-banner").css("display","none");
		$(".bannernone1").css("display","block");
		
	}
	if(ty == 'industry_type_10002'){
		link1.attr("onclick","openWindow('/${projectName}/slwldx/web/page.action?page=wlddxx_sch');");
		link1.append("物流订单信息查询");
		link2.attr("onclick","openWindow('/${projectName}/slwldx/web/page.action?page=wlzfxx_sch');");
		link2.append("物流资费信息查询");
		link2.show();
		$(".entrance-banner").css("display","none");
		$(".bannernone2").css("display","block");
	}
	if(ty == 'industry_type_10003'){
		link1.attr("onclick","openWindow('/${projectName}/slwldx/aadx/index.jsp');");
		link1.append("查看移动电信信息");
		link2.hide();
		$(".entrance-banner").css("display","none");
		$(".bannernone3").css("display","block");
	} 
	
	$.ajax({
		type:'post',
		url:'/${projectName}/camp/hytopic/getListByRid.action',
		data:{"rid":bid},
		dataType:'json',
		success:function(result){
			if(result.success){
				var topics = result.data
				//getContent(topics[0].id);
				var tab=$('.tabsList');
				tab.empty();
				for(var i=0;i<topics.length;i++){
					tab.append("<li><a data-index='"+i+"' onclick='getContent("+topics[i].id+",this)'>"+topics[i].title+"</a></li>");
				}
				tab.find("li").eq(0).find("a").trigger("click");
			}
		}
	})
}

// ***********socket start******************
var websocket = null;
	//判断当前浏览器是否支持WebSocket
	if ('WebSocket' in window) {
		var agentId = "${agentId}";
		if(agentId != null && agentId != "" && typeof(agentId) != "undefined"){
			websocket = new WebSocket("wss://"+document.location.host+"/${projectName}/websocket?agentId=${agentId}");
		}
	}
	else {
		alert('当前浏览器 Not support websocket')
	}
	
	if(websocket != null){
		//连接发生错误的回调方法
		websocket.onerror = function () {
			setMessageInnerHTML("WebSocket连接发生错误");
			var agentId = "${agentId}";
			if(agentId != null && agentId != "" && typeof(agentId) != "undefined"){
				websocket = new WebSocket("wss://"+document.location.host+"/${projectName}/websocket?agentId=${agentId}");
			}
		};
		
		//连接成功建立的回调方法
		websocket.onopen = function () {
			setMessageInnerHTML("WebSocket连接成功");
			heartCheck.start();
		}

		//接收到消息的回调方法
		websocket.onmessage = function (event) {
			setMessageInnerHTML(event.data);
			heartCheck.reset();
		}
		
		//连接关闭的回调方法
		websocket.onclose = function () {
			setMessageInnerHTML("WebSocket连接关闭");
		}
 
		//监听窗口关闭事件，当窗口关闭时，主动去关闭websocket连接，防止连接还没断开就关闭窗口，server端会抛异常。
		window.onbeforeunload = function () {
			closeWebSocket();
		}
 
		//将消息显示在网页上
		function setMessageInnerHTML(innerHTML) {
			var agentId = "${agentId}";
			if(agentId != "" && innerHTML == agentId){
				loadSerData(true);
			}
		}
 
		//关闭WebSocket连接
		function closeWebSocket() {
			websocket.close();
		}
 
		//发送消息
		function send() {
			/* var message = document.getElementById('text').value; */
			websocket.send(message);
		}
	}
		
	// 心跳
	var heartCheck = {
		timeout: 10000,//1分钟
		timeoutObj: null,
		reset: function(){
			clearTimeout(this.timeoutObj);
			this.start();	
		},
		start: function(){
			this.timeoutObj = setTimeout(function(){
				var agentId = "${agentId}";
				if(agentId != null && agentId != "" && typeof(agentId) != "undefined"){
					websocket.send("HeartBeat");
				}
			
			}, this.timeout)
		}
	}


//*********** socket end ******************

</script>
</head>

<body>
<input type="hidden" name="currUser" value="${agentId}" />
<input type="hidden" name="Ordertype" value="2" />
<div class="wrap">
	<div class="content-box">
		<jsp:include page="_header.jsp" />
		
		<div id="fade" class="black_overlay"></div>

		<div class="entranceNav">
			<ul id="bars" class="inner clearfix">
				
			<ul>
		</div>

		<div class="main bg">
			<div class="inner">
				<img class="entrance-banner bannernone1" src="/${projectName}/book/img/banner1.png" alt="" />
				<img class="entrance-banner bannernone2" src="/${projectName}/book/img/banner2.png" alt="" />
				<img class="entrance-banner bannernone3" src="/${projectName}/book/img/banner3.png" alt="" />
				<!-- 实训按钮 -->
				<div class="tanc">
					<a id="link1"></a>
					<a id="link2"></a>
				</div>
				<div class="bg-fff clearfix">
					<div class="entranceCon border-right pull-left">
						<input type="hidden" name="stepId" value="" /><!-- 废 -->
						<input type="hidden" name="hytopicId" value="" />
						<input type="hidden" name="answerId" value="" />
	
						<div class="topicBox">
							<h4>场景描述</h4>
							<div id="question"></div>
						</div>
						
						<!-- 如果是学生 -->
						<c:if test="${CURRENT_USER.state == '3'}">	
							<hr class="hr-dashed">
							<div class="callBox">
								<h4>通话实训</h4>
								<div class="operateBtn" align="center">
								坐席号码：&nbsp;<input name="agentId" onclick="chooseStuds()" readonly="readonly" AUTOCOMPLETE="off" style="width:100px;border-top:0px;border-left:0px;border-right:0px;border-bottom:1px solid #000"
								type="text" value="" placeholder="请输入坐席号码" />
									<button type="button" id="cusOrderId">客户信息</button>
									<button type="button" id="callId">拨打</button>
									<button type="button" id="serOrderId">弹出工单</button>
								</div>
							</div>
						</c:if>
					
					</div>
				
					<div class="tabsright pull-right">
						<h6 id="plist">实训列表&nbsp;</h6>
						<ul class="tabsList">
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<jsp:include page="_footer.jsp" />
<jsp:include page="./pop.jsp" />
<jsp:include page="./hyorder_template.jsp" />
<script type="text/javascript"	src="/${projectName}/book/js/common/login.js"></script>
<script type="text/javascript">

//banner - 书本名称
function openWindow(url){
	window.open(url);
}

// 弹出框变量
var layerIndex = "";
// 点选学生
function chooseStuds(){
	layerIndex = layer.open({
		type: 2,
		shadeClose: true, //点击遮罩关闭
		skin: 'layui-layer-lan', //样式类名
		closeBtn: 1, //不显示关闭按钮
		area: ['800px', '520px'],
		anim: 5,
		title: false,
		content: '/${projectName}/book/studList.jsp',
	}); 
}

// 关闭弹出窗口
function closeLay(){
	layer.close(layerIndex);
}

</script>
	
<iframe id="sipexe" style= "display:none "></iframe>
<script type="text/javascript" src="/${projectName}/book/js/lib/jquery.cookie.js"></script>
<div name="temp" style="display:none">
<!-- 单选题 -->
<jsp:include page="./dxwj-template/select-temp.jsp" />
<!-- 选择题 -->
<!-- 多选题 -->
<jsp:include page="./dxwj-template/selects-temp.jsp" />
<!-- 选择题 -->
<!-- 填空题 -->
<jsp:include page="./dxwj-template/answer-temp.jsp" />
<!-- 选择题 -->
<!-- 空题 -->
<jsp:include page="./dxwj-template/common-temp.jsp" />
<!-- 选择题 -->
</div>

</body>
</html>
