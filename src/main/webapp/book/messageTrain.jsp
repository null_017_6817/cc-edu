<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="ie6"><![endif]-->
<!--[if IE 7]>
<html class="ie7"><![endif]-->
<!--[if IE 8]>
<html class="ie8"><![endif]-->
<!--[if (gte IE 9)]><!-->
<html>
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<title>呼叫中心实训入口页面</title>

<link rel="stylesheet" type="text/css"	href="/${projectName}/book/css/common.css" />
<link rel="stylesheet" type="text/css"	href="/${projectName}/book/css/fonts.css" />
<link rel="stylesheet" type="text/css"	href="/${projectName}/book/mail-fonts/font.css" />
<link rel="stylesheet" type="text/css"	href="/${projectName}/book/css/style.css" />

<script type="text/javascript"	src="/${projectName}/book/js/lib/jquery-1.9.1.min.js"></script>
<script type="text/javascript" charset="utf-8"	src="/${projectName}/js/My97DatePicker/WdatePicker.js"></script>
</head>

<body>
<input type="hidden" name="currUser" value="${agentId}" />
<input type="hidden" name="Ordertype" value="2" />
<div class="wrap">
	<div class="content-box">
		<jsp:include page="_header.jsp" />
		<div id="fade" class="black_overlay"></div>
		<div class="main messagebg">
			<div class="inner message-model clearfix">
				<div class="messageLeft pull-left">
					<div class="messageLeft-top">
						<div class="item">
							<div class="my_protocol">
								<input name="chooseAll" type="checkbox" value="" />
								<i class="input-i"></i>
							</div>
							<label>全选</label>
						</div>
						<div name="classDiv" style="margin-left: 134px;padding: 1px 10px;display:none;">
							<select name="classSelect" onchange="chooseClass(this);" style="width:100px;">
							</select>
						</div> 
					</div>
					<ul class="studList" style="cursor:pointer;">
						
					</ul>
				</div>
				<div class="messageRight pull-right">
					<div class="messageRight-top">
						<span class="talkName"></span>
						<span class="quanfa" onclick="groupSendMsg(this);" title="群发">群发</span>
						<span class="preview" title="预览">预览</span>
					</div>
					<div class="message-infoCon">
					</div>
					<form class="message-infoSend">
						<textarea  maxlength="70" placeholder="最多可输入70字"></textarea>
						<input type="hidden" name="receiveNumHid" value="" />
						<button name="singleBtn"  title="发送" onclick="sendMsg(this);" type="button" class="message-fasong"></button>
						<!-- <button name="groupBtn"  onclick="groupSendMsg(this);" title="群发消息" type="button" class="icon-fs" ></button> -->
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- 手机预览 -->
<div class="phone-preview-bg"></div>
<div class="phone-preview">
	<span class="back"></span>
	<div class="phone-preview-title"></div>
	<div class="phone-preview-con">
	</div>
</div>



<jsp:include page="_footer.jsp" />
<jsp:include page="./pop.jsp" />
<script type="text/javascript"	src="/${projectName}/book/js/common/login.js"></script>
</div>
<div id="loadDiv" style="display:none" >
	<span><img src="/${projectName}/book/img/loading.gif" />正在加载学生列表...</span>
</div>
</body>
<script>
var projectName = "${projectName}"

$(function(){
	var user = "${CURRENT_USER}";
	if(user == null || user == "" || typeof(user) == "undefined"){
		window.location.href= "/"+projectName+"/"
	}
	var state = "${CURRENT_USER.state}";
	
	if(state == null || typeof(state) == "undefined" ){
		alert("登录超时，请重新登录")
		window.location.href= "/"+projectName+"/";
	}
	if(state == "2" || state == "x" ){
		// 初始化班级
		initClass();
		$("div[name='classDiv']").show();
	}
	if(state == "3"){
		var cnumber = "${CURRENT_USER.cnumber}";
		getStuList(cnumber);// 获取学生列表
		$("div[name='classDiv']").hide();
	}
	
	// 全选效果
	$("input[name='chooseAll']").click(function(i,item){
		if($(this).is(':checked')){
			console.log("checked");
			$(".studList").find("li").each(function(n,box){
				$(box).find("input[type='checkbox']").prop("checked",true);
			});
		}
		if(!$(this).is(':checked')){
			console.log("no checked");
			$(".studList").find("li").each(function(n,box){
				$(box).find("input[type='checkbox']").removeProp("checked");
			});
		}
	});
});

// socket 收到消息
messageSocket.onmessage = function (event) {
	var msg = eval("(" + event.data + ")");
	var suc = msg.success;
	if(suc == "success"){
		var sendNum = $(".message-infoSend").find("input[name='receiveNumHid']").val();
		var sendMsgImgSrc = $("li[name='"+sendNum+"']").find("img").eq(0).attr("src");
		if(sendNum == msg.sendNum){
			var html;
			if(sendMsgImgSrc != ""&&typeof(sendMsgImgSrc)!= "undefined"){
				html = "<div class='dialogue'>"
						+		"<a class='touxiang'><img src='"+sendMsgImgSrc+"' title='头像'></a><span>"+msg.content +"</span><p class='liaotian'>"+msg.createTime+"</p>"
						+ "</div>";
			}else{
				html = "<div class='dialogue'>"
					+		"<a class='touxiang'><img src='/${projectName}/book/img/icon/touxiang.png' title='头像'></a><span>"+msg.content +"</span><p class='liaotian'>"+msg.createTime+"</p>"
					+ "</div>";
			}
			$(".message-infoCon").append(html);// 动态拼接消息
			animalShowMsg();// 动态显示消息
			changeIsRead();// 修改已读状态
			showMsgPoint(msg.sendNum,msg.content,msg.createTime);
		}else{
			art.dialog.notice({
				title: '提示',
				width: 300,// 必须指定一个像素宽度值或者百分比，否则浏览器窗口改变可能导致artDialog收缩
				content: msg.sendName+"<"+msg.sendNum+">"+"给您发来新消息",
				time: 5
			});
			var len = $(".studList").find("li[name='" + msg.sendNum + "']").find("i[class='samll-i']").length;
			if( len == 0 ){
				// 添加小红点
				$(".studList").find("li[name='" + msg.sendNum + "']").find("div").eq(0).before("<i class='samll-i'></i>");
			}
			// 最新消息
			var msgShow = msg.content;
			showMsgPoint(msg.sendNum,msgShow,msg.createTime);
		}
	}else{
		var opt = msg.opt;
		if(opt == "logout"){
			art.dialog.notice({
				title: '提示',
				width: 300,// 必须指定一个像素宽度值或者百分比，否则浏览器窗口改变可能导致artDialog收缩
				content: msg.tip,
				time: 5
			});
			window.location.href='/${projectName}/sysmanage/logout/bookLogout.action';
		}
	}
}

// 显示带点的消息
function showMsgPoint(sendNum,content,createTime){
	var msgShow = content;
	if(msgShow != null && msgShow != "" && msgShow.length > 13){
		msgShow = msgShow.substring(0,11)+"..."
	} 
	$(".studList").find("li[name='" + sendNum + "']").find(".message-info").find("p").html(msgShow);
	var timeStr = new Date(createTime).Format("HH:mm")
	$(".studList").find("li[name='" + sendNum + "']").find(".message-info").find("span").eq(1).html(timeStr);
	var li = $(".studList").find("li[name='" + sendNum + "']");
	$(".studList").find("li[name='" + sendNum + "']").remove();
	$(".studList").prepend(li);
}

//预览
$('.preview').click(function () {
	$('.phone-preview-bg').show();
	$('.phone-preview').show();
	preShow();
})
$('.phone-preview .back').click(function () {
	$('.phone-preview-bg').hide();
	$('.phone-preview').hide();
})
$('.phone-preview-bg').click(function () {
	$(this).hide();
	$('.phone-preview').hide();
})

//群发
/* $('.mass').click(function(){
	if ($('.mass').text() === '群发') {
		$('.messageLeft-top .item').css('display', 'inline-block');
		$('.messageLeft li input').css('display', 'inline-block');
		$(this).text('取消').addClass('cancel');
		groupSend();
	} else {
		$('.messageLeft-top .item').css('display', 'none');
		$('.messageLeft li input').css('display', 'none');
		$(this).text('群发').removeClass('cancel');
		singleSend();
	}
}) */

//对Date的扩展，将 Date 转化为指定格式的String  
//月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，   
//年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)   
Date.prototype.Format = function (fmt) { //author: meizz   
	var o = {  
			"M+": this.getMonth() + 1, //月份   
			"d+": this.getDate(), //日   
			"H+": this.getHours(), //小时   
			"m+": this.getMinutes(), //分   
			"s+": this.getSeconds(), //秒   
			"q+": Math.floor((this.getMonth() + 3) / 3), //季度   
			"S": this.getMilliseconds() //毫秒   
		};  
	if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));  
	for (var k in o)  
	if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));  
	return fmt;  
}

// 初始化班级
function initClass(){
	$("select[name='classSelect']").html("");
	$.ajax({
		url:"/${projectName}/mail/message/getClassByUser.action" ,
		type : "post" , 
		data : {} ,
		success : function(result) {
			if( result.success ){
				var data = result.data;
				if(data != null){
					var opt = "<option value=''>请选择</option>"
					for( var i = 0; i < data.length; i++ ){
						opt += "<option value='"+data[i].id+"'>"+data[i].label+"</option>";
					}
					$("select[name='classSelect']").html(opt);
				}else{
				}
			}else{
			}
		}
	});
}

// 选择班级时触发
function chooseClass(t){
	
	var cla = $(t).val();
	if(cla != ""){
		getStuList(cla);
	}
}

//获取学生列表
function getStuList(cnumber){
	
	var aat;
	$.ajax({
		url:"/${projectName}/manager/stud/getAllStudByMsg.action" ,
		type : "post" , 
		data : {"cnumber":cnumber} ,
		beforeSend:function(XMLHttpRequest){
			aat = art.dialog({opacity:0.4,
					title:"加载中",
					lock:true,
					content:$("#loadDiv").html(),
					});
		},
		success : function(result) {
			aat.close();
			if(result.success){
				var html = "";
				var data = result.data;
				if(data != null && data.length > 0){
					for(var i=0;i < data.length; i++){
						var stu = data[i];
						var isRead = stu.unReadNum;
						var isReadCss = "";
						if(isRead > 0){
							isReadCss = "<i class='samll-i'></i>";
						}
						var time = stu.creatTime;
						var timeStr ="";
						if(time != null && typeof(time) != "undefined" && time != ""){
							timeStr = new Date(time).Format("HH:mm")
						}
						
						var msgShow = stu.newMsg;
						if(msgShow != null && msgShow != "" && msgShow.length > 13){
							msgShow = msgShow.substring(0,20)+"..."
						}
						
						var photoUrlHtml ="";
						if(stu.sendPhotoUrl != null && stu.sendPhotoUrl != ""){
							photoUrlHtml ="<a class='touxiang'><img src='/${projectName}/imgs/photos/"+stu.sendCNum+"/"+stu.sendPhotoUrl+"' ></a>";//服务器头像
						}else{
							photoUrlHtml ="<a class='touxiang'><img src='/${projectName}/book/img/icon/touxiang.png' title='头像'></a>";//默认头像
						}
						
						html += "<li name='"+stu.sendNum+"' class='' onclick=\"talkMsg(this,'"+stu.sendName+"');\">"
							 +		isReadCss
							 +		"<div class='my_protocol'><input name='singleBox' type='checkbox' onclick=\"chooseSingle(this,'"+stu.sendNum+"');\" value='"+stu.sendNum+"' />"
							 +		"<i class='input-i'></i></div>"
							 +		"<div class='message-info'>"
							// +			"<span class='touxiang'><img src='/${projectName}/book/img/icon/touxiang.png' title='头像'></span>"
							 +			photoUrlHtml

							 +			"<div class='message-right'><span class='name'>"+stu.sendName+"</span>"
							 +			"<span class='message-time'>"+timeStr+"</span>"
							 +			"<p class='message-content'>"+msgShow+"</p></div>"
							 +		"</div>"
							 +	"</li>";
					}
				}
				$(".messageLeft.pull-left").find(".studList").html(html);
			}else{
				alert(result.msg);
			}
		}
	});
}

// 显示对话信息
function talkMsg( t , talkName){
	if(!isGroupSend){
		$(".studList").find("li").removeClass("on");
		$(t).addClass("on");
		
		var loginImgSrc = $(".loginDiv img").eq(1).attr("src");
		var sendMsgImgSrc ="";

		var sendNum = $(t).find("input[type='checkbox']").val();// 发送者学号
		$(".message-infoSend").find("input[name='receiveNumHid']").val(sendNum);
		var number = "${CURRENT_USER.number}";// 当前登陆者
		$(".message-infoCon").html("");// 清空对话内容
		// ajax查询数据
		$.ajax({
			url : "/${projectName}/mail/message/schMsgList.action" , 
			type : "post" , 
			data : {"sendNum":sendNum} , 
			success : function(result) {
				if( result.success ){
					var data = result.data;
					if(data != null && data.length > 0){
						var html = "";
						for(var i = 0; i < data.length; i++ ){
							var msg = data[i];
							var sendNum = msg.sendNum;
							
							if(sendNum == number){
								html += "<div class='dialogue self'>"
									 +		"<span>"+msg.content +"</span><a class='touxiang'><img src='"+loginImgSrc+"' title='头像'></a><p class='liaotian'>"+msg.createTime+"</p>"
									 +  "</div>";
							}else{
								sendMsgImgSrc = $("li[name='"+sendNum+"']").find("img").eq(0).attr("src");
								if(sendMsgImgSrc != ""&&typeof(sendMsgImgSrc)!= "undefined"){
									html += "<div class='dialogue'>"
										 +		"<a class='touxiang'><img src='"+sendMsgImgSrc+"' title='头像'></a><span>"+msg.content +"</span><p class='liaotian'>"+msg.createTime+"</p>"
										 +  "</div>";
								}else{
									html += "<div class='dialogue'>"
										 +		"<a class='touxiang'><img src='/${projectName}/book/img/icon/touxiang.png' title='头像'></a><span>"+msg.content +"</span><p class='liaotian'>"+msg.createTime+"</p>"
										 +  "</div>";
								}
							}
						}
						$(".message-infoCon").html(html);
						$(".message-infoCon").get(0).scrollTop = $(".message-infoCon").get(0).scrollHeight;
						changeIsRead();
					}
				}else{
					alert(result.msg);
				}
				
				if(sendMsgImgSrc != ""&&typeof(sendMsgImgSrc)!= "undefined"){
					$(".messageRight-top").find(".talkName").html("<a class='touxiang2'><img src='"+sendMsgImgSrc+"' title='头像'></a>"+talkName);// 显示对话人姓名
				}else{
					$(".messageRight-top").find(".talkName").html("<a class='touxiang2'><img src='/${projectName}/book/img/icon/touxiang.png' title='头像'></a>"+talkName);// 显示对话人姓名
				}
			}
		});
	}
}

// 手机预览页面
function preShow(){
	var loginImgSrc = $(".loginDiv img").eq(1).attr("src");
	var sendMsgImgSrc ="";
	
	var sendNum = $(".message-infoSend").find("input[name='receiveNumHid']").val();
	var talkName = $(".studList").find("li[name='" + sendNum + "']").find(".message-info").find("span").eq(1).html();
	$(".phone-preview-title").html(talkName);
	if(sendNum != null && sendNum != ""){
		// ajax查询数据
		$.ajax({
			url : "/${projectName}/mail/message/schMsgList.action" , 
			type : "post" , 
			data : {"sendNum":sendNum} , 
			success : function(result) {
				if( result.success ){
					var data = result.data;
					if(data != null && data.length > 0){
						var html = "";
						for(var i = 0; i < data.length; i++ ){
							var msg = data[i];
							var sendNum = msg.sendNum;
							if(sendNum == number){
								html += "<div class='dialogue self'>"
									 +		"<span>"+msg.content +"</span><a class='touxiang'><img src='"+loginImgSrc+"' title='头像'></a><p class='liaotian'>"+msg.createTime+"</p>"
									 +  "</div>";
							}else{
								sendMsgImgSrc = $("li[name='"+sendNum+"']").find("img").eq(0).attr("src");
								if(sendMsgImgSrc != ""&&typeof(sendMsgImgSrc)!= "undefined"){
									html += "<div class='dialogue'>"
										 +		"<a class='touxiang'><img src='"+sendMsgImgSrc+"' title='头像'></a><span>"+msg.content +"</span><p class='liaotian'>"+msg.createTime+"</p>"
										 +  "</div>";
								}else{
									html += "<div class='dialogue'>"
										 +		"<a class='touxiang'><img src='/${projectName}/book/img/icon/touxiang.png' title='头像'></a><span>"+msg.content +"</span><p class='liaotian'>"+msg.createTime+"</p>"
										 +  "</div>";
								}
							}
						}
						$(".phone-preview-con").html(html);
						$(".phone-preview-con").get(0).scrollTop = $(".phone-preview-con").get(0).scrollHeight;
					}
				}else{
					alert(result.msg);
				}
			}
		});
	}
}

var isGroupSend = false;
// 群发
function groupSend(){
	isGroupSend = true;
	$(".message-infoCon").html("");// 消息内容
	$(".messageRight-top").find(".talkName").html("群发");// 显示对话人姓名
	$("button[name='singleBtn']").hide();// 单发按钮隐藏
	$("button[name='groupBtn']").show();// 群发按钮显示
	// 清除选中状态
	$(".messageLeft.pull-left").find(".studList").find("li").removeClass("on");
	$(".message-infoSend").find("input[name='receiveNumHid']").val("");// 隐藏值设置为空
	$(".preview").hide();
}

// 单发
function singleSend(){
	isGroupSend = false;
	$(".message-infoCon").html("");// 消息内容
	$(".messageRight-top").find(".talkName").html("");// 显示对话人姓名
	$("button[name='singleBtn']").show();// 单发按钮隐藏
	$("button[name='groupBtn']").hide();// 群发按钮显示
	$("input[name='singleBox']").each(function(){
		$(this).removeProp("checked");
	});
	$(".preview").show();// 隐藏预览按钮
}

// 单选
function chooseSingle(t,stuNum){
	
	var talkName = "";
	$("input[name='singleBox']:checked").each(function(){
		if(talkName != ""){
			talkName =talkName+" , " +$(this).next("div").find("span[class='name']").html();
		}else{
			talkName = $(this).next("div").find("span[class='name']").html();
		}
	});
	if(talkName != ""){
		talkName = "群发:("+talkName+")";
	}
	$(".messageRight-top").find(".talkName").html(talkName);// 显示对话人姓名
}

// 修改已读状态
function changeIsRead(){
	var sendNum = $(".message-infoSend").find("input[name='receiveNumHid']").val();
	$.ajax({
		url : "/${projectName}/mail/message/changeIsRead.action" , 
		type : "post" , 
		data : {"sendNum":sendNum} , 
		success : function(result) {
			if(result.success){
				var msg = result.data;
				var len = $(".studList").find("li[name='" + sendNum + "']").find("i").length;
				if( len > 0 ){
					// 去除小红点
					$(".studList").find("li[name='" + sendNum + "']").find(".samll-i").remove();
				}
			}else{
				
			}
		}
	});
	
}

// 发送消息
function sendMsg(t){
	var content = $(t).parent().find("textarea").val();
	var receiveNum = $(".message-infoSend").find("input[name='receiveNumHid']").val();
	
	var flg = true;
	if(receiveNum == null || receiveNum == ""){
		swal({ title: "请选择则对话人",
			type: "error"});
		flg = false;
	}
	
	if(content == null || content == ""){
		swal({ title: "内容不允许为空",
			type: "error"});
		flg = false;
	}
	if(flg){
		$.ajax({
			url : "/${projectName}/mail/message/sendMsg.action" , 
			type : "post" , 
			data : {"receiveNum":receiveNum,"content":content} , 
			success : function(result) {
				if(result.success){
					
					$(t).parent().find("textarea").val("");
					$(t).parent().find("textarea").focus();
					var loginImgSrc = $(".loginDiv img").eq(1).attr("src");
					var msg = result.data;
					var html = "<div class='dialogue self'>"
							 +		"<span>"+msg.content +"</span><a class='touxiang'><img src='"+loginImgSrc+"' title='头像'></a><p class='liaotian'>"+msg.createTime+"</p>"
							 +  "</div>";
					$(".message-infoCon").append(html);
					animalShowMsg();
					showMsgPoint(receiveNum,msg.content,msg.createTime);
				}else{
					alert(result.msg);
				}
			}
		});
	}
}

// 群发消息
function groupSendMsg(t){
	var content = $(".message-infoSend").find("textarea").val();
	
	var receiveNums = new Array();
	$("input[name='singleBox']").each(function(i,item){
		if($(this).is(":checked")){
			receiveNums.push($(this).val());
		}
	});
	
	var flg = true;
	if(receiveNums == null || receiveNums.length == 0){
		swal({ title: "请选择则对话人",
			type: "error"});
		flg = false;
	}
	
	if(content == null || content == ""){
		swal({ title: "内容不允许为空",
			type: "error"});
		flg = false;
	}
	if(flg){
		var obj = {};
		obj.receiveNums = receiveNums;
		obj.content = content;
		$.ajax({
			url : "/${projectName}/mail/message/sendGroupMsg.action" , 
			contentType : "application/json;charset=UTF-8",
			dataType: 'json',
			type : "post" , 
			data :JSON.stringify(obj) , 
			success : function(result) {
				if(result.success ){
					$(t).parent().find("textarea").val("");
					$(t).parent().find("textarea").focus();
					
					var msg = result.data;
					var html = "<div class='dialogue self'>"
							 +		"<span>"+msg.content +"</span><a class='touxiang'><img src='/${projectName}/book/img/icon/touxiang.png' title='头像'></a><p class='liaotian'>"+msg.createTime+"</p>"
							 +  "</div>";
					$(".message-infoCon").append(html);
					animalShowMsg();
					
					for(var i = 0; i < receiveNums.length; i++){
						showMsgPoint(receiveNums[i],msg.content,msg.createTime);
					}
				}
			}
		});
	}
}


// 动画显示信息
function animalShowMsg(){
	var t = $(".message-infoCon").height()+ $(".message-infoCon").scrollTop();
	$('.message-infoCon').animate({'scrollTop':t},1100);
}

</script>
</html>
