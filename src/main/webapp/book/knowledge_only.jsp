<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="ie6"><![endif]-->
<!--[if IE 7]>
<html class="ie7"><![endif]-->
<!--[if IE 8]>
<html class="ie8"><![endif]-->
<!--[if (gte IE 9)]><!-->
<html>
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<title>呼叫中心知识详情页</title>
<link rel="stylesheet" type="text/css"	href="/${projectName}/book/css/common.css" />
<link rel="stylesheet" type="text/css"	href="/${projectName}/book/css/fonts.css" />
<link rel="stylesheet" type="text/css"	href="/${projectName}/book/css/style.css" />

<script type="text/javascript"	src="/${projectName}/book/js/lib/jquery-1.9.1.min.js"></script>
<script type="text/javascript"	src="/${projectName}/book/js/lib/jquery.page.js"></script>
<script type="text/javascript"	src="/${projectName}/book/js/common/page.js"></script>
<style>
.zd_selected {
	color: #f36501;
}
.zd_show {
	display: block;
}
.zd_label {
	font-weight: bold;
}
</style>
<script>
//banner - 书本名称
var projectName = '${projectName}';					
var htmltree = "";
var bookId = '${bookId}';
var tabName= "knowledgeTab";
$(function(){
	var bookRootId = '${bookRoot.id}',
		html = '<img src="/${projectName}/book/img/banner_44.jpg" />';
	$('.banner').append(html);
	
	$.ajax({
		url : "/${projectName}/book/book/schKnowledgeTree.action" ,
		async:false,
		success : function(data) {
			// htmltree = htmltree + "<li class='title'><i class='show'></i><span>"+data.label+"</span></li>";
			htmltree = htmltree + "<li class='title'><i class='hide'></i><span class='zd_label'>"+data.label+"</span></li>";
			var childList = data.childList;
			showall(childList);
			$("#zdtree").append(htmltree);
		}
	});
	catalogueLeft();
	//treeid='2';//TODO 初始时显示【通用】下数据
	search(1);
	
	//根据bookId判断是否显示实训营地
	getOpentrainingValue('${bookRoot.id}');
});


function getOpentrainingValue(bookId){
	$.ajax({
		url : "/${projectName}/book/book/schBookByBookID.action" , 
		type : "post" , 
		data : {"bookId" : bookId} , 
		dataType: "json",
		success : function(result) {
			if (result.success ) {
				var OpentrainingValue = result.data.opentraining;
				if(OpentrainingValue == "1"){
					$("#practiceurl").css("display","block");
				}else{
					$("#practiceurl").css("display","none");
				}
				return OpentrainingValue;
			}
		}
	});
}


function showall(childList) {
	// htmltree = htmltree + "<ul class='padding-left' style='display:block;'>";// 修改默认显示（展开），添加了个样式display:block
	htmltree = htmltree + "<ul class='padding-left zd_items'>";
	for(var i=0; i<childList.length; i++) {
		if(childList[i].childList.length > 0) {
			htmltree = htmltree + "<li class='title'><i class='hide'></i><span treeid="+childList[i].id+" class='zd_label'>" + childList[i].label + "</span></li>";
			showall(childList[i].childList);
		}else{
			// htmltree = htmltree + "<li><span treeid="+childList[i].id+">" + childList[i].label + "</span></li>";
			if (childList[i].bookId == bookId) {
				htmltree = htmltree + "<li style='cursor:pointer' class='zd_selected'><span treeid="+childList[i].id+" class='zd_label'>" + childList[i].label + "</span></li>";
				treeid = childList[i].id;
			} else {
				htmltree = htmltree + "<li style='cursor:pointer' ><span treeid="+childList[i].id+" class='zd_label'>" + childList[i].label + "</span></li>";
			}
		}
	}
	htmltree = htmltree + "</ul>";
}


</script>
</head>

<body>
<div class="wrap">
	<div class="content-box">
		<div class="main bg">
			<div class="detailBox inner">

				<div class="tabsBox clearfix" id="tabs-basic">
					<div class="tabsleft pull-left" style="height:750px;width:40%;overflow-y:auto;">
						<ul class="padding-left" id="zdtree">
						    
						</ul>
					</div>
					
					<div class="tabsInfo border-left pull-right" style="height:750px;width:60%;overflow-y:auto;">
						<form id="_form">
							<div class="search-box">
								输入关键字：<input type="text" name='repoTitle' id="repoTitle" class='easyui-textbox' style="border: 1px solid" /> 
								<input type="button" value="GO"	onclick="search()" />
								<input style="display:none" /> 
							</div>
							
							<table id="_table" style="width:100%;">
								<thead>
									<tr>
										<th>文件类型</th>
										<th>标题</th>
										<!-- <th>目录</th> -->
										<!-- <th>修改时间</th> -->
										<th>操作</th>
									</tr>
								</thead>
							</table>
						</form>
						
						<!-- 分页 -->
						<div class="fydiv">
							<ul class="fenye"></ul>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<jsp:include page="_footer.jsp" />

<jsp:include page="./pop.jsp" />

<script>

	// 	 搜索栏的标题的模糊查询
	function search(pnum) {
		$("#_table").find("tbody").html('');
		// 		 alert($("form input:first-child").val());
		if (pnum == null || pnum == "undefined" ) {
			pnum = 1;
		}
		
		$.ajax({
			url : "/${projectName}/manager/repository/schRepository.action" , type : "post" , data : { 
				"repoTitle" : $("input[name='repoTitle']").val() , "page" : pnum,"pagesize":30, "organids":treeid 
			} , dataType : "json" , success : function(data) {
				getPage(pnum, data);
			}
		});
	}

	function getPage(pn, data) {
		var dataCount = data.total;//总数据条数
		var pageSize = data.pageSize;//每页显示条数
		data = data.rows;
		console.log(data);
		var pageCount = Math.ceil(dataCount / pageSize);//总页数
		if (pn == 0 || pn > pageCount ) {
			return;
		}
		var ul = $(".listul");
		ul.empty();
		//console.log(pageCount+"..."+pn)
		paintPage(pageCount, pn); //绘制页码
		var startPage = pageSize * ( pn - 1 );
		
		for (var j = 0; j < pageSize; j++) {
			if (data[j].changeTime == null ) {
				data[j].changeTime = "";
			}
			// var html = "<tr><td><a href='javascript:void(0);' onclick='showFile(\""+data[j].attachPath +"\")'>" + data[j].repoTitle + "</a></td><td>" + data[j].shortCut + "</td><td>" + data[j].changeTime + "</td></tr>"
			var html = "<tr><td style='width:128px;'><img src='/${projectName}/book/img/pdf.png' style='height:24px;width:24px;'/></td><td>" + data[j].repoTitle + "</td><td><a href='javascript:void(0);' onclick='showFile(\""+data[j].attachPath +"\")'>查看</a></td></tr>"
			$("#_table").append(html);
		}
		
	}

	//绘制页码
	function paintPage(number, currNum) //number 总页数,currNum 当前页  
	{
		var pageUl = $(".fenye");
		pageUl.empty();
		var ulDetail = "";
		
		if (number == 1 ) {
			ulDetail = "<li class=\"start\"><a href=\"javascript:void(0)\">首页</a></li>" + "<li class=\"prev\"><a href=\"javascript:void(0)\">上一页</a></li>"
					+ "<li class=\"numb choose\"><a href=\"javascript:search(1)\">1</a></li>" + "<li class=\"next\"><a href=\"javascript:void(0)\">下一页</a></li>"
					+ "<li class=\"end\"><a href=\"javascript:void(0)\">尾页</a></li>";
		} else if (number == 2 ) {
			ulDetail = "<li class=\"start\"><a href=\"javascript:search(1)\">首页</a></li>" + "<li class=\"prev\"><a href=\"javascript:search(1)\">上一页</a></li>"
					+ "<li class=\"numb" + choosele(currNum, 1) + "\"><a href=\"javascript:search(1)\">1</a></li>" + "<li class=\"numb" + choosele(currNum, 2)
					+ "\"><a href=\"javascript:search(2)\">2</a></li>" + "<li class=\"next\"><a href=\"javascript:search(2)\">下一页</a></li>"
					+ "<li class=\"end\"><a href=\"javascript:search(2)\">尾页</a></li>";
		} else if (number == 3 ) {
			ulDetail = "<li class=\"start\"><a href=\"javascript:search(1)\">首页</a></li>" + "<li class=\"prev\"><a href=\"javascript:search(" + parseInt(currNum - 1)
					+ ")\">上一页</a></li>" + "<li class=\"numb" + choosele(currNum, 1) + "\"><a href=\"javascript:search(1)\">1</a></li>" + "<li class=\"numb"
					+ choosele(currNum, 2) + "\"><a href=\"javascript:search(2)\">2</a></li>" + "<li class=\"numb" + choosele(currNum, 3)
					+ "\"><a href=\"javascript:search(3)\">3</a></li>" + "<li class=\"next\"><a href=\"javascript:search(" + parseInt(currNum + 1) + ")\">下一页</a></li>"
					+ "<li class=\"end\"><a href=\"javascript:search(3)\">尾页</a></li>";
		} else if (number == currNum && currNum > 3 ) {
			ulDetail = "<li class=\"start\"><a href=\"javascript:search(1)\">首页</a></li>" + "<li class=\"prev\"><a href=\"javascript:search(" + parseInt(currNum - 1)
					+ ")\">上一页</a></li>" + "<li class=\"numb\"><a href=\"javascript:search(" + parseInt(currNum - 2) + ")\">" + parseInt(currNum - 2) + "</a></li>"
					+ "<li class=\"numb\"><a href=\"javascript:search(" + parseInt(currNum - 1) + ")\">" + parseInt(currNum - 1) + "</a></li>"
					+ "<li class=\"numb choose\"><a href=\"javascript:search(" + currNum + ")\">" + currNum + "</a></li>" + "<li class=\"next\"><a href=\"javascript:search("
					+ currNum + ")\">下一页</a></li>" + "<li class=\"end\"><a href=\"javascript:search(" + number + ")\">尾页</a></li>";
		} else if (currNum == 1 && number > 3 ) {
			ulDetail = "<li class=\"start\"><a href=\"javascript:search(1)\">首页</a></li>" + "<li class=\"prev\"><a href=\"javascript:void(0)\">上一页</a></li>"
					+ "<li class=\"numb choose\"><a href=\"javascript:void(0)\">1</a></li>" + "<li class=\"numb\"><a href=\"javascript:search(2)\">2</a></li>"
					+ "<li class=\"numb\"><a href=\"javascript:search(3)\">3</a></li>" + "<li class=\"next\"><a href=\"javascript:search(2)\">下一页</a></li>"
					+ "<li class=\"end\"><a href=\"javascript:search(" + number + ")\">尾页</a></li>";
		} else {
			ulDetail = "<li class=\"start\"><a href=\"javascript:search(1)\">首页</a></li>" + "<li class=\"prev\"><a href=\"javascript:search(" + parseInt(currNum - 1)
					+ ")\">上一页</a></li>" + "<li class=\"numb\"><a href=\"javascript:search(" + parseInt(currNum - 1) + ")\">" + parseInt(currNum - 1) + "</a></li>"
					+ "<li class=\"numb choose\"><a href=\"javascript:search(" + currNum + ")\">" + currNum + "</a></li>" + "<li class=\"numb\"><a href=\"javascript:search("
					+ parseInt(currNum + 1) + ")\">" + parseInt(currNum + 1) + "</a></li>" + "<li class=\"next\"><a href=\"javascript:search(" + parseInt(currNum + 1)
					+ ")\">下一页</a></li>" + "<li class=\"end\"><a href=\"javascript:search(" + number + ")\">尾页</a></li>";
		}
		
		$(".fenye").append(ulDetail);
	}

	function choosele(num, cur) {
		if (num == cur ) {
			return " choose";
		} else {
			return "";
		}
	}
	
function showFile(filePath){
	if(filePath==null || filePath=='' || filePath==undefined || filePath=='null'){
		swal({ 
			title: "文件不存在！", 
			type: "error"});
	}else{
		window.open('/${projectName}/js/pdfjs/web/viewer.html?file=/${projectName}/manager/repository/appendUrl/'+filePath+'.action');
	}
}

//菜单折叠展开
function foldZd(zdSelected) {
	//var zdSelected = $(".zd_selected");
	var parentUl = $(zdSelected).parent('ul');
	var prevLi = $(parentUl).prev('li');
	console.log($(parentUl).hasClass('zd_items'));
	if($(parentUl).hasClass('zd_items')) {
		foldZd(prevLi);
	}
	// show 图标
	$(prevLi).children('i').removeClass('hide').addClass('show');
	$(parentUl).show();
	//var parentZd = $(zdSelected).parent('ul');
	//console.log(parentZd);
	//if (parentZd != undefined) {
	//	$(parentZd).show();
	//}
}

//菜单展示
var treeid = "";
function catalogueLeft() {
	var zdSelected = $('.zd_selected');
	foldZd(zdSelected);
	
	var zdLabels = $('.zd_label');
	zdLabels.each(function () {
		$(this).click(function (){
			$('.zd_selected').removeClass('zd_selected');
			$(this).addClass('zd_selected');
		})
	})
	
	
	var title = $('.padding-left li i');
	
	 title.each(function (){
		$(this).click(function(){
	         if ($(this).hasClass('show')) {
	             $(this).removeClass('show').addClass('hide');
	             $(this).parent().next('ul').hide();
	         } else {
	        	 $(this).parent().siblings('li').children('i').removeClass('show').addClass('hide');
	             $(this).removeClass('hide').addClass('show');
	             $(this).parent().siblings('ul').hide();
	             $(this).parent().next('ul').show();
	         }
			
	     });
	 });
	 
	 var li = $('.padding-left li span');
	 li.each(function (){
		 $(this).click(function(){
			treeid = $(this).attr("treeid");
			search(1);
	     });
	 })
	 
}

</script>
</body>
<script type="text/javascript"	src="/${projectName}/book/js/common/login.js"></script>
</html>
