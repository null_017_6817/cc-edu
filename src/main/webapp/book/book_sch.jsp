<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>课本查询</title><!--book-->
	<%@include file="/common/include.jsp" %>
	<script type="text/javascript" charset="utf-8"	src="/${projectName}/ueditor/ueditor.config.js"></script>
	<script type="text/javascript" charset="utf-8"	src="/${projectName}/ueditor/ueditor.all.js"></script>
</head>
<body>
<body class="easyui-layout">
	<div region="west" split="true" title="课本单元" style="width:260px;">
		<dc:includeTree dictId="CC_BOOK" name="booktree" style="asynch_single" bind="select:openBook" value="1"/>
	</div>
	<div id="content" region="center" style="border:0;">
		<table id="bookGrid" title="" fitcolumns=true
		data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#bookGridToolbar',collapsible:true,url:'/${projectName}/book/book/schBook.action',method:'get'">
			<thead>
				<tr>
					<th data-options="field:'checkbox',checkbox:true" ></th>
<!-- 				<th data-options="field:'id',width:25,align:'center',sortable:true"></th>
					<th data-options="field:'pid',width:25,align:'center',sortable:true"></th>  -->
					<th data-options="field:'name',width:25,align:'center',sortable:true">书名或章节名</th>
					<th data-options="field:'bookconcern',width:25,align:'center',sortable:true">出版社</th>
					<th data-options="field:'editor',width:25,align:'center',sortable:true">主编</th>
					<th data-options="field:'publictime',width:25,align:'center',sortable:true">出版时间</th>
					<!-- <th data-options="field:'path',width:25,align:'center',sortable:true">课本路径</th> -->
					<th data-options="field:'state',width:25,align:'center',sortable:true">状态</th>
					<th data-options="field:'type',width:25,align:'center',sortable:true">课本类型</th>
					<!-- <th data-options="field:'profession',width:25,align:'center',sortable:true">专业</th>
					<th data-options="field:'cnumber',width:25,align:'center',sortable:true">班级号</th>
					<th data-options="field:'term',width:25,align:'center',sortable:true">学期</th>  -->
					<!-- <th data-options="field:'',width:25,align:'center',sortable:true">目标</th>  -->
					<th data-options="field:'target',align:'center',formatter:formatTarget">目标</th>
					<th data-options="field:'_oper',align:'center',formatter:formatOper">操作</th>
				</tr>
			</thead>
		</table>
	</div>
<%-- 查询条件设定栏 --%>
<div id="bookGridToolbar">
<!-- 	<div style="margin-bottom:5px"> -->
<!-- 		<a href="javascript:updBook(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true">添加</a> -->
<!-- 		<a href="javascript:delBook()" class="easyui-linkbutton" iconCls="icon-remove" plain="true">删除</a> -->
<!-- 	</div> -->
<!-- 	<form id="_form"> -->
	课本或章节名：<input name='name' id="name" class='easyui-textbox' style="width:200px;" value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
},{iconCls:'icon-search',handler: function(e){doSearch();}
}]"/>
<label for="child"><input type="checkbox" name="child" id="child" value="1" style="vertical-align: middle;width:15px;height:15px" checked="checked"/>包含下级</label>
<a href="javascript:void(0)" onclick="doSearch()" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>&emsp;
<a href="javascript:void(0)" onclick="updBook(0)" class="btn btn-success"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">添加</span></a>&emsp;
<a href="javascript:void(0)" onclick="delBook()" class="btn btn-danger"><span class="icon-trash" style="font-size:16px"></span> <span style="font-size:14px">删除</span></a>&emsp;
<a href="javascript:void(0)" onclick="expBook();" class="btn btn-success"><span class="icon-download-alt" style="font-size:16px"></span> <span style="font-size:14px">导出</span></a>&emsp;
</div>

<!--	出版社：<input name='bookconcern' id="bookconcern" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	主编：<input name='editor' id="editor" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	课本类型：<dc:insertList name="type" dictId="ZD_BOOKTYPE" style="combo_normal" />

 	：<input name='id' id="id" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	班级号：<input name='cnumber' id="cnumber" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	：<input name='pid' id="pid" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/> 
	出版时间：<input name="publictimeBeg" id="publictimeBeg" class="easyui-datebox" style="width:100px;" data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
&gt;&gt;
	<input name="publictimeEnd" id="publictimeEnd" class="easyui-datebox" style="width:100px;" data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}}],
validType:['compareDate[publictimeBeg]']"/>
	课本路径：<input name='path' id="path" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	专业：<input name='profession' id="profession" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	学期：<input name='term' id="term" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>-->
	
<!-- 	<a href="#" class="easyui-linkbutton" iconCls="icon-search" onclick="doSearch()">查询</a> -->
<!-- 	</form> -->


<script>

$(function(){
	var pager = $('#bookGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		/* buttons:[{
			iconCls:'icon-add',
			text:'添加',
			handler:function(){
				updBook(0);
			}
		},{
			iconCls:'icon-remove',
			text:'删除',
			handler:function(){
				var checkedItems = $('#bookGrid').datagrid('getChecked');
				var ids = [];
				$.each(checkedItems, function(index, item){
					ids.push(item.id);
				});
				$.messager.confirm('敬告','删除选定记录？',function(r){
					if(r){
						$.get("/${projectName}/book/book/delBook.action",{ids:ids},function(data){
							$('#bookGrid').datagrid('reload');
							$('#booktree').tree('reload');
						});
					}
				});
			}
		}], */
	});
	
	$('#name').textbox('textbox').keydown(function (e) {
        if (e.keyCode == 13) {
           doSearch();
        }
    });
    
    $('#child').click(function(){
    	doSearch();
    });
    setTimeout("collapseAll()",1000);
})

function collapseAll(){
	booktreeDict.expand(1);
	//$('#booktree').tree('collapseAll');
}

<%-- 执行查询，点击查询按钮时调用 --%>
function doSearch(){
	$('#bookGrid').datagrid('load',{
		name:$('#name').val(),
		id: currBookId,
		child:$('#child').get(0).checked
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatOper(val,row,index){  
	return "&emsp;<a href='javascript:void(0)' onclick='updBook("+val+")' class='_oper' title='修改'>&nbsp;<i class='icon-edit'></i></a>&emsp;";
} 
<%-- 格式化目标 --%>
function formatTarget(val,row,index){  
	var isNull="";
	$.ajax({
		type : "POST",
		async:false,
		url : '/${projectName}/book/book/schBookById.action',
		data : {id:row.id},	
		dataType:'json',
		beforeSend:function(XMLHttpRequest){ 
		},
		success : function(result) {
			if(result.success){
				if(result.data.goal==null||result.data.goal==''||result.data.goal==undefined){
					isNull= "<a href='javascript:void(0)' onclick=editGoal('"+row.id+"')>空</a>";
				}else{
					isNull= "<a href='javascript:void(0)' onclick=editGoal('"+row.id+"')>有</a>";
				}
			}
		}
	}); 
	return isNull;
} 
<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
function updBook(id){
	if(currBookId==undefined){
		$.messager.alert('敬告','请选择课本？');
		return;
	}
	if(id == '0'){
		$("#bookEditWin").dialog({title: '添加课本',resizable:true});
	}
	if(id != '0'){
		$("#bookEditWin").dialog({title: '修改课本',resizable:true});
	}
	QMoveDialog($("#bookEditWin"));
	$('#bookEditWin').dialog('open');
	$('#bookEditWin').get(0).contentWindow.load(id,currBookId);
} 

<%-- 删除记录 --%>
function delBook(){
	var checkedItems = $('#bookGrid').datagrid('getChecked');
		
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/book/book/delBook.action",{ids:ids},function(data){
				//$('#bookGrid').datagrid('reload');
				var res = JSON.parse(data);
				var code = res.code;
				var msg = res.msg;
				if (code == "0") {
					$.messager.alert('敬告', msg);
					return;
				}
				if (code == "1") {
					$.each(checkedItems, function(index, item){
						var node = $('#booktree').tree('find', item.id);
						$('#booktree').tree('remove',node.target);
					});
					reloadData();
				}
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	/* var src = $("#bookEditWin").attr("src");
	$("#bookEditWin").attr("src","");
	$("#bookEditWin").attr("src",src); */
	
	$('#bookEditWin').dialog('close');
	$('#bookGrid').datagrid('reload');
	//$('#booktree').tree('reload');
	//setTimeout("collapseAll()",0.001);
}

var currBookId;
function openBook(json){//{id:'',text:''}
	currBookId=json.id;
	doSearch();
}

var currentId;
// 编辑章节目标
function editGoal(id){
	QMoveDialog($("#goalDIV"));
	$('#goalDIV').dialog('open');
	$('#goalDIV').get(0).contentWindow.load(id);
}

function closeGoalEdit(msg){
	$('#goalDIV').dialog('close')
	reloadData();
	$.messager.show({
		title:'提示',
		msg:msg,
		timeout:2000,
		showType:'slide'
	});
}

function expBook() {
	window.open("/${projectName}/book/book/download.action");
}

</script>

<%-- 修改or添加面板 --%>
<iframe id="bookEditWin" src="/${projectName}/book/book/page.action?page=book_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#bookEditButton'" title="添加课本" style="width:700px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="bookEditButton">
	<a href="javascript:void(0)" onclick="$('#bookEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#bookEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >取 消</a>
</div>
<%-- 查看面板 --%>
<iframe id="bookViewWin" src="/${projectName}/book/book/page.action?page=book_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#bookViewButton'" title="查看课本" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="bookViewButton">
	<a href="javascript:$('#bookViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >关 闭</a>
</div> 
<iframe id="goalDIV" src="/${projectName}/book/book/page.action?page=goalEdit" class="easyui-window" data-options="iconCls:'icon-save',buttons: '#goalEditButton'" title="学习目标" style="width:700px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0">
</iframe>
</body>
</html>


