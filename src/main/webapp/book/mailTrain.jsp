<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="ie6"><![endif]-->
<!--[if IE 7]>
<html class="ie7"><![endif]-->
<!--[if IE 8]>
<html class="ie8"><![endif]-->
<!--[if (gte IE 9)]><!-->
<html>
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<title>呼叫中心实训入口页面</title>
<link rel="stylesheet" type="text/css"	href="/${projectName}/book/css/common.css" />
<link rel="stylesheet" type="text/css"	href="/${projectName}/book/css/fonts.css" />
<link rel="stylesheet" type="text/css"	href="/${projectName}/book/mail-fonts/font.css" />
<link rel="stylesheet" type="text/css"	href="/${projectName}/book/css/style.css" />
<script type="text/javascript"	src="/${projectName}/book/js/lib/jquery-1.9.1.min.js"></script>
<script type="text/javascript" charset="utf-8"	src="/${projectName}/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="/${projectName}/book/js/jqueryForm/jquery.form.js"></script>
<script type="text/javascript" charset="utf-8"	src="/${projectName}/ueditor/ueditor.all.js"></script>
<!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
<!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
<script type="text/javascript" charset="utf-8"	src="/${projectName}/ueditor/lang/zh-cn/zh-cn.js"></script>

<style>

.buttonDiv button{
	width: 100px;height: 30px;border-width: 0px;
	border-radius: 3px;   
	background: #1E90FF;  
	cursor: pointer;   
	outline: none; 
	font-family: Microsoft YaHei; 
	color: white;   
	font-size: 17px;
}
.autographTitle{
	padding-left: 10px;
	margin-top: 0px;
	margin-bottom: 0px;
	font-size: 15px;
	background-color: steelblue;
	color: white;
}


</style>
</head>

<body>
<input type="hidden" name="currUser" value="${agentId}" />
<input type="hidden" name="Ordertype" value="2" />
<div class="wrap">
	<div class="content-box">
		<jsp:include page="_header.jsp" />
		<div id="fade" class="black_overlay"></div>
		<!-- <div class="entranceNav">
			<div class="inner">
				<a href="/${projectName}/book/index.jsp">首页</a>&nbsp;/&nbsp;<span>邮件实训</span>
			</div>
		</div> -->
		<div class="main mailbg">
			<div class="inner mail-model">				
				<div class="tabPanel">
					<ul name="topTab" class="items">
						<li onclick="mailList(this);" class="hit"><i class="title-icon title-icon1"></i>收取</li>
						<li onclick="mailEdit(this);" class="" ><i class="title-icon title-icon2"></i>写邮件</li>							
						<div class="assist">
							<span onClick="deleteMore();"><i class="title-icon title-icon3"></i>删除</span>
							<span onClick="topForWrdMail();" ><i class="title-icon title-icon4"></i>转发</span>
							<span onClick="setTop();"><i class="title-icon title-icon5"></i>置顶</span>
							<span onClick="setAutograph(this);"><i class="title-icon title-icon6"></i>个性签名</span>
							<!-- <div class="search" >
								<input type="text" placeholder="搜索邮件" value="" />
								<i onclick=" schByTitle(this);" class="ser-btn icon-search"></i>
							</div> -->
						</div>
					</ul>
					<div class="panes">
						<div class="pane clearfix" style="display:block;">
							
							<!-- 邮件左侧操作栏start -->
							<div class="receiveLeft">
								<ul name="leftTab">
									<li name="received" onclick="mailOpt('received',this);" class="on" style="cursor:pointer;"><img class="mail-img" src="/${projectName}/book/img/icon/icon-email.png">收件箱</li>
									<li name="draft" onclick="mailOpt('draft',this);" style="cursor:pointer;"><img class="mail-img" src="/${projectName}/book/img/icon/icon-caogao.png">草稿箱</li>
									<li name="unread" onclick="mailOpt('unread',this);" style="cursor:pointer;"><img class="mail-img" src="/${projectName}/book/img/icon/icon-weidu.png">未读邮件</li>
									<li name="hasSend" onclick="mailOpt('hasSend',this)" style="cursor:pointer;"><img class="mail-img" src="/${projectName}/book/img/icon/icon-weifasong.png"> 已发送邮件</li>
									<li name="dustbin" onclick="mailOpt('dustbin',this)" style="cursor:pointer;"><img class="mail-img" src="/${projectName}/book/img/icon/icon-delyou.png">已删除邮件</li>
								</ul>
							</div>
							<!-- 邮件左侧操作栏end -->
							
							<!-- 邮件列表部分 -->
							<div name="list" >
								<div class="receiveCenter addresser">
									<div class="search">									  
										<input type="text" placeholder="搜索"  value="" />	
										<i onclick=" schByTitle(this);" class="ser-icon icon-search"></i>									
									</div>
									<div class="addresser-list">
										
																	
									</div>
								</div>
								<div name="mailShow" class="receiveRight addresser">
									<div class="addresser-btns">
										<span onClick="preMail();" class="icon-left" title="上一封"></span>
										<span onClick="forwardMail();" class="icon-fh" title="转发"></span>
										<span onclick="deleteOne();" class="icon-remove" title="删除"></span>
										<span onClick="nextMail();" class="icon-right" title="下一封"></span>
									</div>
									<dl>
										<dd class="mail-title" name="detailTitle"></dd>
										<dt>
											<label name="detailSendNum" class="ellipsis"></label>
											<label name="duplicate" class="ellipsis"></label>
											<span name="detailReceiveTime" claass="ellipsis"></span>
										</dt>									
										<div name="fileShowDiv" > </div>
									</dl>
									<div name="contentShow" style="padding:20px;" >
									
									</div>								
								</div>
							</div>
							<!-- 邮件列表部分end -->
							
							<!-- 邮件编辑部分start -->
							<div name="edit" style="display:none;">
							   <div class="receiveRight write">
									<h5>联系人列表</h5>
									<select name="classSelect" style="width:100%;float:left;display:none;" onchange="chooseClass(this);" >
										<option value="" >请选择班级</option>
									</select>
									<div class="search">
										<i onclick="schStuByParam();" class="icon-search ser-icon" style="margin-top:10px;"></i>
										<input name="stuLisParam" type="text" placeholder="搜索学生列表" value="" />										
									</div>
									<ul id="contacts">
									
									</ul>
								</div>
								<div class="receiveCenter write">									
									<div name="fileDiv" style="display:none;">
										<form id="fileForm" action="" enctype="multipart/form-data">
											<input type="hidden" name="type" value="file" />
											<input type="file" name="fileInput" />
										</form>
										<form id="imgForm" action="" enctype="multipart/form-data">
											<input type="hidden" name="type" value="img" />
											<input type="file" name="imgInput" accept="image/*" />
										</form>
									</div>
									<div class="send-btn">										
										<span onclick="chooseFile();"><img src="/${projectName}/book/img/icon/icon-fujian.png">附件</span>
										<span onclick="chooseImg()"><img src="/${projectName}/book/img/icon/icon-img.png">图片</span>
										<span onclick="saveDraft();"><img src="/${projectName}/book/img/icon/baocun.png">保存草稿</span>
										<span name="sendBtn" class="send-fa" >发送</span>
									</div>
									<div class="compile-con">
									    <div name="fileNameShow" class="fileShow"></div>
										<div class="item">
											<label>收件人：</label>
											<!-- <input type="text" value="" readonly="readonly" /> --><!-- contenteditable -->
											<div id="receiveInDiv" onfocus="personType(this.id);" tabindex="0" contenteditable class="input"  placeholder="此处点击下，然后从左侧列表选取"></div>
										</div>
										<div class="item">
											<label>抄送人：</label>
											<!-- <input type="text" /> -->
											<div id="duplicateInDiv" onfocus="personType(this.id);" tabindex="0" contenteditable class="input"  placeholder="此处点击下，然后从左侧列表选取"></div>
										</div>
										<div class="item">
											<label>主题：</label>
											<input type="text" name="title" />
										</div>
										<div class="item">
											
											<label>正文：</label>
											<div class="container">
												<!--此处放富文本编辑 -->
												<script id="content" type="text/plain" style="width:100%;height:340px;"></script>
											</div>
										</div>
									</div>
									<!--<div class="send-btn">
										<span name="sendBtn">发送</span>
										<span>附件</span>
										<span>图片</span>
										<span>保存草稿</span>
									</div>  -->
								</div>								
							</div>
							<!-- 邮件编辑部分end -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<jsp:include page="_footer.jsp" />
<jsp:include page="./pop.jsp" />
<script type="text/javascript"	src="/${projectName}/book/js/common/login.js"></script>
</div>
<!-- loadDiv -->
<div id="loadDiv" style="display:none" >
	<span><img src="/${projectName}/book/img/loading.gif" />正在发送，请稍后...</span>
</div>
<!-- 保存草稿 -->
<div id="saveDraft" style="display:none" >
	<span><img src="/${projectName}/book/img/loading.gif" />正在保存至草稿，请稍后...</span>
</div>

<div class="black_overlay"></div>

<!-- 个性签名 -->
<div id="autographShow" style="display:none;top: 159px;margin-left: 28%;width: 800px;position: absolute;background: #fff;z-index: 1002;height: 570px;border-radius: 5px;">
	<div class="autographTitle" > 个性签名 </div>
	<script id="autograph" type="text/plain" style="width:100%;height:440px;"></script>
	<div class="buttonDiv" style="text-align:center">
		<button type="button" onclick="saveAutograph();" style="margin-right: 53px;margin-bottom: 10px;margin-top: 10px;border-width: 0px;">保存</button>
		<button type="button" onclick="closeAutograph();" style="">取消</button>
	</div>
</div>

</body>
<script>
var projectName = "${projectName}"
$(function(){
	var user = "${CURRENT_USER}";
	if(user == null || user == "" || typeof(user) == "undefined"){
		window.location.href= "/"+projectName+"/";
	}
	var state = "${CURRENT_USER.state}";
	/* if(state != '3' ){
		alert("老师暂未开放此功能");
		window.location.href= "/"+projectName+"/";
	} */
	
	// 初始化富文本
	initUE();
	// 绑定发送点击事件
	$("span[name='sendBtn']").click(function(){
		sendMail();
	});
	
});

// 选择附件
function chooseFile(){
	$("div[name='fileDiv']").find("input[name='fileInput']").trigger("click");
	
	$("#fileForm").find("input[name='fileInput']").unbind();
	$("#fileForm").find("input[name='fileInput']").change(function(){
		$("#fileForm").ajaxSubmit({
			url: '/${projectName}/mail/attac/uploadFile.action', 
			type: "post", 
			dataType: "json", 
			success: function (result) {
				if(result.success){
					var data = result.data;
					var html ="<span name='mailFileIn' onclick='removeFile(this);' style='cursor: pointer' data-id='"+data.id+"' >"+data.name+" ; </span>";
					$("div[name='fileNameShow']").append(html);
				}else{
					
				}
				
			},
		});
	});
}

// 选择图片
function chooseImg(){
	$("div[name='fileDiv']").find("input[name='imgInput']").trigger("click");
	
	$("#imgForm").find("input[name='imgInput']").unbind();
	$("#imgForm").find("input[name='imgInput']").change(function(){
		$("#imgForm").ajaxSubmit({
			url: '/${projectName}/mail/attac/uploadImg.action', 
			type: "post", 
			dataType: "json", 
			success: function (result) {
				if(result.success){
					var data = result.data;
					var html ="<span name='mailImgIn' onclick='removeFile(this);' style='cursor: pointer' data-id='"+data.id+"' >"+data.name+" ; </span>";
					$("div[name='fileNameShow']").append(html);
				}else{
					
				}
				
			},
		});
	});
}

// 删除当前
function removeFile(t){
	$(t).remove();
}

// 发送邮件
function sendMail(){
	// 收件人
	var receiveNumShow = "";
	$(".compile-con").find("#receiveInDiv").find("span").each(function(i,item){
		if(receiveNumShow == ""){
			receiveNumShow = $(this).find("input[type='hidden']").val();
		}else{
			receiveNumShow = receiveNumShow + "," + $(this).find("input[type='hidden']").val();
		}
	});
	// 抄送人
	var duplicateNumShow = "";
	$(".compile-con").find("#duplicateInDiv").find("span").each(function(i,item){
		if(duplicateNumShow == ""){
			duplicateNumShow = $(this).find("input[type='hidden']").val();
		}else{
			duplicateNumShow = duplicateNumShow + "," + $(this).find("input[type='hidden']").val();
		}
	});
	// 判断收件人是否为空
	if( receiveNumShow == "" ){
		swal({ title: "收件人不允许为空", type: "error"});
		return false;
	}
	// 邮件标题
	var title = $(".compile-con").find("input[name='title']").val();
	// 邮件内容
	var content = UE.getEditor('content').getContent();
	
	var fileId = "";// 附件id
	$("div[name='fileNameShow']").find("span[name='mailFileIn']").each(function(){
		if(fileId != ""){
			fileId = fileId + $(this).attr("data-id");
		}else{
			fileId = $(this).attr("data-id");
		}
	});
	
	var imgId = "";// 图片id
	$("div[name='fileNameShow']").find("span[name='mailImgIn']").each(function(){
		if(fileId != ""){
			imgId = imgId + $(this).attr("data-id");
		}else{
			imgId = $(this).attr("data-id");
		}
	});
	
	// 拼接参数
	var param = "receiveNumShow="+receiveNumShow+"&duplicateNumShow="+duplicateNumShow
				+"&title="+title+"&fileId="+fileId+"&imgId="+imgId;
	var obj = new Object();
	obj.contentJson=content;
	var aat;
	$.ajax({
		url : "/${projectName}/mail/mailSend/sendMail.action?"+param , 
		contentType : "application/json;charset=UTF-8",
		type : "post" , 
		data : JSON.stringify(obj) , 
		beforeSend:function(XMLHttpRequest){
			aat = art.dialog({opacity:0.7,
					title:"发送邮件",
					lock:true,
					content:$("#loadDiv").html(),
					});
		},
		success : function(result) {
			if(result.success){
				setTimeout(function(){ 
					aat.close();
					swal({ title: result.msg,
					type: "success"});
					sendOfAfter(result.success);
				}, 3000);
			}else{
				setTimeout(function(){ 
					aat.close();
					swal({ title: result.msg,
					type: "error"});
				}, 3000);
			}
		}
	})
}

// 保存至草稿
function saveDraft(){
	
	// 邮件标题
	var title = $(".compile-con").find("input[name='title']").val();
	// 邮件内容
	var content = UE.getEditor('content').getContent();
	// 拼接参数
	var param = "title="+title+"&content="+content;
	var aat;
	$.ajax({
		url : "/${projectName}/mail/mailSend/saveDraft.action" , 
		type : "post" , 
		data : param , 
		beforeSend:function(XMLHttpRequest){
			aat = art.dialog({opacity:0.7,
					title:"保存草稿",
					lock:true,
					content:$("#saveDraft").html(),
					});
		},
		success : function(result) {
			if(result.success){
				setTimeout(function(){ 
					aat.close();
					swal({ title: result.msg,
					type: "success"});
					//sendOfAfter(result.success);
				}, 1500);
			}else{
				setTimeout(function(){ 
					aat.close();
					swal({ title: result.msg,
					type: "error"});
				}, 1500);
			}
		}
	})
}

// 发送后
function sendOfAfter(status){
	if(status){// 发送成功的场合
		// 收件人
		$(".compile-con").find("#receiveInDiv").find("span").remove();
		// 抄送人
		$(".compile-con").find("#duplicateInDiv").find("span").remove();
		// 邮件标题
		$(".compile-con").find("input[name='title']").val("");
		// 邮件内容
		UE.getEditor('content').setContent("");
		// 跳转列表页面
		$("ul[name='topTab']").find('li').eq(0).trigger('click');
	}
}

// 初始化富文本
function initUE(){
	// 标准答案编辑器
	var ue2 = UE.getEditor('content', {
		toolbars : [ [
		'simpleupload','source', //源代码
		'undo', //撤销
		'redo', //重做
		'formatmatch', //格式刷
		'bold', //加粗
		'indent', //首行缩进
		'italic', //斜体
		'underline', //下划线
		'strikethrough', //删除线
		'justifyleft', //居左对齐
		'justifyright', //居右对齐
		'justifycenter', //居中对齐
		'justifyjustify', //两端对齐
		'fullscreen', //全屏
		'touppercase', //字母大写
		'tolowercase', //字母小写
		'forecolor', // 字体颜色
		'rowspacingtop',/*段前*/
		'rowspacingbottom',/*段前*/
		'lineheight',/*行间距*/
		'fontfamily', // 字体
		'fontsize', // 字体大小
		'inserttable', //插入表格
		'insertrow', //前插入行
		'insertcol', //前插入列
		'mergeright', //右合并单元格
		'mergedown', //下合并单元格
		'deleterow', //删除行
		'deletecol', //删除列
		'splittorows', //拆分成行
		'splittocols', //拆分成列
		'splittocells', //完全拆分单元格
		'deletecaption', //删除表格标题
		'inserttitle', //插入标题
		'mergecells', //合并多个单元格
		'deletetable', //删除表格
		'backcolor', //背景色
		] ] , autoHeightEnabled : false , autoFloatEnabled : true , elementPathEnabled : false
	});
	
	var ue3 = UE.getEditor('autograph', {
		toolbars : [ [
		'simpleupload','source', //源代码
		'undo', //撤销
		'redo', //重做
		'formatmatch', //格式刷
		'bold', //加粗
		'indent', //首行缩进
		'italic', //斜体
		'underline', //下划线
		'strikethrough', //删除线
		'justifyleft', //居左对齐
		'justifyright', //居右对齐
		'justifycenter', //居中对齐
		'justifyjustify', //两端对齐
		'fullscreen', //全屏
		'touppercase', //字母大写
		'tolowercase', //字母小写
		'forecolor', // 字体颜色
		'rowspacingtop',/*段前*/
		'rowspacingbottom',/*段前*/
		'lineheight',/*行间距*/
		'fontfamily', // 字体
		'fontsize', // 字体大小
		'inserttable', //插入表格
		'insertrow', //前插入行
		'insertcol', //前插入列
		'mergeright', //右合并单元格
		'mergedown', //下合并单元格
		'deleterow', //删除行
		'deletecol', //删除列
		'splittorows', //拆分成行
		'splittocols', //拆分成列
		'splittocells', //完全拆分单元格
		'deletecaption', //删除表格标题
		'inserttitle', //插入标题
		'mergecells', //合并多个单元格
		'deletetable', //删除表格
		'backcolor', //背景色
		] ], autoHeightEnabled : false , autoFloatEnabled : true , elementPathEnabled : false
	});
	
	UE.Editor.prototype._bkGetActionUrl = UE.Editor.prototype.getActionUrl;  
	UE.Editor.prototype.getActionUrl = function(action){  
		if(action == 'uploadimage' || action == 'uploadscrawl' || action == 'uploadimage'){  
			return '/${projectName}/uploaditemimage.action';  
		}else{  
			return this._bkGetActionUrl.call(this, action);  
		}
	}
	
	ue2.ready(function() {
		$("ul[name='topTab']").find('li').eq(0).trigger('click');
		$(".edui-editor-messageholder.edui-default").css({ "visibility": "hidden" });// 关闭自动保存提示
	});
	ue3.ready(function () {
		$(".edui-editor-messageholder.edui-default").css({ "visibility": "hidden" });// 关闭自动保存提示
	});
}

mailSocket.onmessage = function (event) {
	art.dialog.notice({
		title: '提示',
		width: 300,// 必须指定一个像素宽度值或者百分比，否则浏览器窗口改变可能导致artDialog收缩
		content: event.data,
		time: 5
	});
	$("ul[name='leftTab']").find("li[name='"+leftOpt+"']").trigger('click');
}

//================横向主导航部分start===================//

//邮件切换为列表
function mailList(t){
	changeShow(t,"hit");
	$(".pane.clearfix").find("div[name='list']").show();
	$(".pane.clearfix").find("div[name='edit']").hide();
	$("ul[name='leftTab']").find("li").eq(0).trigger("click");
	
	// 添加置灰效果
	$(".assist span").each(function(){
		$(this).removeClass("disable");
	});
	clearDetail();
	
}

//邮件切换为编辑
function mailEdit(t){
	changeShow(t,"hit");
	
	var state = "${CURRENT_USER.state}";
	if(state == null || typeof(state) == "undefined" ){
		alert("登录超时，请重新登录")
		window.location.href= "/"+projectName+"/";
	}
	if(state == "3"){
		var cnumber = "${CURRENT_USER.cnumber}";
		getStuList(cnumber);
	}
	if(state == "2" || state == "x"){
		initClass();
		$("select[name='classSelect']").show();
	}
	
	$(".pane.clearfix").find("div[name='list']").hide();
	$(".pane.clearfix").find("div[name='edit']").show();
	// 添加置灰效果
	$(".assist span").each(function(){
		$(this).addClass("disable");
	});
	
	$.ajax({
		url : "/${projectName}/mail/autograph/getCurentAutograph.action" , 
		type : "post" , 
		data : {} , 
		async: false,
		success : function(result) { 
			if(result.success){
				var entity = result.data;
				if(entity != null){
					var con = UE.getEditor('content').getContent();
					con = con +entity.content;
					UE.getEditor('content').setContent(con);
				}
			}
		}
	});
}

//初始化班级
function initClass(){
	$("select[name='classSelect']").html("");
	$.ajax({
		url:"/${projectName}/mail/message/getClassByUser.action" ,
		type : "post" , 
		data : {} ,
		success : function(result) {
			if( result.success ){
				var data = result.data;
				if(data != null){
					var opt = "<option value=''>请选择</option>"
					for( var i = 0; i < data.length; i++ ){
						opt += "<option value='"+data[i].id+"'>"+data[i].label+"</option>";
					}
					$("select[name='classSelect']").html(opt);
				}else{
				}
			}else{
			}
		}
	});
}

//选择班级时触发
function chooseClass(t){
	
	var cla = $(t).val();
	if(cla != ""){
		getStuList(cla);
	}
}

//切换选中状态的共通方法
function changeShow(t,cla){
	var isChoose = $(t).attr("class");
	if(isChoose == null || typeof(isChoose) == "undefined"){
		isChoose = "";
	}
	
	if( isChoose.indexOf(cla) == -1){
		$(t).parent("ul").find("li").each(function(){
			$(this).removeClass(cla);
		});
		$(t).addClass(cla);
	}
}

var mailParam;// 查询列表参数
var leftOpt;// 左侧操作
function mailOpt(opt,t){
	// 切换选中状态
	changeShow(t,"on");
	// 清空邮件详情信息
	clearDetail();
	
	leftOpt = opt;
	if(opt == "received"){// 收件箱
		mailParam = new Object();
		var url = "/${projectName}/mail/mailReceive/schMailReceiveList.action";
		mailParam.isDelete = "0";// 删除状态：未删除
		serchMailList(url);// 查询邮件列表
	}
	if(opt == "draft"){// 草稿箱
		mailParam = new Object();
		var url = "/${projectName}/mail/mailSend/schMailSendList.action";
		mailParam.isSend = "0";// 发送状态：未发送
		serchMailList(url);// 查询邮件列表
	}
	if(opt == "unread"){// 未读邮件
		mailParam = new Object();
		var url = "/${projectName}/mail/mailReceive/schMailReceiveList.action";
		mailParam.isDelete = "0";// 删除状态：未删除
		mailParam.isRead = "0";// 已读状态：未读
		serchMailList(url);// 查询邮件列表
	}
	if(opt == "hasSend"){// 已发邮件
		mailParam = new Object();
		var url = "/${projectName}/mail/mailSend/schMailSendList.action";
		mailParam.isSend = "1";// 发送状态：未发送
		serchMailList(url);// 查询邮件列表
	}
	if(opt == "dustbin"){// 已删除邮件
		mailParam = new Object();
		var url = "/${projectName}/mail/mailReceive/schMailReceiveList.action";
		mailParam.isDelete = "1";// 删除状态：已删除
		serchMailList(url);// 查询邮件列表
	}
}

// 搜索邮件
function schByTitle(t){
	
	if(leftOpt == "received"){// 收件箱
		mailParam = new Object();
		var url = "/${projectName}/mail/mailReceive/schMailReceiveList.action";
		mailParam.isDelete = "0";// 删除状态：未删除
		mailParam.title = $(t).parent().find("input").val();// title设置
		serchMailList(url);// 查询邮件列表
	}
	if(leftOpt == "draft"){// 草稿箱
		mailParam = new Object();
		var url = "/${projectName}/mail/mailSend/schMailSendList.action";
		mailParam.isSend = "0";// 发送状态：未发送
		mailParam.title = $(t).parent().find("input").val();// title设置
		serchMailList(url);// 查询邮件列表
	}
	if(leftOpt == "unread"){// 未读邮件
		mailParam = new Object();
		var url = "/${projectName}/mail/mailReceive/schMailReceiveList.action";
		mailParam.isDelete = "0";// 删除状态：未删除
		mailParam.isRead = "0";// 已读状态：未读
		mailParam.title = $(t).parent().find("input").val();// title设置
		serchMailList(url);// 查询邮件列表
	}
	if(leftOpt == "hasSend"){// 已发邮件
		mailParam = new Object();
		var url = "/${projectName}/mail/mailSend/schMailSendList.action";
		mailParam.isSend = "1";// 发送状态：已发送
		mailParam.title = $(t).parent().find("input").val();// title设置
		serchMailList(url);// 查询邮件列表
	}
	if(leftOpt == "dustbin"){// 已删除邮件
		mailParam = new Object();
		var url = "/${projectName}/mail/mailReceive/schMailReceiveList.action";
		mailParam.isDelete = "1";// 删除状态：已删除
		mailParam.title = $(t).parent().find("input").val();// title设置
		serchMailList(url);// 查询邮件列表
	}
}

//对Date的扩展，将 Date 转化为指定格式的String  
//月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，   
//年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)   
Date.prototype.Format = function (fmt) { //author: meizz   
	var o = {  
			"M+": this.getMonth() + 1, //月份   
			"d+": this.getDate(), //日   
			"H+": this.getHours(), //小时   
			"m+": this.getMinutes(), //分   
			"s+": this.getSeconds(), //秒   
			"q+": Math.floor((this.getMonth() + 3) / 3), //季度   
			"S": this.getMilliseconds() //毫秒   
		};  
	if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));  
	for (var k in o)  
	if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));  
	return fmt;  
}

// 查询邮件列表
var mailArray 
function serchMailList(url){
	mailArray = new Array();
	$.ajax({
		url : url ,
		contentType : "application/json;charset=UTF-8",
		dataType: 'json',
		type : "post" , 
		data : JSON.stringify(mailParam) , 
		success : function(result) {
			if(result.success){
				var mails = result.data;
				$(".addresser-list").html("");
				var html = "";
				if(mails != null && mails.length > 0 ){
					for(var i = 0; i < mails.length; i++){
						var mail = mails[i];
						mailArray[i] = mail;
						var time ;
						var name = "";
						if(leftOpt == "hasSend" || leftOpt == "draft" ){
							time = mail.sendTime;
							if(mail.receiveNumShow != null && typeof(mail.receiveNumShow) != "undefined"){
								name = mail.receiveNumShow;
							}
						}else{
							time = mail.receiveTime;
							name = mail.sendName;
						}
						if(time != null){
							time = new Date(time).Format("HH:mm");
						}
						html = html + "<dl onclick=\"showMailDetail('"+i+"',this)\" style='cursor:pointer;'>"
										+"<dt>"
											+"<div class='my_protocol'>"
										    +"<input onclick='singleMial();' data-index='"+i+"' name='mailBox' type='checkbox' value='"+mail.id+"' />"
											+"<i class='input-i'></i></div><label class='ellipsis'>"+name+"</label>"
											+"<span class='ellipsis2'>"+time+"</span>"
											+"</dt>";
										if(mail.isRead == "1"){
											html += "<dd><span ></span> "+mail.title+"</dd>";
										}else{
											html += "<dd><span class='unread'></span> "+mail.title+"</dd>";
										}
										html += "</dl>";
					}
					$(".addresser-list").html(html);
				}else{
					html = "<span style='margin-left:100px;'> <无内容> </span>"
					$(".addresser-list").html(html);
				}
			}else{
				art.dialog.notice({
					title: '提示',
					width: 300,// 必须指定一个像素宽度值或者百分比，否则浏览器窗口改变可能导致artDialog收缩
					content:result.msg,
					time: 5
				});
			}
		}
	});
}

var index ;
// 显示邮件详情页
function showMailDetail(i,t){
	
	// 切换左侧导航
	$(t).parent("div").find("dl").each(function(){
		$(this).removeClass("on");
	});
	$(t).addClass("on");
	
	// 显示前先清空内容
	clearDetail();
	
	// 显示邮件详情
	index = i;
	if(mailArray != null && mailArray[i] != null){
		var mail = mailArray[i];
		$("div[name='mailShow']").find("label[name='detailSendNum']").html("<span class='mail-span'>收件人：</span>"+mail.receiveNumShow);
		$("div[name='mailShow']").find("label[name='duplicate']").html("<span class='mail-span'>抄送人：</span>"+mail.duplicateNumShow);
		$("div[name='mailShow']").find("span[name='detailReceiveTime']").html("<span class='mail-span'>时间：</span>"+mail.createTime);
		$("div[name='mailShow']").find("dd[name='detailTitle']").html(mail.title);
		$("div[name='mailShow']").find("div[name='contentShow']").html(mail.content);
		$("div[name='mailShow']").find("div[name='contentShow']").find("p").css("background-color","");
		if( mail.isRead == 0 ){
			$.ajax({
				url : "/${projectName}/mail/mailReceive/readMail.action" , 
				type : "post" , 
				data : {id:mail.id} , 
				success : function(result) {
					if(result.success){
						$(t).find("dd").find("span").removeClass("unread");
						mailArray[i].isRead=result.data.isRead;
					}
				}
			});
		}
		showFile(i);
	}
}

// 显示file或img
function showFile(i){
	// 清空附件图片区域
	$("div[name='fileShowDiv']").html("");
	
	var mail = mailArray[i];
	var fileId = mail.fileId;
	var fileIdArg = new Array();
	if( fileId != null ){
		fileIdArg = fileId.split(",");
	}
	var imgId = mail.imgId;
	var imgIdArg = new Array();
	if(imgId != null){
		imgIdArg = imgId.split(",");
	}
	var ids = new Array();
	if(fileIdArg != null){
		ids = ids.concat(fileIdArg);
	}
	if(imgIdArg != null){
		ids = ids.concat(imgIdArg);
	}
	
	$.ajax({
		url : "/${projectName}/mail/attac/getAttacList.action" , 
		contentType : "application/json;charset=UTF-8",
		type : "post" , 
		data : JSON.stringify(ids) , 
		success : function(result) {
			if(result.success){
				var data = result.data;
				if(data != null && data.length > 0){
					$("div[name='fileShowDiv']").append("<span class='mail-span'>附件：</span>");
					for(var i = 0; i < data.length ;i++){
						var entity = data[i];
						var html = "<span onclick=\"download('"+entity.url+"','"+entity.type+"','"+entity.id+"');\" style='cursor: pointer' >"+entity.name+" ; </span>";
						$("div[name='fileShowDiv']").append(html);
						
					}
				}
			}
		}
	})
}

// 下载文件
function download(url,type,id){
	window.open("/${projectName}/mail/attac/download.action?id="+id);
}

// 选择单个邮件
function singleMial(){
	// 阻止冒泡
	var oEvent = event;
	oEvent.cancelBubble = true;
	
	var len = $("input[name='mailBox']:checked").length;
	if(len > 1){
		$(".assist span:gt(0)").each(function(){
			$(this).addClass("disable");
		});
	}else{
		$(".assist span:gt(0)").each(function(){
			$(this).removeClass("disable");
		});
	}
}

// 清空邮件详情
function clearDetail(){
	$("div[name='mailShow']").find("label[name='detailSendNum']").html("");
	$("div[name='mailShow']").find("label[name='duplicate']").html("");
	$("div[name='mailShow']").find("span[name='detailReceiveTime']").html("");
	$("div[name='mailShow']").find("dd[name='detailTitle']").html("");
	$("div[name='mailShow']").find("div[name='contentShow']").html("");
	$("div[name='fileShowDiv']").html("");
	UE.getEditor('content').setContent("");
}

// 前一封邮件
function preMail(){
	if(index != null){
		if( index == 0 ){
			art.dialog.tips("已经是第一条数据了",2);
		}
		if(index > 0){
			index--;
			$(".addresser-list").find("dl").eq(index).trigger('click');
		}
	}
}

//前一封邮件
function nextMail(){
	var maxlen = $(".addresser-list").find("dl").length;// 列表长度
	if(maxlen > 0 && index != null ){
		maxlen--
		if( index == maxlen ){
			art.dialog.tips("已经是最后一条数据了",2);
		}
		if(index < maxlen){
			index++;
			$(".addresser-list").find("dl").eq(index).trigger('click');
		}
	}
}

// 内容中的转发
function forwardMail(){
	$("div[name='fileShowDiv']").html("");
	if(index != null && index >= 0){
		if(mailArray != null){
			var mail = mailArray[index];
			$(".compile-con").find("input[name='title']").val(mail.title);
			UE.getEditor('content').setContent(mail.content);
			$("ul[name='topTab']").find('li').eq(1).trigger('click');
			forwordFile(index);
		}
	}
}

// top上的转发
function topForWrdMail(){
	$("div[name='fileShowDiv']").html("");
	var len = $("input[name='mailBox']:checked").length;
	if(len ==1){
		var i = $("input[name='mailBox']:checked").attr("data-index");
		var mail = mailArray[i];
		$(".compile-con").find("input[name='title']").val(mail.title);
		UE.getEditor('content').setContent(mail.content);
		$("ul[name='topTab']").find('li').eq(1).trigger('click');
		forwordFile(i);
	}else{
		art.dialog.notice({
			title: '提示',
			width: 300,// 必须指定一个像素宽度值或者百分比，否则浏览器窗口改变可能导致artDialog收缩
			content:"请勾选要转发的一个邮件（单选）！",
			time: 3
		});
	}
}

// 转发时文件操作
function forwordFile(i){
	var mail = mailArray[i];
	var fileId = mail.fileId;
	var fileIdArg = new Array();
	if( fileId != null ){
		fileIdArg = fileId.split(",");
	}
	var imgId = mail.imgId;
	var imgIdArg = new Array();
	if(imgId != null){
		imgIdArg = imgId.split(",");
	}
	var ids = new Array();
	if(fileIdArg != null){
		ids = ids.concat(fileIdArg);
	}
	if(imgIdArg != null){
		ids = ids.concat(imgIdArg);
	}
	
	$.ajax({
		url : "/${projectName}/mail/attac/getAttacList.action" , 
		contentType : "application/json;charset=UTF-8",
		type : "post" , 
		data : JSON.stringify(ids) , 
		success : function(result) {
			if(result.success){
				var data = result.data;
				if(data != null && data.length > 0){
					$("div[name='fileShowDiv']").append("<span>附件及图片：</span>");
					for(var i = 0; i < data.length ;i++){
						var entity = data[i];
						var html ="<span name='mailFileIn' onclick='removeFile(this);' style='cursor: pointer' data-id='"+entity.id+"' >"+entity.name+" ; </span>";
						$("div[name='fileNameShow']").append(html);
					}
				}
			}
		}
	})
}

// 删除数据(单条)
function deleteOne(){
	// 显示前先清空内容
	clearDetail();
	if(leftOpt != null ){
		var url = "";
		if(leftOpt == "received" || leftOpt == "unread" ||leftOpt == "dustbin" ){
			url = "/${projectName}/mail/mailReceive/recycleOrDelete.action";
		}
		if(leftOpt == "hasSend" || leftOpt == "draft" ){
			url = "/${projectName}/mail/mailSend/deleteMailSends.action";
		}
		
		var mail = mailArray[index];
		if(url != "" && mail != null){
			var ids = new Array();
			// 获取邮件缓存数据
			ids[0]=mail.id
			deleteMails(url,ids);
		}else{
			swal({ title: "删除失败，请刷新后再试",
				type: "error"});
		}
	}
}

// 删除多条邮件
function deleteMore(){
	if(leftOpt != null ){
		var url = "";
		if(leftOpt == "received" || leftOpt == "unread" ||leftOpt == "dustbin" ){
			url = "/${projectName}/mail/mailReceive/recycleOrDelete.action";
		}
		if(leftOpt == "hasSend" || leftOpt == "draft" ){
			url = "/${projectName}/mail/mailSend/deleteMailSends.action";
		}
		
		var ids = new Array();
		$(".addresser-list").find("dl").each(function(i,item){
			var box = $(this).find("dt").find("input[type='checkbox']");
			if($(box).is(':checked')){
				ids.push($(box).val());
			}
		});
		// 判断勾选的邮件数
		if(ids.length == 0 ){
			var len = $(".addresser-list").find("dl").length;
			if(len > 0){
				art.dialog.notice({
					title: '提示',
					width: 300,// 必须指定一个像素宽度值或者百分比，否则浏览器窗口改变可能导致artDialog收缩
					content:"请先勾邮件！",
					time: 3
				});
			}
			return false;
		}
		deleteMails(url ,ids);
	}
}

// 删除邮件
function deleteMails(url , ids){
	$.ajax({
		url : url , 
		contentType : "application/json;charset=UTF-8",
		type : "post" , 
		data : JSON.stringify(ids) , 
		success : function(result) {
			if(result.success){
				swal({ title: result.msg,
					type: "success"});
				index = null;
				$("ul[name='leftTab']").find("li[name='"+leftOpt+"']").trigger('click');
			}else{
				swal({ title: result.msg,
					type: "success"});
			}
		}
	});
}

//置顶
function setTop(){
	if(leftOpt != null ){
		var url = "";
		if(leftOpt == "received" || leftOpt == "unread" ||leftOpt == "dustbin" ){
			url = "/${projectName}/mail/mailReceive/setTop.action";
		}
		if(leftOpt == "hasSend" || leftOpt == "draft" ){
			url = "/${projectName}/mail/mailSend/setTop.action";
		}
		
		var ids = new Array();
		$(".addresser-list").find("dl").each(function(i,item){
			var box = $(this).find("dt").find("input[type='checkbox']");
			if($(box).is(':checked')){
				ids.push($(box).val());
			}
		});
		// 判断勾选的邮件数
		if(ids.length == 1 ){
			$.ajax({
				url : url , 
				contentType : "application/json;charset=UTF-8",
				type : "post" , 
				data : JSON.stringify(ids) , 
				success : function(result) {
					if(result.success){
						swal({ title: result.msg,
							type: "success"});
						$("ul[name='leftTab']").find("li[name='"+leftOpt+"']").trigger('click');
					}else{
						swal({ title: result.msg,
							type: "error"});
					}
				}
			});
		}
		if(ids.length != 1){
			art.dialog.notice({
				title: '提示',
				width: 300,// 必须指定一个像素宽度值或者百分比，否则浏览器窗口改变可能导致artDialog收缩
				content:"请勾选要置顶的一个邮件（单选）！",
				time: 3
			});
		}
	}
}

// 获取学生列表
function getStuList(cnumber){
	
	
	$("#contacts").html("");
	
	
	$.ajax({
		url : "/${projectName}/manager/stud/getAllStudByMail.action" , 
		type : "post" , 
		data : {"cnumber":cnumber} , 
		success : function(result) {
			if(result.success){
				var html = "";
				var data = result.data;
				if(data != null && data.length > 0){
					for(var i=0;i < data.length; i++){
						var stu = data[i];
						html += "<li onclick=\"addPerson('"+stu.number+"','"+stu.name+"');\"><span class='yuanxing'></span>"+stu.name+"<"+stu.number+"></li>";
					}
				}
				$("#contacts").html(html);
			}else{
				swal({ title: result.msg,
					type: "error"});
			}
		}
	});
}

function schStuByParam(){
	var param = $("input[name='stuLisParam']").val();
	var cnumber = ""; // 班级号
	var state = "${CURRENT_USER.state}";
	if(state == null || typeof(state) == "undefined" ){
		alert("登录超时，请重新登录")
		window.location.href= "/"+projectName+"/";
	}
	if(state == "3"){
		cnumber = "${CURRENT_USER.cnumber}";
		getStuList(cnumber);
	}
	if(state == "2" || state == "x"){
		cnumber = $("select[name='classSelect']").val();
	}
	
	$.ajax({
		url : "/${projectName}/manager/stud/getAllStudByMail.action" , 
		type : "post" , 
		data : {"name":param,"number":param,"cnumber":cnumber} , 
		success : function(result) {
			if(result.success){
				var html = "";
				var data = result.data;
				if(data != null && data.length > 0){
					for(var i=0;i < data.length; i++){
						var stu = data[i];
						html += "<li onclick=\"addPerson('"+stu.number+"','"+stu.name+"');\">"+stu.name+"<"+stu.number+"></li>";
					}
				}
				$("#contacts").html(html);
			}else{
				swal({ title: result.msg,
					type: "error"});
			}
		}
	});
}



// 选择接收人或者抄送人
var inputId ;
function addPerson(number,name){
	var flg = true;
	$("#"+inputId).find("span").each(function(i,item){
		var spId = $(this).attr("id");
		if(number == spId){
			flg= false;
		}
	});
	if(flg){
		var but = "<span id='"+number+"'> <button style=\"background-color: #ffffff;border: 0px;height: 28px; \" type=\"button\" onclick=\"removeThis(this);\">"+name+"<"+number+"@edu.com>; </button>"
					+"<input type='hidden' name='receiveNum' value='"+number+"' /> "
				+"</span>";
		$("#"+inputId).append(but);
	}
}

// 删除本元素
function removeThis(t){
	$(t).parent("span").remove();
}

// 区分是收件人还是抄送人
function personType(id){
	inputId = id;
}

// 弹出个性签名页面
function setAutograph(t){
	var content = "";
	$.ajax({
		url : "/${projectName}/mail/autograph/getCurentAutograph.action" , 
		type : "post" , 
		data : {} , 
		async: false,
		success : function(result) { 
			if(result.success){
				var entity = result.data;
				if(entity != null){
					content = entity.content;
					UE.getEditor('autograph').setContent(content);
				}
			}
		}
	});
	
	var disable = $(t).attr("class");
	if( disable != "disable" ){
		$('#autographShow').show();
		$(".black_overlay").show();
	}else{
		swal({ title: "编辑页面禁止修改签名",
			type: "error"});
	} 
}

// 保存签名
function saveAutograph(){
	
	var content = UE.getEditor('autograph').getContent();
	$.ajax({
		url : "/${projectName}/mail/autograph/saveAutograph.action" , 
		type : "post" , 
		data : {"content":content} , 
		success : function(result) { 
			if(result.success){
				closeAutograph();
				swal({ title: result.msg,
					type: "success"});
			}else{
				t.close();
				swal({ title: result.msg,
					type: "error"});
			}
		}
	});
}

function closeAutograph(){
	$('#autographShow').hide();
	$(".black_overlay").hide();
}

</script>
</html>
