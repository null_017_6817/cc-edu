<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<!-- cus -->
<!-- 
<div id="callDiv" class="white_content" style="left:40%;width:auto;height:auto;padding: 20px 6px 0;display: none;position:fixed;" scroll="no">
<a style="position: absolute;top: -2px;right: 6px;" href="javascript:closeCall();">关闭</a>
	<iframe id="callIframe" name="callIframe" data-url="https://192.168.1.199:8080/demo.html" src=""
				style="width:290px;height: 488px;padding: 0; margin: 0px;" frameborder="no" scroll="no"></iframe> 
</div>
-->
<style>
#cusInfoForm .search-data input,
#billAddForm .search-data input,
#billTable .search-data input,select
#cusInfoForm .search-data select {
	border: 1px #ccc solid;
}
</style>
<!-----客户打电话弹出部分--开始-->
<div id="cusOrderDiv" class="white_content">
	<h5>客户信息</h5>
	<form id="orderForm" action="" method="post">
		<input type="hidden" name="id" />
		<input type="hidden" name="userid" />
		<div align="center" style="padding: 20px 0;display:none;">
			行业 : <select name="profession" style="width: 162px; height: 32px;">
				<option value="">请选择则</option>
				<!-- <option value="1">航空</option>
				<option value="2">物流</option>
				<option value="3">电信</option> -->
			</select>
		</div>
		<table align="center" class="search-data" id="dataTable" style="">
			<tr>
				<td><label>姓名 :</label> <input type="text" name="name" value="" /></td>
				<td><label>性别 :</label> 
					<select name="sex" style="width: 162px; height: 32px;">
						<option value="">请选择则</option>
						<option value="1">先生</option>
						<option value="2">女士</option>
					</select>
				</td>
			</tr>
			<tr>
				<td><label>电话 :</label> <input type="text" name="telphone" value="" /></td>
				<td><label>公司 :</label> <input type="text" name="company" value="" /></td>
			</tr>
			<tr>
				<td><label>邮箱 :</label> <input type="text" name="email" value="" /></td>
				<td><label>邮编 :</label> <input type="text" name="zipcode" value="" /></td>
			</tr>
			<tr>
				<td><label>证件类型 :</label> 
					<select name="certType" style="width: 162px; height: 32px;">
						<option value="">请选择则</option>
						<option value="1">身份证</option>
						<option value="2">护照</option>
						<option value="3">军官证</option>
						<option value="4">士兵证</option>
						<option value="5">港澳居民来往内地通行证</option>
						<option value="6">台湾居民来往大陆通行证</option>
					</select>
				</td>
				<td><label>证件号码 :</label> <input type="text" name="certNum" value="" /></td>
			</tr>
			<tr>
				<td colspan="2"><label>产品归属地 :</label> <input type="text" name="proBelong" value="" style="width:452px;" /></td>
			</tr>
			<tr>
				<td colspan="2"><label>地址 :</label> <input type="text" name="addr" value="" style="width:452px;" /></td>
			</tr>
			<tr>
				<td colspan="2"><label>归属地 :</label> <input type="text" name="belong" value="" style="width:452px;" /></td>
			</tr>
		</table>
	</form>
	<div align="center" class="operateBtn">
		<button type="button" id="saveId">保存</button>
		<button type="button" id="cusCloseId">关闭</button>
	</div>
</div>
<!-----客户打电话弹出部分--结束-->
	
<!---- 客服接电话弹出部分--开始 -->
<div id="serOrderDiv" class="Ser_white_content">
	<h5>工单</h5>
	<a id="serCloseId" title="关闭" href="javascript:void(0)"><img style="width:15px;height:15px;" src='/${projectName}/book/img/closeOrder.png' /></a>
	<a id="serMinId" title="最小化" href="javascript:void(0)" onclick="minDiv()" ><img style="width:15px;height:15px;" src='/${projectName}/book/img/minOrder.png' /></a>
	<a id="serMaxId" title="恢复窗口" href="javascript:void(0)" onclick="maxDiv()" ><img style="width:15px;height:15px;" src='/${projectName}/book/img/maxOrder.png' /></a>
	
	<div style="height: 560px; overflow: auto;">
		<form id="cusInfoForm">
			<span class="small-title">客户信息</span>
			<table id="cusTable" class="search-data" >
				<input type="hidden" name="id" />
				<input type="hidden" name="userid" />
				<tr>
					<td colspan="3" style="display:none;"><label>行业 :</label><select name="profession">
							<option value="">请选择则</option>
							<!-- <option value="1">航空</option>
							<option value="2">物流</option>
							<option value="3">电信</option> -->
					</select>
					<span></span>
					</td>
				</tr>
				<tr>
					<td ><label>客户姓名 :</label><input type="text" name="name" value="" /></td>
					<td ><label>客户性别 :</label>
						<select name="sex">
							<option value="">请选择则</option>
							<option value="1">先生</option>
							<option value="2">女士</option>
						</select>
					</td>
					<td ><label>归属地 :</label><input type="text" name="belong" value="" /></td>
				</tr>
				<tr>
					<td><label>联系电话 :</label><input type="text" name="telphone" value="" /></td>
					<td><label>电子邮箱 :</label><input type="text" name="email" value="" /></td>
					<td><label>联系地址 :</label><input type="text" name="addr" value="" /></td>
				</tr>
				
				<tr>
					<td>
						<label>证件类型 :</label><select name="certType" style="width: 162px; height: 32px;">
							<option value="">请选择则</option>
							<option value="1">身份证</option>
							<option value="2">护照</option>
							<option value="3">军官证</option>
							<option value="4">士兵证</option>
							<option value="5">港澳居民来往内地通行证</option>
							<option value="6">台湾居民来往大陆通行证</option>
						</select>
					</td>
					<td><label>证件号码 :</label><input type="text" name="certNum" value="" /></td>
					<td><label>邮政编码 :</label><input type="text" name="zipcode" value="" /></td>
				</tr>
				<tr>
					<td><label>客户编号 :</label><input type="text" name="cusNum" value="" /></td>
					<td><label>产品归属 :</label><input type="text" name="proBelong" value="" /></td>
					<td><label>客户级别 :</label><select name="cusLevel">
							<option value="">请选择则</option>
							<option value="1">普通</option>
							<option value="2">VIP客户</option>
							<option value="3">银卡</option>
							<option value="4">金卡</option>
							<option value="5">钻卡</option>
							<option value="6">集团客户</option>
						</select>
					</td>
				</tr>
			</table>
			<div style="text-align: right;">
				<button type="button" name="cusInfoBtn" onclick="saveCusInfo();">保存</button>
			</div>
		</form>
		
		<form id="billAddForm">
			<span class="small-title">工单信息</span>
			<table id="addBillTable" class="search-data">
				<tr>
					<td><label>单号 :</label><input type="text" readonly="readonly" name="number" value="" /></td>
					<td><label>电话 :</label><input type="text" readonly="readonly" name="contact" value="" /></td>
					<td><label>来电时间 :</label><input type="text" name="tmstr" readonly="readonly" value="" /></td>
				</tr>
				<tr>
					<td><label>受理人 :</label><input type="text" name="userid" readonly="readonly" value="" /></td>
					<td><label>受理方式 :</label><input type="text" name="acceptType" value="" /></td>
					<td><label>归属部门 :</label><input type="text" name="department" value="" /></td>
				</tr>
				<tr>
					<td><label>工单类型 :</label><input type="text" name="business" text-data="工单类型" class="required" value="" /><font color="red">*</font></td>
					<td><label>一级目录 :</label><input type="text" name="directoryFir" value="" /></td>
					<td><label>二级目录 :</label><input type="text" name="directorySec" value="" /></td>
				</tr>
				<tr>
					<td colspan="3"><label>问题描述 :</label><input style="width: 88%;" text-data="问题描述" class="required" type="text" name="descrip" value="" /><font color="red">*</font></td>
				</tr>
				<tr>
					<td colspan="3"><label>处理结果 :</label><input style="width: 88%;" text-data="处理结果" class="required" type="text" name="result" value="" /><font color="red">*</font></td>
				</tr>
				<tr>
					<td colspan="3"><label>工单分配 :</label><input style="width: 89%;" type="text" name="orderAllot" value="" /></td>
				</tr>
			</table>
			<div style="text-align: right;">
				<button type="button" id="saveBillBtn" onclick="saveBill();">保存</button>
			</div>
		</form>
		
		
		<span class="small-title">记录信息</span>
		<div style="padding: 10px;">
			<table id="billTable" style="text-align:center;width:100%;padding: 10px">
				<thead>
					<tr>
						<th>工单号码</th>
						<th>来电日期</th>
						<th>来电时间</th>
						<th>工单类型</th>
						<th>所属部门</th>
						<th>受理人</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
	
			<div class="operateBtn">
				<!-- <button type="button" id="add">添加</button> -->
				<!-- <button type="button" id="hungId">挂断</button> -->
			</div>
		</div>
	</div>
</div>
<!-- 客服接电话弹出部分--结束 -->

<!-- 工单数据存储 开始 -->
<div id="copyDiv" style="display: none;">
	<table id="temp" class="search-data search-data-tab">
		<tr>
			<td align="left" style="padding-left:10px;"><strong>单号：</strong><span name="number" ></span></td>
			<td align="left" style="padding-left:10px;"><strong>电话：</strong><span name="contact" ></span></td>
			<td align="left" style="padding-left:10px;"><strong>来电时间：</strong><span name="tmstr" ></span></td>
		</tr>
		<tr>
			<td align="left" style="padding-left:10px;"><strong>受理人：</strong><span name="userid" ></span></td>
			<td align="left" style="padding-left:10px;"><strong>受理方式：</strong><span name="acceptType" ></span></td>
			<td align="left" style="padding-left:10px;"><strong>归属部门：</strong><span name="department" ></span></td>
		</tr>
		<tr>
			<td align="left" style="padding-left:10px;"><strong>工单类型：</strong><span name="business" ></span></td>
			<td align="left" style="padding-left:10px;"><strong>一级目录：</strong><span name="directoryFir" ></span></td>
			<td align="left" style="padding-left:10px;"><strong>二级目录：</strong><span name="directorySec" ></span></td>
		</tr>
		<tr>
			<td align="left" style="padding-left:10px;" colspan="3"><strong>问题描述：</strong><span name="descrip" ></span></td>
		</tr>
		<tr>
			<td align="left" style="padding-left:10px;" colspan="3"><strong>处理结果：</strong><span name="result" ></span></td>
		</tr>
		<tr>
			<td align="left" style="padding-left:10px;" colspan="3"><strong>工单分配：</strong> <span name="orderAllot" ></span></td>
		</tr>
	</table>
</div>
<!-- 工单数据存储 结束 -->
<script>

var mouseD;
var objDiv = document.getElementById("serOrderDiv"); 
var isIE = document.all ? true : false;//判断浏览器类型 
document.onmousedown = function(evnt) {//当鼠标左键按下后执行此函数 
	var evnt = evnt ? evnt : event; 
	if (evnt.button == (document.all ? 1 : 0)) { 
		mouseD = true;//mouseD为鼠标左键状态标志，为true时表示左键被按下 
	} 
} 
objDiv.style.top = "15%";//设置弹出层距离上边界的距离 
objDiv.style.left = "50%";//设置弹出层距离左边界的距离 

// 鼠标按键按下
objDiv.onmousedown = function(evnt) { 
	objDrag = this;//objDrag为拖动的对象 
	var evnt = evnt ? evnt : event; 
	if (evnt.button == (document.all ? 1 : 0)) { 
		mx = evnt.clientX; 
		my = evnt.clientY; 
		objDiv.style.left = (objDiv.offsetLeft +400)+ "px"; 
		objDiv.style.top = objDiv.offsetTop + "px"; 
		if (isIE) { 
			objDiv.setCapture(); 
			// objDiv.filters.alpha.opacity = 75;//当鼠标按下后透明度改变 
		} else { 
			window.captureEvents(Event.MOUSEMOVE);//捕获鼠标拖动事件 
			// objDiv.style.opacity = 0.75;//当鼠标按下后透明度改变 
		} 
	}
} 

// 鼠标按键松启
document.onmouseup = function() { 
	mouseD = false;//左键松开 
	objDrag = ""; 
	if (isIE) { 
		objDiv.releaseCapture(); 
		// objDiv.filters.alpha.opacity = 100;//当鼠标左键松开后透明度改变 
	} else { 
		window.releaseEvents(objDiv.MOUSEMOVE);//释放鼠标拖动事件 
		// objDiv.style.opacity = 1;//当鼠标左键松开后透明度改变 
	} 
} 

// 鼠标移动
document.onmousemove = function(evnt) { 
	var evnt = evnt ? evnt : event; 
	if (mouseD == true && objDrag) { 
		var mrx = evnt.clientX - mx; 
		var mry = evnt.clientY - my; 
		objDiv.style.left = parseInt(objDiv.style.left) + mrx + "px"; 
		objDiv.style.top = parseInt(objDiv.style.top) + mry + "px"; 
		mx = evnt.clientX; 
		my = evnt.clientY; 
	} 
} 

// 工单窗口最小化
function minDiv(){
	objDiv.style.width=200+"px";
	objDiv.style.height=50+"px";
	$("#serOrderDiv").find("div").eq(0).hide();
	$("#fade").hide();
}

// 工单恢复窗口
function maxDiv(){
	objDiv.style.width="800px";
	objDiv.style.height="610px";
	$("#serOrderDiv").find("div").eq(0).show();
	objDiv.style.top = "15%";//设置弹出层距离上边界的距离 
	objDiv.style.left = "50%";//设置弹出层距离左边界的距离 
	$("#fade").show();
}


//********************************cus start**********************
function cusInit() {
	//弹出按钮
	$("#cusOrderId").click(function() {
		$.ajax({
			type : "POST",
			url : '/'+projectName+'/book/book/getCurrentUser.action',
			dataType:'json',
			success : function(result) {
				if(result != null && result.name != null){
					$("#fade").show();
					$("#cusOrderDiv").show();
					loadCusData($("select[name='profession']").val());
				}else{
					alert("请登录后操作");
				}
			}
		});
	});
	//关闭按钮
	$("#cusCloseId").click(function() {
		$("#cusOrderDiv").hide();
		$("#fade").hide();
	});
	// 下拉框绑定方法
	$("select[name='profession']").change(function() {
		var profession = $(this).val();
		if (profession != "" ) {
			loadCusData($(this).val());
			$("#dataTable").show();
		} else {
			$("#dataTable").hide();
		}
	});
	//保存
	$("#saveId").click(function() {
		saveCusData();
	});
	// 拨号
	$("#callId").click(function() {
		preCall();
	});
}
// 加载数据（客户打电话时弹出内容）
function loadCusData(profession) {
	
	$("#orderForm").find("input").val("");
	$.ajax({
		type : "POST" , url : '/'+projectName+'/practical/workorder/serchCusOrder.action' , data : {
			"profession" : ''
		} , dataType : 'json' , success : function(result) {
			
			if (result != null && result.data != null ) {
				var entity = result.data;
				$("#orderForm").find("input[name='id']").val(entity.id);
				$("#orderForm").find("input[name='userid']").val(entity.userid);
				$("#orderForm").find("input[name='name']").val(entity.name);
				$("#orderForm").find("select[name='sex']").val(entity.sex);
				$("#orderForm").find("input[name='telphone']").val(entity.telphone);
				$("#orderForm").find("input[name='company']").val(entity.company);
				$("#orderForm").find("input[name='profession']").val(entity.profession);
				$("#orderForm").find("input[name='addr']").val(entity.addr);
				$("#orderForm").find("input[name='zipcode']").val(entity.zipcode);
				$("#orderForm").find("input[name='email']").val(entity.email);
				$("#orderForm").find("input[name='belong']").val(entity.belong);
				$("#orderForm").find("select[name='certType']").val(entity.certType);
				$("#orderForm").find("input[name='certNum']").val(entity.certNum);
				$("#orderForm").find("input[name='cusNum']").val(entity.cusNum);
				$("#orderForm").find("input[name='proBelong']").val(entity.proBelong);
			}
		}
	});
}

// 保存数据
function saveCusData() {
	
	var flg = true;
	if(flg){
		var param = $("#orderForm").serialize();
		$.ajax({
			type : "POST" , url : '/'+projectName+'/practical/workorder/saveClient.action' , data : param , async: false ,dataType : 'json' , success : function(result) {
				if (result != null && result.success ) {
					var id = $("input[name='id']").val();
					if (id == null || id == "" ) {
						$("#orderForm").find("input[name='id']").val(result.id);
					}
					$("#cusOrderDiv").hide();
					$("#fade").hide();
					swal({title:result.msg,type:"success"});
					$("#callId").show();
				}
			}
		});
	}else{
		alert("数据未填写完");
	}
}

// 拨号
function preCall() {
	var profession = $("#orderForm").find("select[name='profession']").val();
	var agentId = $("input[name='agentId']").val();
	var id = $("#orderForm").find("input[name='id']").val();
	
	if (agentId == null || agentId == "" ) {
		swal({title:"请填写您要拨打的坐席号",type:"warning"});
		return false;
	}
	
	$.ajax({
		type : "POST" , url : '/'+projectName+'/practical/workorder/serchCusOrder.action' , data : {
			"profession" : ''
		} , dataType : 'json' ,async: false , success : function(result) {
			
			if (result != null && result.data != null ) {
				var entity = result.data;
				$("#orderForm").find("input[name='id']").val(entity.id);
				$("#orderForm").find("input[name='userid']").val(entity.userid);
				$("#orderForm").find("input[name='name']").val(entity.name);
				$("#orderForm").find("select[name='sex']").val(entity.sex);
				$("#orderForm").find("input[name='telphone']").val(entity.telphone);
				$("#orderForm").find("input[name='company']").val(entity.company);
				$("#orderForm").find("input[name='profession']").val(entity.profession);
				$("#orderForm").find("input[name='addr']").val(entity.addr);
				$("#orderForm").find("input[name='zipcode']").val(entity.zipcode);
				$("#orderForm").find("input[name='email']").val(entity.email);
				$("#orderForm").find("input[name='belong']").val(entity.belong);
				$("#orderForm").find("select[name='certType']").val(entity.certType);
				$("#orderForm").find("input[name='certNum']").val(entity.certNum);
				$("#orderForm").find("input[name='cusNum']").val(entity.cusNum);
				$("#orderForm").find("input[name='proBelong']").val(entity.proBelong);
			}
		}
	});
	saveCusData();
	
	
	$.ajax({
		type : "POST" , url : '/'+projectName+'/practical/workorder/preCall.action' , data : {
			"profession" : profession , "agentId" : agentId,"taskId" : topicIdParam
		} , dataType : 'json' , async: false , success : function(result) {
			if (result != null ) {
				if (result.success ) {
					swal({title:result.msg,type:"success"});
					$("#sipexe").attr("src", "sip:"+agentId);
					//websocket.send(agentId);
					//websocket.send("change");
				} else {
					swal({title:result.msg,type:"error"});
				}
			}
		}
	});
}

//********************************cus end**********************

//********************************ser start**********************

function serInit() {
	//弹出按钮
	$("#serOrderId").click(function() {
		loadSerData(false);
	});
	
	//关闭按钮
	$("#serCloseId").click(function() {
		$("#serOrderDiv").hide();
		$("#fade").hide();
	});
	// 挂断电话
	$("#hungId").click(function() {
		preHungUp();
	});
}

// 加载数据
function loadSerData(load) {
	$.ajax({
		type : "POST" , url : '/'+projectName+'/practical/workorder/serSearchCus.action' , 
		data : {} , 
		dataType : 'json' , 
		success : function(result) {
			if (result != null && result.data != null ) {
				$("#serOrderDiv").show();
				$("#fade").show();
				// 如果load为true则重新加载数据
				$("#cusTable").find("input").val("");
				var entity = result.data;
				if (entity.profession != null ) {
					$("#cusTable").find("select[name='profession']").val(entity.profession);
					var item = $("#cusTable").find("select[name='profession'] option:selected").text();
					if(entity.profession == ""){
						$("#cusTable").find("select[name='profession']").next('span').html("未选择");
					}else{
						$("#cusTable").find("select[name='profession']").next('span').html(item);
					}
				}else{
					$("#cusTable").find("select[name='profession']").next('span').html("未选择");
				}
				if (entity.id != null ) {
					$("#cusTable").find("input[name='id']").val(entity.id);
				}
				if (entity.userid != null ) {
					$("#cusTable").find("input[name='userid']").val(entity.userid);
				}
				if (entity.name != null ) {
					$("#cusTable").find("input[name='name']").val(entity.name);
				}
				if (entity.sex != null ) {
					$("#cusTable").find("select[name='sex']").val(entity.sex);
				}
				if (entity.telphone != null ) {
					$("#cusTable").find("input[name='telphone']").val(entity.telphone);
				}
				if (entity.company != null ) {
					$("#cusTable").find("input[name='company']").val(entity.company);
				}
				if (entity.profession != null ) {
					$("#cusTable").find("td[name='profession']").val(entity.profession);
				}
				if (entity.addr != null ) {
					$("#cusTable").find("input[name='addr']").val(entity.addr);
				}
				if (entity.zipcode != null ) {
					$("#cusTable").find("input[name='zipcode']").val(entity.zipcode);
				}
				if (entity.email != null ) {
					$("#cusTable").find("input[name='email']").val(entity.email);
				}
				if (entity.belong != null ) {
					$("#cusTable").find("input[name='belong']").val(entity.belong);
				}
				if (entity.certType != null ) {
					$("#cusTable").find("select[name='certType']").val(entity.certType);
				}
				if (entity.certNum != null ) {
					$("#cusTable").find("input[name='certNum']").val(entity.certNum);
				}
				if (entity.cusNum != null ) {
					$("#cusTable").find("input[name='cusNum']").val(entity.cusNum);
				}
				if (entity.cusLevel != null ) {
					$("#cusTable").find("select[name='cusLevel']").val(entity.cusLevel);
				}
				if (entity.proBelong != null ) {
					$("#cusTable").find("input[name='proBelong']").val(entity.proBelong);
				}
				
				$("#addBillTable").find("input").val("");// 清空之前的数据，重新加载新数据
				// 加载bill数据
				var len = loadBill();
				len++;
				var orderNum = result.timeStamp+len;
				$("#addBillTable").find("input[name='number']").val(orderNum);// 工单号
				$("#addBillTable").find("input[name='contact']").val(entity.telphone);//联系号码
				$("#addBillTable").find("input[name='tmstr']").val(result.callTime);//呼入时间
				$("#addBillTable").find("input[name='userid']").val(result.currentNum);// 受理人
				// 窗口正常大小显示
				maxDiv();
			} else {
				alert("没有电话");
			}
		}
	});
}

// 加载bill数据
function loadBill() {
	var len = 0;
	// 清空tbody
	$("#billTable").find('tbody').empty();
	$.ajax({
		type : "POST" ,
		url : '/'+projectName+'/practical/workorder/serchBill.action' ,
		data : {taskId:topicIdParam,type:1} ,
		async: false,
		dataType : 'json' ,
		success : function(result) {
			if (result != null && result.rows != null ) {
				var trHtml = "";
				var rows = result.rows;
				len = rows.length;
				for (var i = 0; i < rows.length; i++) {
					var hide = $("#copyDiv").html();
					var tr = "<tr style='border:1px solid #00F;'>" 
								+ "<td><a href=\"javascript:showBill('"+rows[i].id+"')\">" + rows[i].number + "</a></td>" 
								+ "<td>" + rows[i].tmYear + "</td>" 
								+ "<td>" + rows[i].tmHour + "</td>"
								+ "<td>" + rows[i].business + "</td>" 
								+ "<td>" + rows[i].department + "</td>" 
								+ "<td>" + rows[i].userid + "</td>" 
							+ "</tr><tr name='hide"+rows[i].id+"' class='hideTr' style='display:none;text-align:center;'><td colspan='6'>"+hide+"</td><tr>";
					trHtml += tr;
				}
				$("#billTable").find('tbody').html(trHtml);
			}
		}
	});
	return len;
}

// 显示所选工单
function showBill(billId){
		$.ajax({
			type : "POST" ,
			url : '/'+projectName+'/practical/workorder/serchBillById.action' ,
			data : {"id":billId} ,
			async: false,
			dataType : 'json' ,
			success : function(result) {
				if(result != null){
					var temp = $("#billTable").find("tr[name='hide"+billId+"']").find("td");
					temp.find("span[name='number']").text(result.number);
					temp.find("span[name='userid']").text(result.userid);
					temp.find("span[name='tmstr']").text(result.tmstr);
					temp.find("span[name='callTime']").text(result.callTime);
					temp.find("span[name='dialing']").text(result.dialing);
					temp.find("span[name='contact']").text(result.contact);
					temp.find("span[name='business']").text(result.business);
					temp.find("span[name='result']").text(result.result);
					temp.find("span[name='descrip']").text(result.descrip);
					temp.find("span[name='orderAllot']").text(result.orderAllot);
					temp.find("span[name='acceptType']").text(result.acceptType);
					temp.find("span[name='directoryFir']").text(result.directoryFir);
					temp.find("span[name='directorySec']").text(result.directorySec);
					temp.find("span[name='department']").text(result.department);
				}else{
					$("#billTable").find("tr[name='hide"+billId+"']").find("td").html("查询错误");
				}
				$("#billTable").find("tr[class='hideTr']").each(function(){
					var na = 'hide'+billId;
					if($(this).attr("name") == na){
						$(this).toggle("fast");
					}else{
						$(this).hide();
					}
				})
			}
		});
	
}

// 保存客户等级
function saveCusInfo(){
	var param = $("#cusInfoForm").serialize();
	
	$.ajax({
		type : "POST" , url : '/'+projectName+'/practical/workorder/changeLevel.action' , data : param , async: false ,dataType : 'json' , success : function(result) {
			if (result != null && result.success ) {
				var id = $("input[name='id']").val();
				if (id == null || id == "" ) {
					$("#orderForm").find("input[name='id']").val(result.id);
				}
				swal({title:result.msg,type:"success"});
			}
		}
	});
}

// 保存表单
function saveBill() {
	
	if(window.confirm('确定保存工单吗？')){
		var msg="";
		var req = true;
		$("#billAddForm").find(".required").each(function(i,item){
			var v = $(this).val();
			if(v == null || $.trim(v) == "" ){
				msg = msg + "  "+ $(this).attr("text-data");
				req = false;
			}
		
		});
		
		if(!req){
			alert(msg+" 不允许为空");
			return;
		}
		
		$("#billAddForm").find("input[name='taskId']").val(topicIdParam);
		$("#billAddForm").find("input[name='type']").val("1");
		var param = $("#billAddForm").serialize()+"&type=1&taskId="+topicIdParam;
		$.ajax({
			type : "GET" , 
			url : '/'+projectName+'/practical/workorder/saveBill.action', 
			data :param,
			dataType : 'json' , 
			success : function(result) {
				if (result.success ) {
					loadBill();
					swal({title:"保存成功",type:"success"});
				} else {
					swal({title:result.msg,type:"error"});
				}
			}
		});
	}else{
		
	}
}

// 挂断电话
function preHungUp() {
	$.ajax({
		type : "POST" , url : '/'+projectName+'/practical/workorder/preHungUp.action' , data : {} , dataType : 'json' , success : function(result) {
			if (result.success ) {
				$("#addBill").hide();
				$("#serOrderDiv").hide();
				$("#fade").hide();
			} else {
				swal({title:result.msg,type:"error"});
			}
		}
	});
}
</script>
