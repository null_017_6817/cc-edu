<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>修改课本</title><!--book-->
	<%@include file="/common/include.jsp" %>
	<style>
		td > span {
			width: 155px !important;
		}
		#bookproperties .label{
			width:150px;
		}
		#bookproperties tr td:last-child{
			width:300px;
		}
		#bookproperties input.textbox-text{
			background:none;
		}
		#bookproperties tr td{
			line-height:34px;
		}
	</style>
<script>
<%-- 查询页点击修改时调用此方法加载记录详情 --%>
function load(id,pid){
	$('#_form').form('load','/${projectName}/book/book/updBook.action?id='+id+'&pid='+pid);
	// 初始化回显数据
	$('#_form').form({
		onLoadSuccess : function(data) {
			showImg(data.path);
			 $('#bookproperties tr.disp').css("display","none");
			 showBookProperties();
		}
	});
};

function showImg(path){
	$("img[name='imgName']").attr("src","/${projectName}/book/book/appendHtmlUrl.action?fileName="+path);
	$("#dataFile").change(function(e){
		var file = e.target.files[0];
		if(file){
			var reader = new FileReader();
			reader.onload=function(e){
				$("img[name='imgName']").attr("src",e.target.result);
			}
			reader.readAsDataURL(file);
	}})
}

<%-- 点击保存按钮时调用此方法。(注:保存按钮在查询列表页) --%>
function submitForm(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#_form').form('submit',{
	success:function(data) {
		data=JSON.parse(data);
		var idHid = $("#_form").find("input[name='id']").val();
		
		if(idHid != null && idHid != "" && typeof(idHid) != "undefined"){
			var node = parent.$('#booktree').tree('find', data.id);
			parent.$('#booktree').tree('update', {
				target: node.target,
				text: data.name
			});
		}else {
			var node=parent.$('#booktree').tree("getSelected");
			parent.$('#booktree').tree('append', {
				parent: node.target,
				data: {
					id: data.id,
					text: data.name
				}
			});
		}
		
		parent.reloadData();
	}
	});
}

function preImg(){
	
}

// 触发file的click事件
function triggerFile(){
	$("#dataFile").trigger('click');
}


//显示书本特有属性
function showBookProperties(){
	var typeValue = typeDict.getValue();
	if(typeValue == 1){//目录章节
		 $('#bookproperties tr.disp').css("display","none");
		 cleanBookProperties()
	}else if(typeValue == 0){//课本
		$('#bookproperties tr.disp').css("display","block");
	}else{
		return null;
	}
}

//清空书本特有属性div下表单值
function cleanBookProperties(){
	//清空input
	$("#bookproperties .disp input[type='text']").each(function(){
	    $(this).val('')
	});
	//清空textarea
	$("#bookproperties .disp textarea").each(function(){
	    $(this).val('')
	});
}


</script>
<style>
.imgbox {
	position: relative;
}
.imgbox .imgbox-img {
	position: absolute;
	top: -165px;
	right: 10px;
	display: inline-block;
	width: 131px;
	height: 180px;
}
.imgbox .imgbox-span {
	position: absolute;
	top: 17px;
	right: 30px;
	display: inline-block;
}
.imgbox [type=button]{ 
	border-radius: 25px;
	border: 0px;
	height: 28px;
	background: #b6c1fb;
}
</style>
</head>
<body>
<form id="_form" method="post" action="/${projectName}/book/book/updBook.action"  enctype="multipart/form-data" >
	<input type="hidden" name="from" value="submit"/>
	<input type="hidden" name='id' id='id'/>
	<input type="hidden" name='pid' value=""/>
	<input type="hidden" name='treetrack' value=""/>
	<table id="bookproperties" style="margin: 0 auto;width:72%;">
		<tr style="display:block;"><td class='label'>书名或章节名：</td><td colspan="2"><input id="name" name="name" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr style="display:block;"><td class='label'>上级机构：</td><td colspan="2"><input id="pname" name='pname' class='easyui-textbox' value='' readonly="readonly" /></td></tr>
		<tr style="display:block;"><td class='label'>状态：</td><td colspan="2"><dc:insertList name="state" dictId="ZD_BOOKSTATE" style="combo_normal"/></td></tr>
		<tr style="display:block;"><td class='label'>类型：</td><td colspan="2"><dc:insertList name="type" dictId="ZD_BOOKTYPE" style="combo_normal" value="0" required="true" bind="select:showBookProperties();"/></td></tr>
		<tr class="disp"><td class='label'>是否开放实训营地：</td><td colspan="2"><dc:insertList name="opentraining" dictId="ZD_SFBZ" style="combo_normal" value="1"/></td></tr>
		<tr class="disp"><td class='label'>出版社：</td><td colspan="2"><input id="bookconcern" name="bookconcern" class="easyui-textbox" data-options=""/></td></tr>
		<tr class="disp"><td class='label'>主编：</td><td colspan="2"><input id="editor" name="editor" class="easyui-textbox" data-options=""/></td></tr>
		<tr class="disp"><td class='label'>出版时间：</td><td colspan="2"><input id="publictimestr" name="publictimestr" class="easyui-datebox" data-options=""/></td></tr>
 		<tr class="disp"><td class='label'>排序：</td><td colspan="2"><input id="bookOrder" name="bookOrder" class="easyui-textbox" data-options=""/></td></tr>
 		<tr class="disp">
			<td class='label'>封面路径：</td>
			<td class="imgbox">
				<input type="hidden" id="path" name="path" />
				<input style="width: 157px;cursor: pointer;" type="button" onclick="triggerFile();" value="选择封面" />
				<input style="display:none;" type="file" name="dataFile" id="dataFile" accept="image/*" />
				<img style="top:-290px;right:-57px;" class="imgbox-img" name="imgName" src="" />
				<span style="top:-100px;right:-98px;" class="imgbox-span">（课本封面预览）</span>
			</td>
		</tr>
		<!-- <tr class="disp"><td class='label'>专业：</td><td colspan="2"><input id="profession" name="profession" class="easyui-textbox" data-options=""/></td></tr>
		<tr class="disp"><td class='label'>班级号：</td><td colspan="2"><input id="cnumber" name="cnumber" class="easyui-textbox" data-options=""/></td></tr>
		<tr class="disp"><td class='label'>学期：</td><td colspan="2"><input id="term" name="term" class="easyui-textbox" data-options=""/></td></tr> -->
		<tr class="disp"><td class='label'>简介：</td><td colspan="2"><textarea id="synopsis" name="synopsis" rows=6 cols="45" class="textarea easyui-validatebox"></textarea></td></tr>
		<tr class="disp"><td class='label'>描述：</td><td colspan="2"><textarea id="remarks" name="remarks" rows=6 cols="45" class="textarea easyui-validatebox"></textarea></td></tr>
	</table>
	
</form>
</body>
</html>
