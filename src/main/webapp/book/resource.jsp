<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="ie6"><![endif]-->
<!--[if IE 7]>
<html class="ie7"><![endif]-->
<!--[if IE 8]>
<html class="ie8"><![endif]-->
<!--[if (gte IE 9)]><!-->
<html>
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<title>呼叫中心在线课件详情页</title>
<link rel="stylesheet" type="text/css"	href="/${projectName}/book/css/common.css" />
<link rel="stylesheet" type="text/css"	href="/${projectName}/book/css/fonts.css" />
<link rel="stylesheet" type="text/css"	href="/${projectName}/book/css/style.css" />

<script type="text/javascript"	src="/${projectName}/book/js/lib/jquery-1.9.1.min.js"></script>
<script type="text/javascript"	src="/${projectName}/book/js/lib/jquery.cookie.js"></script>
<script type="text/javascript"	src="/${projectName}/book/js/common/tab.js"></script>
<script type="text/javascript"	src="/${projectName}/book/js/lib/swfobject.js"></script>
<style>
#icon{
display:none
}
</style>
<script>
var tabName= "resourceTab";

//banner - 书本名称
$(function(){
	var bookRootId = '${bookRoot.id}',
		html = '<img src="/${projectName}/book/img/banner_44.jpg" />';
	$('.banner').append(html);
	openFlash();
	clickTask("${bookId}");
	//根据bookId判断是否显示实训营地
	getOpentrainingValue('${bookRoot.id}');
});

//判断是否启用flash
function openFlash(){
	var flag = false;
	if(window.ActiveXObject){
		try{
				var swf = new ActiveXObject("ShockwaveFlash.ShockwaveFlash");
				if(swf){
				flag = true;
				}
			}catch(e){
		}
	}else{
		try{
			var swf = navigator.plugins['Shockwave Flash'];
			if(swf){
				flag = true;
			}
		}catch(e){
		}
	}
	if(flag){
	$("#flg").hide();
		console.log("running ok");
	}else{
		console.log("running error");
	}
}

function getOpentrainingValue(bookId){
	$.ajax({
		url : "/${projectName}/book/book/schBookByBookID.action" , 
		type : "post" , 
		data : {"bookId" : bookId} , 
		dataType: "json",
		success : function(result) {
			if (result.success ) {
				var OpentrainingValue = result.data.opentraining;
				if(OpentrainingValue == "1"){
					$("#practiceurl").css("display","block");
				}else{
					$("#practiceurl").css("display","none");
				}
				return OpentrainingValue;
			}
		}
	});
}


	var bookId;	// 
	var defaultTask;
	var defaultobj;
	var getFile;
	
	var dataArr = new Array();
	function clickTask(taskId){
		$(".tabsList").html('');
		if(taskId!=null){
			$.cookie('rtaskId', taskId, { expires: 1 }); 
		}
		//tabsList 拼接课程菜单
		$.ajax({ url: "/${projectName}/manager/course/schCourseByBookId.action",
			type : "post",
			context: document.body, 
			data : {bookId:taskId,type:'${type}'},
			success: function(data,result){
				if(result=="success"){
					data = eval("(" + data + ")");
						data = data.rows;
						var defaultObj;
						var obj;
						//根据章节树的taskID，获取课程列表树
						for (var i=0;i<data.length;i++){
							//obj = JSON.stringify(data[i]);
							//默认获取flash
							$(".tabsList").append("<li><a data-index='"+i+"' onclick='getPdf("+i+")' >"+data[i].courTitle+"</a></li>");
							if(i==0){
								defaultObj=obj;
							} 
							dataArr.push(data[i]);
						}
						getPdf(0,defaultObj);
				}else{
					alert('服务器异常！');
				}
			}
		});
	}

	function getFileSuffix(sourcePath){
		var index1 = sourcePath.lastIndexOf(".");
		var index2 = sourcePath.length;
		var type = sourcePath.substring(index1+1,index2);//后缀名
		return type;
	}

	function getPdf(index){
		var nginxHost = "https://"+document.location.host+":9799/";
		var Obj = dataArr[index];
		if(Obj == null || typeof(Obj) == "undefined" ){
			var nodata = "<img src='/${projectName}/book/js/layui/img/nocon2.png'>"
				+ "<p style='text-align:center;font-size: 24px;color: #999999;margin-top: 60px;'>暂无内容哦！</p>";
				$("#altContentOneNewControls").html(nodata);// 无数据图片显示
			return;
		}else{
			//Obj = eval("(" + Obj + ")");
		}
		
		$(".tabsList").find("li").eq(index).siblings().removeClass('active');;
		$(".tabsList").find("li").eq(index).addClass("active");
		if(Obj.attachPath == null || Obj.attachPath == "" || typeof(Obj.attachPath) == "undefined"){
			$(".operationBtn.clearfix").hide();// 隐藏操作按钮
			var nodata = "<img src='/${projectName}/book/js/layui/img/nocon2.png'>"
				+ "<p style='text-align:center;font-size: 24px;color: #999999;margin-top: 60px;'>暂无内容哦！</p>";
				$("#altContentOneNewControls").html(nodata);// 无数据图片显示
			return;
		}
	
		var suffix = "";
		if(Obj.attachPath != null && Obj.attachPath != '' && typeof(Obj.attachPath) != "undefined"){
			suffix = getFileSuffix(Obj.attachPath);
		}
		var flgHtml = "<div  id='flg' style='height: 690px;text-align: center;line-height: 642px;font-size: 15px;background-color: gray;position:absolute;top:0;left:0;width:832px;' >"
			+ "<a href='http://www.adobe.com/go/getflashplayer' onclick='openFlsh();' rel='nofollow' id='flshId'  target='_blank' title=''>点击启用flash</a>"
			+"</div>";
		$(".tab-content-active").html("<div id='altContentOneNewControls'></div>"+flgHtml);
		if(suffix != null && suffix.toLocaleLowerCase() == "pdf"){
			$(".operationBtn").hide();
			$("#altContentOneNewControls").html('');
			var pdfUrl=Obj.attachPath.substring(Obj.attachPath.lastIndexOf("/")+1,Obj.attachPath.lastIndexOf(".")).concat(".pdf");
			getFile=pdfUrl;
			$("#altContentOneNewControls").append("<iframe src='/${projectName}/js/pdfjs/web/viewer.html?file=/${projectName}/manager/course/appendPdfUrl/"+pdfUrl+".action' id='iframe' name='iframe' style='width:100%;' frameborder='0' scrolling='no' height='681px' onreadystatechange='myLoad(this)'></iframe> ");
		}
		
		if(suffix=='swf' || suffix == "SWF"){
			$(".operationBtn.clearfix").hide();
			var flashvarsVideoNewControls = {
				source: "null",
				type: "video",
				streamtype: "file",
				server: "",//Used for rtmp streams
				duration: "52",
				poster: "http://jaris.sourceforge.net/images/poster.png",
				autostart: "false",
				logo: "http://jaris.sourceforge.net/images/logo.png",
				logoposition: "top left",
				logoalpha: "30",
				logowidth: "130",
				logolink: "http://jaris.sourceforge.net",
				hardwarescaling: "false",
				darkcolor: "000000",
				brightcolor: "4c4c4c",
				controlcolor: "FFFFFF",
				hovercolor: "67A8C1",
				controltype:1
			};
			var params = {
				play:"false",
				loop:"false",
				menu: "true",
				//scale: "noScale",
				allowfullscreen: "true",
				allowScriptAccess: "always",
				bgcolor: "#ffffff",
				quality: "high",
				wmode: "opaque"
			};
			var attributes = {
				id: "JarisFLVPlayer"
			};
			if($("#JarisFLVPlayer") != null && typeof($("#JarisFLVPlayer")) != "undefined"  && $("#JarisFLVPlayer").length > 0){
				$("#JarisFLVPlayer").attr("data","/${projectName}/manager/course/appendUrl.action?filename="+Obj.attachPath);
			}else{
				swfobject.embedSWF("/${projectName}/manager/course/appendUrl.action?filename="+Obj.attachPath, "altContentOneNewControls", "100%", "640", "10.0.0", "/${projectName}/book/video/flvplayer.swf", flashvarsVideoNewControls, params, attributes);
			}
		}else if(suffix == "wav" || suffix == "WAV" 
				|| suffix == "mp3" || suffix == "MP3"){
			var srcHost = nginxHost+Obj.attachPath;
			var poster = "";
			if(type == "mp3" || type == "MP3" || type == "wav" || type == "WAV"){
				poster = "/${projectName}/book/img/audioPlayer.jpg";
			}
			$(".operationBtn.clearfix").hide();
			var html = "<video poster='"+poster+"' src='"+srcHost+"'"
					+"\" width='100%' height='100%' type='video/mp4' controls='true' controlslist='nodownload'  >	</video>";
			$(".tab-content-active").html('');
			$(".tab-content-active").append("<div id='altContentOneNewControls'>"+html+"</div>");
			
		} else if(suffix == "MP4" || suffix=="mp4" 
			||  suffix == "flv" || suffix == "FLV" ){
			var srcHost = nginxHost+Obj.attachPath;
			$(".operationBtn.clearfix").hide();
			var html = "<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0' width='100%' height='100%'>"
			+ "<param name='movie' value='/${projectName}/book/video/flvplayerd.swf' />"
			+ "<embed src='/${projectName}/book/video/flvplayerd.swf' allowfullscreen='true' flashvars='vcastr_file="+srcHost+"' pluginspage='http://www.macromedia.com/go/getflashplayer' type='application/x-shockwave-flash' width='700' height='600'></embed>"
			+ "</object>";
			$(".tab-content-active").html('');
			$(".tab-content-active").append("<div id='altContentOneNewControls'>"+html+"</div>");  
		} 
		openFlash();
	}
	function showFullScreen(){
		window.open('/${projectName}/js/pdfjs/web/viewer.html?file=/${projectName}/manager/course/appendPdfUrl/'+getFile+'.action');
	}
	function play() {
    	swfobject.getObjectById("JarisFLVPlayer").Play();
    }
    function pause() {
    	swfobject.getObjectById("JarisFLVPlayer").StopPlay();
    }
    function goStart() {
    	swfobject.getObjectById("JarisFLVPlayer").GotoFrame(1);
    	swfobject.getObjectById("JarisFLVPlayer").Play();
    }
    function full() {
    	//swfobject.getObjectById("JarisFLVPlayer").;
    }
	
	</script>
</head>

<body>
<input style="display:none" type="text" name="fakeusernameremembered"/>
<input style="display:none" type="password" name="fakepasswordremembered"/>
<div class="wrap">
	<div class="content-box">
		<jsp:include page="_header.jsp" />

		<div class="banner">
			<div class="inner">
				<ul class="nav">
					<li><a href="/${projectName}/book/index.jsp">首页</a></li>
					<li>&nbsp;/&nbsp;</li>
					<li><a href="/${projectName}/book/book/page.action?page=catalog&bookRoot=${bookRoot.id}">课程目录</a></li>
					<li>&nbsp;/&nbsp;</li>
					<li>${book.name}</li>
				</ul>
				<span class="bookTitle">${bookRoot.name}</span>
				<dl>
					<dd>
						<div class="teacherInfo">
							<span>作者：${bookRoot.editor}</span>
							<i>|</i>
							<span>出版社：${bookRoot.bookconcern}</span>
							<i>|</i>
							<span>出版时间：<fmt:formatDate value="${bookRoot.publictime}" pattern="yyyy-MM-dd"/></span>
						</div>
						<p title="${bookRoot.remarks}">${bookRoot.remarks}</p>
					</dd>
				</dl>
			</div>
		</div>

		<div class="main bg" style="min-height: 920px;">
			<div class="detailBox inner" >
				<jsp:include page="moduleTab.jsp" />
				
				<div class="tabsBox clearfix" id="tabs-basic">
					<div class="tabsInfo border-right pull-left" style="min-height:682px;">
						<div class="tab-content-active" style="position:relative;">
							<div id="altContentOneNewControls" style="text-align:center"></div>
							<div  id="flg" style="height: 690px;text-align: center;line-height: 642px;font-size: 15px;background-color: gray;position:absolute;top:0;left:0;width:832px;" >
								<a href="http://www.adobe.com/go/getflashplayer" onclick="openFlsh();" rel="nofollow" id="flshId"  target="_blank" title="">点击启用flash</a>
							</div>
						</div>
					</div>

					<div class="tabsright pull-right">
						<h6>资源列表</h6>		
						<ul class="tabsList"></ul>
					</div>
					<ul class="operationBtn clearfix" style="display:none;">
						<li onclick="play()"><img src="/${projectName}/book/img/play.png" alt="播放" title="播放" /></li>
						<li onclick="pause()"><img src="/${projectName}/book/img/pause.png" alt="暂停" title="暂停" /></li>
						<li onclick="goStart()"><img src="/${projectName}/book/img/goStart.png" alt="重播" title="重播" /></li>
						<%-- <li onclick="full()"><img src="/${projectName}/book/img/goStart.png" alt="重播" title="重播" /></li> --%>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<jsp:include page="_footer.jsp" />

<jsp:include page="./pop.jsp" />

<script>
var projectName = '${projectName}';
//iframe自适应高度的函数
var oTime = null;
function resize() {
	if (oTime) {
		clearTimeout(oTime);
	}
	oTime = setTimeout(reset, 200);
}

//iframe自适应高度的函数
function reset() {
	var frame = document.getElementById("iframe"),
		outHeight = frame.offsetHeight,
		inHeight = frame.contentWindow.document.body.scrollHeight,
		h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight,
		ifHeight = h-($(".header").height() + $(".footer").height() + $(".nav").height() + 97);

	if (outHeight < inHeight) {
		frame.style.height = (inHeight + 10) + "px";
	}else if(inHeight>650){
		frame.style.height=(inHeight + 10) + "px";
	}else{
		frame.style.height= ifHeight + "px";
	}
}
</script>
</body>
<script type="text/javascript"	src="/${projectName}/book/js/common/login.js"></script>
</html>