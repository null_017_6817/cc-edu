<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="ie6"><![endif]-->
<!--[if IE 7]>
<html class="ie7"><![endif]-->
<!--[if IE 8]>
<html class="ie8"><![endif]-->
<!--[if (gte IE 9)]><!-->
<html>
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<title>呼叫中心实训营地</title>
<link rel="stylesheet" type="text/css"	href="/${projectName}/book/css/amazeui.min.css" />
<link rel="stylesheet" type="text/css"	href="/${projectName}/book/css/common.css" />
<link rel="stylesheet" type="text/css"	href="/${projectName}/book/css/fonts.css" />
<link rel="stylesheet" type="text/css"	href="/${projectName}/book/css/style.css" />
<link rel="stylesheet" href="/${projectName}/book/js/lib/layui2.3/css/layui.css">
<style>
	.am-tabs-nav li {
		position: relative;
		z-index: 1;
		min-width: 100px;
	}
 
	.am-tabs-nav .am-icon-close {
		position: absolute;
		top: 17;
		right: 12px;
		color: #888;
		cursor: pointer;
		z-index: 100;
	}
	
	.am-tabs-nav .am-icon-edit {
		position: absolute;
		top: 20px;
		right: 9px;
		color: #888;
		cursor: pointer;
		z-index: 100;
	}
	
	.am-tabs-nav .am-icon-check {
		position: absolute;
		top: 20px;
		right: 9px;
		color: #888;
		cursor: pointer;
		z-index: 100;
	}
	
	
	.am-tabs-nav .am-icon-close:hover {
		color: #333;
	}
 
	.am-tabs-nav .am-icon-close ~ a {
		padding-right: 25px !important;
	}
	.js-append-tab{
		float: right;
		border-radius:5px;        
	}
	.js-append-tab:focus{
		outline: 0;
	}
	.am-nav-tabs>li a{
		height:42px;
	}
	.inputtext{
		width:90px;
		border:1px solid #ddd;
		padding:5px;
		heihgt:20px;
	}
	#question table {
		width: 100%;
		margin: 10px 0;
	}
	#question p {
		font-size: 14px;
		line-height: 20px;
		margin: 10px 0;
	}
	#question img {
		max-width: 100%;
		margin: 5px auto;
	}
</style>
<script type="text/javascript"	src="/${projectName}/book/js/lib/jquery-1.9.1.min.js"></script>
<script type="text/javascript"	src="/${projectName}/book/js/lib/amazeui.min.js"></script>
<script type="text/javascript"	src="/${projectName}/book/js/lib/jquery.cookie.js"></script>
<script type="text/javascript"	src="/${projectName}/book/js/common/tab.js"></script>
<script type="text/javascript"	src="/${projectName}/book/js/jssip/jssip-3.0.13.js"></script>
<script type="text/javascript" charset="utf-8"	src="/${projectName}/ueditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8"	src="/${projectName}/ueditor/ueditor.all.js"></script>
<script type="text/javascript" charset="utf-8"	src="/${projectName}/js/My97DatePicker/WdatePicker.js"></script>
<script src="/${projectName}/book/js/layui/js/lib/layui2.3/layui.all.js"></script>
<!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
<!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
<script type="text/javascript" charset="utf-8"	src="/${projectName}/ueditor/lang/zh-cn/zh-cn.js"></script>

<script>

var projectName = '${projectName}';
var isSXYD = true;
var callEmpty = true;

var bookId; // 
var type; // 
var defaultTask;
var defaultobj;
var indexParam;
var dataArry = new Array();
var stateParam;
var topicIdParam;
var templateId;
var eidParam;
var tabName= "practiceurl";
// 初始化方法
$(function() { 
	initUE("editor");
	var bookRootId = '${bookRoot.id}',
	html = '<img src="/${projectName}/book/img/banner_44.jpg" />';
	$('.banner').append(html);
	var t = setTimeout("clickTask(${bookId})", 200);
	cusInit();
	serInit();
	var state = '${state}';
	if(state != null){
		stateParam = state;
		doChange(state);
	}
	// 绑定tab方法
	bindTab();
	// tab点击
	$("li[name='stepTab']").dblclick(function(i,item){
		var t = this;
		liClick(t);
	});
});

function liClick(t){
	var val = $(t).attr("title");
	$(t).find("input[name=stepTitleInput]").val(val);
	$(t).find("span[name=stepTitleSpan]").hide();
	$(t).find("input[name=stepTitleInput]").show();
	setTimeout(function(){
		$(t).find("input[name=stepTitleInput]").focus();
	},50);
} 

function toSpan(t){
	var title = $(t).val();
	var subTitle = "";
	if(title != null && title != "" && typeof(title) != "undefined"){
		if(title.length > 5){
			subTitle = title.substring(0,4)+"...";
		}else{
			subTitle = title;
		}
	}
	$(t).parent("a").parent("li").attr("title",title);
	$(t).parent().find("span[name=stepTitleSpan]").html(subTitle);
	$(t).hide();
	$(t).parent().find("span[name=stepTitleSpan]").show();
}

//初始化富文本按钮
function initUE(uid){
	// 标准答案编辑器
	var ue = UE.getEditor(uid, {
		toolbars : [ [ 
		'source', /*源代码*/'undo', /*撤销*/'redo', /*重做*/'formatmatch', /*格式刷*/'bold', /*加粗*/
		'indent', /*首行缩进*/'italic', /*斜体*/'underline', /*下划线*/'strikethrough', /*删除线*/'time', /*时间*/
		'date', /*日期*/'justifyleft', /*居左对齐*/'justifyright', /*居右对齐*/'justifycenter', /*居中对齐*/'justifyjustify', /*两端对齐*/
		 /*全屏*/'autotypeset', /*自动排版*/'touppercase', /*字母大写*/'tolowercase', /*字母小写*/'forecolor', /* 字体颜色*/
		'rowspacingtop',/*段前*/'rowspacingbottom',/*段前*/'lineheight',/*行间距*/
		'fontfamily', /* 字体*/'fontsize', /* 字体大小*/'inserttable', /*插入表格*/'insertrow', /*前插入行*/'insertcol', /*前插入列*/
		'mergeright', /*右合并单元格*/'mergedown', /*下合并单元格*/'deleterow', /*删除行*/'deletecol', /*删除列*/'splittorows', /*拆分成行*/
		'splittocols', /*拆分成列*/'splittocells', /*完全拆分单元格*/'deletecaption', /*删除表格标题*/'inserttitle', /*插入标题*/
		'mergecells', /*合并多个单元格*/'deletetable', /*删除表格*/'backcolor', /*背景色*/
		] ] , autoHeightEnabled : false , autoFloatEnabled : true , elementPathEnabled : false
	});
}

// 绑定富文本图片上传方法
UE.Editor.prototype._bkGetActionUrl = UE.Editor.prototype.getActionUrl;  
UE.Editor.prototype.getActionUrl = function(action){  
	if(action == 'uploadimage' || action == 'uploadscrawl' || action == 'uploadimage'){  
		return '/${projectName}/uploaditemimage.action';  
	}else{  
		return this._bkGetActionUrl.call(this, action);  
	}
}

// 获取随机数（参数：长度）
function genID(length){
	return Number(Math.random().toString().substr(3,length) + Date.now()).toString(36);
}

// 绑定tab区域方法
function bindTab() {
	var tabCounter = 0;
	var $tab = $('#doc-tab-demo-1');
	var $nav = $tab.find('.am-tabs-nav');
	var $bd = $tab.find('.am-tabs-bd');
	function addTab() {
		
		var len = $("li[name='stepTab']").length;
		if(len < 5){
			var uid = genID(2);
			var nav = "<li title='title' ondblclick='liClick(this)' onclick=\"showStepAnswer(this)\" showflg='1' index='"+(tabCounter+1)+"' name='stepTab' uid='"+uid+"' id='"+ tabCounter + "'>"
						+"<input type='hidden' name='stepId' value='0'></input>"
						+"<span class='am-icon-close'></span> "
						//+"<span onclick=\"editStepTitle('"+uid+"',this)\" class='am-icon-edit'></span>"
						+"<a href='javascript: void(0)'>"
							+"<input id='input"+uid+"' maxlength='20' style='display:none' type='text' onblur='toSpan(this)' name='stepTitleInput' class='inputtext' placeholder='请输入标签' />" 
							+"<span id='span"+uid+"' name='stepTitleSpan'>title</span>"
						+"</a></li>";
			var content = "<div class='topicBox am-tab-panel'> "
							+ " <span>&nbsp;回答：</span> "
							+ " <script id='"+uid+"' type='text\/plain' style='width:100%;height:400px;'><\/script></div>"; 
			$nav.append(nav);
			$bd.append(content);
			tabCounter++;
			$tab.tabs('refresh');
			var index2 = $nav.find('li').size()-1;
			$tab.tabs('open', index2);
			initUE(uid);
		}else{
			swal({ title: "已达最大限制,禁止添加",
				type: "error"});
		}
		
	}
	// 动态添加标签页
	$('.js-append-tab').on('click', function () {
		addTab();
	});
	
	// 移除标签页
	$nav.on('click', '.am-icon-close', function () {
		var tli = $(this);
		swal({ 
			title: '确定要删除吗？', 
			text: '', 
			showCancelButton: true,
			showCloseButton:false,
			confirmButtonColor: '#3085d6',
			confirmButtonText: '确定删除', 
			cancelButtonText:'我点错了',
		}).then(function(confirm){
			if(confirm.value){
				var stepId = tli.parent("li").find("input[name='stepId']").val();
				if(stepId != null && stepId != 0 && typeof(stepId) != "undefined"){
					$.ajax({
						url : "/${projectName}/practice/practiceAnswer/delSxAnswer.action" , 
						type : "post" , 
						data : {"id" : stepId } , 
						success : function(result) {
							if(result.success){
								swal({ 
									title: "删除成功", 
									type: "success"});
								var $item = tli.closest('li');
								var index = $nav.children('li').index($item);
								var index3 = $nav.find('li').size()-1;
								$item.remove();
								$bd.find('.am-tab-panel').eq(index).remove();
								$tab.tabs('open', 0);
								$tab.tabs('refresh');
							}else{
								swal({ title: result.msg,
									type: "error"});
							}
						}
					});
				}else{
					var $item = tli.closest('li');
					var index = $nav.children('li').index($item);
					var index3 = $nav.find('li').size()-1;
					$item.remove();
					$bd.find('.am-tab-panel').eq(index).remove();
					$tab.tabs('open', index3);
					$tab.tabs('refresh');
				}
			}else{
			}
		}) 
	});
}

var stepArr= Array();
// 显示步骤
function showStepAnswer(t){
	var uid = $(t).attr("uid");
	var index = $(t).attr("index");
	var flg = $(t).attr("showflg");
	if(flg == 0){
		$(t).attr("showflg","1");
		UE.getEditor(uid).setContent(stepArr[index].standardAnswer);
	}
}

function initTab(){
	var tabCounter = 0;
	var $tab = $('#doc-tab-demo-1');
	var $nav = $tab.find('.am-tabs-nav');
	var $bd = $tab.find('.am-tabs-bd');
	$('.am-icon-close').each(function (i,item) {
		var tli = $(this);
		var $item = tli.closest('li');
		var index = $nav.children('li').index($item);
		var index3 = $nav.find('li').size()-1;
		$item.remove();
		$bd.find('.am-tab-panel').eq(index).remove();
		$tab.tabs('refresh');
	});
	$tab.tabs('open', 0);
}

//********************************** 实训营地 start****************************
//
function clickTask(bookId){
	initTab();
	$.ajax({
		url : "/${projectName}/examination/exerTopic/getTopicsByBookId.action" , 
		type : "post" , 
		data : {"bookId" : bookId } , 
		//async : false , 
		success : function(result) {
			if (result.success ) {
				var bookLis = result.data;
				if (bookLis != null && bookLis.length > 0 ) {
					for (var i = 0; i < bookLis.length; i++) {
						var obj = bookLis[i];
						$(".tabsList").append("<li><a data-index='"+i+"' onclick=\"getContent("+i+")\" >"+obj.title+"</a></li>");
						dataArry[i] = obj;
					}
				} else {
					$(".tabsList").html("<li><a >无数据</a></li>");
					noData();
				}
				$(".tabsList").find('li').eq(0).find("a").trigger('click');
			} else {
				swal({ 
					title: result.msg, 
					type: "error"});
			}
		}
	});
}
	
// 无数据三个字按下
function noData() {
	$(".tab-content-active").css("display", "none");
	$("#noData").css("display", "block");
	clearData();
}
	
// 显示主内容
function getContent(index) {
	initTab();
	indexParam = index;
	//选中的列表加入活动样式
	$(".tabsList").find("li").eq(indexParam).siblings().removeClass('active');;
	$(".tabsList").find("li").eq(indexParam).addClass("active");
	var Obj = dataArry[index];
	if (Obj != null ) {
		$(".tab-content-active").css("display", "block");
		$("#noData").css("display", "none");
		var topicId = Obj.id;
		topicIdParam = topicId;
		eidParam = Obj.eid;
		templateId=Obj.templateId;
		showTeaOrStu(topicIdParam);
		$.ajax({
			url : "/${projectName}/practice/practiceAnswer/getStepByTopicId.action" , 
			type : "post" , 
			data : {"etid" : topicId} , 
			success : function(result) {
				if (result.success ) {
					var stepList = result.data;
					stepArr = stepList;
					if(stepList != null){
						var $tab = $('#doc-tab-demo-1');
						var $nav = $tab.find('.am-tabs-nav');
						var $bd = $tab.find('.am-tabs-bd');
						for( var i = 0; i < stepList.length; i++ ){
							var step = stepList[i];
							
							var title = step.title;
							
							var subTitle = "";
							if(title != null && title != "" && typeof(title) != "undefined"){
								if(title.length > 5){
									subTitle = title.substring(0,4)+"...";
								}else{
									subTitle = title;
								}
							}else{
								title = "";
							}
							
							$("#question").html(step.stepinfo);
							$("#question").find("table").css("width","100%");
							if(i == 0){								
								$("li[name='stepTab']").eq(0).attr("title",title);
								$("#spaneditor").html(subTitle);
								$("li[uid='editor']").find("input[name='stepId']").val(step.id);
								UE.getEditor('editor').setContent(step.standardAnswer);
							}
							if(i > 0){
								var uid = genID(2);
								var nav = "<li title='"+title+"' ondblclick='liClick(this)' onclick=\"showStepAnswer(this)\" showflg='0' index='"+i+"' name='stepTab' uid='"+uid+"' id='"+ i + "'>"
											+"<input type='hidden' name='stepId' value='"+step.id+"'></input>"
											+"<span class='am-icon-close'></span> "
											//+"<span onclick=\"editStepTitle('"+uid+"',this)\" class='am-icon-edit'></span>"
											+"<a href='javascript: void(0)'>"
												+"<input id='input"+uid+"' maxlength='20' style='display:none' type='text' onblur='toSpan(this)' name='stepTitleInput' class='inputtext' placeholder='请输入标签' />" 
												+"<span id='span"+uid+"' name='stepTitleSpan'>"+subTitle+"</span>"
											+"</a></li>";
								var content = "<div class='topicBox am-tab-panel'> "
												+ " <span>&nbsp;回答：</span> "
												+ " <script id='"+uid+"' type='text\/plain' style='width:100%;height:400px;'><\/script></div>"; 
								$nav.append(nav);
								$bd.append(content);
								$tab.tabs('refresh');
								initUE(uid);
								//$("li[name='stepTab']").eq(i).find("#span"+uid).html(step.title);
							}
						}
					}
				}
			}
		});
	}
}
	
function reTopicId(){
	return topicIdParam;
}

function reTemplateId(){
	return templateId;
}

//登录成功后显示答案
function showAnswer(state,person){
	if(state == "3"){
		getContent(indexParam);
	}
}
	
// 保存数据
function saveData() {
	var arry = new Array();
	$("li[name='stepTab']").each(function(i,item){
		var uid = $(this).attr("uid");
		var title = $(this).attr("title");
		var index = $(this).attr("index");
		var flg = $(this).attr("showflg"); 
		var stepId = $(this).find("input[name='stepId']").val();
		var etid = topicIdParam;
		var standardAnswer = "";
		if(flg == "1"){
			standardAnswer = UE.getEditor(uid).getContent();// 答案
		}else{
			standardAnswer = stepArr[i].standardAnswer;
		}
		var obj = new Object();
		obj.etid = etid;// 题目ID
		obj.standardAnswer = standardAnswer;// 正确答案
		obj.title = title;// title
		obj.id = stepId;// 步骤ID
		obj.eid = eidParam;// 试卷ID
		arry.push(obj);
	});
	
	// 转换为json字符串
	var jsStr = JSON.stringify(arry);
	
	$.ajax({
		type:"POST",
		contentType : "application/json;charset=UTF-8",
		url:'/${projectName}/practice/practiceAnswer/saveSxAnswer.action',
		data:jsStr,
		dataType:'json',
		success:function(result){
			if(result.success){
				var steps = result.data;
				swal({ 
					title: "保存成功", 
					type: "success"});
				if(steps != null && steps.length > 0){
					for(var i = 0 ; i < steps.length; i++){
						var step = steps[i];
						var tab = $("li[name='stepTab']").eq(i);
						if(tab != null && typeof(tab) != "undefined" ){
							tab.find("input[name='stepId']").val(step.id);
						}
					}
				}
			}else{
				swal({ title: result.msg,
					type: "error"});
			} 
		}
	});
}

// 清空数据
function clearData() {
	UE.getEditor('editor').setContent('');
}
	
// 登录成功后显示老师评价或者学生评价
function doChange(state){
	if(state != null && state != ""){
		stateParam = state;
		if( state == "2" || state == "x"){
			$("#teaIfreme").css("display","block");
			$("#callIfreme").css("display","block");
			$("#stuIfreme").css("display","none");
			$("#campcall").css("display","none");
			$(".am-tabs").hide();
		}
		if( state == "3" ){
			$("#teaIfreme").css("display","none");
			$("#callIfreme").css("display","none");
			$("#campcall").css("display","block");
			$("#stuIfreme").css("display","block");
			//setTimeout("autoSave()", 3000);
		}
	}
	
	if(topicIdParam != null && typeof(topicIdParam) != "undefined"){
		showTeaOrStu(topicIdParam);
	}

}
	
function autoSave(){
	var stepId = $("input[name='stepId']").val();// stepId
	var answerId = $("input[name='answerId']").val();// stepId
	var u2 = UE.getEditor('editor').getContent();// 答案
	$.ajax({
		url : "/${projectName}/examination/exerStep/saveStep.action" , type : "post" , data : {
			"id" : stepId, "standardAnswer" : u2
		,"answerId":answerId} , success : function(result) {
			if(result.success){
				$("input[name='stepId']").val(result.stepId);
				$("input[name='answerId']").val(result.answerId);
				
			}else{
			}
		}
	}); 
	setTimeout("autoSave()", 5000);//三秒钟自动保存一次
}
	
// 显示
function showTeaOrStu( topicId ){
	if( stateParam == "3" ){
		var src = $("#stuIfreme").attr("data-url");
		src = src + "&bookId="+topicIdParam;
		$("#stuIfreme").attr("src",src);
		// 老师登录隐藏实训题
		$('.tab-content-active .topicBox').css("display","block");
		$('.tab-content-active .operateBtn').eq(0).css("display","block");
	}
	if( stateParam == "2" ){
		var src = $("#teaIfreme").attr("data-url");
		var src1 = $("#callIfreme").attr("data-url");
		src = src + "&bookId="+topicIdParam;
		$("#teaIfreme").attr("src",src);
		$("#callIfreme").attr("src",src1);
		// 老师登录隐藏实训题
		$('.tab-content-active .topicBox').eq(1).css("display","none");
		$('.tab-content-active .operateBtn').eq(0).css("display","none");
	}
}

//********************************** 实训营地 end****************************
	
function cahngeHeight(height){
		// $("#teaIfreme").css('height',height);
		// $("#callIfreme").css('height',height);
}

//**************************************socket  start****************************
var websocket = null;
//判断当前浏览器是否支持WebSocket
if ('WebSocket' in window) {
	var agentId = "${agentId}";
	if(agentId != null && agentId != "" && typeof(agentId) != "undefined"){
		websocket = new WebSocket("wss://"+document.location.host+"/${projectName}/websocket?agentId=${agentId}");
	}
}else {
	alert('当前浏览器 Not support websocket')
}
	
if(websocket != null){
	//连接发生错误的回调方法
	websocket.onerror = function () {
		setMessageInnerHTML("WebSocket连接发生错误");
		var agentId = "${agentId}";
		if(agentId != null && agentId != "" && typeof(agentId) != "undefined"){
			websocket = new WebSocket("wss://"+document.location.host+"/${projectName}/websocket?agentId=${agentId}");
		}
	};
	
	//连接成功建立的回调方法
	websocket.onopen = function () {
		setMessageInnerHTML("WebSocket连接成功");
		heartCheck.start();
	}

	//接收到消息的回调方法
	websocket.onmessage = function (event) {
		setMessageInnerHTML(event.data);
		heartCheck.reset();
	}
	
	//连接关闭的回调方法
	websocket.onclose = function () {
		setMessageInnerHTML("WebSocket连接关闭");
	}

	//监听窗口关闭事件，当窗口关闭时，主动去关闭websocket连接，防止连接还没断开就关闭窗口，server端会抛异常。
	/* window.onbeforeunload = function (event) {
		closeWebSocket();
	} */

	//将消息显示在网页上
	function setMessageInnerHTML(innerHTML) {
		var agentId = "${agentId}";
		if(agentId != "" && innerHTML == agentId){
			loadSerData(true);
		}
		if(innerHTML == "change"){
			$("#show").empty();
			var html = '<div class="monitorBox"><span style="text-align: center;display: block;font-size:20px;padding: 20px 0;">通话列表</span>';
			html += '<div class="monitorCon">';
			$("#show").append(html);
			clearTimeout(intervalID);
			var intervalID =  setTimeout(loadLisen(),1000);
		}
	}

	//关闭WebSocket连接
	function closeWebSocket() {
		websocket.close();
	}

	//发送消息
	function send() {
		/* var message = document.getElementById('text').value; */
		websocket.send(message);
	}
}
		
// 心跳
var heartCheck = {
	timeout: 10000,//5ms
	timeoutObj: null,
	reset: function(){
		clearTimeout(this.timeoutObj);
		this.start();	
	},
	start: function(){
		this.timeoutObj = setTimeout(function(){
			var agentId = "${agentId}";
			if(websocket != null){
				if(agentId != null && agentId != "" && typeof(agentId) != "undefined"){
					websocket.send("HeartBeat");
				}
			}
		
		}, this.timeout)
	}
}
	
	
function loadLisen(){
	var src = $("#callIfreme").attr("src");
	if($.trim(src) != "" ){
		callIfreme.window.loadStudCalling();
	}
}
	
	
//**************************socket end*************************************
	
// 显示录音列表
function showVideos(taskId,stuNum){
	$("#fade").show();
	$("#vdeioDiv").show();
	$("#vdeioTable").find("tbody").html("");
	$.ajax({
		url : "/${projectName}/camp/trainCamp/getRecords.action" , 
		type : "post" , 
		data : {"taskId":taskId, "toNum":stuNum} , 
		success : function(result) {
			if(result.success){
				var data = result.data;
				if(data != null){
					var trHtml = "";
					for(var i = 0; i < data.length; i++){
						var tr = "<tr style='border:1px solid #00F;'>" 
									+ "<td style='border: 1px #ccc solid;'>"+data[i].fromNum+"</td>" 
									+ "<td style='border: 1px #ccc solid;'>"+data[i].toNum+"</td>" 
									+ "<td style='border: 1px #ccc solid;'>"+data[i].createTime+"</td>"
									+ "<td style='border: 1px #ccc solid;'><audio controls='controls'><source src='"+data[i].voice+"' type='audio/mpeg'/></audio></td>" 
								+ "</tr>";
						trHtml += tr;
						
					}
					$("#vdeioTable").find("tbody").html(trHtml);
				}
			}
		}
	});
}
	
function showOrderList(taskId,stuNum){
	$("#fade").show();
	$("#showOrderDiv").show();
	$("#showOrderTable").find('tbody').html("");
	var type = $("input[name='Ordertype']").val();
	// 清空tbody
	$("#showOrderTable").find('tbody').empty();
	$.ajax({
		type : "POST" ,
		url : '/'+projectName+'/practical/workorder/serchBillByStuNum.action' ,
		data : {"stuNum":stuNum,"taskId":taskId,"type":type} ,
		async: false,
		dataType : 'json' ,
		success : function(result) {
			if (result != null && result.rows != null ) {
				var trHtml = "";
				var rows = result.rows;
				for (var i = 0; i < rows.length; i++) {
					var hide = $("#copyDiv").html();
					var tr = "<tr style='border:1px solid #00F;'>" 
								+ "<td><a href=\"javascript:showBills('"+rows[i].id+"')\">" + rows[i].number + "</a></td>" 
								+ "<td>" + rows[i].tmYear + "</td>" 
								+ "<td>" + rows[i].tmHour + "</td>"
								+ "<td>" + rows[i].business + "</td>" 
								+ "<td>" + rows[i].department + "</td>" 
								+ "<td>" + rows[i].userid + "</td>" 
							+ "</tr><tr name='hide"+rows[i].id+"' class='hideTr' style='display:none;text-align:center;'><td colspan='6'>"+hide+"</td><tr>";
					trHtml += tr;
				}
				$("#showOrderTable").find('tbody').html(trHtml);
			}
		}
	});
}
	
// 显示所选工单
function showBills(billId){
	$.ajax({
		type : "POST" ,
		url : '/${projectName}/practical/workorder/serchBillById.action' ,
		data : {"id":billId} ,
		async: false,
		dataType : 'json' ,
		success : function(result) {
			if(result != null){
				var temp = $("#showOrderTable").find("tr[name='hide"+billId+"']").find("td");
				temp.find("span[name='number']").text(result.number);
				temp.find("span[name='userid']").text(result.userid);
				temp.find("span[name='tmstr']").text(result.tmstr);
				temp.find("span[name='callTime']").text(result.callTime);
				temp.find("span[name='dialing']").text(result.dialing);
				temp.find("span[name='contact']").text(result.contact);
				temp.find("span[name='business']").text(result.business);
				temp.find("span[name='result']").text(result.result);
				temp.find("span[name='descrip']").text(result.descrip);
				temp.find("span[name='orderAllot']").text(result.orderAllot);
				temp.find("span[name='acceptType']").text(result.acceptType);
				temp.find("span[name='directoryFir']").text(result.directoryFir);
				temp.find("span[name='directorySec']").text(result.directorySec);
				temp.find("span[name='department']").text(result.department);
			}else{
				$("#showOrderTable").find("tr[name='hide"+billId+"']").find("td").html("查询错误");
			}
			$("#showOrderTable").find("tr[class='hideTr']").each(function(){
				var na = 'hide'+billId;
				if($(this).attr("name") == na){
					$(this).toggle("fast");
				}else{
					$(this).hide();
				}
			})
		}
	});
}
	
	
// 关闭录音列表
function closeVedioDiv(){
	$("#fade").hide();
	$("#vdeioTable").find("tbody").find("audio").each(function(){
		$(this).remove();
	})
	$("#vdeioDiv").hide();
}
	
// 关闭录音列表
function closeOrderListDiv(){
	$("#fade").hide();
	$("#showOrderTable").find("tbody").find("audio").each(function(){
		$(this).remove();
	})
	$("#showOrderDiv").hide();
}
	
// 弹出框变量
var layerIndex = "";
// 点选学生
function chooseStuds(){
	layerIndex = layer.open({
		type: 2,
		shadeClose: true, //点击遮罩关闭
		skin: 'layui-layer-lan', //样式类名
		closeBtn: 1, //不显示关闭按钮
		area: ['800px', '520px'],
		anim: 5,
		title: false,
		content: '/${projectName}/book/studList.jsp',
	}); 
}

// 关闭弹出窗口
function closeLay(){
	layer.close(layerIndex);
}
</script>
</head>

<body >
<input type="hidden" name="currUser" value="${agentId}" />
<input type="hidden" name="Ordertype" value="1" />
<div class="wrap">
	<div class="content-box">
		<jsp:include page="_header.jsp" />

		<div class="banner">
			<div class="inner">
				<ul class="nav">
					<li><a href="/${projectName}/book/index.jsp">首页</a></li>
					<li>&nbsp;/&nbsp;</li>
					<li><a href="/${projectName}/book/book/page.action?page=catalog&bookRoot=${bookRoot.id}">课程目录</a></li>
					<li>&nbsp;/&nbsp;</li>
					<li>${book.name}</li>
				</ul>
				<span class="bookTitle">${bookRoot.name}</span>
				<dl>
					<dd>
						<div class="teacherInfo">
							<span>作者：${bookRoot.editor}</span>
							<i>|</i>
							<span>出版社：${bookRoot.bookconcern}</span>
							<i>|</i>
							<span>出版时间：<fmt:formatDate value="${bookRoot.publictime}" pattern="yyyy-MM-dd"/></span>
						</div>
						<p title="${bookRoot.remarks}" >${bookRoot.remarks}</p>
					</dd>
				</dl>
			</div>
		</div>

		<div class="main bg">
			<div class="detailBox inner">
				<jsp:include page="moduleTab.jsp" />

				<div class="tabsBox clearfix" id="tabs-basic">
					<div class="tabsInfo border-right pull-left">
						<div class="con tab-content-active" style="display:none;">
							<input type="hidden" name="answerId" value="" />
							
							<!-- 实训营地富文本部分 start -->
							<div class="topicBox">
								<!-- <span>&nbsp;题干：</span> -->
								<div id="question"></div>
							</div>
							<!-- <div class="topicBox">
								<span>&nbsp;回答：</span>
								<script id="editor2" type="text/plain" style="width:100%;height:400px;"></script>
							</div> -->
							
							<div class="am-tabs" data-am-tabs="{noSwipe: 1}" id="doc-tab-demo-1">
								<button type="button" class="am-btn am-btn-primary js-append-tab">添加</button>
								<ul class="am-tabs-nav am-nav am-nav-tabs">
								<li uid="editor" name="stepTab" showflg="1">
									<input type="hidden" name="stepId" value="" />
									<!-- <span onClick="editStepTitle('editor',this)" class="am-icon-edit"></span> -->
									<a href="javascript: void(0)">
										<!-- <input type="text" class="inputtext" value="tab" palceholder="请输入标签"> -->
										<input id='inputeditor' style='display:none' type='text' onblur="toSpan(this)" name='stepTitleInput' class='inputtext' placeholder='请输入标签' /> 
										<span id='spaneditor' name='stepTitleSpan'></span> 
									</a>
								</li>
							</ul>
								<div class="am-tabs-bd">  
									<div class="topicBox am-tab-panel">
										<span>&nbsp;回答：</span>
										<script id="editor" type="text/plain" style="width:100%;height:400px;"></script>
									</div>
								</div>
							</div>
							
							<div class="operateBtn" style="margin-bottom: 40px;">
								<c:if test="${!empty person}">							
									<button id="saveBtn" onclick="saveData();" type="button">保存</button>
								</c:if>
								<!-- <button onclick="clearData();" type="button">重置</button> -->
							</div>
							<!-- 实训营地富文本部分 end -->
							
							<!-- 通话操作部分 start -->
							<c:if test="${CURRENT_USER.state == '3'}">	
								<hr class="hr-dashed">
								<div class="callBox" id="campcall" style="display:none;">
									<h4>通话实训</h4>
									<div class="operateBtn" align="center">
									坐席号码：&nbsp;<input name="agentId" onclick="chooseStuds()" readonly="readonly" style="width:100px;border-top:0px;border-left:0px;border-right:0px;border-bottom:1px solid #000"
									type="text" value="" placeholder="请输入坐席号码" />
										<button type="button" id="cusOrderId">客户信息</button>
										<button type="button" id="callId">拨打</button>
										<button type="button" id="serOrderId">弹出工单</button>
									</div>
								</div>
							</c:if>
							<!-- 通话操作部分 end -->
							
							<hr class="hr-dashed">
							<!-- 加载的iframe部分 start -->
							<div>
								<!-- 学生评价 -->
								<iframe id="stuIfreme" name="stuIfreme" data-url="/${projectName}/camp/trainCamp/page.action?page=xssxpj" src=""
									style="display:none; width:100%;height:645px;margin-bottom:10px;" frameborder="no"></iframe>
								<!-- 老师监听 -->
								<iframe id="callIfreme" name="callIfreme" data-url="/${projectName}/dialog/page.action?page=listen" src=""
									style="display:none; width:100%;height:288px;margin-bottom:10px;" frameborder="no"></iframe>
								<!-- 老师评价 -->
								<iframe id="teaIfreme" name="teaIfreme" data-url="/${projectName}/camp/trainCamp/page.action?page=xssxpj_teacher" src=""
									style="display:none; width:100%;height:720px;margin-bottom:10px;" frameborder="no"></iframe>
							</div>
							<!-- 加载的iframe部分 end -->
							
						</div>
						
						<!-- 无数据 -->
						<div id="noData" style="text-align:center;display:none;height:500px;padding-top: 50px;">
							<img src='/${projectName}/book/js/layui/img/nocon2.png'>
							<p style='text-align:center;font-size: 24px;color: #999999;margin-top: 60px;'>暂无内容哦！</p>
						</div>
					</div>
					
					<div class="tabsright pull-right">
						<h6>实训列表</h6>	
						<ul class="tabsList"></ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	
<jsp:include page="_footer.jsp" />

<!-- 弹出框 -->
<jsp:include page="./pop.jsp" />
<div id="fade" class="black_overlay"></div>
<jsp:include page="./order_template.jsp" />

<div id="vdeioDiv" class="white_content">
	<h5>通话列表</h5>
	<a href="javascript:void(0)" onclick="closeVedioDiv();" style="font-size: 20px;color: #fff;position: absolute;right: 15px;top: 14px;" >×</a>
	<div style="padding: 10px 20px 10px; height: 400px; overflow: auto;">
		<table id="vdeioTable" style="text-align:center;width:100%;">
			<thead>
				<tr style='border: 1px #ccc solid;'>
					<th style='border: 1px #ccc solid;'>拨打人</th>
					<th style='border: 1px #ccc solid;'>接听人</th>
					<th style='border: 1px #ccc solid;'>拨打时间</th>
					<th style='border: 1px #ccc solid;'>操作</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>
</div>

<div id="showOrderDiv" class="white_content">
	<h5>工单列表</h5>
	<a href="javascript:void(0)" onclick="closeOrderListDiv();" style="font-size: 20px;color: #fff;position: absolute;right: 15px;top: 14px;" >×</a>
	<div style="padding: 10px 20px 10px; height: 400px; overflow: auto;">
		<table id="showOrderTable" style="text-align:center;width:100%;">
			<thead>
					<tr>
						<th>工单号码</th>
						<th>来电日期</th>
						<th>来电时间</th>
						<th>工单类型</th>
						<th>所属部门</th>
						<th >受理人</th>
					</tr>
				</thead>
				<tbody></tbody>
		</table>
	</div>
</div>

<div id="copyDiv" style="display: none;">
	<table id="temp" class="search-data search-data-tab">
		<tr>
			<td align="left" style="padding-left:10px;"><strong>单号：</strong><span name="number" ></span></td>
			<td align="left" style="padding-left:10px;"><strong>电话：</strong><span name="contact" ></span></td>
			<td align="left" style="padding-left:10px;"><strong>来电时间：</strong><span name="tmstr" ></span></td>
		</tr>
		<tr>
			<td align="left" style="padding-left:10px;"><strong>受理人：</strong><span name="userid" ></span></td>
			<td align="left" style="padding-left:10px;"><strong>受理方式：</strong><span name="acceptType" ></span></td>
			<td align="left" style="padding-left:10px;"><strong>归属部门：</strong><span name="department" ></span></td>
		</tr>
		<tr>
			<td align="left" style="padding-left:10px;"><strong>工单类型：</strong><span name="business" ></span></td>
			<td align="left" style="padding-left:10px;"><strong>一级目录：</strong><span name="directoryFir" ></span></td>
			<td align="left" style="padding-left:10px;"><strong>二级目录：</strong><span name="directorySec" ></span></td>
		</tr>
		<tr>
			<td align="left" style="padding-left:10px;" colspan="3"><strong>问题描述：</strong><span name="descrip" ></span></td>
		</tr>
		<tr>
			<td align="left" style="padding-left:10px;" colspan="3"><strong>处理结果：</strong><span name="result" ></span></td>
		</tr>
		<tr>
			<td align="left" style="padding-left:10px;" colspan="3"><strong>工单分配：</strong> <span name="orderAllot" ></span></td>
		</tr>
	</table>
</div>

</body>
<script type="text/javascript"	src="/${projectName}/book/js/common/login.js"></script>
<script type="text/javascript">
	function updZp(zpf){
		layer.open({
		    type: 2,
		    shadeClose: true, //点击遮罩关闭
		    skin: 'layui-layer-lan', //样式类名
		    closeBtn: 1, //不显示关闭按钮
		    area: ['780px', '780px'],
		    anim: 5,
		    title: false,
		    content: '/${projectName}/camp/trainCamp/page.action?page=xssxpj_zp&topicId='+topicIdParam+'&zpf='+zpf+'&templateId='+templateId,
	  	});
	}
	function updXzp(id,stus){
		if(id==-1){
			layer.open({
			    type: 2,
			    shadeClose: true, //点击遮罩关闭
			    skin: 'layui-layer-lan', //样式类名
			    closeBtn: 1, //不显示关闭按钮
			    area: ['780px', '780px'],
			    anim: 5,
			    title: false,
			    content: '/${projectName}/camp/trainCamp/page.action?page=xssxpj_xzp&topicId='+topicIdParam+'&stus='+stus+'&templateId='+templateId,
		  	});
		}else{
			layer.open({
			    type: 2,
			    shadeClose: true, //点击遮罩关闭
			    skin: 'layui-layer-lan', //样式类名
			    closeBtn: 1, //不显示关闭按钮
			    area: ['780px', '780px'],
			    anim: 5,
			    title: false,
			    content: '/${projectName}/camp/trainCamp/page.action?page=xssxpj_xzp_upd&topicId='+topicIdParam+'&pfid='+id+'&templateId='+templateId,
		  	});
		}
	}
	function viePj(){
		layer.open({
		    type: 2,
		    shadeClose: true, //点击遮罩关闭
		    skin: 'layui-layer-lan', //样式类名
		    closeBtn: 1, //不显示关闭按钮
		    area: ['910px', '730px'],
		    anim: 5,
		    title: false,
		    content: '/${projectName}/camp/trainCamp/page.action?page=xssxpj_ckpj&topicId='+topicIdParam+'&templateId='+templateId,
	  	});
	}
	function updTcp(stu,tpid,teacher){
		if(tpid==-1){
			layer.open({
			    type: 2,
			    shadeClose: true, //点击遮罩关闭
			    skin: 'layui-layer-lan', //样式类名
			    closeBtn: 1, //不显示关闭按钮
			    area: ['780px', '780px'],
			    anim: 5,
			    title: false,
			    content: '/${projectName}/camp/trainCamp/page.action?page=xssxpj_tcp&topicId='+topicIdParam+'&stuNumber='+stu+'&templateId='+templateId,
		  	});
		}else{
			layer.open({
			    type: 2,
			    shadeClose: true, //点击遮罩关闭
			    skin: 'layui-layer-lan', //样式类名
			    closeBtn: 1, //不显示关闭按钮
			    area: ['780px', '780px'],
			    anim: 5,
			    title: false,
			    content: '/${projectName}/camp/trainCamp/page.action?page=xssxpj_tcp_upd&topicId='+topicIdParam+'&stuNumber='+stu+'&teacher='+teacher+'&templateId='+templateId,
		  	});
		}
	}
	function closepj(){
		layer.closeAll('iframe'); //关闭弹窗
	}
	function reload(){
		layer.closeAll('iframe'); //关闭弹窗
		parent.stuIfreme.location.reload();
	}
	function closetpj(){
		layer.closeAll('iframe'); //关闭弹窗
		parent.teaIfreme.getRecords();
		/* // parent.teaIfreme.location.reload(); */
	}
</script>
</html>
