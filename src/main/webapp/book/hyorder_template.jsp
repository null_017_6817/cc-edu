<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<style>
	#cusInfoForm .search-data input,
	#billAddForm .search-data input,select,
	#billTable .search-data input,
	#cusInfoForm .search-data select {
		border: 1px #ccc solid;
	}
</style>
		<!-- cus -->
		<div id="dxWjDiv" class="white_content" style="overflow-y: hidden;">
			<h3>问卷</h3>
			<a href="javascript:closeDxwj();" style="float: right;margin-top: -12px;margin-right: 5px;" >×</a>
			<a href="javascript:closeDxwj();" style="float: right;margin-top: -22px;margin-right: 10px;" >×</a>
			<div class="content" style="max-height:400px;overflow-y: auto;"></div>
			<div class="optDiv" style="min-height:40px;">
				<button  type="button" onclick="preCall();">拨打</button>
				<button  type="button" onclick="saveWj();">保存</button>
			</div>
		</div>
		
		<!-- 客户打电话弹出部分--开始 -->
		<div id="cusOrderDiv" class="white_content">
			<h5>客户信息</h5>
			<form id="orderForm" action="" method="post">
				<input type="hidden" name="id" />
				<input type="hidden" name="userid" />
				<div style="display:none;" align="center" style="padding: 20px 0;">
					行业 : <select name="profession" style="width: 162px; height: 32px;">
						<option value="">请选择则</option>
						<option value="industry_type_10001">航空</option>
						<option value="industry_type_10002">物流</option>
						<option value="industry_type_10003">电信</option>
					</select>
				</div>
				<table align="center" class="search-data" id="dataTable" >
					<tr>
						<td><label>姓名 :</label> <input type="text" name="name" value="" /></td>
						<td><label>性别 :</label> 
							<select name="sex" style="width: 162px; height: 32px;">
								<option value="">请选择则</option>
								<option value="1">先生</option>
								<option value="2">女士</option>
							</select>
						</td>
					</tr>
					<tr>
						<td><label>电话 :</label> <input type="text" name="telphone" value="" /></td>
						<td><label>公司 :</label> <input type="text" name="company" value="" /></td>
					</tr>
					<tr>
						<td><label>邮箱 :</label> <input type="text" name="email" value="" /></td>
						<td><label>邮编 :</label> <input type="text" name="zipcode" value="" /></td>
					</tr>
					<tr>
						<td><label>证件类型 :</label> 
							<select name="certType" style="width: 162px; height: 32px;">
								<option value="">请选择则</option>
								<option value="1">身份证</option>
								<option value="2">护照</option>
								<option value="3">军官证</option>
								<option value="4">士兵证</option>
								<option value="5">港澳居民来往内地通行证</option>
								<option value="6">台湾居民来往大陆通行证</option>
							</select>
						</td>
						<td><label>证件号码 :</label> <input type="text" name="certNum" value="" /></td>
					</tr>
					<tr>
						<td colspan="2"><label>产品归属地 :</label> <input type="text" name="proBelong" value="" style="width:452px;" /></td>
					</tr>
					<tr>
						<td colspan="2"><label>地址 :</label> <input type="text" name="addr" value="" style="width:452px;" /></td>
					</tr>
					<tr>
						<td colspan="2"><label>归属地 :</label> <input type="text" name="belong" value="" style="width:452px;" /></td>
					</tr>
				</table>
			</form>
			<div align="center" class="operateBtn">
				<button type="button" id="saveId">保存</button>
				<button type="button" id="cusCloseId">关闭</button>
			</div>
		</div>
		<!-----客户打电话弹出部分--结束-->
		
		<!-- 客服接电话弹出部分 --开始 -->
		<div id="serOrderDiv" class="Ser_white_content">
			<h5>行业工单</h5>
			<a id="serCloseId" title="关闭" href="javascript:void(0)"><img style="width:15px;height:15px;" src='/${projectName}/book/img/closeOrder.png' /></a>
			<a id="serMinId" title="最小化" href="javascript:void(0)" onclick="minDiv()" ><img style="width:15px;height:15px;" src='/${projectName}/book/img/minOrder.png' /></a>
			<a id="serMaxId" title="恢复窗口" href="javascript:void(0)" onclick="maxDiv()" ><img style="width:15px;height:15px;" src='/${projectName}/book/img/maxOrder.png' /></a>
			
			<div style="height: 560px; overflow: auto;">
				<!-- 客户信息部分 开始 -->
				<form id="cusInfoForm">
					<span class="small-title">客户信息</span>
					<table id="cusTable" class="search-data" >
						<input type="hidden" name="id" />
						<input type="hidden" name="userid" />
						<tr>
							<td  colspan="3"><label>行业 :</label><select name="profession" style="display:none; width: 150px; height: 28px;margin: 15px 0;">
								<option value="">请选择则</option>
								<option value="industry_type_10001">航空</option>
								<option value="industry_type_10002">物流</option>
								<option value="industry_type_10003">电信</option>
							</select>
							<span></span>
							</td>
						</tr>
						<tr>
							<td ><label>客户姓名 :</label><input type="text" name="name" value="" /></td>
							<td ><label>客户性别 :</label>
								<select name="sex">
									<option value="">请选择则</option>
									<option value="1">先生</option>
									<option value="2">女士</option>
								</select>
							</td>
							<td ><label>归属地 :</label><input type="text" name="belong" value="" /></td>
						</tr>
						<tr>
							<td><label>联系电话 :</label><input type="text" name="telphone" value="" /></td>
							<td><label>电子邮箱 :</label><input type="text" name="email" value="" /></td>
							<td><label>联系地址 :</label><input type="text" name="addr" value="" /></td>
						</tr>
						
						<tr>
							<td><label>证件类型 :</label>
								<select name="certType" style="width: 162px; height: 32px;">
									<option value="">请选择则</option>
									<option value="1">身份证</option>
									<option value="2">护照</option>
									<option value="3">军官证</option>
									<option value="4">士兵证</option>
									<option value="5">港澳居民来往内地通行证</option>
									<option value="6">台湾居民来往大陆通行证</option>
								</select>
							</td>
							<td><label>证件号码 :</label><input type="text" name="certNum" value="" /></td>
							<td><label>邮政编码 :</label><input type="text" name="zipcode" value="" /></td>
						</tr>
						<tr>
							<td><label>客户编号 :</label><input type="text" name="cusNum" value="" /></td>
							<td><label>产品归属 :</label><input type="text" name="proBelong" value="" /></td>
							<td><label>客户级别 :</label>
								<select name="cusLevel">
									<option value="">请选择则</option>
									<option value="1">普通</option>
									<option value="2">VIP客户</option>
									<option value="3">银卡</option>
									<option value="4">金卡</option>
									<option value="5">钻卡</option>
									<option value="6">集团客户</option>
								</select>
							</td>
						</tr>
					</table>
					<div style="text-align: right;">
						<button type="button" name="cusInfoBtn" onclick="saveCusInfo();">保存</button>
					</div>
				</form>
				<!-- 客户信息部分 结束 -->
				
				<!-- 工单部分  开始 -->
				<div class="commonOeder">
					<form id="billAddForm">
						<span class="small-title">工单信息</span>
						
						<div style="text-align: right;">
							<button type="button" id="saveBillBtn" onclick="saveBill();">保存</button>
						</div>
					</form>
					
					<!-- 工单记录列表 开始 -->
					<span class="small-title">记录信息</span>
					<table id="billTable" style="text-align:center;width:100%;">
						<thead>
							<tr>
								<th>工单号码</th>
								<th>来电日期</th>
								<th>来电时间</th>
								<th>工单类型</th>
								<th>所属部门</th>
								<th>受理人</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
					<!-- 工单记录列表 结束 -->
				</div>
				<!-- 工单部分  结束 -->
			</div>
		</div>
		<!-- 客服接电话弹出部分--结束 -->
		
		<!-- common Copy 开始 -->
		<div id="copyDiv" style="display:none;">
			<!-- 航空部分开始 -->
			<table name="air" class="search-data search-data-tab" >
				<tr>
					<td>
						<label>出发城市:</label><input type="text" name="departureCity" value="" />
						<input type="hidden" name="taskId" value="" />
						<input type="hidden" name="type" value="industry_type_10001" />
					</td>
					<td><label>到达城市 :</label><input type="text" name="arriveCity" value="" /></td>
					<td><label>出发日期 :</label><input type="text" class="Wdate" onclick="WdatePicker({isShowClear:false,readOnly:true,dateFmt:'yyyy-MM-dd HH:mm:ss'})" name="departureDateStr" /></td>
				</tr>
				<tr>
					<td><label>航空公司 :</label><input type="text" name="airlineComp" value="" /></td>
					<td><label>客户固话 :</label><input type="text" name="customerTel" value="" /></td>
					<td><label>客户级别 :</label><input type="text" name="customerLevel" value="" /></td>
				</tr>
				<tr>
					<td><label>工单编号 :</label><input type="text" name="number" value="" /></td>
					<td><label>一级 :</label>
						<select name="directoryFir" >
							<option value="1" >咨询工单</option>
							<option value="2" >查询工单</option>
							<option value="3" >受理工单</option>
							<option value="4" >投诉工单</option>
						</select>
					</td>
					<td><label>二级 :</label><input type="text" name="directorySec" value="" /></td>
				</tr>
				
				<tr>
					<td><label>来电号码 :</label><input type="text" name="contact" value="" /></td>
					<td><label>所属区域 :</label><input type="text" name="department" value="" /></td>
					<td><label>呼入时间 :</label><input type="text" class="Wdate" onclick="WdatePicker({isShowClear:false,readOnly:true,dateFmt:'yyyy-MM-dd HH:mm:ss'})" name="tmstr" /></td>
				</tr>
				
				<tr>
					<td colspan="3"><label>问题描述 :</label><input style="width: 89%;" type="text" name="descrip" value="" /></td>
				</tr>
				<tr>
					<td colspan="3"><label>处理结果 :</label><input style="width: 89%;" type="text" name="result" value="" /></td>
				</tr>
				<tr>
					<td colspan="3" >
						<label>工单分配 :</label>
						<span>
							<label style="width:90px"><input type="radio" style="width:15px;vertical-align: middle ;" name="orderAllotD" data="A1" value="A1" />完结&nbsp;&nbsp;</label>
							<label style="width:90px"><input type="radio" style="width:15px;vertical-align: middle ;" name="orderAllotD" data="A2" value="A2" />出票&nbsp;&nbsp;</label>
							<label style="width:90px"><input type="radio" style="width:15px;vertical-align: middle ;" name="orderAllotD" data="A3" value="A3" />修改&nbsp;&nbsp;</label>
							<label style="width:90px"><input type="radio" style="width:15px;vertical-align: middle ;" name="orderAllotD" data="A4" value="A4" />取消&nbsp;&nbsp;</label>
							<label style="width:90px"><input type="radio" style="width:15px;vertical-align: middle ;" name="orderAllotD" data="A5" value="A5" />转投诉部</label>
						</span>
					</td>
				</tr>
			</table>
			<!-- 航空部分结束 -->
			
			<!-- （logistics）物流 部分开始 -->
			<table name="logistics" class="search-data search-data-tab" >
				<tr>
					<td><label>工单编号 :</label>
						<input type="text" name="number" value="" />
						<input type="hidden" name="taskId" value="" />
						<input type="hidden" name="type" value="industry_type_10002" />	
					</td>
					<td><label>一级 :</label>
						<select name="directoryFir" >
							<option value="1" >咨询工单</option>
							<option value="2" >查询工单</option>
							<option value="3" >受理工单</option>
							<option value="4" >投诉工单</option>
						</select>
					</td>
					<td><label>二级 :</label><input type="text" name="directorySec" value="" /></td>
				</tr>
				
				<tr>
					<td><label>来电号码 :</label><input type="text" name="contact" value="" /></td>
					<td><label>所属区域 :</label><input type="text" name="department" value="" /></td>
					<td><label>呼入时间 :</label><input type="text" name="tmstr" value="" /></td>
				</tr>
				<tr>
					<td colspan="3"><label>订单信息:</label><input style="width: 89%;" type="text" name="orderInfo" value="" /></td>
				</tr>
				<tr>
					<td colspan="3"><label>问题描述 :</label><input style="width: 89%;" type="text" name="descrip" value="" /></td>
				</tr>
				<tr>
					<td colspan="3"><label>处理结果 :</label><input style="width: 89%;" type="text" name="result" value="" /></td>
				</tr>
				<tr>
					<td colspan="3">
						<label>工单分配 :</label>
						<span>
							<label ><input type="radio" style="width:15px;vertical-align: middle ;" name="orderAllotD" data="L1" value="L1" />完结&nbsp;&nbsp;</label>
							<label ><input type="radio" style="width:15px;vertical-align: middle ;" name="orderAllotD" data="L2" value="L2" />转派揽收部</label>
							<label ><input type="radio" style="width:15px;vertical-align: middle ;" name="orderAllotD" data="L3" value="L3" />转派投递部</label>
							<label ><input type="radio" style="width:15px;vertical-align: middle ;" name="orderAllotD" data="L4" value="L4" />转派上级单位</label>
							<label ><input type="radio" style="width:15px;vertical-align: middle ;" name="orderAllotD" data="L5" value="L5" />转派投诉部</label>
						</span>
					</td>
				</tr>
			</table>
			<!-- （logistics）物流 部分 结束 -->
		</div>
		<!-- copy 部分结束 -->
		
	<script>
	//***********************common 拖拽开始**********************
	var objDrag;
	var mouseD;
	var objDiv = document.getElementById("serOrderDiv"); 
	var isIE = document.all ? true : false;//判断浏览器类型 
	document.onmousedown = function(evnt) {//当鼠标左键按下后执行此函数 
		var evnt = evnt ? evnt : event; 
		if (evnt.button == (document.all ? 1 : 0)) { 
			mouseD = true;//mouseD为鼠标左键状态标志，为true时表示左键被按下 
		} 
	} 
	objDiv.style.top = "15%";//设置弹出层距离上边界的距离 
	objDiv.style.left = "50%";//设置弹出层距离左边界的距离 

	// 鼠标按键按下
	objDiv.onmousedown = function(evnt) { 
		objDrag = this;//objDrag为拖动的对象 
		var evnt = evnt ? evnt : event; 
		if (evnt.button == (document.all ? 1 : 0)) { 
			mx = evnt.clientX; 
			my = evnt.clientY; 
			objDiv.style.left = (objDiv.offsetLeft +400)+ "px"; 
			objDiv.style.top = objDiv.offsetTop + "px"; 
			if (isIE) { 
				objDiv.setCapture(); 
				// objDiv.filters.alpha.opacity = 75;//当鼠标按下后透明度改变 
			} else { 
				window.captureEvents(Event.MOUSEMOVE);//捕获鼠标拖动事件 
				// objDiv.style.opacity = 0.75;//当鼠标按下后透明度改变 
			} 
		}
	} 
	
	// 鼠标按键松启
	document.onmouseup = function() { 
		mouseD = false;//左键松开 
		objDrag = ""; 
		if (isIE) { 
			objDiv.releaseCapture(); 
			// objDiv.filters.alpha.opacity = 100;//当鼠标左键松开后透明度改变 
		} else { 
			window.releaseEvents(objDiv.MOUSEMOVE);//释放鼠标拖动事件 
			// objDiv.style.opacity = 1;//当鼠标左键松开后透明度改变 
		} 
	} 

	// 鼠标移动
	document.onmousemove = function(evnt) { 
		var evnt = evnt ? evnt : event; 
		if (mouseD == true && objDrag) { 
			var mrx = evnt.clientX - mx; 
			var mry = evnt.clientY - my; 
			objDiv.style.left = parseInt(objDiv.style.left) + mrx + "px"; 
			objDiv.style.top = parseInt(objDiv.style.top) + mry + "px"; 
			mx = evnt.clientX; 
			my = evnt.clientY; 
		} 
	} 

	// 工单窗口最小化
	function minDiv(){
		objDiv.style.width=200+"px";
		objDiv.style.height=50+"px";
		$("#serOrderDiv").find("div").eq(0).hide();
		$("#fade").hide();
	}

	// 工单恢复窗口
	function maxDiv(){
		objDiv.style.width="800px";
		objDiv.style.height="610px";
		$("#serOrderDiv").find("div").eq(0).show();
		objDiv.style.top = "15%";//设置弹出层距离上边界的距离 
		objDiv.style.left = "50%";//设置弹出层距离左边界的距离 
		$("#fade").show();
	}
	//********************************common 拖拽结束**********************
	
	//********************************cus start**********************
	function cusInit() {
		// 点击工单显示
		bindorderShow();
		// 关闭按钮
		bindcloseOrder();
		// 修改行业
		bindProfession();
		// 保存按钮
		bindSaveCus();
		// 拨号
		bindcall();
	}
	
	// 点击工单触发
	function bindorderShow(){
		//弹出按钮
		$("#cusOrderId").click(function() {
			var agentId = $(".callBox").find("input[name='agentId']").val();
			if(agentId != null && agentId != ""){
				if(currentIndustryType == 'industry_type_10003'){
					// 电信的场合
					showWj();
				}else{
					// 非电信的场合
					$.ajax({
						type : "POST",
						url : '/'+projectName+'/book/book/getCurrentUser.action',
						dataType:'json',
						success : function(result) {
							if(result != null && result.name != null){
								$("#fade").show();
								$("#cusOrderDiv").show();
								loadCusData($("select[name='profession']").val());
							}else{
								alert("请登录后操作");
							}
						}
					});
				 } 
			}else{
				alert("请先填写坐席号");
			}
		});
	}
	
	// 关闭按钮
	function bindcloseOrder(){
		//关闭按钮
		$("#cusCloseId").click(function() {
			$("#cusOrderDiv").hide();
			$("#fade").hide();
		});
	}
	
	// 修改行业
	function bindProfession(){
		// 下拉框绑定方法
		$("select[name='profession']").change(function() {
			var profession = $(this).val();
			if (profession != "" ) {
				loadCusData($(this).val());
				$("#dataTable").show();
			} else {
				$("#dataTable").hide();
			}
		});
	}
	
	// 绑定保存方法
	function bindSaveCus(){
		//保存
		$("#saveId").click(function() {
			saveCusData();
		});
	}
	
	// 绑定拨号
	function bindcall(){
		// 拨号
		$("#callId").click(function() {
			preCall();
		});
	}
	
	// 显示问卷
	function showWj(){
		$.ajax({
			url:'/${projectName}/camp/dxwj/getDxwj.action',
			data:{rid:currentTopic},
			type: "post",
			dataType:'json',
			success:function(result){
				if(result.success){
					var data = result.data;
					$("#dxWjDiv").find(".content").html("");
					$("#dxWjDiv").show();
					$("#fade").show();
					if(data != null){
						$("input[name='id']").val(data.id);
						var con = data.content;
						if(con != null){
							var dxwjJsons = con.dxwjJsons;
							if(dxwjJsons != null ){
								for(var i = 0; i < dxwjJsons.length; i++){
									var quest = dxwjJsons[i];
									var type = quest.type;
									addQuestion();
									var qDiv = $("#dxWjDiv").find(".content").find(".questionDiv").eq(i);
									qDiv.find("select[name='type']").val(quest.type);
									qDiv.find("select[name='type']").trigger("change");
									var textHtm = qDiv.find("select[name='type']").find("option:selected").text();
									qDiv.find("select[name='type']").parent('td').append(textHtm);
									qDiv.find("select[name='type']").css("display","none");
									qDiv.find("td[name='question']").html(quest.question);
									
									if(type == 3){
										alert(quest.answer);
										qDiv.find("textarea[name='answer']").html(quest.answer);
									}
									
									if(type == 1 || type == 2){
										var choices = quest.choices
										var inputType = "";
										if(type==1){
											inputType = "radio";
										}
										if(type==2){
											inputType = "checkbox";
										}
										
										if(choices != null){
											for( var j = 0 ; j < choices.length; j++){
												var choice = choices[j];
												var isChoose = choice.isChoose;
												var ck = ""
												if(isChoose == "true"){
													ck = "checked"
												}
												var html = "<tr name='choiceTr'>"
													+ "<td>  </td>"
													+ "<td name='choiceTd'><label class='labelBox'><input name='isChoose' "+ck+" type='"+inputType+"'><span class='" + inputType + "Input'></span><span name='choiseSpan'>"+choice.chooseTxt+"</span></label></td>"
													+ "</tr>";
												qDiv.find("table[name='questionTable']").append(html);
											}
										}
									}
								}
							}
						}
					}
				}else{
					alert(result.msg);
				}
			},
		});
	}
	
	// 添加题目
	function addQuestion(){
		var commonHtml = $("div[name='temp']").find('.question').html();
		commonHtml = "<div class='questionDiv'>"+commonHtml+"</div>";
		$(".content").append(commonHtml);
	}

	// 题型修改
	function changeType(t){
		var type = $(t).val();
		if(type != "" && type != null && type != "undefined" ){
			var table = $(t).parents("table[name='questionTable']").find('tr:gt(0)').remove();
			var question = $("div[name='temp']").find('.question'+type).find("table tbody").html();
			$(t).parents("table[name='questionTable']").append(question);
		}
	}
	
	// 添加选项
	function addChoice(t){
		var html = "<tr name='choiceTr'>"
				+ "<td>选项</td>"
				+ "<td name='choiceTd'><input type='text' value=''/>&nbsp;&nbsp;<button type='button' onclick='removeChoice(this);'>删除选项</button> </td>"
				+ "</tr>";
		$(t).parents("table[name='questionTable']").append(html);
	}

	// 保存问卷
	function saveWj(){
		var agentId = $(".callBox").find("input[name='agentId']").val();
		var cont = "rid="+123+"&taskid="+currentTopic+"&toagentid="+agentId;
		$(".content").find('.questionDiv').each(function(i,item){
			var type = $(this).find("select[name='type']").val();
			cont += "&content.dxwjJsons["+i+"].type="+type;
			if(type == 1 || type == 2){
				$(this).find("tr[name='choiceTr']").each(function(j,item){
					cont += "&content.dxwjJsons["+i+"].choices["+j+"].chooseTxt="
					+$(this).find("span[name='choiseSpan']").html()
					+"&content.dxwjJsons["+i+"].choices["+j+"].isChoose="
					+$(this).find("input[name='isChoose']").is(':checked');
				});
			}else{
				var answer = $(this).find("textarea[name='answer']").val();
				cont += "&content.dxwjJsons["+i+"].answer="+answer;
			}
			cont += "&content.dxwjJsons["+i+"].question="+$(this).find("td[name='question']").html();
			
		});
		
		$.ajax({
			url:'/${projectName}/camp/hyanswer/saveAnswer.action',
			data:cont,
			type: "post",
			dataType:'json',
			success:function(result){
				alert(result.msg);
			}
		});
	}
	
	// 关闭电信问卷页面
	function closeDxwj(){
		$("#dxWjDiv").hide();
		$("#fade").hide();
	}
	
	// 加载数据（客户打电话时弹出内容）
	function loadCusData(profession) {
		
		$("#orderForm").find("input").val("");
		$.ajax({
			type : "POST" , url : '/'+projectName+'/practical/workorder/serchCusOrder.action' , data : {
				"profession" : profession
			} , dataType : 'json' , success : function(result) {
				
				if (result != null && result.data != null ) {
					var entity = result.data;
					$("#orderForm").find("input[name='id']").val(entity.id);
					$("#orderForm").find("input[name='userid']").val(entity.userid);
					$("#orderForm").find("input[name='name']").val(entity.name);
					$("#orderForm").find("select[name='sex']").val(entity.sex);
					$("#orderForm").find("input[name='telphone']").val(entity.telphone);
					$("#orderForm").find("input[name='company']").val(entity.company);
					$("#orderForm").find("input[name='profession']").val(entity.profession);
					$("#orderForm").find("input[name='addr']").val(entity.addr);
					$("#orderForm").find("input[name='zipcode']").val(entity.zipcode);
					$("#orderForm").find("input[name='email']").val(entity.email);
					$("#orderForm").find("input[name='belong']").val(entity.belong);
					$("#orderForm").find("select[name='certType']").val(entity.certType);
					$("#orderForm").find("input[name='certNum']").val(entity.certNum);
					$("#orderForm").find("input[name='cusNum']").val(entity.cusNum);
					$("#orderForm").find("input[name='proBelong']").val(entity.proBelong);
				}
			}
		});
	}

	// 保存数据
	function saveCusData() {
		var flg = true;
		if(flg){
			var param = $("#orderForm").serialize();
			$.ajax({
				type : "POST" , url : '/'+projectName+'/practical/workorder/saveClient.action' , data : param , async: false ,dataType : 'json' , success : function(result) {
					if (result != null && result.success ) {
						var id = $("input[name='id']").val();
						if (id == null || id == "" ) {
							$("#orderForm").find("input[name='id']").val(result.id);
						}
						$("#cusOrderDiv").hide();
						$("#fade").hide();
						swal({title:result.msg,type:"success"});
						$("#callId").show();
					}
				}
			});
		}else{
			alert("数据未填写完");
		}
	}

	// 拨号
	function preCall() {
		$("#orderForm").find("select[name='profession']").val(currentIndustryType);
		var profession = currentIndustryType;
		var agentId = $("input[name='agentId']").val();
		var id = $("#orderForm").find("input[name='id']").val();
		
		if (agentId == null || agentId == "" ) {
			swal({title:"请填写您要拨打的坐席号",type:"warning"});
			return false;
		}
		
		$.ajax({
			type : "POST" , url : '/'+projectName+'/practical/workorder/serchCusOrder.action' , data : {
				"profession" : profession
			} , dataType : 'json' ,async: false , success : function(result) {
				
				if (result != null && result.data != null ) {
					var entity = result.data;
					$("#orderForm").find("input[name='id']").val(entity.id);
					$("#orderForm").find("input[name='userid']").val(entity.userid);
					$("#orderForm").find("input[name='name']").val(entity.name);
					$("#orderForm").find("select[name='sex']").val(entity.sex);
					$("#orderForm").find("input[name='telphone']").val(entity.telphone);
					$("#orderForm").find("input[name='company']").val(entity.company);
					$("#orderForm").find("input[name='profession']").val(entity.profession);
					$("#orderForm").find("input[name='addr']").val(entity.addr);
					$("#orderForm").find("input[name='zipcode']").val(entity.zipcode);
					$("#orderForm").find("input[name='email']").val(entity.email);
					$("#orderForm").find("input[name='belong']").val(entity.belong);
					$("#orderForm").find("select[name='certType']").val(entity.certType);
					$("#orderForm").find("input[name='certNum']").val(entity.certNum);
					$("#orderForm").find("input[name='cusNum']").val(entity.cusNum);
					$("#orderForm").find("input[name='proBelong']").val(entity.proBelong);
				}
			}
		});
		
		saveCusData();
		
		$.ajax({
			type : "POST" , url : '/'+projectName+'/practical/workorder/preCall.action' , data : {
				"profession" : profession , "agentId" : agentId,"taskId" : topicIdParam
			} , dataType : 'json' , success : function(result) {
				if (result != null ) {
					if (result.success ) {
						swal({title:result.msg,type:"success"});
						$("#sipexe").attr("src", "sip:"+agentId);
					} else {
						swal({title:result.msg,type:"error"});
					}
				}
			}
		});
	}
	
	function dxpreCall(){
		var agentId = $("input[name='agentId']").val();
		$.ajax({
			type : "POST" , 
			url : '/'+projectName+'/practical/workorder/preCall.action' , 
			data : {"profession" : profession , "agentId" : agentId,"taskId" : topicIdParam} , 
			dataType : 'json' , 
			success : function(result) {
				if (result != null ) {
					if (result.success ) {
						swal({title:result.msg,type:"success"}); 
						$("#sipexe").attr("src", "sip:"+agentId);
					 } else {
						swal({title:result.msg,type:"error"});
					}
				}
			}
		}); 
	}
	
	//********************************cus end**********************
	
	//********************************ser start**********************
	function serInit() {
		//弹出按钮
		$("#serOrderId").click(function() {
			loadSerData(false);
		});
		
		//关闭按钮
		$("#serCloseId").click(function() {
			$("#serOrderDiv").hide();
			$("#fade").hide();
		});
		// 挂断电话
		$("#hungId").click(function() {
			preHungUp();
		});
	}

	// 加载数据
	function loadSerData(load) {
		$.ajax({
			type : "POST" , url : '/'+projectName+'/practical/workorder/serSearchCus.action' , 
			data : {} , 
			dataType : 'json' , 
			success : function(result) {
				if (result != null && result.data != null ) {
					$("#serOrderDiv").show();
					$("#fade").show();
					
					// 如果load为true则重新加载数据
					$("#cusTable").find("input").val("");
					var entity = result.data;
					var profession = entity.profession;
					// 行业等于航空的场合
					if(profession == "industry_type_10001"){
						showAirOrder();
					}
					// 行业等于物流的场合
					if(profession == "industry_type_10002"){
						showLogisticsOrder();
					}
					
					if (entity.profession != null ) {
						$("#cusTable").find("select[name='profession']").val(entity.profession);
						var item = $("#cusTable").find("select[name='profession'] option:selected").text();
						if(entity.profession == ""){
							$("#cusTable").find("select[name='profession']").next('span').html(item);
						}else{
							$("#cusTable").find("select[name='profession']").next('span').html(item);
						}
					}else{
						$("#cusTable").find("select[name='profession']").next('span').html("未选择");
					}
					if (entity.id != null ) {
						$("#cusTable").find("input[name='id']").val(entity.id);
					}
					if (entity.userid != null ) {
						$("#cusTable").find("input[name='userid']").val(entity.userid);
					}
					if (entity.name != null ) {
						$("#cusTable").find("input[name='name']").val(entity.name);
					}
					if (entity.sex != null ) {
						$("#cusTable").find("select[name='sex']").val(entity.sex);
					}
					if (entity.telphone != null ) {
						$("#cusTable").find("input[name='telphone']").val(entity.telphone);
					}
					if (entity.company != null ) {
						$("#cusTable").find("input[name='company']").val(entity.company);
					}
					if (entity.profession != null ) {
						$("#cusTable").find("td[name='profession']").val(entity.profession);
					}
					if (entity.addr != null ) {
						$("#cusTable").find("input[name='addr']").val(entity.addr);
					}
					if (entity.zipcode != null ) {
						$("#cusTable").find("input[name='zipcode']").val(entity.zipcode);
					}
					if (entity.email != null ) {
						$("#cusTable").find("input[name='email']").val(entity.email);
					}
					if (entity.belong != null ) {
						$("#cusTable").find("input[name='belong']").val(entity.belong);
					}
					if (entity.certType != null ) {
						$("#cusTable").find("select[name='certType']").val(entity.certType);
					}
					if (entity.certNum != null ) {
						$("#cusTable").find("input[name='certNum']").val(entity.certNum);
					}
					if (entity.cusNum != null ) {
						$("#cusTable").find("input[name='cusNum']").val(entity.cusNum);
					}
					if (entity.cusLevel != null ) {
						$("#cusTable").find("select[name='cusLevel']").val(entity.cusLevel);
					}
					if (entity.proBelong != null ) {
						$("#cusTable").find("input[name='proBelong']").val(entity.proBelong);
					}
					
					$("#addBillTable").find("input").val("");// 清空之前的数据，重新加载新数据
					// 加载bill数据
					var len = loadBill(entity.profession);
					len++;
					var orderNum = result.timeStamp+len;
					$("#addBillTable").find("input[name='number']").val(orderNum);// 工单号
					$("#addBillTable").find("input[name='profession']").val(entity.profession);// 工单号
					$("#addBillTable").find("input[name='contact']").val(entity.telphone);//联系号码
					$("#addBillTable").find("input[name='tmstr']").val(result.callTime);//呼入时间
					$("#addBillTable").find("input[name='userid']").val(result.currentNum);// 受理人
					// 窗口正常大小显示
					maxDiv();
				} else {
					alert("没有电话");
				}
			}
		});
	}

	var tt = "";
	// 显示航空工单
	function showAirOrder( ){
		//var table =$("#copyDiv").find("table[name='air']").clone(true);
		var table = $("#copyDiv").find("table[name='air']")
		var tableHtml = "<table id='addBillTable' class='search-data' >"+table.html()+"</table>";
		$(".commonOeder").find("#billAddForm").find(".small-title").next("table").remove();
		$(".commonOeder").find("#billAddForm").find(".small-title").after(tableHtml);
		$(".commonOeder").find("#billAddForm").find("input[name='type']").val("industry_type_10001");
		tt = "industry_type_10001";
		$(".commonOeder").find("#billAddForm").find("input[name='orderAllot']").each(function(i,item){
			var val = $(this).attr("data");
			$(this).attr("value",val);
			this.value=val;
		});
	}
	
	// 显示物流工单
	function showLogisticsOrder( ){
		var table =$("#copyDiv").find("table[name='logistics']").clone(true);
		//var table = $("#copyDiv").find("table[name='logistics']")
		var tableHtml = "<table id='addBillTable' class='search-data' >"+table.html()+"</table>";
		$(".commonOeder").find("#billAddForm").find(".small-title").next("table").remove();
		$(".commonOeder").find("#billAddForm").find(".small-title").after(tableHtml);
		$(".commonOeder").find("#billAddForm").find("input[name='type']").val("industry_type_10002");
		tt = "industry_type_10002";
		$(".commonOeder").find("#billAddForm").find("input[name='orderAllot']").each(function(){
			var val = $(this).attr("data");
			$(this).attr("value",val);
		});
	}
	
	
	// 加载bill数据
	function loadBill(typeParam) {
		var len = 0;
		// 清空tbody
		$("#billTable").find('tbody').empty();
		$.ajax({
			type : "POST" ,
			url : '/'+projectName+'/practical/workorder/serchBill.action' ,
			data : {taskId:currentTopic,type:tt} ,
			async: false,
			dataType : 'json' ,
			success : function(result) {
				if (result != null && result.rows != null ) {
					var trHtml = "";
					var rows = result.rows;
					len = rows.length;
					for (var i = 0; i < rows.length; i++) {
						var hide = "";
						if(typeParam == "industry_type_10001"){
							 hide = $("#copyDiv").find("table[name='air']").html();
						}
						if(typeParam == "industry_type_10002"){
							 hide = $("#copyDiv").find("table[name='logistics']").html();
						}
						if(hide != ""){
							hide = "<table id='temp' style='width: 100%;background-color: #c6e0c5;' class='search-data search-data-tab'>"+hide+"</table>"
						}
						
						var tr = "<tr style='border:1px solid #00F;'>" 
									+ "<td><a href=\"javascript:showBill('"+rows[i].id+"')\">" + rows[i].number + "</a></td>" 
									+ "<td>" + rows[i].tmYear + "</td>" 
									+ "<td>" + rows[i].tmHour + "</td>"
									+ "<td>" + rows[i].business + "</td>" 
									+ "<td>" + rows[i].department + "</td>" 
									+ "<td>" + rows[i].userid + "</td>" 
								+ "</tr><tr name='hide"+rows[i].id+"' class='hideTr' style='display:none;text-align:center;'><td colspan='6'>"+hide+"</td><tr>";
						trHtml += tr;
					}
					$("#billTable").find('tbody').html(trHtml);
				}
			}
		});
		return len;
	}
	
	// 显示所选工单
	function showBill(billId){
		$.ajax({
			type : "POST" ,
			url : '/'+projectName+'/practical/workorder/serchBillById.action' ,
			data : {"id":billId} ,
			async: false,
			dataType : 'json' ,
			success : function(result) {
				if(result != null){
					var temp = $("#billTable").find("tr[name='hide"+billId+"']").find("td");
					temp.find("input[name='number']").parent("td").find("label").after().append(result.number).next("input").remove();
					temp.find("input[name='userid']").parent("td").find("label").after().append(result.userid).next("input").remove();
					temp.find("input[name='tmstr']").parent("td").find("label").after().append(result.tmstr).next("input").remove();
					temp.find("input[name='callTime']").parent("td").find("label").after().append(result.callTime).next("input").remove();
					temp.find("input[name='dialing']").parent("td").find("label").after().append(result.dialing).next("input").remove();
					temp.find("input[name='contact']").parent("td").find("label").after().append(result.contact).next("input").remove();
					temp.find("input[name='business']").parent("td").find("label").after().append(result.business).next("input").remove();
					temp.find("input[name='result']").parent("td").find("label").after().append(result.result).next("input").remove();
					temp.find("input[name='descrip']").parent("td").find("label").after().append(result.descrip).next("input").remove();
					//temp.find("input[name='orderAllot']").val(result.orderAllot);
					temp.find("input[name='orderAllotD']").each(function(i,item){
						if(this.value == result.orderAllot){
							this.checked = true;
						}else{
							$(this).parent("label").remove();
						}
					});
					temp.find("input[name='acceptType']").parent("td").find("label").after().append(result.acceptType).next("input").remove();
					temp.find("select[name='directoryFir']").val(result.directoryFir);
					var fir = temp.find("select[name='directoryFir']").find("option:selected").text();
					temp.find("select[name='directoryFir']").parent("td").find("label").after().append(fir).next("select").remove();
					
					temp.find("input[name='directorySec']").parent("td").find("label").after().append(result.directorySec).next("input").remove();
					temp.find("input[name='department']").parent("td").find("label").after().append(result.department).next("input").remove();
					temp.find("input[name='department']").parent("td").find("label").after().append(result.department).next("input").remove();
					if(result.type == "industry_type_10001"){
						temp.find("input[name='departureCity']").parent("td").find("label").after().append(result.departureCity).next("input").remove();
						temp.find("input[name='arriveCity']").parent("td").find("label").after().append(result.arriveCity).next("input").remove();
						temp.find("input[name='departureDateStr']").parent("td").find("label").after().append(result.departureDateStr).next("input").remove();
						temp.find("input[name='airlineComp']").parent("td").find("label").after().append(result.airlineComp).next("input").remove();
						temp.find("input[name='customerTel']").parent("td").find("label").after().append(result.customerTel).next("input").remove();
						temp.find("input[name='customerLevel']").parent("td").find("label").after().append(result.customerLevel).next("input").remove();
					}
					if( result.type == "industry_type_10002" ){
						temp.find("input[name='orderInfo']").parent("td").find("label").after().append(result.orderInfo).next("input").remove();
					}
				}else{
					$("#billTable").find("tr[name='hide"+billId+"']").find("td").html("查询错误");
				}
				$("#billTable").find("tr[class='hideTr']").each(function(){
					var na = 'hide'+billId;
					if($(this).attr("name") == na){
						$(this).toggle("fast");
					}else{
						$(this).hide();
					}
				})
			}
		});
	}
	
	// 保存客户等级
	function saveCusInfo(){
		var param = $("#cusInfoForm").serialize();
		$.ajax({
			type : "POST" , url : '/'+projectName+'/practical/workorder/changeLevel.action' , data : param , async: false ,dataType : 'json' , success : function(result) {
				if (result != null && result.success ) {
					var id = $("input[name='id']").val();
					if (id == null || id == "" ) {
						$("#orderForm").find("input[name='id']").val(result.id);
					}
					swal({title:result.msg,type:"success"});
				}
			}
		});
	}
	
	// 保存表单
	function saveBill() {
		
		if(window.confirm('确定保存工单吗？')){
			$("#billAddForm").find("input[name='taskId']").val(topicIdParam);
			$("#billAddForm").find("input[name='type']").val(tt);
			var param = $("#billAddForm").serialize();
			var orderAllot = $("#billAddForm").find("input[name='orderAllotD']:checked").attr("data");
			param = param +"&orderAllot="+orderAllot;
			$.ajax({
				type : "POST" , 
				url : '/'+projectName+'/practical/workorder/saveBill.action', 
				data :param,
				dataType : 'json' ,
				success : function(result) {
					if (result.success ) {
						var entity = result.data;
						loadBill(entity.type);
						swal({title:"保存成功",type:"success"});
					} else {
						swal({title:result.msg,type:"error"});
					}
				}
			});
		}else{
			
		}
	}
	
	// 挂断电话
	function preHungUp() {
		$.ajax({
			type : "POST" , url : '/'+projectName+'/practical/workorder/preHungUp.action' , data : {} , dataType : 'json' , success : function(result) {
				if (result.success ) {
					$("#addBill").hide();
					$("#serOrderDiv").hide();
					$("#fade").hide();
				} else {
					swal({title:result.msg,type:"error"});
				}
			}
		});
	}
</script>
