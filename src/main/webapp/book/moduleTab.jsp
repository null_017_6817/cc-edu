<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="nav clearfix">
	<a id="targetTab" href="/${projectName}/book/book/page.action?page=target&bookId=${bookId}&bookRoot=${bookRoot.id}">
		<i class="icon-target"></i>学习目标
	</a>
	<a id="resourceTab" href="/${projectName}/book/book/page.action?page=resource&bookId=${bookId}&bookRoot=${bookRoot.id}&type=2">
		<i class="icon-resource"></i>在线课件
	</a>
	<a id="lineTab" href="/${projectName}/book/book/page.action?page=line&bookId=${bookId}&bookRoot=${bookRoot.id}&type=1">
		<i class="icon-video"></i>在线影音
	</a>
	<a id="practiceurl" href="/${projectName}/book/book/page.action?page=practice&bookId=${bookId}&bookRoot=${bookRoot.id}">
		<i class="icon-practice"></i>实训营地
	</a>
	<a id="testTab" href="/${projectName}/book/book/page.action?page=test&bookId=${bookId}&bookRoot=${bookRoot.id}">
		<i class="icon-test"></i>训练题库
	</a>
	<a id="knowledgeTab" href="/${projectName}/book/book/page.action?page=knowledge&bookId=${bookId}&bookRoot=${bookRoot.id}">
		<i class="icon-knowledge"></i>相关知识
	</a>
</div>