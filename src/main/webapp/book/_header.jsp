<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script src="/${projectName}/common/artDialogs/artDialog.source.js?skin=default" type="text/javascript"></script>
<script src="/${projectName}/common/artDialogs/plugins/iframeTools.source.js" type="text/javascript"></script>
<script type="text/javascript" src="/${projectName}/book/js/jqueryForm/jquery.form.js"></script>

<div class="info">
	<div class="inner" style="line-height: 25px;">
		欢迎访问 &nbsp; 呼叫中心  &nbsp;&nbsp;&nbsp;&nbsp; <span class="date"></span>
	</div>
</div>

<div class="header">
	<input style="display:none;" type="file" name="dataFile" id="dataFile" accept="image/*" />
	<form id="handleUpForm" action="" enctype="multipart/form-data" style="display:none;">
		<input type="file" name="file" value="" style="display:"/>
		<input type="input" name='number' value="${CURRENT_USER.number}" />
	</form>
	
	<div class="inner clearfix">
		<c:if test="${empty CURRENT_USER}">
			<div class="loginDiv">
				<span class="login">登录</span>
			</div>
		</c:if>
		<c:if test="${!empty CURRENT_USER}">	
			<div class="loginDiv">
				<a href="/${projectName}/book/index.jsp" title="首页"><img src="/${projectName}/book/img/icon/home.png"></a>
				<a href="javascript:handleUp();" title="修改头像" class="touxiang"> 
				<c:if test="${!empty CURRENT_USER.photoUrl}">	
				<img id="loginPhoto" src="/${projectName}/imgs/photos/${CURRENT_USER.cnumber}/${CURRENT_USER.photoUrl}" title="头像" height='50' width='50' ">
				</c:if>
				<c:if test="${empty CURRENT_USER.photoUrl}">	
					<img id="loginPhoto" src="/${projectName}/book/img/icon/touxiang.png" title="头像" height='50' width='50' ">
				</c:if>
				${CURRENT_USER.name }</a>
				
				<a href="javascript:showChangePw();" title="修改密码"><img src="/${projectName}/book/img/icon/mima.png"></a>
				<a href="/${projectName}/sysmanage/logout/bookLogout.action" title="注销"><img src="/${projectName}/book/img/icon/zhuxiao.png"></a>
				<c:if test="${CURRENT_USER.state != '3' }">
					<a href='javascript:toManager();' title="后台管理"><img src="/${projectName}/imgs/sysmanage.png" height='25' width='25' ></a>
				</c:if>
	
			</div>
		</c:if>
			
		<img class="header-logo"  src="/${projectName}/book/img/icon/logos.png" alt="" />
	</div>
</div>

<!-- 修改头像弹出框-->
<%-- <div id="picShow" style="display: none;">
	<iframe id="picShowIframe" src="/${projectName}/picshow/page.action?page=index"  title="修改头像" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
</div> --%>

<script>

//************************************** message(短信)  socket  start****************************

var messageSocket = null;
//判断当前浏览器是否支持WebSocket
if ('WebSocket' in window) {
	var number = "${CURRENT_USER.number}";;
	if(number != null && number != "" && typeof(number) != "undefined"){
		messageSocket = new WebSocket("wss://"+document.location.host+"/${projectName}/messageSocket/"+number);
	}
}else {
	alert('当前浏览器 Not support websocket')
}
if(messageSocket != null){

	//连接发生错误的回调方法
	messageSocket.onerror = function () {
		setMessageShow("WebSocket连接发生错误");
		var number = "${CURRENT_USER.number}";;
		if(number != null && number != "" && typeof(number) != "undefined"){
			messageSocket = new WebSocket("wss://"+document.location.host+"/${projectName}/messageSocket/"+number);
		}
	};
	
	//连接成功建立的回调方法
	messageSocket.onopen = function () {
		setMessageShow("WebSocket连接成功");
	}

	//接收到消息的回调方法
	/* messageSocket.onmessage = function (event) {
		setMessageShow(event.data);
	} */
	
	

	//监听窗口关闭事件，当窗口关闭时，主动去关闭websocket连接，防止连接还没断开就关闭窗口，server端会抛异常。
	window.onbeforeunload = function (event) {
		closeMessageWebSocket();
		closeMailWebSocket();
		// 判断closeWebSocket是否为方法（实训营地页面和行业实训页面用）
		if(typeof(closeWebSocket) == 'function'){
			closeWebSocket();
		}
	}

	//将消息显示在网页上
	 function setMessageShow(innerHTML) {
		
		
	} 

	//关闭WebSocket连接
	function closeMessageWebSocket() {
		messageSocket.close();
	}

	//发送消息
	function sendMessageScoket( message ) {
		/* var message = document.getElementById('text').value; */
		messageSocket.send(message);
	}
}
//************************** message(短信) socket end *************************************

//************************************** mail(邮件)  socket  start****************************

var mailSocket = null;
//判断当前浏览器是否支持WebSocket
if ('WebSocket' in window) {
	
	var number = "${CURRENT_USER.number}";
	if(number != null && number != "" && typeof(number) != "undefined"){
		mailSocket = new WebSocket("wss://"+document.location.host+"/${projectName}/mailSocket/"+number);
	}
}else {
	alert('当前浏览器 Not support websocket')
}
if(mailSocket != null){

	//连接发生错误的回调方法
	mailSocket.onerror = function () {
		setMailShow("WebSocket连接发生错误");
		var number = "${CURRENT_USER.number}";
		if(number != null && number != "" && typeof(number) != "undefined"){
			mailSocket = new WebSocket("wss://"+document.location.host+"/${projectName}/mailSocket/"+number);
		}
	};
	
	//连接成功建立的回调方法
	mailSocket.onopen = function () {
		setMessageShow("WebSocket连接成功");
	}

	//接收到消息的回调方法
	/* mailSocket.onmessage = function (event) {
		setMessageShow(event.data);
	} */
	
	

	//监听窗口关闭事件，当窗口关闭时，主动去关闭websocket连接，防止连接还没断开就关闭窗口，server端会抛异常。
	/* window.onbeforeunload = function (event) {
		closeMessageWebSocket();
		// 判断closeWebSocket是否为方法（实训营地页面和行业实训页面用）
		if(typeof(closeWebSocket) == 'function'){
			closeWebSocket();
		}
		// 判断closeWebSocket是否为方法（实训营地页面和行业实训页面用）
		if(typeof(closeMailWebSocket) == 'function'){
			closeWebMailWebSocket();
		}
	} */

	//将消息显示在网页上
	/* function setMessageShow(innerHTML) {
		
		
	} */

	//关闭WebSocket连接
	function closeMailWebSocket() {
		mailSocket.close();
	}

	//发送消息
	function sendMailScoket( message ) {
		/* var message = document.getElementById('text').value; */
		mailSocket.send(message);
	}
}
//************************** mail(邮件) socket end *************************************

function handleUp(){
	$("#handleUpForm").find("input[name='file']").unbind();
	$("#handleUpForm").find("input[name='file']").change(function(){
		var fil = $("#handleUpForm").find("input[name='file']").val();
		if(fil != ""){
			$("#handleUpForm").ajaxSubmit({
				url: '/${projectName}/picshow/uploadByMt.action', 
				type: "post", 
				dataType: "json", 
				success: function (result) {
					setTimeout("window.location.reload()",1500);
				},
			});
		}
	});
	$("#handleUpForm").find("input[name='file']").trigger('click');
	
	
}


</script>
