<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="ie6"><![endif]-->
<!--[if IE 7]>
<html class="ie7"><![endif]-->
<!--[if IE 8]>
<html class="ie8"><![endif]-->
<!--[if (gte IE 9)]><!-->
<html>
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<title>呼叫中心目录页</title>
<link rel="stylesheet" type="text/css"	href="/${projectName}/book/css/common.css" />
<link rel="stylesheet" type="text/css"	href="/${projectName}/book/css/fonts.css" />
<link rel="stylesheet" type="text/css"	href="/${projectName}/book/css/style.css" />

<script type="text/javascript"	src="/${projectName}/book/js/lib/jquery-1.9.1.min.js"></script>

<script type="text/javascript"	src="/${projectName}/book/js/common/show.js"></script>

<script>
var projectName = "${projectName}";
var isCatalog = true;
$(function(){
	var bookRootId = '${bookRoot.id}',
	html = '<img src="/${projectName}/book/img/banner_44.jpg" />';
	$('.banner').append(html);
	
	var sta = "${person.state}";
	isStu(sta);
});

function isStu(sta){
	if(sta == "3"){
		loadExer();
	}else{
		clearExam();
	}
}

// 清空考试区域
function clearExam(){
	$("#examTitle").css("display","none");
	$(".exam").html("");
}

//加载考卷
function loadExer() {
	$("#examTitle").css("display","block");
	$.ajax({
		url:"/${projectName}/examination/exer/schExerAndStatus.action",
		type : "post" , 
		data : {"pageNo":1,"pageSize":3} ,
		success:function(result) {
			if(result.success){
				var data = result.data;// 数据项
				if(data.length>0) {
					var index = 0;
					var serverTime = new Date(data[0].serverTime.replace(/-/g, "/"));
					/* console.log(serverTime); */
					for(var i=0; i<data.length; i++){
						if(data[i].statusReady == 0) {
							index++;
							var startTime = new Date(data[i].startTime.replace(/-/g, "/"));
							var endTime = new Date(data[i].endTime.replace(/-/g, "/"));
							var ul = $(".exam");
							var htm = "<li>"
										+ "<img id='img"+data[i].id+"' src='/${projectName}/book/img/start.png' />"
										+ "<h6>"+data[i].name+ "</h6>"
										+ "<p>"+data[i].remark+"</p>"
										+ "<a id='a"+data[i].id+"' href='javascript:void(0);'>开始考试</a>"
										+ "</li>";
							var html = "<li class='list-"+index+"'><img src='/${projectName}/book/img/icon/exam.png' /> "+data[i].name+ "<p class='list-pop'><span>"+data[i].shortcut+"</span>";
							html += "<a href=\"javascript:void(0);\" class='btn' ></a>";
							html += "</p></li>";
							ul.append(htm);
							if(i == 2){
								var more = "<li style='padding-bottom: 2px;'><h6 style='cursor:pointer;color:red;' onclick='toExam();' >点击查看更多...</h6></li>";
								ul.append(more);
							}
							var exerid = data[i].id;
							var a = $("#a"+exerid);
							var alarm = new Alarm(a,exerid,serverTime, startTime, endTime , function (obj, second, minute, hour, day) { //计时钟
								 obj.empty();
								 obj.append(hour + ':' + minute + ':' + second);
							}, function (obj, eid, examEndTime, currtime) { //倒计时结束
							obj.empty();
							if((currtime - examEndTime) > 0) {
								obj.removeAttr("onclick");
								obj.append("考试结束");
								obj.parent("li").find("img").attr("src",'/${projectName}/book/img/end.png');
							}else {
								obj.attr("onclick", "loadExerTopic("+eid+");");
								obj.append("开始考试");
							}
							});
							alarm.start();
						}
					}
				}
				
			}
		}
	});
	/* console.log('0'); */
}
function loadExerTopic(eid){
	//window.location.href = "/${projectName}/examination/exerTopic/page.action?page=exerTopicExam_vie&eid="+eid;
	window.open("/${projectName}/examination/exerTopic/page.action?page=exerTopicExam_vie&eid="+eid);
}

var Alarm = function (obj, eid, starttime, endtime, examEndTime, countFunc, endFunc) {
	this.obj = obj;
	this.eid = eid;
	this.examEndTime = examEndTime;
	this.starttime = starttime;
	this.time = Math.floor((endtime - starttime) / 1000); //时间
	this.countFunc = countFunc; //计时函数
	this.endFunc = endFunc; //结束函数
	this.flag = 't' + Date.parse(new Date()); //
};
Alarm.prototype.start = function () {
	var self = this;
	
	self.flag = setInterval(function () {
		if (self.time < 0) {
			clearInterval(self.flag);
			self.endFunc(self.obj, self.eid, self.examEndTime, self.starttime);
		} else {
			var minute, hour, day, second;
			day = Math.floor(self.time / 60 / 60 / 24) < 10 ? '0' + Math.floor(self.time / 60 / 60 / 24) : Math.floor(self.time / 60 / 60 / 24);
			hour = Math.floor(self.time / 60 / 60 % 24) < 10 ? '0' + Math.floor(self.time / 60 / 60 % 24) : Math.floor(self.time / 60 / 60 % 24);
			minute = Math.floor(self.time / 60 % 60) < 10 ? '0' + Math.floor(self.time / 60 % 60) : Math.floor(self.time / 60 % 60);
			second = Math.floor(self.time % 60) < 10 ? '0' + Math.floor(self.time % 60) : Math.floor(self.time % 60);
			//倒计时执行函数
			self.countFunc(self.obj, second, minute, hour, day);
			self.time--;
		}
	}, 1000);
}

function toContent( bookRootId,bookId,state){
	if(state == 1){
		window.location.href="/${projectName}/book/book/page.action?page=target&bookId="+bookId+"&bookRoot="+bookRootId;
	}else{
		swal({ 
			title: "没有学习权限", 
			type: "error"});
	}
}

</script>
</head>

<body>
<input style="display:none" type="text" name="fakeusernameremembered"/>
<input style="display:none" type="password" name="fakepasswordremembered"/>
<div class="wrap">
	<div class="content-boxs">
		<jsp:include page="_header.jsp" />

		<div class="banner">
			<div class="inner">
				<ul class="nav">
					<li><a href="/${projectName}/book/index.jsp">首页</a></li>
					<li>&nbsp;/&nbsp;</li>
					<li>课程目录</li>
				</ul>
				<span class="bookTitle">${bookRoot.name}</span>
				<dl>
					<dt style="display:none;"><a href="#">开始学习</a></dt>
					<dd>
						<div class="teacherInfo">
							<span>作者：${bookRoot.editor}</span>
							<i>|</i>
							<span>出版社：${bookRoot.bookconcern}</span>
							<i>|</i>
							<span>出版时间：<fmt:formatDate value="${bookRoot.publictime}" pattern="yyyy-MM-dd"/></span>
						</div>
						<p title="${bookRoot.remarks}">${bookRoot.remarks}</p>
					</dd>
				</dl>
			</div>
		</div>

		<div class="main">
			<div class="inner">
				<!-- <p>课程简介：本教材依据客户信息服务专业核心课程——呼叫中心非语音业务的教学要求编写。从呼叫中心行业非语音业务岗位的技能要求出发，坚持理论与实践相结合、训练与评价相
				结合，全面介绍了呼叫中心非语音业务的基本知识、方法、技巧，包括呼叫中心的电子邮件业务、网上销售业务、短信业务、传真业务、邮寄业务5个项目，具有一定的创新性、前瞻性和较强的实用性。</p> -->
				
				<div class="listCon clearfix">
					<ul class="listLeft">
						<h4>课程目录</h4>
							<c:forEach var="level1" items="${tree}" varStatus="status">
								<li> 
									<div class="listTitle">${level1.name}<i class="icon-down"></i></div>
									<dl>
										<c:forEach var="level2" items="${level1.childs}" varStatus="status">
											<c:if test="${empty level2.childs}">
												<c:if test="${level2.catalogStatus==true}">
													<dd onclick="toContent('${bookRoot.id}','${level2.id}','${level2.state}')" class="list" style="font-size:15px;">${level2.name }<a href="javascript:void(0);" name="can" >继续学习</a><i class="i_icon"><img alt="已完成" src="/${projectName}/book/img/completed.png"></i></dd>
												</c:if>
												<c:if test="${level2.catalogStatus==false}">
													<dd onclick="toContent('${bookRoot.id}','${level2.id}','${level2.state}')" style="font-size:15px;" class="list">${level2.name }<a href="javascript:void(0);" name="can" >开始学习</a><i class="i_icon"><img alt="未完成" src="/${projectName}/book/img/unfinished.png"></i></dd>
												</c:if>
												<%-- <dd onclick="toContent('${bookRoot.id}','${level2.id}','${level2.state}')" class="list" style="background-color: #48825f;color: white;">${level2.name }<a href="javascript:void(0);" name="can" >开始学习</a></dd> --%>
											</c:if>
											<c:if test="${!empty level2.childs}">
												<%-- <dd class="list">${level2.name }<a href="#">开始学习</a></dd> --%>
												<dt class="listElement"><i class="icon-down"></i>${level2.name }</dt>
												<c:forEach var="level3" items="${level2.childs}" varStatus="status">
													<c:if test="${level3.catalogStatus==true}">
														<dd onclick="toContent('${bookRoot.id}','${level3.id}','${level3.state}')" class="list" style="font-size:15px;background-color: #48825f;color: white;">${level3.name }<a href="javascript:void(0);" name="can" >继续学习</a><i class="i_icon"><img alt="已完成" src="/${projectName}/book/img/completed.png"></i></dd>
													</c:if>
													<c:if test="${level3.catalogStatus==false}">
														<dd onclick="toContent('${bookRoot.id}','${level3.id}','${level3.state}')" style="font-size:15px;" class="list" >${level3.name }<a href="javascript:void(0);" name="can" >开始学习</a><i class="i_icon"><img alt="未完成" src="/${projectName}/book/img/unfinished.png"></i></dd>
													</c:if>
													<%-- <dd onclick="toContent('${bookRoot.id}','${level3.id}','${level3.state}')" class="list">${level3.name }<a href="javascript:void(0);" >开始学习</a></dd> --%>
												</c:forEach>
											</c:if>
										</c:forEach>
									</dl>
								</li>
							</c:forEach>
						<!-- <li>
							<div class="listTitle">第一单元 : 电话礼仪 <i class="icon-down"></i></div>
							<dl>
								<dt class="listElement"><i class="icon-down"></i>项目一 : 呼叫中心概述</dt>
								<dd class="list">任务1：初识呼叫中心 <a href="#">开始学习</a></dd>
								<dd class="list">任务2：初识呼叫中心 <a href="#">开始学习</a></dd>
								<dd class="list">任务3：初识呼叫中心 <a href="#">开始学习</a></dd>
							</dl>
							<dl>
								<dt class="listElement"><i class="icon-down"></i>项目二 : 呼叫中心概述</dt>
								<dd class="list">任务1：初识呼叫中心 <a href="#">开始学习</a></dd>
								<dd class="list">任务2：初识呼叫中心 <a href="#">开始学习</a></dd>
								<dd class="list">任务3：初识呼叫中心 <a href="#">开始学习</a></dd>
							</dl>
						</li>
						<li>
							<div class="listTitle">项目一 : 呼叫中心概述 <i class="icon-down"></i></div>
							<dl>
								<dd class="list">任务1：初识呼叫中心 <a href="#">开始学习</a></dd>
								<dd class="list">任务2：初识呼叫中心 <a href="#">开始学习</a></dd>
								<dd class="list">任务3：初识呼叫中心 <a href="#">开始学习</a></dd>
							</dl>
						</li> -->
					</ul>
					
					<div class="listRight">
						 <h4>课程指导</h4>
						<%-- <dl class="teacherIfon">
							<dt><img src="/${projectName}/book/img/start.png"></dt>
							<dd>王老师</dd>
						</dl>  --%>
						<div class="guidance">
							<i class="icon-down"></i>
							<h6>课程简介</h6>
							<p>
								${bookRoot.synopsis}
							</p>
						</div>
						
						<h4 id="examTitle" style="display:none;">考试通知</h4>
						<ul class="exam">
							<%-- <li>
								<img src="/${projectName}/book/img/start.png" />
								<h6>呼入业务实训</h6>
								<p>云计算是一种全新的企业IT交付方式，是社会进一步精细化</p>
								<a href="">开始考试</a>
							</li>
							<li>
								<img src="/${projectName}/book/img/end.png" />
								<h6>呼入业务实训</h6>
								<p>本教材依据客户信息服务专业核心课程——呼叫中心非语音业务的教学要求编写</p>
							</li> --%>
						</dl>
					</div>					
				</div>
			</div>
		</div>
	</div>
</div>

<jsp:include page="_footer.jsp" />

<jsp:include page="./pop.jsp" />
<script type="text/javascript"	src="/${projectName}/book/js/common/login.js"></script>
</body>
</html>
