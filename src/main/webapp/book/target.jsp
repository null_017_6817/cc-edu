<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="ie6"><![endif]-->
<!--[if IE 7]>
<html class="ie7"><![endif]-->
<!--[if IE 8]>
<html class="ie8"><![endif]-->
<!--[if (gte IE 9)]><!-->
<html>
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<title>呼叫中心学习目标</title>
<link rel="stylesheet" type="text/css"	href="/${projectName}/book/css/common.css" />
<link rel="stylesheet" type="text/css"	href="/${projectName}/book/css/fonts.css" />
<link rel="stylesheet" type="text/css"	href="/${projectName}/book/css/style.css" />

<script type="text/javascript"	src="/${projectName}/book/js/lib/jquery-1.9.1.min.js"></script>

<style>
#content ul,#content ul li {
    padding: 0;
    margin: 0;
    list-style: unset;
}
</style>
<script>
var projectName = "${projectName}";
var tabName= "targetTab";
//banner - 书本名称
$(function(){
	var bookRootId = '${bookRoot.id}',
		html = '<img src="/${projectName}/book/img/banner_44.jpg" />';
	$('.banner').append(html);
	initData();
	//根据bookId判断是否显示实训营地
	getOpentrainingValue('${bookRoot.id}');
});
function getOpentrainingValue(bookId){
	$.ajax({
		url : "/${projectName}/book/book/schBookByBookID.action" , 
		type : "post" , 
		data : {"bookId" : bookId} , 
		dataType: "json",
		success : function(result) {
			if (result.success ) {
				var OpentrainingValue = result.data.opentraining;
				if(OpentrainingValue == "1"){
					$("#practiceurl").css("display","block");
				}else{
					$("#practiceurl").css("display","none");
				}
				return OpentrainingValue;
			}
		}
	});
}

//初始化数据
function initData(){
	var html = $("#content").html();
	if(html == null || html == "" || typeof(html) == "undefined" ){
		$("#content").css("text-align","center");
		var nodata = "<img src='/${projectName}/book/js/layui/img/nocon2.png'>"
			+ "<p style='text-align:center;font-size: 24px;color: #999999;margin-top: 60px;'>暂无内容哦！</p>";
		$("#content").html(nodata);
	}
}


</script>
</head>

<body>
<input style="display:none" type="text" name="fakeusernameremembered"/>
<input style="display:none" type="password" name="fakepasswordremembered"/>
<div class="wrap">
	<div class="content-box">
		<jsp:include page="_header.jsp" />

		<div class="banner">
			<div class="inner">
				<ul class="nav">
					<li><a href="/${projectName}/book/index.jsp">首页</a></li>
					<li>&nbsp;/&nbsp;</li>
					<li><a href="/${projectName}/book/book/page.action?page=catalog&bookRoot=${bookRoot.id}">课程目录</a></li>
					<li>&nbsp;/&nbsp;</li>
					<li>${book.name}</li>
				</ul>
				<span class="bookTitle">${bookRoot.name}</span>
				<dl>
					<dd>
						<div class="teacherInfo">
							<span>作者：${bookRoot.editor}</span>
							<i>|</i>
							<span>出版社：${bookRoot.bookconcern}</span>
							<i>|</i>
							<span>出版时间：<fmt:formatDate value="${bookRoot.publictime}" pattern="yyyy-MM-dd"/></span>
						</div>
						<p title="${bookRoot.remarks}" >${bookRoot.remarks}</p>
					</dd>
				</dl>
			</div>
		</div>

		<div class="main bg">
			<div class="detailBox inner">
				<jsp:include page="moduleTab.jsp" />

				<div class="tabsBox bg clearfix" id="tabs-basic">
					<div id="content" style="padding:50px;list-style:unset !important;">${book.goal}</div>
				</div>
			</div>
		</div>
	</div>
</div>
<jsp:include page="_footer.jsp" />
<jsp:include page="./pop.jsp" />
</body>
<script type="text/javascript"	src="/${projectName}/book/js/common/login.js"></script>
</html>