<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="ie6"><![endif]-->
<!--[if IE 7]>
<html class="ie7"><![endif]-->
<!--[if IE 8]>
<html class="ie8"><![endif]-->
<!--[if (gte IE 9)]><!-->
<html><!--<![endif]-->
<head>
	<meta charset="utf-8"/>
	<meta name="keywords" content=""/>
	<meta name="description" content=""/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta http-equiv="X-UA-Compatible" content="IE=8"/>
	<meta HTTP-EQUIV="pragma" CONTENT="no-cache"> 
	<meta HTTP-EQUIV="Cache-Control" CONTENT="no-cache, must-revalidate"> 
	<meta HTTP-EQUIV="expires" CONTENT="0"> 
	<title>呼叫中心首页</title>

	<link rel="stylesheet" type="text/css" href="/${projectName}/book/css/common.css"/>
	<link rel="stylesheet" type="text/css" href="/${projectName}/book/css/fonts.css"/>
	<link rel="stylesheet" type="text/css" href="/${projectName}/book/css/index.css"/>
	<script src="/${projectName}/book/js/lib/jquery-1.9.1.min.js" type="text/javascript"></script>
	
	<script> 
	var projectName = '${projectName}';
	var mainJsp = true;
	
	// 初始化方法
	$(function(){
		// 轮播图
		$('#b03').unslider({
			dots: true
		});
		
		// 加载课本
		loadBooks();
		
		// 图片绑定抖动效果
		$(".train .clearfix").find("li").hover(function(){
			$(this).shake(1,5,400);
		},function(){  
			//$(this).shake(2, 10, 400);
		})	
		// getCurrentUser();
	})
	
	// 获取当前用户
	/* function getCurrentUser(){
		$.ajax({
			type : "POST",
			url : '/${projectName}/book/book/getCurrentUser.action',
			dataType:'json',
			success : function(result) {
				if(result != null && result.name != null){
					var manageUrl = ""
					if(result.state != "3"){
						manageUrl = "<li><a href='javascript:toManager();'>后台管理</a></li>" 
					}
					var htm = "<a href='JavaScript:void(0);' onclick='showPhoneBox();' class='icon-phone'></a><a href='javascript:void(0);' onclick=\"toPractice();\" class='icon-practice'></a><div class='user'><span class=' icon-logins'></span><i class='icon-down'></i>"
						+"<ul>" 
						+"<li>["+result.name+"]</li>"
						+"<li><a href='/${projectName}/sysmanage/logout/bookLogout.action'>注销</a></li>"
						+"<li><a href='javascript:showChangePw();'>修改密码</a></li>" 
						+manageUrl
						+"</ul></div>";
						
					$(".loginDiv").html(htm);
					
					$('.login-con span').trigger('click');
					
					showExam(result.state);
				}else{
					$('#examDIV').hide();
				}
			}
		});
	} */
	
	// 加载图书
	function loadBooks(){
		$.ajax({ url: "/${projectName}/book/book/schBookForMain.action", 
			context: document.body, 
			dataType : "json",
			success: function(data){
			data = data.rows;
			///${projectName}/data/img/bk1.png
			for (var i=0;i<data.length;i++){
				$(".piclist ul").append(" <li class='pic'><a class='imgdiv' onclick=\"toCatalog('"+data[i].id+"')\" href='javascript:void(0);'><img src='/${projectName}/book/book/appendHtmlUrl.action?fileName="+data[i].path+"' style='height:100%;width:100%;'/></a><span>"+data[i].name+"</span></li>");
			}
			//在ul下循环创建  <ul class="class-start clearfix">
			$(this).addClass("done");
		}});
	}
	
	// 进入目录页
	function toCatalog( rootId ){
		var user = "${CURRENT_USER}";
		if(user != null && user != "" && typeof(user) != "undefined"){
			var href = "/${projectName}/book/book/page.action?page=catalog&bookRoot="+rootId+"&type=1";
			window.location.href = href;
		}else{
			swal({ 
				title: '需要登录后才能操作', 
				text: '', 
				imageUrl:"/${projectName}/book/img/noPower.png",
				showCancelButton: true,
				showCloseButton:false,
				imageWidth: 200,
				imageHeight:200,
				confirmButtonColor: '#3085d6',
				confirmButtonText: '现在登录', 
				cancelButtonText:'我再想想',
			}).then(function(confirm){
				if(confirm.value){
					// 弹出登录窗口
					$('.login').trigger("click")
				}else{
				}
			}) 
		}
	}
	</script>
</head>

<body>
<input style="display:none" type="text" name="fakeusernameremembered"/>
<input style="display:none" type="password" name="fakepasswordremembered"/>

<div class="wrap">
	<div class="content-box">
		<jsp:include page="_header.jsp" />

		<div class="banner inner" id="b03">
			<ul>
				<li><img src="/${projectName}/book/img/banner1.jpg" /></li>
				<li><img src="/${projectName}/book/img/banner2.jpg" /></li>
				<li><img src="/${projectName}/book/img/banner3.jpg" /></li>
				<li><img src="/${projectName}/book/img/banner4.jpg" /></li>
			</ul>
		</div>

		<div class="main">
			<div class="section section-1">
				<div class="inner">
					<c:if test="${!empty CURRENT_USER}">
					<h4><img src="/${projectName}/book/img/train/train.png" /> &nbsp;&nbsp;实训</h4>
					<div class="train">
						<ul class="clearfix">
							<li><a href="javascript:toMail();"><img src="/${projectName}/book/img/train/mailTrain.png" /></a></li>
							<li><a href="javascript:toMessage();"><img src="/${projectName}/book/img/train/messageTrain.png" /></a></li>
							<li><a href="javascript:toPractice();" ><img src="/${projectName}/book/img/train/hyTrain.png" /></a></li>
							<li><a href="javascript:toExam();"><img src="/${projectName}/book/img/train/other.png" /></a></li>
						</ul>
					</div>
					</c:if>
					<h4><img src="/${projectName}/book/img/train/book.png" /> &nbsp;&nbsp;课程</h3>
					<div class="piclist">
						<ul class="clearfix"></ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- 轮播插件 --> 
<script src="/${projectName}/book/js/lib/unslider.min.js" type="text/javascript"></script>
<jsp:include page="_footer.jsp" />
<jsp:include page="./pop.jsp" />

<script>

// 自定义抖动方法
jQuery.fn.shake = function (intShakes /*Amount of shakes*/, intDistance /*Shake distance*/, intDuration /*Time duration*/) {
	this.each(function () {
		var jqNode = $(this);
		jqNode.css({ position: 'relative' });
		for (var x = 1; x <= intShakes; x++) {
			jqNode.animate({ left: (intDistance * -1) }, (((intDuration / intShakes) / 4)))
			.animate({ left: intDistance }, ((intDuration / intShakes) / 2))
			.animate({ left: 0 }, (((intDuration / intShakes) / 4)));
		}
	});
	return this;
}

</script>
<script src="/${projectName}/book/js/common/login.js" type="text/javascript"></script>
</body>
</html>
