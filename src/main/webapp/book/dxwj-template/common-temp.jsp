<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="questionDiv question">
	<table name="questionTable" >
		<tr>
			<td>类型</td>
			<td>
				<select name="type" onchange="changeType(this)">
					<option value="">请选择</option>
					<c:forEach items="${selectOp}" var="item" varStatus="sta">
						<option value="${item.code}">${item.name}</option>
					</c:forEach>
				</select>
			</td>
		</tr>
	</table>
</div>