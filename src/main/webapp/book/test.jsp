<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="ie6"><![endif]-->
<!--[if IE 7]>
<html class="ie7"><![endif]-->
<!--[if IE 8]>
<html class="ie8"><![endif]-->
<!--[if (gte IE 9)]><!-->
<html>
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<title>呼叫中心训练题库</title>
<link rel="stylesheet" type="text/css"	href="/${projectName}/book/css/common.css" />
<link rel="stylesheet" type="text/css"	href="/${projectName}/book/css/fonts.css" />
<link rel="stylesheet" type="text/css"	href="/${projectName}/book/css/style.css" />

<script type="text/javascript"	src="/${projectName}/book/js/lib/jquery-1.9.1.min.js"></script>
<style>

</style>
</head>

<body>
<div class="wrap">
	<div class="content-box">
		<jsp:include page="_header.jsp" />

		<div class="banner">
			<div class="inner">
				<ul class="nav">
					<li><a href="/${projectName}/book/index.jsp">首页</a></li>
					<li>&nbsp;/&nbsp;</li>
					<li><a href="/${projectName}/book/book/page.action?page=catalog&bookRoot=${bookRoot.id}">课程目录</a></li>
					<li>&nbsp;/&nbsp;</li>
					<li>${book.name}</li>
				</ul>
				<span class="bookTitle">${bookRoot.name}</span>
				<dl>
					<dd>
						<div class="teacherInfo">
							<span>作者：${bookRoot.editor}</span>
							<i>|</i>
							<span>出版社：${bookRoot.bookconcern}</span>
							<i>|</i>
							<span>出版时间：<fmt:formatDate value="${bookRoot.publictime}" pattern="yyyy-MM-dd"/></span>
						</div>
						<p title="${bookRoot.remarks}" >${bookRoot.remarks}</p>
					</dd>
				</dl>
			</div>
		</div>

		<div class="main bg">
			<div class="detailBox inner">
				<jsp:include page="moduleTab.jsp" />
		
				<div class="tabsBox clearfix" id="tabs-basic">
					<div class="tabsInfo border-right pull-left">
						<div class="con tab-content-active">
							<iframe class="test-con" id='iframe' name='iframe' width='100%'
								style="min-height: 700px;" frameborder='0' scrolling='auto'></iframe>
						</div>
						<div id="noData" style="text-align:center;display:none;height:500px;padding-top: 50px;">
							<img src='/${projectName}/book/js/layui/img/nocon2.png'>
							<p style='text-align:center;font-size: 24px;color: #999999;margin-top: 60px;'>暂无内容哦！</p>
						</div>
					</div>
				
					<div class="tabsright pull-right" style="overflow-y: scroll;max-height: 650px;">
						<h6>训练列表</h6>	
						<ul class="tabsList"></ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<jsp:include page="_footer.jsp" />

<jsp:include page="./pop.jsp" />

<script type="text/javascript">
var tabName= "testTab";
//banner - 书本名称
$(function(){
	var bookRootId = '${bookRoot.id}',
		html = '<img src="/${projectName}/book/img/banner_44.jpg" />';
	$('.banner').append(html);
	clickTask("${bookId}");
	
	//根据bookId判断是否显示实训营地
	getOpentrainingValue('${bookRoot.id}');
});


function getOpentrainingValue(bookId){
	$.ajax({
		url : "/${projectName}/book/book/schBookByBookID.action" , 
		type : "post" , 
		data : {"bookId" : bookId} , 
		dataType: "json",
		success : function(result) {
			if (result.success ) {
				var OpentrainingValue = result.data.opentraining;
				if(OpentrainingValue == "1"){
					$("#practiceurl").css("display","block");
				}else{
					$("#practiceurl").css("display","none");
				}
				return OpentrainingValue;
			}
		}
	});
	
}


var isTest = true;
var projectName = '${projectName}';
	var bookId = "${bookId}";
	var dataArry = new Array();
	var book ;
	$(function() {
		var t = setTimeout("schBookTree()", 200);
	});
	function schBookTree() {
		$.ajax({
			url : "/${projectName}/book/book/schBookTree.action" , type : "post" , context : document.body , data : {
				id : "${bookId}"
			} , success : function(data, result) {
				if (result == "success" ) {
					data = eval("(" + data + ")");
					data = data.rows;
					//拼章节菜单
					for (var i = 0; i < data.length; i++) {
						$(".chapterList").append("<li>" + data[i].name + "<ul class='lists' id='lists"+data[i].id+"'></ul></li>");
						var tdata = data[i].tdata;
						for (var j = 0; j < tdata.length; j++) {
							//默认选中Task
							if (i == 0 && j == 0 ) {
								//存放cookie
								var taskId = $.cookie('ltaskId');
								if (taskId != null ) {
									clickTask(taskId);
								} else {
									clickTask(tdata[0].id);
								}
							}
							$("#lists" + data[i].id + "").append("<li><a href='javascript:;' onclick='clickTask(" + tdata[j].id + ")'>" + tdata[j].name + "</a></ul>");
						}
					}
						
				} else {
					swal({title:'服务器异常！',
						type:"error"});
				}
			}
		});
	}
	
	function clickTask(taskId){
		$.ajax({
			url : "/${projectName}/examination/exerTopic/getTopicsNotCampByBookId.action" , 
			type : "post" , 
			data : {"bookId" : taskId , type : '4'
			} , async : false , success : function(result) {
				if (result.success ) {
					var bookLis = result.data;
					if (bookLis != null && bookLis.length > 0 ) {
						//var html = "";
						for (var i = 0; i < bookLis.length; i++) {
							var obj = bookLis[i];
							var msgShow = obj.title;
							if(msgShow != null && msgShow != "" && msgShow.length > 16){
								msgShow = msgShow.substring(0,14)+"..."
							}
							var hasDoShowFlg = ''
							if(obj.hasDo == "false"){
								hasDoShowFlg = "display:none;";
							}
							hasdoFlg = "<image src='/${projectName}/book/img/hasDo.png' style='width:15px;float:left;padding-top: 10px;"+hasDoShowFlg+"'>"
							$(".tabsList").append("<li>"+hasdoFlg+"<a data-hasdo='"+obj.hasDo+"' data-index='"+i+"' title='"+obj.title+"' onclick=\"getContent("+i+")\" >"+msgShow+"</a></li>");
							dataArry[i] = obj;
						}
					} else {
						$(".tabsList").html("<li><a >无数据</a></li>");
						noData();
					}
					$(".tabsList").find('li').eq(0).find("a").trigger('click');
				} else {
					swal({ 
						title: result.msg, 
						type: "error"});
				}
			}
		});
	}
	
	// 添加已做标识
	function taskAddFlg(){
		$("li[class='active']").find("img").show();
	}
	
	// 清除数据
	function clearTopic() {
		$(".test-con").attr("src", "");
	}
	
	// 无数据三个字按下
	function noData() {
		$(".tab-content-active").css("display", "none");
		$("#noData").css("display", "block");
		clearTopic();
	}
	
	function getContent(index) {
		//选中的列表加入活动样式
		/* $(".tabsList").find("a[data-options='" + book + "-" + index + "']").addClass("on").siblings().removeClass('on'); */
		$(".tabsList").find("li").eq(index).siblings().removeClass('active');;
		$(".tabsList").find("li").eq(index).addClass("active");
		var Obj = dataArry[index];
		if (Obj != null ) {
			$(".tab-content-active").css("display", "block");
			$("#noData").css("display", "none");
			var topicId = Obj.id;
			var bookId = Obj.bookId;
			$(".test-con").attr("src", "/${projectName }/examination/doexer.jsp?type=exer&bookId=" + bookId + "&etid=" + topicId);
			//$("iframe").get(0).contentWindow.loadActive(topicId);
		}
	}
	
	// 供子页面调用的提示消息方法
	function showNotice(msg){
		art.dialog.tips("操作成功",2);
	}
	
	// 上一题
	function preTopic(){
		var len = $(".tabsList").find("li").length;
		// 判断题目数量
		if(len > 0){
			var activeLi = $(".tabsList").find("li[class=active]");
			var index = $(".tabsList").find("li").index(activeLi);
			// 判断是否是第一个题目
			if(index > 0){
				$(".tabsList").find("li").eq((index-1)).find('a').trigger('click');
			}
		}
	}
	
	// 下一题
	function nextTopic(){
		var len = $(".tabsList").find("li").length;
		if(len > 0){
			var activeLi = $(".tabsList").find("li[class=active]");
			var index = $(".tabsList").find("li").index(activeLi);
			// 判断是否还能下一题
			if((index+1) < len ){
				$(".tabsList").find("li").eq((index+1)).find('a').trigger('click');
			}
		}
	}
	
</script>
	<script type="text/javascript" src="/${projectName}/book/js/lib/jquery.cookie.js"></script>
	<script type="text/javascript" src="/${projectName}/book/js/common/tab.js"></script>
	<script type="text/javascript" src="/${projectName}/book/js/common/login.js"></script>
</body>
</html>