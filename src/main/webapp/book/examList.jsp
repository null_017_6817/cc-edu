<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="ie6"><![endif]-->
<!--[if IE 7]>
<html class="ie7"><![endif]-->
<!--[if IE 8]>
<html class="ie8"><![endif]-->
<!--[if (gte IE 9)]><!-->
<html>
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<title>呼叫中心考试列表页面</title>
<link rel="stylesheet" type="text/css"	href="/${projectName}/book/css/common.css" />
<link rel="stylesheet" type="text/css"	href="/${projectName}/book/css/fonts.css" />
<link rel="stylesheet" type="text/css"	href="/${projectName}/book/css/style.css" />
<link rel="stylesheet" href="/${projectName}/book/js/layui/js/lib/layui2.3/css/layui.css">
<link rel="stylesheet" type="text/css"	href="/${projectName}/book/js/layui/css/style.css">
<script src="/${projectName}/book/js/layui/js/lib/layui2.3/layui.all.js"></script>
<script type="text/javascript"	src="/${projectName}/book/js/lib/jquery-1.9.1.min.js"></script>
<%-- <link rel="stylesheet" type="text/css"	href="/${projectName}/book/js/jQueryHdPaging/paging.css">
<script type="text/javascript"	src="/${projectName}/book/js/jQueryHdPaging/paging.js"></script> --%>

<style>
#content ul,#content ul li {
	padding: 0;
	margin: 0;
	list-style: unset;
}
</style>
<script>
var projectName = "${projectName}";
window.pageSize = 8; //一页条数
var init = true;// 初始化标识
var dictId = "";

// 初始化
$(function(){
	initDict();// 初始化检索条件
	loadExer(1);// 初始化考试列表
});

// 初始化数据
function initDict(){
	$.ajax({
		url : "/${projectName}/examination/exer/getShutCutByRoot.action" , 
		type : "post" , 
		data : {} ,
		dataType:"json",
		success : function(result) {
			if(result.success){
				var data = result.data;
				if(data != null && data.length > 0){
					var html = "<p onclick='chooseAll(this);' class='on'>全部</p>";
					for( var i = 0 ; i < data.length; i++ ){
						var dic = data[i];
						html += "<p onclick=\"getByPid('"+dic.id+"',this);\" id='"+dic.id+"'>"+dic.label+"</p>";
					}
					$("div[name='itemP']").html(html);
				}
			}
		}
	});
}

// 根据父节点获取数据
function getByPid( pid , t ){
	
	doChoose(t);
	$(t).parent("div").parent("div").nextAll().remove();
	$.ajax({
		url : "/${projectName}/examination/exer/getShutCutByPid.action" , 
		type : "post" , 
		data : {'pid':pid} ,
		dataType:"json",
		success : function(result) {
			if(result.success){
				var data = result.data;
				if(data != null && data.length > 0){
					var phtml = "<p onclick='chooseAll(this);' class='on'>全部</p>";
					for( var i = 0 ; i < data.length; i++ ){
						var dic = data[i];
						phtml += "<p onclick=\"getByPid('"+dic.id+"',this);\" id='"+dic.id+"'>"+dic.label+"</p>";
					}
					var divHtml = "<div name='itemDiv' class='testlist-topnav1 testlist-topnavck1'>"
								+	"<div class='testlist-topnav1-lb'></div>"
								+	"<div name='itemP' class='testlist-topnav1-lbs testlist-topnav1-lbsck1'>"
								+	phtml
								+	"</div>"
								+	"<i onclick='initClick(this)' class='layui-icon layui-icon-down'></i>"
								+"</div>";
					$(t).parent("div").parent("div").after(divHtml);
				}
			}
		}
	});
	
}

//选中效果
function doChoose(t){
	$(t).parent().find("p").siblings().removeClass('on');;
	$(t).addClass("on");
	dictId = $(t).attr("id");
	init = true;
	loadExer(1);
}

// 点击全选
function chooseAll(t){
	
	$(t).parent("div").parent("div").nextAll().remove();
	var rootDiv = $("div[name=itemDiv]");
	var len = rootDiv.length;
	if(len > 1){
		len = len-2;
	}else{
		len = len-1;
	}
	$(t).parent().find("p").siblings().removeClass('on');;
	$(t).addClass("on");
	dictId = $("div[name=itemDiv]").eq(len).find("div[name='itemP']").find(".on").attr("id");
	init = true;
	loadExer(1);
}

//加载考卷
function loadExer( pageNo ) {
	$(".testlist-ul").html("");
	
	var state = "${CURRENT_USER.state}";
	var url = "";
	if(state == null || typeof(state) == "undefined" ){
		alert("登录超时，请重新登录")
		window.location.href= "/"+projectName+"/";
	}
	if(state == '3' ){
		url = "/${projectName}/examination/exer/schExerAndStatus.action";
	}
	if(state == '2' || state == 'x'){
		url = "/${projectName}/examination/exer/schExcerByTeacher.action";
	}
	
	$.ajax({
		url:url,
		type : "post" , 
		data : {"pageNo":pageNo,"pageSize":pageSize,"shortcut":dictId} ,
		success:function(result) {
			if(result.success){
				window.totalNum = result.total;// 总条数
				var data = result.data;// 数据项
				if(data.length>0) {
					var serverTime = new Date(data[0].serverTime.replace(/-/g, "/"));
					for(var i=0; i<data.length; i++){
						/* if(data[i].statusReady == 0) { */
						var totalScore = data[i].totalScore
						if(totalScore == null || totalScore == "undefined"){
							totalScore = 0;
						}
						var startTime = new Date(data[i].startTime.replace(/-/g, "/"));
						var endTime = new Date(data[i].endTime.replace(/-/g, "/"));
						var ul = $(".testlist-ul");
						var htm = "<li id='rootLi"+data[i].id+"' class='testlist-ul-li'>";
						if (i%4 == 3) {
							htm = "<li id='rootLi"+data[i].id+"' class='testlist-ul-li' style='margin-right:0px'>";
						}
						
						// 循环试卷上面的图片
						var ys = 1;
						if(i <= 2){
							ys = i+1;
						}else{
							ys = (i+1)%3 == 0 ? 3 : (i+1)%3;
						}
						
						htm	+= "<div class='testlist-ul-img'>"
								+ "<img src='/${projectName}/book/js/layui/img/testlist"+ys+".png'>"
								+ "<div id='sj"+data[i].id+"' class='testlist-sj'></div>"
								+ "<div id='ed"+data[i].id+"' class='testlist-ed'></div>"
							+ "</div>"
							+ "<div class='testlist-ul-con1'>"
								+ "<p>"+data[i].name+"</p>"
								/* + "<p>"+data[i].remark+"</p>" */
							+ "</div>"
							+ "<div class='testlist-ul-con2'>"
								+ "<p>总分：<span style='color:#f12222;'>"+totalScore+"分</span></p>"
								+ "<p>时长："+data[i].diff+"分钟</p>"
								+ "<p>开始："+data[i].startTime+"</p>"
								+ "<p>结束："+data[i].endTime+"</p>"
							+ "</div>"
							+ "<div class='testlist-ul-con3'>"
								+ "<a id='a"+data[i].id+"' data='"+data[i].id+"' href='javascript:void(0);' class='testlist-ul-con3-btn1'>加载中...</a>"
							+ "</div>"
							+ "</li>"
						ul.append(htm); 
						var exerid = data[i].id;
						var a = $("#a"+exerid);
						var alarm = new Alarm(a,exerid,serverTime, startTime, endTime , function (obj, second, minute, hour, day) { //计时钟
							obj.empty();
							obj.append(hour + ':' + minute + ':' + second);
							obj.addClass("disable");
							var currentId = obj.attr("data");
							$("#sj"+currentId).attr("class","testlist-sj2");
							$("#ed"+currentId).attr("class","testlist-ed2");
							$("#ed"+currentId).html("未开始");
							 
						}, function (obj, eid, examEndTime, currtime) { //倒计时结束
							
							obj.empty();
							
							var state = "${CURRENT_USER.state}";
							if(state == 3){
								if((currtime - examEndTime) > 0) {
									obj.removeAttr("onclick");
									obj.append("考试结束");
									obj.addClass("disable");
									var currentId = obj.attr("data");
									$("#sj"+currentId).attr("class","testlist-sj3");
									$("#ed"+currentId).attr("class","testlist-ed3");
									$("#ed"+currentId).html("已结束");
								}else {
									obj.attr("onclick", "loadExerTopic("+eid+");");
									obj.append("开始考试");
									obj.removeClass("disable");
									var currentId = obj.attr("data");
									$("#sj"+currentId).attr("class","testlist-sj1");
									$("#ed"+currentId).attr("class","testlist-ed1");
									$("#ed"+currentId).html("进行中");
								}
							}
							if(state == 2 || state == 'x'){
								if((currtime - examEndTime) > 0) {
									obj.attr("onclick", "loadExerTopic("+eid+");");
									obj.append("预览试卷");
									obj.removeClass("disable");
									var currentId = obj.attr("data");
									$("#sj"+currentId).attr("class","testlist-sj3");
									$("#ed"+currentId).attr("class","testlist-ed3");
									$("#ed"+currentId).html("已结束");
								}else {
									obj.attr("onclick", "loadExerTopic("+eid+");");
									obj.append("预览试卷");
									obj.removeClass("disable");
									var currentId = obj.attr("data");
									$("#sj"+currentId).attr("class","testlist-sj1");
									$("#ed"+currentId).attr("class","testlist-ed1");
									$("#ed"+currentId).html("进行中");
								}
							}
						});
						alarm.start(); 
						/* } */
					}
					// 判断是否为初次加载
					if(init){
						showPageNum(pageNo,getTotalPageNum(),window.totalNum);
					}
				}
			}
		}
	});
}

function isStud(){
	
}



// 考试界面
function loadExerTopic(eid){
	//window.location.href = "/${projectName}/examination/exerTopic/page.action?page=exerTopicExam_vie&eid="+eid;
	var state = "${CURRENT_USER.state}";
	if(state == '3'){
		var url = "/${projectName}/examination/exerTopic/page.action?page=exerTopicExam_vie&eid="+eid;
		showAnswerReq(url,eid);
	}else{
		var url = "/${projectName}/examination/exer/page.action?page=exer_preview&id="+eid;
		showAnswerReq(url,eid);
	}
}

function showAnswerReq(url,eid){
	//多窗口模式，层叠置顶
	var index = layer.open({
		type: 2 //此处以iframe举例
		,title: '答题要求'
		,closeBtn :1
		,area: ['800px', '550px']
		,resize: true
		,maxmin: true 
		,content: '/'+projectName+'/examination/answerReq/answerReq_view.jsp?id='+eid
		,btn: '确定'
		,btnAlign: 'c' //按钮居中
		,shade: 0.1 //不显示遮罩
		,yes: function(){
			window.open(url,"ppp");
			layer.close(index);
		},cancel : function(){
			
		}
	});
}
	
	
	
	
var Alarm = function (obj, eid, starttime, endtime, examEndTime, countFunc, endFunc) {
	this.obj = obj;
	this.eid = eid;
	this.examEndTime = examEndTime;
	this.starttime = starttime;
	this.time = Math.floor((endtime - starttime) / 1000); //时间
	this.countFunc = countFunc; //计时函数
	this.endFunc = endFunc; //结束函数
	this.flag = 't' + Date.parse(new Date()); //
};
Alarm.prototype.start = function () {
	var self = this;
	
	self.flag = setInterval(function () {
		if (self.time < 0) {
			clearInterval(self.flag);
			self.endFunc(self.obj, self.eid, self.examEndTime, self.starttime);
		} else {
			var minute, hour, day, second;
			day = Math.floor(self.time / 60 / 60 / 24) < 10 ? '0' + Math.floor(self.time / 60 / 60 / 24) : Math.floor(self.time / 60 / 60 / 24);
			hour = Math.floor(self.time / 60 / 60 % 24) < 10 ? '0' + Math.floor(self.time / 60 / 60 % 24) : Math.floor(self.time / 60 / 60 % 24);
			minute = Math.floor(self.time / 60 % 60) < 10 ? '0' + Math.floor(self.time / 60 % 60) : Math.floor(self.time / 60 % 60);
			second = Math.floor(self.time % 60) < 10 ? '0' + Math.floor(self.time % 60) : Math.floor(self.time % 60);
			//倒计时执行函数
			self.countFunc(self.obj, second, minute, hour, day);
			self.time--;
		}
	}, 1000);
}

// 分页处理
function showPageNum( pageNo , totalPage , totalCount){
	
	layui.use(['laypage', 'layer'], function(){
		var laypage = layui.laypage
		,layer = layui.layer;
		
		//总页数大于页码总数
		laypage.render({
			elem: 'fenye'
			,count: totalCount //数据总数
			,prev:'<'
			,next:'>'
			,limit: pageSize //每页显示的条数。
			,theme: '#1675e9' 
			,groups: 4
			,jump: function(obj){
				if(!init){
					loadExer(obj.curr);
				}else{
					init = false;
				}
			}
		});
	});
}


// 计算总页数
function getTotalPageNum(){
	var totalPages = Math.ceil(window.totalNum / window.pageSize);
	return totalPages;
}

</script>
</head>

<body>
<input style="display:none" type="text" name="fakeusernameremembered"/>
<input style="display:none" type="password" name="fakepasswordremembered"/>
<div class="wrap">
	<div class="content-box">
		<jsp:include page="_header.jsp" />

		<div class="testlist-topnav-box">
			<div class="testlist-topnav">
				<div name="itemDiv" class="testlist-topnav1 testlist-topnavck1">
					<div class="testlist-topnav1-lb">目录：</div>
					<div name="itemP" class="testlist-topnav1-lbs testlist-topnav1-lbsck1">
					</div>
					<i class="layui-icon layui-icon-down"></i>
				</div>
			</div>
		</div>


		<ul class="testlist-ul">
			
		</ul>
		
		<div class="layfenye" style="width:1160px;margin: 0 auto;text-align:center;margin-bottom:100px;">
			<div id="fenye"></div>
		</div>
		<!-- <div class="fenyedifang" style="width:1160px;height:140px;margin: 0 auto;text-align:center;"><div class="box" id="box" ></div></div> -->
	</div>
</div>
<jsp:include page="_footer.jsp" />
<jsp:include page="./pop.jsp" />
</body>
<script type="text/javascript"	src="/${projectName}/book/js/common/login.js"></script>
<script>

function initClick(t){
	if ($(t).hasClass('ion')) {
		$(t).removeClass("ion");
		$(t).parent().animate({ maxHeight :"55px"});
	}
	else{
		$(t).addClass("ion");
		$(t).parent().animate({maxHeight :"2000px"});
	}
}

$(".layui-icon").each(function(i,item){
	$(item).click(function(){
		if ($(this).hasClass('ion')) {
			$(this).removeClass("ion");
			$(this).parent().animate({ maxHeight :"55px"});
		}
		else{
			$(this).addClass("ion");
			$(this).parent().animate({maxHeight :"2000px"});
		}
	})
})
</script>
</html>
