<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>修改课本</title><!--book-->
	<%@include file="/common/include.jsp" %>
	<script type="text/javascript" charset="utf-8"	src="/${projectName}/ueditor/ueditor.config.js"></script>
	<script type="text/javascript" charset="utf-8"	src="/${projectName}/ueditor/ueditor.all.js"></script>
<script>

function initUE(){
	var ue = UE.getEditor('editor', {
		toolbars : [ [ 
		'simpleupload','source', //源代码
		'undo', //撤销
		'redo', //重做
		'formatmatch', //格式刷
		'bold', //加粗
		'indent', //首行缩进
		'italic', //斜体
		'underline', //下划线
		'strikethrough', //删除线
		'time', //时间
		'date', //日期
		'justifyleft', //居左对齐
		'justifyright', //居右对齐
		'justifycenter', //居中对齐
		'justifyjustify', //两端对齐
		'fullscreen', //全屏
		'autotypeset', //自动排版
		'touppercase', //字母大写
		'tolowercase', //字母小写
		'forecolor', // 字体颜色
		'fontsize', // 字体大小
		'rowspacingtop',/*段前*/
		'rowspacingbottom',/*段前*/
		'lineheight',/*行间距*/
		'inserttable', //插入表格
		'insertrow', //前插入行
		'insertcol', //前插入列
		'mergeright', //右合并单元格
		'mergedown', //下合并单元格
		'deleterow', //删除行
		'deletecol', //删除列
		'splittorows', //拆分成行
		'splittocols', //拆分成列
		'splittocells', //完全拆分单元格
		'deletecaption', //删除表格标题
		'inserttitle', //插入标题
		'mergecells', //合并多个单元格
		'deletetable', //删除表格
		'backcolor', //背景色
		] ] , autoHeightEnabled : false , autoFloatEnabled : true , elementPathEnabled : false
	});
	UE.Editor.prototype._bkGetActionUrl = UE.Editor.prototype.getActionUrl;  
	UE.Editor.prototype.getActionUrl = function(action){  
		if(action == 'uploadimage' || action == 'uploadscrawl' || action == 'uploadimage'){  
			return '/${projectName}/uploaditemimage.action';  
		}else{  
			return this._bkGetActionUrl.call(this, action);  
		}
	}
}
$(function(){
	initUE();
});
var idHid
<%-- 查询页点击修改时调用此方法加载记录详情 --%>
function load(id){
	
	idHid = id;
	UE.getEditor('editor').setContent("");
	$.ajax({
		type : "POST",
		url : '/${projectName}/book/book/schBookById.action',
		data : {id:id},	
		dataType:'json',
		beforeSend:function(XMLHttpRequest){ 
		},
		success : function(result) {
			if(result.success){
				UE.getEditor('editor').setContent(result.data.goal);
			}
		}
	}); 
};

<%-- 点击保存按钮时调用此方法。(注:保存按钮在查询列表页) --%>
function submitForm(){
	
	var goal = UE.getEditor('editor').getContent();// 答案
	$.ajax({
		type : "POST",
		url : '/${projectName}/book/book/saveGoal.action',
		data : {"id":idHid,"goal":goal},	
		dataType:'json',
		beforeSend:function(XMLHttpRequest){ 
		},
		success : function(result) {
			parent.closeGoalEdit(result.msg);
		}
	});
	
}

</script>

</head>
<body>
<form id="_form" method="post" action="/${projectName}/book/book/updBook.action"  enctype="multipart/form-data" >
	<input type="hidden" name="idHid" value="" />
	<script id="editor" type="text/plain" style="width:98%;height:300px;margin:10px;"></script>
	<div align="center" >
		<a href="javascript:parent.$('#goalDIV').get(0).contentWindow.submitForm()"  class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
		<a href="javascript:parent.$('#goalDIV').dialog('close')"  class="easyui-linkbutton" iconCls="icon-cancel" >取 消</a>
	</div>
</form>
<script>

/* var ue = UE.getEditor('editor');
	ue.ready(function(){
		ue.setHeight(280);
	}) */
</script>
</body>
</html>
