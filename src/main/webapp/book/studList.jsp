<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>学生列表</title>
	<meta http-equiv="keywords" content="考试列表">
	<meta http-equiv="description" content="考试列表">
	
	<script type="text/javascript"	src="/${projectName}/book/js/lib/jquery-1.9.1.min.js"></script>
	<link rel="stylesheet" href="/${projectName}/book/js/layui/js/lib/layui2.3/css/layui.css">
	<link rel="stylesheet" href="/${projectName}/book/js/layui/css/common.css">
	<link rel="stylesheet" href="/${projectName}/book/js/layui/css/style.css">
</head>
<body class="testlist-t">
	<ul class="testlist-t-ul">
	</ul>
<script>

$(function(){
	// 初始化学生列表
	initStudList();
});

var studSocket = null;
//判断当前浏览器是否支持WebSocket
if ('WebSocket' in window) {
	var number = "${CURRENT_USER.number}";;
	if(number != null && number != "" && typeof(number) != "undefined"){
		studSocket = new WebSocket("wss://"+document.location.host+"/${projectName}/studSocket");
	}
}else {
	alert('当前浏览器 Not support websocket')
}
if(studSocket != null){

	//连接发生错误的回调方法
	studSocket.onerror = function () {
		setStudShow("WebSocket连接发生错误");
		var number = "${CURRENT_USER.number}";;
		if(number != null && number != "" && typeof(number) != "undefined"){
			studSocket = new WebSocket("wss://"+document.location.host+"/${projectName}/studSocket/"+number);
		}
	};
	
	//连接成功建立的回调方法
	studSocket.onopen = function () {
		//setStudShow("WebSocket连接成功");
		heartCheck.start();
	}

	//接收到消息的回调方法
	studSocket.onmessage = function (event) {
		setStudShow(event.data);
	}
	
	

	//监听窗口关闭事件，当窗口关闭时，主动去关闭websocket连接，防止连接还没断开就关闭窗口，server端会抛异常。
	window.onbeforeunload = function (event) {
		closeStudWebSocket();
	}

	//将消息显示在网页上
	function setStudShow(msg) {
		var data = eval("(" + msg + ")");
		var number = data.number;
		var dotype = data.doType;
		var agentId = data.agentId;
		if(dotype == "login"){
			$("#"+number+"li").addClass("on");
			$("#"+number+"li").removeClass("noon");
			$("#"+number+"li").attr("onclick","choose('"+agentId+"','on')");
		}
		if(dotype == "logout"){
			$("#"+number+"li").addClass("noon");
			$("#"+number+"li").removeClass("on");
			$("#"+number+"li").attr("onclick","choose('','')");
		}
	}

	//关闭WebSocket连接
	function closeStudWebSocket() {
		studSocket.close();
	}

	//发送消息
	function sendStudScoket( message ) {
		studSocket.send(message);
	}
}

//心跳
var heartCheck = {
	timeout: 10000,//1分钟
	timeoutObj: null,
	reset: function(){
		clearTimeout(this.timeoutObj);
		this.start();	
	},
	start: function(){
		this.timeoutObj = setTimeout(function(){
			studSocket.send("stud-HeartBeat");
		
		}, this.timeout)
	}
}


//点选学生
function choose(agentId,onLine){
	
	$("input[name='agentId']" , parent.document).val(agentId);
	parent.closeLay();
	/* if(agentId != null && agentId != "" && typeof(agentId) != "undefined"){
		if(onLine == "on"){
		}else{
			
		}
	} */
	//parent.layer.close();
}

// 初始化学生列表
function initStudList(){
	var cnumber = "${CURRENT_USER.cnumber}";
	var number = "${CURRENT_USER.number}";
	$.ajax({
		url:"/${projectName}/manager/stud/getStudDictByCnumber.action" ,
		type : "post" , 
		data : {"cnumber":cnumber} ,
		success : function(result){
			if(result.success){
				var datas = result.data;
				var html = "";
				if(datas != null){
					for( var i = 0; i < datas.length; i++ ){
						var stu = datas[i];
						
						if(stu.number != number){
							var img = "<img alt='' src='/${projectName}/book/js/layui/img/ta2.png'>";
							if(stu.photoUrl != null && stu.photoUrl != ""){
								
								img = "<img alt='' src='/${projectName}/imgs/photos/"+cnumber+"/"+stu.photoUrl+"'>";
							}
							var online = "noon";
							
							
							if(stu.isLogin == "1"){
								online = "on";
							}
							var htm = "<li id=\""+stu.number+"li\" onclick=\"choose('"+stu.agentId+"','"+online+"');\" class='testlist-t-li "+online+"'>"
								+ "<div class='testlist-t-li-bg'>"
								+ img
								+ "</div>"
								+ "<p>"+stu.number+"</p>"
								+ "</li>";
							html += htm;
						}
					}
					$(".testlist-t-ul").html(html )
				}
			}else{
				
			}
		}
	}); 
}


</script>
</body>
</html>