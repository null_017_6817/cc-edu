<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>查看课本</title><!--book-->
	<%@include file="/common/include.jsp" %>
<script>
function load(id){
	$('#_form').form('load','/${projectName}/book/book/vieBook.action?toL=true&id='+id);
};
</script>
</head>
<body>
<form id="_form">
	<input type="hidden" name='id' value=""/>
	<table style="margin: 0 auto;">
		<tr><td class='label'>：</td><td><input type='text' name='pid' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>书名或章节名：</td><td><input type='text' name='name' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>出版社：</td><td><input type='text' name='bookconcern' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>主编：</td><td><input type='text' name='editor' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>出版时间：</td><td><input type='text' name='publictime' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>封面路径：</td><td><input type='text' name='path' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>0:课本 1:目录章节：</td><td><input type='text' name='type' class='viewbox' readonly='readonly'/></td></tr>
		<!-- <tr><td class='label'>专业：</td><td><input type='text' name='profession' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>班级号：</td><td><input type='text' name='cnumber' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>学期：</td><td><input type='text' name='term' class='viewbox' readonly='readonly'/></td></tr> -->
	</table>

</form>
</body>
</html>
