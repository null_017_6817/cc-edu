<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<script type="text/javascript"	src="/${projectName}/book/js/sweetAlert/sweetalert2.min.js"></script>
<link rel="stylesheet" type="text/css"	href="/${projectName}/book/js/sweetAlert/sweetalert2.min.css" />

<div class="loginBox" id="loginBox">
	<div class="login-con">
		<span>X</span>
		<h4>登录</h4>
		<form  id="login_Form" method="post"  >
			<div><label class="icon-login"></label><input type="text" name="number" value="" placeholder="请输入登录名" /></div>
			<div><label class="icon-pasword"></label><input type="password" name="passwd" onfocus="this.type='password'"  value="" placeholder="请输入密码" /></div>
			<div><label class="icon-zuoxi"></label><input type="text" name="agentId" value="" placeholder="请输入坐席号" /></div>
			<button id="loginBtn" type="button" >登录 </button>
		</form>
	</div>
</div>

<div class="loginBox" id="changePw">
	<div class="login-con">
		<span>X</span>
		<h4>修改密码</h4>
		<form id="changePw_Form" method="post"  >
			<input type="password" name="oldPw" value="" placeholder="请输入原密码" /><br>
			<input type="password" name="newPw" value="" placeholder="请输入新密码" /><br>
			<input type="password" name="confirmPw" value="" placeholder="请再次输入新密码" />
			<button id="changePwBtn" type="button" >确认 </button>
		</form>
	</div>
</div>
<iframe id="sipexe" style= "display:none "></iframe>
