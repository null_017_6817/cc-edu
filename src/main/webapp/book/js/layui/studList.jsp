<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>学生列表</title>
	<meta http-equiv="keywords" content="考试列表">
	<meta http-equiv="description" content="考试列表">
	<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
	<link rel="stylesheet" href="/${projectName}/book/js/layui/js/lib/layui2.3/css/layui.css">
	<link rel="stylesheet" href="/${projectName}/book/js/layui/fonts/font.css">
	<link rel="stylesheet" href="/${projectName}/book/js/layui/css/common.css">
	<link rel="stylesheet" href="/${projectName}/book/js/layui/css/style.css">
</head>
<body class="testlist-t">
	<ul class="testlist-t-ul">
		<li class="testlist-t-li on">
			<div class="testlist-t-li-bg"></div>
			<p>123123123</p>
		</li>
		<li class="testlist-t-li on">
			<div class="testlist-t-li-bg"></div>
			<p>123123123</p>
		</li>
		<li class="testlist-t-li on">
			<div class="testlist-t-li-bg"></div>
			<p>123123123</p>
		</li>
		<li class="testlist-t-li on">
			<div class="testlist-t-li-bg"></div>
			<p>123123123</p>
		</li>
		<li class="testlist-t-li on">
			<div class="testlist-t-li-bg"></div>
			<p>123123123</p>
		</li>
		<li class="testlist-t-li on">
			<div class="testlist-t-li-bg"></div>
			<p>123123123</p>
		</li>
		<li class="testlist-t-li on">
			<div class="testlist-t-li-bg"></div>
			<p>123123123</p>
		</li>
		<li class="testlist-t-li on">
			<div class="testlist-t-li-bg"></div>
			<p>123123123</p>
		</li>
		<li class="testlist-t-li on">
			<div class="testlist-t-li-bg"></div>
			<p>123123123</p>
		</li>
		<li class="testlist-t-li on">
			<div class="testlist-t-li-bg"></div>
			<p>123123123</p>
		</li>
		<li class="testlist-t-li">
			<div class="testlist-t-li-bg"></div>
			<p>123123123</p>
		</li>
		<li class="testlist-t-li">
			<div class="testlist-t-li-bg"></div>
			<p>123123123</p>
		</li>
		<li class="testlist-t-li">
			<div class="testlist-t-li-bg"></div>
			<p>123123123</p>
		</li>
		<li class="testlist-t-li">
			<div class="testlist-t-li-bg"></div>
			<p>123123123</p>
		</li>
		<li class="testlist-t-li">
			<div class="testlist-t-li-bg"></div>
			<p>123123123</p>
		</li>
		<li class="testlist-t-li">
			<div class="testlist-t-li-bg"></div>
			<p>123123123</p>
		</li>
		<li class="testlist-t-li">
			<div class="testlist-t-li-bg"></div>
			<p>123123123</p>
		</li>
		<li class="testlist-t-li">
			<div class="testlist-t-li-bg"></div>
			<p>123123123</p>
		</li>
		<li class="testlist-t-li">
			<div class="testlist-t-li-bg"></div>
			<p>123123123</p>
		</li>
		<li class="testlist-t-li">
			<div class="testlist-t-li-bg"></div>
			<p>123123123</p>
		</li>
		<li class="testlist-t-li">
			<div class="testlist-t-li-bg"></div>
			<p>123123123</p>
		</li>
		<li class="testlist-t-li">
			<div class="testlist-t-li-bg"></div>
			<p>123123123</p>
		</li>
		<li class="testlist-t-li">
			<div class="testlist-t-li-bg"></div>
			<p>123123123</p>
		</li>
		<li class="testlist-t-li">
			<div class="testlist-t-li-bg"></div>
			<p>123123123</p>
		</li>
		<li class="testlist-t-li">
			<div class="testlist-t-li-bg"></div>
			<p>123123123</p>
		</li>
		<li class="testlist-t-li">
			<div class="testlist-t-li-bg"></div>
			<p>123123123</p>
		</li>
		<li class="testlist-t-li">
			<div class="testlist-t-li-bg"></div>
			<p>123123123</p>
		</li>
		<li class="testlist-t-li">
			<div class="testlist-t-li-bg"></div>
			<p>123123123</p>
		</li>
		<li class="testlist-t-li">
			<div class="testlist-t-li-bg"></div>
			<p>123123123</p>
		</li>
		<li class="testlist-t-li">
			<div class="testlist-t-li-bg"></div>
			<p>123123123</p>
		</li>
		<li class="testlist-t-li">
			<div class="testlist-t-li-bg"></div>
			<p>123123123</p>
		</li>
		<li class="testlist-t-li">
			<div class="testlist-t-li-bg"></div>
			<p>123123123</p>
		</li>
	</ul>
<script src="js/lib/jquery.min.js"></script>
<script src="js/lib/layui2.3/layui.all.js"></script>
<script src="js/common.js"></script>
</body>
</html>