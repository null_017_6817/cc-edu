
$(".circleChart#tjt1").circleChart({
            color: "#1792ee",
			backgroundColor: "#b9defa",
			background: true,
			speed: 2000,
			widthRatio: 0.1,
			unit: 'percent',
			counterclockwise: false,
			size: 110,
			startAngle: 75,
			animate: true,
			backgroundFix: true,
			lineCap: "round",
			animation: "easeInOutCubic",
			text: 0,
			redraw: false,
			cAngle: 0,
			textCenter: true,
			textSize: 30,
			textWeight: 'normal',
			textFamily: 'sans-serif',
			relativeTextSize: 1 / 7,
			autoCss: true,
			onDraw: function(el, circle) {
                circle.text(Math.round(circle.value) + "%");
            }   
        });
$(".circleChart#tjt2").circleChart({
            color: "#33cd98",
			backgroundColor: "#c2eee0",
			background: true,
			speed: 2000,
			widthRatio: 0.1,
			unit: 'percent',
			counterclockwise: false,
			size: 110,
			startAngle: 75,
			animate: true,
			backgroundFix: true,
			lineCap: "round",
			animation: "easeInOutCubic",
			text: 0,
			redraw: false,
			cAngle: 0,
			textCenter: true,
			textSize: 30,
			textWeight: 'normal',
			textFamily: 'sans-serif',
			relativeTextSize: 1 / 7,
			autoCss: true,
			onDraw: function(el, circle) {
                circle.text(Math.round(circle.value) + "%");
            }   
        });
$(".circleChart#tjt3").circleChart({
            color: "#f3a317",
			backgroundColor: "#fbe3b9",
			background: true,
			speed: 2000,
			widthRatio: 0.1,
			unit: 'percent',
			counterclockwise: false,
			size: 110,
			startAngle: 75,
			animate: true,
			backgroundFix: true,
			lineCap: "round",
			animation: "easeInOutCubic",
			text: 0,
			redraw: false,
			cAngle: 0,
			textCenter: true,
			textSize: 30,
			textWeight: 'normal',
			textFamily: 'sans-serif',
			relativeTextSize: 1 / 7,
			autoCss: true,
			onDraw: function(el, circle) {
                circle.text(Math.round(circle.value) + "%");
            }   
        });

$(".circleChart#ckzftjt1").circleChart({
            color: "#1792ee",
			backgroundColor: "#b9defa",
			background: true,
			speed: 2000,
			widthRatio: 0.1,
			unit: 'percent',
			counterclockwise: false,
			size: 78,
			startAngle: 75,
			animate: true,
			backgroundFix: true,
			lineCap: "round",
			animation: "easeInOutCubic",
			text: 0,
			redraw: false,
			cAngle: 0,
			textCenter: true,
			textSize: 20,
			textWeight: 'normal',
			textFamily: 'sans-serif',
			relativeTextSize: 1 / 7,
			autoCss: true,
			onDraw: function(el, circle) {
                circle.text(Math.round(circle.value) + "%");
            }   
        });
$(".circleChart#ckzftjt2").circleChart({
            color: "#33cd98",
			backgroundColor: "#c2eee0",
			background: true,
			speed: 2000,
			widthRatio: 0.1,
			unit: 'percent',
			counterclockwise: false,
			size: 78,
			startAngle: 75,
			animate: true,
			backgroundFix: true,
			lineCap: "round",
			animation: "easeInOutCubic",
			text: 0,
			redraw: false,
			cAngle: 0,
			textCenter: true,
			textSize: 20,
			textWeight: 'normal',
			textFamily: 'sans-serif',
			relativeTextSize: 1 / 7,
			autoCss: true,
			onDraw: function(el, circle) {
                circle.text(Math.round(circle.value) + "%");
            }   
        });
$(".circleChart#ckzftjt3").circleChart({
            color: "#f3a317",
			backgroundColor: "#fbe3b9",
			background: true,
			speed: 2000,
			widthRatio: 0.1,
			unit: 'percent',
			counterclockwise: false,
			size: 78,
			startAngle: 75,
			animate: true,
			backgroundFix: true,
			lineCap: "round",
			animation: "easeInOutCubic",
			text: 0,
			redraw: false,
			cAngle: 0,
			textCenter: true,
			textSize: 20,
			textWeight: 'normal',
			textFamily: 'sans-serif',
			relativeTextSize: 1 / 7,
			autoCss: true,
			onDraw: function(el, circle) {
                circle.text(Math.round(circle.value) + "%");
            }   
        });



function jbyhZp(){
	layer.open({
		    type: 2,
		    shadeClose: true, //点击遮罩关闭
		    skin: 'layui-layer-lan', //样式类名
		    closeBtn: 1, //不显示关闭按钮
		    area: ['780px', '780px'],
		    anim: 5,
		    title: false,
		    content: '/cc-edu/camp/trainCamp/page.action?page=xssxpj_zp',
	  	});
	}


form.on('submit(jbyhZp)', function(data){
  var mypost = JSON.stringify(data.field);//提交的资料
   
  layer.closeAll('iframe'); //关闭弹窗
});

function jbyhXzp(){
	  	layer.open({
		    type: 2,
		    shadeClose: true, //点击遮罩关闭
		    skin: 'layui-layer-lan', //样式类名
		    closeBtn: 1, //不显示关闭按钮
		    area: ['780px', '780px'],
		    anim: 5,
		    title: false,
		    content: 'xssxpj_hp.jsp',
	  	});
	}
	form.on('submit(jbyhXzp)', function(data){
  	var mypost = JSON.stringify(data.field);//提交的资料
  	 
  	layer.closeAll('iframe'); //关闭弹窗
});
function jbyhCkzf(){
	  	layer.open({
		    type: 2,
		    shadeClose: true, //点击遮罩关闭
		    skin: 'layui-layer-lan', //样式类名
		    closeBtn: 1, //不显示关闭按钮
		    area: ['910px', '730px'],
		    anim: 5,
		    title: false,
		    content: 'jbyh_ckzf.html',
	  	});
	}

//查看总分选择学生
$(".jbyhckpf-conlsbtn").click(function(){
	$(".jbyhckpf-conlsbtn").each(function(){
		$(this).removeClass("on");
	})
	$(this).addClass("on");
})
