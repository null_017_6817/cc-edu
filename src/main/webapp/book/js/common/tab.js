$(document).ready(function () {
	var widget = $('#tabs-basic');
	var tabs = widget.find('ul a'),
		content = widget.find('.tabsInfo > div');
	tabs.on('click', function (e) {
		e.preventDefault();

		// Get the data-index attribute, and show the matching content div
		var index = $(this).data('index');

		tabs.removeClass('active');
		content.removeClass('tab-content-active');
		$(this).addClass('active');
		content.eq(index).addClass('tab-content-active');
	});
});