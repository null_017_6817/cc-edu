$(function(){  
	//显示登录弹层
	$('.login').click(function () {
		$('#loginBox').show();
		$('body').css('overflow', 'hidden');
	});
	
	//关闭登录弹层
	$('.login-con span').click(function () {
		$(this).parent('div').parent('div').hide();
		$('body').css('overflow', 'auto');
	});
	
	$("#loginBtn").click(function(){
		userLogin();
	});
	
	$("#changePwBtn").click(function(){
		changePw();
	});	
	
	$("#dataFile").change(function(e){
		var file = e.target.files[0];
		if(file){
			var reader = new FileReader();
			reader.onload=function(e){
				$("img[name='imgName']").attr("src",e.target.result);
			}
			reader.readAsDataURL(file);
	}})
	changeActionTab();
})
var tabName;
// 修改学习模块tab选中效果
function changeActionTab(){
	// tabName 是学习模块页面定义的常亮
	if(tabName != null && tabName != "" && typeof(tabName) != "undefined"){
		$(".nav.clearfix").find("#"+tabName).siblings().removeClass('active');
		$(".nav.clearfix").find("#"+tabName).addClass('active');
	}
}


// 学生,老师登录
function userLogin(){
	
	var number = $("#login_Form").find("input[name='number']").val();
	var password = $("#login_Form").find("input[name='password']").val();
	var agentId = $("#login_Form").find("input[name='agentId']").val();
	if(number == "" || password == "" || agentId == ""){
		return false;
	}
	
	var param = $("#login_Form").serialize();
	
	$.ajax({
		type : "POST",
		url : '/'+projectName+'/sysmanage/login/bookLogin.action',
		data : param,	
		dataType:'json',
		success : function(result) {
			if(result.success){
				if(typeof(mainJsp) != "undefined" && mainJsp){
					loadPhotos(result.Cnumber);
					
					var person = result.person;
					if(person != null){
						var manageUrl = ""
						var htm ="";
						if(person.state != "3"){//教师
							manageUrl = "<li><a href='javascript:toManager();'>后台管理</a></li>" 
							htm = "<a href='JavaScript:void(0);' onclick='showPhoneBox();' class='icon-phone'></a><a href='javascript:void(0);' onclick=\"toPractice();\" class='icon-practice'></a><div class='user'><span class=' icon-logins'></span><i class='icon-down'></i>"
									+"<ul>" 
									+"<li>["+person.name+"]</li>"
									+"<li><a href=\"/"+projectName+"/sysmanage/logout/bookLogout.action\">注销</a></li>"
									+"<li><a href='javascript:showChangePw();'>修改密码</a></li>" 
									+manageUrl
									+"</ul></div>";		
						}else{//学生
							var stuPhotoUrl =result.stuPhotoUrl;
							var Cnumber =result.Cnumber;
							var photoUrlHtml ="";
							if(typeof(stuPhotoUrl) != "undefined" && stuPhotoUrl != null &&stuPhotoUrl != ""){//未上传头像
								photoUrlHtml ="<span class='icon-logins'></span>";//默认头像		
							}else{
								photoUrlHtml ="<img src='/${projectName}/imgs/photos/"+Cnumber+"/"+stuPhotoUrl+"' height='50' width='50'>";//服务器头像
							}
							var htm = "<a href='JavaScript:void(0);' onclick='showPhoneBox();' class='icon-phone'></a><a href='javascript:void(0);' onclick=\"toPractice();\" class='icon-practice'></a><div class='user'>"
								+ photoUrlHtml
								+"<i class='icon-down'></i>"
								+"<ul>" 
								+"<li>["+person.name+"]</li>"
								+"<li><a href=\"/"+projectName+"/sysmanage/logout/bookLogout.action\">注销</a></li>"
								+"<li><a href='javascript:showChangePw();'>修改密码</a></li>" 
								+"</ul></div>";		
						}
								
						
						$(".loginDiv").html(htm);
						$('.login-con span').trigger('click');
						
						
					}else{
						swal({ 
							title: "登录失败！请刷新后再试", 
							type: "error"});
					}
					
				}else{
				}
				window.location.href= "/"+projectName+"/"
				/*var person = result.person;
				if(person != null){
					var manageUrl = ""
					if(person.state != "3"){
						manageUrl = "<li><a href='javascript:toManager();'>后台管理</a></li>" 
					}
					
					var htm = "<a href='' class='icon-phone'></a><a href='javascript:void(0);' onclick=\"toPractice();\" class='icon-practice'></a><div class='user'><span class=' icon-logins'></span><i class='icon-down'></i>"
						+"<ul>" 
						+"<li>["+person.name+"]</li>"
						+"<li><a href=\"/"+projectName+"/sysmanage/logout/bookLogout.action\">注销</a></li>"
						+"<li><a href='javascript:showChangePw();'>修改密码</a></li>" 
						+manageUrl
						+"</ul></div>";				
					
					$(".loginDiv").html(htm);
								
					$('.login-con span').trigger('click');
					// 如果为实训营地页面则触发判断里的方法
					if(typeof(isSXYD) != "undefined" && isSXYD){
						$("input[name='currUser']").val(person.agentId);
						doChange(person.state);
						showAnswer(person.state,person);
					}
					
					// 如果为训练题库则刷新页面中iframe部分
					if(typeof(isTest) != "undefined" && isTest){
						iframe.window.location.reload()
					}
					
					if(typeof(mainJsp) != "undefined" && mainJsp){
						showExam(person.state);
					}
					
					if(typeof(isCatalog) != "undefined" && isCatalog){
						isStu(person.state);
					}
				}*/
			}else{
				swal({ 
					title: result.msg, 
					type: "error"});
			}
		}
	});
}

//加载班级头像文件
function loadPhotos(cnumber){
	var param = {"cnumber":cnumber};
	$.ajax({
		type : "POST",
		url : '/'+projectName+'/sysmanage/login/loadPhotos.action',
		data : param,	
		success : function(result) {
			if(result.success){
				console.log("头像加载成功");
			}else{
				console.log("头像加载失败");
			}
		}
	});
}

// 显示修改密码的弹出框
function showChangePw(){
	$('#changePw').find("input[type='password']").val('');
	$('#changePw').show();
	$('body').css('overflow', 'hidden');
}

// 修改密码
function changePw(){
	
	var param = $("#changePw_Form").serialize();
	
	$.ajax({
		type : "POST",
		url : '/'+projectName+'/sysmanage/changePw/changePw.action',
		data : param,	
		success : function(result) {
			if(result.success){
				$('#changePw').find('.login-con span').trigger('click');
				swal({ 
					title: "修改成功", 
					type: "success"});
			}else{
				swal({ 
					title: result.msg, 
					type: "error"});
			}
		}
	});
}

function showPhoneBox(){
	$("#sipexe").attr("src", "sip:");
}

function newAlert(msg,type){
	swal({ title: msg,
		type: type});
}

// 后台管理
function toManager(){
	window.open('/'+projectName+'/sysmanage/login/page.action?page=main',"main");
}

// 实训行业
function toPractice(){
	//window.open('/'+projectName+'/book/book/page.action?page=entrance');
	window.location.href='/'+projectName+'/book/book/page.action?page=entrance';
}

// 进入短信训练页面
function toMessage(){
	//window.open('/'+projectName+'/book/book/page.action?page=entrance');
	window.location.href='/'+projectName+'/book/book/page.action?page=messageTrain';
}

//进入邮件训练页面
function toMail(){
	//window.open('/'+projectName+'/book/book/page.action?page=entrance');
	window.location.href='/'+projectName+'/book/book/page.action?page=mailTrain';
}

//进入考试列表页面
function toExam(){
	window.location.href='/'+projectName+'/book/book/page.action?page=examList';
}

document.onkeydown=function(event){
	var e = event || window.event || arguments.callee.caller.arguments[0];
	if(e && e.keyCode==13){ // enter 键
		//要做的事情
		if($('#loginBox').css('display') == "block"){
			userLogin();
		}
		if($('#changePw').css('display') == "block"){
			changePw();
		}
	}
}

if(typeof(mailSocket) != "undefined" && mailSocket != null){
	mailSocket.onmessage = function (event) {
		art.dialog.notice({
			title: '提示',
			width: 300,// 必须指定一个像素宽度值或者百分比，否则浏览器窗口改变可能导致artDialog收缩
			content: event.data,
			time: 5
		});
	}
}

if(typeof(messageSocket) != "undefined" && messageSocket != null){
	messageSocket.onmessage = function (event) {
		var msg = eval("(" + event.data + ")");
		var suc = msg.success;
		if(suc == "success"){
			art.dialog.notice({
				title: '提示',
				width: 250,// 必须指定一个像素宽度值或者百分比，否则浏览器窗口改变可能导致artDialog收缩
				content: '您有一条未读信息，请查收',
				time: 5
			});
		}else{
			var opt = msg.opt;
			if(opt == "logout"){
				swal({ 
					title: '提示', 
					text: '系统已达到最大登录人数，请稍后再重新尝试登录', 
					showCancelButton: false,
					showCloseButton:false,
					confirmButtonColor: '#3085d6',
					confirmButtonText: '好的', 
				}).then(function(confirm){
					if(confirm.value){
						// 弹出登录窗口
						$('.login').trigger("click")
						window.location.href='/'+projectName+'/sysmanage/logout/bookLogout.action';
					}
				}) 
			}
		}
	}
}


//info time
$(function () {
	var show_day=new Array('星期日','星期一','星期二','星期三','星期四','星期五','星期六'),
		time=new Date(),
		year=time.getFullYear(),
		month=time.getMonth()+1, 
		date=time.getDate(), 
		day=time.getDay(); 
		
	//2017年09月08日  &nbsp; 星期五
	$('.info').find($('.date')).html(year+'年'+month+'月'+date+'日'+'  '+show_day[day]);
})