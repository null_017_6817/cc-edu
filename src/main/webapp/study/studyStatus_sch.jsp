<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.qtrmoon.com/tags-dictionary" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>查询</title><!--studyStatus-->
	<%@include file="/common/include.jsp" %>

<script>
var winToggle;
$(function(){
	winToggle=new QWinToggle().regist(["studyStatusEditWin","studyStatusViewWin"]);
	var pager = $('#studyStatusGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});			
})

<%-- 执行查询，点击查询按钮时调用 --%>
function doStudyStatusSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#studyStatusGrid').datagrid('load',{
		number:$('#number').val(),
		status:$('#status').val()
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatStudyStatusOper(val,row,index){  
    return "&emsp;<a href='javascript:void(0)' onclick='updStudyStatus("+val+")' class='_oper' title='修改'>&nbsp;<i class='icon-edit'></i>&nbsp;</a>"
    			+"<a href='javascript:void(0)' onclick='vieStudyStatus("+val+")' class='_oper' title='查看'>&nbsp;<i class='icon-info-sign'></i>&nbsp;</a>&emsp;"; 
} 

<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
function updStudyStatus(id){
	winToggle.open("studyStatusEditWin");
	$('#studyStatusEditWin').get(0).contentWindow.load(id);
} 

<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
function vieStudyStatus(id){
	winToggle.open("studyStatusViewWin");
	$('#studyStatusViewWin').get(0).contentWindow.load(id);
}

<%-- 删除记录 --%>
function delStudyStatus(){
	var ids=_collectIds('#studyStatusGrid');
	if(ids.length==0){$.messager.alert('敬告','请选择删除的记录');return;}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/study/studyStatus/delStudyStatus.action",{ids:ids},function(data){
				$('#studyStatusGrid').datagrid('reload');
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#studyStatusEditWin').dialog('close');
	$('#studyStatusGrid').datagrid('reload');
}
</script>
</head>
<body>
<table id="studyStatusGrid" title="" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#studyStatusGridToolbar',collapsible:true,url:'/${projectName}/study/studyStatus/schStudyStatus.action',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" ></th>
			<th data-options="field:'bookId',width:80,sortable:true">章节id</th>
			<th data-options="field:'number',sortable:true">学号</th>
			<th data-options="field:'status',sortable:true">状态</th>

			<th data-options="field:'_oper',align:'center',formatter:formatStudyStatusOper">操作</th>
		</tr>
	</thead>
</table>
<%-- 查询条件设定栏 --%>
<div id="studyStatusGridToolbar">
	<form id="_form" class="shline">
	学号：<input name='number' id="number" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	状态：<input name='status' id="status" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>

	<a href="javascript:void(0)" onclick="doStudyStatusSearch()" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>
	</form>
	<div class="opline">
		<a href="javascript:updStudyStatus(0)" class="btn btn-success"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">添加</span></a>
		<a href="javascript:delStudyStatus()" class="btn btn-danger"><span class="icon-trash" style="font-size:16px"></span> <span style="font-size:14px">删除</span></a>
	</div>
</div>


<%-- 修改or添加面板 --%>
<iframe id="studyStatusEditWin" src="/${projectName}/study/studyStatus/page.action?page=studyStatus_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#studyStatusEditButton'" title="编辑" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="studyStatusEditButton">
	<a href="javascript:void(0)" onclick="$('#studyStatusEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#studyStatusEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >取 消</a>
</div>
<%-- 查看面板 --%>
<iframe id="studyStatusViewWin" src="/${projectName}/study/studyStatus/page.action?page=studyStatus_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#studyStatusViewButton'" title="查看" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="studyStatusViewButton">
	<a href="javascript:$('#studyStatusViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >关 闭</a>
</div>
</body>
</html>
