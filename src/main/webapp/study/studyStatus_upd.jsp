<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.qtrmoon.com/tags-dictionary" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>编辑</title><!--studyStatus-->
	<%@include file="/common/include.jsp" %>
<script>
var submitable=true;//防止重复提交
<%-- 查询页点击修改时调用此方法加载记录详情 --%>
function load(id){
	$('#_form').form('load','/${projectName}/study/studyStatus/vieStudyStatus.action?id='+id);
};

<%-- 点击保存按钮时调用此方法。(注:保存按钮在查询列表页) --%>
function submitForm(){
	if(!submitable)return;
	if(!$("#_form").form("validate")){
		return;
	}
	$('#_form').form('submit',{
		success:function(data) {
			parent.reloadData();
		}
	});
	submitable=false;
}
</script>
</head>
<body>
<form id="_form" method="post" action="/${projectName}/study/studyStatus/updStudyStatus.action">
	<input type="hidden" name='id' id='id'/>
	<table style='width:100%'>
		<tr><td class='label' style="width:120px">章节id：</td><td><input id="bookId" name="bookId" class="easyui-textbox" data-options="required:true,validType:['int']"/></td></tr>
		<tr><td class='label' >学号：</td><td><input id="number" name="number" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label' >状态：</td><td><input id="status" name="status" class="easyui-textbox" data-options="required:true"/></td></tr>
	</table>

</form>
</body>
</html>
