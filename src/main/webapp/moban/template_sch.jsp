<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.qtrmoon.com/tags-dictionary" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>模板表查询</title><!--template-->
	<%@include file="/common/include.jsp" %>

<script>
var winToggle;
$(function(){
	winToggle=new QWinToggle().regist(["templateEditWin","templateViewWin","templateAddWin"]);
	var pager = $('#templateGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});			
})

<%-- 执行查询，点击查询按钮时调用 --%>
function doTemplateSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#templateGrid').datagrid('load',{
		templateName:$('#templateName').val(),
		founderId:$('#foundId').val()
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatTemplateOper(val,row,index){  
    return "&emsp;<a href='javascript:void(0)' onclick='updTemplate("+val+")' class='_oper' title='编辑'>&nbsp;<i class='icon-edit'></i>&nbsp;</a>"
    			+"<a href='javascript:void(0)' onclick='vieTemplate("+val+")' class='_oper' title='查看'>&nbsp;<i class='icon-info-sign'></i>&nbsp;</a>&emsp;"; 
} 

<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
function updTemplate(id){
	winToggle.open("templateEditWin");
	$('#templateEditWin').get(0).contentWindow.load(id);
} 

<%-- 打开添加页面 --%>
function addTemplate(){
	winToggle.open("templateAddWin");
	$('#templateAddWin').get(0).contentWindow.load(0);
} 

<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
function vieTemplate(id){
	winToggle.open("templateViewWin");
	$('#templateViewWin').get(0).contentWindow.load(id);
}

<%-- 删除记录 --%>
function delTemplate(){
	var checkedItems = $('#templateGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/template/delTemplate.action",{ids:ids},function(data){
				$('#templateGrid').datagrid('reload');
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#templateEditWin').dialog('close');
	$('#templateAddWin').dialog('close');
	$('#templateGrid').datagrid('reload');
}
</script>
</head>
<body>
<table id="templateGrid" title="" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#templateGridToolbar',collapsible:true,url:'/${projectName}/template/schTemplate.action',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" > </th>
			<th data-options="field:'templateName',width:2,sortable:true">模板名称</th>
			<th data-options="field:'totalScoreRatio',sortable:true">总分比例</th>
			<th data-options="field:'score',sortable:true">分值</th>
			<th data-options="field:'memo',width:10,sortable:true">说明</th>
			<th data-options="field:'founderId',sortable:true">创建人</th>
			<th data-options="field:'creationTime',sortable:true">创建时间</th>

			<th data-options="field:'_oper',align:'center',formatter:formatTemplateOper">操作</th>
		</tr>
	</thead>
</table>
<%-- 查询条件设定栏 --%>
<div id="templateGridToolbar">
	<form id="_form" class="shline">
	模板名称：<input name='templateName' id="templateName" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>
	创建人：<input name='foundId' id="foundId" class='easyui-textbox' value='' data-options="iconWidth:16,
icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
}]"/>

	<a href="javascript:void(0)" onclick="doTemplateSearch()" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>
	<a href="javascript:addTemplate()" class="btn btn-success"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">添加</span></a>&emsp;
	<a href="javascript:delTemplate()" class="btn btn-danger"><span class="icon-trash" style="font-size:16px"></span> <span style="font-size:14px">删除</span></a>
	</form>
</div>


<%-- 修改面板 --%>
<iframe id="templateEditWin" src="/${projectName}/template/page.action?page=template_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#templateEditButton'" title="编辑模板" style="width:1000px;height:750px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="templateEditButton">
	<a href="javascript:void(0)" onclick="$('#templateEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#templateEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >取 消</a>
</div>



<%-- 添加面板 --%>
<iframe id="templateAddWin" src="/${projectName}/template/page.action?page=template_add" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#templateAddButton'" title="添加模板" style="width:1000px;height:750px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="templateAddButton">
	<a href="javascript:void(0)" onclick="$('#templateAddWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#templateAddWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >取 消</a>
</div>


<%-- 查看面板 --%>
<iframe id="templateViewWin" src="/${projectName}/template/page.action?page=template_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#templateViewButton'" title="查看模板" style="width:550px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="templateViewButton">
	<a href="javascript:$('#templateViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >关 闭</a>
</div>
</body>
</html>
