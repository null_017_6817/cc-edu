<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.qtrmoon.com/tags-dictionary" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>编辑模板表</title><!--template-->
	<%@include file="/common/include.jsp" %>
<script>
var winToggle;
$(function(){
	winToggle=new QWinToggle().regist(["templateIndexAddWin"]);
	$('#templateIndexAddWin').dialog('close');
});
<%-- 查询页点击修改时调用此方法加载记录详情 --%>
function load(id){
	$('#_form').form('load','/${projectName}/template/vieTemplate.action?id='+id);
	
	$('#_form').form({
		onLoadSuccess : function(data) {
			$('#totalScoreRatio1').textbox('setValue','');
			$('#totalScoreRatio2').textbox('setValue','');
			$('#totalScoreRatio3').textbox('setValue','');
			$('#zhibiao-category-list').html('');
		}
	});
	
};
function reloadData(_id,_indexQuantity,_indexContent,tcid){
	$('#templateIndexAddWin').dialog('close');
	var trs = $('#zhibiao-category-list tr');
	
	//分类ID集合
	var ids = $(trs).find('input[name="classificationName"]');
	//遍历集合，找到值为_id的行
	for(var m = 0 ;m < ids.length;m++){
		if(m == _id-1){
			var td = $(trs[m]).find('td:eq(3)');
			$(td).html(_indexQuantity+'<input name="indexQuantity" type="hidden" value='+_indexQuantity+'><input name="indexContent" type="hidden" value='+_indexContent+'>')
			break;
		}
	}
}
<%-- 点击保存按钮时调用此方法。(注:保存按钮在查询列表页) --%>
function submitForm(){
	if(!$("#_form").form("validate")){
		return;
	}
	var totalScoreRatio1  = $('#totalScoreRatio1').textbox("getValue");
	var totalScoreRatio2  = $('#totalScoreRatio2').textbox("getValue");
	var totalScoreRatio3  = $('#totalScoreRatio3').textbox("getValue");
	if(parseInt(totalScoreRatio1) + parseInt(totalScoreRatio2) +parseInt(totalScoreRatio3)!=100){
		$.messager.alert('提示','自评、互评、师评三个选项之和必须等于100');
		return;
	}
	$('#totalScoreRatio').val(totalScoreRatio1+':'+totalScoreRatio2+':'+totalScoreRatio3);
	
	var fenlei = $('#zhibiao-category-list').html();
	if(fenlei == ''){
		$.messager.alert('提示','请添加分类');
		return;
	}
	
	var fenleis = $('#zhibiao-category-list tr');
	var calculationProportions = $(fenleis).find('input[name="calculationProportion"]');
	var totalcalculationProportions = 0;
	for(var i = 0 ;i < calculationProportions.length;i++){
		totalcalculationProportions+=parseInt(calculationProportions[i].value);
	}
	if(totalcalculationProportions!=100){
		$.messager.alert('提示','所有分类计算比例总和必须等于100');
		return;
	}
	
	//遍历分类列表
	for(var j= 0 ;j < fenleis.length;j++) {
		$(fenleis[j]).find('input[name="serialNumber"]').attr('name','templateCategoryList['+j+'].serialNumber');
		$(fenleis[j]).find('input[name="classificationName"]').attr('name','templateCategoryList['+j+'].classificationName');
		$(fenleis[j]).find('input[name="calculationProportion"]').attr('name','templateCategoryList['+j+'].calculationProportion');
		$(fenleis[j]).find('input[name="indexContent"]').attr('name','templateCategoryList['+j+'].indexContent');
		$(fenleis[j]).find('input[name="indexQuantity"]').attr('name','templateCategoryList['+j+'].indexQuantity');
	}
	
	$('#_form').form('submit',{
      success:function(data) {
      	parent.reloadData();
      }
   });
}
</script>
<style>
	.score_ratio {width:60px!important}
	.custom_text_box{width:50%!important}
	thead {background: #f2f2f2}
	#zhibiao{margin-left:64px;margin-top:14px;width:85%;}
	#zhibiao td {height: 27px!important;padding-left: 3px;text-align: center;}
	#zhibiao td input {text-align: center}
	#zhibiao .compute-ratio{width:60px;}
	#zhibiao .category{width:40%;}
</style>
<script>
	var i = 0;
	var zhibiao = {
			//新增一行
			addRow:function(){
				i++;
				var tr = '<tr align="center">';
				tr+='<td style="width:40px"><span>'+i+'</span><input type="hidden" name="serialNumber" value='+i+'></input></td>';
				tr+='<td style="width:140px"><input name="classificationName" class="easyui-validatebox easyui-textbox category"   data-options="required:true"  type="text" value="名称1"/></td>';
				tr+='<td style="width:60px"><input  name="calculationProportion" class="easyui-validatebox easyui-numberbox compute-ratio"  data-options="required:true"  type="text" value="0"/></td>';
				tr+='<td style="width:80px">0<input name="id" value="0" type="hidden"/></td>';
				tr+='<td style="width:80px"><span onclick=manageIndex("'+i+'") title="维护指标" class="icon-edit" style="font-size:16px"></span>&nbsp;&nbsp;<span  onclick="zhibiao.delRow(this)" title="删除分类" class="icon-trash" style="font-size:16px"></span></td>';
				/* tr+='<td style="width:80px"><span  onclick="zhibiao.delRow(this)" title="删除分类" class="icon-trash" style="font-size:16px"></span></td>'; */
				var trs= $('#zhibiao-category-list').html();
				
				if(trs==''){
					$('#zhibiao-category-list').append(tr);
				}else {
					$(tr).insertAfter($('#zhibiao-category-list tr:last'));
				}
				$.parser.parse('#zhibiao-category-list');
				//重新计算已存在行的序号
				this.calculateSerialNumber();
				
			},
			//删除行
			delRow:function(obj){
				i--;
				$(obj).parent().parent().remove();
				$.parser.parse('#zhibiao-category-list');
				//重新计算已存在行的序号
				this.calculateSerialNumber();
			},
			//重新计算序号
			calculateSerialNumber:function(){
				var fenleis = $('#zhibiao-category-list tr');
				//遍历分类列表
				for(var j= 0,k=1 ;j < fenleis.length;j++,k++) {
					var tr = fenleis[j];
					var serialNumberTd = $(tr).find('td')[0];
					$(serialNumberTd).find('span').html(k);
					$(serialNumberTd).find('input').val(k);
					var mIndex = $(tr).find('td')[4];
					$(mIndex).find('span').eq(0).attr("onclick","manageIndex("+k+")");
				}
			}
	};

	<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
	function manageIndex(_id){
		var trs = $('#zhibiao-category-list tr');
		var classificationName=$(trs[_id-1]).find('input[name="classificationName"]').val();
		var indexContent=$(trs[_id-1]).find('input[name="indexContent"]').val();
		winToggle.open("templateIndexAddWin");
		$('#templateIndexAddWin').get(0).contentWindow.load(_id,0,classificationName,indexContent);
	}
</script>
</head>
<body>
<form id="_form" method="post" action="/${projectName}/template/updTemplate.action">
	<input id="totalScoreRatio" name="totalScoreRatio" type="hidden"/>
	<table>
		<tr><td class='label' style="width:120px">模板名称：</td><td><input id="templateName" name="templateName" class="easyui-textbox custom_text_box" data-options="required:true"/></td></tr>
		<tr><td class='label' >总分比例：</td>
		<td>
		<input id="totalScoreRatio1" name="totalScoreRatio1" class="easyui-numberbox score_ratio" data-options="required:true,min:0,max:100,percision:1" prompt="0~100"/>:
		<input id="totalScoreRatio2" name="totalScoreRatio2" class="easyui-numberbox score_ratio" data-options="required:true,min:0,max:100,percision:1" prompt="0~100"/>:
		<input id="totalScoreRatio3" name="totalScoreRatio3" class="easyui-numberbox score_ratio" data-options="required:true,min:0,max:100,percision:1" prompt="0~100"/>（自评：互评：师评）
		</td>
		</tr>
		<tr><td class='label' >分值设定：</td><td><input id="score" name="score" class="easyui-textbox custom_text_box" data-options="required:true" prompt="填写分值，以逗号隔开。例：10,5,0"/></td></tr>
	    <tr><td class='label' >说明：</td><td><input id="memo" name="memo" class="custom_text_box easyui-textbox"/></td></tr>
		<tr><td class='label' >考核指标：</td><td><a href="javascript:void(0)" onclick="zhibiao.addRow()" class="btn"> <span style="font-size:14px">添加分类</span></a></td></tr>
	</table>
	
	<table id="zhibiao">
		<thead>
			<tr align="center">
				<td style="width:40px">序号</td>
				<td style="width:170px">分类名称</td>
				<td style="width:80px">计算比例(%)</td>
				<td style="width:80px">指标数量</td>
				<td style="width:40px">操作</td>
			</tr>
		</thead>
		<tbody id="zhibiao-category-list"></tbody>
		
	</table>
</form>
</body>


</html>
<%-- 添加面板 --%>
<iframe id="templateIndexAddWin" src="/${projectName}/template/page.action?page=templateIndex_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#templateIndexAddButton'" title="考核指标列表" style="width:750px;height:550px;padding:5px;" scrolling="auto" frameborder="0"></iframe>
<div id="templateIndexAddButton">
	<a href="javascript:void(0)" onclick="$('#templateIndexAddWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#templateIndexAddWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >取 消</a>
</div>
