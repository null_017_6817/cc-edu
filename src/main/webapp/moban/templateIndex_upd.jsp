<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.qtrmoon.com/tags-dictionary" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>编辑模板指标表</title><!--templateIndex-->
	<%@include file="/common/include.jsp" %>
<script>
var tid=0;
<%-- 查询页点击修改时调用此方法加载记录详情 --%>
function load(_id,_seriaNumber,_classificationName,_indexContent){
	if(_seriaNumber==0){
		tid=_id;
		$('#zhibiao-index-list').html('');
		$('#index_title').html(_id+"、"+_classificationName);
		$("#id").val(0);
		if(_indexContent!=null){
			var templateIndexs = $.parseJSON(_indexContent);
			for(var i = 0 ;i < templateIndexs.length;i++){
				var templateIndex = templateIndexs[i];
				zhibiao.rendRow(templateIndex.serialNumber,templateIndex.indexContent);
			}
		}
	}else{
		//每次加载优先清除指标列表
		$('#zhibiao-index-list').html('');
		$('#index_title').html(_seriaNumber+"、"+_classificationName);
		$("#id").val(_id);
		if(_indexContent!=null){
			var templateIndexs = $.parseJSON(_indexContent);
			if(templateIndexs!=null){
				for(var i = 0 ;i < templateIndexs.length;i++){
					var templateIndex = templateIndexs[i];
					zhibiao.rendRow(templateIndex.serialNumber,templateIndex.indexContent);
				}
			}
		}
		/* $('#_form').form('load','/${projectName}/template/getTemplateCategoryIndex.action?id='+_id);
		
		$('#_form').form({
			onLoadSuccess : function(data) {
				//解析指标列表，进行表格行渲染
				if(data.indexContent != null) {
					var templateIndexs = $.parseJSON(data.indexContent);
					for(var i = 0 ;i < templateIndexs.length;i++){
						var templateIndex = templateIndexs[i];
						zhibiao.rendRow(templateIndex.serialNumber,templateIndex.indexContent);
					}
				}
			}
		}); */
	}
};

<%-- 点击保存按钮时调用此方法。(注:保存按钮在查询列表页) --%>
function submitForm(){
	if(!$("#_form").form("validate")){
		return;
	}
	//分类处理
	var index = $('#zhibiao-index-list').html();
	if(index == ''){
		$.messager.alert('提示','请添加考核指标');
		return;
	}
	
	var indexs = $('#zhibiao-index-list tr');
	//指标数量
	$('#indexQuantity').val(indexs.length);
	
	var v_indexs = [];
	
	//遍历指标列表
	for(var j= 0 ;j < indexs.length;j++) {
		var v_index = {};
		v_index.serialNumber = 	$(indexs[j]).find('input[name="serialNumber-row"]').val();
		v_index.indexContent = $(indexs[j]).find('input[name="indexContent-row"]').val();
		v_indexs.push(v_index);
	}
	//指标内容
	$('#indexContent').val(JSON.stringify(v_indexs));
	
	if($('#id').val()==0){
		parent.reloadData(tid,indexs.length,JSON.stringify(v_indexs),0);
	}else{
		parent.reloadData($('#id').val(),indexs.length,JSON.stringify(v_indexs),$('#id').val());
	}
	
	/* $('#_form').form('submit',{
      success:function(data) {
      	parent.reloadData($('#id').val(),indexs.length);
      }
   }); */
}

var i = 0;
var zhibiao = {
		//新增一行,新增一行设置指标ID=0
		addRow:function(){
			i++;
			var tr = '<tr align="center">';
			tr+='<td style="width:8%"><span>'+i+'</span><input type="hidden" name="serialNumber-row" value='+i+'></input></td>';
			tr+='<td style="width:82%"><input name="indexContent-row" class="easyui-validatebox easyui-textbox custom_text_box"   data-options="required:true"/></td>';
			tr+='<td style="width:10%"><span  onclick="zhibiao.delRow(this)" title="删除" class="icon-trash" style="font-size:16px"></span></td>';
			var trs= $('#zhibiao-index-list').html();
			
			if(trs==''){
				$('#zhibiao-index-list').append(tr);
			}else {
				$(tr).insertAfter($('#zhibiao-index-list tr:last'));
			}
			//重新解析表格里面的元素
			$.parser.parse($('#zhibiao-index-list'));
			//重新计算已存在行的序号
			this.calculateSerialNumber();
		},
		//删除行
		delRow:function(obj){
			i--;
			$(obj).parent().parent().remove();
			//重新计算已存在行的序号
			this.calculateSerialNumber();
		},
		
		//渲染一行
		rendRow:function(_serialNumber,_indexContent){
			i++;
			var tr = '<tr align="center">';
			tr+='<td style="width:8%"><span>'+_serialNumber+'</span><input type="hidden" name="serialNumber-row" value='+_serialNumber+'></input></td>';
			tr+='<td style="width:82%"><input name="indexContent-row" class="easyui-validatebox easyui-textbox custom_text_box"   data-options="required:true"  type="text" value="'+_indexContent+'"/></td>';
			tr+='<td style="width:10%"></span><span  onclick="zhibiao.delRow(this)" title="删除" class="icon-trash" style="font-size:16px"></span></td>';
			var trs= $('#zhibiao-index-list').html();
			
			if(trs==''){
				$('#zhibiao-index-list').append(tr);
			}else {
				$(tr).insertAfter($('#zhibiao-index-list tr:last'));
			}
			//重新解析表格里面的元素
			$.parser.parse($('#zhibiao-index-list'));
		},
		//重新计算序号
		calculateSerialNumber:function(){
			var indexs = $('#zhibiao-index-list tr');

			//遍历指标列表
			for(var j= 0,k=1 ;j < indexs.length;j++,k++) {
				var tr = indexs[j];
				var serialNumberTd = $(tr).find('td')[0];
				$(serialNumberTd).find('span').html(k);
				$(serialNumberTd).find('input').val(k);
			}
		}
};

</script>
<style>
	.custom_text_box{width:70%!important}
	thead {background: #f2f2f2}
	#zhibiao{margin-left:32px;margin-top:20px;width:94%; margin-right:32px;}
	#zhibiao td {height: 27px!important;padding-left: 3px;text-align: center;}
	#zhibiao td input {text-align: left}
	#zhibiao .compute-ratio{width:60px;}
	#zhibiao .category{width:170px;}
	.index_title {margin-left:32px;margin-top:13px}
</style>
</head>
<body>
<form id="_form" method="post" action="#">
	<input id="id" name="id" type="hidden"/>
	<input id="indexQuantity" name="indexQuantity" type="hidden"/>
	<input id="indexContent" name="indexContent" type="hidden"/>
	
	<div  class="index_title"><span id="index_title"></span>：<a style="margin-left:15px;" href="javascript:void(0)" onclick="zhibiao.addRow()" class="btn"> <span style="font-size:14px">添加指标</span></a></div>
	
	<table id="zhibiao">
		<thead>
			<tr align="center">
				<td style="width:8%">序号</td>
				<td style="width:82%">指标内容</td>
				<td style="width:10%">操作</td>
			</tr>
		</thead>
		<tbody id="zhibiao-index-list"></tbody>
		
	</table>
</form>

</body>
</html>
