<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.qtrmoon.com/tags-dictionary" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>查看模板表</title><!--template-->
	<%@include file="/common/include.jsp" %>
<script>
function load(id){
	$('#_form').form('load','/${projectName}/template/vieTemplate.action?id='+id);
	
	$('#_form').form({
		"onLoadSuccess":function(data){
			rendIndexData(data);
		}
	})
};

//渲染指标
function rendIndexData(data){
	var categorys = $.parseJSON(data.templateCategories);
	var zhibiaos='';
	for(var i = 0 ; i <categorys.length;i++){
		var category = categorys[i];
		zhibiaos+='<div class="panel_state"><div class="ax_label">';
		zhibiaos+='<span>'+category.serialNumber+'、'+category.classificationName+'（'+category.calculationProportion+'%）</span></div>';
		var zhibiao = $.parseJSON(category.indexContent);
		if(zhibiao!=null&&zhibiao.length>0){
			for(var k = 0 ; k < zhibiao.length;k++){
				zhibiaos+='<div class="ax_default">';
				zhibiaos+='<div class="shu_default "><img class="img " src="../imgs/shu.png"></div>';
				zhibiaos+='<div class="heng_default"><img class="img" src="../imgs/heng.png"></div></div>';
				zhibiaos+='<div class="ax_text">';
				zhibiaos+='<span>'+zhibiao[k].indexContent+'</span></div>';
			}
		}
	}
	$('.zhibiao-list').html('').html(zhibiaos);
}
</script>

<style type="text/css">
	#_form table td{
		padding-left:15px;
	}
	.zhiB{
		font-size:0;
	}
	.zhibiao_title{
		display:inline-block;
		vertical-align:top;
		margin-bottom:10px;
		font-size:12px;
		width:18%;
		padding:3px 0 3px 15px;
		box-sizing:border-box;
	}
	.zhibiao-list {
		margin-top:3px;
		display:inline-block;
		vertical-align:top;
		border:1px solid #ccc;
		width:82%;
		box-sizing:border-box;
		padding:10px;
		font-size:12px;
	}
	.zhibiao-list span {
		display:block;
		margin-left:10%;
	}
	.zhibiao-list .ax_label span{
		margin:0;
		margin-bottom:10px;
	}
	.zhibiao-list .ax_text{
		height: 26px;
	    line-height: 46px;
	}
	.vertical-line{
		border-left:1px dashed #ccc;
	}
	.zhibiao-list .ax_default{
		position:relative;
	}
	.zhibiao-list .shu_default,.zhibiao-list .heng_default{
		position:absolute;
		left:10px;
	}
	.zhibiao-list .shu_default{
		top:0;
	}
	.zhibiao-list .heng_default{
		top:16px;
	}
	.zhibiao-list .panel_state{
		margin:10px 0 15px 0;
	}
	
</style>
</head>
<body>
<form id="_form">
	<input type="hidden" name='id' value=""/>
	<input type="hidden" name='founderId' value=""/>
	<input type="hidden" name='creationTime' value=""/>
	<table>
		<tr><td class='label'>模板名称：</td><td><input type='text' name='templateName' class='viewbox' readonly='readonly' style="width: 400px"/></td></tr>
		<tr><td class='label'>总分比例：</td><td><input type='text' name='totalScoreRatio' class='viewbox' readonly='readonly' style="width: 400px"/></td></tr>
		<tr><td class='label'>分值设定：</td><td><input type='text' name='score' class='viewbox' readonly='readonly' style="width: 400px"/></td></tr>
		<!-- <tr><td class='label'>说明：</td><td><input type='text' name='memo' class='viewbox' readonly='readonly'/></td></tr> -->
	</table>
	<div class="zhiB">
		<span class="zhibiao_title">考核指标：</span>
		<div class="zhibiao-list">
		      
		</div>
	</div>
</form>
</body>
</html>
