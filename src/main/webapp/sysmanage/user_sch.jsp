<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>用户表查询</title>
	<%@include file="/common/include.jsp" %>
	<style>
	.easyui-linkbutton{margin-right:5px}.conditions.hide{display:none}.conditions .con-span{display:inline-block;width:70px}.opt-buttons{margin-left:-20px}
	.textbox-text{width:166px;height:30px;line-height:30px;border-radius:6px}
	.updpass{background-image: url(/${projectName}/imgs/updpass.png);    
		width: 16px;
	    height: 16px;
	    display: inline-block;
	    background-size: 90%;
	    background-repeat: no-repeat;
	    vertical-align: -3px;
    }
	</style>
</head>
<body class="easyui-layout">
	<%-- <div title="组织机构" style="width:260px;" data-options="iconCls:'icon-sitemap',region:'west',split:true,collapsible:true">
		<dc:includeTree dictId="SYS_ORGAN" name="organtree" style="synch_multi" bind="select:openOrgan"/>
	</div> --%>
	<div id="content" region="center" style="border:0;">
		<table id="userGrid" title="教师查询" fitcolumns=true
			data-options="iconCls:'icon-user',singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#userGridToolbar',collapsible:true,method:'get'">
			<thead>
				<tr>
					<th data-options="field:'checkbox',checkbox:true" style="padding:5px 5px;"></th>
					<th data-options="field:'number',sortable:true">登录名</th>
					<th data-options="field:'name',sortable:true">姓名</th>
					<th data-options="field:'sex',sortable:true">性别</th>
					<th data-options="field:'organname',width:80,sortable:true">所属组织</th>
					<th data-options="field:'cnumberName',width:80,sortable:true">班级</th>
					<!-- <th data-options="field:'birthday',sortable:true">出生日期</th> -->
		
					<th data-options="field:'_oper',align:'center',formatter:formatOper">操作</th>
				</tr>
			</thead>
		</table>
	</div>
<div id="userGridToolbar">
	<div style="padding:0 20px;">

	</div>
	<div style="padding:0 20px 6px 20px;">
	姓名：<input class="easyui-textbox" name="name" id="name" data-options="
						iconWidth:16,
						icons: [{iconCls:'icon-clear',
							handler: function(e){$(e.data.target).textbox('setValue', '');}
						}]"/>
	<a href="javascript:doSearch()" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>&emsp;
			<a href="javascript:updUser(0)" class="btn btn-success"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">添加</span></a>&emsp;
		<a href="javascript:delUser()" class="btn btn-danger"><span class="icon-trash" style="font-size:16px"></span> <span style="font-size:14px">删除</span></a>
	</div>
</div>


<script>
$(function(){
	var pager = $('#userGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[]
	});
	//注册回车查询
	$('#name').textbox('textbox').keydown(function (e) {
       if (e.keyCode == 13) {
          doSearch();
       }
   });
   window.setTimeout(function() {
	   $("#userGrid").datagrid({url:'/${projectName}/sysmanage/user/schUser.action'});
   }, 1);
})

   
function doSearch(){
	$('#userGrid').datagrid('load',{
		name: $('#name').val()
	});
}
function formatOper(val,row,index){  
    return "&emsp;<a href='javascript:void(0)' onclick='updUser("+val+")' title='修改' class='_oper'>&nbsp;<i class='icon-edit'></i>&nbsp;</a>"
    		+"<a href='javascript:void(0)' onclick='resetPw("+val+")' title='重置密码' class='_oper'>&nbsp;<i class='updpass'></i>&nbsp;</a>&emsp;";  
} 

function resetPw(id){
	$.messager.confirm('敬告','确定要重置密码吗？',function(r){
		$.ajax({
			url : "/${projectName}/sysmanage/user/resetPw.action" , 
			type : "post" , 
			data : {"id":id} , 
			success : function(result) {
				alert(result.msg)
			}
		});
	});
}

function updUser(id){
	if(id == '0'){
		$("#userEditWin").dialog({title: '添加用户',resizable:true});
	}
	if(id != '0'){
		$("#userEditWin").dialog({title: '修改用户',resizable:true});
	}
	QMoveDialog($("#userEditWin"));
	$('#userViewWin').dialog('close');
	$('#userEditWin').dialog('open');
	$('#userEditWin').get(0).contentWindow.load(id);
} 
function vieUser(id){
	QMoveDialog($("#userViewWin"));
	$('#userEditWin').dialog('close');
	$('#userViewWin').dialog('open');
	$('#userViewWin').get(0).contentWindow.load(id);
}
<%-- 删除记录 --%>
function delUser(){
	var checkedItems = $('#userGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/sysmanage/user/delUser.action",{ids:ids},function(data){
				$('#userGrid').datagrid('reload');
			});
		}
	});
}
function reloadData(){
	$('#userEditWin').dialog('close');
	$('#userGrid').datagrid('reload');
}
var currOrganId;
function openOrgan(json){//{id:'',text:''}
	currOrganId=json.id;
	doSearch(true);
}
</script>
<iframe id="userEditWin" src="/${projectName}/sysmanage/user/page.action?page=user_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#userEditButton'" title="添加教师" style="width:500px;height:80%;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="userEditButton">
	<a href="javascript:void(0)" onclick="$('#userEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" id="saveBtn" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#userEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >关 闭</a>
</div>

<iframe id="userViewWin" src="/${projectName}/sysmanage/user/page.action?page=user_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#userViewButton'" title="查看教师" style="width:500px;height:80%;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="userViewButton">
	<a href="javascript:void(0)" onclick="$('#userViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >关 闭</a>
</div>
</body>
</html>
