<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>功能单元查询</title>
	<%@include file="/common/include.jsp" %>
</head>
<body>
<table id="functionunitGrid" title="功能单元" fitcolumns=true
	data-options="iconCls:'icon-qrcode',singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#functionunitGridToolbar',collapsible:true,url:'/${projectName}/sysmanage/functionunit/schFunctionunit.action',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" ></th>
			<th data-options="field:'name',sortable:true">功能单元名称</th>
			<th data-options="field:'info',width:80,sortable:true">功能单元说明</th>
			<th data-options="field:'_oper',align:'center',formatter:formatOper">操作</th>
		</tr>
	</thead>
</table>
<div id="functionunitGridToolbar">
	<div style="padding:0 20px;">
		<a href="javascript:updFunctionunit(0)" class="btn btn-success"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">添加</span></a>&emsp;
		<a href="javascript:delFunctionunit()" class="btn btn-danger"><span class="icon-trash" style="font-size:16px"></span> <span style="font-size:14px">删除</span></a>
	</div>
	<div style="padding:0 20px 6px 20px;">
		功能单元名称：<input class="easyui-textbox" name="name" id="name" style="width:160px;" data-options="
							iconWidth:16,
							icons: [{iconCls:'icon-clear',
								handler: function(e){$(e.data.target).textbox('setValue', '');}
							}
						]"/>
		<a href="javascript:doSearch()" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>
	</div>
</div>


<script>
$(function(){
	var pager = $('#functionunitGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[]
	});			
})
function doSearch(){
	$('#functionunitGrid').datagrid('load',{
		name: $('#name').val(),
		email: $('#email').val()
	});
}
function formatOper(val,row,index){  
    return "&emsp;<a href='javascript:void(0)' onclick='updFunctionunit("+val+")' class='_oper'>&nbsp;<i class='icon-edit'></i></a>&emsp;";
} 

function updFunctionunit(id){
	$('#functionunitEditWin').dialog('open');
	$('#functionunitEditWin').get(0).contentWindow.load(id);
}
function delFunctionunit(){
	var checkedItems = $('#functionunitGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/sysmanage/functionunit/delFunctionunit.action",{ids:ids},function(data){
				$('#functionunitGrid').datagrid('reload');
			});
		}
	});
}
function reloadData(){
	$('#functionunitEditWin').dialog('close');
	$('#functionunitGrid').datagrid('reload');
}
</script>
<iframe id="functionunitEditWin" src="/${projectName}/sysmanage/functionunit_upd.jsp" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#functionunitEditButton'" title="修改功能单元" closed="true" style="width:500px;height:500px;padding:5px;" scrolling="no" frameborder="0"></iframe>
<div id="functionunitEditButton">
	<a href="javascript:$('#functionunitEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:$('#functionunitEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >取 消</a>
</div>
</body>
</html>
