<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<title>角色查询</title>
	<%@include file="/common/include.jsp" %>
</head>
<body>
<table id="roleGrid" title="" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#roleGridToolbar',collapsible:true,url:'/${projectName}/sysmanage/role/schRole.action',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" ></th>
			<th data-options="field:'name',sortable:true">角色名称</th>
			<th data-options="field:'info',width:80,sortable:true">说明</th>
			<th data-options="field:'_oper',align:'center',formatter:formatOper">操作</th>
		</tr>
	</thead>
</table>
<div id="roleGridToolbar">
	
	<div style="padding:0 20px 6px 20px;">
	角色名称：<input class="easyui-textbox" name="name" id="name" style="width:160px;" data-options="
							iconWidth:16,
							icons: [{iconCls:'icon-clear',
								handler: function(e){$(e.data.target).textbox('setValue', '');}
							}
						]"/>
		<a href="javascript:doSearch()" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>&emsp;
		<a href="javascript:updRole(0)" class="btn btn-success"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">添加</span></a>&emsp;
		<a href="javascript:delRole()" class="btn btn-danger"><span class="icon-trash" style="font-size:16px"></span> <span style="font-size:14px">删除</span></a>
	</div>
</div>


<script>
$(function(){
	var pager = $('#roleGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[]
	});
})
function doSearch(){
	$('#roleGrid').datagrid('load',{
		name: $('#name').val()
	});
}
function formatOper(val,row,index){
    return "&emsp;<a href='javascript:void(0)' onclick='updRole("+val+")' class='_oper'>&nbsp;<i class='icon-edit'></i></a>&emsp;";
}

function updRole(id){
	if(id == '0'){
		$("#roleEditWin").dialog({title: '添加角色',resizable:true});
	}
	if(id != '0'){
		$("#roleEditWin").dialog({title: '修改角色',resizable:true});
	}
	$('#roleEditWin').dialog('open');
	$('#roleEditWin').get(0).contentWindow.load(id);
} 
<%-- 删除记录 --%>
function delRole(){
	var checkedItems = $('#roleGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/sysmanage/role/delRole.action",{ids:ids},function(data){
				$('#roleGrid').datagrid('reload');
			});
		}
	});
}
function reloadData(){
	$('#roleEditWin').dialog('close');
	$('#roleGrid').datagrid('reload');
}
</script>
<iframe id="roleEditWin" src="/${projectName}/sysmanage/role_upd.jsp" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#roleEditButton'" title="添加角色" closed="true" style="width:500px;height:500px;padding:5px;" scrolling="no" frameborder="0"></iframe>
<div id="roleEditButton">
	<a href="javascript:$('#roleEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:$('#roleEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >取 消</a>
</div>
</body>
</html>
