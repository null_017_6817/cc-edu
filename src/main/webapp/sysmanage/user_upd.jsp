<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>修改用户表</title>
	<%@include file="/common/include.jsp" %>
		<style>
		td > span {
			width: 155px !important;
		}
	   </style>
<script>
$(function(){
	$('#_form').form({
		onLoadSuccess:function(){
			classListDict.select($('#cnumber').val());
		}
	});
});
function load(id){
	$('#_form').form('load','/${projectName}/sysmanage/user/vieUser.action?id='+id);
};

function submitForm(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#cnumber').val(classListDict.getValue());
	$('#_form').form('submit',{
      success:function(data) {
      	parent.reloadData();
      }
   });
}
</script>
<style>
.textbox-text{height:35px;line-height:35px;}
</style>
</head>
<body>
<form id="_form" method="post" action="/${projectName}/sysmanage/user/updUser.action">
	<input type="hidden" name="from" value="submit"/>
	<input type="hidden" name='id' value=""/>
	<input type="hidden" name='state' value=""/>
	<input type="hidden" name='classify' value=""/>
	<input type="hidden" name='modulename' value=""/>
	<input type="hidden" name="cnumber" id="cnumber"/>
	<table>
		<tr><td class='label' style="width:120px">姓名：</td><td><input type='text' name='name' id='name' class='easyui-textbox' data-options='required:true' value=''/></td></tr>
		<!-- <tr><td class='label'>单位：</td><td><input type='text' name='organname' id='organname' class='easyui-textbox' readonly="readonly"/></td></tr> -->
		<tr><td class='label'>所属组织：</td><td><dc:insertList name="organid" dictId="SYS_ORGAN" style="combo_normal" /></td></tr>
		<tr><td class='label'>登录名：</td><td><input type='text' name='number' id='number' class='easyui-textbox' data-options="required:true" value=''/></td></tr>
		<!-- <tr><td class='label'>密码：</td><td><input type='text' name='password' id='password' class='easyui-textbox' data-options='' value=''/></td></tr> -->
		<!-- <tr><td class='label'>出生日期：</td><td><input type='text' name='birthdaystr' id='birthdaystr' class='easyui-datebox' value=''/></td></tr> -->
		<tr><td class='label'>
			性别：
			</td>
			<td>
				<dc:insertList name="sex" dictId="ZD_XB" style="combo_normal" />
			</td></tr>
		<tr><td class='label'>班级：</td><td>
			<dc:includeList name="classList" dictId="CC_CLASSES" style="bill_checkbox"/>
		</td></tr>
	</table>

</form>
</body>
</html>
