<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>机构查询</title>
	<%@include file="/common/include.jsp" %>
</head>
<body class="easyui-layout">
	<div title="组织机构" style="width:260px;" data-options="iconCls:'icon-sitemap',region:'west',split:true,collapsible:true">
		<dc:includeTree dictId="SYS_ORGAN" name="organtree" style="synch_single" bind="select:openOrgan"/>
	</div>
	<div id="content" region="center" style="border:0;">
		<table id="organGrid" title="机构查询" fitcolumns=true
			data-options="iconCls:'icon-search',singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#organGridToolbar',collapsible:true,url:'/${projectName}/sysmanage/organ/schOrgan.action',method:'get'">
			<thead>
				<tr>
					<th data-options="field:'checkbox',checkbox:true" ></th>
					<th data-options="field:'name',sortable:true">机构名称</th>
					<!-- <th data-options="field:'ismaster',sortable:true">是否机构</th> -->
					<th data-options="field:'info',width:80,sortable:true">说明</th>
					<th data-options="field:'code',sortable:true">机构编码</th>
					<th data-options="field:'type',sortable:true">机构类型</th>
					<th data-options="field:'_oper',align:'center',formatter:formatOper">操作</th>
				</tr>
			</thead>
		</table>
		<div id="organGridToolbar">
			
			<div style="padding:0 20px 6px 20px;">
				机构名称：<input class="easyui-textbox" name="name" id="name" style="width:200px;" data-options="
								iconWidth:16,
								icons: [{iconCls:'icon-clear',
									handler: function(e){$(e.data.target).textbox('setValue', '');}
								}]"/>
				<label for="child"><input type="checkbox" name="child" id="child" value="1" style="vertical-align: middle;width:15px;height:15px" checked="checked"/>包含下级</label>
				
				<a href="javascript:doSearch()" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>&emsp;
				<a href="javascript:updOrgan(0)" class="btn btn-success"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">添加</span></a>&emsp;
				<a href="javascript:delOrgan()" class="btn btn-danger"><span class="icon-trash" style="font-size:16px"></span> <span style="font-size:14px">删除</span></a>
			</div>
		</div>
	</div>
<script>
$(function(){
	var pager = $('#organGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[]
	});
	
	$('#name').textbox('textbox').keydown(function (e) {
        if (e.keyCode == 13) {
           doSearch();
        }
    });
    
    $('#child').click(function(){
    	doSearch();
    });
})

var currOrganId;
function openOrgan(json){//{id:'',text:''}
	currOrganId=json.id;
	doSearch();
}

function doSearch(){
	$('#organGrid').datagrid('load',{
		name: $('#name').val(),
		id: currOrganId,
		child:$('#child').get(0).checked
	});
}
function formatOper(val,row,index){  
    return "&emsp;<a href='javascript:void(0)' onclick='updOrgan("+val+")' class='_oper'>&nbsp;<i class='icon-edit'></i></a>&emsp;";
} 

function updOrgan(id){
	if(id == '0'){
		$("#organEditWin").dialog({title: '添加机构',resizable:true});
	}
	if(id != '0'){
		$("#organEditWin").dialog({title: '修改机构',resizable:true});
	}
	QMoveDialog($("#organEditWin"));
	$('#organEditWin').dialog('open');
	$('#organEditWin').get(0).contentWindow.load(id,currOrganId);
}
function delOrgan(){
	var checkedItems = $('#organGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.id);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/sysmanage/organ/delOrgan.action",{ids:ids},function(data){
				var res = JSON.parse(data);
				var status = res.status;
				if("0" == status) {
					$('#organtree').tree('reload');
					$('#organGrid').datagrid('reload');
				}
				if("1" == status) {
					$.messager.alert('敬告','不能删除父目录!');
				}
			});
		}
	});
}
function reloadData(){
	$('#organEditWin').dialog('close');
	$('#organGrid').datagrid('reload');
	$('#organtree').tree('reload');
}
</script>
<iframe id="organEditWin" src="/${projectName}/sysmanage/organ_upd.jsp" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#organEditButton'" title="添加机构" style="width:500px;height:500px;top:999px;padding:5px;" scrolling="no" frameborder="0"></iframe>
<div id="organEditButton">
	<a href="javascript:void(0)" onclick="$('#organEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#organEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-remove" >取 消</a>
</div>
</body>
</html>
