<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>修改机构</title>
	<%@include file="/common/include.jsp" %>
		<style>
		td > span {
			width: 155px !important;
		}
		.easyui-fluid{
		   width:393px !important;
		   margin-right:0;
		}
	</style>
<script>
$(function(){
	$('#_form').form({
		onLoadSuccess:function(){
			roleListDict.select($('#roles').val());
		}
	});
});
function load(id,pid){
	pidDict.tar.combotree("reload")
	if(pid>0){
		$('#_form').form('load','./organ/vieOrgan.action?id='+id+'&pid='+pid);
	}else{
		$('#_form').form('load','./organ/vieOrgan.action?id='+id);
	}
	
};
function submitForm(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#roles').val(roleListDict.getValue());
	$('#_form').form('submit',{
      success:function(data) {
      	parent.reloadData();
      }
   });
}
</script>
<style>
</style>
</head>
<body>
<form id="_form" method="post" action="./organ/updOrgan.action">
	<input type="hidden" name="from" value="submit"/>
	<input type="hidden" name='id' value=""/>
	<!-- <input type="hidden" name='pid' value=""/> -->
	<input type="hidden" name='ord' value=""/>
	<input type="hidden" name='treetrack' value=""/>
	<input type="hidden" name='modulename' value=""/>
	<input type="hidden" name='funcunits' value=""/>
	<input type="hidden" name='type' value=""/>
	<input type="hidden" name='code' value=""/>
	<input type="hidden" name="roles" id="roles"/>
	<table style="width:100%">
		<tr><td class='label' style="min-width:70px;">机构名称：</td><td><input name='name' class='easyui-textbox' data-options='required:true' value='' style="width:150px;"/></td></tr>
		<tr><td class='label' style="min-width:70px;">上级机构：</td><td><dc:insertTree name="pid" dictId="SYS_ORGAN" style="synch_single" /></td></tr>
		<!-- </td><td><input name='pname' class='easyui-textbox' value='' readonly="readonly" style="width:150px;"/></td></tr> -->
		
		<%-- <tr><td class='label' style="min-width:70px;">是否机构：</td><td><dc:insertList name="ismaster" dictId="SYS_ORGAN_NATURE" style="enu_radio"/></td></tr> --%>
		<tr><td class='label' style="min-width:70px;">说明：</td><td style="width:395px">
		<input name='info' class='easyui-textbox' value='' style="width:100%;height:40px;"/></td></tr>
		<tr><td class='label' min-width:70px;>角色：</td><td>
			<dc:includeList name="roleList" dictId="SYS_ROLE" style="bill_checkbox" cssWidth="150px"/>
		</td></tr>
	</table>
</form>
</body>
</html>
