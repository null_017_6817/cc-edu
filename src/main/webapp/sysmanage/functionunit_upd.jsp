<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>修改功能单元</title>
	<link rel="stylesheet" type="text/css" href="/LABSSM/common/base.css">
	<link rel="stylesheet" type="text/css" href="/LABSSM/common/css/font-awesome.css">
 	<link rel="stylesheet" type="text/css" href="/LABSSM/easyui1_5/themes/custom/uimaker/easyui.css" id="easyuiTheme" >
	<link rel="stylesheet" type="text/css" href="/LABSSM/common/com.css">
	
	<script type="text/javascript" src="/LABSSM/easyui1_5/jquery.min.js"></script>
	<script type="text/javascript" src="/LABSSM/easyui1_5/themes/custom/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="/LABSSM/easyui1_5/themes/custom/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="/LABSSM/dictionary/dictionary2.js"></script>
	<script type="text/javascript" src="/LABSSM/common/validate.js"></script>
	<script type="text/javascript" src="/LABSSM/common/com.js"></script>
<script>
function load(id){
	$('#_form').form('load','./functionunit/updFunctionunit.action?id='+id);
};
function submitForm(){
	$('#_form').form('submit',{
      success:function(data) {
      	parent.reloadData();
      }
   });
}
</script>
</head>
<body>
<form id="_form" method="post" action="./functionunit/updFunctionunit.action">
	<input type="hidden" name="from" value="submit"/>
	<input type="hidden" name='id' value=""/>
	<input type="hidden" name='ord' value=""/>
	<input type="hidden" name='picico' value=""/>
	<input type="hidden" name='modulename' value=""/>
	<input type="hidden" name='funcs' value=""/>
	<table>
		<tr><td class="label">功能单元名称：</td><td><input type='text' name='name' class='easyui-textbox' data-options='required:true' value=''/></td></tr>
		<tr><td class="label">功能单元说明：</td><td>
		<textarea rows="10" cols="30" name="info"></textarea>
		</td></tr>
	</table>

</form>
</body>
</html>
