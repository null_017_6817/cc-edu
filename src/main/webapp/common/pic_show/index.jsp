<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>修改头像</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script src="/${projectName}/common/pic_show/xiuxiu.js" type="text/javascript"></script>
<script type="text/javascript">

window.onload=function(){
	xiuxiu.embedSWF("altContent",5,"100%","100%");
       /*第1个参数是加载编辑器div容器，第2个参数是编辑器类型，第3个参数是div容器宽，第4个参数是div容器高*/
    xiuxiu.setUploadType(2);
	xiuxiu.setUploadURL("https://"+document.location.host+"${ pageContext.request.contextPath }/picshow/uploadByMt.action");//修改为您自己的上传接收图片程序
	xiuxiu.onBeforeUpload = function(data, id){
		xiuxiu.setUploadArgs({filetype: data.type, type: "image", filename: data.name,number : "${CURRENT_USER.number}"});
	}

	xiuxiu.onInit = function ()
	{
		xiuxiu.loadPhoto("http://open.web.meitu.com/sources/images/1.jpg");
	}	
	xiuxiu.onUploadResponse = function (data)
	{
		alert("上传响应" + data);  
		$(window.parent.frames["picShowIframe"].document).find("picShow").hide();
		
	}
}
</script>
<style type="text/css">
	html, body { height:100%; overflow:hidden; }
	body { margin:0; }
</style>
</head>
<body>
<div id="altContent">
	<h1>修改头像</h1>
</div>
</body>
</html>
