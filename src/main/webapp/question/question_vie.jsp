<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>查看练习</title>
	<%@include file="/common/include.jsp" %>
<script>
function load(id){
	$('#_form').form('load','/${projectName}/paper/question/vieQuestion.action?toL=true&id='+id);
};
</script>
</head>
<body>
<form id="_form">
	<!-- <input type="hidden" name='id' value=""/>
	<table>
		<tr>
			<td class='label'>
			试卷名称：
			</td>
			<td>
			<input type='text' name='name' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			分类目录：
			</td>
			<td>
				<input type='text' name='shortcut' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			教研室：
			</td>
			<td>
			<input type='text' name='department' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			发起教师：
			</td>
			<td>
				<input type='text' name='teacher' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			题库编号：
			</td>
			<td>
			<input type='text' name='bankId' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			题库名：
			</td>
			<td>
				<input type='text' name='bankName' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			创建时间：
			</td>
			<td>
			<input type='text' name='createTime' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			练习开始时间：
			</td>
			<td>
				<input type='text' name='startTime' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			练习结束时间：
			</td>
			<td>
			<input type='text' name='endTime' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			有效期：
			</td>
			<td>
				<input type='text' name='expireTime' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			题数：
			</td>
			<td>
			<input type='text' name='totalNum' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			总分值：
			</td>
			<td>
				<input type='text' name='totalScore' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			及格分值：
			</td>
			<td>
			<input type='text' name='passScore' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			是否显示标准答案：
			</td>
			<td>
				<input type='text' name='showStandard' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			是否可重复做题：
			</td>
			<td>
			<input type='text' name='canRepeat' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			备注：
			</td>
			<td>
				<input type='text' name='remark' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
	</table> -->
	
	<input type="hidden" name='bankId' value=""/>
	<table style="margin: 0 auto;">
		<tr><td class='label'>分校编号：</td><td><input type='text' name='vccId' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>题库名：</td><td><input type='text' name='bankName' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>级别：</td><td><input type='text' name='level' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>行业：</td><td><input type='text' name='profession' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>备注：</td><td><input type='text' name='remark' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>创建时间：</td><td><input type='text' name='createTime' class='viewbox' readonly='readonly'/></td></tr>
		<tr><td class='label'>状态：</td><td><input type='text' name='status' class='viewbox' readonly='readonly'/></td></tr>
	</table>
	
</form>
</body>
</html>
