<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>修改练习</title>
	<%@include file="/common/include.jsp" %>
	<style>
		td > span {
			width: 155px !important;
		}	
	</style>
<script>
<%-- 查询页点击修改时调用此方法加载记录详情 --%>
function load(id){
	$('#_form').form('load','/${projectName}/paper/question/vieQuestion.action?bankId='+id);
};

<%-- 点击保存按钮时调用此方法。(注:保存按钮在查询列表页) --%>
function submitForm(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#_form').form('submit',{
      success:function(data) {
      	parent.reloadData();
      }
   });
}
</script>
</head>
<body>
<form id="_form" method="post" action="/${projectName}/paper/question/updQuestion.action">
	<input type="hidden" name='bankId' id='bankId'/>
	<table>
		<!-- <tr><td class='label' style="width: 168px">分校编号：</td><td><input id="vccId" name="vccId" class="easyui-textbox"  data-options="required:true,validType:'length[1,6]'"/></td></tr> -->
		<tr><td class='label'>题库名：</td><td><input id="bankName" name="bankName" class="easyui-textbox" data-options="required:true"/></td></tr>
		<tr><td class='label'>级别：</td><td style="width:155px;"><dc:insertList name="level" dictId="ZD_BANKLEVEL" style="combo_normal"/></td></tr>
		<tr><td class='label'>行业：</td><td><input id="profession" name="profession" class="easyui-textbox" data-options="required:true"/></td></tr>
		<!-- <tr><td class='label'>创建时间：</td><td><input id="createTime" name="createTime" class="easyui-datebox" data-options="required:true"/></td></tr> -->
		<tr><td class='label'>状态：</td><td><dc:insertList name="status" dictId="ZD_BANKSTATUS" required="true" style="combo_normal"/></td></tr>
		<tr style="display:none"><td class='label'>类型：</td><td><dc:insertList name="type" dictId="ZD_BANKTYPE" style="combo_normal"/></td></tr>
		<tr><td class='label'>理论题库：</td><td><dc:insertList name="isTheory" dictId="ZD_SFBZ" style="combo_normal"/></td></tr>
		<tr><td class='label'>备注：</td><td><!-- <input id="remark" name="remark" class="easyui-textbox" data-options=""/> -->
		<textarea id="remark" name="remark" rows="3" cols="50" class="textarea easyui-validatebox"></textarea>
		</td></tr>
	</table>
	
	
</form>
</body>
</html>
