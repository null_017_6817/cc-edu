<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>习题查询</title>
	<%@include file="/common/include.jsp" %>

<script>
$(function(){
	<%-- 为分页行设置"添加"和"删除"按钮 --%>
	var pager = $('#exerTopicGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});		
	var isExport='${export}';
	if(isExport=="yes"){
		$("#export").show();
	}else{
		$("#export").hide();
	}
})

<%-- 执行查询，点击查询按钮时调用 --%>
function doSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#exerTopicGrid').datagrid('load',{
		title:$('#title').val(),
		// type:$('#type').val(),
		type: typeDict.getValue(),
		level:$('#level').val(),
		// profession:$('#profession').val(),
		profession: professionDict.getValue(),
		difficulty:$('#difficulty').val(),
		author:$('#author').val(),
		beginStepId:$('#beginStepId').val()
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatOper(val,row,index){ 
	// 
	// return "&emsp;<a href='javascript:void(0)' onclick=\"updExerTopic('"+val+"')\" class='_oper' style='display:none;' title='修改'>&nbsp;<i class='icon-edit'></i></a>&emsp;"
	// +"&emsp;<a href='javascript:void(0)' onclick=\"editExerTopic('"+val+"')\" class='_oper' title='修改'>&nbsp;<i class='icon-edit'></i></a>&emsp;"

	return "&emsp;<a href='javascript:void(0)' onclick=\"editExerTopic('"+val+"')\" class='_oper' title='修改'>&nbsp;<i class='icon-edit'></i></a>&emsp;"
 ;

	//return "<span onclick=\"updExerTopic('"+val+"')\" style='text-decoration:underline;cursor:pointer;'>修改</span> ";
				
} 

<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
function updExerTopic(id){
	$("input[name='topicIdHide']").val(id);
	$("#exerTopicEditWin").attr("src","");
	var url = "/${projectName}/editor/sortable.jsp";
	$("iframe[name='exerTopicEditWin']").attr("src",url);
	QMoveDialog($("#exerTopicEditWin"));
	$('#exerTopicViewWin').dialog('close');
	$('#exerTopicEditWin').dialog('open');
	//$('#exerTopicEditWin').get(0).contentWindow.load(id,"${paramId}");
} 

function getBankId() {
	var bankId = $("input[name='bankIdHide']").val();
	return bankId;
}

function addExerTopic(id){
	$("#exerTopicAndStepAddWin").attr("src","");
	var url = "/${projectName}/editor/topic_add.jsp";
	$("iframe[name='exerTopicAndStepAddWin']").attr("src",url);
	QMoveDialog($("#exerTopicAndStepAddWin"));
	// $('#exerTopicViewWin').dialog('close');
	$('#exerTopicAndStepAddWin').dialog('open');
	// $('#exerTopicAddWin').get(0).contentWindow.load();
}

function editExerTopic(id) {
	$("input[name='topicIdHide']").val(id);
	$("#exerTopicAndStepEditWin").attr("src", "");
	var url = "/${projectName}/editor/topic_edit.jsp";
	$("iframe[name='exerTopicAndStepEditWin']").attr("src", url);
	QMoveDialog($("#exerTopicAndStepEditWin"));
	$("#exerTopicAndStepEditWin").dialog('open');
}

<%-- 打开查看页面，点击记录行的"查看"链接时调用 --%>
function vieExerTopic(id){
	QMoveDialog($("#exerTopicViewWin"));
	$('#exerTopicEditWin').dialog('close');
	$('#exerTopicViewWin').dialog('open');
	$('#exerTopicViewWin').get(0).contentWindow.load(id);
}

<%-- 删除记录 --%>
function delExerTopic(){
	var checkedItems = $('#exerTopicGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(item.topicId);
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.get("/${projectName}/paper/question/delExerTopic.action",{ids:ids},function(data){
				$('#exerTopicGrid').datagrid('reload');
			});
		}
	});
}

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#exerTopicEditWin').dialog('close');
	$('#exerTopicGrid').datagrid('reload');
}

//上传ZIP/XML/XLS/XLSX题目
function uploadTopic() {
	var bankId =  "${bankId}";
	QMoveDialog($("#topicfile"));
	$('#topicfile').dialog('open');
	//$('#topicfile').get(0).contentWindow.load(bankId);
}

function exportTemplate() {
	window.open("/${projectName}/manager/bank/exportTemplate.action");
}

function exportTheory() {
	var bankId = $("input[name='bankIdHide']").val();
	window.open("/${projectName}/manager/bankTopic/exportTheory.action?bankId="+bankId);
}

function cleansingTopicStep() {
	$.get("/${projectName}/manager/bankTopic/cleansingTopicAndStep.action",function(res){
		var status = res.status;
		console.log(status);
		if(status == "1") {
			$.messager.alert("温馨提示", "清洗完成");
		}
	});
}

function openStatusDialog() {
	$("#status_dialog").dialog("open");
}

function closeStatusDialog() {
	$("#status_dialog").dialog("close");
}

// ajax保存数据
function ajaxSave( ){
	var topicForm = exerTopicEditWin.window.getTopicForm();
	var topicType = topicForm.find("select[name='type']").val();
	var formSer;
	var obj = new Object();
	if(topicType != "实训营地"){// 练习
		obj.stepTtitle = "";
		obj.param = "";
		obj.standardAnswer = "";
		obj.stepXmls = exerTopicEditWin.window.getDataArg();
		formSer = topicForm.find('form').serialize()+"&bankId=${paramId}";;
	}
	if(topicType == "实训营地"){// 实训
		obj = exerTopicEditWin.window.getPractical();
		formSer = topicForm.find('form').serialize()+"&bankId=${paramId}";
	}
	formSer += "&topicIdParam="+$("input[name='topicIdHide']").val();
	var jsStr = JSON.stringify(obj);
	$.ajax({
		type:"POST",
		contentType : "application/json;charset=UTF-8",
		url:'/${projectName}/paper/question/saveTopicAndStep.action?'+formSer,
		data:jsStr,
		dataType:'json',
		success:function(result){
			if(result.success){
				$('#exerTopicEditWin').window('close');
				$('#exerTopicGrid').datagrid('reload');
			}else{
				alert(result.msg);
			}
		}
	});
}

function saveTopicAndStep(type_) {
	var topicData = "";
	var stepData = "";
	if(type_ == "add") {
		topicData = exerTopicAndStepAddWin.window.getTopicFormData();
		console.log(topicData);
		stepData = exerTopicAndStepAddWin.window.getStepFormData();
		console.log(stepData);
	}
	if(type_ == "edit") {
		topicData = exerTopicAndStepEditWin.window.getTopicFormData();
		console.log(topicData);
		stepData = exerTopicAndStepEditWin.window.getStepFormData();
		console.log(stepData);
	}
	$.ajax({
		type:"POST",
		url:'/${projectName}/manager/bankTopic/saveBankTopicAndStep.action',
		data:{"topicFormData":topicData,"stepFormData":stepData},
		dataType:'json',
		async: false,
		success:function(result){
			var status = result.status;
			if(status == "1") {
				var topicId = result.topicId;
				var stepId = result.stepId;
				var totalScore = result.totalScore;
				if(type_ == "add") {
					exerTopicAndStepAddWin.window.setSaveResult(topicId, stepId, totalScore);
				}
				if(type_ == "edit") {
					exerTopicAndStepEditWin.window.setSaveResult(topicId, stepId, totalScore);
				}
				$.messager.show({title:"温馨提示",msg:"保存成功",showType:"show",style: {right:'',bottom:''},width:300,height:150,timeout:1000});
				$('#exerTopicGrid').datagrid('reload');
			}
			if(status == "0") {
				$.messager.alert("温馨提示", "数据校验错误");
			}
		}
	});
}

</script>
</head>
<body>
<input type="hidden" name="topicIdHide" value="" />
<input type="hidden" name="bankIdHide" value="${paramId}" />
<table id="exerTopicGrid" title="题目" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#exerTopicGridToolbar',collapsible:true,url:'/${projectName}/paper/question/schBankTopic.action?bankId=${paramId}',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" ></th>
			<th data-options="field:'bankId',width:25,align:'center',sortable:true">题库编号</th>
			<th data-options="field:'title',width:25,align:'center',sortable:true">题目名称</th>
			<th data-options="field:'typeName',width:25,align:'center',sortable:true">类型</th>
			<!-- <th data-options="field:'level',width:25,align:'center',sortable:true">级别</th> -->
			<th data-options="field:'professionLabel',width:25,align:'center',sortable:true">行业</th>
			<th data-options="field:'difficultyLabel',width:25,align:'center',sortable:true">难度</th>
			<th data-options="field:'keyWords',width:25,align:'center',sortable:true">关键字</th>
			<th data-options="field:'totalScore',width:25,align:'center',sortable:true">分数</th>
			<th data-options="field:'bookName',width:25,align:'center',sortable:true">章节</th>
			<!-- <th data-options="field:'talkSkill',sortable:true">话术</th> -->
			<th data-options="field:'_oper',align:'center',formatter:formatOper">操作</th>
		</tr>
	</thead>
</table>
<%-- 查询条件设定栏 --%>
<div id="exerTopicGridToolbar">
	<div class="opline">
		
	</div>
	<form id="_form" class="shline">
		题目名称：<input name='title' id="title" class='easyui-textbox' value='' data-options="iconWidth:16,
			icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
			}]"/>
		类型：
		<%-- <input name='type' id="type" class='easyui-textbox' value='' data-options="iconWidth:16,
			icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
			}]"/>
			 --%>
		<%-- <input id="type" name="type" class="easyui-combobox" data-options="valueField:'value',textField:'label',data:[{label:'理论题',value:'1'},{label:'实训题',value:'2'},{label:'技能训练题',value:'3'},{label:'实训营地题',value:'4'}]" editable="false" /> --%>
		<dc:insertList name="type" dictId="ZD_TOPICTYPE_NEW" style="combo_normal"/>
			
		<!-- 级别：<input name='level' id="level" class='easyui-textbox' value='' data-options="iconWidth:16,
			icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
			}]"/> -->
		<%-- 行业：<input name='profession' id="profession" class='easyui-textbox' value='' data-options="iconWidth:16,
			icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
			}]"/> --%>
		行业: <dc:insertTree name="profession" dictId="ZD_REPOSITORY" style="synch_single"/>
		<a href="javascript:void(0)" onclick="doSearch()" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>&emsp;
		<%-- <a href="javascript:void(0)" onclick="updExerTopic(0)" class="btn btn-success" style="display:none;"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">添加</span></a>&emsp; --%>
		<a href="javascript:void(0)" onclick="addExerTopic(0)" class="btn btn-success"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">添加</span></a>&emsp;
		<a href="javascript:void(0)" onclick="delExerTopic()" class="btn btn-danger"><span class="icon-trash" style="font-size:16px"></span> <span style="font-size:14px">删除</span></a>&emsp;
		<a href="javascript:void(0)" onclick="uploadTopic()" class="btn btn-success"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">打包上传</span></a>&emsp;
		<a href="javascript:void(0)" onclick="exportTemplate()" class="btn btn-orange"><span class="icon-download-alt" style="font-size:16px"></span> <span style="font-size:14px">模板下载</span></a>&emsp;
		<a href="javascript:void(0)" id="export" onclick="exportTheory()" class="btn btn-orange"><span class="icon-download-alt" style="font-size:16px"></span> <span style="font-size:14px">数据导出</span></a>
		<%-- <a href="javascript:void(0)" onclick="cleansingTopicStep()" class="btn btn-orange"><span class="icon-download-alt" style="font-size:16px"></span> <span style="font-size:14px">数据清洗</span></a>&emsp; --%>
		<a href="javascript:window.location.href='/${projectName}/paper/question/page.action?page=question_sch'"  class="btn btn-orange"><span class="icon-undo" style="font-size:16px"></span> <span style="font-size:14px">返回</span></a>
	
	</form>
</div>

<div id="exerTopicEditWin" class="easyui-window" title="题目" inline="true"
	data-options="modal:true,iconCls:'icon-save'" style="width:70%;height:550px;top:35px;"
	closed="true">
<iframe  name="exerTopicEditWin" src="/${projectName}/editor/sortable.jsp"  style="width:98%;height:90%;padding:5px;" scrolling="auto" frameborder="0"></iframe>
<div align="center" id="exerTopicEditButton">
	<a href="javascript:void(0)" onclick="ajaxSave();" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#exerTopicEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >取 消</a>
</div>
</div>

<div id="exerTopicAndStepAddWin" class="easyui-window" title="添加题目" inline="true"
	data-options="modal:true,iconCls:'icon-save'" style="width:70%;height:700px;top:35px;"
	closed="true">
	<iframe  name="exerTopicAndStepAddWin" src="/${projectName}/editor/topic_add.jsp"  style="width:98%;height:90%;padding:5px;" scrolling="auto" frameborder="0"></iframe>
	<div align="center" id="exerTopicAndStepAddButton">
		<a href="javascript:void(0)" onclick="saveTopicAndStep('add');" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
		<a href="javascript:void(0)" onclick="$('#exerTopicAndStepAddWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >取 消</a>
	</div>
</div>

<div id="exerTopicAndStepEditWin" class="easyui-window" title="修改题目" inline="true"
	data-options="modal:true,iconCls:'icon-save'" style="width:70%;height:700px;top:35px;"
	closed="true">
	<iframe  name="exerTopicAndStepEditWin" src="/${projectName}/editor/topic_edit.jsp"  style="width:98%;height:90%;padding:5px;" scrolling="auto" frameborder="0"></iframe>
	<div align="center" id="exerTopicAndStepEditButton">
		<a href="javascript:void(0)" onclick="saveTopicAndStep('edit');" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
		<a href="javascript:void(0)" onclick="$('#exerTopicAndStepEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >取 消</a>
	</div>
</div>



<%-- 查看面板 --%>
<iframe id="exerTopicViewWin" src="/${projectName}/paper/question/page.action?page=topic_vie&id=${paramId}" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#exerTopicViewButton'" title="查看习题" style="width:600px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="exerTopicViewButton">
	<a href="javascript:$('#exerTopicViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >关 闭</a>
</div>

<iframe id="topicfile" src="/${projectName}/manager/bank/page.action?page=bankTopic_file&bankId=${paramId}" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#studfileButton'" title="导入" style="width:500px;height:200px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="studfileButton">
	<a href="javascript:void(0)" onclick="$('#topicfile').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">上传</a>
	<a href="javascript:void(0)" onclick="$('#topicfile').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >关 闭</a>
</div>

<div id="status_dialog" class="easyui-dialog" data-options="modal:true,closable:false,title:'数据加载',closed:true" style="width:25%;height:90px;">
	<div id="loadDiv" >
		<span><img src="/${projectName}/book/img/loading.gif" />请稍候...</span>
	</div>
</div>

</body>
</html>
