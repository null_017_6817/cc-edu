<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<title>老师阅卷</title>
	<%@include file="/common/include.jsp" %>
	<style>
		span.textbox.combo {
			width: 155px !important;
		}	
	</style>
<script>
$(function(){
	<%-- 为分页行设置"添加"和"删除"按钮 --%>
	var pager = $('#exerGrid').datagrid().datagrid('getPager');	// get the pager of datagrid
	pager.pagination({
		buttons:[],
	});			
})

<%-- 执行查询，点击查询按钮时调用 --%>
function doSearch(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#exerGrid').datagrid('load',{
		bankName:$('#bankName').val(),
		level:levelDict.getValue(),
		profession:$('#profession').val(),
		status:statusDict.getValue(),
		type:typeDict.getValue()
	});
}

<%-- 查询列表datagrid的"操作"列的内容渲染，若有其他操作请修改此方法 --%>
function formatOper(val,row,index){
	return "<a href='javascript:void(0)' onclick='viwTopic("+row.bankId+")' class='_oper' title='查看题目'>&nbsp;<i class='icon-align-justify'></i></a>&emsp;"
			+"<a href='javascript:void(0)' onclick=\"updExer('"+val+"')\" class='_oper' title='修改' ><i class='icon-edit'></i></a> ";
} 

<%-- 刷新数据，当添加或修改了数据后调用。从upd页中调用。 --%>
function reloadData(){
	$('#exerEditWin').dialog('close');
	$('#exerGrid').datagrid('reload');
}

<%-- 删除记录 --%>
function delExer(){
	var checkedItems = $('#exerGrid').datagrid('getChecked');
	var ids = [];
	$.each(checkedItems, function(index, item){
		ids.push(ids.push(item.bankId));
	});
	if(ids.length==0){
		$.messager.alert('敬告','请选择删除的记录？');
		return;
	}
	$.messager.confirm('敬告','删除选定记录？',function(r){
		if(r){
			$.ajax({
				type : "POST",
				url : '/${projectName}/paper/question/delQuestion.action',
				data : {ids:ids},	
				async: false,
				dataType:'json',
				success : function(result) {
					if(result.success){
						$('#exerGrid').datagrid('reload');
					}else{
						alert(result.msg);
					}
				}
			});
		}
	});
}

<%-- 打开修改页面，点击记录行的"修改"链接时调用 --%>
function updExer(id){
	if(id == '0'){
		$("#exerEditWin").dialog({title: '添加题库',resizable:true});
	}
	if(id != '0'){
		$("#exerEditWin").dialog({title: '修改题库',resizable:true});
	}
	
	QMoveDialog($("#exerEditWin"));
	$('#exerViewWin').dialog('close');
	$('#exerEditWin').dialog('open');
	$('#exerEditWin').get(0).contentWindow.load(id);
} 

// 查看题目
function viwTopic(id){
	window.location.href="/${projectName}/paper/question/page.action?page=topic_sch&id="+id;
}


</script>
</head>
<body>
<table id="exerGrid" title="题库编辑" fitcolumns=true
	data-options="singleSelect:false,pagination:true,rownumbers:true,fit:true,striped:true,toolbar:'#exerGridToolbar',collapsible:true,url:'/${projectName}/paper/question/schQuestion.action',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'checkbox',checkbox:true" ></th>
			<th data-options="field:'bankName',width:25,align:'center',sortable:true">题库名称</th>
			<th data-options="field:'bankId',width:25,align:'center',sortable:true">题库编号</th>
			<th data-options="field:'profession',width:25,align:'center',sortable:true">行业</th>
			<!-- <th data-options="field:'createTime',width:25,align:'center',sortable:true">练习开始时间</th> -->
			<th data-options="field:'status',width:25,align:'center',sortable:true">状态</th>
			<th data-options="field:'level',width:25,align:'center',sortable:true">级别</th>
			<th data-options="field:'type',width:25,align:'center',sortable:true">类型</th>
			<th data-options="field:'topicNums',width:25,align:'center',sortable:true">题目数</th>
			<th data-options="field:'isTheory',width:25,align:'center',sortable:true">理论题库</th>
			<!-- <th data-options="field:'remark',sortable:true">备注</th> -->
			<th data-options="field:'_oper',align:'center',formatter:formatOper">操作</th>
		</tr>
	</thead>
</table>
<%-- 查询条件设定栏 --%>
<div id="exerGridToolbar">
	<div class="opline">
		
	</div>
	<form id="_form" class="shline">
		题库名称：<input name='bankName' id="bankName" class='easyui-textbox' value='' data-options="iconWidth:16,
			icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
			}]"/>
		行业：<input name='profession' id="profession" class='easyui-textbox' value='' data-options="iconWidth:16,
			icons: [{iconCls:'icon-clear',handler: function(e){$(e.data.target).textbox('setValue', '');}
			}]"/>
		状态：<dc:insertList name="status" dictId="ZD_BANKSTATUS" style="combo_normal"/>
		级别：<dc:insertList name="level" dictId="ZD_BANKLEVEL" style="combo_normal"/>
		类型：<dc:insertList name="type" dictId="ZD_BANKTYPE" style="combo_normal"/>
		<a href="javascript:void(0)" onclick="doSearch()" class="btn btn-info"><span class="icon-search" style="font-size:16px"></span> <span style="font-size:14px">查询</span></a>&emsp;
		<a href="javascript:void(0)" onclick="updExer(0)" class="btn btn-success"><span class="icon-plus" style="font-size:16px"></span> <span style="font-size:14px">添加</span></a>&emsp;
		<a href="javascript:void(0)" onclick="delExer()" class="btn btn-danger"><span class="icon-trash" style="font-size:16px"></span> <span style="font-size:14px">删除</span></a>
	</form>
</div>

<%-- 修改or添加面板 --%>
<iframe id="exerEditWin" src="/${projectName}/paper/question/page.action?page=question_upd" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#exerEditButton'" title="添加题库" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="exerEditButton">
	<a href="javascript:void(0)" onclick="$('#exerEditWin').get(0).contentWindow.submitForm()" class="easyui-linkbutton" iconCls="icon-ok">保 存</a>
	<a href="javascript:void(0)" onclick="$('#exerEditWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >取 消</a>
</div>
<%-- 查看面板 --%>
<iframe id="exerViewWin" src="/${projectName}/paper/question/page.action?page=question_vie" class="easyui-dialog" data-options="iconCls:'icon-save',buttons: '#exerViewButton'" title="查看题录" style="width:500px;height:500px;padding:5px;top:5000px" scrolling="auto" frameborder="0"></iframe>
<div id="exerViewButton">
	<a href="javascript:$('#exerViewWin').dialog('close');" class="easyui-linkbutton" iconCls="icon-cancel" >关 闭</a>
</div>

</body>
</html>
