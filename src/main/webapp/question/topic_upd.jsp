<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>修改习题</title>
	<%@include file="/common/include.jsp" %>
<script>

//初始化方法
$(function(){
	// 给步骤类型绑定change事件
	$('#questionType').combobox({
		onChange:function( newVal , oldVal ){
			chooseType( newVal , oldVal );
		}
	})
	// 单选框
	$("input[name='raBtn']").click(function(){
		$("#standardAnswer").textbox("setValue", $(this).val());
	});
	// 复选框
	$("input[name='ckBtn']").click(function(){
		var answer = "";
		$("input[name='ckBtn']:checked").each(function() { 
			if(answer == "" ){
				answer = $(this).val();
			} else {
				answer = answer +","+ $(this).val();
			}
		}); 
		$("#standardAnswer").textbox("setValue", answer);
	});
});

<%-- 查询页点击修改时调用此方法加载记录详情 --%>
function load(id,eid){
	$('#_form').form('load','/${projectName}/paper/question/vieBankTopic.action?id='+id+"&bankId="+eid);
};

<%-- 点击保存按钮时调用此方法。(注:保存按钮在查询列表页) --%>
function submitForm(){
	if(!$("#_form").form("validate")){
		return;
	}
	$('#_form').form('submit',{
		success:function(data) {
			parent.reloadData();
		}
	});
}

//步骤类型选择
function chooseType( newVal , oldVal ){
	$("tr[name='textAnswer']").show();// 显示答案
	$("#standardAnswer").textbox("setValue", "");// 清空答案
	$("input[type='radio']").removeAttr('checked');// 单选清空选择
	$("input[type='checkbox']").removeAttr('checked');// 多选清空选择则
	$('#standardAnswer').textbox('readonly');  //设置输入框为禁用
	if ( newVal != null && typeof(newVal) != "undefined" && newVal != "" ){
		$("tr[name='choice_single']").hide();// 单选答案
		$("tr[name='choice_single']").hide();// 单选答案
		$("tr[name='choice_multi']").hide();// 多选答案
		$("tr[name='judgeAnswer']").hide();// 判断答案
		
		if( newVal == 'choice_single' || newVal == 'choice_multi' ){
			$("tr[name='"+newVal+"']").show();// 单选或多选的答案显示
			$("tr[name='choice']").show();
			$("tr[name='choice']").each(function(){
				$(this).find('td').eq(1).find("input").textbox({required:true});
			});
		} else if ( newVal == 'judge' ) {
			$("tr[name='judgeAnswer']").show();// 判断答案
		} else {
			$('#standardAnswer').textbox({readonly:false});  //设置输入框为禁用
			$("tr[name='unchoose']").show();
			$("tr[name='choice']").hide();
			$("tr[name='choice']").each(function(){
				$(this).find('td').eq(1).find("input").textbox({required:false});
			});
		}
	} else {
		
	}
}

</script>
</head>
<body>
	<form id="_form" method="post" action="/${projectName}/paper/question/updExerTopic.action">
		<input type="hidden" name='id' id='id' />
		<input type="hidden" name='eid' id="eid" />
		<table id="topicTable">
			<tr>
				<td class='label'>题库编号：</td>
				<td><input id="bankId" name="bankId" class="easyui-textbox" data-options="required:true"/></td>
				<td class='label'>考题编号：</td>
				<td><input id="topicId" name="topicId" class="easyui-textbox" data-options="required:true"/></td>
				<td class='label'>备注：</td>
				<td><input id="remark" name="remark" class="easyui-textbox" data-options="required:true"/></td>
				<td class='label'>标题：</td>
				<td><input id="title" name="title" class="easyui-textbox" data-options="required:true"/></td>
			</tr>
			<tr>
				<td class='label'>类型：</td>
				<td><input id="type" name="type" class="easyui-textbox" data-options="required:true"/></td>
				<td class='label'>级别：</td>
				<td><input id="level" name="level" class="easyui-textbox" data-options="required:true"/></td>
				<td class='label'>行业：</td>
				<td><input id="profession" name="profession" class="easyui-textbox" data-options="required:true"/></td>
				<td class='label'>关键字：</td>
				<td><input id="keyWords" name="keyWords" class="easyui-textbox" data-options="required:true"/></td>
			</tr>
			<tr>
				<td class='label'>能力类型：</td>
				<td><input id="bookmodul" name="bookmodul" class="easyui-textbox" data-options="required:true"/></td>
				<td class='label'>难度：</td>
				<td><input id="difficulty" name="difficulty" class="easyui-textbox" data-options="required:true"/></td>
				<td class='label'>作者：</td>
				<td><input id="author" name="author" class="easyui-textbox" data-options="required:true"/></td>
				<td class='label'>时间：</td>
				<td><input id="creattimestr" name="creattimestr" class="easyui-datebox" data-options="required:true"/></td>
			</tr>
			<tr>
				<td class='label'>合并录音：</td>
				<td><input id="mergeVoice" name="mergeVoice" class="easyui-textbox" data-options="required:true,validType:['int']"/></td>
				<td class='label'>客户资料：</td>
				<td><input id="customer" name="customer" class="easyui-textbox" data-options="required:true"/></td>
				<td class='label'>话术：</td>
				<td><input id="talkSkill" name="talkSkill" class="easyui-textbox" data-options="required:true"/></td>
				<td class='label'>总分：</td>
				<td><input id="totalScore" name="totalScore" class="easyui-textbox" data-options="required:true,validType:['int']"/></td>
			</tr>
			<tr>
				<td class='label'>及格分：</td>
				<td><input id="passScore" name="passScore" class="easyui-textbox" data-options="required:true,validType:['int']"/></td>
				<td class='label'>开始步骤：</td>
				<td><input id="beginStepId" name="beginStepId" class="easyui-textbox" data-options="required:true"/></td>
				<td class='label'>创建时间：</td>
				<td><input id="createTimestr" name="createTimestr" class="easyui-datebox" data-options="required:true"/></td>
				<td class='label'>状态：</td>
				<td><input id="status" name="status" class="easyui-textbox" data-options="required:true,validType:['int']"/></td>
			</tr>
			<tr>
				<td class='label'>能力类型：</td>
				<td><input id="capability" name="capability" class="easyui-textbox" data-options="required:true"/></td>
				<td class='label'>文档A标题：</td>
				<td><input id="docATitle" name="docATitle" class="easyui-textbox" data-options="required:true"/></td>
				<td class='label'>文档B标题：</td>
				<td><input id="docBTitle" name="docBTitle" class="easyui-textbox" data-options="required:true"/></td>
				<td class='label'>文档C标题：</td>
				<td><input id="docCTitle" name="docCTitle" class="easyui-textbox" data-options="required:true"/></td>
			</tr>
			<tr>
				<td class='label'>文档D标题：</td>
				<td><input id="docDTitle" name="docDTitle" class="easyui-textbox" data-options="required:true"/></td>
				<td class='label'>文档E标题：</td>
				<td><input id="docETitle" name="docETitle" class="easyui-textbox" data-options="required:true"/></td>
				<td class='label'>文档A答案：</td>
				<td><input id="docAText" name="docAText" class="easyui-textbox" data-options="required:true,validType:['int']"/></td>
				<td class='label'>文档B答案：</td>
				<td><input id="docBText" name="docBText" class="easyui-textbox" data-options="required:true,validType:['int']"/></td>
			</tr>
			<tr>
				<td class='label'>文档C答案：</td>
				<td><input id="docCText" name="docCText" class="easyui-textbox" data-options="required:true,validType:['int']"/></td>
				<td class='label'>文档D答案：</td>
				<td><input id="docDText" name="docDText" class="easyui-textbox" data-options="required:true,validType:['int']"/></td>
				<td class='label'>文档E答案：</td>
				<td><input id="docEText" name="docEText" class="easyui-textbox" data-options="required:true,validType:['int']"/></td>
				<td class='label'>在练习中的序号：</td>
				<td><input id="orderNo" name="orderNo" class="easyui-textbox" data-options="required:true,validType:['int']"/></td>
			</tr>
		</table>
	</form>
</body>
</html>
