<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/dictionary.tld" prefix="dc"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>查看习题</title>
	<%@include file="/common/include.jsp" %>
<script>
function load(id){
	alert(id);
	$('#_form').form('load','/${projectName}/paper/question/vieBankTopic.action?toL=true&topicId='+id);
};
</script>
</head>
<body>
<form id="_form">
	<input type="hidden" name='id' value=""/>
	<table style="margin: 0 auto;">
		<tr>
			<td class='label'>
			练习编号：
			</td>
			<td>
			<input type='text' name='eid' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			在练习中的序号：
			</td>
			<td>
				<input type='text' name='orderNo' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			题库编号：
			</td>
			<td>
			<input type='text' name='bankId' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			考题编号：
			</td>
			<td>
				<input type='text' name='topicId' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			备注：
			</td>
			<td>
			<input type='text' name='remark' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			标题：
			</td>
			<td>
				<input type='text' name='title' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			类型：
			</td>
			<td>
			<input type='text' name='type' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			级别：
			</td>
			<td>
				<input type='text' name='level' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			行业：
			</td>
			<td>
			<input type='text' name='profession' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			关键字：
			</td>
			<td>
				<input type='text' name='keyWords' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			能力类型：
			</td>
			<td>
			<input type='text' name='bookmodul' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			难度：
			</td>
			<td>
				<input type='text' name='difficulty' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			作者：
			</td>
			<td>
			<input type='text' name='author' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			时间：
			</td>
			<td>
				<input type='text' name='creattime' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			合并录音：
			</td>
			<td>
			<input type='text' name='mergeVoice' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			客户资料：
			</td>
			<td>
				<input type='text' name='customer' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			话术：
			</td>
			<td>
			<input type='text' name='talkSkill' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			总分：
			</td>
			<td>
				<input type='text' name='totalScore' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			及格分：
			</td>
			<td>
			<input type='text' name='passScore' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			开始步骤：
			</td>
			<td>
				<input type='text' name='beginStepId' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			创建时间：
			</td>
			<td>
			<input type='text' name='createTime' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			状态：
			</td>
			<td>
				<input type='text' name='status' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			能力类型：
			</td>
			<td>
			<input type='text' name='capability' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			文档A标题：
			</td>
			<td>
				<input type='text' name='docATitle' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			文档B标题：
			</td>
			<td>
			<input type='text' name='docBTitle' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			文档C标题：
			</td>
			<td>
				<input type='text' name='docCTitle' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			文档D标题：
			</td>
			<td>
			<input type='text' name='docDTitle' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			文档E标题：
			</td>
			<td>
				<input type='text' name='docETitle' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			文档A答案：
			</td>
			<td>
			<input type='text' name='docAText' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			文档B答案：
			</td>
			<td>
				<input type='text' name='docBText' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			文档C答案：
			</td>
			<td>
			<input type='text' name='docCText' class='viewbox' readonly='readonly'/>
			</td>
			<td class='label'>
			文档D答案：
			</td>
			<td>
				<input type='text' name='docDText' class='viewbox' readonly='readonly'/>
			</td>
		</tr>
		<tr>
			<td class='label'>
			文档E答案：
			</td>
			<td>
			<input type='text' name='docEText' class='viewbox' readonly='readonly'/>
			</td>
			<td>&nbsp;</td><td>&nbsp;</td></tr>
	</table>

</form>
</body>
</html>
