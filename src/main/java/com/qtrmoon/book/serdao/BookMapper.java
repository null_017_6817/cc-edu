package com.qtrmoon.book.serdao;

import java.util.List;
import org.springframework.stereotype.Component;

import com.qtrmoon.book.pojo.Book;

/**
 * 课本mapper接口
 */
@Component
public interface BookMapper {
	
	/**
	 * Book的条件查询方法
	 * @param book 承载查询条件
	 * @return 返回Book的集合
	 */
	public List<Book> schBook(Book book);
	
	/**
	 * Book的主键查询方法
	 * @param id 主键值
	 * @return 返回Book实体
	 */
	public Book schBookById(Integer id);
	
	/**
	 * Book的父Id查询方法
	 * @param pid 父Id
	 * @return 返回Book实体
	 */
	public List<Book> schBookByPid(Integer pid);
	
	/**
	 * Book的添加方法
	 * @param book 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addBook(Book book);
	
	/**
	 * Book的修改方法
	 * @param book 承载数据的Bean
	 */
	public void updBook(Book book);
	
	/**
	 * Book的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delBook(Integer id);
	
	/**
	 * Book的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delBooks(Integer[] ids);

	/**
	 * 根据Name查询book
	 */
	public List<Book> schBookByName(Book book);
}
