package com.qtrmoon.book.serdao;

import java.util.List;
import org.springframework.stereotype.Component;

import com.qtrmoon.book.pojo.Trainindusty;

/** 实训行业表实体映射 */
@Component
public interface TrainindustyMapper {
	
	/**
	 * Trainindusty的条件查询方法
	 * @param trainindusty 承载查询条件
	 * @return 返回Trainindusty的集合
	 */
	public List<Trainindusty> schTrainindusty(Trainindusty trainindusty);
	
	/**
	 * Trainindusty的主键查询方法
	 * @param id 主键值
	 * @return 返回Trainindusty实体
	 */
	public Trainindusty schTrainindustyById(String id);
	
	/**
	 * Trainindusty的添加方法
	 * @param trainindusty 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addTrainindusty(Trainindusty trainindusty);
	
	/**
	 * Trainindusty的修改方法
	 * @param trainindusty 承载数据的Bean
	 */
	public void updTrainindusty(Trainindusty trainindusty);
	
	/**
	 * Trainindusty的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delTrainindusty(String id);
}
