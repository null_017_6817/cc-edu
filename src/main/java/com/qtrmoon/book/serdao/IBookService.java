package com.qtrmoon.book.serdao;
import java.util.List;

import com.qtrmoon.book.pojo.*;
import com.qtrmoon.camp.pojo.ZdRepository;

/**
 * 课本service层接口
 */
public interface IBookService {
	
	/***************** Book方法组 *****************/
	/**
	 * Book的条件查询方法
	 * @param book 承载查询条件的Bean
	 * @return 返回Book的集合
	 */
	public List<Book> schBook(Book book);
	
	/**
	 * 根据父Id查询book
	 * @param pid 主键值
	 * @return 返回Book实体
	 */
	public List<Book> schBookByPid(Integer pid);
	
	/**
	 * 根据Type查询book
	 * @param book 承载查询条件的Bean
	 * @return 返回Book实体
	 */
	public List<Book> schBookByType(Book book);
	
	/**
	 * Book的主键查询方法
	 * @param id 主键值
	 * @return 返回Book实体
	 */
	public Book schBookById(Integer id);
	
	/**
	 * Book的添加方法
	 * @param book 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addBook(Book book);
	
	/**
	 * Book的修改方法
	 * @param book 承载数据的Bean
	 */
	public void updBook(Book book);
	
	/**
	 * Book的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delBook(Integer id);
	
	/**
	 * Book的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delBook(Integer[] ids);

	/**
	 * 根据Name查寻Book
	 */
	public List<Book> schBookByName(Book book);

	/***************** Trainindusty方法组 *****************/
	/**
	 * Trainindusty的条件查询方法
	 * @param trainindusty 承载查询条件的Bean
	 * @return 返回Trainindusty的集合
	 */
	public List<Trainindusty> schTrainindusty(Trainindusty trainindusty);
	
	/**
	 * Trainindusty的主键查询方法
	 * @param id 主键值
	 * @return 返回Trainindusty实体
	 */
	public Trainindusty schTrainindustyById(String id);
	
	/**
	 * Trainindusty的添加方法
	 * @param trainindusty 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addTrainindusty(Trainindusty trainindusty);
	
	/**
	 * Trainindusty的修改方法
	 * @param trainindusty 承载数据的Bean
	 */
	public void updTrainindusty(Trainindusty trainindusty);
	
	/**
	 * Trainindusty的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delTrainindusty(String id);
	
	/**
	 * Trainindusty的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delTrainindusty(String[] ids);
	
	/**
	 * 查询所有
	 * @author qinbo
	 * @param trainindusty
	 * @return
	 */
	public List<Trainindusty> schAllTrainindusty(Trainindusty trainindusty);
	
	/**
	 * 查询知识库树
	 * @return
	 */
	public ZdRepository schKnowlegeTree();
}//end