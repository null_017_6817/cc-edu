package com.qtrmoon.book.serdao;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.qtrmoon.book.pojo.Book;
import com.qtrmoon.book.pojo.Trainindusty;
import com.qtrmoon.camp.pojo.ZdRepository;
import com.qtrmoon.camp.serdao.ZdRepositoryMapper;

/**
 * 课本service层实现类
 */
@Service("bookService")
public class BookServiceImpl implements IBookService {
	
	@Autowired
	private BookMapper bookMapper; // 声明模块Mapper实例
	
	@Autowired
	private ZdRepositoryMapper zdRepositoryMapper; // 声明模块Mapper实例

	/***************** Book方法组 *****************/
	/**
	 * Book的条件查询方法
	 * @param book 承载查询条件的Bean
	 * @return 返回Book的集合
	 */
	@Override
	public List<Book> schBook(Book book) {
		PageHelper.startPage(book.getPage(), book.getPagesize());//设置分页显示
		List<Book> list=bookMapper.schBook(book);// 查询列表
		Page<Book> page = (Page<Book>)list;//为了设置总记录数先类型强转
		book.setDatasize(page.getTotal());
		return list;//返回结果集
	}
	
	/**
	 * Book的条件查询方法
	 * @param book 承载查询条件的Bean
	 * @return 返回Book的集合
	 */
	public List<Book> schBookByType(Book book) {
		List<Book> list=bookMapper.schBook(book);// 查询列表
		return list;// 返回结果集
	}
	
	/***************** Book方法组 *****************/
	/**
	 * Book的条件查询方法
	 * @param book 承载查询条件的Bean
	 * @return 返回Book的集合
	 */
	@Override
	public List<Book> schBookByPid(Integer pid) {
		return bookMapper.schBookByPid(pid);// 返回结果集
	}
	
	/**
	 * Book的主键查询方法
	 * @param id 主键值
	 * @return 返回Book实体
	 */
	@Override
	public Book schBookById(Integer id) {
		return bookMapper.schBookById(id);// 返回结果集
	}
	
	/**
	 * Book的添加方法
	 * @param book 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	@Override
	public Integer addBook(Book book) {
		return bookMapper.addBook(book);// 返回结果集
	}

	/**
	 * Book的修改方法
	 * @param book 承载数据的Bean
	 */
	@Override
	public void updBook(Book book) {
		bookMapper.updBook(book);//更新数据
	}

	@Override
	public List<Book> schBookByName(Book book) {
		return bookMapper.schBookByName(book);
	}

	/**
	 * Book的单记录删除方法
	 * @param id 要删除的主键值
	 */
	@Override
	public void delBook(Integer id) {
		bookMapper.delBook(id);// 删除数据
	}

	/**
	 * Book的批量删除方法
	 * @param ids 主键值的数组
	 */
	@Override
	public void delBook(Integer[] ids) {
		bookMapper.delBooks(ids);// 批量删除数据
	}

	@Autowired
	private TrainindustyMapper trainindustyMapper;// 声明模块Mapper实例


	/***************** Trainindusty方法组 *****************/
	/**
	 * Trainindusty的条件查询方法
	 * @param trainindusty 承载查询条件的Bean
	 * @return 返回Trainindusty的集合
	 */
	@Override
	public List<Trainindusty> schTrainindusty(Trainindusty trainindusty) {
		PageHelper.startPage(trainindusty.getPage(), trainindusty.getPagesize());//设置分页显示
		List<Trainindusty> list=trainindustyMapper.schTrainindusty(trainindusty);
		Page<Trainindusty> page = (Page<Trainindusty>)list;//为了设置总记录数先类型强转
		trainindusty.setDatasize(page.getTotal());
		return list;
	}
	
	/**
	 * Trainindusty的主键查询方法
	 * @param id 主键值
	 * @return 返回Trainindusty实体
	 */
	@Override
	public Trainindusty schTrainindustyById(String id) {
		return trainindustyMapper.schTrainindustyById(id);// 返回查询的数据
	}
	
	/**
	 * Trainindusty的添加方法
	 * @param trainindusty 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	@Override
	public Integer addTrainindusty(Trainindusty trainindusty) {
		return trainindustyMapper.addTrainindusty(trainindusty);// 返回查询的数据
	}

	/**
	 * Trainindusty的修改方法
	 * @param trainindusty 承载数据的Bean
	 */
	@Override
	public void updTrainindusty(Trainindusty trainindusty) {
		trainindustyMapper.updTrainindusty(trainindusty);// 更新数据
	}

	/**
	 * Trainindusty的单记录删除方法
	 * @param id 要删除的主键值
	 */
	@Override
	public void delTrainindusty(String id) {
		trainindustyMapper.delTrainindusty(id);// 删除数据
	}

	/**
	 * Trainindusty的批量删除方法
	 * @param ids 主键值的数组
	 */
	@Override
	public void delTrainindusty(String[] ids) {
		for (String id : ids) {// 循环
			trainindustyMapper.delTrainindusty(id);// 批量删除数据
		}
	}

	/**
	 * 查询行业无分页
	 */
	@Override
	public List<Trainindusty> schAllTrainindusty(Trainindusty trainindusty){
		return trainindustyMapper.schTrainindusty(trainindusty);// 返回查询的数据
	}

	/**
	 * 查询知识库树
	 * @param 
	 */
	@Override
	public ZdRepository schKnowlegeTree() {
		ZdRepository zdRepository = zdRepositoryMapper.schRootRepository();
		List<ZdRepository> childList = new ArrayList<ZdRepository>();// 实例化list
		buildKnowledgeTree(zdRepository, childList);// 构建知识树
		return zdRepository;// 返回处理结果集
	}
	
	/**
	 * 构建知识树
	 * @param zdRepository
	 * @param childList
	 */
	private void buildKnowledgeTree(ZdRepository zdRepository, List<ZdRepository> childList) {
		childList = zdRepositoryMapper.schRepositoryByPId(zdRepository.getId());// 根据富基店查询数据
		for (ZdRepository zdRepositoryres : childList) {//循环
			List<ZdRepository> childList2 = new ArrayList<ZdRepository>();// 查询字典
			buildKnowledgeTree(zdRepositoryres, childList2);//设置属性
		}
		zdRepository.setChildList(childList);// 设置ChildList
	}

}//end