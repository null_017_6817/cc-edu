package com.qtrmoon.book.ctrl;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.qtrmoon.book.pojo.Trainindusty;
import com.qtrmoon.book.serdao.IBookService;
import com.qtrmoon.camp.pojo.Hytopic;
import com.qtrmoon.camp.serdao.ICampService;
import com.qtrmoon.common.BaseController;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.examination.pojo.ExerTopic;
import com.qtrmoon.examination.serdao.IExaminationService;

/**
 * 实训行业表控制器
 */
@Controller
@RequestMapping("/book/trainindusty")
public class TrainindustyController extends BaseController {
	@Autowired
	private IBookService bookService; // 声明模块Service实例

	@Autowired
	private IExaminationService examinationService;// 声明模块Service实例
	
	@Autowired
	private ICampService campService; // 声明模块Service实例

	/**
	 * 查询方法，自带分页设置。
	 * @param trainindusty
	 * 承载查询条件的Bean
	 * @param request
	 * 可用来获取非Bean内的参数
	 * @param response
	 * 返回请求,输出Json
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schTrainindusty.action")
	public void schTrainindusty(Trainindusty trainindusty,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		noCache(response);
		trainindusty.setCondition(""); // 防sql注入
		trainindusty.assLike("id", "pid", "name", "type");

		List<Trainindusty> list = bookService.schTrainindusty(trainindusty); // 调用Service查询数据
		/* 将数据Bean的集合转换为Json集合 */
		JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
		JSONObject obj; // 声明用于构造Bean的Json对象
		for (Trainindusty u : list) { // 循环查询的数据集构造Json数据集
			obj = u.getJsonInDict(); // 将Bean转换为Json对象
			obj.put("_oper", u.getId()); // 为"操作列"设定主键值
			datas.add(obj); // 添加Json对象到Json集合
		}
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		JSONObject res = new JSONObject(); // 声明页面返回Json
		res.put("total", trainindusty.getDatasize()); // 设置数据总记录数
		res.put("rows", datas); // 设置数据集
		response.getWriter().print(res.toJSONString()); // 输出到页面
	}

	/**
	 * 预添加or预修改or查看方法
	 * @param trainindusty
	 * 预添加时可传入一些初始值；预修改和查看时用来承载主键值
	 * @param toL
	 * 挂字典的字段是否需要转为字典Label值。true则进行转换
	 * @param response
	 * 返回请求,输出Json
	 * @throws IOException
	 */
	@RequestMapping(value = "/vieTrainindusty.action")
	public void vieTrainindusty(Trainindusty trainindusty, Boolean toL,
			HttpServletResponse response) throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (toL != null && toL) { // 查看页面加载数据时调用，返回的对象已进行字典转换
			if (trainindusty.getId() != null && trainindusty.getId() != "") { // 检测主键非空
				trainindusty = bookService.schTrainindustyById(trainindusty
						.getId()); // 按主键查询数据
				response.getWriter().print(trainindusty.getJsonInDict()); // 输出经过字典转换Json对象到查看页(vie_)
			}
		} else { // 修改页面加载数据时调用
			if (trainindusty.getId() != null && trainindusty.getId() != "") { // 检查主键非空
				trainindusty = bookService.schTrainindustyById(trainindusty
						.getId()); // 按主键查询数据
				response.getWriter().print(trainindusty.getJson()); // 输出Json对象到修改页(upd_)
			} else { // 添加页面加载数据时调用，可设置添加页面中一些属性的预设值。
				response.getWriter().print(new Trainindusty().getJson()); // 输出Json对象到添加页(upd_)
			}
		}
	}

	/**
	 * 添加或修改的提交方法
	 * @param trainindusty
	 * 数据表单Bean
	 * @param response
	 * 返回请求
	 * @throws IOException
	 */
	@RequestMapping(value = "/updTrainindusty.action")
	public void updTrainindusty(Trainindusty trainindusty,
			HttpServletResponse response) throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (trainindusty.getId() == null || trainindusty.getId() == "") { // 检测主键为空则
			trainindusty.setId(UUID.randomUUID().toString().replaceAll("-", ""));// 替换-为空窜
			trainindusty.setCreateTime(new Date());
			bookService.addTrainindusty(trainindusty); // 添加数据
		} else { // 主键非空则
			bookService.updTrainindusty(trainindusty); // 修改数据
		}
		DictBuffer.updCache("ZD_TRAININDUSTY");
	}

	/**
	 * 删除方法
	 * @param ids
	 * 待删除数据的主键数组
	 * @param response
	 * 返回请求
	 */
	@RequestMapping(value = "/delTrainindusty.action")
	public void delTrainindusty(@RequestParam(value = "ids[]") String[] ids,
			HttpServletResponse response) {
		noCache(response); // Ajax方法设定无缓存
		bookService.delTrainindusty(ids); // 删除数据
		DictBuffer.updCache("ZD_TRAININDUSTY");
	}

	/**
	 * JSP页面跳转
	 * @param page
	 * jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value = "/page.action")
	public String goPage(String page) { // 放过本Ctrl的页面，不合法的跳转到error页。
		if ("index".equals(page) || "trainindusty_sch".equals(page)
				|| "trainindusty_vie".equals(page)// 跳转页面
				|| "trainindusty_upd".equals(page)) {// 跳转页面
			return "/book/" + page;// 跳转页面
		} else {
			return "/error";// 跳转error页面
		}
	}

	/**
	 * 查询所有
	 * @param trainindusty
	 * 承载查询条件的Bean
	 * @param request
	 * 可用来获取非Bean内的参数
	 * @param response
	 * 返回请求,输出Json
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schAllTrainindusty.action")
	public void schAllTrainindusty(Trainindusty trainindusty,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		noCache(response);// 设置ajax无缓存
		trainindusty.setCondition("order by orderid");//设置动态sql排序
		List<Trainindusty> list = bookService.schAllTrainindusty(trainindusty); // 调用Service查询数据
		Hytopic hytopic = new Hytopic();
		hytopic.setRid(list.get(0).getId());
		List<Hytopic> topics = campService.schHytopic(hytopic);// 根据id查询数据
		Integer eid = topics.get(0).getId();// 从查询结中获取id
		/* 将数据Bean的集合转换为Json集合 */
		JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
		JSONObject obj; // 声明用于构造Bean的Json对象
		for (Trainindusty u : list) { // 循环查询的数据集构造Json数据集
			obj = u.getJson(); // 将Bean转换为Json对象
			obj.put("_oper", u.getId()); // 为"操作列"设定主键值
			datas.add(obj); // 添加Json对象到Json集合
		}
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		JSONObject res = new JSONObject(); // 声明页面返回Json
		res.put("total", trainindusty.getDatasize()); // 设置数据总记录数
		res.put("rows", datas); // 设置数据集
		res.put("eid", eid);// 设置试卷id
		response.getWriter().print(res.toJSONString()); // 输出到页面
	}
}
