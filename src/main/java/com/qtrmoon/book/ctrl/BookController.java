package com.qtrmoon.book.ctrl;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.mysql.jdbc.StringUtils;
import com.qtrmoon.book.constant.BookType;
import com.qtrmoon.book.pojo.Book;
import com.qtrmoon.book.serdao.IBookService;
import com.qtrmoon.book.util.FilePath;
import com.qtrmoon.camp.constant.DxwjQuestionType;
import com.qtrmoon.camp.pojo.ZdRepository;
import com.qtrmoon.common.BaseController;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.dictionary.bsd.DictBean;
import com.qtrmoon.examination.pojo.ExerAnswer;
import com.qtrmoon.examination.pojo.ExerTopic;
import com.qtrmoon.examination.serdao.IExaminationService;
import com.qtrmoon.manager.pojo.Person;
import com.qtrmoon.manager.pojo.Stud;
import com.qtrmoon.manager.serdao.IManagerService;
import com.qtrmoon.study.pojo.StudyStatus;
import com.qtrmoon.study.serdao.IStudyService;
import com.qtrmoon.sysmanage.SysConstant;
import com.qtrmoon.sysmanage.serdao.ISysmanageService;
import com.qtrmoon.util.UuidUtil;

/**
 * 课本控制器
 */
@Controller
@RequestMapping("/book/book")
public class BookController extends BaseController {
	
	@Autowired
	private IBookService bookService; // 声明模块Service实例
	@Autowired
	private ISysmanageService sysmanageService; // 声明模块Service实例
	@Autowired
	private IManagerService managerService; // 声明模块Service实例
	@Autowired
	private IExaminationService examinationService; // exeam方法组声明模块Service实例
	@Autowired
	private IStudyService studyService;		
	/**
	 * 查询方法，自带分页设置。
	 * @param book
	 * 承载查询条件的Bean
	 * @param request
	 * 可用来获取非Bean内的参数
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schBook.action")
	public void schBook(Book book, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		noCache(response); // response清除缓存
		book.setCondition(""); // 防sql注入
		book.assLike("name", "bookconcern", "editor", "path"); // 批量字段拼接%
		List<Book> list = null; // 初始化book集合
		if (book.getId() != null) {// 判断必要参数是否为空
			boolean child = Boolean.parseBoolean(request.getParameter("child")); // 判断是否含有child参数
			if (child) {// 判断必要参数是否为空
				String treetrack;
				try {
					treetrack =  bookService.schBookById(book.getId()).getTreetrack();
					book.setCondition("AND TREETRACK like '" + treetrack
							+ "-%' or TREETRACK ='" + treetrack + "'");// 拼接sql参数
				} catch (Exception e) {
					book.setPid(book.getId());//设置此父
				}
			} else {
				book.setPid(book.getId()); // 设置父节点
			}
			list = bookService.schBook(book); // 查询book列表
			/* 将数据Bean的集合转换为Json集合 */
			JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
			JSONObject obj; // 声明用于构造Bean的Json对象
			for (Book u : list) { // 循环查询的数据集构造Json数据集
				obj = u.getJsonInDict(); // 将Bean转换为Json对象
				obj.put("_oper", u.getId()); // 为"操作列"设定主键值
				datas.add(obj); // 添加Json对象到Json集合
			}
			/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
			JSONObject res = new JSONObject(); // 声明页面返回Json
			res.put("total", book.getDatasize()); // 设置数据总记录数
			res.put("rows", datas); // 设置数据集
			response.getWriter().print(res.toJSONString()); // 输出到页面
		}
	}

	/**
	 * 查询课本列表
	 * @param book
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schBookForMain.action")
	public void schBookForMain(Book book, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		noCache(response); // 设置response无缓存
		book.setCondition(""); // 防止sql注入
		book.assLike("name", "bookconcern", "editor", "path"); // 批量字段拼接%，like查询用
		book.setType(BookType.COURSE.getCode()); // 设置图书类型为课本
		book.setCondition("AND state = '1' order by bookOrder"); // 设置排序，根据bookOrder排序
		List<Book> list = bookService.schBookByType(book); // 根据类型查询book列表
		JSONArray datas = new JSONArray(); // 初始化JSONArray对象
		JSONObject obj; // 初始化JSONObject对象
		for (Book u : list) { // 循环
			obj = u.getJsonInDict(); // Java对象转换为json对象
			obj.put("_oper", u.getId()); // 添加_oper属性
			datas.add(obj); // json对象放置到json集合中
			//
		}
		JSONObject res = new JSONObject(); // 初始化返回json对象
		res.put("total", book.getDatasize()); // 添加总数属性
		res.put("rows", datas); // 添加数据属性
		response.getWriter().print(res.toJSONString()); // 返回数据
	}

	/**
	 * 查询方法，自带分页设置。
	 * @param book
	 * 承载查询条件的Bean
	 * @param request
	 * 可用来获取非Bean内的参数
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schBookTree.action")
	public void schBookTree(Book book, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		noCache(response); // 设置response无缓存
		book.assLike("name", "bookconcern", "editor", "path"); // 防sql注入
		List<Book> unitList = bookService.schBookByPid(book.getId()); // 调用Service查询数据
		/* 将数据Bean的集合转换为Json集合 */
		JSONArray udata = new JSONArray(); // 构造业务数据的JSON集合
		JSONObject uobj = null; // 声明用于构造Bean的Json对象
		JSONObject tobj; // 初始化json对象
		for (Book u : unitList) { // 循环
			uobj = u.getJsonInDict(); // java对象转json对象
			List<Book> taskList = bookService.schBookByPid(u.getId()); // 将Bean转换为Json对象
			JSONArray tdata = new JSONArray(); // 构造业务数据的JSON集合
			for (Book t : taskList) { // 循环
				tobj = t.getJsonInDict(); // 将Bean转换为Json对象
				tdata.add(tobj); // 将json对象添加到json集合中
			}
			uobj.put("tdata", tdata); // json对象设置数据属性
			udata.add(uobj); // 将json对象添加到json集合中
		}
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		JSONObject res = new JSONObject(); // 声明页面返回Json
		res.put("rows", udata); // 设置数据集
		response.getWriter().print(res.toJSONString()); // 输出到页面
	}

	/**
	 * 根据父节点查询章节
	 * @param book
	 * @param request
	 * @param response
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value = "/schBookByPid.action")
	public Map<String, Object> schBookByPid(Book book,
			HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> result = new HashMap<String, Object>(); // 初始化返回结果集
		try {
			List<Book> bookList = new ArrayList<Book>(); // 初始化book集合
			if (book != null && book.getId() != null// 判断必要参数是否为空
					&& !"".equals(book.getId())) { // 判断必要参数是否为空
				bookList = bookService.schBookByPid(book.getId()); // 根据父节点查询所有子节点
			}
			result.put("success", true); // 设置成功标识
			result.put("data", bookList); // 设置返回数据
			result.put("msg", "查询成功"); // 设置提示消息
		} catch (Exception e) {
			e.printStackTrace(); // 打印异常
			result.put("success", false); // 设置成功标识
			result.put("msg", "查询异常"); // 设置提示消息
		} finally {
			return result; // 返回结果集
		}
	}

	/**
	 * 根据id查询章节 
	 * @param book
	 * @param request
	 * @param response
	 * @return
	 */
	@ResponseBody
	@SuppressWarnings("finally")
	@RequestMapping(value = "/schBookById.action")
	public Map<String, Object> schBookById(Book book,
			HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> result = new HashMap<String, Object>(); // 初始化返回结合
		try {
			Book bookResult = new Book(); // 初始化课本对象
			if (book != null && book.getId() != null // 判断必要参数是否为空
					&& !"".equals(book.getId())) { // 判断必要参数是否为空
				bookResult = bookService.schBookById(book.getId()); // 查询数据
			}
			result.put("success", true); // 设置返回状态
			result.put("data", bookResult); // 设置返回数据
			result.put("msg", "查询成功"); // 设置返回消息
		} catch (Exception e) {
			e.printStackTrace(); // 打印异常信息
			result.put("success", false); // 设置返回状态
			result.put("msg", "查询异常"); // 设置返回消息
		} finally {
			return result; // 返回结果
		}
	}

	/**
	 * 保存章节目标 
	 * @param book
	 * @param request
	 * @param response
	 * @return
	 */
	@ResponseBody
	@SuppressWarnings("finally")
	@RequestMapping(value = "/saveGoal.action")
	public Map<String, Object> saveGoal(Book book, HttpServletRequest request,
			HttpServletResponse response) {
		Map<String, Object> result = new HashMap<String, Object>(); // 初始化返回对象
		try {
			Book bookResult = bookService.schBookById(book.getId()); // 查询数据
			bookResult.setGoal(book.getGoal()); // 设置目标
			bookService.updBook(bookResult); // 更新数据
			result.put("success", true); // 设置成功标识
			result.put("data", bookResult); // 设置返回数据
			result.put("msg", "操作成功"); // 设置返回消息
		} catch (Exception e) {
			e.printStackTrace(); // 打印异常信息
			result.put("success", false); // 设置成功标识
			result.put("msg", "服务器异常"); // 设置提示消息
		} finally {
			return result; // 返回结果
		}
	}

	/**
	 * 预添加or预修改or查看方法
	 * @param book
	 * 预添加时可传入一些初始值；预修改和查看时用来承载主键值
	 * @param toL
	 * 挂字典的字段是否需要转为字典Label值。true则进行转换
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/vieBook.action")
	public void vieBook(Book book, Boolean toL, HttpServletResponse response)
			throws IOException {
		noCache(response);// Ajax方法设定无缓存
		if (toL != null && toL) { // 查看页面加载数据时调用，返回的对象已进行字典转换
			if (book.getId() != null && book.getId() != 0) { // 检测主键非空
				book = bookService.schBookById(book.getId()); // 按主键查询数据
				response.getWriter().print(book.getJsonInDict()); // 输出经过字典转换Json对象到查看页(vie_)
			}
		} else { // 修改页面加载数据时调用
			if (book.getId() != null && book.getId() != 0) { // 检查主键非空
				book = bookService.schBookById(book.getId()); // 按主键查询数据
				response.getWriter().print(book.getJson()); // 输出Json对象到修改页(upd_)
			} else { // 添加页面加载数据时调用，可设置添加页面中一些属性的预设值。
				response.getWriter().print(new Book().getJson()); // 输出Json对象到添加页(upd_)
			}
		}
	}
	
	
	/**
	 * 预添加or预修改or查看方法
	 * @param book
	 * 预添加时可传入一些初始值；预修改和查看时用来承载主键值
	 * @param toL
	 * 挂字典的字段是否需要转为字典Label值。true则进行转换
	 * @param response
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/schBookByBookID.action")
	public Map<String, Object> schBookByBookID(Integer bookId, Boolean toL, HttpServletResponse response)
			throws IOException {
		Map<String, Object> result = new HashMap<String, Object>(); // 初始化返回对象
		noCache(response);// Ajax方法设定无缓存
		if (toL != null && toL) { // 查看页面加载数据时调用，返回的对象已进行字典转换
			if (bookId != null && bookId != 0) { // 检测主键非空
				Book book = bookService.schBookById(bookId); // 按主键查询数据
				result.put("success", true); // 设置成功标识
				result.put("data", book); // 设置返回数据
				result.put("msg", "查询成功"); // 设置返回消息
				return result;
			}
		} else { // 修改页面加载数据时调用
			if (bookId != null && bookId != 0) { // 检查主键非空
				Book book = bookService.schBookById(bookId); // 按主键查询数据
				result.put("success", true); // 设置成功标识
				result.put("data", book); // 设置返回数据
				result.put("msg", "查询成功"); // 设置返回消息
				return result;
				//response.getWriter().print(book.getJson()); // 输出Json对象到修改页(upd_)
			} else { // 添加页面加载数据时调用，可设置添加页面中一些属性的预设值。
				JSONObject book = new Book().getJson();
				result.put("success", true); // 设置成功标识
				result.put("data", book); // 设置返回数据
				result.put("msg", "查询失败"); // 设置返回消息
				return result;
			}
		}
		return result;
	}

	/**
	 * 添加或修改的提交方法
	 * @param book
	 * 数据表单Bean
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/updBook.action", produces = "text/html;charset=utf-8")
	public void updBook(Book book, String from,
			@RequestParam(value="dataFile",required = false) CommonsMultipartFile dataFile,HttpServletResponse response)
			throws IOException {
		noCache(response); // Ajax方法设定无缓存
		
		// 文件完整路径
		String filePath = "";
		// 文件名
		String fileName = UuidUtil.get32UUID();
		// 文件后缀名 extName
		String extName = "";
		String fullName = "";// 定义常量
		
		if (dataFile != null// 定义常量
				&& dataFile.getOriginalFilename().lastIndexOf(".") >= 0) {// 定义常量
			extName = dataFile.getOriginalFilename().substring(
					dataFile.getOriginalFilename().lastIndexOf("."));// 截取
		}
		try {
			if (null != dataFile && !dataFile.isEmpty() && !"".equals(extName)) {// 判断
				filePath = FilePath.getFilePath("courseware")+File.separator+"data"+File.separator+"img";// 拼接
				fullName = fileName + extName;// 拼接
				File targetFile = new File(filePath, fullName);// 文件对象
				if (!targetFile.exists()) {// 判断
					targetFile.mkdirs();// 创建文件夹
				}
				dataFile.transferTo(targetFile);//上传文件
				book.setPath("data/img/" + fullName);
			}
		} catch (Exception e) {
			e.printStackTrace();// 异常信息
		}
		
		if ("submit".equals(from)) { // 判断是否为submit
			if (book.getId() == null) { // 判断id是否为空
				bookService.addBook(book); // 添加数据到数据库
				if (book.getPid() != null) { // 判断pid是否为null 
					Book pBook = bookService.schBookById(book.getPid()); // 根据父id查询数据
					Book entity = bookService.schBookById(book.getId()); // 根据id查询数据
					book.setTreetrack(pBook.getTreetrack() + "-" + book.getId()); // 设置treetrack属性
					book.setGoal(entity.getGoal()); // 设置目标
					if(StringUtils.isNullOrEmpty(book.getOpentraining())){//为是否显示实训营地设置默认值，默认值1 显示
						book.setOpentraining("1");
					}
					bookService.updBook(book); // 更新数据
				}
			} else {
				Book entity = bookService.schBookById(book.getId()); // 根据id查询数据
				book.setGoal(entity.getGoal()); // 设置目标
				if(StringUtils.isNullOrEmpty(book.getOpentraining())){//为是否显示实训营地设置默认值，默认值1 显示
					book.setOpentraining("1");
				}
				bookService.updBook(book); // 更新数据
			}
			DictBuffer.updCache("CC_BOOK"); // 更新缓存数据
			DictBuffer.updCache("ZD_REPOSITORY");
			response.getWriter().print(book.getJson()); // 返回json数据
		} else {
			if (book.getId() != null && book.getId() != 0) { // 判断必要参数是否为空或者0
				book = bookService.schBookById(book.getId()); // 根据id查询数据
				if (book.getPid() != null) { // 判断id是否为空
					DictBean pBook = DictBuffer.getDictById("CC_BOOK",
							book.getPid() + ""); // 从缓存中根据id获取数据
					if (pBook != null) // 判断bean是否为null
						book.setPname(pBook.getLabel()); // 设置父节点名称
				} else {
					book.setPname("无"); // 父节点名称为"无"
				}
				response.getWriter().print(book.getJson()); // 返回json数据
			} else {
				Book newBook = new Book(); // 初始化book对象
				newBook.setPid(book.getPid()); // 设置父节点id
				newBook.setPname(DictBuffer.getDictById("CC_BOOK",
						book.getPid() + "").getLabel()); // 从缓存中根据id获取数据
				response.getWriter().print(newBook.getJson()); // 返回json数据
			}
		}
	}

	/**
	 * 删除方法
	 * @param ids
	 * 待删除数据的主键数组
	 * @param response
	 * @throws IOException 
	 */
	@RequestMapping(value = "/delBook.action")
	public void delBook(@RequestParam(value = "ids[]") Integer[] ids,
			HttpServletResponse response) throws IOException {
		noCache(response); // Ajax方法设定无缓存
		JSONObject res = new JSONObject();
		boolean canDelete = true;
		for (int id : ids) {
			List<Book> childs = bookService.schBookByPid(id);
			if (childs != null && childs.size() > 0) {
				canDelete = false;
				break;
			}
		}
		if (!canDelete) {
			res.put("code", "0");
			res.put("msg", "包含子级章节目录，不允许删除！");
		} else {
			bookService.delBook(ids); // 删除数据
			res.put("code", "1");
			res.put("msg", "删除成功");
		}
		DictBuffer.updCache("CC_BOOK"); // 更新缓存数据
		DictBuffer.updCache("ZD_REPOSITORY");
		response.getWriter().print(res.toJSONString());
	}

	/**
	 * JSP页面跳转
	 * @param page
	 * jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value = "/page.action")
	public String goPage(String page, String bookId, String bookRoot,
			String type, HttpServletRequest request, Model model) {
		// 放过本Ctrl的页面，不合法的跳转到error页。
		if ("index".equals(page) || "book_sch".equals(page) // 判断并跳转页面
				|| "book_vie".equals(page) || "entrance".equals(page) // 判断并跳转页面
				|| "book_upd".equals(page) || "line".equals(page) // 判断并跳转页面
				|| "resource".equals(page) || "knowledge".equals(page) // 判断并跳转页面
				|| "test".equals(page) || "practice".equals(page) // 判断并跳转页面
				|| "main".equals(page) || "catalog".equals(page) // 判断并跳转页面
				|| "target".equals(page) || "messageTrain".equals(page)
				|| "mailTrain".equals(page) || "examList".equals(page)
				|| "goalEdit".equals(page) || "knowledge_only".equals(page)) { // 判断并跳转页面
			model.addAttribute("type", type); // 放置type参数
			Person person = (Person) request.getSession(true).getAttribute(
					SysConstant.CURRENT_USER); // 获取当前登录用户
			if (person != null) { // 当前登陆者不为空的场合
				model.addAttribute("person", person); // 放置当前用户
				model.addAttribute("state", person.getState()); // 设置用户状态
				model.addAttribute("agentId", person.getAgentId()); // 设置 坐席id
			}
			if ("entrance".equals(page)) { // entrance页面的场合
				List<Map<String, Object>> list = new ArrayList<Map<String, Object>>(); // 设置结果集
				for (DxwjQuestionType s : DxwjQuestionType.values()) { // 循环
					Map<String, Object> map = new HashMap<String, Object>(); // 初始化结果集
					map.put("name", s.getName()); // 设置name属性
					map.put("code", s.getCode()); // 设置code属性
					list.add(map); // list添加map
				}
				model.addAttribute("selectOp", list); // 设置selectOp属性
			}
			toPageParam(bookRoot, bookId, model, page,person.getNumber()); // 调用toPageParam方法
			return "/book/" + page; // 返回指定页面
		} else {
			return "/error"; // 返回错误页面
		}
	}

	/**
	 * 设置传递到页面的参数
	 * @param bookRoot
	 * @param bookId
	 * @param model
	 * @param page
	 * @param number 
	 */
	public void toPageParam(String bookRoot, String bookId, Model model,
			String page, String number) {
		if (bookRoot != null) { // 如果bookRoot不为null
			Book root = bookService.schBookById(Integer.valueOf(bookRoot)); // 根据id查询数据
			model.addAttribute("bookRoot", root); // 设置bookRoot
			if ("catalog".equals(page)) { // 是catalog页面的场合
				List<Book> childList = new ArrayList<Book>(); // 初始化book集合
				getTreeData(root, childList,number); // 获取章节树数据
				model.addAttribute("tree", root.getChilds()); // 设置章节树数据到前台页面
			}
		}
		if (bookId != null) { // bookId 不为
			model.addAttribute("bookId", bookId); // 设置bookId 
			Book book = bookService.schBookById(Integer.valueOf(bookId)); // 根据id查询book
			model.addAttribute("book", book); // 设置book属性
		}
	}

	/**
	 * 获取树数据
	 * @param parent
	 * @param childList
	 */
	private void getTreeData(Book parent, List<Book> childList,String number) {
		childList = bookService.schBookByPid(parent.getId()); // 根据父id查询所有子数据
		for (Book book : childList) { // 循环
			List<Book> childList1 = new ArrayList<Book>(); // 初始化book列表
			// 这里拿到 当前的 id  判断完成习题
			Boolean yesOrNo = judgingExerciseCompletion(book,number);
			book.setCatalogStatus(yesOrNo);//  目录 状态
			getTreeData(book, childList1,number); // 递归获取树数据 
		}
		parent.setChilds(childList);// 拼接树形数据
	}
	/**
	 * 当前登陆人的  是否完成了 当前点击的目录下的习题
	 * @param  book id
	 * @return  true or fales
	 */
	private boolean judgingExerciseCompletion(Book book,String number) {
		boolean flag = false; 
		/*HashSet<ExerTopic> schExerTopic = examinationService.getExerTopicBybookidNumSet(book);// 查询习题数量
		if(schExerTopic!=null&&schExerTopic.size()>0) {
			List<Boolean> compareTrue = new ArrayList<Boolean>();// 构建 比较的 list 集合 true
			List<Boolean> compareFalse= new ArrayList<Boolean>();// 构建 比较的 list 集合 false
			for (ExerTopic exerTopic2 : schExerTopic) { // 这里忽略步骤 去 答题卡里面找 数据 
				ExerAnswer exerAnswer = new ExerAnswer();// 构建答题卡 对象
				exerAnswer.setStudNumber(number);
				exerAnswer.setEtid(exerTopic2.getId());
				List<ExerAnswer> ans = examinationService.schExerAnswerListDistinct(exerAnswer);// 查询 答题卡 忽略 步骤?
				if(ans != null && ans.size() > 0){
					compareTrue.add(true);
				}else {
					compareFalse.add(false);
				}
			}
			// 这里 进行比较如果 compare ！=schExerTopic.size()
			if(compareTrue.size()==schExerTopic.size()) {
				flag = true;
			}else {
				flag = false;
			}
		}else {
			//如果习题数量 是 
		}*/
		StudyStatus stusta = new StudyStatus();
		stusta.setNumber(number);
		stusta.setBookId(book.getId());
		List<StudyStatus> stuss = studyService.schStudyStatus(stusta);
		if(stuss!=null&&stuss.size()>0) {
			flag=true;
		}else {
			flag =false;
		}
		return flag;
	}

	/**
	 * 查询知识库树
	 * @param request
	 * @param response
	 * @param modelMap
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/schKnowledgeTree.action")
	public ZdRepository schKnowledgeTree(HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap) {
		ZdRepository zdRepository = bookService.schKnowlegeTree();//查询数据
		modelMap.put("zdRepository", zdRepository);//放置数据到model
		return zdRepository;// 返回数据
	}

	/**
	 * 获取当前用户 
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getCurrentUser.action")
	public Person getCurrentUser(HttpServletRequest request) {
		Person currentPerson = (Person) request.getSession(true).getAttribute(
				SysConstant.CURRENT_USER); // 获取当前用户
		Person person = new Stud(); // 初始化学生实体
		if (currentPerson != null) { // 如果当前用户不为空
			person.setName(currentPerson.getName()); // 设置用户name
			person.setState(currentPerson.getState()); // 设置用户状态
			person.setAgentId(currentPerson.getAgentId()); // 设置坐席号
		}
		return person; // 返回结果
	}
	
	/**
	 * html文件夹 
	 * @param filename
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/appendHtmlUrl.action")
	public void appendHtmlUrl(String fileName,HttpServletResponse response) {
		String path = FilePath.getFilePath("courseware")+fileName;
		try {
			File file = new File(path);// 文件对象
			if (file.exists()) {// 判断
				FileInputStream in = new FileInputStream(path);// 文件对象输入流
				ServletOutputStream out = response.getOutputStream();// 输出流
				IOUtils.copy(in, out);// 以流的形式展示到前台
				IOUtils.closeQuietly(in);// 关闭流
				IOUtils.closeQuietly(out);// 关闭流
			} else {
				logger.info("文件不存在！不产生数据流输出到前台页面。缺失文件及路径为：" + path);// log
			}
		} catch (Exception e) {
			e.printStackTrace();//异常信息
		}
	}

	private void loadBookData2Excel(Book book, final HSSFSheet sheet) {
		String treeTrack = book.getTreetrack();
		String[] treeDepth = treeTrack.split("-");
		int depth = treeDepth.length;
		int rowNum = sheet.getLastRowNum();
		HSSFRow row = sheet.createRow(rowNum + 1);
		int nameCellIndex = (depth - 1) * 1;
		// int idCellIndex = nameCellIndex + 1;
		HSSFCell nameCell = row.createCell(nameCellIndex);
		// HSSFCell idCell = row.createCell(idCellIndex);
		nameCell.setCellValue(book.getName()+"^"+book.getId());
		// idCell.setCellValue(book.getId());
		List<Book> childs = book.getChilds();
		if(childs == null || childs.size() <= 0)  {
			return;
		}
		for(int i = 0; i < childs.size(); i++) {
			loadBookData2Excel(childs.get(i), sheet);
		}
	}

	/**
	 * 下载Book树
	 * @param file
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/download.action")
	public void downloadBookTree(HttpServletRequest request,
			HttpServletResponse response) {
		// String filename = "";
		Book root = bookService.schBookById(1); // 根据id查询数据
		List<Book> bookList = new ArrayList<Book>();
		getTreeData(root, bookList,null);
		String path = request.getSession().getServletContext().getRealPath("/data/doc"); // 获取服务器路径
		// String filePrefix = filename.substring(0, filename.lastIndexOf("."));
		// String downloadName = filePrefix + ".xls";
		String filePath = path + File.separator + "BookTreeData.xls";// 拼接名称
		try {
			File file = new File(filePath);// 文件对象
			if(file.exists()) {
				Workbook wb = WorkbookFactory.create(file);
				Sheet sheet = wb.getSheetAt(0);
				int rowNum = sheet.getLastRowNum();
				Book schBook = new Book();
				schBook.setPagesize(Integer.MAX_VALUE/10);
				List<Book> allBook = bookService.schBook(schBook);
				if(allBook != null) {
					if(allBook.size() != rowNum) {
						file.delete();
						HSSFWorkbook hssfWorkbook = new HSSFWorkbook();
						HSSFSheet hssfSheet = hssfWorkbook.createSheet("课本目录");
						loadBookData2Excel(root, hssfSheet);
						file.createNewFile();
						FileOutputStream stream = FileUtils.openOutputStream(file);
						hssfWorkbook.write(stream);
						stream.close();
					}
				}
			}else {
				HSSFWorkbook hssfWorkbook = new HSSFWorkbook();
				HSSFSheet hssfSheet = hssfWorkbook.createSheet("课本目录");
				loadBookData2Excel(root, hssfSheet);
				file.createNewFile();
				FileOutputStream stream = FileUtils.openOutputStream(file);
				hssfWorkbook.write(stream);
				stream.close();
			}
			BufferedInputStream fis = new BufferedInputStream(
					new FileInputStream(filePath));// 输入流
			byte[] buffer = new byte[fis.available()];// 定义字节
			fis.read(buffer);// 读
			fis.close();// 关闭
			// 清空response
			response.reset();
			// 设置response的Header
			response.addHeader("Content-Disposition", "attachment;filename="
					+ "BookTreeData.xls");// 设置response的Header
			response.addHeader("Content-Length", "" + file.length());// 设置response的Header
			OutputStream toClient = new BufferedOutputStream(
					response.getOutputStream());// 输出流
			response.setContentType("application/vnd.ms-excel;charset=gb2312");// 设置属性
			toClient.write(buffer);// 写
			toClient.flush();// 刷新
			toClient.close();// 关闭
		} catch (Exception e) {
			e.printStackTrace();// 打印异常
		}
	}
	
	/**
	 * 获取Pdf
	 * @param filename
	 * @param request
	 * @param response
	 */
	@RequestMapping(value="/getPdfStream/{filename}.action")
	public void getPdfStream(@PathVariable("filename")String filename,HttpServletRequest request,HttpServletResponse response){
		String webRoot = request.getSession().getServletContext().getRealPath("");
		String webPath= File.separator+"data"+File.separator+"courseCache"+File.separator+"data"+File.separator+"doc"+File.separator;
		filename= webRoot.concat(webPath).concat(filename);
		if(filename != null){
			try{
				File file = new File(filename);
				if(file.exists()){
					IOUtils.copy(new FileInputStream(filename), response.getOutputStream());
				}else{
					System.out.println("文件不存在！不产生数据流输出到前台页面。缺失文件及路径为："+filename);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 获取Pdf(相关知识)
	 * @param filename
	 * @param request
	 * @param response
	 */
	@RequestMapping(value="/getRepositoryPdfStream/{filename}.action")
	public void getRepositoryPdfStream(@PathVariable("filename")String filename,HttpServletRequest request,HttpServletResponse response){
		String webRoot = request.getSession().getServletContext().getRealPath("");
		String webPath= File.separator+"data"+File.separator+"repositor"+File.separator;
		filename= webRoot.concat(webPath).concat(filename);
		if(filename != null){
			try{
				File file = new File(filename);
				if(file.exists()){
					IOUtils.copy(new FileInputStream(filename), response.getOutputStream());
				}else{
					System.out.println("文件不存在！不产生数据流输出到前台页面。缺失文件及路径为："+filename);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
}

