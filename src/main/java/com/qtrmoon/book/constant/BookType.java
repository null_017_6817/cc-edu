package com.qtrmoon.book.constant;

/**
 * 课程类型
 */
public enum BookType {
	 
	COURSE("课程", 0), CATALOG("目录", 1);// 课程或章节
	
	private String name ;//名称
	private int code ;//码值
	// 构造函数
	private BookType( String name , int index ){
		this.name = name ;
		this.code = index ;
	}
	// 获取名称
	public String getName() {
		return name;
	}
	// 设置名称
	public void setName(String name) {
		this.name = name;
	}
	// 获取码值
	public int getCode() {
		return code;
	}
	// 设置码值
	public void setCode(int code) {
		this.code = code;
	}

}
