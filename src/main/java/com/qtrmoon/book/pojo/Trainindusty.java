package com.qtrmoon.book.pojo;
import java.util.Date;

import org.json.simple.JSONObject;

import com.qtrmoon.common.PageForm;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.toolkit.DateTransfer;

/** 实训行业表实体类 */
public class Trainindusty extends PageForm{
	
	private String id;// id
	private String pid;// 预留字段
	private String name;// 实训行业名称
	private String type;// 实训行业类型(预留字段）
	private String icon;// 实训行业类型(预留字段）
	private Date createTime;// 创建时间
	private String createTimeBeg,createTimeEnd;//时间条件的跨度查询属性
	private String orderid;//排序字段
	/** default constructor */
	public Trainindusty() {
	
	}	
	/** 获取 */
	public String getId() {
		return this.id;
	}
	/** 设置 */
	public void setId(String id) {
		this.id = id;
	}
	/** 获取预留字段 */
	public String getPid() {
		return this.pid;
	}
	/** 设置预留字段 */
	public void setPid(String pid) {
		this.pid = pid;
	}
	/** 获取实训行业名称 */
	public String getName() {
		return this.name;
	}
	/** 设置实训行业名称 */
	public void setName(String name) {
		this.name = name;
	}
	/** 获取实训行业类型(预留字段） */
	public String getType() {
		return this.type;
	}
	/** 设置实训行业类型(预留字段） */
	public void setType(String type) {
		this.type = type;
	}
	/** 获取创建时间 */
	public Date getCreateTime() {
		return this.createTime;
	}
	/** 设置创建时间 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/** 设定[创建时间]时间转载字段值，页面提交时由springmvc自动装载 */
	public void setCreateTimestr(String tm) {
		this.createTime=_getTime(tm);
	}
	/** 获取[创建时间]时间起始条件 */
	public String getCreateTimeBeg() {
		return createTimeBeg;
	}
	/** 获取[创建时间]时间起始条件(Dao/Mapper查询时调用) */
	public Date getCreateTimeBegDate() {
		return _getBegDate(createTimeBeg);
	}
	/** 设置[创建时间]时间起始条件(表单提交时自动装载) */
	public void setCreateTimeBeg(String createTimeBeg) {
		this.createTimeBeg = createTimeBeg;
	}
	
	/** 获取[创建时间]时间结束条件 */
	public String getCreateTimeEnd() {
		return createTimeEnd;
	}
	/** 获取[创建时间]时间结束条件(Dao/Mapper查询时调用) */
	public Date getCreateTimeEndDate() {
		return _getEndDate(createTimeEnd);
	}
	/** 设置[创建时间]时间结束条件(表单提交时自动装载) */
	public void setCreateTimeEnd(String createTimeEnd) {
		this.createTimeEnd = createTimeEnd;
	}
	/** 获取orderID*/
	public String getOrderid(){
		return orderid;
	}
	/** 设置orderID*/
	public void setOrderid(String orderid){
		this.orderid = orderid;
	}
	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("pid", pid);
		obj.put("name", name);
		obj.put("type", type);
		obj.put("icon", icon);
		//时间字段处理。createTime查询列表页使用，createTimestr修改页使用
		obj.put("createTime", DateTransfer.toString(createTime,"yyyy/MM/dd"));
		obj.put("createTimestr", DateTransfer.toString(createTime,"yyyy/MM/dd"));
		obj.put("orderid", orderid);
		return obj;
	}
	
	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("pid", pid);
		obj.put("name", name);
		obj.put("type", DictBuffer.getLabel("ZD_INDUSTRY_TYPE", type.toString()));
		//时间字段处理。createTime查询列表页使用，createTimestr修改页使用
		obj.put("createTime", DateTransfer.toString(createTime,"yyyy/MM/dd"));
		obj.put("createTimestr", DateTransfer.toString(createTime,"yyyy/MM/dd"));
		obj.put("orderid", orderid);
		return obj;
	}
}
