package com.qtrmoon.book.pojo;

import java.util.Date;
import java.util.List;

import org.json.simple.JSONObject;

import com.qtrmoon.common.PageForm;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.toolkit.DateTransfer;

/**
 * 课本Object类
 */
public class Book extends PageForm {

	private Integer id; // id
	private Integer pid; // 父ID
	private String name; // 书名或章节名
	private String pname; // 父节点名称
	private String bookconcern; // 出版社
	private String editor; // 主编
	private Date publictime; // 出版时间
	private String publictimeBeg, publictimeEnd;// 时间条件的跨度查询属性
	private String path; // 课本路径
	private Integer type; // 0:课本 1:目录章节
	private String profession; // 专业
	private String cnumber; // 班级号
	private String term; // 学期
	private String treetrack; // 树形id关系
	private String synopsis; // 简介（banner图下显示）
	private String remarks; // 描述（banner图上显示）
	private String state; // 状态（0：禁止学习，1：允许学习）
	private String goal; // 学习目标
	private List<Book> childs; // 子节点结合
	private String opentraining; //是否开放实训营地（1：开放，0：不开放  默值为null:开放）
	private Integer bookOrder; // 排序字段 
	private Boolean catalogStatus;// 目录状态 不入库

	// Constructors
	/** default constructor */
	public Book() {

	}

	// 获取子节点集合
	public List<Book> getChilds() {
		return childs;
	}

	// 设置子节点集合
	public void setChilds(List<Book> childs) {
		this.childs = childs;
	}

	// 获取大纲
	public String getSynopsis() {
		return synopsis;
	}

	// 设置大纲
	public void setSynopsis(String synopsis) {
		this.synopsis = synopsis;
	}

	// 获取备注
	public String getRemarks() {
		return remarks;
	}

	// 设置备注
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	// 获取状态
	public String getState() {
		return state;
	}

	// 设置状态
	public void setState(String state) {
		this.state = state;
	}

	// 获取目标
	public String getGoal() {
		return goal;
	}

	// 设置目标
	public void setGoal(String goal) {
		this.goal = goal;
	}

	// getter and setter
	/** 获取 */
	public Integer getId() {
		return this.id;
	}

	/** 设置 */
	public void setId(Integer id) {
		this.id = id;
	}

	/** 获取 */
	public Integer getPid() {
		return this.pid;
	}

	/** 设置 */
	public void setPid(Integer pid) {
		this.pid = pid;
	}

	/** 获取书名或章节名 */
	public String getName() {
		return this.name;
	}

	/** 设置书名或章节名 */
	public void setName(String name) {
		this.name = name;
	}

	/** 获取出版社 */
	public String getBookconcern() {
		return this.bookconcern;
	}

	/** 设置出版社 */
	public void setBookconcern(String bookconcern) {
		this.bookconcern = bookconcern;
	}

	/** 获取主编 */
	public String getEditor() {
		return this.editor;
	}

	/** 设置主编 */
	public void setEditor(String editor) {
		this.editor = editor;
	}

	/** 获取出版时间 */
	public Date getPublictime() {
		return this.publictime;
	}

	/** 设置出版时间 */
	public void setPublictime(Date publictime) {
		this.publictime = publictime;
	}

	/** 设定[出版时间]时间转载字段值，页面提交时由springmvc自动装载 */
	public void setPublictimestr(String tm) {
		this.publictime = _getTime(tm);
	}

	/** 获取[出版时间]时间起始条件 */
	public String getPublictimeBeg() {
		return publictimeBeg;
	}

	/** 获取[出版时间]时间起始条件(Dao/Mapper查询时调用) */
	public Date getPublictimeBegDate() {
		return _getBegDate(publictimeBeg);
	}

	/** 设置[出版时间]时间起始条件(表单提交时自动装载) */
	public void setPublictimeBeg(String publictimeBeg) {
		this.publictimeBeg = publictimeBeg;
	}

	/** 获取[出版时间]时间结束条件 */
	public String getPublictimeEnd() {
		return publictimeEnd;
	}

	/** 获取[出版时间]时间结束条件(Dao/Mapper查询时调用) */
	public Date getPublictimeEndDate() {
		return _getEndDate(publictimeEnd);
	}

	/** 设置[出版时间]时间结束条件(表单提交时自动装载) */
	public void setPublictimeEnd(String publictimeEnd) {
		this.publictimeEnd = publictimeEnd;
	}

	/** 获取课本路径 */
	public String getPath() {
		return this.path;
	}

	/** 设置课本路径 */
	public void setPath(String path) {
		this.path = path;
	}

	/** 获取0:课本 1:目录章节 */
	public Integer getType() {
		return this.type;
	}

	/** 设置0:课本 1:目录章节 */
	public void setType(Integer type) {
		this.type = type;
	}

	/** 获取专业 */
	public String getProfession() {
		return this.profession;
	}

	/** 设置专业 */
	public void setProfession(String profession) {
		this.profession = profession;
	}

	/** 获取班级号 */
	public String getCnumber() {
		return this.cnumber;
	}

	/** 设置班级号 */
	public void setCnumber(String cnumber) {
		this.cnumber = cnumber;
	}

	/** 获取学期 */
	public String getTerm() {
		return this.term;
	}

	/** 设置学期 */
	public void setTerm(String term) {
		this.term = term;
	}

	/** 获取treetrack */
	public String getTreetrack() {
		return treetrack;
	}

	/** 设置treetrack */
	public void setTreetrack(String treetrack) {
		this.treetrack = treetrack;
	}

	/** 设置父节点名称 */
	public String getPname() {
		return pname;
	}

	/** 设获取父节点名称 */
	public void setPname(String pname) {
		this.pname = pname;
	}
	/** 设置是否开放实训营地*/
	public String getOpentraining() {
		return opentraining;
	}
	/** 设获取是否开放实训营地*/
	public void setOpentraining(String opentraining) {
		this.opentraining = opentraining;
	}

	public Integer getBookOrder() {
		return bookOrder;
	}

	public void setBookOrder(Integer bookOrder) {
		this.bookOrder = bookOrder;
	}
	
	public Boolean getCatalogStatus() {
		return catalogStatus;
	}

	public void setCatalogStatus(Boolean catalogStatus) {
		this.catalogStatus = catalogStatus;
	}

	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj = new JSONObject();
		obj.put("id", id);
		obj.put("pid", pid);
		obj.put("name", name);
		obj.put("pname", pname);
		obj.put("bookconcern", bookconcern);
		obj.put("editor", editor);
		// 时间字段处理。publictime查询列表页使用，publictimestr修改页使用
		obj.put("publictime",
				DateTransfer.toString(publictime, "yyyy-MM-dd HH:mm:ss"));
		obj.put("publictimestr",
				DateTransfer.toString(publictime, "yyyy-MM-dd HH:mm:ss"));
		obj.put("path", path);
		obj.put("type", type);
		obj.put("profession", profession);
		obj.put("cnumber", cnumber);
		obj.put("term", term);
		obj.put("treetrack", treetrack);
		obj.put("synopsis", synopsis);
		obj.put("remarks", remarks);
		obj.put("state", state);
		obj.put("goal", goal);
		obj.put("opentraining", opentraining);
		obj.put("bookOrder", bookOrder);
		return obj;
	}

	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj = new JSONObject();
		obj.put("id", id);
		obj.put("pid", pid);
		obj.put("name", name);
		obj.put("pname", pname);
		obj.put("bookconcern", bookconcern);
		obj.put("editor", editor);
		// 时间字段处理。publictime查询列表页使用，publictimestr修改页使用
		obj.put("publictime",
				DateTransfer.toString(publictime, "yyyy-MM-dd HH:mm:ss"));
		obj.put("publictimestr",
				DateTransfer.toString(publictime, "yyyy-MM-dd HH:mm:ss"));
		obj.put("path", path);
		if (type != null && !type.equals("")) {// label
			obj.put("type", DictBuffer.getLabel("ZD_BOOKTYPE", type.toString()));
		}
		obj.put("profession", profession);
		obj.put("cnumber", cnumber);
		obj.put("term", term);
		obj.put("treetrack", treetrack);
		obj.put("synopsis", synopsis);
		obj.put("remarks", remarks);
		if (state != null && !state.equals("")) {// label
			obj.put("state",
					DictBuffer.getLabel("ZD_BOOKSTATE", state.toString()));
		}
		obj.put("goal", goal);
		if (opentraining != null && !opentraining.equals("")) {// opentraining
			obj.put("opentraining",
					DictBuffer.getLabel("ZD_SFBZ", opentraining.toString()));
		}
		obj.put("bookOrder", bookOrder);
		return obj;
	}
}