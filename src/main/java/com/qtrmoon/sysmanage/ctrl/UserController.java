package com.qtrmoon.sysmanage.ctrl;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qtrmoon.common.BaseController;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.sysmanage.constant.RolesConstant;
import com.qtrmoon.sysmanage.constant.StateConstant;
import com.qtrmoon.sysmanage.pojo.User;
import com.qtrmoon.sysmanage.serdao.ISysmanageService;
import com.qtrmoon.toolkit.MD5;
/**
 * 用户类
 *
 */
@Controller
@RequestMapping("/sysmanage/user")
public class UserController extends BaseController{
	@Autowired
	private ISysmanageService sysmanageService;//注入service
	/**
	 * 查询用户
	 * @param user
	 * @param organids
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/schUser.action")
	public void schUser(User user,String organids,HttpServletResponse response) throws IOException{
		noCache(response);//清空缓存
		user.setCondition("");//防sql注入
		user.assLike("name");
		if(organids!=null&&organids.indexOf(",")>0){
			String sql="";//sql字符串
			String[] orgs=organids.split(",");//机构数组
			for(String org:orgs){
				sql+="organid='"+org+"' or ";//设置机构查询条件
			}
			sql=sql.substring(0,sql.length()-3);//舍弃or
			user.setOrganid(null);
			user.setCondition(" and("+sql+")");//设置查询条件
		}else if(organids!=null&&!organids.equals("")){
			user.setOrganid(Integer.parseInt(organids));//设置机构id
		}
		user.setState(StateConstant.TEACHER);
		user.setRoles(RolesConstant.TEACHER);
		List<User> list=null;
		list=sysmanageService.schUser(user);//查询用户
		JSONArray datas=new JSONArray();//json数组
		JSONObject obj;//json对象
		for(User u:list){
			obj=u.getJsonInDict();//获取用户json对象
			obj.put("_oper",u.getId());//主键
			datas.add(obj);//填充json数组
		}
		JSONObject res=new JSONObject();
		res.put("total",user.getDatasize());//设置数据总数
		res.put("rows", datas);//设置数据
		response.getWriter().print(res.toJSONString());//输出数据到view层
	}
	
	/**
	 * 预添加or预修改or查看方法
	 * @param user 预添加时可传入一些初始值；预修改和查看时用来承载主键值
	 * @param toL 挂字典的字段是否需要转为字典Label值。true则进行转换
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/vieUser.action")
	public void vieUser(User user,Boolean toL,HttpServletResponse response) throws IOException{
		noCache(response);
		if(toL!=null&&toL){//查看，转字典
			if(user.getId()!=null&&user.getId()!=0){
				user=sysmanageService.schUserById(user.getId());//查询用户
				response.getWriter().print(user.getJsonInDict());//输出数据到view层
			}
		}else{
			if(user.getId()!=null&&user.getId()!=0){//预修改
				user=sysmanageService.schUserById(user.getId());//查询用户
				JSONObject result = user.getJson();//获取json对象
				result.put("password","");//不展示密码
				response.getWriter().print(result);//输出数据到view层
			}else{//预添加
				User newUser=new User();//创建用户
				newUser.setOrganid(user.getOrganid());//设置机构id
				response.getWriter().print(newUser.getJson());//输出数据到view层
			}
		}
	}
	/**
	 * 更新用户
	 * @param user
	 * @param from
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value="/updUser.action")
	public void updUser(User user,String from,HttpServletResponse response) throws IOException{
		noCache(response);
		user.setState("2");//设置状态
		user.setRoles("9");//设置角色
		if(user.getId()==null){
			if(user.getPasswd() != null && !"".equals(user.getPasswd())){
				user.setPasswd(MD5.getInstance().getMD5to32(user.getPasswd()));//设置密码
			}else{
				user.setPasswd(MD5.getInstance().getMD5to32(User.PASSWORD));//重设密码
			}
			user.setNumber(user.getNumber());//设置学号
			sysmanageService.addUser(user);//添加用户
		}else{
			User u = sysmanageService.schUserById(user.getId());//查询用户
			user.setPasswd(u.getPasswd());//设置面
			sysmanageService.updUser(user);//更新用户
		}
		DictBuffer.updCache("ZD_USER");// 更新缓存
		DictBuffer.updCache("ZD_STUD");// 更新缓存
		DictBuffer.updCache("ZD_TEACHER");// 更新缓存
		
	}
	
	/**
	 * 重置老师密码
	 * @param user
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value="/resetPw.action")
	public Map<String,Object> resetPw(User user,HttpServletResponse response) throws IOException{
		noCache(response);//清除缓存
		Map<String,Object> result = new HashMap<String,Object>();//创建map
		try{
			if(user != null && user.getId() != null){
				User u = sysmanageService.schUserById(user.getId());//查询用户
				u.setPasswd(MD5.getInstance().getMD5to32(User.PASSWORD));//设置密码
				sysmanageService.updUser(u);//更新用户
				result.put("success", true);//输出数据到view层
				result.put("msg", "重置成功！");//输出数据到view层
			}else{
				result.put("success", false);//输出数据到view层
				result.put("msg", "重置失败,必要参数为空");//输出数据到view层
			}
		}catch(Exception e){
			e.printStackTrace();//打印异常
			result.put("success", false);//输出数据到view层
			result.put("msg", "重置失败,服务器异常");//输出数据到view层
		}finally{
			return result;//返回结果
		}
	}
	/**
	 * 删除用户
	 * @param ids
	 * @param response
	 */
	@RequestMapping(value="/delUser.action")
	public void delUser(@RequestParam(value = "ids[]") Integer[] ids,HttpServletResponse response){
		noCache(response);
		sysmanageService.delUser(ids);//批量删除用户
		DictBuffer.updCache("ZD_USER");// 更新缓存
		DictBuffer.updCache("ZD_STUD");// 更新缓存
		DictBuffer.updCache("ZD_TEACHER");// 更新缓存
	}
	
	/**
	 * JSP页面跳转
	 * @param page jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value="/page.action")
	public String goPage(String page){
		if("index".equals(page)||"user_sch".equals(page)||"user_upd".equals(page)||"user_vie".equals(page)){
			return "/sysmanage/"+page;//返回相应页面
		}else{
			return "/error";//输出数据到view层
		}
	}
}
