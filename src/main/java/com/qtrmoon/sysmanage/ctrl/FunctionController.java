package com.qtrmoon.sysmanage.ctrl;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.qtrmoon.common.BaseController;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.sysmanage.pojo.Function;
import com.qtrmoon.sysmanage.serdao.ISysmanageService;
/**
 * 功能控制类
 *
 */
@Controller
@RequestMapping("/sysmanage/function")
public class FunctionController extends BaseController{
	@Autowired
	private ISysmanageService sysmanageService;//声明servie实例

	/**
	 * 查询方法，自带分页设置。
	 * @param function 承载查询条件的Bean
	 * @param request 可用来获取非Bean内的参数
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/schFunction.action")
	public void schFunction(Function function,HttpServletRequest request,HttpServletResponse response) throws IOException{
		noCache(response);//清除缓存
		function.setCondition("");//防sql注入
		List<Function> list=null;
		list=sysmanageService.schFunction(function);//获取功能
		JSONArray datas=new JSONArray();//json数组对象
		JSONObject obj;
		for(Function u:list){
			obj=u.getJson();//获取json对象
			obj.put("_oper",u.getId());//填充行id
			datas.add(obj);//添加对象到集合对象中
		}
		JSONObject res=new JSONObject();//初始化对象
		res.put("total",function.getDatasize());//获取功能总数
		res.put("rows", datas);//集合填充到对象中
		response.getWriter().print(res.toJSONString());//向view层输出json字符串
	}
	
	/**
	 * 预添加or预修改or查看方法
	 * @param function 预添加时可传入一些初始值；预修改和查看时用来承载主键值
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value="/vieFunction.action")
	public void vieFunction(Function function,HttpServletResponse response) throws IOException{
		noCache(response);//清除缓存
		if(function.getId()!=null&&function.getId()!=0){//预修改
			function=sysmanageService.schFunctionById(function.getId());//获取功能
			response.getWriter().print(function.getJson());//向view层输出json字符串
		}else{//预添加
			Function fun=new Function();//创建功能
			fun.setPid(function.getPid());//设置功能父功能
			response.getWriter().print(fun.getJson());//向view层输出json字符串
		}
	}
	
	/**
	 * 添加或修改的提交方法
	 * @param function 数据表单Bean
	 * @param file
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value="/updFunction.action")
	public void updFunction(Function function,@RequestParam(value = "file", required = false) MultipartFile file,HttpServletResponse response) throws IOException{
		noCache(response);//清除缓存
		if(function.getId()==null){
			sysmanageService.addFunction(function);//添加功能
		}else{
			sysmanageService.updFunction(function);//修改功能
		}
		DictBuffer.updCache("SYS_FUNCS_ALL");//更新所有功能缓存
		DictBuffer.updCache("SYS_FUNCS_MENU");//更新菜单缓存
	}
	
	/**
	 * 删除方法
	 * @param ids 待删除数据的主键数组
	 * @param response
	 */
	@RequestMapping(value="/delFunction.action")
	public void delFunction(@RequestParam(value = "ids[]") Integer[] ids,HttpServletResponse response){
		noCache(response);//清除缓存
		sysmanageService.delFunction(ids);//根据id删除功能
		DictBuffer.updCache("SYS_FUNCS_ALL");//更新所有功能缓存
		DictBuffer.updCache("SYS_FUNCS_MENU");//更新菜单缓存
	}
	/**
	 * 调换功能
	 * @param source
	 * @param target
	 * @param point
	 * @param response
	 */
	@RequestMapping(value="/dropFunction.action")
	public void dropFunction(String source,//源ID
											String target,//目标ID
											String point,//位置['append'、'top' 或 'bottom']
											HttpServletResponse response){
		noCache(response);//清除缓存
		if("append".equals(point)){
			Function sf=sysmanageService.schFunctionById(Integer.parseInt(source));//查看功能
			sf.setPid(Integer.parseInt(target));//设置父id
			sysmanageService.updFunction(sf);//更新功能
		}else{
			Function sf=sysmanageService.schFunctionById(Integer.parseInt(source));//获取源功能
			Function tf=sysmanageService.schFunctionById(Integer.parseInt(target));//获取目标功能
			sf.setPid(tf.getPid());//设置目标为源的父
			if("top".equals(point)){
				sf.setOrd(tf.getOrd()-1);//设置排序减1
			}else if("bottom".equals(point)){
				sf.setOrd(tf.getOrd()+1);//设置排序加1
			}	
			sysmanageService.updFunction(sf);//更新功能
		}
		DictBuffer.updCache("SYS_FUNCS_ALL");//更新所有功能缓存
		DictBuffer.updCache("SYS_FUNCS_MENU");//更新菜单缓存
	}
	
	/**
	 * JSP页面跳转
	 * @param page jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value="/page.action")
	public String goPage(String page){
		if("index".equals(page)||"function_sch".equals(page)||"function_upd".equals(page)){
			return "/sysmanage/"+page;
		}else{
			return "/error";
		}
	}
}
