package com.qtrmoon.sysmanage.ctrl;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.qtrmoon.common.BaseController;
import com.qtrmoon.common.Constant;
import com.qtrmoon.manager.constant.YesOrNoConstant;
import com.qtrmoon.manager.pojo.Person;
import com.qtrmoon.manager.pojo.Stud;
import com.qtrmoon.manager.serdao.IManagerService;
import com.qtrmoon.sysmanage.SysConstant;
import com.qtrmoon.sysmanage.pojo.User;
import com.qtrmoon.sysmanage.serdao.ISysmanageService;


/**
 * 系统退出控制器
 * @Class Name LogoutController
 */
@Controller
@RequestMapping("/sysmanage/logout")
public class LogoutController extends BaseController{

	@Autowired
	private IManagerService managerService;								//声明模块Service实例
	@Autowired
	private ISysmanageService sysmanageService;//声明模块Service实例
	
	/**
	 * 删除session中存储的用户信息，并返回到登录界面
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping(value="/logout.action")
	public void logout(HttpServletRequest request,HttpServletResponse response) throws IOException{
		request.getSession(true).removeAttribute(SysConstant.CURRENT_USER);//移除用户
		response.sendRedirect("/"+Constant.PRONAME+"/");//重定向到项目根目录
	}
	
	
	@RequestMapping(value="/bookLogout.action")
	public void bookLogout(HttpServletRequest request,HttpServletResponse response) throws IOException{
		
		Person person = (Person)request.getSession(true).getAttribute(SysConstant.CURRENT_USER);//获取用户
		if(person != null){
			User us = sysmanageService.schUserById(person.getId());//获取用户
			if(us != null){
				us.setIsLogin(YesOrNoConstant.NO);
				us.setAgentId("");//清空坐席号
				sysmanageService.updUser(us);//更新用户
			}
		}
		request.getSession(true).invalidate();
		//request.getSession(true).removeAttribute(SysConstant.CURRENT_USER);//移除用户
		response.sendRedirect("/"+Constant.PRONAME+"/");
	}
}
