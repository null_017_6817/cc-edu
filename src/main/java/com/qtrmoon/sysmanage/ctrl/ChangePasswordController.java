package com.qtrmoon.sysmanage.ctrl;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qtrmoon.common.BaseController;
import com.qtrmoon.manager.pojo.Person;
import com.qtrmoon.manager.pojo.Stud;
import com.qtrmoon.manager.serdao.IManagerService;
import com.qtrmoon.sysmanage.SysConstant;
import com.qtrmoon.sysmanage.pojo.User;
import com.qtrmoon.sysmanage.serdao.ISysmanageService;
import com.qtrmoon.toolkit.MD5;

/**
 * 修改密码
 * @Class Name ChangePasswordController
 */
@Controller
@RequestMapping(value="/sysmanage/changePw")
public class ChangePasswordController  extends BaseController{
	
	@Autowired
	private ISysmanageService sysmanageService;//声明模块Service实例
	@Autowired
	private IManagerService managerService;								//声明模块Service实例
	
	/**
	 * 修改密码
	 * @param request
	 * @param response
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value="/changePw.action")
	public Map<String,Object> changePw (HttpServletRequest request, HttpServletResponse response){
		Map<String,Object> result = new HashMap<String,Object>();
		try{
			String oldPw = request.getParameter("oldPw");			// 老密码
			String newPw = request.getParameter("newPw");			// 新密码
			String confirmPw = request.getParameter("confirmPw");	// 确认密码
			Person person = (Person)request.getSession(true).getAttribute(SysConstant.CURRENT_USER); // 获取当亲用户
			if( person != null && person.getId() != null ){ //当前用户存在的场合
				String state = person.getState(); // 
				if("2".equals(state) || "x".equals(state)){// 老师修改密码的场合
					User user = sysmanageService.schUserById(person.getId()); // 数据库中获取当前登录者数据
					String pwd = user.getPasswd(); // 获取用户密码
					oldPw = MD5.getInstance().getMD5to32(oldPw);
					if( pwd.equals( oldPw ) ){
						if( newPw.equals(confirmPw) ){
							user.setPasswd(MD5.getInstance().getMD5to32(newPw));
							sysmanageService.updUser(user);
							result.put("success", true); // 返回到页面的数据
							result.put("msg", "新密码不一致");// 返回到页面的数据
						}else{
							result.put("success", false); // 返回到页面的数据
							result.put("msg", "新密码不一致");// 返回到页面的数据
						}
					}else{
						result.put("success", false); // 返回到页面的数据
						result.put("msg", "原密码错误");// 返回到页面的数据
					}
				}else if("3".equals(state)){ //学生修改密码的场合
					Stud stud = managerService.schStudById(person.getId());
					String pwd = stud.getPasswd(); // 获取用户密码
					oldPw = MD5.getInstance().getMD5to32(oldPw);
					if( pwd.equals( oldPw ) ){
						if( newPw.equals(confirmPw) ){
							stud.setPasswd(newPw);
							managerService.updStud(stud);
							result.put("success", true); // 返回到页面的数据
							result.put("msg", "修改成功");// 返回到页面的数据
						}else{
							result.put("success", false); // 返回到页面的数据
							result.put("msg", "新密码不一致");// 返回到页面的数据
						}
					}else{
						result.put("success", false); // 返回到页面的数据
						result.put("msg", "原密码错误");// 返回到页面的数据
					}
				}else{
					result.put("success", false); // 返回到页面的数据
					result.put("msg", "用户身份未知");// 返回到页面的数据
				}
			} else { // 当前用户不存在的场合
				result.put("success", false); // 返回到页面的数据
				result.put("msg", "登录超时，请重亲登录后再操作");// 返回到页面的数据
			}
		}catch(Exception e){
			e.printStackTrace(); // 异常信息
			result.put("success", false);// 返回到页面的数据
			result.put("msg", "系统异常");// 返回到页面的数据
		}finally{
			return result;
		}
	}
}
