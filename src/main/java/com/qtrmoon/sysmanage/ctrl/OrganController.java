package com.qtrmoon.sysmanage.ctrl;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.qtrmoon.camp.pojo.ZdRepository;
import com.qtrmoon.common.BaseController;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.dictionary.bsd.DictBean;
import com.qtrmoon.sysmanage.pojo.Organ;
import com.qtrmoon.sysmanage.serdao.ISysmanageService;
/**
 * 机构类
 *
 */
@Controller
@RequestMapping("/sysmanage/organ")
public class OrganController extends BaseController{
	@Autowired
	private ISysmanageService sysmanageService;//注入service

	@SuppressWarnings("unchecked")
	@RequestMapping(value="/schOrgan.action")
	public void schOrgan(Organ organ,HttpServletRequest request,HttpServletResponse response) throws IOException{
		noCache(response);//清空缓存
		organ.setCondition("");//防sql注入
		organ.assLike("name");//模糊查询
		List<Organ> list=null;//声明list
		if(organ.getId()!=null){
			boolean child=Boolean.parseBoolean(request.getParameter("child"));//是否有子
			if(child){
				
				String treetrack;//获取树结构字段
				try {
					treetrack = sysmanageService.schOrganById(organ.getId()).getTreetrack();
					organ.setCondition("AND TREETRACK like '" + treetrack
							+ "-%' or TREETRACK ='" + treetrack + "'");// 拼接sql参数
				} catch (Exception e) {
					organ.setPid(organ.getId());//设置此父
				}
				
			}else{
				organ.setPid(organ.getId());//设置此父
			}
			list=sysmanageService.schOrgan(organ);//查询机构
			JSONArray datas=new JSONArray();//json数组
			JSONObject obj;//json对象
			for(Organ u:list){
				obj=u.getJsonInDict();//获取json对象
				obj.put("_oper",u.getId());//主键
				datas.add(obj);//填充数组
			}
			JSONObject res=new JSONObject();//json对象
			res.put("total",organ.getDatasize());//设置总数
			res.put("rows", datas);//设置数据
			response.getWriter().print(res.toJSONString());//输出到输出流
		}
	}
	
	/**
	 * 更新机构
	 * @param organ
	 * @param from
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value="/updOrgan.action")
	public void updOrgan(Organ organ,HttpServletResponse response) throws IOException{
		noCache(response);//清空缓存
		if(organ.getId()==null){
			sysmanageService.addOrgan(organ);//添加机构
			if(organ.getPid()!=null){//设置节点的树路径
				Organ pOrgan=sysmanageService.schOrganById(organ.getPid());//查询父机构
				organ.setTreetrack(pOrgan.getTreetrack()+"-"+organ.getId());//设置机构树字段
				sysmanageService.updOrgan(organ);//更新机构
			}
		}else{
			sysmanageService.updOrgan(organ);//更新机构
		}
		DictBuffer.updCache("SYS_ORGAN");//更新缓存
	}
	
	/**
	 * 预添加or预修改or查看方法
	 * @param organ
	 * 预添加时可传入一些初始值；预修改和查看时用来承载主键值
	 * 挂字典的字段是否需要转为字典Label值。true则进行转换
	 * @param response
	 * 返回请求,输出Json
	 * @throws IOException
	 */
	@RequestMapping(value = "/vieOrgan.action")
	public void vieOrgan(Organ organ, Boolean toL,
			HttpServletResponse response) throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (toL != null && toL) { // 查看页面加载数据时调用，返回的对象已进行字典转换
			if (organ.getId() != null && organ.getId() != 0) { // 检测主键非空
				organ = sysmanageService.schOrganById(organ.getId()); // 按主键查询数据
				response.getWriter().print(organ.getJsonInDict()); // 输出经过字典转换Json对象到查看页(vie_)
			}
		} else { // 修改页面加载数据时调用
			if (organ.getId() != null && organ.getId() != 0) { // 检查主键非空
				organ = sysmanageService.schOrganById(organ.getId()); // 按主键查询数据
				response.getWriter().print(organ.getJson()); // 输出Json对象到修改页(upd_)
			} else { // 添加页面加载数据时调用，可设置添加页面中一些属性的预设值。
				if(organ.getPid()!=null&&organ.getPid()>0){
					Organ r=new Organ();
					r.setPid(organ.getPid());
					response.getWriter().print(r.getJson()); // 输出Json对象到添加页(upd_)
				}else{
					response.getWriter().print(new Organ().getJson()); // 输出Json对象到添加页(upd_)
				}
			}
		}
	}
	
	/**
	 * 删除机构
	 * @param ids
	 * @param response
	 */
	@RequestMapping(value="/delOrgan.action")
	public void delOrgan(@RequestParam(value = "ids[]") Integer[] ids,HttpServletResponse response) throws Exception{
		noCache(response);//清空缓存
		JSONObject res=new JSONObject();
		boolean canDelete = true;
		for(int id : ids) {
			Organ organ=new Organ();
			organ.setPid(id);
			List<Organ> organs = sysmanageService.schOrgan(organ);
			if(organs != null && organs.size() > 0) {
				canDelete = false;
			}
		}
		if(canDelete) {
			sysmanageService.delOrgan(ids);//删除机构
			res.put("status", "0");
		}else {
			res.put("status", "1");
		}
		DictBuffer.updCache("SYS_ORGAN");//更新机构字典
		response.getWriter().print(res.toJSONString());
	}
	
	/**
	 * JSP页面跳转
	 * @param page jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value="/page.action")
	public String goPage(String page){
		if("index".equals(page)||"organ_sch".equals(page)||"organ_upd".equals(page)||"organ_vie".equals(page)){
			return "/sysmanage/"+page;//返回相应页面
		}else{
			return "/error";//返回相应页面
		}
	}
}
