package com.qtrmoon.sysmanage.ctrl;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.qtrmoon.common.BaseController;
import com.qtrmoon.sysmanage.pojo.Functionunit;
import com.qtrmoon.sysmanage.serdao.ISysmanageService;
/**
 * 
 * 功能单元类
 */
@Controller
@RequestMapping("/sysmanage/functionunit")
public class FunctionunitController extends BaseController{
	@Autowired
	private ISysmanageService sysmanageService;//service注入
	/**
	 * 查询功能单元
	 * @param functionunit
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/schFunctionunit.action")
	public void schFunctionunit(Functionunit functionunit,HttpServletRequest request,HttpServletResponse response) throws IOException{
		noCache(response);//清空缓存
		functionunit.setCondition("");//防sql注入
		List<Functionunit> list=null;
		list=sysmanageService.schFunctionunit(functionunit);//查询功能单元列表
		JSONArray datas=new JSONArray();
		JSONObject obj;
		for(Functionunit u:list){
			obj=u.getJson();//获取json对象
			obj.put("_oper",u.getId());//获取行id
			datas.add(obj);//
		}
		JSONObject res=new JSONObject();
		res.put("total",functionunit.getDatasize());//功能单元总数
		res.put("rows", datas);//填充数据
		response.getWriter().print(res.toJSONString());//输出到view层
	}
	/**
	 * 更新功能单元
	 * @param functionunit
	 * @param from
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value="/updFunctionunit.action")
	public void updFunctionunit(Functionunit functionunit,String from,HttpServletResponse response) throws IOException{
		noCache(response);//清空缓存
		if("submit".equals(from)){
			if(functionunit.getId()==null){
				sysmanageService.addFunctionunit(functionunit);//添加功能单元
			}else{
				sysmanageService.updFunctionunit(functionunit);//更新功能单元
			}
		}else{
			if(functionunit.getId()!=null&&functionunit.getId()!=0){
				functionunit=sysmanageService.schFunctionunitById(functionunit.getId());//根据id查询功能单元
				response.getWriter().print(functionunit.getJson());//输出到view层
			}else{
				response.getWriter().print(new Functionunit().getJson());//输出到view层
			}
		}
	}
	/**
	 * 删除功能单元
	 * @param ids
	 * @param response
	 */
	@RequestMapping(value="/delFunctionunit.action")
	public void delFunctionunit(@RequestParam(value = "ids[]") Integer[] ids,HttpServletResponse response){
		noCache(response);
		sysmanageService.delFunctionunit(ids);//删除功能单元
	}
	
	/**
	 * JSP页面跳转
	 * @param page jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value="/page.action")
	public String goPage(String page){
		if("index".equals(page)||"functionunit_sch".equals(page)||"functionunit_upd".equals(page)||"functionunit_vie".equals(page)){
			return "/sysmanage/"+page;
		}else{
			return "/error";
		}
	}
}
