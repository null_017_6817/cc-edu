package com.qtrmoon.sysmanage.ctrl;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.qtrmoon.common.BaseController;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.sysmanage.pojo.Role;
import com.qtrmoon.sysmanage.serdao.ISysmanageService;
/**
 * 角色类
 *
 */
@Controller
@RequestMapping("/sysmanage/role")
public class RoleController extends BaseController{
	@Autowired
	private ISysmanageService sysmanageService;//注入service
	/**
	 * 查询角色
	 * @param role
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/schRole.action")
	public void schRole(Role role,HttpServletRequest request,HttpServletResponse response) throws IOException{
		noCache(response);//清空缓存
		role.setCondition("");//防sql注入
		role.assLike("name");
		List<Role> list=null;
		list=sysmanageService.schRole(role);//查询角色列表
		JSONArray datas=new JSONArray();//json数组
		JSONObject obj;
		for(Role u:list){
			obj=u.getJson();//获取json对象
			obj.put("_oper",u.getId());//主键
			datas.add(obj);//填充json数组
		}
		JSONObject res=new JSONObject();
		res.put("total",role.getDatasize());//设置数据总数
		res.put("rows", datas);//设置数据
		response.getWriter().print(res.toJSONString());//输出json到view层
	}
	@RequestMapping(value="/updRole.action")
	public void updRole(Role role,String from,HttpServletResponse response) throws IOException{
		noCache(response);//清空缓存
		if("submit".equals(from)){//表单提交
			if(role.getId()==null){
				sysmanageService.addRole(role);//添加角色
			}else{
				sysmanageService.updRole(role);//更新角色
			}
			DictBuffer.updCache("SYS_ROLE");//刷新角色字典
		}else{
			if(role.getId()!=null&&role.getId()!=0){
				role=sysmanageService.schRoleById(role.getId());//获取角色
				response.getWriter().print(role.getJson());//输出json到view层
			}else{
				response.getWriter().print(new Role().getJson());//输出json到view层
			}
		}
	}
	/**
	 * 删除角色
	 * @param ids
	 * @param response
	 */
	@RequestMapping(value="/delRole.action")
	public void delRole(@RequestParam(value = "ids[]") Integer[] ids,HttpServletResponse response){
		noCache(response);//清空缓存
		sysmanageService.delRole(ids);//批量删除角色
		DictBuffer.updCache("SYS_ROLE");//刷新角色字典
	}
	
	/**
	 * JSP页面跳转
	 * @param page jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value="/page.action")
	public String goPage(String page){
		if("index".equals(page)||"role_sch".equals(page)||"role_upd".equals(page)||"role_vie".equals(page)){
			return "/sysmanage/"+page;//返回相应页面
		}else{
			return "/error";//返回相应页面
		}
	}
	
}
