package com.qtrmoon.sysmanage.ctrl;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qtrmoon.book.util.FilePath;
import com.qtrmoon.camp.ctrl.MessageSocket;
import com.qtrmoon.camp.ctrl.UserOnlineConstant;
import com.qtrmoon.camp.ctrl.WebSocketTest;
import com.qtrmoon.common.BaseController;
import com.qtrmoon.common.Constant;
import com.qtrmoon.manager.constant.YesOrNoConstant;
import com.qtrmoon.manager.pojo.Person;
import com.qtrmoon.manager.pojo.Stud;
import com.qtrmoon.manager.serdao.IManagerService;
import com.qtrmoon.sysmanage.SysConstant;
import com.qtrmoon.sysmanage.pojo.Organ;
import com.qtrmoon.sysmanage.pojo.User;
import com.qtrmoon.sysmanage.serdao.ISysmanageService;
import com.qtrmoon.toolkit.MD5;
import com.qtrmoon.util.FileUpload;
import com.qtrmoon.util.PathUtil;

/**
 * 登录注销等功能，不需要权限判定，任何用户都可以请求该类的方法。
 */
@Controller
@RequestMapping("/sysmanage/login")
public class LoginController extends BaseController{
	
	@Autowired
	private ISysmanageService sysmanageService;//注入service
	@Autowired
	private IManagerService managerService;//注入service

	/**
	 * 
	 * 后台登录
	 * 2017年9月8日
	 * @param user
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws IOException
	 */
	@Deprecated
	@RequestMapping(value="/login.action")
	public String login(User user,HttpServletRequest request,HttpServletResponse response) throws IOException{
		noCache(response);//清空缓存
		if("3".equals(user.getState()))return null;
		Person validPerson=valid(user);// 校验用户名密码
		if(validPerson!=null){
			validPerson.setLoginIp(request.getRemoteAddr());//获取登陆ip
			request.getSession(true).setAttribute(SysConstant.CURRENT_USER, validPerson);//person放入session中
			response.sendRedirect("/"+Constant.PRONAME+"/sysmanage/login/page.action?page=main");//跳转到主页
		}else{
			response.getWriter().print("error");//输出错误
		}
		return null;
	}
	
	/**
	 * 验证登陆者账号密码的方法
	 * @param user
	 * @return
	 */
	@Deprecated
	private Person valid(User user){
		Person validPerson=null;
		if("3".equals(user.getState())){//学生角色
			Stud stud=new Stud();//创建学生
			stud.setNumber(user.getNumber());
			stud.setPasswd(MD5.getInstance().getMD5to32(user.getPasswd()));//加密密码
			List<Stud> userList = managerService.schStud(stud);//查询学生是否存在
			if (userList.size() > 0)userList.get(0).setState("3"); validPerson=userList.get(0);//获取学生
		}else{
			User sqlForm = new User();//创建用户
			sqlForm.setNumber(user.getNumber());//设置学号
			List<User> userList = sysmanageService.schUser(sqlForm);//查询老师或管理员
			if (userList.size() > 0){
				userList.get(0).setState("2");//设置状态
				validPerson=userList.get(0);//获取用户
			}
		}
		
		if (validPerson!=null) {
			String psw = user.getPasswd();//获取密码
			if (Constant.getBooleanConstant("pswmd5")) {
				psw = MD5.getInstance().getMD5to32(psw);//加密登陆密码
			}
			if (validPerson.getPasswd().equals(psw)) {//登陆密码和数据库密码相同
				sysmanageService.setUserAuthor(validPerson);
				if(validPerson.getMenu()!=null){
					Organ organ=sysmanageService.schOrganById(validPerson.getOrganid());//查询机构
					validPerson.setOrgan(organ);//设置机构
				}
			}
		}
		return validPerson;//返回用户
	}
	
	/**
	 * 前台登录
	 * @param user
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws IOException
	 */
	@SuppressWarnings({ "finally" })
	@ResponseBody
	@RequestMapping(value="/bookLogin.action")
	public Map<String,Object> bookLogin(User user,HttpServletRequest request,HttpServletResponse response) throws IOException{
		noCache(response);
		Map<String,Object> result = new HashMap<String,Object>();
		try{
			Person validPerson = preValid(user);// 前台登录校验用户名密码
			
			int maxNum = 0;
			String maxNumStr = FilePath.getFilePath("maxOnlineNum");
			if(maxNumStr != null){
				maxNum = Integer.valueOf(maxNumStr);
			}else{
				result.put("success",false);// 返回到页面的数据
				result.put("msg", "登录失败,未授权在线人数");// 返回到页面的数据
				return result;
			}
			// 最大登录数
			int count = UserOnlineConstant.onlineNum;
			if(count >= maxNum ){
				Person person = (Person)request.getSession(true).getAttribute(SysConstant.CURRENT_USER);//获取用户
				if(person != null){
					User us = sysmanageService.schUserById(person.getId());//获取用户
					if(us != null){
						us.setIsLogin(YesOrNoConstant.NO);
						us.setAgentId("");//清空坐席号
						sysmanageService.updUser(us);//更新用户
					}
				}
				request.getSession(true).invalidate();
				result.put("success",false);// 返回到页面的数据
				result.put("msg", "登录失败,已超过最大登录数");// 返回到页面的数据
				return result;
			}
			
			if(validPerson!=null){
				validPerson.setLoginIp(request.getRemoteAddr());//设置登陆ip
				String state = validPerson.getState();
				// 登录时，更新坐席号，如果state{2：老师，3：学生}
				User us = sysmanageService.schUserById(validPerson.getId());
				us.setAgentId(user.getAgentId());//设置坐席号
				us.setIsLogin(YesOrNoConstant.YES);// 修改为已登录
				sysmanageService.updUser(us);//更新用户
				result.put("Cnumber", us.getCnumber());
				result.put("stuPhotoUrl", us.getPhotoUrl());
					
					
				validPerson.setAgentId(user.getAgentId());// 设置agentId到当前用户
				request.getSession(true).setAttribute(SysConstant.CURRENT_USER, validPerson);//设置用户到session域
				Person person = new Stud();//创建时间
				person.setName(validPerson.getName());//设置姓名
				person.setState(validPerson.getState());//设置状态
				person.setAgentId(validPerson.getAgentId());//设置坐席号
				
				result.put("person", person);// state---2:老师，3：学生
				result.put("success", true);// 返回到页面的数据
				result.put("msg", "登录成功");// 返回到页面的数据
				
			}else{
				result.put("success",false);// 返回到页面的数据
				result.put("msg", "登录失败,用户名或密码错误");// 返回到页面的数据
			}
		}catch(Exception e){
			e.printStackTrace();// 打印异常
			result.put("success",false);// 返回到页面的数据
			result.put("msg", "登录异常");// 返回到页面的数据
		}finally{
			return result;// 返回到页面的数据
		}
	}
	
	/**
	 * 前台登录校验
	 * @param user
	 * @return
	 */
	private Person preValid(User user){
		Person validPerson=null;//验证人

		User sqlForm = new User();
		sqlForm.setNumber(user.getNumber());//设置登陆名
		List<User> teaList = sysmanageService.schUser(sqlForm);//查询用户
		if (teaList != null && teaList.size() > 0){
			validPerson=teaList.get(0);//选取第一个用户
		}
		//如果
		if (validPerson !=null) {
			String psw = user.getPasswd();//获取密码
			if (Constant.getBooleanConstant("pswmd5")) {
				psw = MD5.getInstance().getMD5to32(psw);//加密登陆密码
			}
			if (validPerson.getPasswd().equals(psw)) {
				sysmanageService.setUserAuthor(validPerson);//设置用户
				if(validPerson.getMenu()!=null){
					Organ organ=sysmanageService.schOrganById(validPerson.getOrganid());//查询机构
					validPerson.setOrgan(organ);//设置机构
				}
			}else{
				validPerson=null;//赋值null
			}
		}
		return validPerson;//返回验证人
	}
	
	/**
	 * 用户申请接口，用户登录成功后再调用本接口。
	 * 自助角色申请，输入用户主键，将用户所在部门的角色赋予给用户。
	 * @param request
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping(value="/selfApply.action")
	public void selfApply(String id,HttpServletRequest request,HttpServletResponse response) throws IOException{
		noCache(response);
		User user=(User)request.getSession(true).getAttribute(SysConstant.CURRENT_USER);//获取用户
		if(id!=null&&user!=null&&id.equals(user.getId())){//校验必须是用户自己来申请
			Organ organ=sysmanageService.schOrganById(user.getOrganid());//查询机构
			user.addRoles(organ.getRoles());//添加角色
			sysmanageService.updUser(user);//更新用户
			response.getWriter().print("success");// 返回到页面的数据
			return;
		}
		response.getWriter().print("failure");// 返回到页面的数据
	}
	
	/**
	 * 退出系统方法
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/logout.action")
	public String logout(HttpServletRequest request){
		request.getSession(true).removeAttribute(SysConstant.CURRENT_USER);//移除用户
		return "/index.jsp";//返回首页
	}
	
	
	/**
	 * JSP页面跳转
	 * @param page jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value="/page.action")
	public String goPage(String page, String state, Model model){
		if("index".equals(page) || "main".equals(page)){
			return "/"+page;//返回相应页面
		}else if("login".equals(page)) {
			model.addAttribute("state", state);
			return "/"+page;//返回相应页面
		}
		else{
			return "/error";//返回相应页面
		}
	}
	
	/**
	 * 跳转提示权限页面
	 * @return
	 */
	@RequestMapping(value="/goAuthority.action")
	public String goAuthority(){
		
		return "/noAuthority";
	}
	
	
	
	/**
	 * 头像加载
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/loadPhotos.action")
	public Map<String,Object> loadPhotos(HttpServletRequest request,HttpServletResponse response){
		Map<String,Object> result = new HashMap<String,Object>();
		//Stud stud=(Stud)request.getSession(true).getAttribute(SysConstant.CURRENT_USER);//获取当前用户
		String cnumber= request.getParameter("cnumber");			//学生班级信息
		try {
			if(StringUtils.isNotBlank(cnumber)){
				FilePath.getFilePath("photoUrl");
				String serverPath =PathUtil.appendBackSlant(FilePath.getFilePath("photoUrl"))+cnumber;
				String webPath=request.getSession().getServletContext().getRealPath("")+"/imgs/photos/"+cnumber;
				logger.info("服务器头像地址："+serverPath);
				logger.info("web应用头像地址："+webPath);
				FileUpload.copyDir(serverPath,webPath);
				result.put("success",true);
			}else{
				result.put("success",false);
			}
		} catch (IOException e) {
			e.printStackTrace();
			result.put("success",false);
		}
		return result;// 返回到页面的数据
	}
	
	
}