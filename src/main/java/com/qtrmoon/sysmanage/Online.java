package com.qtrmoon.sysmanage;

import java.util.HashMap;
import java.util.Map;

import com.qtrmoon.manager.pojo.Person;
/**
 * 在线用户类
 * @author HuaTang_DYC
 *
 */
public class Online {
	private static Map<String, Person> onlinePerson = new HashMap<String, Person>();// 在线用户
	/**
	 * 获取在线用户
	 * @return
	 */
	public static Map<String, Person> getOnlinePerson() {
		return onlinePerson;
	}
	/**
	 * 添加在线用户
	 * @param person
	 */
	public static void addPerson(Person person) {
		if (onlinePerson.get(person.getAgentId())== null) {
			onlinePerson.put(person.getAgentId().toString(),person);//放入在线用户
		}
	}
	/**
	 * 删除在线用户
	 * @param id
	 */
	public static void delPerson(String id) {
		onlinePerson.remove(id);
	}
}
