package com.qtrmoon.sysmanage.pojo;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.json.simple.JSONObject;

import com.qtrmoon.common.PageForm;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.dictionary.bsd.DictBean;
import com.qtrmoon.manager.pojo.Person;
import com.qtrmoon.sysmanage.Online;
import com.qtrmoon.util.LoginStudUtil;
	/** 用户表Object类 */
	public class User extends PageForm implements Person, HttpSessionBindingListener{
	//Fields
	
	private Integer id;// 主键
	private String number;// 学号
	private String name;// 姓名
	private String cnumber;// 班级号
	private String sex;// 性别
	private String info;// 资料
	private String passwd;// 密码
	private String curRoles;// 0：学生（本色） 1：陪练员 2：观察员


 
	private String agentId;// CC座席号
	private String callExpired;// 通话过期时刻
	private Integer partnerType;// 人员类型
	private String partnerClass;// 班级/教研室
	private String partnerId;// 学号/教师
	private String partnerAgent;// 陪练坐席号
	private String curExercise;// 当前做题进度
	private String myExercise;// 我的试卷
	private String remark;// 备注
	private String isexam;// 是否正在考试{0:未考试，1:正在考试}
	private String photoUrl;// 头像路径
	private String isLogin;// 是否登录{0:未登录，1:已登录}
	private String login;// 登录
	private Integer organid;// 组织机构ID
	private String roles;// 角色
	private String state;// 状态(管理员或启用)
	
	//缓存属性，不入库
	private String loginIp;//登录用户的IP地址
	private Organ organ;//登录用户所属机构
//	private Function menuRoot;//菜单的总根
	private List<Function> menu;//自己的权限表，任一节点已构造好树结构，便于从任意节点寻子。
	public static String PASSWORD="111111";//设置重置密码常量
	
	private String sessionId;//session标志
	private Integer taskId;//任务id
	private	Integer callState = 0; //0 未通话 1:拨打2:接听
	private String profession;//行业
	private List<String> to;//去电信息
	private Integer answerState;//应答状态
	private String callTime;//接听时间
	
	private String taskType;
	
	//Constructors
	/** default constructor */
	public User() {
	
	}
	
	//getter and setter
	
	
	
	//getter and setter
	/** 获取主键 */
	public Integer getId() {
		return this.id;
	}
	/** 设置主键 */
	public void setId(Integer id) {
		this.id = id;
	}
	/** 获取学号 */
	public String getNumber() {
		return this.number;
	}
	/** 设置学号 */
	public void setNumber(String number) {
		this.number = number;
	}
	/** 获取姓名 */
	public String getName() {
		return this.name;
	}
	/** 设置姓名 */
	public void setName(String name) {
		this.name = name;
	}
	/** 获取班级号 */
	public String getCnumber() {
		return this.cnumber;
	}
	/** 设置班级号 */
	public void setCnumber(String cnumber) {
		this.cnumber = cnumber;
	}
	/** 获取性别 */
	public String getSex() {
		return this.sex;
	}
	/** 设置性别 */
	public void setSex(String sex) {
		this.sex = sex;
	}
	/** 获取资料 */
	public String getInfo() {
		return this.info;
	}
	/** 设置资料 */
	public void setInfo(String info) {
		this.info = info;
	}
	/** 获取密码 */
	public String getPasswd() {
		return this.passwd;
	}
	/** 设置密码 */
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	/** 获取0：学生（本色）
 1：陪练员
 2：观察员
  */
	public String getCurRoles() {
		return this.curRoles;
	}
	/** 设置0：学生（本色）
 1：陪练员
 2：观察员
  */
	public void setCurRoles(String curRoles) {
		this.curRoles = curRoles;
	}
	/** 获取CC座席号 */
	public String getAgentId() {
		return this.agentId;
	}
	/** 设置CC座席号 */
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	/** 获取通话过期时刻 */
	public String getCallExpired() {
		return this.callExpired;
	}
	/** 设置通话过期时刻 */
	public void setCallExpired(String callExpired) {
		this.callExpired = callExpired;
	}
	/** 获取人员类型 */
	public Integer getPartnerType() {
		return this.partnerType;
	}
	/** 设置人员类型 */
	public void setPartnerType(Integer partnerType) {
		this.partnerType = partnerType;
	}
	/** 获取班级/教研室 */
	public String getPartnerClass() {
		return this.partnerClass;
	}
	/** 设置班级/教研室 */
	public void setPartnerClass(String partnerClass) {
		this.partnerClass = partnerClass;
	}
	/** 获取学号/教师 */
	public String getPartnerId() {
		return this.partnerId;
	}
	/** 设置学号/教师 */
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	/** 获取陪练坐席号 */
	public String getPartnerAgent() {
		return this.partnerAgent;
	}
	/** 设置陪练坐席号 */
	public void setPartnerAgent(String partnerAgent) {
		this.partnerAgent = partnerAgent;
	}
	/** 获取当前做题进度 */
	public String getCurExercise() {
		return this.curExercise;
	}
	/** 设置当前做题进度 */
	public void setCurExercise(String curExercise) {
		this.curExercise = curExercise;
	}
	/** 获取我的试卷 */
	public String getMyExercise() {
		return this.myExercise;
	}
	/** 设置我的试卷 */
	public void setMyExercise(String myExercise) {
		this.myExercise = myExercise;
	}
	/** 获取备注 */
	public String getRemark() {
		return this.remark;
	}
	/** 设置备注 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/** 获取是否正在考试{0:未考试，1:正在考试} */
	public String getIsexam() {
		return this.isexam;
	}
	/** 设置是否正在考试{0:未考试，1:正在考试} */
	public void setIsexam(String isexam) {
		this.isexam = isexam;
	}
	/** 获取头像路径 */
	public String getPhotoUrl() {
		return this.photoUrl;
	}
	/** 设置头像路径 */
	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}
	/** 获取是否登录{0:未登录，1:已登录} */
	public String getIsLogin() {
		return this.isLogin;
	}
	/** 设置是否登录{0:未登录，1:已登录} */
	public void setIsLogin(String isLogin) {
		this.isLogin = isLogin;
	}
	/** 获取登录 */
	public String getLogin() {
		return this.login;
	}
	/** 设置登录 */
	public void setLogin(String login) {
		this.login = login;
	}
	/** 获取组织机构ID */
	public Integer getOrganid() {
		return this.organid;
	}
	/** 设置组织机构ID */
	public void setOrganid(Integer organid) {
		this.organid = organid;
	}
	/** 获取角色 */
	public String getRoles() {
		return this.roles;
	}
	/** 设置角色 */
	public void setRoles(String roles) {
		this.roles = roles;
	}
	/** 获取状态(管理员或启用) */
	public String getState() {
		return this.state;
	}
	/** 设置状态(管理员或启用) */
	public void setState(String state) {
		this.state = state;
	}
	

	/**
	 * 在已有的角色集合上附加角色，并清洗重复ID。
	 * @param roles2
	 */
	public void addRoles(String r2) {
		if(r2==null||r2.equals("")){//为空
			return;
		}
		if(roles==null||roles.equals("")){//角色为空
			roles=r2;//赋值
		}else{
			//借助Map进行重复清洗。
			String rstr=roles+","+r2;
			String[] rarr=rstr.split(",");//分隔角色id
			Map<String,String> tmp=new HashMap<String,String>();//创建map
			for(String r:rarr){//遍历
				tmp.put(r, r);//填充map
			}
			String str=Arrays.toString(tmp.keySet().toArray());//获取id串
			roles=str.replace("[", "").replace("]", "").replaceAll(" ", "").trim();//去取[]
		}
	}

	
	/**
	 * 绑定session
	 */
	@Override
	public void valueBound(HttpSessionBindingEvent event) {
		String id = agentId;//坐席号
		Person person = Online.getOnlinePerson().get(id);//获取在线人
		if(person != null) {
			person.setSessionId(id);//设置seesion标识
		}else{
			this.setSessionId(id);//设置seesion标识
			Online.addPerson(this);//添加在线人
		}
		LoginStudUtil.studLogin(this);
	}

	@Override
	public void valueUnbound(HttpSessionBindingEvent event) {
		String id = agentId;//坐席号
		Person person = Online.getOnlinePerson().get(id);//获取在线人
		if(person != null && person.getSessionId().equals(id)){
			Online.delPerson(id);//删除seesion标识
		}
		LoginStudUtil.studLogout(this);
	}

	/**
	 * 获取sessionid
	 */
	public String getSessionId() {
		return sessionId;
	}
	/**
	 * 设置sessionid
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	/**
	 * 获取任务id
	 */
	public Integer getTaskId() {
		return taskId;
	}
	/**
	 * 设置任务id
	 */
	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}
	/**
	 * 获取电话状态
	 */
	public Integer getCallState() {
		return callState;
	}
	/**
	 * 设置电话状态
	 */
	public void setCallState(Integer callState) {
		this.callState = callState;
	}
	/**
	 * 获取行业
	 */
	public String getProfession() {
		return profession;
	}
	/**
	 * 设置行业
	 */
	public void setProfession(String profession) {
		this.profession = profession;
	}
	/**
	 * 获取去电信息
	 */
	public List<String> getTo() {
		return to;
	}
	/**
	 * 设置去电信息
	 */
	public void setTo(List<String> to) {
		this.to = to;
	}

	public String getTaskType() {
		return taskType;
	}

	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}

	

	/**
	 * 获取loginIp
	 * @return the loginIp
	 */
	public String getLoginIp() {
		return loginIp;
	}

	/**
	 * 设置loginIp
	 * @param loginIp the loginIp to set
	 */
	public void setLoginIp(String loginIp) {
		this.loginIp = loginIp;
	}

	/**
	 * 获取organ
	 * @return the organ
	 */
	public Organ getOrgan() {
		return organ;
	}

	/**
	 * 设置organ
	 * @param organ the organ to set
	 */
	public void setOrgan(Organ organ) {
		this.organ = organ;
	}

	/**
	 * 获取menu
	 * @return the menu
	 */
	public List<Function> getMenu() {
		return menu;
	}

	/**
	 * 设置menu
	 * @param menu the menu to set
	 */
	public void setMenu(List<Function> menu) {
		this.menu = menu;
	}

	/**
	 * 获取pASSWORD
	 * @return the pASSWORD
	 */
	public static String getPASSWORD() {
		return PASSWORD;
	}

	/**
	 * 设置pASSWORD
	 * @param pASSWORD the pASSWORD to set
	 */
	public static void setPASSWORD(String pASSWORD) {
		PASSWORD = pASSWORD;
	}

	/**
	 * 获取answerState
	 * @return the answerState
	 */
	public Integer getAnswerState() {
		return answerState;
	}

	/**
	 * 设置answerState
	 * @param answerState the answerState to set
	 */
	public void setAnswerState(Integer answerState) {
		this.answerState = answerState;
	}

	/**
	 * 获取CallTime
	 */
	public String getCallTime() {
		return callTime;
	}
	/**
	 * 设置CallTime
	 */
	public void setCallTime(String callTime) {
		this.callTime = callTime;
	}

	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("number", number);
		obj.put("name", name);
		obj.put("cnumber", cnumber);
		obj.put("sex", sex);
		obj.put("info", info);
		obj.put("passwd", passwd);
		obj.put("curRoles", curRoles);
		obj.put("agentId", agentId);
		obj.put("callExpired", callExpired);
		obj.put("partnerType", partnerType);
		obj.put("partnerClass", partnerClass);
		obj.put("partnerId", partnerId);
		obj.put("partnerAgent", partnerAgent);
		obj.put("curExercise", curExercise);
		obj.put("myExercise", myExercise);
		obj.put("remark", remark);
		obj.put("isexam", isexam);
		obj.put("photoUrl", photoUrl);
		obj.put("isLogin", isLogin);
		obj.put("login", login);
		obj.put("organid", organid);
		obj.put("roles", roles);
		obj.put("state", state);
		

		if(organid!=null&&!organid.equals("")){
			DictBean dict=DictBuffer.getDictById("SYS_ORGAN", organid.toString());
			if(dict!=null){
				obj.put("organname", dict.getLabel());
			}
		}
		obj.put("sessionId", sessionId);
		obj.put("taskId", taskId);
		obj.put("callState", callState);
		obj.put("profession", profession);
		
		return obj;
	}
	
	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("number", number);
		obj.put("name", name);
		obj.put("cnumber", cnumber);
		if(cnumber != null && !"".equals(cnumber)){
			obj.put("cnumberName", DictBuffer.getLabel("CC_CLASSES", cnumber));
		}
		if(sex!=null&&!sex.equals("")){
			obj.put("sex", DictBuffer.getLabel("ZD_XB", sex));
		}
		obj.put("info", info);
		obj.put("passwd", passwd);
		obj.put("curRoles", curRoles);
		obj.put("agentId", agentId);
		obj.put("callExpired", callExpired);
		obj.put("partnerType", partnerType);
		obj.put("partnerClass", partnerClass);
		obj.put("partnerId", partnerId);
		obj.put("partnerAgent", partnerAgent);
		obj.put("curExercise", curExercise);
		obj.put("myExercise", myExercise);
		obj.put("remark", remark);
		obj.put("isexam", isexam);
		obj.put("photoUrl", photoUrl);
		obj.put("isLogin", isLogin);
		obj.put("login", login);
		obj.put("organid", organid);
		obj.put("state", state);
		
		if(roles!=null&&!roles.equals("")){
			obj.put("roles", DictBuffer.getLabel("SYS_ROLE", roles));
		}

		if(organid!=null&&!organid.equals("")){
			obj.put("organname", DictBuffer.getLabel("SYS_ORGAN", organid.toString()));
		}
		
		obj.put("sessionId", sessionId);
		obj.put("taskId", taskId);
		obj.put("callState", callState);
		obj.put("profession", profession);
		
		return obj;
	}
}
