package com.qtrmoon.sysmanage.pojo;
import org.json.simple.JSONObject;

import com.qtrmoon.common.PageForm;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.dictionary.bsd.DictBean;
	/** 
	 * 机构Object类 
	 *
	 */
	public class Organ extends PageForm{
	//Fields
	
	private Integer id;// 主键
	private Integer pid;// 父
	private String pname;// 父机构名称，不存库
	private String name;// 机构名称
	private String ismaster;// 是否机构
	private String info;// 说明
	private Integer ord;// 排序
	private String treetrack;// 树结构路径
	private String code;// 机构编码
	private String type;// 机构类型
	private String modulename;// 模块类别
	private String funcunits;//
	private String roles;// 机构角色，机构内的用户会自动赋予的权限。
	//Constructors
	/** default constructor */
	public Organ() {
	
	}	
	//getter and setter
	
	/**
	 * 获取json对象
	 * @return
	 */
	public JSONObject getJson() {
		JSONObject obj=new JSONObject();
		obj.put("id", this.getId());
		obj.put("pid", this.getPid());
		obj.put("pname", this.getPname());
		obj.put("name", this.getName());
		obj.put("ismaster", this.getIsmaster());
		obj.put("info", this.getInfo());
		obj.put("ord", this.getOrd());
		obj.put("treetrack", this.getTreetrack());
		obj.put("code", this.getCode());
		obj.put("type", this.getType());
		obj.put("modulename", this.getModulename());
		obj.put("funcunits", this.getFuncunits());
		obj.put("roles", roles);
		return obj;
	}
	
	/**
	 * 获取id
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}


	/**
	 * 设置id
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}


	/**
	 * 获取pid
	 * @return the pid
	 */
	public Integer getPid() {
		return pid;
	}


	/**
	 * 设置pid
	 * @param pid the pid to set
	 */
	public void setPid(Integer pid) {
		this.pid = pid;
	}


	/**
	 * 获取pname
	 * @return the pname
	 */
	public String getPname() {
		return pname;
	}


	/**
	 * 设置pname
	 * @param pname the pname to set
	 */
	public void setPname(String pname) {
		this.pname = pname;
	}


	/**
	 * 获取name
	 * @return the name
	 */
	public String getName() {
		return name;
	}


	/**
	 * 设置name
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * 获取ismaster
	 * @return the ismaster
	 */
	public String getIsmaster() {
		return ismaster;
	}


	/**
	 * 设置ismaster
	 * @param ismaster the ismaster to set
	 */
	public void setIsmaster(String ismaster) {
		this.ismaster = ismaster;
	}


	/**
	 * 获取info
	 * @return the info
	 */
	public String getInfo() {
		return info;
	}


	/**
	 * 设置info
	 * @param info the info to set
	 */
	public void setInfo(String info) {
		this.info = info;
	}


	/**
	 * 获取ord
	 * @return the ord
	 */
	public Integer getOrd() {
		return ord;
	}


	/**
	 * 设置ord
	 * @param ord the ord to set
	 */
	public void setOrd(Integer ord) {
		this.ord = ord;
	}


	/**
	 * 获取treetrack
	 * @return the treetrack
	 */
	public String getTreetrack() {
		return treetrack;
	}


	/**
	 * 设置treetrack
	 * @param treetrack the treetrack to set
	 */
	public void setTreetrack(String treetrack) {
		this.treetrack = treetrack;
	}


	/**
	 * 获取code
	 * @return the code
	 */
	public String getCode() {
		return code;
	}


	/**
	 * 设置code
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}


	/**
	 * 获取type
	 * @return the type
	 */
	public String getType() {
		return type;
	}


	/**
	 * 设置type
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}


	/**
	 * 获取modulename
	 * @return the modulename
	 */
	public String getModulename() {
		return modulename;
	}


	/**
	 * 设置modulename
	 * @param modulename the modulename to set
	 */
	public void setModulename(String modulename) {
		this.modulename = modulename;
	}


	/**
	 * 获取funcunits
	 * @return the funcunits
	 */
	public String getFuncunits() {
		return funcunits;
	}


	/**
	 * 设置funcunits
	 * @param funcunits the funcunits to set
	 */
	public void setFuncunits(String funcunits) {
		this.funcunits = funcunits;
	}


	/**
	 * 获取roles
	 * @return the roles
	 */
	public String getRoles() {
		return roles;
	}


	/**
	 * 设置roles
	 * @param roles the roles to set
	 */
	public void setRoles(String roles) {
		this.roles = roles;
	}

	/**
	 * 获取字典json对象
	 * @return
	 */
	public JSONObject getJsonInDict() {
		JSONObject obj=new JSONObject();
		obj.put("id", this.getId());
		obj.put("pid", this.getPid());
		obj.put("pname", this.getPname());
		obj.put("name", this.getName());
		if(ismaster!=null&&!ismaster.equals("")){
			DictBean dict=DictBuffer.getDictById("SYS_ORGAN_NATURE", ismaster);
			if(dict!=null){
				obj.put("ismaster", dict.getLabel());
			}
		}
		obj.put("info", this.getInfo());
		obj.put("ord", this.getOrd());
		obj.put("treetrack", this.getTreetrack());
		obj.put("code", this.getCode());
		obj.put("type", this.getType());
		obj.put("modulename", this.getModulename());
		obj.put("funcunits", this.getFuncunits());
		if(roles!=null&&!roles.equals("")){
			obj.put("roles", DictBuffer.getLabel("SYS_ROLE", roles));
		}
		return obj;
	}
	
}
