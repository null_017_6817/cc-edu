package com.qtrmoon.sysmanage.pojo;
import org.json.simple.JSONObject;

import com.qtrmoon.common.PageForm;
	/** 
	 * 功能单元Object类 
	 *
	 */
	public class Functionunit extends PageForm{
	//Fields
	
	private Integer id;// 主键
	private String name;// 功能单元名称
	private String info;// 功能单元说明
	private Integer ord;// 排序
	private String picico;// 功能单元图标
	private String modulename;// 模块类别
	private String funcs;// 
	//Constructors
	/** default constructor */
	public Functionunit() {
	
	}	
	//getter and setter
	/**
	 * 获取主键
	 * @return
	 */
	public Integer getId() {
		return this.id;
	}
	/**
	 * 设置主键
	 * @param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取名称
	 * @return
	 */
	public String getName() {
		return this.name;
	}
	/**
	 * 设置名称
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取描述
	 * @return
	 */
	public String getInfo() {
		return this.info;
	}
	/**
	 * 设置描述
	 * @param info
	 */
	public void setInfo(String info) {
		this.info = info;
	}
	/**
	 * 获取顺序
	 * @return
	 */
	public Integer getOrd() {
		return this.ord;
	}
	/**
	 * 设置顺序
	 * @param ord
	 */
	public void setOrd(Integer ord) {
		this.ord = ord;
	}
	/**
	 * 获取图标
	 * @return
	 */
	public String getPicico() {
		return this.picico;
	}
	/**
	 * 设置图标
	 * @param picico
	 */
	public void setPicico(String picico) {
		this.picico = picico;
	}
	/**
	 * 获取模块名
	 * @return
	 */
	public String getModulename() {
		return this.modulename;
	}
	/**
	 * 设置模块名
	 * @param modulename
	 */
	public void setModulename(String modulename) {
		this.modulename = modulename;
	}
	/**
	 * 获取功能
	 * @return
	 */
	public String getFuncs() {
		return this.funcs;
	}
	/**
	 * 设置功能
	 * @param funcs
	 */
	public void setFuncs(String funcs) {
		this.funcs = funcs;
	}
	/**
	 * 获取json对象
	 * @return
	 */
	public JSONObject getJson() {
		JSONObject obj=new JSONObject();
		obj.put("id", this.getId());
		obj.put("name", this.getName());
		obj.put("info", this.getInfo());
		obj.put("ord", this.getOrd());
		obj.put("picico", this.getPicico());
		obj.put("modulename", this.getModulename());
		obj.put("funcs", this.getFuncs());
		return obj;
	}
}
