package com.qtrmoon.sysmanage.pojo;
import org.json.simple.JSONObject;

import com.qtrmoon.common.PageForm;
	/** 角色Object类 */
	public class Role extends PageForm{
	//Fields
	
	private Integer id;// 主键
	private String name;// 角色名称
	private String islocal;// 是否全局角色
	private String info;// 说明
	private Integer ord;// 排序
	private String classify;// 类别
	private String organid;// 组织机构ID
	private String modulename;// 模块类别
	private String funcs;// 功能集
	//Constructors
	/** default constructor */
	public Role() {
	
	}	
	//getter and setter
	/**
	 * 获取字典对象
	 * @return
	 */
	public JSONObject getJson() {
		JSONObject obj=new JSONObject();
		obj.put("id", this.getId());
		obj.put("name", this.getName());
		obj.put("islocal", this.getIslocal());
		obj.put("info", this.getInfo());
		obj.put("ord", this.getOrd());
		obj.put("classify", this.getClassify());
		obj.put("organid", this.getOrganid());
		obj.put("modulename", this.getModulename());
		obj.put("funcs", this.getFuncs());
		return obj;
	}

	/**
	 * 获取id
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * 设置id
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * 获取name
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * 设置name
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 获取islocal
	 * @return the islocal
	 */
	public String getIslocal() {
		return islocal;
	}

	/**
	 * 设置islocal
	 * @param islocal the islocal to set
	 */
	public void setIslocal(String islocal) {
		this.islocal = islocal;
	}

	/**
	 * 获取info
	 * @return the info
	 */
	public String getInfo() {
		return info;
	}

	/**
	 * 设置info
	 * @param info the info to set
	 */
	public void setInfo(String info) {
		this.info = info;
	}

	/**
	 * 获取ord
	 * @return the ord
	 */
	public Integer getOrd() {
		return ord;
	}

	/**
	 * 设置ord
	 * @param ord the ord to set
	 */
	public void setOrd(Integer ord) {
		this.ord = ord;
	}

	/**
	 * 获取classify
	 * @return the classify
	 */
	public String getClassify() {
		return classify;
	}

	/**
	 * 设置classify
	 * @param classify the classify to set
	 */
	public void setClassify(String classify) {
		this.classify = classify;
	}

	/**
	 * 获取organid
	 * @return the organid
	 */
	public String getOrganid() {
		return organid;
	}

	/**
	 * 设置organid
	 * @param organid the organid to set
	 */
	public void setOrganid(String organid) {
		this.organid = organid;
	}

	/**
	 * 获取modulename
	 * @return the modulename
	 */
	public String getModulename() {
		return modulename;
	}

	/**
	 * 设置modulename
	 * @param modulename the modulename to set
	 */
	public void setModulename(String modulename) {
		this.modulename = modulename;
	}

	/**
	 * 获取funcs
	 * @return the funcs
	 */
	public String getFuncs() {
		return funcs;
	}

	/**
	 * 设置funcs
	 * @param funcs the funcs to set
	 */
	public void setFuncs(String funcs) {
		this.funcs = funcs;
	}
}
