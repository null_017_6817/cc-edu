package com.qtrmoon.sysmanage.pojo;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import com.qtrmoon.toolkit.tree.TreeNode;
	/** 功能Object类 */
	public class Function extends TreeNode{
	//Fields
	
	private Integer id;// 主键
	private String name;// 姓名
	private Integer pid;// 父
	private String link;// 链接
	private String isshow;// 显示 [菜单1 权限2 隐藏权限0]
	private String info;// 说明
	private String picico;// 图标
	private String picimg;// 图片
	private Integer ord;// 排序
	private String treetrack;// 树序
	private String modulename;// 模块
	private String isleaf;// 叶子节点
	private String level;// 叶子节点


	//Constructors
	/** default constructor */
	public Function() {
	
	}	
	//getter and setter
	
	/** 获取主键 */
	public Integer getId() {
		return this.id;
	}
	/**设置主键*/
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取功能名称
	 * @return
	 */
	public String getName() {
		return this.name;
	}
	/**
	 * 设置功能名称
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取父id
	 * @return
	 */
	public Integer getPid() {
		return this.pid;
	}
	/**
	 * 设置父id
	 * @param pid
	 */
	public void setPid(Integer pid) {
		this.pid = pid;
	}
	/**
	 * 获取url
	 * @return
	 */
	public String getLink() {
		return this.link;
	}
	/**
	 * 设置url
	 * @param link
	 */
	public void setLink(String link) {
		this.link = link;
	}
	/**
	 * 获取是否显示
	 * @return
	 */
	public String getIsshow() {
		return this.isshow;
	}
	/**
	 * 设置是否显示
	 * @param isshow
	 */
	public void setIsshow(String isshow) {
		this.isshow = isshow;
	}
	/**
	 * 获取描述
	 * @return
	 */
	public String getInfo() {
		return this.info;
	}
	/**
	 * 设置描述
	 * @param info
	 */
	public void setInfo(String info) {
		this.info = info;
	}
	/**
	 * 获取功能图标
	 * @return
	 */
	public String getPicico() {
		return this.picico;
	}
	/**
	 * 设置功能图标
	 * @param picico
	 */
	public void setPicico(String picico) {
		this.picico = picico;
	}
	/**
	 * 获取图片
	 * @return
	 */
	public String getPicimg() {
		return this.picimg;
	}
	/**
	 * 设置图片
	 * @param picimg
	 */
	public void setPicimg(String picimg) {
		this.picimg = picimg;
	}
	/**
	 * 获取顺序
	 * @return
	 */
	public Integer getOrd() {
		return this.ord;
	}
	/**
	 * 设置顺序
	 * @param ord
	 */
	public void setOrd(Integer ord) {
		this.ord = ord;
	}
	/**
	 * 获取树结构
	 * @return
	 */
	public String getTreetrack() {
		return this.treetrack;
	}
	/**
	 * 设置树结构
	 * @param treetrack
	 */
	public void setTreetrack(String treetrack) {
		this.treetrack = treetrack;
	}
	/**
	 * 获取模块名
	 * @return
	 */
	public String getModulename() {
		return this.modulename;
	}
	/**
	 * 设置模块名
	 * @param modulename
	 */
	public void setModulename(String modulename) {
		this.modulename = modulename;
	}
	/**
	 * 获取json对象
	 * @return
	 */
	public JSONObject getJson() {
		JSONObject obj=new JSONObject();
		obj.put("id", this.getId());
		obj.put("name", this.getName());
		obj.put("pid", this.getPid());
		obj.put("link", this.getLink());
		obj.put("isshow", this.getIsshow());
		obj.put("info", this.getInfo());
		obj.put("picico", this.getPicico());
		obj.put("picimg", this.getPicimg());
		obj.put("ord", this.getOrd());
		obj.put("treetrack", this.getTreetrack());
		obj.put("modulename", this.getModulename());
		obj.put("isleaf", this.getIsleaf());
		obj.put("level", this.getLevel());
		return obj;
	}
	/**
	 * 获取某些字段json对象
	 * @return
	 */
	private JSONObject getJsonSimple() {
		JSONObject obj=new JSONObject();
		obj.put("id", this.getId());
		obj.put("name", this.getName());
		obj.put("pid", this.getPid());
		obj.put("link", this.getLink());
		obj.put("isshow", this.getIsshow());
		obj.put("picico", this.getPicico());
		obj.put("ord", this.getOrd());
		obj.put("isleaf", this.getIsleaf());
		obj.put("level", this.getLevel());
		return obj;
	}
	/**
	 * 获取菜单json
	 * @return
	 */
	public JSONArray getMenuJson() {
		JSONArray funcs=new JSONArray();
		for(int i=0;i<this.getCnodeList().size();i++){
			Function fun=(Function)this.getCnodeList().get(i);//获取子对象
			JSONObject funjson=fun.getJsonSimple();//子对象转json对象
			funjson.put("children", fun.getMenuJson());//放入子集
			funcs.add(funjson);//填充集合
		}
		return funcs;
	}
	/**
	 * 获取树id
	 */
	@Override
	public String getTreeId() {
		return id.toString();
	}
	/**
	 * 获取树父id
	 */
	@Override
	public String getTreePId() {
		if(pid==null)return "-1";
		return pid.toString();
	}
	/**
	 * 克隆对象
	 */
	public Function clone() {
		Function fun=new Function();
		fun.setId(id);
		fun.setName(name);
		fun.setPid(pid);
		fun.setLink(link);
		fun.setIsshow(isshow);
		fun.setPicico(picico);
		fun.setOrd(ord);
		fun.setIsleaf(isleaf);
		fun.setLevel(level);
		return fun;
	}

	public String getIsleaf() {
		return isleaf;
	}

	public void setIsleaf(String isleaf) {
		this.isleaf = isleaf;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}
}
