package com.qtrmoon.sysmanage.serdao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.qtrmoon.sysmanage.pojo.Function;
/**
 * 功能类映射层
 *
 */
@Component
public interface FunctionMapper {
	/**
	 * Function的条件查询方法
	 * @param function 承载查询条件
	 * @return 返回Function的集合
	 */
	public List<Function> schFunction(Function function);
	
	/**根据id查询功能
	 * @param id
	 * @return
	 */
	public Function schFunctionById(Integer id);
	
	/**
	 * 功能的in查询
	 * @param ids
	 * @return
	 */
	public List<Function> schHiddenFunction(Integer[] ids);
	/**
	 * 添加功能
	 * @param function
	 * @return
	 */
	public Integer addFunction(Function function);
	
	/**
	 * 更新功能
	 * @param function
	 */
	public void updFunction(Function function);
	
	/**
	 * 删除功能
	 * @param id
	 */
	public void delFunction(Integer id);
	
	/**
	 * 批量删除功能
	 * @param ids
	 */
	public void delFunctions(Integer[] ids);
}
