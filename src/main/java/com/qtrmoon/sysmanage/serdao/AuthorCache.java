package com.qtrmoon.sysmanage.serdao;

import java.util.ArrayList;
import java.util.List;

import com.qtrmoon.manager.pojo.Person;
import com.qtrmoon.sysmanage.SysConstant;
import com.qtrmoon.sysmanage.pojo.Function;
import com.qtrmoon.sysmanage.pojo.Module;
import com.qtrmoon.sysmanage.pojo.Role;
import com.qtrmoon.sysmanage.pojo.User;
import com.qtrmoon.toolkit.tree.TreeNode;
import com.qtrmoon.toolkit.tree.TreeUtil;
/**
 * 权限缓存
 * @author HuaTang_DYC
 *
 */
public class AuthorCache {
	private static AuthorCache aUtil;//自引用
	private static List<Module> moduleList;//模块集合
	private static List<Role> roleList;//角色集合
	private static List<Function>funcList;//功能集合
	
	private AuthorCache(){}
	//初始化权限缓存
	public static AuthorCache ins(){
		if(aUtil==null){
			aUtil=new AuthorCache();//创建缓存
		}
		return aUtil;
	}
	
	/**
	 * 获取用户的合法权限集合，构造专属菜单。
	 * @param id
	 * @return
	 */
	public void setUserAuthor(Person user) {
		if(user==null||user.getRoles()==null)return;
		String roles=user.getRoles();//获取用户角色并格式化。
		roles=","+roles.trim().replaceAll(" ", "")+",";
		String funcstr="";//获取用户功能集主键串
		for(Role role:roleList){
			if(roles.indexOf(","+role.getId()+",")>=0){
				funcstr+=role.getFuncs()+",";//工能串
			}
		}
		if(funcstr.length()>0){//功能集主键串格式化
			funcstr=funcstr.substring(0,funcstr.length()-1);//去逗号
			funcstr=","+funcstr.trim().replaceAll(" ", "")+",";//去空格
		}
		List<Function> flist=new ArrayList<Function>();//用户功能权限集合
		for(Function fun:funcList){
			if(funcstr.indexOf(","+fun.getId()+",")>=0&&!flist.contains(fun)){//去重
				flist.add(fun);//添加功能
			}
		}
		//用户构造带根节点的功能权限集合，从缓存中重新clone，重组树结构。
		List<Function> newflist=new ArrayList<Function>();
		for(Function fun:flist){
			trace(fun,newflist);//拍讯
		}
		List<Function> menuList=new ArrayList<Function>();//用户菜单集合，构造菜单树
		for (Function fun:newflist){
			if (fun.getIsshow().equals(SysConstant.FUNCTION_SHOW)) {//显示
				menuList.add(fun);//添加功能
			}
		}
		new FunctionTreeUtil().getOrderTree(menuList);//获取层次树
		user.setMenu(newflist);//填充菜单
	}
	/**
	 * 根据子节点添加所有的父节点
	 * @param fun
	 * @param newflist
	 */
	private void trace(Function fun, List<Function> newflist) {
		if(fun==null)return;//无功能
		if(!hasAdd(fun.getId(),newflist)){
			newflist.add(fun.clone());//填充list
		}
		trace((Function)fun.getPNode(),newflist);//排序	
	}

	/**
	 * 是否添加过
	 * @param id
	 * @return
	 */
	private boolean hasAdd(Integer id,List<Function> newflist) {
		for(Function fun:newflist){//遍历
			if(fun.getId().intValue()==id){//已添加
				return true;//返回真
			}
		}
		return false;//返回假
	}

	/**
	 * 获取用户持有的模块。
	 * @param user
	 * @return
	 */
	public List<Module> getUserModule(User user){
		List<Module> nlist=new ArrayList<Module>();//创建list
		List<Function> flist=user.getMenu();//获取功能
		for(Module m:moduleList){//遍历
			for(Function f:flist){//遍历
				if(m.getFuncIndex()!=null&&m.getFuncIndex().indexOf(","+f.getId()+",")>=0){//模块功能
					nlist.add(m);//添加功能
					break;//中断
				}
			}
		}
		return nlist;//返回list
	}
	
	/**
	 * 获取用户的菜单
	 * @param user 用户带权限
	 * @param moduleid 模块ID
	 * @return 用户某模块下的功能菜单
	 */
	public List<Function> getUserMenu(User user ,int moduleid){
		String funcIndex="";//空字符串
		for(Module m:moduleList){//遍历
			if(moduleid==m.getId()){//获取模块id
				funcIndex=m.getFuncIndex();//获取模块功能串
			}
		}
		List<Function> funcList=user.getMenu();//获取功能
		List<Function> flist=new ArrayList<Function>();//创建用户list
		for(Function fun:funcList){//遍历
			if(funcIndex.indexOf(","+fun.getId()+",")>=0){//包含
				flist.add(fun);//添加功能
			}
		}
		return flist;//返回list
	}
	
	
	/**
	 * 获取模块下的主菜单
	 * @param id 模块id
	 * @return
	 */
	public List<Function> schMenuByModule(int id) {
		String funcIndex="";//功能串
		for(Module m:moduleList){//遍历
			if(id==m.getId()){//相等
				funcIndex=m.getFuncIndex();//获取功能串
			}
		}
		List<Function> flist=new ArrayList<Function>();//获取list
		for(Function fun:funcList){//遍历
			if(funcIndex.indexOf(","+fun.getId()+",")>=0){//包含菜单
				flist.add(fun);//添加
			}
		}
		return flist;//返回list
	}
	/**
	 * 初始化权限缓存集合
	 * @param mList 模块集合
	 * @param rlist 角色集合
	 * @param flist 功能集合
	 */
	public void init(List<Module> mList,List<Role> rlist,List<Function>flist){
		if(mList!=null){//模块不为空
			moduleList=mList;//赋值
			for(Module m:moduleList){//遍历
				String funcs=m.getFuncs();//获取模块功能
				if(funcs==null)continue;//跳过继续下次
				funcs=funcs.trim();//去空格
				funcs=","+funcs+",";//拼接字符串
				m.setFuncIndex(funcs);//设置功能串
			}
		}
		if(rlist!=null)roleList=rlist;//赋值角色
		if(flist!=null){
			funcList=flist;//赋值功能
			new FunctionTreeUtil().getOrderTree(funcList);//排序
		}
	}

	/**
	 * 是否需要调用init初始化权限缓存集合
	 * @return
	 */
	public boolean empty() {
		return roleList==null||funcList==null;
	}
	
	/**
	 * 清空权限缓存
	 * 添加和修改角色时调用了此方法。
	 */
	public void clear(){
		roleList=null;//清空角色
		funcList=null;//清空功能
	}

}
/**
 * 功能树工具类
 *
 */
class FunctionTreeUtil extends TreeUtil{
	/**
	 * 层次
	 */
	@Override
	public String render(TreeNode node) {
		return null;//返回空
	}
}