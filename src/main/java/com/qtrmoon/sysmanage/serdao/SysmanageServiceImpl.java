package com.qtrmoon.sysmanage.serdao;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.qtrmoon.manager.pojo.Person;
import com.qtrmoon.sysmanage.pojo.Function;
import com.qtrmoon.sysmanage.pojo.Functionunit;
import com.qtrmoon.sysmanage.pojo.Module;
import com.qtrmoon.sysmanage.pojo.Organ;
import com.qtrmoon.sysmanage.pojo.Role;
import com.qtrmoon.sysmanage.pojo.User;
/**
 * 系统类业务层
 *
 */
@Service("sysmanageService")
public class SysmanageServiceImpl implements ISysmanageService {
	@Autowired
	private FunctionMapper functionMapper;//声明mapper

	@Autowired
	private FunctionunitMapper functionunitMapper;//声明mapper

	@Autowired
	private OrganMapper organMapper;//声明mapper

	@Autowired
	private RoleMapper roleMapper;//声明mapper

	@Autowired
	private UserMapper userMapper;//声明mapper
	
	@Autowired
	private ModuleMapper moduleMapper;//声明mapper

	/**
	 * 查询功能
	 */
	@Override
	public List<Function> schFunction(Function function) {
		if(function.getPagesize()>0){
			PageHelper.startPage(function.getPage(), function.getPagesize());//设置分页
		}
		List<Function> list=functionMapper.schFunction(function);//查询功能列表
		if(function.getPagesize()>0){//页码大于0
			Page<Function> page = (Page<Function>)list;//强转分页对象
			function.setDatasize(page.getTotal());//设置总页数
		}
		return list;//返回list
	}
	/**
	 * 根据id查询功能
	 */
	@Override
	public Function schFunctionById(Integer id) {
		return functionMapper.schFunctionById(id);
	}
	/**
	 * 添加功能
	 */
	@Override
	public Integer addFunction(Function function) {
		return functionMapper.addFunction(function);
	}
	/**
	 * 更新功能
	 */
	@Override
	public void updFunction(Function function) {
		functionMapper.updFunction(function);
	}
	/**
	 * 删除功能
	 */
	@Override
	public void delFunction(Integer id) {
		functionMapper.delFunction(id);
	}
	/**
	 * 批量删除功能
	 */
	@Override
	public void delFunction(Integer[] ids) {
		functionMapper.delFunctions(ids);
	}

	/**
	 * 查询功能单元
	 */
	@Override
	public List<Functionunit> schFunctionunit(Functionunit functionunit) {
		PageHelper.startPage(functionunit.getPage(), functionunit.getPagesize());//设置分页
		List<Functionunit> list=functionunitMapper.schFunctionunit(functionunit);//查询功能列表
		Page<Functionunit> page = (Page<Functionunit>)list;//强转分页对象
		functionunit.setDatasize(page.getTotal());//设置总页数
		return list;//返回list
	}
	/**
	 * 根据id查询功能单元
	 */
	@Override
	public Functionunit schFunctionunitById(Integer id) {
		return functionunitMapper.schFunctionunitById(id);
	}
	/**
	 * 添加功能单元
	 */
	@Override
	public Integer addFunctionunit(Functionunit functionunit) {
		return functionunitMapper.addFunctionunit(functionunit);
	}
	/**
	 * 更新功能单元
	 */
	@Override
	public void updFunctionunit(Functionunit functionunit) {
		functionunitMapper.updFunctionunit(functionunit);
	}
	/**
	 * 删除功能单元
	 * @param id
	 */
	@Override
	public void delFunctionunit(Integer id) {
		functionunitMapper.delFunctionunit(id);
	}
	/**
	 * 批量删除功能单元
	 * @param ids
	 */
	@Override
	public void delFunctionunit(Integer[] ids) {
		functionunitMapper.delFunctionunits(ids);
	}

	/**
	 * 查询机构
	 * @param organ
	 * @return
	 */
	@Override
	public List<Organ> schOrgan(Organ organ) {
		PageHelper.startPage(organ.getPage(), organ.getPagesize());//设置分页
		List<Organ> list=organMapper.schOrgan(organ);//查询机构列表
		Page<Organ> page = (Page<Organ>)list;//强转分页对象
		organ.setDatasize(page.getTotal());//设置总页数
		return list;//返回list
	}
	/**
	 * 根据id查询机构
	 * @param id
	 * @return
	 */
	@Override
	public Organ schOrganById(Integer id) {
		return organMapper.schOrganById(id);
	}
	/**
	 * 添加机构
	 * @param organ
	 * @return
	 */
	@Override
	public Integer addOrgan(Organ organ) {
		return organMapper.addOrgan(organ);
	}
	/**
	 * 更新机构
	 * @param organ
	 */
	@Override
	public void updOrgan(Organ organ) {
		organMapper.updOrgan(organ);
	}
	/**
	 * 根据id删除机构
	 * @param id
	 */
	@Override
	public void delOrgan(Integer id) {
		organMapper.delOrgan(id);
	}
	/**
	 * 批量删除机构
	 * @param ids
	 */
	@Override
	public void delOrgan(Integer[] ids) {
		organMapper.delOrgans(ids);
	}

	/**
	 * 查询角色
	 * @param role
	 * @return
	 */
	@Override
	public List<Role> schRole(Role role) {
		if(role.getPagesize()>0){
			PageHelper.startPage(role.getPage(), role.getPagesize());//设置分页
		}
		List<Role> list=roleMapper.schRole(role);//查询角色列表
		if(role.getPagesize()>0){
			Page<Role> page = (Page<Role>)list;//强转分页对象
			role.setDatasize(page.getTotal());//设置总页数
		}else{
			role.setDatasize(list.size());//设置总页数
		}
		return list;//返回list
	}
	/**
	 * 根据id查询角色
	 * @param id
	 * @return
	 */
	@Override
	public Role schRoleById(Integer id) {
		return roleMapper.schRoleById(id);
	}
	/**
	 * 添加角色
	 * @param role
	 * @return
	 */
	@Override
	public Integer addRole(Role role) {
		AuthorCache.ins().clear();//清空缓存
		return roleMapper.addRole(role);
	}
	/**
	 * 更新角色
	 * @param role
	 */
	@Override
	public void updRole(Role role) {
		AuthorCache.ins().clear();//清空缓存
		Integer[] ids=toIntarr(role.getFuncs());//数字数组
		if(ids!=null){
			List<Function> flist=functionMapper.schHiddenFunction(ids);//获取功能列表
			String res="";//资源串
			for(Function f:flist){//遍历
				res+=","+f.getId();//获取资源串
			}
			if(res.length()>0)res=res.substring(0,res.length()-1);//去逗号
			role.setFuncs(role.getFuncs()+res);//设置资源
		}
		roleMapper.updRole(role);//更新角色
	}
	/**
	 * 字符串转int数组
	 * @param fstr
	 * @return
	 */
	private Integer[] toIntarr(String fstr){
		Integer ids[]=null;//int数组
		if(fstr!=null&&fstr.length()>0){//不为空
			String[] farr=fstr.split(",");//字符串数组
			ids=new Integer[farr.length];//int数组
			for(int i=0;i<farr.length;i++){//遍历
				ids[i]=Integer.parseInt(farr[i]);//填充数组
			}
		}
		return ids;//返回数组
	}
	/**
	 * 删除角色
	 * @param id
	 */
	@Override
	public void delRole(Integer id) {
		roleMapper.delRole(id);
	}
	/**
	 * 批量删除角色
	 * @param ids
	 */
	@Override
	public void delRole(Integer[] ids) {
		roleMapper.delRoles(ids);
	}
	
	/**
	 * 查询用户
	 */
	@Override
	public List<User> schUser(User user) {
		PageHelper.startPage(user.getPage(), user.getPagesize());//设置分页
		List<User> list=userMapper.schUser(user);//查询用户
		Page<User> page = (Page<User>)list;//转分页
		user.setDatasize(page.getTotal());//设置总页
		return list;//返回list
	}
	
	/**
	 * 查询用户（无分页）
	 */
	@Override
	public List<User> schUserList(User user) {
		List<User> list=userMapper.schUser(user);//查询用户
		return list;//返回list
	}
	
	/**
	 * 根据id查询用户
	 */
	@Override
	public User schUserById(Integer id) {
		return userMapper.schUserById(id);
	}
	
	/**
	 * 根据学号/工号查询用户
	 */
	@Override
	public User schUserByNumber(String number) {
		return userMapper.schUserByNumber(number);
	}
	
	/**
	 * 添加用户
	 */
	@Override
	public Integer addUser(User user) {
		return userMapper.addUser(user);
	}
	/**
	 * 更新用户
	 */
	@Override
	public void updUser(User user) {
		userMapper.updUser(user);
	}
	/**
	 * 删除用户
	 */
	@Override
	public void delUser(Integer id) {
		userMapper.delUser(id);
	}
	/**
	 * 批量删除用户
	 */
	@Override
	public void delUser(Integer[] ids) {
		userMapper.delUsers(ids);
	}

	/***************** Module方法组 *****************/
	/**
	 * Module的条件查询方法
	 * @param module 承载查询条件的Bean
	 * @return 返回Module的集合
	 */
	@Override
	public List<Module> schModule(Module module) {
		if(module.getPagesize()>0){
			PageHelper.startPage(module.getPage(), module.getPagesize());//设置分页
		}
		List<Module> list=moduleMapper.schModule(module);//查询模块
		if(module.getPagesize()>0){
			Page<Module> page = (Page<Module>)list;//转分页
			module.setDatasize(page.getTotal());//设置总页
		}else{
			module.setDatasize(list.size());//设置总页
		}
		return list;//返回list
	}
	
	/**
	 * Module的主键查询方法
	 * @param id 主键值
	 * @return 返回Module实体
	 */
	@Override
	public Module schModuleById(Integer id) {
		return moduleMapper.schModuleById(id);
	}
	
	/**
	 * Module的添加方法
	 * @param module 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	@Override
	public Integer addModule(Module module) {
		return moduleMapper.addModule(module);
	}

	/**
	 * Module的修改方法
	 * @param module 承载数据的Bean
	 */
	@Override
	public void updModule(Module module) {
		moduleMapper.updModule(module);
	}

	/**
	 * Module的单记录删除方法
	 * @param id 要删除的主键值
	 */
	@Override
	public void delModule(Integer id) {
		moduleMapper.delModule(id);
	}

	/**
	 * Module的批量删除方法
	 * @param ids 主键值的数组
	 */
	@Override
	public void delModule(Integer[] ids) {
		moduleMapper.delModules(ids);
	}
	/**
	 * 设置用户权限
	 */
	@Override
	public void setUserAuthor(Person loginUser) {
		AuthorCache auth = AuthorCache.ins();//初始化缓存
		if(auth.empty()){
			Role r=new Role();
			r.setPagesize(-1);//设置查询数量
			
			Function f=new Function();
			f.setPagesize(-1);//设置查询数量
			
			Module m=new Module();
			m.setPagesize(-1);//设置查询数量
			
			auth.init(schModule(m),schRole(r), schFunction(f));//初始化权限
		}
		AuthorCache.ins().setUserAuthor(loginUser);//设置用户权限
	}

}
