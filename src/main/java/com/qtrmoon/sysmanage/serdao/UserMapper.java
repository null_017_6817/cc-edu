package com.qtrmoon.sysmanage.serdao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.qtrmoon.sysmanage.pojo.User;
/**
 * 用户类持久层
 *
 */
@Component
public interface UserMapper {
	/**
	 * 查询用户
	 * @param user
	 * @return
	 */
	public List<User> schUser(User user);
	/**
	 * 根据id查询用户
	 * @param id
	 * @return
	 */
	public User schUserById(Integer id);
	/**
	 * 添加用户
	 * @param user
	 * @return
	 */
	public Integer addUser(User user);
	/**
	 * 更新用户
	 * @param user
	 */
	public void updUser(User user);
	/**
	 * 删除用户
	 * @param id
	 */
	public void delUser(Integer id);
	/**
	 * 批量删除用户
	 * @param ids
	 */
	public void delUsers(Integer[] ids);
	/**
	 * 根据sql查询用户
	 * @param sql
	 * @return
	 */
	public List<User> query(String sql);
	
	/**
	 * 根据学号/工号 查询用户
	 * @param sql
	 * @return
	 */
	public User schUserByNumber(String number);
	
}
