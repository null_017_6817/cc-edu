package com.qtrmoon.sysmanage.serdao;

import java.util.List;
import org.springframework.stereotype.Component;

import com.qtrmoon.sysmanage.pojo.Organ;
/**
 * 机构类持久层
 *
 */
@Component
public interface OrganMapper {
	/**
	 * 查询机构
	 * @param organ
	 * @return
	 */
	public List<Organ> schOrgan(Organ organ);
	/**
	 * 根据id查询机构
	 * @param id
	 * @return
	 */
	public Organ schOrganById(Integer id);
	/**
	 * 添加机构
	 * @param organ
	 * @return
	 */
	public Integer addOrgan(Organ organ);
	/**
	 * 更新机构
	 * @param organ
	 */
	public void updOrgan(Organ organ);
	/**
	 * 根据id删除机构
	 * @param id
	 */
	public void delOrgan(Integer id);
	/**
	 * 批量删除机构
	 * @param ids
	 */
	public void delOrgans(Integer[] ids);
	/**
	 * 根据sql查询机构
	 * @param sql
	 * @return
	 */
	public List<Organ> query(String sql);
	
	/**
	 * 根据id查询子部门
	 * @param 父部门id
	 * @return
	 */
	public List<Organ> schChildById(Integer id); 
}
