package com.qtrmoon.sysmanage.serdao;
import java.util.List;

import com.qtrmoon.manager.pojo.Person;
import com.qtrmoon.sysmanage.pojo.Function;
import com.qtrmoon.sysmanage.pojo.Functionunit;
import com.qtrmoon.sysmanage.pojo.Module;
import com.qtrmoon.sysmanage.pojo.Organ;
import com.qtrmoon.sysmanage.pojo.Role;
import com.qtrmoon.sysmanage.pojo.User;
/**
 * 系统业务层接口
 *
 */
public interface ISysmanageService {
	/**
	 * Function的条件查询方法
	 * @param function 承载查询条件
	 * @return 返回Function的集合
	 */
	public List<Function> schFunction(Function function);
	/**
	 * Function的主键查询方法
	 * @param id 主键值
	 * @return 返回Function实体
	 */
	public Function schFunctionById(Integer id);
	/**
	 * Function的添加方法
	 * @param function 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addFunction(Function function);
	/**
	 * Function的修改方法
	 * @param function 承载数据的Bean
	 */
	public void updFunction(Function function);
	/**
	 * Function的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delFunction(Integer id);
	/**
	 * Function的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delFunction(Integer[] ids);
	/**
	 * 查询功能单元
	 * @param functionunit
	 * @return
	 */
	public List<Functionunit> schFunctionunit(Functionunit functionunit);
	/**
	 * 根据id查询功能单元
	 * @param id
	 * @return
	 */
	public Functionunit schFunctionunitById(Integer id);
	/**
	 * 添加功能单元
	 * @param functionunit
	 * @return
	 */
	public Integer addFunctionunit(Functionunit functionunit);
	/**
	 * 更新功能单元
	 * @param functionunit
	 */
	public void updFunctionunit(Functionunit functionunit);
	/**
	 * 删除功能单元
	 * @param id
	 */
	public void delFunctionunit(Integer id);
	/**
	 * 批量删除功能单元
	 * @param ids
	 */
	public void delFunctionunit(Integer[] ids);
	/**
	 * 查询机构
	 * @param organ
	 * @return
	 */
	public List<Organ> schOrgan(Organ organ);
	/**
	 * 根据id查询机构
	 * @param id
	 * @return
	 */
	public Organ schOrganById(Integer id);
	/**
	 * 添加机构
	 * @param organ
	 * @return
	 */
	public Integer addOrgan(Organ organ);
	/**
	 * 更新机构
	 * @param organ
	 */
	public void updOrgan(Organ organ);
	/**
	 * 删除机构
	 * @param id
	 */
	public void delOrgan(Integer id);
	/**
	 * 批量删除机构
	 * @param ids
	 */
	public void delOrgan(Integer[] ids);
	/**
	 * 查询角色
	 * @param role
	 * @return
	 */
	public List<Role> schRole(Role role);
	/**
	 * 根据id查询角色
	 * @param id
	 * @return
	 */
	public Role schRoleById(Integer id);
	/**
	 * 添加角色
	 * @param role
	 * @return
	 */
	public Integer addRole(Role role);
	/**
	 * 更新角色
	 * @param role
	 */
	public void updRole(Role role);
	/**
	 * 删除角色
	 * @param id
	 */
	public void delRole(Integer id);
	/**
	 * 批量删除角色
	 * @param ids
	 */
	public void delRole(Integer[] ids);
	/**
	 * 查询用户
	 * @param user
	 * @return
	 */
	public List<User> schUser(User user);
	
	/**
	 * 查询用户（无分页）
	 * @param user
	 * @return
	 */
	public List<User> schUserList(User user);
	
	/**
	 * 根据id查询用户
	 * @param id
	 * @return
	 */
	public User schUserById(Integer id);
	/**
	 * 添加用户
	 * @param user
	 * @return
	 */
	public Integer addUser(User user);
	/**
	 * 更新用户
	 * @param user
	 */
	public void updUser(User user);
	/**
	 * 删除用户
	 * @param id
	 */
	public void delUser(Integer id);
	/**
	 * 批量删除用户
	 * @param ids
	 */
	public void delUser(Integer[] ids);
	
	/***************** Module方法组 *****************/
	/**
	 * Module的条件查询方法
	 * @param module 承载查询条件的Bean
	 * @return 返回Module的集合
	 */
	public List<Module> schModule(Module module);
	
	/**
	 * Module的主键查询方法
	 * @param id 主键值
	 * @return 返回Module实体
	 */
	public Module schModuleById(Integer id);
	
	/**
	 * Module的添加方法
	 * @param module 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addModule(Module module);
	
	/**
	 * Module的修改方法
	 * @param module 承载数据的Bean
	 */
	public void updModule(Module module);
	
	/**
	 * Module的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delModule(Integer id);
	
	/**
	 * Module的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delModule(Integer[] ids);

	/**
	 * 为用户分配权限集合
	 * @param loginUser
	 * @return
	 */
	public void setUserAuthor(Person loginUser);
	
	/**
	 * 根据学号/工号查询用户
	 */
	public User schUserByNumber(String number);
}
