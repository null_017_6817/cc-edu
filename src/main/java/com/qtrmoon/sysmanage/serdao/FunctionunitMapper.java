package com.qtrmoon.sysmanage.serdao;

import java.util.List;
import org.springframework.stereotype.Component;

import com.qtrmoon.sysmanage.pojo.Functionunit;
/**
 * 功能单元类映射层
 *
 */
@Component
public interface FunctionunitMapper {
	/**
	 * 查询功能单元
	 * @param functionunit
	 * @return
	 */
	public List<Functionunit> schFunctionunit(Functionunit functionunit);
	/**
	 * 根据id查询功能单元
	 * @param id
	 * @return
	 */
	public Functionunit schFunctionunitById(Integer id);
	/**
	 * 添加功能单元
	 * @param functionunit
	 * @return
	 */
	public Integer addFunctionunit(Functionunit functionunit);
	/**
	 * 更新功能单元
	 * @param functionunit
	 */
	public void updFunctionunit(Functionunit functionunit);
	/**
	 * 删除功能单元
	 * @param id
	 */
	public void delFunctionunit(Integer id);
	/**
	 * 批量删除功能单元
	 * @param ids
	 */
	public void delFunctionunits(Integer[] ids);
	/**
	 * 查询功能单元
	 * @param sql
	 * @return
	 */
	public List<Functionunit> query(String sql);
}
