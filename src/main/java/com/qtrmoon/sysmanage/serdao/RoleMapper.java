package com.qtrmoon.sysmanage.serdao;

import java.util.List;
import org.springframework.stereotype.Component;

import com.qtrmoon.sysmanage.pojo.Role;
/**
 * 角色类持久层
 *
 */
@Component
public interface RoleMapper {
	/**
	 * 查询角色
	 * @param role
	 * @return
	 */
	public List<Role> schRole(Role role);
	/**
	 * 根据id查询角色
	 * @param id
	 * @return
	 */
	public Role schRoleById(Integer id);
	/**
	 * 添加角色
	 * @param role
	 * @return
	 */
	public Integer addRole(Role role);
	/**
	 * 更新角色
	 * @param role
	 */
	public void updRole(Role role);
	/**
	 * 删除角色
	 * @param id
	 */
	public void delRole(Integer id);
	/**
	 * 批量删除角色
	 * @param ids
	 */
	public void delRoles(Integer[] ids);
	/**
	 * 根据sql查询角色
	 * @param sql
	 * @return
	 */
	public List<Role> query(String sql);
}
