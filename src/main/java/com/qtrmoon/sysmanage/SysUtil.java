package com.qtrmoon.sysmanage;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.qtrmoon.manager.pojo.Person;
import com.qtrmoon.sysmanage.pojo.Organ;
import com.qtrmoon.sysmanage.pojo.User;
/**
 * 系统工具类
 * @author HuaTang_DYC
 *
 */
public class SysUtil {
	/**
	 * 获取当前登录用户
	 * 
	 * @param request
	 * @return
	 */
	public static Person getCurrentUser(HttpServletRequest request) {
		HttpSession session = request.getSession();//获取session
		Person userForm = (Person) session.getAttribute(SysConstant.CURRENT_USER);//获取当前登录人
		return userForm;//返回登录人
	}
	
	/**
	 * 获取当前登录机构
	 * 
	 * @param request
	 * @return
	 */
	public static Organ getCurrentOrgan(HttpServletRequest request) {
		return getCurrentUser(request).getOrgan();//返回机构
	}
}
