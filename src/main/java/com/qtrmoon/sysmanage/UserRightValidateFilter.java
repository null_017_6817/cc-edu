package com.qtrmoon.sysmanage;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.qtrmoon.common.Constant;
import com.qtrmoon.manager.pojo.Person;
import com.qtrmoon.manager.serdao.IManagerService;
import com.qtrmoon.sysmanage.pojo.Function;
/**
 * 过滤器
 *
 */
@WebFilter(filterName="userRightValidateFilter",urlPatterns="/*")
public class UserRightValidateFilter implements Filter {
	
	@Autowired
	private IManagerService imanagerService;
	/**
	 * 绿色地址
	 */
    private static final String[] letitgo=new String[]{
        "/",
        "/dict",
        "/index.jsp",
        "/main.jsp",
        "/manage.jsp",
        "/book/index.jsp",
        
        "/common/error_illegal.jsp",
        "/common/error_timeout.jsp",
        "/common/error.jsp",
        "/book/book/schBookForMain.action",
        "/book/book/appendHtmlUrl.action",
        "/sysmanage/login/login.action",
        "/sysmanage/login/bookLogin.action",
        "/sysmanage/logout/bookLogout.action",
        "/sysmanage/login/logout.action",
        "/sysmanage/changePw/changePw.action",
        "/sysmanage/login/page.action",
        /*"/sysmanage/module/schMenuByModule.action",
        
        "/manager/course/schCourseByBookId.action",
        
        "/examination/exerTopic/getTopicsByBookId.action",
        "/examination/exerTopic/getExerStepByEid.action",
        "/examination/exerTopic/getExerStepByBook.action",
        "/examination/exerTopic/getTopicsNotCampByBookId.action",
        "/examination/exerTopic/page.action",
        
        "/examination/exerStep/getStepByTopicId.action", 
        "/examination/exerStep/saveStep.action",
        "/examination/exerStep/schExerStepById.action",
        
        "/websocket",
        
        "/examination/exerAnswer/schExerAnswerByEtsId.action",
        "/examination/exerAnswer/schExerAnswerByEtssId.action",
        "/examination/exerAnswer/updExerAnswerByEtsId",
        "/examination/exerAnswer/hasAnswer.action",
        "/examination/exerAnswer/delAnswer.action",
        
        "/examination/automark/autoMark.action",
        
        "/examination/exer/schExerAndStatus.action",
        "/examination/exer/schExerById.action",
        
        "/examination/exerStep/uploadVoice.action",
        
        "/manager/repository/schRepositoryByCutOrTitle.action",
        "/manager/repository/schRepository.action",
        "/manager/repository/appendUrl.action",
        
        "/manager/course/appendUrl.action",
        
        "/paper/question/appendUrl.action",
        "/paper/question/appendHtmlUrl.action",
        
        "/practical/workorder/*",
        "/examination/doexer.jsp",
        "/book/*",*/
        "/manager/stud/*",
        "/dialog/*",
        "/export/*",
        
        "/slwldx/*",
        "/camp/*",
        "*jquery.min.map",
        "*.mp4",
        "*.jpg",
        "*.gif",
        "*.png",
        "*.js",
        "*.woff",
        "*.css",
        "*.swf",
        "*.html",
        "*.wav",
        "*.pdf"
    };
    private static Pattern[] ltgp;//模板
    
    // 考试链接
	private static final String[] examLink = new String[]{
		"/",
		"/book/index.jsp",
		"/book/book/schBookForMain.action",
		"/logout/bookLogout.action",
		"sysmanage/changePw/changePw.action",
		"sysmanage/login/bookLogin.action",
		"/book/book/page.action?page=catalog",
		
		"/book/catalog.jsp",
		"/examination/exer/schExerAndStatus.action",
		"/examination/exerTopic/page.action?page=exerTopicExam_vie",
		"/examination/exerTopicExam_vie.jsp",
		"/examination/exer/schExerById.action",
		"/examination/exerTopic/getExerStepByEid.action",
		"/examination/exerTopic/getExerStepByBook.action",
		"/book/book/getCurrentUser.action",
		"/examination/exerTopic/page.action",
		"/examination/doexer.jsp",
		"/examination/exerStep/schExerStepById.action",
		"/examination/exerAnswer/schExerAnswerByEtsId.action",
		
		"*.jpg",
		"*.gif",
		"*.png",
		"*.js",
		"*.woff",
		"*.css",
		"*.html",
		"*.wav",
	};
	private static Pattern[] examGo;//模板
	
	//实训营地链接
	private static final String[] sxydLink = new String[]{
		"/examination/exerTopic/getTopicsByBookId.action",
		"/examination/exerStep/saveStep.action",
		"/examination/exerStep/saveStep.action",
		"/camp/trainCamp/getRecords.action",
		"/camp/trainCamp/page.action?page=xspj",
		"/dialog/page.action?page=listen",
		"/camp/trainCamp/page.action?page=teacher_sch",
		"/book/book/getCurrentUser.action",
		"/practical/workorder/serchCusOrder.action",
		"/practical/workorder/saveClient.action",
		"/practical/workorder/preCall.action",
		"/practical/workorder/serSearchCus.action",
		"/practical/workorder/serchBill.action",
		"/practical/workorder/serchBillById.action",
		"/practical/workorder/changeLevel.action",
		"/practical/workorder/saveBill.action",
		"/practical/workorder/preHungUp.action"
	};
	private static Pattern[] sxydGo;//模板
	

    private static Map<Integer,Pattern> cache=new HashMap<Integer,Pattern>();//创建map
    /**
     * 摧毁
     */
    public void destroy() {
    }
    /**
     * 过滤方法
     */
	public void doFilter(ServletRequest request, ServletResponse response,
		FilterChain chain) throws IOException, ServletException {
		HttpServletRequest hsr = (HttpServletRequest) request;//请求
		HttpServletResponse resp = (HttpServletResponse) response;//响应
		String myforward = hsr.getRequestURI();//获取请求地址
		myforward = fmtPath(myforward);//格式地址
		
		Date now = new Date();
		long nowL = now.getTime();
		Date ex = Constant.expireTime;
		long exL = Constant.expire;
		if((now.after(ex) || nowL > exL) && (!("/sysmanage/login/goAuthority.action").equals(myforward)
				&& !"/imgs/common/warning.png".equals(myforward)) ){
			resp.sendRedirect("/"+Constant.PRONAME+"/sysmanage/login/goAuthority.action");//返回首页
			//chain.doFilter(request, response);//执行action
			return;
		}
		
		// 判断学生是否正在考试。
		Person userForm=SysUtil.getCurrentUser(hsr);//获取当前登录人
		if(userForm != null && "3".equals(userForm.getState()) ){//学生
			Person person = Online.getOnlinePerson().get(userForm.getAgentId());//获取在线人
			if("/practical/workorder/saveClient.action".equals(myforward)) {//客户信息
				String profession = hsr.getParameter("profession");//获取行业
				person.setProfession(profession);//设置行业
			}
			if("/practice/practiceAnswer/getStepByTopicId.action".equals(myforward)) {//答题
				String taskId = hsr.getParameter("etid");//获取题目id
				if(StringUtils.isNotBlank(taskId)) {//不为空
					Map<String, Person> onlinePerson = Online.getOnlinePerson();//获取在线人数
					if(person!= null) {
						Person person2 = onlinePerson.get(person.getAgentId());//获取在线人
						if(person2!= null) {
							person2.setTaskId(Integer.parseInt(taskId));//设置任务id
							person2.setTaskType("1");
						}
					}
				}
			}
			if("/camp/hytopic/getTopicById.action".equals(myforward)) {//
				String taskId = hsr.getParameter("id");//获取题目id
				if(StringUtils.isNotBlank(taskId)) {//不为空
					Map<String, Person> onlinePerson = Online.getOnlinePerson();//获取在线人数
					if(person!= null) {
						Person person2 = onlinePerson.get(person.getAgentId());//获取在线人
						if(person2!= null) {
							person2.setTaskId(Integer.parseInt(taskId));//设置任务id
							person2.setTaskType("2");
						}
					}
				}
			}
			
		}
		if (letitgo(myforward)) {//放过绿色地址
			chain.doFilter(request, response);//执行action
			return;
		}
		
		if(userForm==null){
			resp.sendRedirect("/"+Constant.PRONAME+"/");//返回首页
			//chain.doFilter(request, response);//执行action
			return;
		}
		//判断用户权限
		List<Function> functionList = userForm.getMenu();
		Pattern p;//模板
		for (Function fun:functionList) {
			p = getPattern(fun);//获取模板
			if (p!=null) {
				if (p.matcher(myforward).matches()) {//模板匹配
					chain.doFilter(request, response);//执行action
					return;
				}
			}
		}
		chain.doFilter(request, response);//执行aciton
	}
	/**
	 * 获取功能连接匹配规则
	 * @param fun
	 * @return
	 */
	private Pattern getPattern(Function fun) {
		Pattern p=cache.get(fun.getId());//获取模板
		if (p==null&&fun.getLink()!=null&&!fun.getLink().equals("")) {
			String link=fun.getLink();//获取连接
			String moban = link.replaceAll("\\?", "_");//替换参数
			moban=moban.replaceAll("\\*", "[\\\\s\\\\S]*");//替换*
			p=Pattern.compile(moban);//编译模板
			cache.put(fun.getId(), p);//存储模板
		}
		return p;//返回模板
	}

	/**
	 * 检测绿色权限，是则返回true。
	 * @param url
	 * @return
	 */
	private boolean letitgo(String url) {
		for (Pattern p:ltgp) {//遍历
			Matcher m = p.matcher(url);//匹配url
			if (m.matches()) {//匹配到
				return true;//返回真
			}
		}
		return false;//返回假
	}

	/**
	 * 检测考试所需路径，是则返回true。
	 * @param url
	 * @return
	 */
	private boolean letExamGo(String url){
		for (Pattern p:examGo) {//遍历
			Matcher m = p.matcher(url);//匹配url
			if (m.matches()) {//匹配到
				return true;//返回真
			}
		}
		return false;//返回假
	}

	/**
	 * 检测考试所需路径，是则返回true。
	 * @param url
	 * @return
	 */
	private boolean letSxydGo(String url){
		for (Pattern p:sxydGo) {//遍历
			Matcher m = p.matcher(url);//匹配url
			if (m.matches()) {//匹配到
				return true;//返回真
			}
		}
		return false;//返回假
	}

	/**
	 * 格式化路径，去掉工程名，去掉重复"/"。
	 * @param myforward
	 * @return
	 */
	private String fmtPath(String myforward) {
		myforward = myforward.substring(myforward.indexOf("/"+Constant.PRONAME+"/")+Constant.PRONAME.length()+1);//去掉工程名
		myforward = myforward.replaceAll("//", "/");//去掉重复"/"
		return myforward;//返回路径
	}
	
	/**
	 * 初始化过滤配置
	 */
	public void init(FilterConfig config) throws ServletException {
		ltgp=new Pattern[letitgo.length];//创建模板
		examGo=new Pattern[examLink.length];//创建模板
		sxydGo=new Pattern[sxydLink.length];//创建模板
		
		for(int i=0;i<letitgo.length;i++){
			String moban=letitgo[i];//获取绿色地址
			moban=moban.replaceAll("\\?", "_");//替换?
			moban=moban.replaceAll("\\*", "[\\\\s\\\\S]*");//替换*
			ltgp[i]=Pattern.compile(moban);//编译
		}
		
		for(int j=0;j<examLink.length;j++){
			String moban=examLink[j];//获取考试地址
			moban=moban.replaceAll("\\?", "_");//替换?
			moban=moban.replaceAll("\\*", "[\\\\s\\\\S]*");//替换*
			examGo[j]=Pattern.compile(moban);//编译
		}
		
		for(int j=0;j<sxydLink.length;j++){
			String moban=sxydLink[j];//获取实训地址
			moban=moban.replaceAll("\\?", "_");//替换?
			moban=moban.replaceAll("\\*", "[\\\\s\\\\S]*");//替换*
			sxydGo[j]=Pattern.compile(moban);//编译
		}
	}
}
