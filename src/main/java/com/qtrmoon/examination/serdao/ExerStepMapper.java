package com.qtrmoon.examination.serdao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.qtrmoon.examination.pojo.ExerStep;
import com.qtrmoon.examination.pojo.ExerStepExt;
/**
 * 练习步骤持久层
 */
@Component
public interface ExerStepMapper {
	/**
	 * ExerStep的条件查询方法
	 * @param exerStep 承载查询条件
	 * @return 返回ExerStep的集合
	 */
	public List<ExerStep> schExerStep(ExerStep exerStep);
	
	/**
	 * ExerStep的主键查询方法
	 * @param id 主键值
	 * @return 返回ExerStep实体
	 */
	public ExerStep schExerStepById(Integer id);
	
	/**
	 * ExerStep的添加方法
	 * @param exerStep 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addExerStep(ExerStep exerStep);
	
	/**
	 * ExerStep的修改方法
	 * @param exerStep 承载数据的Bean
	 */
	public void updExerStep(ExerStep exerStep);
	
	/**
	 * ExerStep的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delExerStep(Integer id);
	
	/**
	 * ExerStep的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delExerSteps(Integer[] ids);
	
	/**
	 * 根据其他参数删除
	 * @param param
	 */
	public void delStepByParam(Map<String,Object> param);
	/**
	 * 查询步骤非开始结束节点
	 * @param exerStep
	 * @return
	 */
	public List<ExerStep> schExerStepNotBE(ExerStep exerStep);
	
	/**
	 * 自动阅卷/老师阅卷step列表
	 * @param param
	 */
	public List<ExerStep> markList(Map<String,Object> param);
	
	/**
	 * 查询步骤列表，和学生做题答案
	 * @param param
	 * @return
	 */
	public List<ExerStepExt> serchStepAndAnswer(Map<String,Object> param);
	/**
	 * 查询阅卷步骤非开始结束
	 * @param exerStep
	 * @return
	 */
	public List<ExerStep> schExerStepNotBETeacher(ExerStep exerStep);
}
