package com.qtrmoon.examination.serdao;

import java.util.List;
import org.springframework.stereotype.Component;

import com.qtrmoon.examination.pojo.ExamCategory;
/** 实体映射 */
@Component
public interface ExamCategoryMapper {
	/**
	 * ExamCategory的条件查询方法
	 * @param examCategory 承载查询条件
	 * @return 返回ExamCategory的集合
	 */
	public List<ExamCategory> schExamCategory(ExamCategory examCategory);
	
	/**
	 * ExamCategory的主键查询方法
	 * @param id 主键值
	 * @return 返回ExamCategory实体
	 */
	public ExamCategory schExamCategoryById(Integer id);
	
	/**
	 * ExamCategory的添加方法
	 * @param examCategory 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addExamCategory(ExamCategory examCategory);
	
	/**
	 * ExamCategory的修改方法
	 * @param examCategory 承载数据的Bean
	 */
	public void updExamCategory(ExamCategory examCategory);
	
	/**
	 * ExamCategory的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delExamCategory(Integer id);
	
	/**
	 * ExamCategory的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delExamCategorys(Integer[] ids);
}
