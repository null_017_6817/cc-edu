package com.qtrmoon.examination.serdao;

import java.util.List;
import org.springframework.stereotype.Component;

import com.qtrmoon.examination.pojo.ExerPaper;
/**
 * 试卷持久层
 * @author HuaTang_DYC
 *
 */
@Component
public interface ExerPaperMapper {
	/**
	 * ExerPaper的条件查询方法
	 * @param exerPaper 承载查询条件
	 * @return 返回ExerPaper的集合
	 */
	public List<ExerPaper> schExerPaper(ExerPaper exerPaper);
	
	/**
	 * ExerPaper的主键查询方法
	 * @param id 主键值
	 * @return 返回ExerPaper实体
	 */
	public ExerPaper schExerPaperById(Integer id);
	
	/**
	 * ExerPaper的添加方法
	 * @param exerPaper 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addExerPaper(ExerPaper exerPaper);
	
	/**
	 * ExerPaper的修改方法
	 * @param exerPaper 承载数据的Bean
	 */
	public void updExerPaper(ExerPaper exerPaper);
	
	/**
	 * ExerPaper的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delExerPaper(Integer id);
	
	/**
	 * ExerPaper的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delExerPapers(Integer[] ids);
}
