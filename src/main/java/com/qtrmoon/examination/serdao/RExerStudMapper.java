package com.qtrmoon.examination.serdao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import com.qtrmoon.examination.pojo.RExerStud;
/**
 * 学生答案持久层
 */
@Component
public interface RExerStudMapper {
	/**
	 * RExerStud的条件查询方法
	 * @param rExerStud 承载查询条件
	 * @return 返回RExerStud的集合
	 */
	public List<RExerStud> schRExerStud(RExerStud rExerStud);
	
	/**
	 * RExerStud的主键查询方法
	 * @param id 主键值
	 * @return 返回RExerStud实体
	 */
	public RExerStud schRExerStudById(Integer id);
	
	/**
	 * RExerStud的添加方法
	 * @param rExerStud 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addRExerStud(RExerStud rExerStud);
	
	/**
	 * RExerStud的修改方法
	 * @param rExerStud 承载数据的Bean
	 */
	public void updRExerStud(RExerStud rExerStud);
	
	/**
	 * RExerStud的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delRExerStud(Integer id);
	
	/**
	 * RExerStud的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delRExerStuds(Integer[] ids);
	
	/**
	 * 根据其他参数删除数据
	 * @param param
	 */
	public void delReStudents(Map<String,Object> param);
	
	/**
	 * RExerStud的根据班级、试卷分组查询方法
	 * @param classid 班级id
	 * @return 返回RExerStud的集合
	 */
	public List<RExerStud> schRExerStudBjExer(@Param("classid")String classid,@Param("name")String name);
	
	/**
	 * 教师阅卷查询列表
	 * @param rExerStud
	 * @return
	 */
	public List<RExerStud> schMarkRExerStud(RExerStud rExerStud);
	
}
