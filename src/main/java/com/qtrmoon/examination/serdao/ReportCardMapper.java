package com.qtrmoon.examination.serdao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import com.qtrmoon.examination.pojo.ReportCard;
/**
 * 成绩单持久层
 */
@Component
public interface ReportCardMapper {
	
	/**
	 * ReportCard的条件查询方法
	 * @param reportCard
	 * @return
	 */
	List<ReportCard> schReportCard(ReportCard reportCard);

	/**
	 * ReportCard的主键查询方法
	 * @param integer
	 * @return
	 */
	ReportCard schReportCardById(Integer integer);

	/**
	 * 导出Excel选中方法
	 * @param ids
	 * @return
	 */
	List<ReportCard> schReportExcel(@Param("ids") Integer[] ids);
}
