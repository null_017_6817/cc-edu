package com.qtrmoon.examination.serdao;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.ibatis.annotations.Param;
import org.dom4j.DocumentException;
import org.jdom.JDOMException;

import com.qtrmoon.book.pojo.Book;
import com.qtrmoon.examination.pojo.ExamCategory;
import com.qtrmoon.examination.pojo.Exer;
import com.qtrmoon.examination.pojo.ExerAnswer;
import com.qtrmoon.examination.pojo.ExerPaper;
import com.qtrmoon.examination.pojo.ExerStep;
import com.qtrmoon.examination.pojo.ExerStepExt;
import com.qtrmoon.examination.pojo.ExerTopic;
import com.qtrmoon.examination.pojo.RExerStud;
import com.qtrmoon.examination.pojo.ReportCard;
import com.qtrmoon.examination.pojo.StepInfo;
import com.qtrmoon.manager.pojo.BankTopic;

import freemarker.template.TemplateException;
/**
 * 考试接口
 * @author HuaTang_DYC
 *
 */
public interface IExaminationService {
	
	/***************** Exer方法组 *****************/
	/**
	 * Exer的条件查询方法
	 * @param exer 承载查询条件的Bean
	 * @return 返回Exer的集合
	 */
	public List<Exer> schExer(Exer exer);
	
	public List<Exer> schMarkExer(Exer exer);
	
	/**
	 * 查询试卷列表（无分页）
	 * @param exer
	 * @return
	 */
	public List<Exer> schExerList(Exer exer);
	
	/**
	 * Exer的主键查询方法
	 * @param id 主键值
	 * @return 返回Exer实体
	 */
	public Exer schExerById(Integer id);
	
	/**
	 * Exer的添加方法
	 * @param exer 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addExer(Exer exer);
	
	/**
	 * Exer的修改方法
	 * @param exer 承载数据的Bean
	 */
	public void updExer(Exer exer);
	
	/**
	 * 保存试卷和题目
	 * @param exer
	 */
	public Map<String,Object> updExerAndTopic(Exer exer,HttpServletRequest request) throws 
	RuntimeException, DocumentException, IOException, 
	TemplateException, JDOMException;
	
	/**
	 * 取消试卷和题目
	 * @param exer
	 * @return
	 */
	public void updCancel(Exer exer);
	
	/**
	 * Exer的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delExer(Integer id);
	
	/**
	 * Exer的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delExer(Integer[] ids);

	/**
	 * 删除
	 * @param ids
	 */
	public void delExerAndTopic(Integer[] ids);
	
	
	/***************** ExerAnswer方法组 *****************/
	/**
	 * ExerAnswer的条件查询方法
	 * @param exerAnswer 承载查询条件的Bean
	 * @return 返回ExerAnswer的集合
	 */
	public List<ExerAnswer> schExerAnswer(ExerAnswer exerAnswer);
	
	/**
	 * ExerAnswer的条件查询方法(无分页)
	 * @param exerAnswer 承载查询条件的Bean
	 * @return 返回ExerAnswer的集合
	 */
	public List<ExerAnswer> schExerAnswerList(ExerAnswer exerAnswer);
	
	/**
	 * ExerAnswer的主键查询方法
	 * @param id 主键值
	 * @return 返回ExerAnswer实体
	 */
	public ExerAnswer schExerAnswerById(Integer id);
	
	/**
	 * ExerAnswer的添加方法
	 * @param exerAnswer 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addExerAnswer(ExerAnswer exerAnswer);
	
	/**
	 * ExerAnswer的修改方法
	 * @param exerAnswer 承载数据的Bean
	 */
	public void updExerAnswer(ExerAnswer exerAnswer);
	
	/**
	 * ExerAnswer的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delExerAnswer(Integer id);
	
	/**
	 * ExerAnswer的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delExerAnswer(Integer[] ids);


	
	/***************** ExerPaper方法组 *****************/
	/**
	 * ExerPaper的条件查询方法
	 * @param exerPaper 承载查询条件的Bean
	 * @return 返回ExerPaper的集合
	 */
	public List<ExerPaper> schExerPaper(ExerPaper exerPaper);
	
	/**
	 * ExerPaper的主键查询方法
	 * @param id 主键值
	 * @return 返回ExerPaper实体
	 */
	public ExerPaper schExerPaperById(Integer id);
	
	/**
	 * ExerPaper的添加方法
	 * @param exerPaper 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addExerPaper(ExerPaper exerPaper);
	
	/**
	 * ExerPaper的修改方法
	 * @param exerPaper 承载数据的Bean
	 */
	public void updExerPaper(ExerPaper exerPaper);
	
	/**
	 * ExerPaper的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delExerPaper(Integer id);
	
	/**
	 * ExerPaper的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delExerPaper(Integer[] ids);


	
	/***************** ExerTopic方法组 *****************/
	/**
	 * ExerTopic的条件查询方法
	 * @param exerTopic 承载查询条件的Bean
	 * @return 返回ExerTopic的集合
	 */
	public List<ExerTopic> schExerTopic(ExerTopic exerTopic);
	
	/**
	 * 查询topic列表无分页
	 * @param exerTopic
	 * @return
	 */
	public List<ExerTopic> schExerTopicList(ExerTopic exerTopic);
	
	/**
	 * ExerTopic的主键查询方法
	 * @param id 主键值
	 * @return 返回ExerTopic实体
	 */
	public ExerTopic schExerTopicById(Integer id);
	
	/**
	 * ExerTopic的添加方法
	 * @param exerTopic 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addExerTopic(ExerTopic exerTopic);
	
	/**
	 * ExerTopic的修改方法
	 * @param exerTopic 承载数据的Bean
	 */
	public void updExerTopic(ExerTopic exerTopic);
	
	/**
	 * ExerTopic的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delExerTopic(Integer id);
	
	/**
	 * ExerTopic的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delExerTopic(Integer[] ids);

	/**
	 * 删除topic和step和answer
	 * @param ids
	 */
	public void delExerTopicAndStep(Integer[] ids);
	
	/**
	 * 复制数据（组织练习）
	 * @param param
	 * @return
	 */
	public boolean updateToCopy(List<BankTopic> bankTopicList,Integer eid,HttpServletRequest request,String type)throws IllegalAccessException, InvocationTargetException,
	ParseException, DocumentException, IOException, TemplateException,JDOMException ;
	
	/**
	 * 查询所有
	 * @author qinbo
	 * @param exerTopic
	 * @return
	 */
	public List<ExerTopic> schAllExerTopic(ExerTopic exerTopic);
	
	/***************** RExerStud方法组 *****************/
	/***************** ExerStep方法组 *****************/
	/**
	 * RExerStud的条件查询方法
	 * @param rExerStud 承载查询条件的Bean
	 * @return 返回RExerStud的集合
	 * ExerStep的条件查询方法
	 * @param exerStep 承载查询条件的Bean
	 * @return 返回ExerStep的集合
	 */
	public List<RExerStud> schRExerStud(RExerStud rExerStud);
	
	/**
	 * 教师阅卷
	 * @param rExerStud
	 * @return
	 */
	public List<RExerStud> schMarkRExerStud(RExerStud rExerStud);
	
	/**
	 * 教师阅卷(无分页)
	 * @param rExerStud
	 * @return
	 */
	public List<RExerStud> schMarkRExerStudList(RExerStud rExerStud);
	
	/**
	 * RExerStud的条件查询方法(无分页)
	 * @param rExerStud 承载查询条件的Bean
	 * @return 返回RExerStud的集合
	 * ExerStep的条件查询方法
	 * @param exerStep 承载查询条件的Bean
	 * @return 返回ExerStep的集合
	 */
	public List<RExerStud> schRExerStudList(RExerStud rExerStud);
	
	/**
	 * 
	 * @param exerStep
	 * @return
	 */
	public List<ExerStep> schExerStep(ExerStep exerStep);
	
	/**
	 * 查询step列表（无分页）
	 * @param exerStep
	 * @return
	 */
	public List<ExerStep> schExerStepList(ExerStep exerStep);
	
	/**
	 * RExerStud的主键查询方法
	 * ExerStep的主键查询方法
	 * @param id 主键值
	 * @return 返回RExerStud实体
	 * @return 返回ExerStep实体
	 */
	public RExerStud schRExerStudById(Integer id);
	/**
	 * 根据id查询步骤
	 * @param id
	 * @return
	 */
	public ExerStep schExerStepById(Integer id);
	
	/**
	 * RExerStud的添加方法
	 * @param rExerStud 承载数据的Bean，执行添加后会更新入库后的主键值
	 * ExerStep的添加方法
	 * @param exerStep 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addRExerStud(RExerStud rExerStud);
	/**
	 * 添加练习步骤
	 * @param exerStep
	 * @return
	 */
	public Integer addExerStep(ExerStep exerStep);
	
	/**
	 * 保存练习与学生的关系
	 * @param stuIds
	 * @param eid
	 * @param classId
	 */
	public void addGx(String stuIds, String eid, String classId);
	
	/**
	 * RExerStud的修改方法
	 * @param rExerStud 承载数据的Bean
	 * ExerStep的修改方法
	 * @param exerStep 承载数据的Bean
	 */
	public void updRExerStud(RExerStud rExerStud);
	/**
	 * 更新练习步骤
	 * @param exerStep
	 */
	public void updExerStep(ExerStep exerStep);
	
	/**
	 * RExerStud的单记录删除方法
	 * ExerStep的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delRExerStud(Integer id);
	/**
	 * 删除练习步骤
	 * @param id
	 */
	public void delExerStep(Integer id);
	/**
	 * RExerStud的批量删除方法
	 * ExerStep的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delRExerStud(Integer[] ids);
	/**
	 * 批量删除练习步骤
	 * @param ids
	 */
	public void delExerStep(Integer[] ids);

	/**
	 * 根据其他参数删除
	 * @param param
	 */
	public void delReStudents(Map<String,Object> param);
	
	/**
	 * RExerStud的根据班级、试卷分组查询方法
	 * @param classid 班级号
	 * @return 返回RExerStud的集合
	 */
	public List<RExerStud> schRExerStudBjExer(String classid,String name);
	
	/**
	 * 查询步骤及学生做题答案
	 * @param param
	 * @return
	 */
	public List<ExerStepExt> serchStepAndAnswer(Map<String,Object> param)throws DocumentException, JDOMException, IOException;
	
	/***************** ReportCard方法组 *****************/
	
	/**
	 * ReportCard的条件查询方法
	 * @param id 
	 */
	public List<ReportCard> schReportCard(ReportCard reportCard, Integer id);
	
	/**
	 * 根据ID查询
	 */
	public ReportCard schReportCardById(Integer id);
	
	/**
	 * 导出选中的
	 */
	public List<ReportCard> schReportExcel(Integer[] ids);
	/**
	 * 查询练习题目和状态
	 * @param exerTopic
	 * @param studid
	 * @return
	 */
	public List<ExerTopic> schExerTopicAndStatus(ExerTopic exerTopic, String studid);

	
	/**
	 * 自动阅卷
	 * @param param（eid:练习ID，stuNum：学生学号）
	 * @return
	 */
	public void updAutoMark(Map<String,Object> param) throws DocumentException, JDOMException, IOException;

	/**
	 * 教师阅卷得分汇总
	 * @param param
	 * @return
	 */
	public RExerStud updTeacherMarkTotal(Map<String,Object> param);
	
	/**
	 * 获取题目和步骤id
	 * @param eid
	 * @return
	 */
	public List<Map<Integer, List<Integer>>> getTopicIdsAndStepIds(Integer eid);
	/**
	 * 更新答案
	 * @param exerAnswer
	 */
	public void updExerAnswerByEtsId(ExerAnswer exerAnswer);
	/**
	 * 查询练习和状态
	 * @param exer
	 * @return
	 */
	public List<Exer> schExerAndStatus(Exer exer);
	/**
	 * 根据课本获得题目步骤id
	 * @param bookId
	 * @return
	 */
	public Map<Integer, List<Integer>> getTopicIdsAndStepIdsByBookId(
			Integer bookId);
	/**
	 * 获取阅卷题目步骤id
	 * @param eid
	 * @return
	 */
	public Map<Integer, List<Integer>> getTopicIdsAndStepIdsTeacher(
			Integer eid);
	/**
	 * 更新答案
	 * @param answers
	 */
	public void updExerAnswerByEtId(List<ExerAnswer> answers);
	
	public void updExerAnswerByEtIdd(List<ExerAnswer> answers);
	
	// 老师更新分数
	public void updScoreByTeacher(List<ExerAnswer> answers);
	
	
	/**
	 * 查询阅卷题目
	 * @param exerTopic
	 * @return
	 */
	public List<ExerTopic> schExerTopicTeacher(ExerTopic exerTopic);
	/**
	 * 查询非实训营地的题目
	 * @param exerTopic
	 * @return
	 */
	public List<ExerTopic> schExerTopicNotCampList(ExerTopic exerTopic);
	
	/***************** ExamCategory方法组 *****************/
	/**
	 * ExamCategory的条件查询方法
	 * @param examCategory 承载查询条件的Bean
	 * @return 返回ExamCategory的集合
	 */
	public List<ExamCategory> schExamCategory(ExamCategory examCategory);
	
	/**
	 * ExamCategory的条件查询方法(无分页)
	 * @param examCategory 承载查询条件的Bean
	 * @return 返回ExamCategory的集合
	 */
	public List<ExamCategory> schExamCategoryList(ExamCategory examCategory);
	
	/**
	 * ExamCategory的主键查询方法
	 * @param id 主键值
	 * @return 返回ExamCategory实体
	 */
	public ExamCategory schExamCategoryById(Integer id);
	
	/**
	 * ExamCategory的添加方法
	 * @param examCategory 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addExamCategory(ExamCategory examCategory);
	
	/**
	 * ExamCategory的修改方法
	 * @param examCategory 承载数据的Bean
	 */
	public void updExamCategory(ExamCategory examCategory);
	
	/**
	 * ExamCategory的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delExamCategory(Integer id);
	
	/**
	 * ExamCategory的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delExamCategory(Integer[] ids);
	
	
	/**
	 * 查询个人完成情况 
	 * @param eid 试卷id
	 * @param classid 班级号
	 * @param wcqk 完成情况
	 * @return 返回结果的集合
	 */
	public List<Map<String,Object>> schPersonalCompletion(Integer eid,Integer classid,String wcqk);
	
	/**
	 * 查询任务统计 
	 * @param eid 试卷id
	 * @param classid 班级号
	 * @return 返回结果的集合
	 */
	public List<Map<String,Object>> schTaskStatistics(Integer eid,Integer classid);
	
	/**
	 * 查询学习过程分析 
	 * @param classid 班级号
	 * @param studid 学号
	 * @return 返回结果的集合
	 */
	public List<Map<String,Object>> schLearningProcess(Integer classid,Integer studid);
	
	/**
	 * 查询任务统计查任务 
	 * @param classid 班级号
	 * @param studid 学号
	 * @return 返回结果的集合
	 */
	public List<Map<String,Object>> schOrderStatistics(Integer classid,Integer studid);
	
	/**
	 * 查询班级完成情况 
	 * @param eid 试卷id
	 * @return 返回结果的集合
	 */
	public List<Map<String,Object>> schClassCompletion(Integer eid);
	
	/**
	 * 预览试卷service层处理
	 * @param exer
	 * @return Map集合
	 */
	public Map<String,Object> previewExer(Exer exer,String projectName)throws DocumentException, JDOMException, IOException;
	
	/**
	 * 预览题目service层处理
	 * @param exer
	 * @return Map集合
	 */
	public List<StepInfo> previewTopic(ExerTopic topic,String projectName)throws DocumentException, JDOMException, IOException;
	
	/**
	 * 发卷和撤卷操作
	 * @param addStr 关系表要添加的序号集合
	 * @param delStr 关系表要删除的序号集合
	 * @param eid 试卷ID
	 * @param teachers 分配教师的集合
	 */
	public void updAddAndDelRExer(String addStr,String delStr,String eid,String teachers);
	
	/**
	 * 获取阅卷题目列表
	 * @param eid
	 * @param studNum
	 */
	public Map<String,Object> markTopicsList(Integer eid,String studNum);
	
	/**
	 * 保存实训营地步骤和学生作答
	 * @param steps
	 */
	public List<ExerStep> saveSxStepAndAnswer(List<ExerStep> steps,String studNum);
	
	/**
	 * 删除实训营地步骤和学生作答
	 * @param step
	 * @param studNum
	 * @return
	 */
	public void delSxStepAndAnswer(ExerStep step,String studNum);
	/**
	 * 根据 booid 查询 习题 
	 * @param book
	 * @return
	 */
	public List<ExerTopic> getExerTopicBybookidNum(Book book);
	/**
	 * 根据 booid 查询 习题 set 类型
	 * @param book
	 * @return
	 */
	public HashSet<ExerTopic> getExerTopicBybookidNumSet(Book book);

	/**
	 * 查询去重的
	 * @param exerAnswer
	 * @return
	 */
	public List<ExerAnswer> schExerAnswerListDistinct(ExerAnswer exerAnswer);
}//end
