package com.qtrmoon.examination.serdao;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.output.FileWriterWithEncoding;
import org.apache.commons.lang.StringUtils;
import org.dom4j.DocumentException;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import com.qtrmoon.examination.constant.ExerConstant;
import com.qtrmoon.examination.pojo.ChoiceQuestion;
import com.qtrmoon.examination.pojo.Prompt;
import com.qtrmoon.examination.pojo.StepInfo;
import com.qtrmoon.manager.constant.TopicTypeConstant;
import com.qtrmoon.manager.pojo.BankStep;
import com.qtrmoon.toolkit.XmlUtil;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
/**
 * freemaker工具类
 * @author HuaTang_DYC
 *
 */
public class FreemakerTools{
	/**
	 * 生成静态页面
	 * @param eid
	 * @param etid
	 * @param esid
	 * @param stepInfo
	 * @param request
	 * @param bankId
	 * @param topicId
	 * @throws IOException
	 * @throws TemplateException
	 * @throws DocumentException
	 * @throws JDOMException
	 */
	public static void releaseStepHtml(Integer eid, Integer etid, Integer esid,String stepInfo,HttpServletRequest request,
			String bankId,String topicId,BankStep bankStep) throws IOException, TemplateException, DocumentException, JDOMException {
		Configuration cfg = new Configuration(Configuration.VERSION_2_3_23);// 获取参数
		String mubanPath = request.getSession().getServletContext().getRealPath(ExerConstant.FREEMAKER_FILE_PATH);// 获取参数
		String destPath = ExerConstant.HTML_FILE_PATH;// 获取参数
		String projectName =request.getContextPath();// 获取参数
		String audioPath = projectName+"/paper/question/appendUrl.action?filename="// 获取参数
				+bankId+File.separator+topicId+File.separator;// 获取参数
		cfg.setDirectoryForTemplateLoading(new File(mubanPath));// 设置参数
		StepInfo step = xmlToStep(stepInfo, audioPath);// 解析文件
		step.setBankId(bankId);
		step.setTopicId(topicId);
		step.setProjectName(projectName);// 设置参数
		String exerType = step.getType();// 设置参数
		Template choiceTemplate = null;// 设置参数
		if(ExerConstant.TOPIC_STEP_TYPE_QUESTION_CHOICE_SINGLE.equals(exerType)) {// 判断参数
			choiceTemplate = cfg.getTemplate(ExerConstant.FREEMAKER_TOPIC_STEP_TYPE_QUESTION_CHOICE_SINGLE, "UTF-8");// 获取模板
		}else if(ExerConstant.TOPIC_STEP_TYPE_VOICE.equals(exerType)) {// 判断参数
			choiceTemplate = cfg.getTemplate(ExerConstant.FREEMAKER_TOPIC_STEP_TYPE_QUESTION_PROMPT, "UTF-8");// 获取模板
		}else if(ExerConstant.TOPIC_STEP_TYPE_STATE.equals(exerType)) {// 判断参数
			choiceTemplate = cfg.getTemplate(ExerConstant.FREEMAKER_TOPIC_STEP_TYPE_STATE, "UTF-8");// 获取模板
		}else if(ExerConstant.TOPIC_STEP_TYPE_REPEAT.equals(exerType)) {// 判断参数
			choiceTemplate = cfg.getTemplate(ExerConstant.FREEMAKER_TOPIC_STEP_TYPE_REPEAT, "UTF-8");// 获取模板
		}else if(ExerConstant.TOPIC_STEP_TYPE_QUESTION_JUDGE.equals(exerType)) {// 判断参数
			choiceTemplate = cfg.getTemplate(ExerConstant.FREEMAKER_TOPIC_STEP_TYPE_QUESTION_JUDGE, "UTF-8");// 获取模板
		}else if(ExerConstant.TOPIC_STEP_TYPE_QUESTION_CLOZE.equals(exerType)) {// 判断参数
			if(StringUtils.isNotBlank(step.getPrompt().getAudio())) {// 判断参数
				choiceTemplate = cfg.getTemplate(ExerConstant.FREEMAKER_TOPIC_STEP_TYPE_QUESTION_CLOZE, "UTF-8");// 获取模板
			}else{
				choiceTemplate = cfg.getTemplate(ExerConstant.FREEMAKER_TOPIC_STEP_TYPE_QUESTION_NOVOICECLOZE, "UTF-8");// 获取模板
			}
		}else if(ExerConstant.TOPIC_STEP_TYPE_QUESTION_CHOICE_MULTI.equals(exerType)) {// 判断参数
			choiceTemplate = cfg.getTemplate(ExerConstant.FREEMAKER_TOPIC_STEP_TYPE_QUESTION_CHOICE_MULTI, "UTF-8");// 获取模板
		}else if(ExerConstant.TOPIC_STEP_TYPE_QUESTION_ANSWER.equals(exerType)) {// 判断参数
			if(bankStep != null &&  TopicTypeConstant.ANSWER.equals(bankStep.getType())){ //填空题
				choiceTemplate = cfg.getTemplate(ExerConstant.FREEMAKER_TOPIC_STEP_TYPE_QUESTION_NOFILEANSWER, "UTF-8");// 获取模板
			}
			if(bankStep != null &&  TopicTypeConstant.ANALYZE.equals(bankStep.getType())//数据分析
					|| TopicTypeConstant.VERBAL.equals(bankStep.getType()) // 话术编写
					|| TopicTypeConstant.STEP_DRAW.equals(bankStep.getType())){// 画图连线题
				choiceTemplate = cfg.getTemplate(ExerConstant.FREEMAKER_TOPIC_STEP_TYPE_QUESTION_ANSWER, "UTF-8");// 获取模板
			}
		
		}
		Map<String, Object> map = new HashMap<String, Object>();// 实例化对象
		map.put("step", step);// 设置参数
		File file = new File(destPath + "/eid" +eid + "/" + etid + "_" + esid + ".html");// 设置参数
		if(!file.getParentFile().exists()) {// 判断参数
			file.getParentFile().mkdirs();// 创建文件夹
		}
		Writer writer  = new FileWriterWithEncoding(file, "UTF-8");// 读取
		if(choiceTemplate != null) {// 判断参数
			choiceTemplate.process(map, writer);// 生成文件
		}
		writer.flush();// 刷新流
		writer.close();// 关闭流
	}
	/**
	 * xml转换step
	 * @param stepInfo
	 * @param audioPath
	 * @return
	 * @throws DocumentException
	 * @throws JDOMException
	 * @throws IOException
	 */
	public static StepInfo xmlToStep(String stepInfo, String audioPath) throws DocumentException, JDOMException, IOException {
		StepInfo step = new StepInfo();// 定义对象
		if(stepInfo != null){
			XmlUtil xml=new XmlUtil();// 定义对象
			xml.setCharset("UTF-8");// 设置字符编码
			
			SAXBuilder sb = new SAXBuilder();// 定义对象
			ByteArrayInputStream bais=new ByteArrayInputStream(stepInfo.getBytes("UTF-8"));// 定义流
			Document doc = sb.build(bais);// 定义对象
			Element elem = doc.getRootElement();// 获取节点
			String type = elem.getAttributeValue("type");// 获取节点
			step.setType(type);// 设置参数
			String title = elem.getChildTextTrim(ExerConstant.STEP_TITLE);// 获取节点
			Prompt prompt = new Prompt();// 定义对象
			Element element = elem.getChild(ExerConstant.STEP_PROMPT);// 获取节点
			if(element != null) {// 判断
				prompt.setContext(elem.getChildText(ExerConstant.STEP_PROMPT));// 设置参数
				String audio = element.getAttributeValue(ExerConstant.STEP_AUDIO);// 设置参数
				if(!StringUtils.isBlank(audio)) {// 判断
					prompt.setAudio(audioPath + element.getAttributeValue(ExerConstant.STEP_AUDIO));// 设置参数
					prompt.setAudioHid(element.getAttributeValue(ExerConstant.STEP_AUDIO));// 设置参数
				}
				prompt.setRepeat(element.getAttributeValue(ExerConstant.STEP_REPEAT));// 设置参数
				prompt.setFileName(element.getAttributeValue(ExerConstant.STEP_PROMPT_FILENAME));
				prompt.setOrigiName(element.getAttributeValue(ExerConstant.STEP_PROMPT_ORIGINAME));
			}
			
			step.setTitle(title);// 设置参数
			step.setPrompt(prompt);// 设置参数
			step.setStandardAnswer(elem.getChildText(ExerConstant.STEP_STANDARD_ANSWER));// 设置参数
			
			Element answerElem=elem.getChild(ExerConstant.STEP_ANSWER);// 获取节点
			List<ChoiceQuestion> choiceQuestions = null;// 定义对象
			if(answerElem != null) {// 判断
				String answertype = answerElem.getAttributeValue(ExerConstant.STEP_TYPE);// 获取属性
				if(ExerConstant.TOPIC_STEP_TYPE_QUESTION.equals(type)) {// 判断
					step.setType(answertype);// 设置参数
				}
				List<Element> choiceList=answerElem.getChildren(ExerConstant.STEP_CHOICE);// 获取节点
				choiceQuestions = new ArrayList<ChoiceQuestion>();// 定义对象
				for(Element choice:choiceList){// 循环
					ChoiceQuestion choiceQuestion = new ChoiceQuestion();// 定义对象
					choiceQuestion.setId(choice.getAttributeValue(ExerConstant.STEP_ID));// 设置参数
					choiceQuestion.setAudio(audioPath + choice.getAttributeValue(ExerConstant.STEP_AUDIO));// 设置参数
					choiceQuestion.setAudioHid(choice.getAttributeValue(ExerConstant.STEP_AUDIO));// 设置参数
					choiceQuestion.setNext(choice.getAttributeValue(ExerConstant.STEP_NEXT));// 设置参数
					choiceQuestion.setContext(choice.getText());// 设置参数
					choiceQuestions.add(choiceQuestion);// 添加
				}
			}
			step.setChoiceQuestionList(choiceQuestions);// 设置参数
		}
		return step;// 返回数据
	}
}
