package com.qtrmoon.examination.serdao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

@Component
public interface DataDisplayMapper {
	/**
	 * 查询个人完成情况 
	 * @param eid 试卷id
	 * @param classid 班级号
	 * @param wcqk 完成情况
	 * @return 返回结果的集合
	 */
	public List<Map<String,Object>> schPersonalCompletion(@Param("eid")Integer eid,@Param("classid")Integer classid,@Param("wcqk")String wcqk);
	
	/**
	 * 查询任务统计查人数 
	 * @param eid 试卷id
	 * @param classid 班级号
	 * @return 返回结果的集合
	 */
	public List<Map<String,Object>> schTaskStatistics(@Param("eid")Integer eid,@Param("classid")Integer classid);
	
	/**
	 * 查询学习过程分析 
	 * @param classid 班级号
	 * @param studid 学号
	 * @return 返回结果的集合
	 */
	public List<Map<String,Object>> schLearningProcess(@Param("classid")Integer classid,@Param("studid")Integer studid);

	/**
	 * 查询任务统计查任务 
	 * @param classid 班级号
	 * @param studid 学号
	 * @return 返回结果的集合
	 */
	public List<Map<String,Object>> schOrderStatistics(@Param("classid")Integer classid,@Param("studid")Integer studid);
	
	/**
	 * 查询班级完成情况 
	 * @param eid 试卷id
	 * @return 返回结果的集合
	 */
	public List<Map<String,Object>> schClassCompletion(@Param("eid")Integer eid);
}
