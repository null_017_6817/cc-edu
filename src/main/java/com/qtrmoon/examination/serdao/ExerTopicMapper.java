package com.qtrmoon.examination.serdao;

import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.qtrmoon.examination.pojo.ExerTopic;

@Component
public interface ExerTopicMapper {
	/**
	 * ExerTopic的条件查询方法
	 * @param exerTopic 承载查询条件
	 * @return 返回ExerTopic的集合
	 */
	public List<ExerTopic> schExerTopic(ExerTopic exerTopic);
	
	/**
	 * ExerTopic的主键查询方法
	 * @param id 主键值
	 * @return 返回ExerTopic实体
	 */
	public ExerTopic schExerTopicById(Integer id);
	
	/**
	 * ExerTopic的添加方法
	 * @param exerTopic 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addExerTopic(ExerTopic exerTopic);
	
	/**
	 * ExerTopic的修改方法
	 * @param exerTopic 承载数据的Bean
	 */
	public void updExerTopic(ExerTopic exerTopic);
	
	/**
	 * ExerTopic的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delExerTopic(Integer id);
	
	/**
	 * ExerTopic的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delExerTopics(Integer[] ids);

	/**
	 * 查询题目和状态
	 * @param exerTopic
	 * @return
	 */
	public List<ExerTopic> schExerTopicAndStatus(ExerTopic exerTopic);
	
	/**
	 * 根据参数获取最大order_no
	 * @param param
	 * @return
	 */
	public Integer getMaxOrderByParam(Map<String,Object> param);

	/**
	 * 根据eid查询数据
	 * @param eid
	 * @return
	 */
	public List<ExerTopic> schExerTopicByEid(Integer eid);

	/**
	 * 根据bookid查询数据
	 * @param bookId
	 * @return
	 */
	public List<ExerTopic> schExerTopicByBookId(Integer bookId);
	
	/**
	 * 查询老师数据
	 * @param exerTopic
	 * @return
	 */
	public List<ExerTopic> schExerTopicTeacher(ExerTopic exerTopic);

	/**
	 * 查询数据
	 * @param eid
	 * @return
	 */
	public List<ExerTopic> schExerTopicTeacherByEidNotTrian(Integer eid);

	/**
	 * 查询数据
	 * @param eid
	 * @return
	 */
	public List<ExerTopic> schExerTopicNotCamp(ExerTopic exerTopic);

	/**
	 * 查询数据
	 * @param eid
	 * @return
	 */
	public List<ExerTopic> schExerTopicByBookIdNotTrian(Integer bookId);
	
	/**
	 * 根据试卷ID删除数据
	 * @param eid
	 */
	public void delByExerId(Integer eid);
	
	/**
	 * 更新排序号
	 * @param exerTopic
	 */
	public void updOrderNo(ExerTopic exerTopic);
	
	/**
	 * set 查询
	 * @param id
	 * @return
	 */
	public HashSet<ExerTopic> schExerTopicByBookIdNotTrianSet(Integer id);
}
