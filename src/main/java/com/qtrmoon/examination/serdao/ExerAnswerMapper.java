package com.qtrmoon.examination.serdao;

import java.util.List;
import org.springframework.stereotype.Component;

import com.qtrmoon.examination.pojo.ExerAnswer;
/**
 * 练习答案持久层
 */
@Component
public interface ExerAnswerMapper {
	/**
	 * ExerAnswer的条件查询方法
	 * @param exerAnswer 承载查询条件
	 * @return 返回ExerAnswer的集合
	 */
	public List<ExerAnswer> schExerAnswer(ExerAnswer exerAnswer);
	
	/**
	 * ExerAnswer的主键查询方法
	 * @param id 主键值
	 * @return 返回ExerAnswer实体
	 */
	public ExerAnswer schExerAnswerById(Integer id);
	
	/**
	 * ExerAnswer的添加方法
	 * @param exerAnswer 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addExerAnswer(ExerAnswer exerAnswer);
	
	/**
	 * ExerAnswer的修改方法
	 * @param exerAnswer 承载数据的Bean
	 */
	public void updExerAnswer(ExerAnswer exerAnswer);
	
	/**
	 * ExerAnswer的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delExerAnswer(Integer id);
	
	/**
	 * ExerAnswer的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delExerAnswers(Integer[] ids);
	/**
	 * 更新练习答案
	 * @param exerAnswer
	 */
	public void updExerAnswerByEtsId(ExerAnswer exerAnswer);
	//更新分数
	public void updExerAnswerByEtsIdd(ExerAnswer exerAnswer);
	/**
	 * 查询练习答案
	 * @param exerAnswer
	 * @return
	 */
	public List<ExerAnswer> schExerAnswerByEtsId(ExerAnswer exerAnswer);
	
	/**
	 * 根据stepId(esid)删除
	 * @param id
	 */
	public void delExerAnswerByEsid(Integer id);
	
	/**
	 * 根据参数删除数据
	 * @param exerAnswer
	 */
	public void delExerAnswerByParam(ExerAnswer exerAnswer);
	
	/**
	 * 去重查询
	 * @param exerAnswer
	 * @return
	 */
	public List<ExerAnswer> schExerAnswerListDistinct(ExerAnswer exerAnswer);
}
