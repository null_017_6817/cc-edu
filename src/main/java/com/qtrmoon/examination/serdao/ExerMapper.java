package com.qtrmoon.examination.serdao;

import java.util.List;
import org.springframework.stereotype.Component;

import com.qtrmoon.examination.pojo.Exer;
/**
 * 练习持久层
 * @author HuaTang_DYC
 *
 */
@Component
public interface ExerMapper {
	/**
	 * Exer的条件查询方法
	 * @param exer 承载查询条件
	 * @return 返回Exer的集合
	 */
	public List<Exer> schExer(Exer exer);
	
	/**
	 * Exer的主键查询方法
	 * @param id 主键值
	 * @return 返回Exer实体
	 */
	public Exer schExerById(Integer id);
	
	/**
	 * Exer的添加方法
	 * @param exer 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addExer(Exer exer);
	
	/**
	 * Exer的修改方法
	 * @param exer 承载数据的Bean
	 */
	public void updExer(Exer exer);
	
	/**
	 * Exer的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delExer(Integer id);
	
	/**
	 * Exer的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delExers(Integer[] ids);
	/**
	 * 查询练习和状态
	 * @param exer
	 * @return
	 */
	public List<Exer> schExerAndStatus(Exer exer);
	
	/**
	 * 查询阅卷列表
	 * @param exer
	 * @return
	 */
	public List<Exer> schExerByMark(Exer exer);
}
