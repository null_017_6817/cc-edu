package com.qtrmoon.examination.serdao;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.dom4j.DocumentException;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.xml.sax.InputSource;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.qtrmoon.book.pojo.Book;
import com.qtrmoon.examination.constant.AddTypeConstant;
import com.qtrmoon.examination.constant.ExerConstant;
import com.qtrmoon.examination.constant.IsFinishConstant;
import com.qtrmoon.examination.constant.IsSaveConstant;
import com.qtrmoon.examination.pojo.ExamCategory;
import com.qtrmoon.examination.pojo.Exer;
import com.qtrmoon.examination.pojo.ExerAnswer;
import com.qtrmoon.examination.pojo.ExerPaper;
import com.qtrmoon.examination.pojo.ExerStep;
import com.qtrmoon.examination.pojo.ExerStepExt;
import com.qtrmoon.examination.pojo.ExerTopic;
import com.qtrmoon.examination.pojo.Prompt;
import com.qtrmoon.examination.pojo.RExerStud;
import com.qtrmoon.examination.pojo.ReportCard;
import com.qtrmoon.examination.pojo.StepInfo;
import com.qtrmoon.manager.constant.TopicTypeConstant;
import com.qtrmoon.manager.pojo.BankStep;
import com.qtrmoon.manager.pojo.BankTopic;
import com.qtrmoon.manager.serdao.BankStepMapper;
import com.qtrmoon.manager.serdao.BankTopicMapper;
import com.qtrmoon.study.pojo.StudyStatus;
import com.qtrmoon.study.serdao.StudyStatusMapper;
import com.qtrmoon.sysmanage.pojo.User;
import com.qtrmoon.sysmanage.serdao.UserMapper;
import com.qtrmoon.util.RandomUtils;

import freemarker.template.TemplateException;
/**
 * 考试业务层
 */
@Service("examinationService")
public class ExaminationServiceImpl implements IExaminationService {
	@Autowired
	private ExerMapper exerMapper; //声明模块Mapper实例

	/***************** Exer方法组 *****************/
	/**
	 * Exer的条件查询方法
	 * @param exer
	 * 承载查询条件的Bean
	 * @return 返回Exer的集合
	 */
	@Override
	public List<Exer> schExer(Exer exer) {
		PageHelper.startPage(exer.getPage(), exer.getPagesize());// 设置分页显示
		List<Exer> list = exerMapper.schExer(exer);// 查询数据
		Page<Exer> page = (Page<Exer>) list;// 为了设置总记录数先类型强转
		exer.setDatasize(page.getTotal());// 设置数据长度
		return list;// 返回结果集
	}

	/**
	 * Exer的条件查询方法
	 * @param exer
	 * 承载查询条件的Bean
	 * @return 返回Exer的集合
	 */
	@Override
	public List<Exer> schMarkExer(Exer exer) {
		PageHelper.startPage(exer.getPage(), exer.getPagesize());// 设置分页显示
		List<Exer> list = exerMapper.schExerByMark(exer);// 查询数据
		Page<Exer> page = (Page<Exer>) list;// 为了设置总记录数先类型强转
		exer.setDatasize(page.getTotal());// 设置数据长度
		return list;// 返回结果集
	}
	
	
	/**
	 * 查询试卷列表（无分页）
	 * @param exer
	 * 承载查询条件的Bean
	 * @return 返回Exer的集合
	 */
	@Override
	public List<Exer> schExerList(Exer exer){
		List<Exer> list = exerMapper.schExer(exer);// 查询数据
		return list;// 返回结果集
	}
	
	/**
	 * Exer的主键查询方法
	 * @param id
	 * 主键值
	 * @return 返回Exer实体
	 */
	@Override
	public Exer schExerById(Integer id) {
		return exerMapper.schExerById(id);// 返回结果集
	}

	/**
	 * Exer的添加方法
	 * @param exer
	 * 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	@Override
	public Integer addExer(Exer exer) {
		return exerMapper.addExer(exer);// 返回结果集
	}

	/**
	 * Exer的修改方法
	 * @param exer
	 * 承载数据的Bean
	 */
	@Override
	public void updExer(Exer exer) {
		exerMapper.updExer(exer);// 更新数据
	}

	/**
	 * 更新试卷及试卷题目
	 * @param exer
	 * @return 
	 * @throws JDOMException 
	 * @throws TemplateException 
	 * @throws IOException 
	 * @throws DocumentException 
	 * @throws RuntimeException 
	 */
	public Map<String,Object> updExerAndTopic(Exer exer,HttpServletRequest request) throws 
		RuntimeException, DocumentException, IOException, 
		TemplateException, JDOMException{
		Map<String,Object> result = new HashMap<String,Object>();
		// 查询数据
		Exer exeParam = exerMapper.schExerById(exer.getId());
		if(exeParam != null && exeParam.getId() != null){
			exeParam.setName(exer.getName());// 试卷名称
			exeParam.setStartTime(exer.getStartTime());// 开始时间
			exeParam.setPassScore(exer.getPassScore());// 及格分
			exeParam.setEndTime(exer.getEndTime());// 结束时间
			exeParam.setDepartment(exer.getDepartment());// 教研室
			exeParam.setTotalScore(exer.getTotalScore());// 总分数
			exeParam.setShowStandard(exer.getShowStandard());// 是否显示正确答案
			exeParam.setShortcut(exer.getShortcut());// 分类目录
			exeParam.setIsSave(IsSaveConstant.YES_CODE);// 是否保存，已保存
			exeParam.setTopicIds(exer.getTopicIds());// 题目ID的数组
			exeParam.setTypeSortStr(exer.getTypeSortStr());
			exeParam.setBankIdsStr(exer.getBankIdsStr());
			exeParam.setNumParamStr(JSON.toJSONString(exer.getNumParam()));// 数量选择转字符串存储
			exer.setAddType(exeParam.getAddType()); // 添加方式
			
			// 查询发卷人数
			RExerStud re = new RExerStud();
			re.setEid(exeParam.getId());
			int stuNum = 0;
			List<RExerStud> rss = this.schRExerStudList(re);
			if(rss != null && rss.size() > 0){
				stuNum = rss.size();
			}
			if (stuNum == 0) {
				int num = getTopicNum(exer,request);// 计算题目数
				exeParam.setTotalNum(num);// 题目数量
			}
			// 更新试卷
			exerMapper.updExer(exeParam);
			result.put("success", true);
			result.put("data", exeParam);
		}else{
			result.put("success", false);
			result.put("msg", "试卷不存在，请重新打开此页面添加试题");
		}
		return result;
	}

	private int getTopicNum(Exer exer,HttpServletRequest request) throws 
		RuntimeException, DocumentException, IOException, 
		TemplateException, JDOMException{
		int num = 0;
		if(exer != null && exer.getId() != null
				&& exer.getAddType() != null ){
			if( AddTypeConstant.HAND.equals(exer.getAddType()) ){
				if( exer.getTopicIds() != null ){
					Integer[] topicIds = exer.getTopicIds();
					for( int i = 0 ; i < topicIds.length ; i++){
						Integer topicId = topicIds[i];
						ExerTopic exerTopic = new ExerTopic();
						exerTopic.setId(topicId);
						exerTopic.setOrderNo(i);
						exerTopicMapper.updOrderNo(exerTopic);
						num++;
					}
				}
			}
			if(AddTypeConstant.AUTOMATIC.equals(exer.getAddType())){
				String[] typeSort = exer.getTypeSort();
				// 各题型数目
				Map<String,Object> numParam = exer.getNumParam();
				// 题库ID集合
				String[] bankIds = exer.getBankIds();
				// 题库ID
				String bankIdParam = "";
				for( int j = 0; j < bankIds.length; j++ ){
					String itemId = bankIds[j];
					if(itemId != null){
						if( !"".equals(bankIdParam) ){
							bankIdParam = bankIdParam + ",'"+itemId+"'";
						}else{
							bankIdParam = "'"+itemId+"'";
						}
					}
				}
				if( !"".equals(bankIdParam)){
					bankIdParam = "("+bankIdParam+")";
				}
				
				// 删除题目
				Integer eid = exer.getId();
				ExerTopic et = new ExerTopic();
				et.setEid(eid);
				exerTopicMapper.schExerTopic(et);
				exerTopicMapper.delByExerId(eid);
				
				ExerStep ste = new ExerStep();
				ste.setEid(eid);
				List<ExerStep> stes = exerStepMapper.schExerStep(ste);
				if(stes != null && stes.size() > 0){
					for(ExerStep item : stes){
						if(item != null && item.getId() != null){
							exerAnswerMapper// 根据stepId（esid）删除所有学生作答
							.delExerAnswerByEsid(item.getId());// 根据stepId（esid）删除所有学生作答
							
						}
					}
				}
				
				// 删除步骤
				Map<String,Object> delStep = new HashMap<String,Object>();
				delStep.put("eid", eid);
				exerStepMapper.delStepByParam(delStep);
				
				List<BankTopic> topicCatch = new ArrayList<BankTopic>();
				// 根据题型排序循环获取数据
				for(int i = 0; i < typeSort.length; i++){
					String type = typeSort[i];// 题目类型
					List<BankTopic> ts = addTopicByType(type, numParam, eid, bankIdParam);
					if(ts != null && ts.size() > 0){
						topicCatch.addAll(ts);
					}
				}
				updateToCopy(topicCatch,eid ,request, "auto");
				
				List<ExerTopic> topics = exerTopicMapper.schExerTopicByEid(eid);
				if(topics != null){
					num = topics.size();
				}
			}
		}
		return num;
	}
	
	/**
	 * 根据题型添加数据
	 * @param type
	 * @param numParam
	 * @param eid
	 * @param bankIdPram
	 * @return
	 */
	private List<BankTopic> addTopicByType(String type, Map<String,Object> numParam,Integer eid,String bankIdPram){
		List<BankTopic> topics = new ArrayList<BankTopic>();
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("bankId",bankIdPram );
		param.put("topicType", TopicTypeConstant.THEORY);//题目类型为【理论题】 
		if(TopicTypeConstant.SINGLE.equals(type)){// 单选
			
			// 单选题分数
			int singleScore = numParam.get("singleScore") == null 
					|| "".equals(numParam.get("singleScore")) ? 0 : Integer.valueOf(numParam.get("singleScore").toString());
			// 单选易
			int singleNumY = numParam.get("singleNumY") == null 
					|| "" .equals(numParam.get("singleNumY")) ? 0:Integer.valueOf(numParam.get("singleNumY").toString());
			// 单选中
			int singleNumZ = numParam.get("singleNumZ") == null
					|| "" .equals(numParam.get("singleNumZ"))? 0:Integer.valueOf(numParam.get("singleNumZ").toString());
			// 单选难
			int singleNumN = numParam.get("singleNumN") == null
					|| "" .equals(numParam.get("singleNumN"))? 0:Integer.valueOf(numParam.get("singleNumN").toString());
			if(singleNumY > 0){
				catchTopic(param,TopicTypeConstant.SINGLE,TopicTypeConstant.PRIMARY,topics,singleNumY,singleScore);
			}
			if(singleNumZ > 0){
				catchTopic(param,TopicTypeConstant.SINGLE,TopicTypeConstant.MIDDLE,topics,singleNumY,singleScore);
			}
			if(singleNumN > 0){
				catchTopic(param,TopicTypeConstant.SINGLE,TopicTypeConstant.HIGH,topics,singleNumY,singleScore);
			}
		}
		if(TopicTypeConstant.MULTI.equals(type)){// 多选
			// 多选分数
			int multiScore = numParam.get("multiScore") == null 
					|| "".equals(numParam.get("multiScore")) ? 0 : Integer.valueOf(numParam.get("multiScore").toString());
			// 多选易
			int multiNumY = numParam.get("multiNumY") == null 
					|| "" .equals(numParam.get("multiNumY"))? 0:Integer.valueOf(numParam.get("multiNumY").toString());
			// 多选中
			int multiNumZ = numParam.get("multiNumZ") == null 
					|| "" .equals(numParam.get("multiNumZ"))? 0:Integer.valueOf(numParam.get("multiNumZ").toString());
			// 多选难
			int multiNumN = numParam.get("multiNumN") == null 
					|| "" .equals(numParam.get("multiNumN"))? 0:Integer.valueOf(numParam.get("multiNumN").toString());
			if(multiNumY > 0){
				catchTopic(param,TopicTypeConstant.MULTI,TopicTypeConstant.PRIMARY,topics,multiNumY,multiScore);
			}
			if(multiNumZ > 0){
				catchTopic(param,TopicTypeConstant.MULTI,TopicTypeConstant.MIDDLE,topics,multiNumZ,multiScore);
			}
			if(multiNumN > 0){
				catchTopic(param,TopicTypeConstant.MULTI,TopicTypeConstant.HIGH,topics,multiNumN,multiScore);
			}
		}
		if(TopicTypeConstant.JUDGE.equals(type)){// 判断
			// 判断分数
			int judgeScore = numParam.get("judgeScore") == null 
					|| "".equals(numParam.get("judgeScore")) ? 0 : Integer.valueOf(numParam.get("judgeScore").toString());
			// 判断易
			int judgeNumY = numParam.get("judgeNumY") == null 
					|| "" .equals(numParam.get("judgeNumY"))? 0:Integer.valueOf(numParam.get("judgeNumY").toString());
			// 判断中
			int judgeNumZ = numParam.get("judgeNumZ") == null 
					|| "" .equals(numParam.get("judgeNumZ"))? 0:Integer.valueOf(numParam.get("judgeNumZ").toString());
			// 判断难
			int judgeNumN = numParam.get("judgeNumN") == null 
					|| "" .equals(numParam.get("judgeNumN"))? 0:Integer.valueOf(numParam.get("judgeNumN").toString());
			if(judgeNumY > 0){
				catchTopic(param,TopicTypeConstant.JUDGE,TopicTypeConstant.PRIMARY,topics,judgeNumY,judgeScore);
			}
			if(judgeNumZ > 0){
				catchTopic(param,TopicTypeConstant.JUDGE,TopicTypeConstant.MIDDLE,topics,judgeNumZ,judgeScore);
			}
			if(judgeNumN > 0){
				catchTopic(param,TopicTypeConstant.JUDGE,TopicTypeConstant.HIGH,topics,judgeNumN,judgeScore);
			}
		}
		if(TopicTypeConstant.CLOZED.equals(type)){// 填空
			// 天空分数
			int clozedScore = numParam.get("clozedScore") == null 
					|| "".equals(numParam.get("clozedScore")) ? 0 : Integer.valueOf(numParam.get("clozedScore").toString());
			// 填空易
			int clozedNumY = numParam.get("clozedNumY") == null 
					|| "" .equals(numParam.get("clozedNumY"))? 0:Integer.valueOf(numParam.get("clozedNumY").toString());
			// 填空中
			int clozedNumZ = numParam.get("clozedNumZ") == null 
					|| "" .equals(numParam.get("clozedNumZ"))? 0:Integer.valueOf(numParam.get("clozedNumZ").toString());
			// 填空难
			int clozedNumN = numParam.get("clozedNumN") == null 
					|| "" .equals(numParam.get("clozedNumN"))? 0:Integer.valueOf(numParam.get("clozedNumN").toString());
			if(clozedNumY > 0){
				catchTopic(param,TopicTypeConstant.CLOZED,TopicTypeConstant.PRIMARY,topics,clozedNumY,clozedScore);
			}
			if(clozedNumZ > 0){
				catchTopic(param,TopicTypeConstant.CLOZED,TopicTypeConstant.MIDDLE,topics,clozedNumZ,clozedScore);
			}
			if(clozedNumN > 0){
				catchTopic(param,TopicTypeConstant.CLOZED,TopicTypeConstant.HIGH,topics,clozedNumN,clozedScore);
			}
		}
		if(TopicTypeConstant.ANSWER.equals(type)){// 问答
			
			// 问答分数
			int answerScore = numParam.get("answerScore") == null 
					|| "".equals(numParam.get("answerScore")) ? 0 : Integer.valueOf(numParam.get("answerScore").toString());
			// 问答易
			int answerNumY = numParam.get("answerNumY") == null 
					|| "" .equals(numParam.get("answerNumY"))? 0:Integer.valueOf(numParam.get("answerNumY").toString());
			// 问答中
			int answerNumZ = numParam.get("answerNumZ") == null 
					|| "" .equals(numParam.get("answerNumZ"))? 0:Integer.valueOf(numParam.get("answerNumZ").toString());
			// 问答难
			int answerNumN = numParam.get("answerNumN") == null 
					|| "" .equals(numParam.get("answerNumN"))? 0:Integer.valueOf(numParam.get("answerNumN").toString());
			if(answerNumY > 0){
				catchTopic(param,TopicTypeConstant.ANSWER,TopicTypeConstant.PRIMARY,topics,answerNumY,answerScore);
			}
			if(answerNumZ > 0){
				catchTopic(param,TopicTypeConstant.ANSWER,TopicTypeConstant.MIDDLE,topics,answerNumZ,answerScore);
			}
			if(answerNumN > 0){
				catchTopic(param,TopicTypeConstant.ANSWER,TopicTypeConstant.HIGH,topics,answerNumN,answerScore);
			}
		}
		return topics;
	}
	
	// 添加数据（自动组卷用）
	private void catchTopic(Map<String,Object> param , String stepType,
			String difficulty , List<BankTopic> topics ,int typeNum, int score){
		param.put("stepType", "('"+stepType+"')");// 步骤类型为单选
		param.put("difficulty",difficulty);// 难度为简单
		List<BankTopic> ys = bankStepMapper.schStepByAutoPage(param);
		if( ys != null && ys.size() > 0 ){
			int size = ys.size();
			if(size == 1 || size <= typeNum ){
				topics.addAll(ys);
			}else{
				if(typeNum < size){
					HashSet<Integer> set = new HashSet<Integer>();
					RandomUtils.randomSet(0, (ys.size()-1), typeNum,set);
					//int[] nums = RandomUtils.myRandom(0, (ys.size()-1), typeNum);
					for (Integer i : set) {
						BankTopic bt = ys.get(i);
						bt.setTotalScore(score);// 设置分数
						topics.add(bt);
					}
				}
			}
		}
	}
	
	/**
	 * 取消所选试卷和题目
	 */
	public void updCancel(Exer exer){
		// 查询试卷
		Exer exeParam = exerMapper.schExerById(exer.getId());
		if(exeParam != null){
			if( IsSaveConstant.NO_CODE.equals(exeParam.getIsSave()) ){// 状态为未保存的试卷
				// 删除此试卷
				exerMapper.delExer(exer.getId());
				// 删除试卷下的题目
				exerTopicMapper.delByExerId(exer.getId());
			}else{
				List<ExerTopic> topics = exerTopicMapper.schExerTopicByEid(exeParam.getId());
				if(topics != null){
					exeParam.setTotalNum(topics.size());
				}else{
					exeParam.setTotalNum(0);
				}
				exerMapper.updExer(exeParam);
			}
		}
	}
	
	/**
	 * Exer的单记录删除方法
	 * @param id
	 * 要删除的主键值
	 */
	@Override
	public void delExer(Integer id) {
		exerMapper.delExer(id);// 删除数据
	}

	/**
	 * Exer的批量删除方法
	 * @param ids
	 * 主键值的数组
	 */
	@Override
	public void delExer(Integer[] ids) {
		exerMapper.delExers(ids);// 删除数据
	}

	/**
	 * 删除数据
	 * @param ids
	 */
	public void delExerAndTopic(Integer[] ids) {
		if (ids != null && ids.length > 0) {// 判断IDS是否为空
			for (Integer id : ids) {// 循环
				if (id != null) {// 判断id是否为null
					ExerTopic exerTopic = new ExerTopic();// 实例化对象
					exerTopic.setEid(id);// 设置考试id
					List<ExerTopic> list = exerTopicMapper// 调用Service查询数据
							.schExerTopic(exerTopic); // 调用Service查询数据
					if (list != null && list.size() > 0) {// 判断list是否为null
						for (ExerTopic topic : list) {// 循环
							if (topic != null) {// 判断对象是否为空
								Map<String, Object> param = new HashMap<String, Object>();// 实例化参数集合
								param.put("etid", topic.getId());// 设置参数
								exerStepMapper.delStepByParam(param);//删除步骤
								exerTopicMapper.delExerTopic(topic.getId());//删除题目
							}
						}
					}
					exerMapper.delExer(id);//删除练习
				}
			}
		}
	}

	@Autowired
	private ExerAnswerMapper exerAnswerMapper;//声明模块Mapper实例

	/***************** ExerAnswer方法组 *****************/
	/**
	 * ExerAnswer的条件查询方法
	 * @param exerAnswer
	 * 承载查询条件的Bean
	 * @return 返回ExerAnswer的集合
	 */
	@Override
	public List<ExerAnswer> schExerAnswer(ExerAnswer exerAnswer) {
		PageHelper.startPage(exerAnswer.getPage(), exerAnswer.getPagesize());// 设置分页显示
		List<ExerAnswer> list = exerAnswerMapper.schExerAnswer(exerAnswer);// 查询数据
		Page<ExerAnswer> page = (Page<ExerAnswer>) list;// 为了设置总记录数先类型强转
		exerAnswer.setDatasize(page.getTotal());// 设置分页数据
		return list;// 返回结果集
	}

	public List<ExerAnswer> schExerAnswerList(ExerAnswer exerAnswer){
		List<ExerAnswer> list = exerAnswerMapper.schExerAnswer(exerAnswer);// 查询数据
		return list;// 返回结果集
	}
	
	
	/**
	 * ExerAnswer的主键查询方法
	 * @param id 主键值
	 * @return 返回ExerAnswer实体
	 */
	@Override
	public ExerAnswer schExerAnswerById(Integer id) {
		return exerAnswerMapper.schExerAnswerById(id);// 返回结果集
	}

	/**
	 * ExerAnswer的添加方法
	 * @param exerAnswer
	 * 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	@Override
	public Integer addExerAnswer(ExerAnswer exerAnswer) {
		return exerAnswerMapper.addExerAnswer(exerAnswer);// 返回结果集
	}

	/**
	 * ExerAnswer的修改方法
	 * @param exerAnswer
	 * 承载数据的Bean
	 */
	@Override
	public void updExerAnswer(ExerAnswer exerAnswer) {
		exerAnswerMapper.updExerAnswer(exerAnswer);// 更新数据
	}

	/**
	 * ExerAnswer的单记录删除方法
	 * @param id
	 * 要删除的主键值
	 */
	@Override
	public void delExerAnswer(Integer id) {
		exerAnswerMapper.delExerAnswer(id);//删除数据
	}

	/**
	 * ExerAnswer的批量删除方法
	 * @param ids
	 * 主键值的数组
	 */
	@Override
	public void delExerAnswer(Integer[] ids) {
		exerAnswerMapper.delExerAnswers(ids);// 删除数据
	}

	@Autowired
	private ExerPaperMapper exerPaperMapper;//声明模块Mapper实例

	/***************** ExerPaper方法组 *****************/
	/**
	 * ExerPaper的条件查询方法
	 * @param exerPaper
	 * 承载查询条件的Bean
	 * @return 返回ExerPaper的集合
	 */
	@Override
	public List<ExerPaper> schExerPaper(ExerPaper exerPaper) {
		PageHelper.startPage(exerPaper.getPage(), exerPaper.getPagesize());// 设置分页显示
		List<ExerPaper> list = exerPaperMapper.schExerPaper(exerPaper);// 查询数据
		Page<ExerPaper> page = (Page<ExerPaper>) list;// 为了设置总记录数先类型强转
		exerPaper.setDatasize(page.getTotal());// 设置数据长度
		return list;// 返回结果集
	}

	/**
	 * ExerPaper的主键查询方法
	 * @param id
	 * 主键值
	 * @return 返回ExerPaper实体
	 */
	@Override
	public ExerPaper schExerPaperById(Integer id) {
		return exerPaperMapper.schExerPaperById(id);// 返回结果集
	}

	/**
	 * ExerPaper的添加方法
	 * @param exerPaper
	 * 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	@Override
	public Integer addExerPaper(ExerPaper exerPaper) {
		return exerPaperMapper.addExerPaper(exerPaper);// 返回结果集
	}

	/**
	 * ExerPaper的修改方法
	 * @param exerPaper
	 * 承载数据的Bean
	 */
	@Override
	public void updExerPaper(ExerPaper exerPaper) {
		exerPaperMapper.updExerPaper(exerPaper);// 更新数据
	}

	/**
	 * ExerPaper的单记录删除方法
	 * @param id
	 * 要删除的主键值
	 */
	@Override
	public void delExerPaper(Integer id) {
		exerPaperMapper.delExerPaper(id);// 删除数据
	}

	/**
	 * ExerPaper的批量删除方法
	 * @param ids
	 * 主键值的数组
	 */
	@Override
	public void delExerPaper(Integer[] ids) {
		exerPaperMapper.delExerPapers(ids);// 删除数据
	}

	@Autowired
	private ExerTopicMapper exerTopicMapper; // 实例化Mapper调用

	/***************** ExerTopic方法组 *****************/
	/**
	 * ExerTopic的条件查询方法
	 * @param exerTopic
	 * 承载查询条件的Bean
	 * @return 返回ExerTopic的集合
	 */
	@Override
	public List<ExerTopic> schExerTopic(ExerTopic exerTopic) {
		PageHelper.startPage(exerTopic.getPage(), exerTopic.getPagesize());// 设置分页显示
		List<ExerTopic> list = exerTopicMapper.schExerTopic(exerTopic);// 查询数据
		Page<ExerTopic> page = (Page<ExerTopic>) list;// 为了设置总记录数先类型强转
		exerTopic.setDatasize(page.getTotal());// 设置数据长度
		return list;// 返回结果集
	}

	/**
	 * 查询topic列表（无分页） 
	 * @param exerTopic
	 * @return
	 */
	public List<ExerTopic> schExerTopicList(ExerTopic exerTopic) {
		List<ExerTopic> list = exerTopicMapper.schExerTopic(exerTopic);// 查询数据
		return list;// 返回结果集
	}

	/**
	 * ExerTopic的主键查询方法
	 * @param id 主键值
	 * @return 返回ExerTopic实体
	 */
	@Override
	public ExerTopic schExerTopicById(Integer id) {
		return exerTopicMapper.schExerTopicById(id);// 返回结果集
	}

	/**
	 * ExerTopic的添加方法
	 * @param exerTopic
	 * 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	@Override
	public Integer addExerTopic(ExerTopic exerTopic) {
		return exerTopicMapper.addExerTopic(exerTopic);// 添加数据
	}

	/**
	 * ExerTopic的修改方法
	 * @param exerTopic
	 * 承载数据的Bean
	 */
	@Override
	public void updExerTopic(ExerTopic exerTopic) {
		exerTopicMapper.updExerTopic(exerTopic);// 更新数据
	}

	/**
	 * ExerTopic的单记录删除方法
	 * @param id
	 * 要删除的主键值
	 */
	@Override
	public void delExerTopic(Integer id) {
		exerTopicMapper.delExerTopic(id);// 删除数据
	}

	/**
	 * ExerTopic的批量删除方法
	 * @param ids
	 * 主键值的数组
	 */
	@Override
	public void delExerTopic(Integer[] ids) {
		exerTopicMapper.delExerTopics(ids);// 删除数据
	}

	/**
	 * 删除topic和step和answer
	 * @param ids
	 */
	public void delExerTopicAndStep(Integer[] ids) {
		if (ids != null && ids.length > 0) {// 判断参数是否为空
			for (Integer id : ids) {// 循环
				if (id != null) {// 判断参数是否为null
					ExerTopic exerTopic = exerTopicMapper.schExerTopicById(id);// 查询数据
					if (exerTopic != null) {// 判断参数是否为null
						ExerStep exerStep = new ExerStep();// 实例化对象
						exerStep.setEid(exerTopic.getEid());// 设置eid
						exerStep.setEtid(id);//设置etid
						List<ExerStep> stepList = exerStepMapper// 查询数据
								.schExerStep(exerStep);// 查询数据
						if (stepList != null && stepList.size() > 0) {// 判断参数是否为空
							for (int i = 0; i < stepList.size(); i++) {// 循环
								if (stepList.get(i) != null// 判断参数是否为空
										&& stepList.get(i).getId() != null) {// 判断参数是否为空
									
									exerAnswerMapper// 根据stepId（esid）删除所有学生作答
											.delExerAnswerByEsid(stepList// 根据stepId（esid）删除所有学生作答
													.get(i).getId());// 根据stepId（esid）删除所有学生作答
									exerStepMapper.delExerStep(stepList.get(i)// 根据stepId删除step
											.getId());// 根据stepId删除step
								}
							}
						}
						exerTopicMapper.delExerTopic(id);// 根据topicId删除topic
					}
				}
			}
		}
	}

	/**
	 * 查询所有
	 * @param exerTopic
	 * 承载查询条件的Bean
	 * @return 返回ExerTopic的集合
	 */
	@Override
	public List<ExerTopic> schAllExerTopic(ExerTopic exerTopic) {
		return exerTopicMapper.schExerTopic(exerTopic);// 返回查询结果
	}

	@Autowired
	private BankTopicMapper bankTopicMapper;// 实例化Mapper调用
	@Autowired
	private BankStepMapper bankStepMapper;// 实例化Mapper调用

	/**
	 * 复制数据（组织练习）
	 * @param param
	 * @return
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 * @throws ParseException
	 * @throws DocumentException
	 * @throws TemplateException
	 * @throws IOException
	 * @throws JDOMException
	 */
	public boolean updateToCopy(List<BankTopic> bankTopicList, Integer eid
			,HttpServletRequest request, String type) throws RuntimeException,
			DocumentException, IOException, TemplateException, JDOMException {
		if (bankTopicList != null && bankTopicList.size() > 0) {// 判断参数是否为空
			for (int i = 0; i < bankTopicList.size(); i++) {// 循环调用
				BankTopic bankTopic = bankTopicList.get(i);//获取元素
				if (bankTopic != null) {// 判断参数是否为空
					ExerTopic exerTopic = new ExerTopic();// 实例化对像
					exerTopic.setEid(eid);//设置eid
					exerTopic.setBankId(bankTopic.getBankId());// 设置bankid
					exerTopic.setTopicId(bankTopic.getTopicId());// 设置topicid
					boolean saveFlg = checkHas(exerTopic);//是否拥有题目
					if (saveFlg) {// 含有标识
						Map<String, Object> param = new HashMap<String, Object>();// 参数集合
						param.put("eid", exerTopic.getEid());// 设置eid
						Integer maxOrder = exerTopicMapper//获取最大排序
								.getMaxOrderByParam(param);//获取最大排序
						exerTopic.setOrderNo(maxOrder == null ? 0////设置排序号
								: maxOrder + 1);//设置排序号
						exerTopic.setRemark(bankTopic.getRemark());//设置 备注
						exerTopic.setTitle(bankTopic.getTitle());//设置 标题
						exerTopic.setType(bankTopic.getType());//设置 类型
						exerTopic.setLevel(bankTopic.getLevel());//设置 级别
						exerTopic.setProfession(bankTopic.getProfession());//设置 行业
						exerTopic.setDifficulty(bankTopic.getDifficulty());//设置 难度
						exerTopic.setAuthor(bankTopic.getAuthor());//设置 作者
						exerTopic.setMergeVoice(bankTopic.getMergeVoice());//设置 合并录音
						exerTopic.setTotalScore(bankTopic.getTotalScore());//设置 总分
						exerTopic.setPassScore(bankTopic.getPassScore());//设置 及格分
						exerTopic.setBeginStepId(bankTopic.getBeginStepId());//设置属性 BeginStepId
						exerTopic.setStatus(bankTopic.getStatus());//设置属性 Status
						exerTopic.setDocATitle(bankTopic.getDocATitle());//设置 ATitle
						exerTopic.setDocBTitle(bankTopic.getDocBTitle());//设置 BTitle
						exerTopic.setDocCTitle(bankTopic.getDocCTitle());//设置 CTitle
						exerTopic.setDocDTitle(bankTopic.getDocDTitle());//设置 DTitle
						exerTopic.setDocETitle(bankTopic.getDocETitle());//设置 ETitle
						exerTopic.setDocAText(bankTopic.getDocATmplt());//设置 AText
						exerTopic.setDocBText(bankTopic.getDocBTmplt());//设置 BText
						exerTopic.setDocCText(bankTopic.getDocCTmplt());//设置 CText
						exerTopic.setDocDText(bankTopic.getDocDTmplt());//设置 DText
						exerTopic.setDocEText(bankTopic.getDocETmplt());//设置 EText
						exerTopic.setCustomer(bankTopic.getCustomer());//设置 
						exerTopic.setKeyWords(bankTopic.getKeyWords());//设置 关键字
						exerTopic.setBookmodul(bankTopic.getBookmodul());//设置属性
						exerTopic.setCapability(bankTopic.getCapability());//设置属性
						exerTopic.setTalkSkill(bankTopic.getTalkSkill());//设置属性
						exerTopic.setTemplateId(bankTopic.getTemplateId());// 设置模板属性
						if ("".equals(type) && bankTopic.getBookId() != null) {// 判断参数是否为空
							exerTopic.setBookId(bankTopic.getBookId().toString());//设置课本
						}
						exerTopic.setCreattime(new Date());// 设置创建时间
						exerTopic.setCreateTime(new Date());// 设置创建时间
						// 保存topic
						exerTopicMapper.addExerTopic(exerTopic);//添加练习题目
						addToCopyStep(exerTopic, exerTopic.getId(), request,type);// 复制step
					}
				}
			}
		}
		return true;// 返回状态
	}

	/**
	 * 检查是否已经含有该数据 
	 * @param exerTopic
	 * @return
	 */
	private boolean checkHas(ExerTopic exerTopic) {
		List<ExerTopic> result = exerTopicMapper.schExerTopic(exerTopic);//查询练习题目
		if (result != null && result.size() > 0) {// 判断集合长度
			for(ExerTopic topic : result){// 循环
				Map<String,Object> param = new HashMap<String,Object>();// 参数集合
				param.put("etid", topic.getId());//放入题目id
				exerStepMapper.delStepByParam(param);//删除步骤
				exerTopicMapper.delExerTopic(topic.getId());//删除练习题目
			}
		} 
		return true;// 返回成功状态
	}

	/**
	 * 复制数据(操作步骤)
	 * @param exerTopic
	 * @throws DocumentException
	 * @throws TemplateException
	 * @throws IOException
	 * @throws JDOMException
	 */
	private void addToCopyStep(ExerTopic exerTopic, Integer id,
			HttpServletRequest request, String optType) {
		if (exerTopic == null) {// 判断对象是否等于null
			return;// 返回
		}
		try {
			BankStep bankStep = new BankStep();// 实例化对象
			bankStep.setBankId(exerTopic.getBankId());// 设置参数
			bankStep.setTopicId(exerTopic.getTopicId());// 设置参数
			
			List<BankStep> bankStepList = bankStepMapper.schBankStep(bankStep);//查询题库步骤
			if (bankStepList != null && bankStepList.size() > 0) {// 判断集合是否为空
				for (int j = 0; j < bankStepList.size(); j++) {// 循环
					BankStep bSte = bankStepList.get(j);// 获取第j个元素
					if("begin".equals(bSte.getType())||"end".equals(bSte.getType())){//去除步骤type为begin和end的集合数据
						bankStepList.remove(j);//去除步骤type为begin和end的集合数据
					}
				}
				
				for (int i = 0; i < bankStepList.size(); i++) {//循环
					BankStep bankSte = bankStepList.get(i);// 获取第i个元素
					if (bankSte != null) {// 判断对象是否为空
						ExerStep exerStep = new ExerStep();// 实例化对象
						exerStep.setEid(exerTopic.getEid());//设置练习id
						exerStep.setEtid(id);//设置练习题id
						exerStep.setStep(bankSte.getStepId());//设置属性
						exerStep.setType(bankSte.getType());//设置属性
						exerStep.setTitle(bankSte.getTitle());//设置属性
						String stepInfo = bankSte.getParam();//设置属性
						/*if(!"实训营地".equals(exerTopic.getType())){// 不是实训营地的场合，拆分填空题的分数
							stepInfo = doClozeInfo(stepInfo);//拆分填空题分数
						}*/
						exerStep.setStepinfo(stepInfo);//设置步骤信息
						if("auto".equals(optType)){
							String score = exerTopic.getTotalScore()==null?"0":exerTopic.getTotalScore().toString();
							exerStep.setScore(score);//设置分数
						}else{
							exerStep.setScore(bankSte.getScore());//设置分数
						}
						
						exerStep.setStandardAnswer(bankSte.getStandardAnswer());//设置标准答案
						exerStep.setOrderNo(bankSte.getOrderNo());//设置排序
						String type = bankSte.getType();
						exerStepMapper.addExerStep(exerStep);//添加步骤
						if (ExerConstant.STEP_BEGIN_ID.equalsIgnoreCase(type)
								|| ExerConstant.STEP_END_ID.equalsIgnoreCase(type)
								|| TopicTypeConstant.CAMPING_STR.equals(exerTopic.getType())
								|| TopicTypeConstant.CAMPING.equals(exerTopic.getType())) {
							continue;//跳过开始结束和实训营地
						}
						Integer esid = exerStep.getId();// 获取参数
						Integer etid = exerTopic.getId();// 获取参数
						Integer eid = exerTopic.getEid();// 获取参数
						String bankId = bankSte.getBankId();// 获取参数
						String topicId = bankSte.getTopicId();// 获取参数
						FreemakerTools.releaseStepHtml(eid, etid, esid, stepInfo,//生成html
							request, bankId, topicId,bankSte);//生成html
						
					}
				}
			}
		} catch (IOException | TemplateException
				| DocumentException | JDOMException e) {
			e.printStackTrace();
			throw new RuntimeException(e);// 抛出运行时异常
		}
	}

	/**
	 * 修改填空题的分数
	 * @return
	 * @throws IOException 
	 * @throws JDOMException 
	 * @throws DocumentException 
	 */
	private String doClozeInfo(String xml) throws DocumentException, JDOMException, IOException{
		String result = xml;// 定义常量
		if(xml != null){// 判断是否为null
			StepInfo stepInfo = FreemakerTools.xmlToStep(xml, "");// 调用xmlToStep
			if(ExerConstant.TOPIC_STEP_TYPE_QUESTION_CLOZE.equals(stepInfo.getType())){// 判断参数
				StringReader xmlReader = new StringReader(xml);// json对象
				/** **创建新的输入源SAX 解析器将使用 InputSource 对象来确定如何读取 XML 输入 */
				InputSource xmlSource = new InputSource(xmlReader);
				/** *创建一个SAXBuilder* */
				SAXBuilder builder = new SAXBuilder();
				try{
					/** *通过输入源SAX构造一个Document** */
					Document doc = builder.build(xmlSource);
					Element root = doc.getRootElement();// 获取root节点
					Element scorEle = root.getChild("score");// 获取score子节点
					Element answEle = root.getChild(ExerConstant.STEP_STANDARD_ANSWER);// 获取子节点
					if(scorEle != null && scorEle.getText() != null // 判断参数是否为null
							&& answEle != null && answEle.getText() != null){// 判断参数是否为null
						String[] scoArry =  scorEle.getText().split(",");// 按逗号拆分
						String[] answArry =  answEle.getText().split(",");// 按逗号拆分
						String newScore = "";// 定义参数
						if(scoArry != null && scoArry.length == 1 && answArry.length > 1){// 判断长度
							int num = answArry.length;// 获取长度
							float topicScore = Float.valueOf(scoArry[0]);// 转换
							float gpa = topicScore/num;    //每道题每个步骤平均分取整
							for( int i = 0; i < num ; i++){// 循环
								if(i < (num-1)){// 判断大小
									newScore += gpa+",";// 拼接
								}else{
									newScore += gpa;// 拼接
								}
							}
							scorEle.setText(newScore);// 设置内容
						}else{
							newScore = scorEle.getText();
						}
						String pre = xml.split("<score>")[0];//拆分获取
						String aft = xml.split("</score>")[1];//拆分获取
						result = pre+"<score>"+newScore+"</score>"+aft;// 拼接
					}
				}catch(Exception e){
					e.printStackTrace();// 打印异常信息
				}
			}
		}
		return result;// 返回结果
	}
	
	@Autowired
	private RExerStudMapper rExerStudMapper;// 实例化Mapper调用
	@Autowired
	private ExerStepMapper exerStepMapper;// 实例化Mapper调用

	/***************** RExerStud方法组 *****************/
	/***************** ExerStep方法组 *****************/
	/**
	 * RExerStud的条件查询方法
	 * @param rExerStud
	 *  承载查询条件的Bean
	 * @return 返回RExerStud的集合 ExerStep的条件查询方法
	 * @param exerStep
	 * 承载查询条件的Bean
	 * @return 返回ExerStep的集合
	 */
	@Override
	public List<RExerStud> schRExerStud(RExerStud rExerStud) {
		PageHelper.startPage(rExerStud.getPage(), rExerStud.getPagesize());// 设置分页显示
		List<RExerStud> list = rExerStudMapper.schRExerStud(rExerStud);// 查询数据
		Page<RExerStud> page = (Page<RExerStud>) list;// 为了设置总记录数先类型强转
		rExerStud.setDatasize(page.getTotal());// 设置数据长度
		return list;// 返回结果
	}

	/**
	 * 查询列表（教师阅卷页面用）
	 */
	public List<RExerStud> schMarkRExerStud(RExerStud rExerStud){
		PageHelper.startPage(rExerStud.getPage(), rExerStud.getPagesize());// 设置分页显示
		List<RExerStud> list = rExerStudMapper.schMarkRExerStud(rExerStud);// 查询数据
		Page<RExerStud> page = (Page<RExerStud>) list;// 为了设置总记录数先类型强转
		rExerStud.setDatasize(page.getTotal());// 设置数据长度
		return list;// 返回结果
	}
	
	/**
	 * 查询列表（教师阅卷页面用）(无分页)
	 */
	public List<RExerStud> schMarkRExerStudList(RExerStud rExerStud){
		List<RExerStud> list = rExerStudMapper.schMarkRExerStud(rExerStud);// 查询数据
		return list;// 返回结果
	}
	
	
	/**
	 * RExerStud的条件查询方法(无分页)
	 * @param rExerStud
	 *  承载查询条件的Bean
	 * @return 返回RExerStud的集合 ExerStep的条件查询方法
	 * @param exerStep
	 * 承载查询条件的Bean
	 * @return 返回ExerStep的集合
	 */
	@Override
	public List<RExerStud> schRExerStudList(RExerStud rExerStud) {
		List<RExerStud> list = rExerStudMapper.schRExerStud(rExerStud);// 查询数据
		return list;// 返回结果
	}
	
	/**
	 * 查询步骤
	 * @param exerStep
	 */
	public List<ExerStep> schExerStep(ExerStep exerStep) {
		PageHelper.startPage(exerStep.getPage(), exerStep.getPagesize());// 设置分页显示
		List<ExerStep> list = exerStepMapper.schExerStep(exerStep);// 查询数据
		Page<ExerStep> page = (Page<ExerStep>) list;// 为了设置总记录数先类型强转
		exerStep.setDatasize(page.getTotal());// 设置数据长度
		return list;// 返回结果
	}

	/**
	 * 查询step列表（无分页） 
	 * @param exerStep
	 * @return
	 */
	public List<ExerStep> schExerStepList(ExerStep exerStep) {
		List<ExerStep> list = exerStepMapper.schExerStep(exerStep);// 查询数据
		return list;// 返回结果
	}

	/**
	 * RExerStud的主键查询方法 ExerStep的主键查询方法
	 * @param id
	 * 主键值
	 * @return 返回RExerStud实体
	 * @return 返回ExerStep实体
	 */
	@Override
	public RExerStud schRExerStudById(Integer id) {
		return rExerStudMapper.schRExerStudById(id);// 返回结果
	}

	/**
	 * 根据id查询数据
	 * @param id
	 */
	public ExerStep schExerStepById(Integer id) {
		return exerStepMapper.schExerStepById(id);// 返回结果
	}

	/**
	 * RExerStud的添加方法
	 * @param rExerStud
	 * 承载数据的Bean，执行添加后会更新入库后的主键值 ExerStep的添加方法
	 * @param exerStep
	 * 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	@Override
	public Integer addRExerStud(RExerStud rExerStud) {
		return rExerStudMapper.addRExerStud(rExerStud);// 添加数据
	}

	/**
	 * 添加步骤
	 * @param exerStep
	 */
	public Integer addExerStep(ExerStep exerStep) {
		return exerStepMapper.addExerStep(exerStep);// 添加数据
	}

	/**
	 * 保存练习与学生的关系 
	 * @param stuIds
	 * @param eid
	 * @param classId
	 */
	public void addGx(String stuIds, String eid, String classId) {
		if (stuIds != null) {// 判断参数是否为null
			String[] stuArg = stuIds.split(",");// 拆分参数
			if (stuArg != null && stuArg.length > 0) {// 判断集合是否为空
				for (String str : stuArg) {// 循环
					if (str != null && !"".equals(str)) {// 判断参数是否为空
						RExerStud rExerStud = new RExerStud();//设置参数
						rExerStud.setEid(Integer.valueOf(eid));//设置参数
						rExerStud.setClassid(classId);//设置参数
						rExerStud.setStudid(str);//设置参数
						Exer exer = exerMapper//查询数据
								.schExerById(Integer.valueOf(eid));//查询练习
						List<RExerStud> resultList = rExerStudMapper//查询数据
								.schRExerStud(rExerStud);//查询学生练习
						if (resultList == null || resultList.size() == 0// 判断参数是否为空
								&& exer != null) {// 判断参数是否为空
							rExerStud.setName(exer.getName());// 试卷名
							rExerStud.setStatus(0); // 默认值0
							rExerStud.setStatusReady(0);// 默认值0
							rExerStud.setCreateTime(new Date());
							rExerStudMapper.addRExerStud(rExerStud);//添加中间数据
						}
					}
				}
			}
		}
	}

	/**
	 * RExerStud的修改方法
	 * @param rExerStud
	 * 承载数据的Bean ExerStep的修改方法
	 * @param exerStep
	 * 承载数据的Bean
	 */
	@Override
	public void updRExerStud(RExerStud rExerStud) {
		rExerStudMapper.updRExerStud(rExerStud);// 更新数据
	}
	
	/**
	 * 更新练习步骤
	 * @param exerStep
	 */
	public void updExerStep(ExerStep exerStep) {
		exerStepMapper.updExerStep(exerStep);// 更新数据
	}

	/**
	 * RExerStud的单记录删除方法 ExerStep的单记录删除方法
	 * @param id
	 * 要删除的主键值
	 */
	@Override
	public void delRExerStud(Integer id) {
		rExerStudMapper.delRExerStud(id);// 删除数据
	}
	/**
	 * 删除练习步骤
	 * @param id
	 */
	public void delExerStep(Integer id) {
		exerStepMapper.delExerStep(id);// 删除数据
	}

	/**
	 * RExerStud的批量删除方法 ExerStep的批量删除方法
	 * @param ids
	 * 主键值的数组
	 */
	@Override
	public void delRExerStud(Integer[] ids) {
		rExerStudMapper.delRExerStuds(ids);// 删除数据
	}
	
	/**
	 * 删除练习步骤
	 * @param ids
	 */
	public void delExerStep(Integer[] ids) {
		exerStepMapper.delExerSteps(ids);// 删除数据
	}

	/**
	 * 根据其他参数删除数据
	 * @param param
	 */
	public void delReStudents(Map<String, Object> param) {
		rExerStudMapper.delReStudents(param);// 删除数据
	}

	/**
	 * RExerStud的根据班级、试卷分组查询方法
	 * @param classid 班级号
	 * @return 返回RExerStud的集合
	 */
	@Override
	public List<RExerStud> schRExerStudBjExer(String classid,String name) {
		List<RExerStud> list = rExerStudMapper.schRExerStudBjExer(classid,name);// 查询数据
		return list;// 返回结果
	}
	
	/**
	 * 查询步骤及学生做题答案 
	 * @param param
	 * @return
	 * @throws IOException
	 * @throws JDOMException
	 * @throws DocumentException
	 */
	public List<ExerStepExt> serchStepAndAnswer(Map<String, Object> param)
			throws DocumentException, JDOMException, IOException {
		List<ExerStepExt> exts = exerStepMapper.serchStepAndAnswer(param);//查询步骤和答案
		if (exts != null && exts.size() > 0) {// 判断参数
			for (int i = 0; i < exts.size(); i++) {// 循环
				ExerStepExt stepExt = exts.get(i);// 获取元素
				if (stepExt != null) {// 判断对象
					String stepXml = stepExt.getStepinfo();//获取步骤信息
					StepInfo stepInfo = FreemakerTools.xmlToStep(stepXml,//xml转换step
							"/cc-edu/upload/data/");//xml转换step
					if (stepInfo != null) {// 判断对象
						if ("cloze".equals(stepInfo.getType())) {// 判断参数
							StringReader xmlReader = new StringReader(stepXml);//读取step
							InputSource xmlSource = new InputSource(xmlReader);//生成输入流
							SAXBuilder builder = new SAXBuilder();// 实例化对象
							/** *通过输入源SAX构造一个Document** */
							Document doc = builder.build(xmlSource);
							Element root = doc.getRootElement();//获取根节点
							Element scorEle = root.getChild("score");//获取分数
							stepInfo.setScore(scorEle != null ? scorEle//设置分数
									.getText() : null);//设置分数
						} else {
							stepInfo.setScore(String.valueOf(stepExt.getScore()));//设置分数
						}
					}
					stepExt.setStepEntity(stepInfo);//设置步骤信息
				}
			}
		}
		return exts;// 返回结果
	}

	/**
	 * 查询答题状态
	 * @param exerTopic
	 * @param studid
	 */
	@Override
	public List<ExerTopic> schExerTopicAndStatus(ExerTopic exerTopic,
			String studid) {
		PageHelper.startPage(exerTopic.getPage(), exerTopic.getPagesize());// 设置分页显示
		exerTopic.setStudid(studid);// 设置参数
		List<ExerTopic> list = exerTopicMapper.schExerTopicAndStatus(exerTopic);// 查询数据
		Page<ExerTopic> page = (Page<ExerTopic>) list;// 为了设置总记录数先类型强转
		exerTopic.setDatasize(page.getTotal());//设置长度
		return list;// 返回结果
	}

	/**
	 * 自动阅卷 
	 * @param param
	 * {eid:练习ID,stuNum：学生学号}
	 * @return
	 * @throws IOException
	 * @throws JDOMException
	 * @throws DocumentException
	 */
	public void updAutoMark(Map<String, Object> param)
			throws DocumentException, JDOMException, IOException {
		Integer eid = (Integer) param.get("eid"); // 练习id
		String stuNum = (String) param.get("stuNum"); // 学生编号
		if (eid != null && stuNum != null) {// 判断参数
			ExerTopic exerTopic = new ExerTopic();// 实例化对象
			exerTopic.setEid(eid);// 设置参数
			exerTopic.setCondition("AND type != '实训营地'");// 设置参数
			List<ExerTopic> topicLis = exerTopicMapper.schExerTopic(exerTopic);// 实例化对象
			float scoreAll = 0;// 设置参数
			if (topicLis != null && topicLis.size() > 0) {// 判断参数
				for (ExerTopic topic : topicLis) {// 循环
					float scare = doMarkStep(topic, eid, stuNum);// 查询步骤开始阅卷
					scoreAll += scare;// 分数相加
				}
			}
			// 查询并更新（更新分数和阅卷状态）关系表
			RExerStud rExerStud = new RExerStud();
			rExerStud.setEid(eid);// 设置参数
			rExerStud.setStudid(stuNum);// 设置参数
			List<RExerStud> reList = rExerStudMapper.schRExerStud(rExerStud);// 查询数据
			if (reList != null && reList.size() > 0 && reList.get(0) != null) {// 判断参数
				RExerStud reStu = reList.get(0);// 获取元素
				reStu.setStatus(1); // 阅卷状态
				reStu.setObjective(scoreAll); // 考卷分数
				float sub = reStu.getSubjective()==null?0f:reStu.getSubjective();//
				reStu.setMark(scoreAll+sub);
				rExerStudMapper.updRExerStud(reStu);// 更新数据
			}
		}
	}

	/**
	 * 自动阅卷-计算topic所对应的step的得分，并求和返回 
	 * @param topic
	 * @param eid
	 * @param stuNum
	 * @return
	 * @throws IOException
	 * @throws JDOMException
	 * @throws DocumentException
	 */
	private float doMarkStep(ExerTopic topic, Integer eid, String stuNum)
			throws DocumentException, JDOMException, IOException {
		float scoreAll = 0;
		if (topic != null && topic.getId() != null) {// 判断参数
			Integer etid = topic.getId();// 设置参数
			Map<String, Object> map = new HashMap<String, Object>();// 实例化对象
			map.put("eid", eid);//存储练习id
			map.put("markType", "EXISTS");// 自动阅卷
			map.put("etid", etid);//存储练习题目id
			List<ExerStep> stepList = exerStepMapper.markList(map);//查询步骤列表
			if (stepList != null && stepList.size() > 0) {// 判断参数
				for (ExerStep step : stepList) {// 循环
					if (step != null && step.getStepinfo() != null) {// 判断参数
						ExerAnswer exerAnswer = new ExerAnswer();// 实例化对象
						exerAnswer.setEid(eid);//设置练习id
						exerAnswer.setEtid(etid);//设置练习题目id
						exerAnswer.setEsid(step.getId());//设置练习步骤id
						exerAnswer.setStudNumber(stuNum);//设置学号
						List<ExerAnswer> answerList = exerAnswerMapper//查询学生答案
								.schExerAnswer(exerAnswer);//查询学生答案
						if (answerList != null && answerList.size() == 1) {// 判断参数
							ExerAnswer stepAnswer = answerList.get(0);// 获取元素
							String answer = step.getStandardAnswer(); // 标准答案
							String stuAnswer = stepAnswer.getAnswer(); // 学生答案
							if (answer != null && stuAnswer != null// 判断参数
									&& !"".equals(answer)// 判断参数
									&& !"".equals(stuAnswer)) {// 判断参数
								StepInfo stepInfo = FreemakerTools.xmlToStep(
										step.getStepinfo(), "");// 调用方法
								
								if (stepInfo != null// 如果是填空题单独处理
										&& "cloze".equals(stepInfo.getType())) {// 如果是填空题单独处理
									Prompt prompt = stepInfo.getPrompt();
									
									String xml = step.getStepinfo();// 定义参数
									String score = getElementByXml(xml, "score");// 获取分数
									
									if (score != null && !"".equals(score)) {// 如果总分不为空
										String[] answerArr = answer.split(",");// 拆分
										String[] stuAnswerArr = stuAnswer// 拆分
												.split("@@@");// 拆分
										int Len = answerArr.length;/*Integer.min(// 取较小的长度
												answerArr.length,// 取较小的长度
												stuAnswerArr.length);*/// 取较小的长度
										int rightNum = 0;// 填空题正确个数
										
										if(prompt != null && !StringUtils.isEmpty(prompt.getAudioHid())){
											score = step.getScore();//获取步骤多个空的总分
											// 听打录入题
											scoreAll = doLap(Len,answerArr,stuAnswerArr,rightNum,score,step,stepAnswer,scoreAll);
										}else{
											// 填空题
											scoreAll = doCloze(Len,answerArr,stuAnswerArr, rightNum,score,step,
													stepAnswer, scoreAll);
										}
									}
								}
								
								// 非填空题的情况，直接对比答案
								else {
									// 学生答案在不为空的情况下跟标准答案对比，如果相同则执行if里面的内容
									if(stuAnswer.contains("@@@")){// 判断参数
										stuAnswer = stuAnswer.replace("@@@", ",");//分隔答案
									}
									
									if (stuAnswer != null// 判断参数
											&& !"".equals(stuAnswer)// 判断参数
											&& stuAnswer.equals(answer)) {// 判断参数
										String stepScore = step.getScore(); // 步骤分数
										stepAnswer//设置分数
												.setScore(stepScore == null ? 0f//设置分数
														: Float.valueOf(stepScore));//设置分数
										exerAnswerMapper
												.updExerAnswer(stepAnswer);//更新答案
										scoreAll += stepAnswer.getScore();//计算总分
									}else{
										stepAnswer.setScore(0f);//设置分数
										exerAnswerMapper
										.updExerAnswer(stepAnswer);//更新答案
									}
								}
							}
						}
					}
				}
			}
		}
		return scoreAll; //  返回结果
	}

	/**
	 * 处理听打录入题(Listen -And- Play)
	 * @param step
	 * @param answer
	 * @param stuAnswer
	 * @param stepAnswer
	 * @param scoreAll
	 * @throws JDOMException
	 * @throws IOException
	 */
	private float doLap(int Len,String[] answerArr,String[] stuAnswerArr, 
			int rightNum,String score,ExerStep step,ExerAnswer stepAnswer,float scoreAll) throws JDOMException, IOException{
		int total = 0;
		for (int i = 0; i < Len; i++) {// 循环
			int stuAnLen = stuAnswerArr.length;
			if ( stuAnLen > i && stuAnswerArr[i] != null// 判断参数
					&& answerArr[i] != null) {// 判断参数
				total += answerArr[i].length();
				// 对比每个空中正确的字数
				String answerStr = answerArr[i] == null ? "" : answerArr[i].trim();
				String stuAnswerStr = stuAnswerArr[i] == null ? "" : stuAnswerArr[i].trim();
				for(int s = 0 ; s< answerStr.length(); s++){
					String aBuff = answerStr.substring(s,(s+1));
					String sBuff = stuAnswerStr.length() > s ? stuAnswerStr.substring(s, (s+1)):"";
					if( aBuff.equalsIgnoreCase(sBuff) ){// 忽略大小写进行对比
						rightNum++;// 加一
					}
				}
			}
		}
		/*score = step.getScore();
		Float sum = 0f;*/
		
		/*if(total > 0){
			sum = (Float.valueOf(score)/total)*rightNum;
			if(sum != null){
				Integer ss = Math.round(sum) ;
				Integer dd = ss == null ? 0: ss;
				sum = Float.valueOf(dd);
			}else{
				sum = 0f;
			}
		}*/
		
		stepAnswer.setScore(Float.valueOf(rightNum));// 设置分数
		exerAnswerMapper.updExerAnswer(stepAnswer);//更新答案
		return rightNum;
	}
	
	
	/**
	 * 处理填空题
	 * @param step
	 * @param answer
	 * @param stuAnswer
	 * @param stepAnswer
	 * @param scoreAll
	 * @throws JDOMException
	 * @throws IOException
	 */
	private float doCloze(int Len,String[] answerArr,String[] stuAnswerArr, 
		int rightNum,String score,ExerStep step,
		ExerAnswer stepAnswer,float scoreAll) throws JDOMException, IOException{
		int stuAnsLen = 0;
		if(stuAnswerArr != null){
			stuAnsLen = stuAnswerArr.length;
		}
		List<Integer> rightArr = new ArrayList<Integer>();// 记录填空题的第几个空正确，结果放到list中
		for (int i = 0; i < Len; i++) {// 循环
			if (stuAnsLen > i && answerArr[i] != null && stuAnswerArr[i] != null// 判断参数
					&& !"".equals(stuAnswerArr[i])// 判断参数
					&& answerArr[i].equals(stuAnswerArr[i])) {// 判断参数
				rightNum++;// 加一
				rightArr.add(i);// 添加集合
			}
		}
			
			// 如果分数中包含逗号，说明分数为逗号拼接的分数字符串
			// 则根据rightArr中存储的正确答案的索引去分数数组（scoreArr）中找到对应索引的位置，并相加求和
			/*if (score.indexOf(",") >= 0) {// 判断参数
				String[] scoreArr = score// 拆分
						.split(",");// 拆分
				float sum = 0;// 定义常量
				for (int i = 0; i < rightArr.size(); i++) {// 循环
					if (rightArr.get(i) < scoreArr.length) {// 判断参数
						sum += Float.valueOf(scoreArr[rightArr.get(i)]);// 分数相加
					}
				}
				stepAnswer.setScore(sum);// 设置分数
			} else {
				// 分数中不包含小数则，以特定规则把分数分配给每个空
				if (step.getScore() != null// 判断参数
						&& answerArr != null) {// 判断参数
					float average = 0; // 虚拟平均分
					int answerLen = answerArr.length;// 设置常量
					float sco = step.getScore() != null ? Float// 设置常量
							.valueOf(step// 设置常量
									.getScore())// 设置常量
							: 0f;// 设置常量
					// 如果平均分配刚刚好（分数除以空数余数为0）
					if (sco % answerLen == 0f) {// 判断参数
						average = sco / answerLen;// 设置常量
						stepAnswer.setScore(average// 设置常量
								* rightNum);// 设置常量
					} else {
						// 给每个空平均分配分数不满足时
						// 如果填空题中，正确的空格数小于正确答案的长度
						if (rightNum < answerArr.length) {// 判断参数
							// 初始平均分为10，如果乘以正确答案的长度<=总分，则返回虚拟平均分为10，
							// 否则初始平均分减一继续对比
							for (int i = 10; i > 0; i--) {// 循环
								if ((answerLen * i) <= sco) {// 判断参数
									average = i;// 设置常量
									break;// 跳出循环
								}
							}
							stepAnswer.setScore(average * rightNum);//设置分数
						} else {
							stepAnswer.setScore(sco);//设置分数
						}
					}
				}
			}*/
		scoreAll = Float.valueOf(score)*Float.valueOf(rightNum);
		stepAnswer.setScore(scoreAll);
		exerAnswerMapper.updExerAnswer(stepAnswer);//更新答案
		return scoreAll;
	}
	
	
	
	/**
	 * 从xml里获取指定节点的数据 
	 * @param xml
	 * @param element
	 * @return
	 * @throws JDOMException
	 * @throws IOException
	 */
	private String getElementByXml(String xml, String element)
			throws JDOMException, IOException {
		/** *创建一个新的字符串*** */
		StringReader xmlReader = new StringReader(xml);
		/** **创建新的输入源SAX 解析器将使用 InputSource 对象来确定如何读取 XML 输入 */
		InputSource xmlSource = new InputSource(xmlReader);
		/** *创建一个SAXBuilder* */
		SAXBuilder builder = new SAXBuilder();
		/** *通过输入源SAX构造一个Document** */
		Document doc = builder.build(xmlSource);
		Element root = doc.getRootElement();// 获取根节点
		Element scorEle = root.getChild(element);// 获取指定节点
		String result = "";//设置常量
		if (scorEle != null) {//判断
			result = scorEle.getText();//设置常量
		}
		return result;// 返回结果
	}

	/**
	 * 教师阅卷得总分 
	 * @param param
	 * @return
	 */
	public RExerStud updTeacherMarkTotal(Map<String, Object> param) {
		RExerStud reStu = null;//设置常量
		if (param != null) {//判断
			Integer eid = (Integer) param.get("eid");//设置常量
			String stuNum = param.get("stuNum") != null ? (String) param//设置常量
					.get("stuNum") : "";//设置常量
			ExerTopic exerTopic = new ExerTopic();// 实例化对象
			exerTopic.setEid(eid);//设置类型
			exerTopic.setStudid(stuNum);//设置学号
			List<ExerTopic> topicLis = exerTopicMapper.schExerTopicTeacher(exerTopic);//查询练习题目
			float scoreAll = 0;//设置常量
			if (topicLis != null && topicLis.size() > 0) {//判断
				for (int i = 0; i < topicLis.size(); i++) {// 循环
					ExerTopic topic = topicLis.get(i);// 获取元素
					// 获取topic所对应的step的得分相加
					float score = getStepScore(topic, stuNum);
					// 把每个topic对应的得分相加
					scoreAll += score;
				}
			}
			// 更新关系表中的主观题分数（老师阅卷的分数）和总分（主观题分数+客观题分数）
			RExerStud rExerStud = new RExerStud();
			rExerStud.setEid(eid);//设置常量
			rExerStud.setStudid(stuNum);//设置常量
			List<RExerStud> reList = rExerStudMapper.schRExerStud(rExerStud);// 查询数据
			if (reList != null && reList.size() > 0 && reList.get(0) != null) {//判断
				reStu = reList.get(0);// 获取元素
				reStu.setStatus(2); // 阅卷状态
				reStu.setSubjective(scoreAll-reStu.getObjective()); // 主观题得分
				reStu.setMark(scoreAll);//设置阅卷得分
				rExerStudMapper.updRExerStud(reStu);//更新学生得分
			}
		}
		return reStu;// 返回结果
	}

	/**
	 * 教师阅卷，获取topic 所对应的step的得分总和 
	 * @param topic
	 * @param stuNum
	 * @return
	 */
	private float getStepScore(ExerTopic topic, String stuNum) {
		float score = 0;//设置常量
		if (topic != null) {// 判断
			Map<String, Object> map = new HashMap<String, Object>();// 定义对象
			// 根据topic获取所对应的老师阅卷部分的step
			map.put("eid", topic.getEid());
			// map.put("markType", "NOT EXISTS");// 教师阅卷(需求修改，要月所有卷子)
			map.put("etid", topic.getId());//设置参数
			List<ExerStep> stepList = exerStepMapper.markList(map);// 查询结果
			if (stepList != null && stepList.size() > 0) {// 判断
				for (int i = 0; i < stepList.size(); i++) {// 循环
					ExerStep step = stepList.get(i);// 获取对象
					if (step != null && step.getId() != null) {// 判断
						// 根据step获取所对应的老师得分
						ExerAnswer exerAnswer = new ExerAnswer();
						exerAnswer.setEid(topic.getEid());//设置练习id
						exerAnswer.setEtid(topic.getId());//设置题目id
						exerAnswer.setEsid(step.getId());//设置步骤id
						exerAnswer.setStudNumber(stuNum);//设置学号
						List<ExerAnswer> answerList = exerAnswerMapper//获取练习答案
								.schExerAnswer(exerAnswer);//获取练习答案
						if (answerList != null && answerList.size() == 1// 判断
								&& answerList.get(0) != null// 判断
								&& answerList.get(0).getScore() != null) {// 判断
							score += answerList.get(0).getScore();//获取总分
						}
					}
				}
			}
		}
		return score;// 返回结果
	}

	/***************** ReportCard方法组 *****************/
	@Autowired
	private ReportCardMapper reportCardMapper; // 实例化Mapper调用

	/**
	 * ReportCard的条件查询方法
	 * @param reportCard
	 * 承载查询条件的Bean
	 * @return 返回ReportCard的集合
	 */
	@Override
	public List<ReportCard> schReportCard(ReportCard reportCard, Integer id) {
		PageHelper.startPage(reportCard.getPage(), reportCard.getPagesize());// 设置分页显示
		reportCard.setEid(id);// 设置eid
		List<ReportCard> list = reportCardMapper.schReportCard(reportCard);// 查询数据
		Page<ReportCard> page = (Page<ReportCard>) list;// 为了设置总记录数先类型强转
		reportCard.setDatasize(page.getTotal());// 设置长度
		return list;// 返回结果
	}

	/**
	 * ReportCard的主键查询方法
	 * @param id
	 * 主键值
	 * @return 返回ReportCard实体
	 */
	@Override
	public ReportCard schReportCardById(Integer id) {
		return reportCardMapper.schReportCardById(id);// 返回结果
	}

	/**
	 * 导出选中
	 * @param ids
	 */
	@Override
	public List<ReportCard> schReportExcel(Integer[] ids) {
		return reportCardMapper.schReportExcel(ids);// 返回结果
	}
	/**
	 * 获取练习下的题和步骤
	 * @param eid
	 */
	@Override
	public List<Map<Integer, List<Integer>>> getTopicIdsAndStepIds(Integer eid) {
		
		List<Map<Integer, List<Integer>>> result = new LinkedList<Map<Integer,List<Integer>>>();
		List<ExerTopic> exerTopics = exerTopicMapper.schExerTopicByEid(eid);//练习题目列表
		for (ExerTopic exerTopic : exerTopics) {// 循环
			Map<Integer, List<Integer>> map = new LinkedHashMap<Integer, List<Integer>>();// 实例化返回结果集
			Integer etid = exerTopic.getId();// 设置常量
			ExerStep exerStep = new ExerStep();// 对象
			exerStep.setEid(eid);//设置属性
			exerStep.setEtid(etid);//设置属性
			// exerStep.setOrderCol("order_no");//设置属性
			exerStep.setCondition("ORDER BY CAST(title as SIGNED)");
			List<ExerStep> schExerStep = exerStepMapper//查询练习非开始结束节点
					.schExerStepNotBE(exerStep);//查询练习非开始结束节点
			List<Integer> stepIds = new LinkedList<Integer>();// 定义对象
			for (ExerStep exerStepres : schExerStep) {// 循环
				stepIds.add(exerStepres.getId());//添加步骤
			}
			map.put(etid, stepIds);//存储题目对应的步骤id
			result.add(map);
		}
		return result;// 返回结果
	}
	/**
	 * 获取教师题目和步骤
	 * @param eid
	 */
	@Override
	public Map<Integer, List<Integer>> getTopicIdsAndStepIdsTeacher(Integer eid) {
		Map<Integer, List<Integer>> map = new LinkedHashMap<Integer, List<Integer>>();// 实例化返回结果集
		List<ExerTopic> exerTopics = exerTopicMapper// 查询数据
				.schExerTopicTeacherByEidNotTrian(eid);// 查询数据
		for (ExerTopic exerTopic : exerTopics) {// 循环
			Integer etid = exerTopic.getId();// 获取id
			ExerStep exerStep = new ExerStep();// 定义对象
			exerStep.setEid(eid);//设置属性
			exerStep.setEtid(etid);//设置属性
			exerStep.setOrderCol("order_no");//设置属性
			List<ExerStep> schExerStep = exerStepMapper//查询练习非开始结束节点
					.schExerStepNotBETeacher(exerStep);//查询练习非开始结束节点
			if (schExerStep == null || schExerStep.size() == 0) {// 判断
				continue;// 继续
			}
			List<Integer> stepIds = new LinkedList<Integer>();// 定义对象
			for (ExerStep exerStepres : schExerStep) {// 循环
				stepIds.add(exerStepres.getId());//添加步骤id
			}
			map.put(etid, stepIds);//存储题目对应的步骤id
		}
		return map;// 返回结果
	}
	/**
	 * 通过课本获取题目和步骤
	 * @param bookId
	 */
	@Override
	public Map<Integer, List<Integer>> getTopicIdsAndStepIdsByBookId(
			Integer bookId) {
		Map<Integer, List<Integer>> map = new LinkedHashMap<Integer, List<Integer>>();// 实例化返回结果集
		List<ExerTopic> exerTopics = exerTopicMapper// 查询数据
				.schExerTopicByBookIdNotTrian(bookId);// 查询数据
		for (ExerTopic exerTopic : exerTopics) {// 循环
			Integer etid = exerTopic.getId();//设置参数
			Integer eid = exerTopic.getEid();//设置参数
			ExerStep exerStep = new ExerStep();// 定义对象
			exerStep.setEid(eid);//设置属性
			exerStep.setEtid(etid);//设置属性
			exerStep.setOrderCol("order_no");//设置属性
			List<ExerStep> schExerStep = exerStepMapper
					.schExerStepNotBE(exerStep);//查询练习非开始结束节点
			List<Integer> stepIds = new LinkedList<Integer>();
			for (ExerStep exerStepres : schExerStep) {
				stepIds.add(exerStepres.getId());//添加步骤id
			}
			map.put(etid, stepIds);//存储题目对应的步骤id
		}
		return map;// 返回结果
	}
	
	@Autowired
	private StudyStatusMapper studyStatusMapper;
	
	
	/**
	 * 根据题目步骤更新答案
	 * @param exerAnswer
	 */
	@Override
	public void updExerAnswerByEtsId(ExerAnswer exerAnswer) {
		if(exerAnswer != null){
			Integer etid = exerAnswer.getEtid();
			ExerTopic exT = exerTopicMapper.schExerTopicById(etid);
			if(exT != null && exT.getBookId() != null && exT.getBookId() != ""
					&& !TopicTypeConstant.CAMPING.equals(exT.getType())){
				
				String stuNum = exerAnswer.getStudNumber();
//				StudyStatus stusta = new StudyStatus();
//				stusta.setNumber(stuNum);
//				stusta.setBookId(Integer.valueOf(exT.getBookId()));
//				List<StudyStatus> stuss = studyStatusMapper.schStudyStatus(stusta);
//				if(stuss == null || stuss.size() == 0){
//					stusta.setStatus("1");
//					studyStatusMapper.addStudyStatus(stusta);
//				}
			}
			
			List<ExerAnswer> exerAnswers = this.schExerAnswerByEtsId(exerAnswer);// 实例化返回结果集
			if (exerAnswers != null && exerAnswers.size() != 0) {// 判断
				if (exerAnswers.size() == 1) {// 判断
					exerAnswerMapper.updExerAnswerByEtsId(exerAnswer);//更新答案
				} else {
					throw new RuntimeException("数据库答案表存在脏数据");// 抛出异常
				}
			} else {
				this.addExerAnswer(exerAnswer);//添加答案
			}
		}
		
		
	}
	/**
	 * 根据题目步骤搜索答案
	 * @param exerAnswer
	 * @return
	 */
	private List<ExerAnswer> schExerAnswerByEtsId(ExerAnswer exerAnswer) {
		return exerAnswerMapper.schExerAnswerByEtsId(exerAnswer);// 返回结果
	}
	/**
	 * 查询练习状态
	 * @param exer
	 */
	@Override
	public List<Exer> schExerAndStatus(Exer exer) {
		//exer.setCondition(" and t.start_time is not null and t.end_time is not null");// 设置sql 
		return exerMapper.schExerAndStatus(exer);// 返回结果
	}
	/**
	 * 更新答案
	 * @param answers
	 */
	@Override
	public void updExerAnswerByEtId(List<ExerAnswer> answers) {
		for (ExerAnswer exerAnswer : answers) { // 循环
			exerAnswerMapper.updExerAnswerByEtsId(exerAnswer);// 更新数据
		}
	}
	
	/**
	 * 更新分数
	 * @param answers
	 */
	@Override
	public void updExerAnswerByEtIdd(List<ExerAnswer> answers) {
		for (ExerAnswer exerAnswer : answers) { // 循环
			exerAnswerMapper.updExerAnswerByEtsIdd(exerAnswer);// 更新数据
		}
	}
	
	/**
	 * 查询阅卷题目
	 * @param exerTopic
	 */
	@Override
	public List<ExerTopic> schExerTopicTeacher(ExerTopic exerTopic) {
		List<ExerTopic> list = exerTopicMapper.schExerTopicTeacher(exerTopic);// 查询数据
		return list;// 返回结果
	}
	/**
	 * 查询不是实训营地的练习题目
	 * @param exerTopic
	 */
	@Override
	public List<ExerTopic> schExerTopicNotCampList(ExerTopic exerTopic) {
		List<ExerTopic> list = exerTopicMapper.schExerTopicNotCamp(exerTopic);// 查询数据
		return list;
	}
	
	@Autowired
	private ExamCategoryMapper examCategoryMapper;


	/***************** ExamCategory方法组 *****************/
	/**
	 * ExamCategory的条件查询方法
	 * @param examCategory 承载查询条件的Bean
	 * @return 返回ExamCategory的集合
	 */
	@Override
	public List<ExamCategory> schExamCategory(ExamCategory examCategory) {
		PageHelper.startPage(examCategory.getPage(), examCategory.getPagesize());//设置分页显示
		List<ExamCategory> list=examCategoryMapper.schExamCategory(examCategory);
		Page<ExamCategory> page = (Page<ExamCategory>)list;//为了设置总记录数先类型强转
		examCategory.setDatasize(page.getTotal());
		return list;
	}
	
	/**
	 * ExamCategory的条件查询方法(无分页)
	 * @param examCategory 承载查询条件的Bean
	 * @return 返回ExamCategory的集合
	 */
	@Override
	public List<ExamCategory> schExamCategoryList(ExamCategory examCategory){
		List<ExamCategory> list=examCategoryMapper.schExamCategory(examCategory);
		return list;
	}
	
	/**
	 * ExamCategory的主键查询方法
	 * @param id 主键值
	 * @return 返回ExamCategory实体
	 */
	@Override
	public ExamCategory schExamCategoryById(Integer id) {
		return examCategoryMapper.schExamCategoryById(id);
	}
	
	/**
	 * ExamCategory的添加方法
	 * @param examCategory 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	@Override
	public Integer addExamCategory(ExamCategory examCategory) {
		return examCategoryMapper.addExamCategory(examCategory);
	}

	/**
	 * ExamCategory的修改方法
	 * @param examCategory 承载数据的Bean
	 */
	@Override
	public void updExamCategory(ExamCategory examCategory) {
		examCategoryMapper.updExamCategory(examCategory);
	}

	/**
	 * ExamCategory的单记录删除方法
	 * @param id 要删除的主键值
	 */
	@Override
	public void delExamCategory(Integer id) {
		examCategoryMapper.delExamCategory(id);
	}

	/**
	 * ExamCategory的批量删除方法
	 * @param ids 主键值的数组
	 */
	@Override
	public void delExamCategory(Integer[] ids) {
		examCategoryMapper.delExamCategorys(ids);
	}

	
	@Autowired
	private DataDisplayMapper dataDisplayMapper;
	
	/**
	 * 查询个人完成情况 
	 * @param eid 试卷id
	 * @param classid 班级号
	 * @param wcqk 完成情况
	 * @return 返回结果的集合
	 */
	@Override
	public List<Map<String, Object>> schPersonalCompletion(Integer eid,
			Integer classid, String wcqk) {
		return dataDisplayMapper.schPersonalCompletion(eid, classid, wcqk);
	}
	
	/**
	 * 查询任务统计 
	 * @param eid 试卷id
	 * @param classid 班级号
	 * @return 返回结果的集合
	 */
	@Override
	public List<Map<String, Object>> schTaskStatistics(Integer eid,
			Integer classid) {
		return dataDisplayMapper.schTaskStatistics(eid, classid);
	}

	/**
	 * 查询学习过程分析 
	 * @param classid 班级号
	 * @param studid 学号
	 * @return 返回结果的集合
	 */
	@Override
	public List<Map<String, Object>> schLearningProcess(Integer classid,
			Integer studid) {
		return dataDisplayMapper.schLearningProcess(classid, studid);
	}

	/**
	 * 查询任务统计查任务 
	 * @param classid 班级号
	 * @param studid 学号
	 * @return 返回结果的集合
	 */
	@Override
	public List<Map<String, Object>> schOrderStatistics(Integer classid,
			Integer studid) {
		return dataDisplayMapper.schOrderStatistics(classid, studid);
	}

	/**
	 * 查询班级完成情况 
	 * @param eid 试卷id
	 * @return 返回结果的集合
	 */
	@Override
	public List<Map<String, Object>> schClassCompletion(Integer eid) {
		return dataDisplayMapper.schClassCompletion(eid);
	}
	
	
	/**
	 * 预览试卷service层处理
	 * @param exer
	 * @return Map集合
	 * @throws IOException 
	 * @throws JDOMException 
	 * @throws DocumentException 
	 */
	public Map<String,Object> previewExer(Exer exer,String projectName) throws 
			DocumentException, JDOMException, IOException{
		
		Map<String,Object> result = new HashMap<String,Object>();
		Integer eid = exer.getId();
		Exer exerReult = exerMapper.schExerById(eid);
		
		if(exerReult != null){
			// 设置试卷（返回页面用）
			result.put("exer", exerReult.getJsonInDict());
			if(exerReult.getEndTime() != null && exerReult.getStartTime() != null){
				long diff = (exerReult.getEndTime().getTime())// 计算时间差
						- (exerReult.getStartTime().getTime());// 计算时间差
				long diffs = diff / (1000 * 60);// 转换为分钟
				result.put("diff", diffs);
			}
			// 查询试卷下的所有题目
			List<ExerTopic> topics = exerTopicMapper.schExerTopicByEid(eid);
			if(topics != null && topics.size() > 0){
				// 设置题目（返回页面用）
				result.put("topics", topics);
				Map<String,Object> stepMap = new HashMap<String,Object>();
				
				for(int i = 0; i < topics.size(); i++){
					ExerTopic topic = topics.get(i);
					if(topic != null && topic.getId() != null){
						List<StepInfo> stepInfos = new ArrayList<StepInfo>();
						stepInfos = previewTopic(topic,projectName);
						stepMap.put(topic.getId().toString(), stepInfos);
					}
				}
				result.put("steps", stepMap);
			}
		}
		return result;
	}
	
	/**
	 * 预览题目service层处理
	 * @param exer
	 * @return Map集合
	 */
	public List<StepInfo> previewTopic(ExerTopic topic,String projectName)throws 
			DocumentException, JDOMException, IOException{
		List<StepInfo> stepInfos = new ArrayList<StepInfo>();
		if(topic == null || topic.getId() == null){
			return stepInfos;
		}
		// 题目类型
		String topicType = topic.getType();
		// 查询步骤
		ExerStep stepParam = new ExerStep();
		stepParam.setEid(topic.getEid());// 设置试卷ID
		stepParam.setEtid(topic.getId());// 设置题目ID
		stepParam.setCondition("ORDER BY cast(title as SIGNED)");
		List<ExerStep> steps = exerStepMapper.schExerStep(stepParam);
		// 步骤不为null的场合
		if(steps != null){
			for(int s = 0; s < steps.size(); s++){
				ExerStep step = steps.get(s);
				if(step != null ){
					String stepType = step.getType();
					String xml = step.getStepinfo();
					if(stepType != null && !TopicTypeConstant.CAMPING.equals(topicType)
						&& !TopicTypeConstant.CAMPING_STR.equals(topicType)
						&& stepType != null && !"".equals(stepType)&& !TopicTypeConstant.STEP_CAMPING.equals(stepType)
						&& !TopicTypeConstant.STEP_CAMPING_STR.equals(stepType)  ){
					
						String path = projectName + "/paper/question/appendUrl.action?filename="// 调用工具方法
								+topic.getBankId()+File.separator+topic.getTopicId()+File.separator;
						
						StepInfo stepInfo = FreemakerTools.xmlToStep(xml, path);
						if(stepInfo != null){
							if("cloze".equals(stepInfo.getType()) && step != null && xml != null){// 判断
								xml = step.getStepinfo();// 获取属性
								StringReader xmlReader = new StringReader(xml);// 定义对象
								InputSource xmlSource = new InputSource(xmlReader);// 定义对象
								SAXBuilder builder = new SAXBuilder();// 定义对象
								//** *通过输入源SAX构造一个Document** *//*
								Document doc = builder.build(xmlSource);
								Element root = doc.getRootElement();// 获取节点
								Element scorEle = root.getChild("score");//获取属性
								stepInfo.setScore(scorEle != null? scorEle.getText():"0");// 设置总分
							}else{
								stepInfo.setScore(step.getScore());// 设置总分
							}
							stepInfo.setScore(step.getScore());
							if(stepType.length() <= 2){//TODO 数据清洗后去掉判断，直接执行判断里面的代码
								stepInfo.setType(stepType);
							}
							stepInfo.setBankId(topic.getBankId());
							stepInfo.setTopicId(topic.getTopicId()); 
							stepInfos.add(stepInfo);
						}else{
							stepInfo = new StepInfo();
						}
						stepInfo.setEsid(step.getId());
					}
				}
			}
		}
		return stepInfos;
	}
	
	@Autowired
	private UserMapper userMapper;
	
	/**
	 * 发卷和撤卷操作
	 */
	public void updAddAndDelRExer(String addStr,String delStr,String eid,String teachers){
		
		String[] addArr = addStr.split(",");
		String[] delArr = delStr.split(",");
		
		Exer exer = exerMapper.schExerById(Integer.valueOf(eid));
		if(exer != null){
			// 更新阅卷老师字段
			exer.setTeachers(teachers);
			exerMapper.updExer(exer);
			// 操作关系表（add）
			Date now = new Date();
			for(String add : addArr){
				if (StringUtils.isEmpty(add)) {
					continue;
				}
				RExerStud rExerStud  = new RExerStud();
				rExerStud.setEid(exer.getId());
				rExerStud.setStudid(add);
				List<RExerStud> rstuds = rExerStudMapper.schRExerStud(rExerStud);
				if(rstuds == null || rstuds.size() == 0){
					User user = userMapper.schUserByNumber(add);
					if(user != null){
						rExerStud.setClassid(user.getCnumber());
					}
					rExerStud.setCreateTime(now);
					rExerStud.setName(exer.getName());
					rExerStud.setStatus(0);
					rExerStud.setStatusReady(0);
					rExerStudMapper.addRExerStud(rExerStud);
				}
			}
		}
		// 操作关系表和答案表（del）
		String delsql = "";
		for(String del : delArr){
			if(del != null && !"".equals(del)){
				if(!"".equals(delsql)){
					delsql += ", '"+del+"'";
				}else{
					delsql = "'"+del+"'";
				}
			}
		}
		
		if(!"".equals(delsql)){
			// 删除关系表数据
			Map<String,Object> param = new HashMap<String,Object>();
			param.put("eid", eid);
			param.put("condition", "AND STUDID in ("+delsql+")");
			rExerStudMapper.delReStudents(param);
			
			// 删除答案表数据
			ExerAnswer exerAnswer  = new ExerAnswer();
			exerAnswer.setEid(Integer.valueOf(eid));
			exerAnswer.setCondition("AND STUD_NUMBER in ("+delsql+")");
			exerAnswerMapper.delExerAnswerByParam(exerAnswer);
		}
	}
	
	/**
	 * 获取阅卷题目列表
	 * @param eid
	 * @param studNum
	 */
	public Map<String,Object> markTopicsList(Integer eid,String studNum){
		Map<String,Object> result = new HashMap<String,Object>();
		Map<String,Object> teacherScoreMap = new HashMap<String,Object>();// 老师得分集合
		List<Map<String,Object>> topicsMap = new ArrayList<Map<String,Object>>();// 题目集合
		Map<String,Object> studAnswerMap = new HashMap<String,Object>();// 老师得分集合
		if(eid != null && studNum != null){
			List<ExerTopic> topics = exerTopicMapper.schExerTopicByEid(eid);
			
			if(topics != null ){
				for( int i = 0; i < topics.size(); i++ ){
					Map<String,Object> obj = new HashMap<String,Object>();
					
					ExerTopic topic = topics.get(i);
					Integer topicId = topic.getId();
					obj.put("topicId", topicId);
					obj.put("topicTitle", topic.getTitle());
					
					// 查询步骤
					List<ExerStep> steps = markGetSteps(topic);
					
					List<ExerAnswer> answers = null;
					String isFinish = IsFinishConstant.YES_FINISH_CODE;// 初始值：已完成
					// 获取答案
					answers = markGetAnswers(steps, eid,topicId,studNum);
					if( answers != null ){
						for(ExerAnswer answer : answers){
							if(answer != null ){
								Float score = answer.getScore();
								if(answer.getScore() == null){
									isFinish = IsFinishConstant.NO_FINISH_CODE;// 有得分为null时，标识未完成
									score = 0f;
								}
								if(answer.getEsid() != null){
									teacherScoreMap.put(answer.getEsid().toString(), score);
									studAnswerMap.put(answer.getEsid().toString(), answer);
								}
							}
						}
						if(answers.size() < steps.size()){
							isFinish = IsFinishConstant.NO_FINISH_CODE;// 得分集合长度小于步骤集合长度时，标识未完成
						}
					}else{
						isFinish = IsFinishConstant.NO_FINISH_CODE;// 答案集合为空时，标识未完成
					}
					obj.put("isFinish", isFinish);
					topicsMap.add(obj);
				}
			}
			result.put("teacherScoreMap", teacherScoreMap);
			result.put("studAnswerMap", studAnswerMap);
			result.put("topics", topicsMap);
		}
		return result;
	}
	
	/**
	 * 更新分数
	 * @param answers
	 */
	@Override
	public void updScoreByTeacher(List<ExerAnswer> answers) {
		if(answers != null){
			for (ExerAnswer exerAnswer : answers) { // 循环
				if(exerAnswer != null && exerAnswer.getEid() != null
						&& exerAnswer.getEtid() != null && exerAnswer.getEsid() != null
						&& exerAnswer.getStudNumber() != null){
					ExerAnswer param = new ExerAnswer();
					param.setEid(exerAnswer.getEid());// 试卷ID
					param.setEtid(exerAnswer.getEtid());// 题目ID
					param.setEsid(exerAnswer.getEsid());// 步骤ID
					param.setStudNumber(exerAnswer.getStudNumber());// 学号
					List<ExerAnswer> ans = exerAnswerMapper.schExerAnswerByEtsId(exerAnswer);
					if(ans != null && ans.size() > 0 && ans.get(0) != null){
						ExerAnswer answer = ans.get(0);
						answer.setScore(exerAnswer.getScore());
						exerAnswerMapper.updExerAnswer(answer);
					}else{
						exerAnswerMapper.addExerAnswer(exerAnswer);
					}
				}
			}
			// 更新进度
			if(answers.size() > 0 && answers.get(0) != null){
				Integer eid = answers.get(0).getEid();
				String stuNum = answers.get(0).getStudNumber();
				updMarkProgress(eid,stuNum);
			}
		}
	}
	
	// 更新阅卷进度
	private void updMarkProgress(Integer eid ,String studNum){
		Float stepTotal = 0f;// 步骤数量总和
		Float hasMark = 0f;// 已阅卷数量总和
		Float totalScaore = 0f;
		List<ExerTopic> topics = exerTopicMapper.schExerTopicByEid(eid);
		for( ExerTopic topic : topics ){
			if(topic != null && topic.getId() != null){
				
				List<ExerStep> steps = markGetSteps(topic);
				stepTotal += steps.size();
				List<ExerAnswer> answers = markGetAnswers(steps,eid,topic.getId(),studNum);
				for(ExerAnswer an : answers){
					if(an != null && an.getScore() != null){
						hasMark++;
						totalScaore += an.getScore();
					}
				}
			}
		}
		Float pro = (hasMark/stepTotal)*100;
		RExerStud rExerStud = new RExerStud();
		rExerStud.setEid(eid);
		rExerStud.setStudid(studNum);
		List<RExerStud> rs = rExerStudMapper.schRExerStud(rExerStud);
		if(rs != null && rs.size() > 0 && rs.get(0) != null){
			RExerStud upd = rs.get(0);
			upd.setMarkProgress(pro);
			upd.setMark(totalScaore);
			rExerStudMapper.updRExerStud(upd);
		}
	}
	
	// 阅卷获取步骤
	private List<ExerStep> markGetSteps(ExerTopic topic){
		List<ExerStep> steps = null;
		if(topic != null && topic.getId() != null){
			ExerStep stepParam = new ExerStep();
			stepParam.setEtid(topic.getId());
			stepParam.setEid(topic.getEid());
			stepParam.setCondition("AND TYPE NOT IN ('voice','"+TopicTypeConstant.VOICE+"', "
					+ "'"+TopicTypeConstant.STEP_CAMPING+"', '"+TopicTypeConstant.STEP_CAMPING_STR+"')");
			steps = exerStepMapper.schExerStep(stepParam);
		}
		if(steps == null){
			steps = new ArrayList<ExerStep>();
		}
		return steps;
	}
	
	/**
	 * 阅卷（获取答案）
	 * @param steps
	 * @param eid
	 * @param topicId
	 * @param studNum
	 * @return
	 */
	private List<ExerAnswer> markGetAnswers(List<ExerStep> steps,Integer eid,
			Integer topicId,String studNum){
		List<ExerAnswer> result = null;
		String sqlParam = "";
		if(steps != null ){
			for(ExerStep step : steps){
				if(step != null && step.getId() != null
						&& !"".equals(step.getId())){
					if( !"".equals(sqlParam) ){
						sqlParam += ", '"+step.getId()+"'";
					}else{
						sqlParam = "'"+step.getId()+"'";
					}
				}
			}
		}
		if( !"".equals(sqlParam) ){
			// 查询答案
			ExerAnswer answerParam = new ExerAnswer();
			answerParam.setEid(eid);
			answerParam.setEtid(topicId);
			answerParam.setStudNumber(studNum);
			answerParam.setCondition("AND ESID IN("+sqlParam+")");
			result = exerAnswerMapper.schExerAnswer(answerParam);
		}
		if(result == null){
			result = new ArrayList<ExerAnswer>();
		}
		return result ;
	}
	
	/**
	 * 保存实训步骤和答案
	 */
	public List<ExerStep> saveSxStepAndAnswer(List<ExerStep> exerSteps,String studNum){
		if(exerSteps != null && exerSteps.size() > 0 && studNum != null){
			for( int i = 0 ; i < exerSteps.size(); i ++ ){
				ExerStep step = exerSteps.get(i);
				if(step != null && step.getEtid() != null && step.getId() != null ){
					int esid = step.getId();
					if(esid == 0){
						ExerStep exerStep = new ExerStep();
						exerStep.setEid(step.getEid());
						exerStep.setEtid(step.getEtid());
						exerStep.setTitle(step.getTitle());
						exerStepMapper.addExerStep(exerStep);// 添加步骤
						step.setId(exerStep.getId());
					}else{
						ExerStep exerStep = exerStepMapper.schExerStepById(esid);
						exerStep.setTitle(step.getTitle());
						exerStepMapper.updExerStep(exerStep);// 更新步骤
					}
					ExerAnswer exerAnswer = new ExerAnswer();
					exerAnswer.setEid(step.getEid());
					exerAnswer.setEtid(step.getEtid());
					exerAnswer.setEsid(step.getId());
					exerAnswer.setStudNumber(studNum);
					List<ExerAnswer> ans = exerAnswerMapper.schExerAnswer(exerAnswer);
					if(ans != null && ans.size() > 0 && ans.get(0) != null){
						ExerAnswer an = ans.get(0);
						an.setAnswer(step.getStandardAnswer());
						exerAnswerMapper.updExerAnswer(an);
					}
					if(ans == null || ans.size() == 0 ){
						exerAnswer.setAnswer(step.getStandardAnswer());
						exerAnswerMapper.addExerAnswer(exerAnswer);
					}
				}
			}
		}
		return exerSteps;
	}
	
	/**
	 * 删除学生上传的学生营地答案及步骤
	 */
	public void delSxStepAndAnswer(ExerStep step,String studNum){
		if(step != null && step.getId() != null && studNum != null){
			ExerStep exerStep = exerStepMapper.schExerStepById(step.getId());
			if(exerStep != null){
				ExerAnswer exerAnswer = new ExerAnswer();
				exerAnswer.setEid(exerStep.getEid());
				exerAnswer.setEtid(exerStep.getEtid());
				exerAnswer.setEsid(exerStep.getId());
				exerAnswer.setStudNumber(studNum);
				List<ExerAnswer> ans = exerAnswerMapper.schExerAnswer(exerAnswer);
				if(ans != null && ans.size() > 0){
					for( ExerAnswer an : ans ){
						if(an != null && an.getId() != null){
							exerAnswerMapper.delExerAnswer(an.getId());
						}
					}
				}
				
				exerStepMapper.delExerStep(step.getId());;
			}
		}
	}
	/**
	 * 根据 booid 查询 习题 
	 * @param book
	 * @return
	 */
	public List<ExerTopic> getExerTopicBybookidNum(Book book){
		return exerTopicMapper.schExerTopicByBookIdNotTrian(book.getId());
	};
	/**
	 * set接收
	 */
	@Override
	public HashSet<ExerTopic> getExerTopicBybookidNumSet(Book book) {
		return exerTopicMapper.schExerTopicByBookIdNotTrianSet(book.getId());
	}
	/**
	 * answer 去重查询
	 * @return list 
	 */ 
	@Override
	public List<ExerAnswer> schExerAnswerListDistinct(ExerAnswer exerAnswer) {
		return exerAnswerMapper.schExerAnswerListDistinct(exerAnswer);
	}
	
}//end
