package com.qtrmoon.examination.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.dictionary.bsd.DictBean;
import com.qtrmoon.dictionary.bsd.DictCatalog;
import com.qtrmoon.examination.pojo.Exer;
import com.qtrmoon.examination.serdao.IExaminationService;
import com.qtrmoon.util.ApplicationContextUtil;

/**
 * 试卷的目录树工具类
 */
public class ExamShutCutUtil {

	// 获取service
	private static IExaminationService examinationService = (IExaminationService) 
			ApplicationContextUtil.getApplicationContext().getBean("examinationService");
	// 字典标识
	// private static String dictKey = "EXAM_TREE";
	private static String dictKey = "ZD_EXAM_CATEGORY";
	// 虚拟根节点ID
	private static String rootId = "_JAVAMAO";
	// 日志对象
	private static Logger logger = LoggerFactory.getLogger(ExamShutCutUtil.class);
	// 试卷的shutcut
	private static List<DictBean> examShutCut ;
	
	
	/**
	 * 构建(shutcut后台字典)
	 */
	/*public static void buildShutCutDic(){
		try{
			logger.info("构建试卷树===>开始");
			List<String> list = getAllShutCut();
			doBeanList(list);
			DictCatalog ca=new DictCatalog(dictKey);
			ca.setStyle("T#D#N");
			List<DictBean> dictList=new ArrayList<DictBean>();
			dictList.addAll(examShutCut);
			DictBuffer.addCatalogToCache(ca, dictList);
			logger.info("构建试卷树===>结束");
		}catch(Exception e){
			logger.error("构建试卷树异常");
			e.printStackTrace();
		}
		
	}*/
	
	/**
	 * 初始化数据
	 * @return
	 */
	/*private static List<String> getAllShutCut(){
		List<String> list = new ArrayList<String>();
		Exer exer = new Exer();
		List<Exer> exerList = examinationService.schExerList(exer);
		if( exerList != null ){
			for( int i = 0 ; i < exerList.size(); i++ ){
				Exer ex = exerList.get(i);
				if(ex != null && !StringUtils.isEmpty(ex.getShortcut())){
					if( !ex.getShortcut().endsWith("/") ){
						ex.setShortcut(ex.getShortcut()+"/");
						examinationService.updExer(ex);
					}
					list.add(ex.getShortcut());
				}
			}
		}
		return list;
	}*/
	
	/**
	 * 组装成list对象集合
	 * @param list
	 * @return
	 */
	/*private static List<DictBean> doBeanList(List<String> list){
		examShutCut = new ArrayList<DictBean>();
		if(list != null){
			for(int i = 0; i < list.size(); i++){
				String str = list.get(i);
				if(str != null && str.startsWith("/")){
					str = str.substring(1);
				}
				String[] stArg = str.split("/");
				if(stArg != null){
					for(int n = 0 ; n < stArg.length; n++){
						DictBean newDic = new DictBean();
						if(n > 0){
							newDic.setPid(stArg[n-1]);
						}else{
							newDic.setPid(rootId);
						}
						newDic.setId(stArg[n]);
						newDic.setLabel(stArg[n]);
						addByDiff(newDic);
					}
				}
			}
		}
		return examShutCut;
	}*/
	
	/**
	 * 添加不同的对象（试卷shutCut）
	 * @param dic
	 */
	/*private static void addByDiff(DictBean dic){
		if(examShutCut != null){
			boolean flg = false;
			for(int i = 0; i < examShutCut.size(); i++){
				DictBean t = examShutCut.get(i);
				if(t != null && dic != null && t.getPid() != null  
						&& !StringUtils.isEmpty(t.getId()) 
						&& !StringUtils.isEmpty(t.getLabel())
						&& t.getId().equals(dic.getId()) 
						&& t.getPid().equals(dic.getPid()) 
						&& t.getLabel().equals(dic.getLabel()) ){
					flg = true;
				}
			}
			if(!flg){
				examShutCut.add(dic);
			}
		}
	}*/
	
	/**
	 * 根据父节点查询字典数据
	 * @param pid
	 * @return [{id:"id",label:"label",pid:"pid"}...]
	 */
	public static List<Map<String,String>> getDictByPid(String pid){
		logger.info("根据父节点获取试卷树，父节点：{}  开始",pid);
		
		/*
		 * 注意：直接返回DictBean，ajax会报序列化异常，可能因为DictBean结构复杂，所以重新组装map集合
		 */
		List<Map<String,String>> result = new ArrayList<Map<String,String>>();
		List<DictBean> dictBeans = DictBuffer.getChildDict(dictKey, pid);
		if(dictBeans != null && dictBeans.size() > 0){
			for(DictBean bean : dictBeans ){
				Map<String,String> map = new HashMap<String,String>();
				map.put("id", bean.getId());
				map.put("label", bean.getLabel());
				map.put("pid", bean.getPid());
				result.add(map);
			}
		}
		logger.info("根据父节点获取试卷树，父节点：{}  结束",pid);
		return result;
	}
	
	/**
	 * 根据根节点查询字典数据
	 * @param
	 * @return[{id:"id",label:"label",pid:"pid"}...]
	 */
	public static List<Map<String,String>> getDictByRoot(){
		logger.info("根据root节点获取试卷树，root节点：{}  开始",rootId);
		List<Map<String,String>> dictBeans = getDictByPid(rootId);
		logger.info("根据root节点获取试卷树，root节点：{}  结束",rootId);
		return dictBeans;
	}
}
