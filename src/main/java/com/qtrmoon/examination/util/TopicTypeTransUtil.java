package com.qtrmoon.examination.util;

import java.util.HashMap;
import java.util.Map;

import com.qtrmoon.manager.constant.TopicTypeConstant;

/**
 * 题型工具类
 */
public class TopicTypeTransUtil {

	private static Map<String,Object> topicTypes = null;// topictype集合
	private static Map<String,Object> stepTypes = null;// steptype集合
	
	/**
	 * 获取步骤类型map集合
	 * @return
	 */
	public static Map<String,Object> getTopicTypeToMap(){
		if(topicTypes == null){
			topicTypes = topicTypeToMap();
		}
		return topicTypes;
	}
	
	/**
	 * 获取步骤类型map集合
	 * @return
	 */
	public static Map<String,Object> getStepTypeToMap(){
		if(stepTypes == null){
			stepTypes = stepTypeToMap();
		}
		return stepTypes;
	}
	
	/**
	 * 根据题目类型code获取label
	 * @param code
	 * @return
	 */
	public static String topicCodeTransLabel(String code){
		String result = "";
		if(code != null){
			if(topicTypes == null){
				topicTypes = topicTypeToMap();
			}
			result = topicTypes.get(code)== null? "" : topicTypes.get(code).toString();
		}
		return result;
	}
	
	/**
	 * 根据步骤类型code获取label
	 * @param code
	 * @return
	 */
	public static String stepCodeTransLabel(String code){
		String result = "";
		if(code != null){
			if(stepTypes == null){
				stepTypes = stepTypeToMap();
			}
			result = stepTypes.get(code)== null? "" : stepTypes.get(code).toString();
		}
		return result;
	}
	
	/**
	 * 获取题目类型map对象
	 * @return
	 */
	private static Map<String,Object> topicTypeToMap(){
		Map<String,Object> result = new HashMap<String,Object>();
		result.put(TopicTypeConstant.THEORY, TopicTypeConstant.THEORY_STR);// 理论题
		result.put(TopicTypeConstant.PRACTICAL, TopicTypeConstant.PRACTICAL_STR);// 实训题
		result.put(TopicTypeConstant.SKILL, TopicTypeConstant.SKILL_STR);// 技能训练题
		result.put(TopicTypeConstant.CAMPING, TopicTypeConstant.CAMPING_STR);// 实训营地题
		return result;
	}
	
	/**
	 * 设置步骤类型map对象
	 * @return
	 */
	private static Map<String,Object> stepTypeToMap(){
		Map<String,Object> result = new HashMap<String,Object>();
		result.put(TopicTypeConstant.SINGLE, TopicTypeConstant.SINGLE_STR);// 单选
		result.put(TopicTypeConstant.MULTI, TopicTypeConstant.MULTI_STR);// 多选
		result.put(TopicTypeConstant.ANSWER, TopicTypeConstant.ANSWER_STR);// 问答
		result.put(TopicTypeConstant.JUDGE, TopicTypeConstant.JUDGE_STR);// 判断
		result.put(TopicTypeConstant.CLOZE, TopicTypeConstant.CLOZE_STR);// 听打录入
		result.put(TopicTypeConstant.REPEAT, TopicTypeConstant.REPEAT_STR);// 复读
		result.put(TopicTypeConstant.STATE, TopicTypeConstant.STATE_STR);// 应答
		result.put(TopicTypeConstant.VOICE, TopicTypeConstant.VOICE_STR);// 语音
		result.put(TopicTypeConstant.CLOZED, TopicTypeConstant.CLOZED_STR);// 填空
		result.put(TopicTypeConstant.ZHTX, TopicTypeConstant.ZHTX_STR);// 综合
		result.put(TopicTypeConstant.ANALYZE, TopicTypeConstant.ANALYZE_STR);// 数据分析
		result.put(TopicTypeConstant.VERBAL, TopicTypeConstant.VERBAL_STR);// 话术编写
		result.put(TopicTypeConstant.STEP_CAMPING, TopicTypeConstant.STEP_CAMPING_STR);// 实训营地
		result.put(TopicTypeConstant.STEP_DRAW, TopicTypeConstant.STEP_DRAW_STR);// 画图连线题
		return result;
	}
	
}
