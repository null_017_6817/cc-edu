package com.qtrmoon.examination.ctrl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.dom4j.DocumentException;
import org.jdom.JDOMException;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.qtrmoon.common.BaseController;
import com.qtrmoon.examination.pojo.RExerStud;
import com.qtrmoon.examination.serdao.IExaminationService;
import com.qtrmoon.manager.constant.YesOrNoConstant;
import com.qtrmoon.manager.pojo.Person;
import com.qtrmoon.manager.pojo.Stud;
import com.qtrmoon.manager.serdao.IManagerService;
import com.qtrmoon.sysmanage.SysConstant;

/**
 * 自动阅卷测试类
 * @Class Name AutoMarkController
 */
@Controller
@RequestMapping("/examination/automark")
public class AutoMarkController extends BaseController {

	@Autowired
	private IExaminationService examinationService;				//声明模块Service实例
	@Autowired
	private IManagerService managerService;//声明模块Service实例
	
	/**
	 * 自动阅卷测试方法
	 * @param rExerStud
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws JDOMException 
	 * @throws DocumentException 
	 */
	@SuppressWarnings({ "unchecked" })
	@RequestMapping(value="/autoMark.action")
	public void autoMark(RExerStud rExerStud, HttpServletRequest request,HttpServletResponse response) throws IOException, DocumentException, JDOMException{
		noCache(response);// ajax设置无缓存
		JSONObject result = new JSONObject();// 实例化json对象
		try{
			Person person = (Person)request.getSession(true).getAttribute(SysConstant.CURRENT_USER);// 获取当前用户
			if(person != null){// 如果当前用户不为null
				Integer eid = rExerStud.getEid(); // 获取试卷id
				Map<String,Object> param = new HashMap<String,Object>();// 实例化map集合
				param.put("eid", eid);	// 练习编号
				param.put("stuNum", person.getNumber());// 学号
				examinationService.updAutoMark(param);// 调用自动阅卷方法
				removeExamState(person);// 清除考试状态
				result.put("success", true);// 赋值成功状态
			}else{
				result.put("success", false);// 赋值成功状态
				result.put("msg", "用户未登录");// 赋值提示信息
			}
		}catch(Exception e){
			e.printStackTrace();
			result.put("success", false);// 赋值成功状态
			result.put("msg", "服务器异常");// 赋值提示信息
		}
		response.getWriter().print(result.toJSONString());// 返回json数据
	}
	
	/**
	 * 清除考试状态
	 * @param person
	 */
	private void removeExamState(Person person){
		if(person != null){
			Stud stu = managerService.schStudById(person.getId());// 查询学生
			stu.setIsExam(YesOrNoConstant.NO);// 去除考试状态
			managerService.updStudData(stu);// 更新数据
		}
	}
}
