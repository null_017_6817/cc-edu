package com.qtrmoon.examination.ctrl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.qtrmoon.common.BaseController;
import com.qtrmoon.examination.pojo.Exer;
import com.qtrmoon.examination.pojo.ReportCard;
import com.qtrmoon.examination.serdao.IExaminationService;
import com.qtrmoon.toolkit.DateTransfer;
import com.qtrmoon.util.CopyOfReportUtil;

/**
 * 成绩控制器
 */
@Controller
@RequestMapping("/examination/scoreCollec")
public class ScoreCollecController extends BaseController {

	@Autowired
	private IExaminationService examinationService; // 声明模块Service实例

	/**
	 * 查看练习列表
	 * @param exer
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schScoreCollec.action")
	public void schExer(Exer exer, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		noCache(response);// ajax设置无缓存
		exer.setCondition(""); // 防sql注入
		exer.assLike("name", "shortcut", "department", "teacher", "expireTime",// 拼接%
				"remark");// 拼接%
		List<Exer> list = examinationService.schExer(exer); // 调用Service查询数据
		/* 将数据Bean的集合转换为Json集合 */
		JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
		JSONObject obj; // 声明用于构造Bean的Json对象
		for (Exer u : list) { // 循环查询的数据集构造Json数据集
			obj = u.getJsonInDict(); // 将Bean转换为Json对象
			obj.put("_oper", u.getId()); // 为"操作列"设定主键值
			datas.add(obj); // 添加Json对象到Json集合
		}
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		JSONObject res = new JSONObject(); // 声明页面返回Json
		res.put("total", exer.getDatasize()); // 设置数据总记录数
		res.put("rows", datas); // 设置数据集
		response.getWriter().print(res.toJSONString()); // 输出到页面
	}

	/**
	 * 查看成绩单
	 * @param exer
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schReportCard.action")
	public void schReportCard(Integer eid, Integer id, ReportCard reportCard,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		noCache(response);// ajax设置无缓存
		reportCard.setCondition(""); // 防sql注入
		reportCard.assLike("name", "markTeacher", "remark");// 拼接%
		List<ReportCard> list = examinationService.schReportCard(reportCard,eid); // 调用Service查询数据
		/* 将数据Bean的集合转换为Json集合 */
		JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
		JSONObject obj; // 声明用于构造Bean的Json对象
		for (ReportCard u : list) { // 循环查询的数据集构造Json数据集
			obj = u.getJsonInDict(); // 将Bean转换为Json对象
			obj.put("_oper", u.getId()); // 为"操作列"设定主键值
			datas.add(obj); // 添加Json对象到Json集合
		}
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		JSONObject res = new JSONObject(); // 声明页面返回Json
		res.put("total", reportCard.getDatasize()); // 设置数据总记录数
		res.put("rows", datas); // 设置数据集
		response.getWriter().print(res.toJSONString()); // 输出到页面
	}

	/**
	 * 预添加or预修改or查看方法
	 * @param exer
	 * 预添加时可传入一些初始值；预修改和查看时用来承载主键值
	 * @param toL
	 * 挂字典的字段是否需要转为字典Label值。true则进行转换
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/vieReportCard.action")
	public void vieReportCard(ReportCard reportCard, Boolean toL,
			HttpServletResponse response) throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (toL != null && toL) { // 查看页面加载数据时调用，返回的对象已进行字典转换
			if (reportCard.getId() != null && reportCard.getId() != 0) { // 检测主键非空
				reportCard = examinationService.schReportCardById(reportCard
						.getId()); // 按主键查询数据
				response.getWriter().print(reportCard.getJsonInDict()); // 输出经过字典转换Json对象到查看页(vie_)
			}
		} else { // 修改页面加载数据时调用
			if (reportCard.getId() != null && reportCard.getId() != 0) { // 检查主键非空
				reportCard = examinationService.schReportCardById(reportCard// 按主键查询数据
						.getId()); // 按主键查询数据
				response.getWriter().print(reportCard.getJson()); // 输出Json对象到修改页(upd_)
			} else { // 添加页面加载数据时调用，可设置添加页面中一些属性的预设值。
				response.getWriter().print(new Exer().getJson()); // 输出Json对象到添加页(upd_)
			}
		}
	}

	/**
	 * 预添加or预修改or查看方法
	 * @param exer
	 * 预添加时可传入一些初始值；预修改和查看时用来承载主键值
	 * @param toL
	 * 挂字典的字段是否需要转为字典Label值。true则进行转换
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/vieScoreCollec.action")
	public void vieExer(Exer exer, Boolean toL, HttpServletResponse response)
			throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (toL != null && toL) { // 查看页面加载数据时调用，返回的对象已进行字典转换
			if (exer.getId() != null && exer.getId() != 0) { // 检测主键非空
				exer = examinationService.schExerById(exer.getId()); // 按主键查询数据
				response.getWriter().print(exer.getJsonInDict()); // 输出经过字典转换Json对象到查看页(vie_)
			}
		} else { // 修改页面加载数据时调用
			if (exer.getId() != null && exer.getId() != 0) { // 检查主键非空
				exer = examinationService.schExerById(exer.getId()); // 按主键查询数据
				response.getWriter().print(exer.getJson()); // 输出Json对象到修改页(upd_)
			} else { // 添加页面加载数据时调用，可设置添加页面中一些属性的预设值。
				response.getWriter().print(new Exer().getJson()); // 输出Json对象到添加页(upd_)
			}
		}
	}

	/**
	 * 添加或修改的提交方法
	 * @param exer
	 * 数据表单Bean
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/updScoreCollec.action")
	public void updExer(Exer exer, HttpServletResponse response)
			throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (exer.getId() == null) { // 检测主键为空则
			examinationService.addExer(exer); // 添加数据
		} else { // 主键非空则
			examinationService.updExer(exer); // 修改数据
		}
	}

	/**
	 * 删除方法
	 * @param ids
	 * 待删除数据的主键数组
	 * @param response
	 */
	@RequestMapping(value = "/delScoreCollec.action")
	public void delExer(@RequestParam(value = "ids[]") Integer[] ids,
			HttpServletResponse response) {
		noCache(response); // Ajax方法设定无缓存
		examinationService.delExerAndTopic(ids); // 删除数据
	}

	/**
	 * JSP页面跳转
	 * @param page
	 * jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value = "/page.action")
	public String goPage(String page, String id, Model model) { // 放过本Ctrl的页面，不合法的跳转到error页。
		if ("index".equals(page) || "scoreCollec_sch".equals(page)// 判断页面
				|| "scoreCollec_vie".equals(page)// 判断页面
				|| "scoreCollec_upd".equals(page)// 判断页面
				|| "sReportCard_sch".equals(page)) {// 判断页面
			model.addAttribute("reportCardId", id);// 设置参数到页面
			return "/examination/" + page;// 跳转页面
		} else {
			return "/error";// 跳转错误页面
		}
	}

	/**
	 * 导出全部和导出选中
	 * @param reportCard
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/downLoadReportCard.action")
	public void downLoadDajg(@RequestParam(value = "ids") Integer[] ids,
			Integer eId,String eName ,ReportCard reportCard, HttpServletResponse response)
			throws IOException {
		response.setCharacterEncoding("utf-8");
		String fileName = new SimpleDateFormat("yyyy-MM-dd").format(new Date()).toString()+".xls";
		if( eName != null && !"".equals(eName) ){
			fileName = "["+eName+"]"+fileName;
		}else if(eId != null){
			Exer exer = examinationService.schExerById(eId);
			if(exer != null && exer.getName() != null && !"".equals(exer.getName())){
				fileName = "["+exer.getName()+"]"+fileName;
			}
		}
		
		noCache(response); // Ajax方法设定无缓存
		List<ReportCard> list = null;// 初始化集合参数
		if (ids != null && ids.length > 0) {// 判断id集合是否为空
			list = examinationService.schReportExcel(ids); // 调用Service查询数据导出选中的
		}
		/* 将数据Bean的集合转换为Json集合 */
		JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
		for (ReportCard u : list) { // 循环查询的数据集构造Json数据集
			datas.add(u.getJsonInDict()); // 添加Json对象到Json集合
		}
		File file = File.createTempFile(DateTransfer.timeToId(), ".xls");// 创建文件对象
		CopyOfReportUtil.writeExcel(ReportCard.getHeads(), datas, file); // 文件路径
		response.reset();// response清空
		response.setContentType("application/octet-stream;charset=utf-8");
		response.setHeader("Content-Disposition",// 设置head信息
				"attachment;filename="+new String(fileName.getBytes(), "iso-8859-1"));// 设置head信息
		InputStream is = new FileInputStream(file);// 设置输入流
		OutputStream out = response.getOutputStream();// 设置输出流
		IOUtils.copy(is, out);// 输出数据
		IOUtils.closeQuietly(is);// 关闭流
		IOUtils.closeQuietly(out);// 关闭流
	}
}
