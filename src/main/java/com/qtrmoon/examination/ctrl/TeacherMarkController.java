package com.qtrmoon.examination.ctrl;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.poifs.filesystem.DirectoryEntry;
import org.apache.poi.poifs.filesystem.DocumentEntry;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.dom4j.DocumentException;
import org.jdom.JDOMException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qtrmoon.book.pojo.Book;
import com.qtrmoon.book.serdao.IBookService;
import com.qtrmoon.book.util.FilePath;
import com.qtrmoon.common.BaseController;
import com.qtrmoon.examination.constant.IsSaveConstant;
import com.qtrmoon.examination.constant.MarkStatusConstant;
import com.qtrmoon.examination.pojo.Exer;
import com.qtrmoon.examination.pojo.ExerAnswer;
import com.qtrmoon.examination.pojo.ExerStep;
import com.qtrmoon.examination.pojo.ExerStepExt;
import com.qtrmoon.examination.pojo.ExerTopic;
import com.qtrmoon.examination.pojo.RExerStud;
import com.qtrmoon.examination.serdao.IExaminationService;
import com.qtrmoon.examination.util.TopicTypeTransUtil;
import com.qtrmoon.manager.constant.TopicTypeConstant;
import com.qtrmoon.manager.pojo.Person;
import com.qtrmoon.manager.pojo.Stud;
import com.qtrmoon.manager.serdao.IManagerService;
import com.qtrmoon.practice.pojo.PracticeAnswer;
import com.qtrmoon.practice.serdao.IPracticeService;
import com.qtrmoon.sysmanage.SysConstant;
import com.qtrmoon.sysmanage.pojo.User;
import com.qtrmoon.sysmanage.serdao.ISysmanageService;
import com.qtrmoon.toolkit.DateTransfer;
import com.qtrmoon.util.FileUpload;
import com.qtrmoon.util.ZipUtil;

/**
 * 教师阅卷控制器
 */
@Controller
@RequestMapping("/examination/mark")
public class TeacherMarkController extends BaseController {

	@Autowired
	private IExaminationService examinationService; // 声明模块Service实例
	@Autowired
	private IManagerService managerService; // 声明模块Service实例
	@Autowired
	private IBookService bookService; // 声明模块Service实例
	@Autowired
	private ISysmanageService sysmanageService;//注入service
	@Autowired
	private IPracticeService practiceService;		
	
	/**
	 * 查询老师需要阅卷的列表
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schTeacherMark.action")
	public void schTeacherMark(Exer exer ,HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		Person user = (Person) request.getSession(true).getAttribute(// 获取当前用户
				SysConstant.CURRENT_USER);// 获取当前用户
		/* 将数据Bean的集合转换为Json集合 */
		JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
		JSONObject obj; // 声明用于构造Bean的Json对象
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		JSONObject res = new JSONObject(); // 声明页面返回Json
		if (user != null && user.getId() != null) {// 如果当前用户不为空
			Integer userId = user.getId();// 获取用户id
			exer.setIsSave(IsSaveConstant.YES_CODE);// 已保存的数据
			exer.assLike("name");
			exer.setTeachers("%," + userId + ",%");// 设置分配老师模糊查询
			if( MarkStatusConstant.MARK_NO.equals(exer.getMarkStatus()) ){
				exer.setCondition("AND (m.markProgress is null OR m.markProgress = 0) ");
			}
			if( MarkStatusConstant.MARK_ING.equals(exer.getMarkStatus()) ){
				exer.setCondition("AND (m.markProgress > 0  AND m.markProgress < 100 )");
			}
			if( MarkStatusConstant.MARK_FINISH.equals(exer.getMarkStatus()) ){
				exer.setCondition("AND m.markProgress = 100 ");
			}
			
			List<Exer> list = examinationService.schMarkExer(exer); // 调用Service查询数据
			for (Exer u : list) { // 循环查询的数据集构造Json数据集
				if( u != null ){
					ExerTopic exerTopic = new ExerTopic();
					exerTopic.setEid(u.getId());
					exerTopic.setType(TopicTypeConstant.SKILL);
					List<ExerTopic> topics = examinationService.schExerTopicList(exerTopic);
					List<ExerStep> steps = getStepsByTopics(topics,u.getId());
					obj = u.getJsonInDict(); // 将Bean转换为Json对象
					obj.put("stepNum", steps.size()); // 为"操作列"设定主键值
					obj.put("_oper", u.getId()); // 为"操作列"设定主键值
					datas.add(obj); // 添加Json对象到Json集合
				}
			}
		}
		res.put("total", exer.getDatasize()); // 设置数据总记录数
		res.put("rows", datas); // 设置数据集
		response.getWriter().print(res.toJSONString()); // 输出到页面
	}

	/**
	 * 查询学生列表
	 * @param exer
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schStudLis.action")
	public void schStudLis(RExerStud rExerStud, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		noCache(response);
		JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
		JSONObject obj; // 声明用于构造Bean的Json对象
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		JSONObject res = new JSONObject(); // 声明页面返回Json
		Stud stud = new Stud();// 实例化学生实体
		if (rExerStud != null && rExerStud.getEid() != null// 判断必要参数是否为空
				&& rExerStud.getClassid() != null// 判断必要参数是否为空
				&& !"".equals(rExerStud.getClassid())) {// 判断必要参数是否为空
			List<RExerStud> reList = examinationService.schRExerStud(rExerStud); // 调用Service查询数据
			String stuIds = "";// 初始化参数
			for (RExerStud entity : reList) {// 循环
				if (entity != null && entity.getStudid() != null) {// 判断必要参数是否为空
					String stuid = entity.getStudid();// 获取学生id
					if (!"".equals(stuIds)) {// 判断id是否为空
						stuIds = stuIds + ",'" + stuid + "'";// 拼接sql条件
					} else {
						stuIds = "'" + stuid + "'";// 拼接sql条件
					}
				}
			}
			if (!"".equals(stuIds)) {// 判断ids是否为空
				stud.setCondition(" AND number in (" + stuIds + ")");//设置动态sql
				List<Stud> stuList = managerService.schStud(stud); // 调用Service查询数据
				for (Stud u : stuList) { // 循环查询的数据集构造Json数据集
					obj = u.getJsonInDict(); // 将Bean转换为Json对象
					obj.put("_oper", u.getId()); // 为"操作列"设定主键值
					datas.add(obj); // 添加Json对象到Json集合
				}
			}
		}
		res.put("total", stud.getDatasize()); // 设置数据总记录数
		res.put("rows", datas); // 设置数据集
		response.getWriter().print(res.toJSONString()); // 输出到页面
	}

	/**
	 * 根据topic获取step和学生的答案
	 * @param exerTopic
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws DocumentException
	 * @throws JDOMException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/getMarkTopicList.action")
	public void getMarkTopicList(ExerTopic exerTopic,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException, DocumentException, JDOMException {
		noCache(response); // //禁止缓存
		String stuNum = request.getParameter("stuNum");// 获取stunum参数
		exerTopic.setCondition(""); // 防sql注入
		JSONObject obj = new JSONObject();// 实例化json对象
		List<ExerTopic> topicList = examinationService.schExerTopic(exerTopic); // 调用Service查询数据
		obj.put("datasize", exerTopic.getDatasize());// 设置参数
		obj.put("page", exerTopic.getPage());// 设置参数

		if (topicList != null && topicList.size() > 0// 判断必要参数是否为空
				&& topicList.get(0) != null) {// 判断必要参数是否为空
			Map<String, Object> map = new HashMap<String, Object>();// 实例化参数集合
			map.put("eid", topicList.get(0).getEid());// 设置参数
			map.put("etid", topicList.get(0).getId());// 设置参数
			map.put("stuNum", stuNum);// 设置参数
			List<ExerStepExt> stepList = examinationService// 调用Service查询数据
					.serchStepAndAnswer(map); // 调用Service查询数据
			obj.put("stepList", stepList);// 设置steplist
		}
		response.getWriter().print(obj.toJSONString());//输出json数据
	}

	/**
	 * 教师阅卷完成后，此方法对个个step的得分求和
	 * @param request
	 * @param response
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value = "/sumTeacherMark.action")
	public Map<String, Object> sumTeacherMark(ExerTopic exerTopic,
			HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> result = new HashMap<String, Object>();// 初始化返回结果集
		try {
			Integer eid = exerTopic.getEid();// 获取参数
			String stuNum = exerTopic.getStudid();// 获取参数
			if (eid != null && stuNum != null) {// 判断必要参数是否为空
				Map<String, Object> param = new HashMap<String, Object>();// 实例化参数集合
				param.put("eid", eid);// 设置参数
				param.put("stuNum", stuNum);// 设置参数
				RExerStud rExerStud = examinationService// 查询数据库
						.updTeacherMarkTotal(param);// 查询数据库
				if (rExerStud != null) {
					result.put("success", true);// 设置返回状态
					result.put("data", rExerStud);// 返回数据
					result.put("msg", "操作成功");// 返回提示信息
				} else {
					result.put("success", false);// 设置返回状态
					result.put("msg", "操作失败,未查询到数据");// 返回提示信息
				}
			} else {
				result.put("success", false);// 设置返回状态
				result.put("msg", "操作失败,参数不能为空");// 返回提示信息
			}
		} catch (Exception e) {
			e.printStackTrace();// 打印异常信息
			result.put("success", false);// 设置返回状态
			result.put("msg", "操作异常");// 返回提示信息
		} finally {
			return result;// 返回数据
		}
	}

	/**
	 * 页面跳转控制方法
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "/page.action")
	public String goPage(String page, String eid, String stuNum, Model model,String endTime) { // 放过本Ctrl的页面，不合法的跳转到error页。
		if ("index".equals(page) || "teacher_mark".equals(page)// 判断跳转页面
				|| "choose_stud".equals(page) || "markTopic".equals(page)
				|| "exer_stud".equals(page) || "exer_mark".equals(page)) {// 判断跳转页面
			if(endTime!=null){
				boolean isCouldMark=false;
				Date date=new Date();
				SimpleDateFormat  sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String dateStr=sdf.format(date);
				if(dateStr.compareTo(endTime)>0){
					isCouldMark=true;
				}
				model.addAttribute("isCouldMark",isCouldMark);
			}
			
			if( "exer_mark".equals(page) ){
				List<String> stus = new ArrayList<String>();
				if(eid != null){
					RExerStud reParam = new RExerStud();
					reParam.setEid(Integer.valueOf(eid));
					List<RExerStud> res = examinationService.schRExerStudList(reParam);
					if(res != null && res.size() > 0){
						for( RExerStud re : res ){
							stus.add(re.getStudid());
						}
					}
				}
				model.addAttribute("stuNumArr",stus);
			}
			model.addAttribute("eid", eid);// 设置参数
			model.addAttribute("stuNum", stuNum);// 设置参数
			return "/examination/" + page;// 跳转页面
		} else {
			return "/error";// 返回错误页面
		}
	}

	/**
	 * 导出学生答案
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings({ "unused" })
	@RequestMapping(value = "/exportSxAnswerToWord.action")
	public void exportSxAnswerToWord(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		String taskId = request.getParameter("taskId"); // 任务ID
		String number = request.getParameter("number"); // 学号
		try {
			if (taskId != null && number != null && !"".equals(taskId)// 判断必要参数是否为空
					&& !"".equals(number)) {// 判断必要参数是否为空
				
				PracticeAnswer pa = new PracticeAnswer();
				pa.setStunum(number);
				pa.setEtid(Integer.valueOf(taskId));
				List<PracticeAnswer> pas = practiceService.schPracticeAnswerList(pa);
				
				String content = "";// 定义常量
				content+="<html><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'><head><style> "
						+ "table {width:100%;} "
						+ "table td {min-width: 15%;} </style></head><body>";// 拼接参数
				if (pas != null && pas.size() > 0) {// 判断集合是否大于0
					for (PracticeAnswer panswer : pas) {
						if(!"".equals(panswer.getTitle())&&panswer.getTitle()!=null){
							content+="<p style='box-sizing: border-box; padding: 0px; margin-top: 0px; margin-bottom: 0px; text-align: center; color: rgb(51, 51, 51); font-family: &quot;Microsoft YaHei&quot;; font-size: 14px; white-space: normal; background-color: rgb(255, 255, 255);'>"+panswer.getTitle()+"</p>";
						}else{
							content+="<p style='box-sizing: border-box; padding: 0px; margin-top: 0px; margin-bottom: 0px; text-align: center; color: rgb(51, 51, 51); font-family: &quot;Microsoft YaHei&quot;; font-size: 14px; white-space: normal; background-color: rgb(255, 255, 255);'>暂无标题</p>";
						}
						
						if (panswer.getStuanswer() != null ) {// 判断集合是否为空
							content += panswer.getStuanswer();// 拼接参数
						} 
					}
				}else{
					ExerStep stepParam = new ExerStep();// 实例化对象
					stepParam.setEtid(Integer.valueOf(taskId));// 设置题目id
					List<ExerStep> stps = examinationService.schExerStepList(stepParam);
					if(stps != null && stps.size() > 0){
						ExerStep stp = stps.get(0);
						if(stp != null && stp.getTitle() != null && !"".equals(stp.getTitle())){
							content += "<p style='box-sizing: border-box; padding: 0px; margin-top: 0px; margin-bottom: 0px; text-align: center; color: rgb(51, 51, 51); font-family: &quot;Microsoft YaHei&quot;; font-size: 14px; white-space: normal; background-color: rgb(255, 255, 255);'>"+stp.getTitle()+"</p>";
						}else{
							content += "<p style='box-sizing: border-box; padding: 0px; margin-top: 0px; margin-bottom: 0px; text-align: center; color: rgb(51, 51, 51); font-family: &quot;Microsoft YaHei&quot;; font-size: 14px; white-space: normal; background-color: rgb(255, 255, 255);'>暂无标题</p>";
						}
						if(stp != null && stp.getStandardAnswer() != null){
							content += stp.getStandardAnswer();
						}
					}
				}
				content +="</body></html>";
				byte b[] = content.getBytes("utf-8"); // 这里是必须要设置编码的，不然导出中文就会乱码。
				ByteArrayInputStream bais = new ByteArrayInputStream(b);// 将字节数组包装到流中
				POIFSFileSystem poifs = new POIFSFileSystem();// 【关键部分】生成word格式
				DirectoryEntry directory = poifs.getRoot(); // 设置word格式
				DocumentEntry documentEntry = directory.createDocument(// 设置word格式
						"WordDocument", bais); // 设置word格式
				String fileName = "";// 定义常量
				ExerTopic topic = examinationService// 根据id查询数据
						.schExerTopicById(Integer.valueOf(taskId));// 根据id查询数据
				if (topic != null) {// 判断topic是否为空
					fileName = getFileName(topic, number);// 调用getFileName
				}
				// 输出文件
				request.setCharacterEncoding("utf-8");// 设置字符集
				response.setContentType("application/msword");// 导出word格式
				response.addHeader("Content-Disposition",// 设置header
						"attachment;filename="// 设置header
						+ new String(// 设置header
								(fileName + ".doc").getBytes(),// 设置header
								"iso-8859-1"));// 设置header
				response.setCharacterEncoding("utf-8");// 设置字符集
				OutputStream ostream = response.getOutputStream();// 输出流
				poifs.writeFilesystem(ostream);// 输出
				bais.close();// 关闭流
				ostream.close();// 关闭流
			} else {
				response.getWriter().println("Lack of necessary parameters!");// 必要参数为空
			}
		} catch (Exception e) {
			response.getWriter().println("server error!");// 系统异常
			e.printStackTrace();// 打印异常信息
		}
	}

	/**
	 * 拼接文件名
	 * @param topic
	 * @param number
	 * @return
	 */
	private String getFileName(ExerTopic topic, String number) {
		Book leaf = bookService.schBookById(Integer.valueOf(topic.getBookId()));// 查询数据
		List<Book> books = new ArrayList<Book>();
		getBookAndParents(books, leaf);// 调用getBookAndParents
		String bookName = "";// 定义常量
		if (books != null && books.size() > 0) {// 判断集合是否为空
			for (int i = books.size() - 1; i >= 0; i--) {// 循环
				if (i == 0) {// 当下标等于0 时
					bookName = bookName + books.get(i).getName();// 拼接参数
				} else {
					bookName = bookName + books.get(i).getName() + "--";// 拼接参数
				}
			}
		}
		String stuName = "(" + number + ")";// 拼接括号
		Stud stu = managerService.schStudByXh(number);// 查询数据
		if (stu != null) {// 如果查询结果不为空
			stuName = stu.getName() + stuName;// 拼接文件名
		}
		String fileName = "【" + bookName + "】" + "【" + topic.getTitle() + "】"// 设置文件名
				+ stuName;// 设置文件名
		return fileName;// 返回文件名称
	}

	/**
	 * 迭代获取（章节）父节点
	 * @param books
	 * @param leaf
	 */
	private void getBookAndParents(List<Book> books, Book leaf) {
		if (leaf != null) {// 判断是否为空
			Integer pid = leaf.getPid();// 获取父节点id
			if (pid != 0) {// 如果父节点id不等于0
				books.add(leaf);// 添加leaf
				Book parent = bookService.schBookById(leaf.getPid());// 根据id查询
				getBookAndParents(books, parent);// 调用getBookAndParents
			} else {
				return;// 跳出循环
			}
		} else {
			return;// 跳出循环
		}
	}
	
	/**
	 * 获取阅卷的题目列表
	 * @param eid 试卷ID
	 * @param studNum 学生学号
	 * @return 
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value="markTopicsList.action")
	public Map<String,Object> markTopicsList(Integer eid , String studNum ){
		Map<String,Object> result = new HashMap<String,Object>();
		try{
			result = examinationService.markTopicsList(eid, studNum);
			
			User user = sysmanageService.schUserByNumber(studNum);
			result.put("user", user);
			result.put("success", true);
		}catch(Exception e){
			result.put("success", false);
			result.put("msg",  "服务器异常");
			e.printStackTrace();
		}finally{
			return result;
		}
	}
	
	/**
	 * 打包下载（指定试卷下所有学生上传的附件答案）
	 * @param eid
	 * @throws Exception 
	 */
	@RequestMapping(value="downAllAnswer.action")
	public void downAllAnswer(Integer eid,HttpServletRequest request,
			HttpServletResponse response) throws Exception{
		try{
			
			if(eid != null ){
				String webRoot = request.getSession().getServletContext().getRealPath("");
				// 查询试卷表
				Exer exer = examinationService.schExerById(eid);
				// 查询关系表
				RExerStud rExerStud = new RExerStud();
				rExerStud.setEid(eid);
				List<RExerStud> rss = examinationService.schMarkRExerStudList(rExerStud);
				
				if(exer != null && rss != null && rss.size() > 0){
					String exerName = exer.getName();
					
					String targetPath = webRoot+File.separator+"data"+File.separator+"answerFile"
							+File.separator + eid + "-" + exerName ;
					String filePath = targetPath;
					String toName = eid + "-" + exerName + ".zip";
					String toPath = webRoot+File.separator+"data"+File.separator+"answerZip"
							+File.separator ;
					String toFullPath = toPath+toName;
					
					ExerTopic exerTopic = new ExerTopic();
					exerTopic.setEid(eid);
					exerTopic.setType(TopicTypeConstant.SKILL);
					List<ExerTopic> topics = examinationService.schAllExerTopic(exerTopic);
					if(topics != null){
						for(int i = 0; i < topics.size(); i++){
							ExerTopic topic = topics.get(i);
							if( topic != null && topic.getId() != null ){
								ExerStep stepParam = new ExerStep();
								stepParam.setEid(eid);
								stepParam.setEtid(topic.getId());
								stepParam.setCondition( "AND type != "+TopicTypeConstant.CLOZE );
								List<ExerStep> steps = examinationService.schExerStepList(stepParam);
								if(steps != null && steps.size() > 0){
									for( int s = 0 ; s < steps.size(); s++ ){
										ExerStep step = steps.get(s);
										if(step != null && step.getId() != null){
											
											ExerAnswer exerAnswer = new ExerAnswer();
											exerAnswer.setEid(eid);
											exerAnswer.setEtid(topic.getId());
											exerAnswer.setEsid(steps.get(s).getId());
											List<ExerAnswer> ans = examinationService.schExerAnswerList(exerAnswer);
											if( ans != null && ans.size() > 0){
												for( ExerAnswer ea : ans ){
													if( ea != null && ea.getStudNumber() != null
															&& ea.getFileUrl() != null ){
														String epath =  File.separator + eid	// 试卷ID
																+ File.separator + ea.getEtid()	// 题目ID
																+ File.separator + ea.getEsid() // 步骤ID
																+ File.separator + ea.getStudNumber();// 学生学号
														String sourcePath = FilePath.getFilePath("stuzippath")+epath+File.separator+ea.getFileUrl();//路径前缀，配置文件中
														File file = new File(sourcePath);
														RExerStud re = getRsByNum(rss,ea.getStudNumber());
														if(file != null && file.exists() && re != null 
																&& re.getClassid() != null && !"".equals(re.getClassid())){
															String typeLabel = TopicTypeTransUtil.stepCodeTransLabel(step.getType());
															String suffix = getSuffix(ea.getFileUrl());// 文件扩展名
															String mkFilePath = targetPath;
															if(topics.size() > 1){
																mkFilePath = targetPath+File.separator+topic.getTitle();
															}
															File newFile = new File(mkFilePath);
															if(!newFile.isDirectory()){
																newFile.mkdirs();
															}
															String upTimeStr = "";
															Date upTime = ea.getUploadTime();
															if(upTime != null){
																upTimeStr = DateTransfer.toString(upTime,"yyyyMMddHHmmss");
															}
															String targetFullPath = mkFilePath+File.separator
																	+ea.getStudNumber()+"-"+upTimeStr+"-【"+typeLabel+"】"+(i+1)+"-"+(s+1)+suffix;
															FileUpload.copyFile(sourcePath, targetFullPath);
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
					
					File toFileDir = new File(toPath);
					if(!toFileDir.isDirectory()){
						toFileDir.mkdirs();
					}
					FileOutputStream fos1 = new FileOutputStream(new File(toFullPath));
					ZipUtil.toZip(filePath, fos1, true);
					ZipUtil.deleteUnzipFile(filePath);
					downZipFile(response,toFullPath,toName);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			String msg = "<div style='text-align: center;padding-top: 15%;'><span style='text-align: center;"
					+ "color: red;font-size: 20px;'>服务器异常，请稍后再试!<span></div>";
			response.setContentType("text/html");//设置contentType属性
			BufferedOutputStream toClient = new BufferedOutputStream(
					response.getOutputStream());// 输出流
			toClient.write(msg.getBytes());// 写
			toClient.flush();// 刷新
		}
	}
	
	private void downZipFile(HttpServletResponse response,String fullPath,String fileName){
		try{
			OutputStream toClient = new BufferedOutputStream(
					response.getOutputStream());// 输出流
			File zipFile = new File(fullPath);
			if(zipFile.exists()){
				File file = new File(fullPath);
				BufferedInputStream fis = new BufferedInputStream(
						new FileInputStream(fullPath));// 输入流
				byte[] buffer = new byte[fis.available()];// 定义字节
				fis.read(buffer);// 读
				fis.close();// 关闭
				// 清空response
				response.reset();
				// 设置response的Header
				response.addHeader("Content-Disposition", "attachment;filename="
						+ new String(fileName.getBytes(), "ISO-8859-1"));// 设置response的Header
				response.addHeader("Content-Length", "" + file.length());// 设置response的Header
				
				response.setContentType("application/vnd.ms-excel;charset=utf-8");// 设置属性
				toClient.write(buffer);// 写
				toClient.flush();// 刷新
				toClient.close();// 关闭
			}else{
				String msg = "<div style='text-align: center;padding-top: 15%;'><span style='text-align: center;"
						+ "color: red;font-size: 20px;'>文件不存在，下载失败!<span></div>";
				response.setContentType("text/html");//设置contentType属性
				toClient = new BufferedOutputStream(
						response.getOutputStream());// 输出流
				toClient.write(msg.getBytes());// 写
				toClient.flush();// 刷新
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 根据学号获取实体
	 * @param rss
	 * @param number
	 * @return
	 */
	private RExerStud getRsByNum(List<RExerStud> rss,String number){
		RExerStud result = null;
		if(rss != null && rss.size() > 0 ){
			for(RExerStud rs : rss){
				if(rs != null && rs.getClassid() != null && !"".equals(rs.getClassid())
					&&  rs.getStudid() != null && rs.getStudid().equals(number) ){
					result = rs;
					break;
				}
			}
		}
		return result;
	}
	
	/**
	 * 获取答案集合
	 * @param topics
	 * @param eid
	 * @return
	 */
	private List<ExerAnswer> getAllAnswerByTopics(List<ExerTopic> topics,Integer eid){
		
		List<ExerAnswer> answers = new ArrayList<ExerAnswer>();
		// 查询全部的步骤
		List<ExerStep> steps = getStepsByTopics(topics,eid);
		// 拼接stepID参数
		String answerSql = "";
		if(steps != null && steps.size() > 0 ){
			for( ExerStep step : steps ){
				if(step != null && step.getId() != null){
					if( !"".equals(answerSql) ){
						answerSql = answerSql + ", '"+step.getId()+"'";
					}else{
						answerSql = "'"+step.getId()+"'";
					}
				}
			}
		}
		//查询答案集合
		if( !"".equals(answerSql) ){
			ExerAnswer ea = new ExerAnswer();
			ea.setEid(eid);
			ea.setCondition("AND esid IN ("+answerSql+")");
			answers = examinationService.schExerAnswerList(ea);
		}
		return answers;
	}
	
	/**
	 * 根据题目列表获取步骤列表
	 * @param topics
	 * @param eid
	 * @return
	 */
	private List<ExerStep> getStepsByTopics(List<ExerTopic> topics,Integer eid){
		String setpSql = "";
		// 查询全部的步骤
		List<ExerStep> steps = null;
		if( topics != null && topics.size() > 0 ){
			for(ExerTopic topic : topics){
				if(topic != null && topic.getId() != null){
					if(!"".equals(setpSql)){
						setpSql = setpSql + ", '"+topic.getId()+"'";
					}else{
						setpSql = "'"+topic.getId()+"'";
					}
				}
			}
		}
		if(!"".equals(setpSql)){
			ExerStep step = new ExerStep();
			step.setEid(eid);
			step.setCondition("AND etid IN ("+setpSql+") AND type != "+TopicTypeConstant.CLOZE);
			steps = examinationService.schExerStepList(step);
		}
		if(steps == null){
			steps = new ArrayList<ExerStep>();
		}
		return steps;
	}
	
	
	/**
	 * 获取扩展名称
	 * @param filename
	 * @return
	 */
	private String getSuffix(String filename){
		String suffix = "";
		if ((filename != null) && (filename.length() > 0)) {  // 判断 
			int dot = filename.lastIndexOf('.');  // 截取 
			if ((dot >-1) && (dot < (filename.length() - 1))) {  // 判断 
				suffix = filename.substring(dot + 1); // 截取  
			}
		}
		if(!"".equals(suffix)){
			suffix="."+suffix;
		}
		return suffix;
	}
	
}
