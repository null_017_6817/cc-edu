package com.qtrmoon.examination.ctrl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.qtrmoon.common.BaseController;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.examination.constant.AddTypeConstant;
import com.qtrmoon.examination.constant.IsSaveConstant;
import com.qtrmoon.examination.pojo.ExamCategory;
import com.qtrmoon.examination.pojo.Exer;
import com.qtrmoon.examination.pojo.ExerTopic;
import com.qtrmoon.examination.pojo.RExerStud;
import com.qtrmoon.examination.serdao.IExaminationService;
import com.qtrmoon.examination.util.ExamShutCutUtil;
import com.qtrmoon.examination.util.TopicTypeTransUtil;
import com.qtrmoon.manager.pojo.Bank;
import com.qtrmoon.manager.pojo.Person;
import com.qtrmoon.manager.pojo.Stud;
import com.qtrmoon.manager.serdao.IManagerService;
import com.qtrmoon.sysmanage.SysConstant;
import com.qtrmoon.sysmanage.constant.StateConstant;
import com.qtrmoon.sysmanage.pojo.User;
import com.qtrmoon.sysmanage.serdao.ISysmanageService;

/**
 * 试卷控制器
 */
@Controller
@RequestMapping("/examination/exer")
public class ExerController extends BaseController {
	@Autowired
	private IExaminationService examinationService; // 声明模块Service实例
	@Autowired
	private ISysmanageService sysmanageService;// 声明模块Service实例
	@Autowired
	private IManagerService managerService;// 声明模块Service实例

	/**
	 * 查看练习列表
	 * @param exer
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schExer.action")
	public void schExer(Exer exer, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		noCache(response);// 设置ajax 无缓存
		exer.setCondition(""); // 防sql注入
		exer.assLike("name", "department", "teacher", "expireTime",// 拼接%
				"remark");// 拼接%
		
		if(exer.getShortcut() != null && !"".equals(exer.getShortcut())){
			List<Integer> shutCutList = new ArrayList<Integer>();
			Integer pid = Integer.valueOf(exer.getShortcut());
			getAllShutCutId(pid,shutCutList);
			String shutCutsStr = getShutcutSqlStr(shutCutList);
			exer.setShortcut("");
			exer.setCondition("AND shortcut in ("+shutCutsStr+")");
		}
		exer.setIsSave(IsSaveConstant.YES_CODE);// 设置状态为已保存
		exer.setOrderCol("ID DESC");
		List<Exer> list = examinationService.schExer(exer); // 调用Service查询数据
		/* 将数据Bean的集合转换为Json集合 */
		JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
		JSONObject obj; // 声明用于构造Bean的Json对象
		for (Exer u : list) { // 循环查询的数据集构造Json数据集
			RExerStud re = new RExerStud();
			re.setEid(u.getId());
			int stuNum = 0;
			List<RExerStud> rss = examinationService.schRExerStudList(re);
			if(rss != null && rss.size() > 0){
				stuNum = rss.size();
			}
			obj = u.getJsonInDict(); // 将Bean转换为Json对象
			obj.put("stuNum", stuNum);
			obj.put("_oper", u.getId()); // 为"操作列"设定主键值
			datas.add(obj); // 添加Json对象到Json集合
		}
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		JSONObject res = new JSONObject(); // 声明页面返回Json
		res.put("total", exer.getDatasize()); // 设置数据总记录数
		res.put("rows", datas); // 设置数据集
		response.getWriter().print(res.toJSONString()); // 输出到页面
	}

	
	/**
	 * 查看练习列表
	 * @param exer
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schExerByCollec.action")
	public void schExerByCollec(Exer exer, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		noCache(response);// 设置ajax 无缓存
		exer.setCondition(""); // 防sql注入
		exer.assLike("name", "department", "teacher", "expireTime",// 拼接%
				"remark");// 拼接%
		
		Person person = (Person) request.getSession().getAttribute(// 获取当前用户
				SysConstant.CURRENT_USER);// 获取当前用户
		if(person != null && StateConstant.TEACHER.equals(person.getState())){
			if(exer != null ){
				exer.setCondition("AND t.start_time is not null and t.end_time is not null"
					+ " AND teachers LIKE '%"+person.getId()+"%'");
				exer.setIsSave(IsSaveConstant.YES_CODE);// 设置状态为已保存
				List<Exer> list = examinationService.schExer(exer); // 调用Service查询数据
				/* 将数据Bean的集合转换为Json集合 */
				JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
				JSONObject obj; // 声明用于构造Bean的Json对象
				for (Exer u : list) { // 循环查询的数据集构造Json数据集
					obj = u.getJsonInDict(); // 将Bean转换为Json对象
					obj.put("_oper", u.getId()); // 为"操作列"设定主键值
					datas.add(obj); // 添加Json对象到Json集合
				}
				/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
				JSONObject res = new JSONObject(); // 声明页面返回Json
				res.put("total", exer.getDatasize()); // 设置数据总记录数
				res.put("rows", datas); // 设置数据集
				response.getWriter().print(res.toJSONString()); // 输出到页面
			}
		}
	}
	
	/**
	 * 递归添加ID
	 * @param pid
	 * @param list
	 */
	private void getAllShutCutId(Integer pid , List<Integer> shutCuts){
		if( pid != null ){
			shutCuts.add(pid);
		}
		ExamCategory examCategory = new ExamCategory();
		examCategory.setPid(pid);;
		List<ExamCategory> examCategorys = examinationService.schExamCategoryList(examCategory);
		if(examCategorys != null && examCategorys.size() > 0){
			for(ExamCategory item : examCategorys){
				Integer itemId = item.getId();
				getAllShutCutId(itemId,shutCuts);
			}
		}
	}
	
	private String getShutcutSqlStr(List<Integer> shutCuts){
		String result = "";
		if( shutCuts != null && shutCuts.size() > 0 ){
			for(Integer id : shutCuts){
				if(id != null){
					if( !"".equals(result) ){
						result = result + ", '"+id+"'";
					}else{
						result = "'"+id+"'";
					}
				}
			}
		}
		return result;
	}
	
	/**
	 * 查看练习
	 * @param exer
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/schExerById.action")
	public void schExerById(Integer id, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		noCache(response);// 设置ajax 无缓存
		Exer exer = examinationService.schExerById(id); // 调用Service查询数据
		/* 将数据Bean的集合转换为Json集合 */
		JSONObject obj; // 声明用于构造Bean的Json对象
		obj = exer.getJsonInDict(); // 将Bean转换为Json对象
		response.getWriter().print(obj.toJSONString()); // 输出到页面
	}

	/**
	 * 查看练习列表(学生)
	 * @param exer
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings({ "finally", "unchecked" })
	@ResponseBody
	@RequestMapping(value = "/schExerAndStatus.action")
	public Map<String,Object> schExerAndStatus(Exer exer, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		noCache(response);// 设置ajax 无缓存
		exer.setCondition("");
		Map<String,Object> result = new HashMap<String,Object>();
		String pageNo = (String)request.getParameter("pageNo");
		String pageSize = (String)request.getParameter("pageSize");
		try{
			if(exer != null && exer.getShortcut() != null && !"".equals(exer.getShortcut())){
				List<Integer> shutCutList = new ArrayList<Integer>();
				Integer pid = Integer.valueOf(exer.getShortcut());
				getAllShutCutId(pid,shutCutList);
				String shutCutsStr = getShutcutSqlStr(shutCutList);
				exer.setShortcut("");
				exer.setCondition("AND t.shortcut in ("+shutCutsStr+") and t.start_time is not null and t.end_time is not null");
			}
			
			//exer.setCondition(""); // 防sql注入
			exer.assLike("name");// 拼接%
			Person person = (Person) request.getSession().getAttribute(// 获取当前用户
					SysConstant.CURRENT_USER);// 获取当前用户
			if (person != null && StateConstant.STUD.equals(person.getState())) {// 判断当前用户是否为空,且为学生
				String number = person.getNumber();
				exer.setNumber(number);//设置学号
				exer.setCondition(exer.getCondition()+"  ORDER BY CAST(t.name as SIGNED)");
				List<Exer> list = examinationService.schExerAndStatus(exer); // 调用Service查询数据
				int startIndex = (Integer.valueOf(pageNo)-1)*Integer.valueOf(pageSize);
				int endIndex = (Integer.valueOf(pageNo)*Integer.valueOf(pageSize))-1;
				JSONArray datas = new JSONArray();
				int total = 0;// 初始化数据总条数
				if(list != null && list.size() > 0){
					total = list.size();// 数据总条数设置
					for(int i = startIndex ; i <= endIndex; i++){// 根据分页参数截取数据
						if(i <= (total-1) && list.get(i) != null){
							Exer ex = list.get(i);
							JSONObject obj = ex.getJsonInDict();
							if (ex != null && ex.getEndTime() != null// 判断必要参数是否为null
									&& ex.getStartTime() != null) {// 判断必要参数是否为null
								long diff = (ex.getEndTime().getTime())// 计算时间差
										- (ex.getStartTime().getTime());// 计算时间差
								long diffs = diff / (1000 * 60);// 转换为分钟
								obj.put("diff", diffs);
							}
							datas.add(obj);
						}
					}
					result.put("total", total);
				} 
				result.put("success", true);
				result.put("data", datas);
			}else{
				result.put("success", false);
				result.put("msg", "登录状态异常，请重新登录！");
			}
		}catch(Exception e ){
			result.put("success", false);
			result.put("msg", "服务器异常！请重试");
			e.printStackTrace();
		}finally{
			return result;
		}
	}
	
	/**
	 * 
	 * @param exer
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@SuppressWarnings({ "finally", "unchecked" })
	@ResponseBody
	@RequestMapping(value="schExcerByTeacher.action")
	public Map<String,Object> schExcerByTeacher(Exer exer, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		
		Map<String,Object> result = new HashMap<String,Object>();
		noCache(response);// 设置ajax 无缓存
		String pageNo = (String)request.getParameter("pageNo");
		String pageSize = (String)request.getParameter("pageSize");
		try{
			Person person = (Person) request.getSession().getAttribute(// 获取当前用户
					SysConstant.CURRENT_USER);// 获取当前用户
			if(person != null && StateConstant.TEACHER.equals(person.getState())){
				
				if(exer != null ){
					
					if (exer.getShortcut() != null && !"".equals(exer.getShortcut())){
						List<Integer> shutCutList = new ArrayList<Integer>();
						Integer pid = Integer.valueOf(exer.getShortcut());
						getAllShutCutId(pid,shutCutList);
						String shutCutsStr = getShutcutSqlStr(shutCutList);
						exer.setShortcut("");
						exer.setCondition("AND t.shortcut in ("+shutCutsStr+") AND t.start_time is not null and t.end_time is not null"
								+ "  AND (  teachers LIKE '%"+person.getId()+"%' OR teacher = '"+person.getId()+"' )");
					}else{
						exer.setCondition("AND t.start_time is not null and t.end_time is not null"
								+ "  AND (  teachers LIKE '%"+person.getId()+"%' OR teacher = '"+person.getId()+"' )");
					}
				}
				exer.setCondition(exer.getCondition()+"  ORDER BY CAST(t.name as SIGNED)");
				List<Exer> list = examinationService.schExerList(exer);
				int startIndex = (Integer.valueOf(pageNo)-1)*Integer.valueOf(pageSize);
				int endIndex = (Integer.valueOf(pageNo)*Integer.valueOf(pageSize))-1;
				JSONArray datas = new JSONArray();
				int total = 0;// 初始化数据总条数
				if(list != null && list.size() > 0){
					total = list.size();// 数据总条数设置
					for(int i = startIndex ; i <= endIndex; i++){// 根据分页参数截取数据
						if(i <= (total-1) && list.get(i) != null){
							Exer ex = list.get(i);
							JSONObject obj = ex.getJsonInDict();
							if (ex != null && ex.getEndTime() != null// 判断必要参数是否为null
									&& ex.getStartTime() != null) {// 判断必要参数是否为null
								long diff = (ex.getEndTime().getTime())// 计算时间差
										- (ex.getStartTime().getTime());// 计算时间差
								long diffs = diff / (1000 * 60);// 转换为分钟
								obj.put("diff", diffs);
							}
							datas.add(obj);
						}
					}
					result.put("total", total);
				} 
				result.put("success", true);
				result.put("data", datas);
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			return result;
		}
		
	}
	
	/**
	 * 查询题目列表
	 * @param exerTopic
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schExerTopic.action")
	public void schExerTopic(ExerTopic exerTopic, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		noCache(response);// 设置ajax 无缓存
		exerTopic.setCondition(""); // 防sql注入
		String col = exerTopic.getOrderCol();
		if(StringUtils.isEmpty(col) || ",".equals(col)){
			exerTopic.setOrderCol("order_no");
		}
		List<ExerTopic> list = examinationService.schExerTopicList(exerTopic); // 调用Service查询数据
		/* 将数据Bean的集合转换为Json集合 */
		JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
		JSONObject obj; // 声明用于构造Bean的Json对象
		Exer exer = examinationService.schExerById(exerTopic.getEid());// 根据id查询数据
		String exerName = "";//初始化参数
		if (exer != null) {//判断参数是否为null
			exerName = exer.getName();// 获取名称
		}
		for (ExerTopic u : list) { // 循环查询的数据集构造Json数据集
			obj = u.getJsonInDict(); // 将Bean转换为Json对象
			obj.put("exerName", exerName);// 设置名称
			obj.put("_oper", u.getId()); // 为"操作列"设定主键值
			datas.add(obj); // 添加Json对象到Json集合
		}
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		JSONObject res = new JSONObject(); // 声明页面返回Json
		res.put("total", exerTopic.getDatasize()); // 设置数据总记录数
		res.put("rows", datas); // 设置数据集
		response.getWriter().print(res.toJSONString()); // 输出到页面
	}

	/**
	 * 查询老师列表
	 * @param user
	 * @param organids
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schUser.action")
	public void schUser(User user, String organids, HttpServletResponse response)
			throws IOException {
		noCache(response);// 设置ajax 无缓存
		user.setCondition(""); // 防sql注入
		user.assLike("username");// 拼接%
		user.setState("2"); // 发卷给老师（选取老师）
		if (organids != null && organids.indexOf(",") > 0) {// 判断参数
			String sql = "";//初始化参数
			String[] orgs = organids.split(",");// 根据逗号拆分字符串
			for (String org : orgs) {// 循环
				sql += "organid='" + org + "' or ";// 拼接参数
			}
			sql = sql.substring(0, sql.length() - 3);// 截取字符串
			user.setOrganid(null);// 设置机构id
			user.setCondition(" and(" + sql + ")");// 设置动态sql
		} else if (organids != null && !organids.equals("")) {// 判断必要参数是否为空
			user.setOrganid(Integer.parseInt(organids)); // 设置机构id
		}
		List<User> list = null;// 初始化集合
		list = sysmanageService.schUser(user);// 查询user
		JSONArray datas = new JSONArray();// 初始化json集合对象
		JSONObject obj;// 初始化json对象
		for (User u : list) {// 循环
			obj = u.getJsonInDict();// 获取json数据
			obj.put("_oper", u.getId());// 设置_oper
			datas.add(obj);// 集合添加元素
		}
		JSONObject res = new JSONObject();// 初始化json对象
		res.put("total", user.getDatasize());// 设置返回数据
		res.put("rows", datas);// 设置返回数据
		response.getWriter().print(res.toJSONString());// 返回json数据
	}

	/**
	 * 根据关系表查询学生
	 * @param exer
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schStudByRe.action")
	public void schStudByRe(Exer exer, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		noCache(response);// 设置ajax 无缓存
		if (exer != null && exer.getId() != null) {// 判断必要参数是否为null
			Integer eid = exer.getId();// 获取试卷id
			RExerStud rExerStud = new RExerStud();// 初始化对象
			rExerStud.setEid(eid);// 设置试卷id
			List<RExerStud> list = examinationService.schRExerStud(rExerStud); // 调用Service查询数据
			if (list != null && list.size() > 0) {// 判断集合是否为空
				String stuIds = "";// 定义参数
				for (int i = 0; i < list.size(); i++) { // 循环
					RExerStud rEntity = list.get(i);// 获取第i个元素
					if (rEntity != null && rEntity.getStudid() != null // 判断必要参数是否为空
							&& !"".equals(rEntity.getStudid())) {// 判断必要参数是否为空
						if (!"".equals(stuIds)) {// 判断必要参数是否为空
							stuIds = stuIds + ",'" + rEntity.getStudid() + "'";// 拼接参数
						} else {
							stuIds = "'" + rEntity.getStudid() + "'";// 拼接参数
						}
					}
				}
				if (!"".equals(stuIds)) {// 判断必要参数是否为空
					Stud stud = new Stud();// 实例化学生对象
					stud.setCondition(" and number in (" + stuIds + ")");// 设置动态sql
					List<Stud> stuList = managerService.schStud(stud); // 调用Service查询数据
					/* 将数据Bean的集合转换为Json集合 */
					JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
					JSONObject obj; // 声明用于构造Bean的Json对象
					for (Stud u : stuList) { // 循环查询的数据集构造Json数据集
						obj = u.getJsonInDict(); // 将Bean转换为Json对象
						obj.put("_oper", u.getId()); // 为"操作列"设定主键值
						datas.add(obj); // 添加Json对象到Json集合
					}
					/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
					JSONObject res = new JSONObject(); // 声明页面返回Json
					res.put("total", stud.getDatasize()); // 设置数据总记录数
					res.put("rows", datas); // 设置数据集
					response.getWriter().print(res.toJSONString()); // 输出到页面
				}
			}
		}
	}

	/**
	 * 预添加or预修改or查看方法
	 * @param exer
	 * 预添加时可传入一些初始值；预修改和查看时用来承载主键值
	 * @param toL
	 * 挂字典的字段是否需要转为字典Label值。true则进行转换
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/vieExer.action")
	public void vieExer(Exer exer, Boolean toL, HttpServletResponse response)
			throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (toL != null && toL) { // 查看页面加载数据时调用，返回的对象已进行字典转换
			if (exer.getId() != null && exer.getId() != 0) { // 检测主键非空
				exer = examinationService.schExerById(exer.getId()); // 按主键查询数据
				response.getWriter().print(exer.getJsonInDict()); // 输出经过字典转换Json对象到查看页(vie_)
			}
		} else { // 修改页面加载数据时调用
			if (exer.getId() != null && exer.getId() != 0) { // 检查主键非空
				exer = examinationService.schExerById(exer.getId()); // 按主键查询数据
				if(exer != null){
					String bankIds = exer.getBankIdsStr();
					List<Map<String,Object>> listmap = new ArrayList<Map<String,Object>>();
					if(bankIds != null && !"".equals(bankIds)){
						Bank bankParam = new Bank();
						bankParam.setCondition("AND bank_id in ("+bankIds+")");
						List<Bank> banks = managerService.schBankList(bankParam);
						if(banks != null){
							for( Bank bank : banks ){
								if(bank != null){
									Map<String,Object> map = new HashMap<String,Object>();
									map.put("bankId", bank.getBankId());
									map.put("bankName", bank.getBankName());
									listmap.add(map);
								}
							}
							
						}
					}
					String jsonString = JSON.toJSONString(listmap);
					exer.setBankIdsStr(jsonString);
					response.getWriter().print(exer.getJson()); // 输出Json对象到修改页(upd_)
				}
			} else { // 添加页面加载数据时调用，可设置添加页面中一些属性的预设值。
				response.getWriter().print(new Exer().getJson()); // 输出Json对象到添加页(upd_)
			}
		}
	}
	
	/**
	 * 添加或修改的提交方法
	 * @param exer
	 * 数据表单Bean
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/updExer.action")
	public void updExer(Exer exer, HttpServletResponse response)
			throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (exer.getId() == null) { // 检测主键为空则
			exer.setCreateTime(new Date());//设置创建时间
			examinationService.addExer(exer); // 添加数据
		} else { // 主键非空则
			Exer ee = examinationService.schExerById(exer.getId());// 查询数据
			if(ee != null){
				exer.setTeachers(ee.getTeachers());
				exer.setCreateTime(ee.getCreateTime());//设置创建时间
			}
			examinationService.updExer(exer); // 修改数据
		}
	}

	/**
	 * 修改被分配老师
	 * @param exer
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping(value = "/updTeacher.action")
	public void updTeacher(Exer exer, HttpServletResponse response)
			throws IOException {
		noCache(response); // Ajax方法设定无缓存
		JSONObject obj = new JSONObject();// 初始化json数据
		try {
			if (exer != null && exer.getId() != null// 判断必要参数是否为空
					&& !"".equals(exer.getId())) {// 判断必要参数是否为空
				Exer entity = examinationService.schExerById(exer.getId());// 根据id查询数据
				entity.setTeachers(exer.getTeachers());// 设置阅卷老师
				examinationService.updExer(entity); // 修改数据
				obj.put("success", true);// 返回成功标识
				obj.put("msg", "保存成功");// 返回提示信息
			} else {
				obj.put("success", false);// 返回成功标识
				obj.put("msg", "保存失败");// 返回提示信息
			}
		} catch (Exception e) {
			e.printStackTrace();
			obj.put("success", false);// 返回成功标识
			obj.put("msg", "保存异常");// 返回提示信息
		} finally {
			response.getWriter().print(obj.toJSONString());// 返回json数据
		}
	}

	/**
	 * 撤销试卷
	 * @param numbers
	 * @param eid
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping(value = "/delReStudents.action")
	public void delReStudents(String numbers, String eid,
			HttpServletResponse response) throws IOException {
		noCache(response); // Ajax方法设定无缓存
		JSONObject result = new JSONObject();// 初始化json数据
		try {
			if (numbers != null) {// 判断必要参数是否为空
				String[] numberArg = numbers.split(",");// 根据逗号拆分字符串
				String inParam = "";// 定义参数
				for (int i = 0; i < numberArg.length; i++) {// 循环
					if (numberArg[i] != null && !"".equals(numberArg[i])) {// 判断必要参数是否为空
						if (!"".equals(inParam)) {// 判断必要参数是否为空
							inParam = inParam + ",'" + numberArg[i] + "'";// 拼接字符串
						} else {
							inParam = "'" + numberArg[i] + "'";// 拼接字符串
						}
					}
				}
				Map<String, Object> param = new HashMap<String, Object>();// 初始化查询条件集合
				param.put("eid", eid);// 设置试卷id
				param.put("condition", " and studid in (" + inParam + ")");// 设置动态sql
				examinationService.delReStudents(param);// 删除数据
				result.put("success", true);// 返回成功标识
				result.put("msg", "操作成功");// 返回提示信息
			} else {
				result.put("success", false);// 返回成功标识
				result.put("msg", "操作失败");// 返回提示信息
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);// 返回成功标识
			result.put("msg", "操作异常");// 返回提示信息
		} finally {
			response.getWriter().print(result.toJSONString());// 返回json数据
		}
	}

	/**
	 * 删除方法
	 * @param ids
	 * 待删除数据的主键数组
	 * @param response
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value = "/delExer.action")
	public Map<String, Object> delExer(
			@RequestParam(value = "ids[]") Integer[] ids,
			HttpServletResponse response) {
		noCache(response); // Ajax方法设定无缓存
		Map<String, Object> map = new HashMap<String, Object>();// 初始化返回结果集
		try {
			String eids = "";// 定义参数
			if (ids != null && ids.length > 0) {// 判断必要参数是否为空
				for (int i = 0; i < ids.length; i++) {// 循环
					if (ids[i] != null && !"".equals(ids[i])) {// 判断必要参数是否为空
						if ("".equals(eids)) {// 判断必要参数是否为空
							eids = "'" + ids[i] + "'";// 拼接参数
						} else {
							eids = eids + "," + "'" + ids[i] + "'";// 拼接参数
						}
					}
				}
				ExerTopic exerTopic = new ExerTopic();// 实例化对象
				exerTopic.setCondition("AND eid in (" + eids + ")");// 设置动态sql语句
				List<ExerTopic> list = examinationService// 调用Service查询数据
						.schExerTopic(exerTopic); // 调用Service查询数据
				if (list != null && list.size() > 0) {// 判断集合是否为空
					map.put("success", false);// 返回成功标识
					map.put("msg", "所选练习含有试题，禁止删除");// 返回提示信息
				} else {
					examinationService.delExer(ids); // 删除数据
					map.put("success", true);// 返回成功标识
					map.put("msg", "删除成功");// 返回提示信息
					if(ids.length>0){
						Map<String, Object> param = new HashMap<String, Object>();// 初始化查询条件集合
						for(int i=0;i<ids.length;i++){
							param.put("eid", ids[i]);// 设置试卷id
							examinationService.delReStudents(param);// 删除数据
						}
					}
					DictBuffer.updCache("ZD_SJ");//更新缓存
				}
			} else {
				map.put("success", false);// 返回成功标识
				map.put("msg", "参数为空，删除失败");// 返回提示信息
			}
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);// 返回成功标识
			map.put("msg", "删除异常");// 返回提示信息
		} finally {
			return map;
		}
	}

	/**
	 * 更新试卷和题目
	 * @param exer
	 * @param request
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value="saveExerAndTopic.action")
	public Map<String,Object> saveExerAndTopic(@RequestBody Exer exer, HttpServletRequest request){
		// 实例化返回结果集
		Map<String,Object> result =  new HashMap<String,Object>();
		try{
			// 判断参数是否为空
			if(exer != null && exer.getId() != null ){
				result = examinationService.updExerAndTopic(exer,request);
				DictBuffer.updCache("ZD_SJ");// 更新缓存
			}else{
				result.put("success", false);
				result.put("msg", "必要参数缺失，请重新打开此页面添加试题");
			}
		}catch(Exception e){
			e.printStackTrace();
			result.put("success", false);
			result.put("msg", "系统异常,请重新登录后再试");
		}finally{
			return result;
		}
	}
	
	/**
	 * 添加默认试卷(手动组卷)
	 * @param exer
	 * @param request
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value="addDeFaultHandExer.action" , method=RequestMethod.POST)
	public Map<String,Object> addDeFaultExer(HttpServletRequest request){
		// 实例化返回结果集
		Map<String,Object> result =  new HashMap<String,Object>();
		try{
			Person person = (Person)request.getSession(true).getAttribute(SysConstant.CURRENT_USER);
			if(person != null && person.getId() != null){
				//试卷类型为手动组卷【AddTypeConstant.AUTOMATIC=0】
				Exer exer = addDeFaultExer(AddTypeConstant.HAND,person);
				result.put("success", true);
				result.put("data", exer);
			}else{
				result.put("success", false);
				result.put("msg", "登录状态异常，请重新登录");
			}
		}catch(Exception e){
			e.printStackTrace();
			result.put("success", false);
			result.put("msg", "系统异常,请重新登录");
		}finally{
			return result;
		}
	}
	
	/**
	 * 添加默认试卷（自动组卷）
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value="addDeFaultAutoExer.action" , method=RequestMethod.POST)
	public Map<String,Object> addDeFaultAutoExer(HttpServletRequest request){
		// 实例化返回结果集
		Map<String,Object> result =  new HashMap<String,Object>();
		try{
			Person person = (Person)request.getSession(true).getAttribute(SysConstant.CURRENT_USER);
			if(person != null && person.getId() != null){
				//试卷类型为手动组卷【AddTypeConstant.AUTOMATIC=1】
				Exer exer = addDeFaultExer(AddTypeConstant.AUTOMATIC,person);
				result.put("success", true);
				result.put("data", exer);
			}else{
				result.put("success", false);
				result.put("msg", "登录状态异常，请重新登录");
			}
		}catch(Exception e){
			e.printStackTrace();
			result.put("success", false);
			result.put("msg", "系统异常,请重新登录");
		}finally{
			return result;
		}
	}
	
	/**
	 * 添加默认试卷
	 * @param addType
	 * @param person
	 */
	private Exer addDeFaultExer(String addType,Person person){
		Exer exer = new Exer();
		exer.setIsSave(IsSaveConstant.NO_CODE);// 设置初始值为未保存
		exer.setTeacher(person.getId().toString());// 设置发起老师
		exer.setCreateTime(new Date());// 创建日期
		exer.setAddType(addType);// 添加类型
		exer.setRemark("预保存数据");// 设置备注
		examinationService.addExer(exer);
		return exer;
	}
	
	/**
	 * 取消操作控制器
	 * @param exer
	 * @param request
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value="cancelExerAndTopic.action" , method=RequestMethod.POST)
	public Map<String,Object> cancelExerAndTopic( Exer exer ,HttpServletRequest request){
		// 实例化返回结果集
		Map<String,Object> result =  new HashMap<String,Object>();
		try{
			if( exer != null && exer.getId() != null ){
				/** 取消按钮service操作
				 *  若此试卷未保存则删除此试卷和试卷中的题目
				 *  若此试卷已保存则无操作
				 */ 
				examinationService.updCancel(exer);
				result.put("success", true);
				result.put("data", exer);
			}else{
				result.put("success", false);
				result.put("msg", "必要参数缺失，请重新登录后再试");
			}
		}catch(Exception e){
			e.printStackTrace();
			result.put("success", false);
			result.put("msg", "系统异常,请重新登录");
		}finally{
			return result;
		}
	}
	
	/**
	 * 预览试卷
	 * @param exer
	 * @param request
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value="previewExer.action")
	public Map<String,Object> previewExer(Exer exer ,HttpServletRequest request){
		Map<String,Object> result = new HashMap<String,Object>();
		try{
			if(exer != null && exer.getId() != null){
				String projectName = request.getContextPath();// 获取参数
				result = examinationService.previewExer(exer,projectName);
				result.put("topicTypeMap", TopicTypeTransUtil.getTopicTypeToMap());// 题目类型集合
				result.put("stepTypeMap", TopicTypeTransUtil.getStepTypeToMap());// 步骤类型集合
				result.put("success", true);
			}else{
				result.put("success", false);
				result.put("msg", "必要参数缺失,请重试！");
			}
		}catch(Exception e){
			e.printStackTrace();
			result.put("success", true);
			result.put("msg", "服务器异常，请重新登录后再试！");
		}finally{
			return result;
		}
	}
	
	/**
	 * JSP页面跳转
	 * @param page
	 * jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value = "/page.action")
	public String goPage(String page, String id, Integer stuNum, Model model) { // 放过本Ctrl的页面，不合法的跳转到error页。
		if ("index".equals(page) || "exer_sch".equals(page)
				|| "exer_vie".equals(page) || "exer_upd".equals(page)
				|| "exer_exerTopic".equals(page) || "exer_autoTopic".equals(page)
				|| "exer_preview".equals(page) || "exer_toPerson".equals(page)
				|| "exers_toPerson".equals(page)) {// 判断跳转页面
			model.addAttribute("exerId", id);// 设置参数到页面
			if (stuNum != null) {
				model.addAttribute("stuNum", stuNum);
			} else {
				model.addAttribute("stuNum", 0);
			}
			return "/examination/" + page;// 跳转页面
		} else {
			return "/error";// 跳转错误页面
		}
	}
	
	/**
	 * 根据虚拟根节点获取数据
	 * @param request
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value="getShutCutByRoot.action")
	public Map<String,Object> getShutCutByRoot(HttpServletRequest request){
		Map<String,Object> result = new HashMap<String,Object>();
		try{
			List<Map<String,String>> dicts = ExamShutCutUtil.getDictByRoot();
			result.put("success", true);
			result.put("data", dicts);
		}catch(Exception e){
			result.put("success", false);
			result.put("msg", "服务器异常");
			logger.error("根据根节点获取试卷字典异常");
			e.printStackTrace();
		}finally{
			return result;
		}
	}
	
	/**
	 * 根据虚拟根节点获取数据
	 * @param request
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value="getShutCutByPid.action")
	public Map<String,Object> getShutCutByPid(String pid ,HttpServletRequest request){
		Map<String,Object> result = new HashMap<String,Object>();
		try{
			List<Map<String,String>> dicts = ExamShutCutUtil.getDictByPid(pid);
			result.put("success", true);
			result.put("data", dicts);
		}catch(Exception e){
			result.put("success", false);
			result.put("msg", "服务器异常");
			logger.error("根据父节点获取试卷字典异常");
			e.printStackTrace();
		}finally{
			return result;
		}
	}
	
	
	/**
	 * 更新答题要求
	 * @param request
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value="updAnswerReq.action")
	public Map<String,Object> updAnswerReq(HttpServletRequest request,
			Integer id, String answerReq){
		
		Map<String,Object> result = new HashMap<String,Object>();
		try{
			if (id != null) {
				Exer exer = examinationService.schExerById(id);
				exer.setAnswerReq(answerReq);
				examinationService.updExer(exer);
				result.put("success", true);
				result.put("msg", "操作成功");
			} else {
				result.put("success", false);
				result.put("msg", "必要参数缺失");
			}
		}catch(Exception e){
			result.put("success", false);
			result.put("msg", "服务器异常");
			logger.error("更新试卷的答题要求异常");
			e.printStackTrace();
		}finally{
			return result;
		}
	}
	
}