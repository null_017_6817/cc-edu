package com.qtrmoon.examination.ctrl;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qtrmoon.book.util.FilePath;
import com.qtrmoon.common.BaseController;
import com.qtrmoon.examination.pojo.ExerAnswer;
import com.qtrmoon.examination.pojo.ExerTopic;
import com.qtrmoon.examination.serdao.IExaminationService;
import com.qtrmoon.manager.pojo.Person;
import com.qtrmoon.study.pojo.StudyStatus;
import com.qtrmoon.study.serdao.IStudyService;
import com.qtrmoon.sysmanage.SysConstant;
import com.qtrmoon.util.FileUpload;

/**
 * 试卷答案控制器
 */
@Controller
@RequestMapping("/examination/exerAnswer")
public class ExerAnswerController extends BaseController {
	
	@Autowired
	private IExaminationService examinationService; // 声明模块Service实例
	
	@Autowired
	private IStudyService studyService;		

	/**
	 * 查询方法，自带分页设置。
	 * @param exerAnswer
	 * 承载查询条件的Bean
	 * @param request
	 * 可用来获取非Bean内的参数
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schExerAnswer.action")
	public void schExerAnswer(ExerAnswer exerAnswer,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		noCache(response);// ajax设置无缓存
		exerAnswer.setCondition(""); // 防sql注入
		exerAnswer.assLike("remark", "answer");// 添加%
		List<ExerAnswer> list = examinationService.schExerAnswer(exerAnswer); // 调用Service查询数据
		/* 将数据Bean的集合转换为Json集合 */
		JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
		JSONObject obj; // 声明用于构造Bean的Json对象
		for (ExerAnswer u : list) { // 循环查询的数据集构造Json数据集
			obj = u.getJsonInDict(); // 将Bean转换为Json对象
			obj.put("_oper", u.getId()); // 为"操作列"设定主键值
			datas.add(obj); // 添加Json对象到Json集合
		}
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		JSONObject res = new JSONObject(); // 声明页面返回Json
		res.put("total", exerAnswer.getDatasize()); // 设置数据总记录数
		res.put("rows", datas); // 设置数据集
		response.getWriter().print(res.toJSONString()); // 输出到页面
	}

	/**
	 * 查询方法，自带分页设置。
	 * @param exerAnswer
	 * 承载查询条件的Bean
	 * @param request
	 * 可用来获取非Bean内的参数
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schExerAnswerByEtsId.action")
	public void schExerAnswerByEtsId(ExerAnswer exerAnswer,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		noCache(response);// ajax设置无缓存
		Person person = (Person) request.getSession(true).getAttribute(// 获取当前用户
				SysConstant.CURRENT_USER);// 获取当前用户
		JSONObject obj = new JSONObject(); // 声明用于构造Bean的Json对象
		if (person != null) {
			exerAnswer.setStudNumber(person.getNumber());// 设置学号
			List<ExerAnswer> list = examinationService// 调用Service查询数据
					.schExerAnswer(exerAnswer); // 调用Service查询数据
			/* 将数据Bean的集合转换为Json集合 */
			for (ExerAnswer u : list) { // 循环查询的数据集构造Json数据集
				obj = u.getJsonInDict(); // 将Bean转换为Json对象
				obj.put("_oper", u.getId()); // 为"操作列"设定主键值
			}
		}
		response.getWriter().print(obj.toJSONString()); // 输出到页面
	}

	/**
	 * 查询方法，自带分页设置。
	 * @param exerAnswer
	 * 承载查询条件的Bean
	 * @param request
	 * 可用来获取非Bean内的参数
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schExerAnswerByEtssId.action")
	public void schExerAnswerByEtssId(ExerAnswer exerAnswer,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		noCache(response);// ajax设置无缓存
		JSONObject obj = new JSONObject(); // 声明用于构造Bean的Json对象
		List<ExerAnswer> list = examinationService.schExerAnswerList(exerAnswer); // 调用Service查询数据
		/* 将数据Bean的集合转换为Json集合 */
		for (ExerAnswer u : list) { // 循环查询的数据集构造Json数据集
			obj = u.getJsonInDict(); // 将Bean转换为Json对象
			obj.put("_oper", u.getId()); // 为"操作列"设定主键值
		}
		response.getWriter().print(obj.toJSONString()); // 输出到页面
	}

	/**
	 * 预添加or预修改or查看方法
	 * @param exerAnswer
	 * 预添加时可传入一些初始值；预修改和查看时用来承载主键值
	 * @param toL
	 * 挂字典的字段是否需要转为字典Label值。true则进行转换
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/vieExerAnswer.action")
	public void vieExerAnswer(ExerAnswer exerAnswer, Boolean toL,
			HttpServletResponse response) throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (toL != null && toL) { // 查看页面加载数据时调用，返回的对象已进行字典转换
			if (exerAnswer.getId() != null && exerAnswer.getId() != 0) { // 检测主键非空
				exerAnswer = examinationService.schExerAnswerById(exerAnswer// 按主键查询数据
						.getId()); // 按主键查询数据
				response.getWriter().print(exerAnswer.getJsonInDict()); // 输出经过字典转换Json对象到查看页(vie_)
			}
		} else { // 修改页面加载数据时调用
			if (exerAnswer.getId() != null && exerAnswer.getId() != 0) { // 检查主键非空
				exerAnswer = examinationService.schExerAnswerById(exerAnswer// 按主键查询数据
						.getId()); // 按主键查询数据
				response.getWriter().print(exerAnswer.getJson()); // 输出Json对象到修改页(upd_)
			} else { // 添加页面加载数据时调用，可设置添加页面中一些属性的预设值。
				response.getWriter().print(new ExerAnswer().getJson()); // 输出Json对象到添加页(upd_)
			}
		}
	}

	/**
	 * 添加或修改的提交方法
	 * @param exerAnswer
	 * 数据表单Bean
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/updExerAnswer.action")
	public void updExerAnswer(ExerAnswer exerAnswer,
			HttpServletResponse response) throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (exerAnswer.getId() == null) { // 检测主键为空则
			examinationService.addExerAnswer(exerAnswer); // 添加数据
		} else { // 主键非空则
			examinationService.updExerAnswer(exerAnswer); // 修改数据
		}
	}

	/**
	 * 添加或修改的提交方法
	 * @param exerAnswer
	 * 数据表单Bean
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/updExerAnswerByEtsId.action")
	public void updExerAnswerByEtsId(ExerAnswer exerAnswer,
			HttpServletResponse response, HttpServletRequest request)
			throws IOException {
		noCache(response); // Ajax方法设定无缓存
		Person person = (Person) request.getSession().getAttribute(// 获取当前用户
				SysConstant.CURRENT_USER);// 获取当前用户
		if (person != null) {// 如果当前用户不为null 
			String number = person.getNumber();// 获取学号
			exerAnswer.setStudNumber(number);// 设置学号
			examinationService.updExerAnswerByEtsId(exerAnswer); // 修改数据
		}
	}
	
	/**
	 * 添加或修改的提交方法
	 * @param exerAnswer
	 * 数据表单Bean
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/updHasDoneByEtsId.action")
	public void updHasDoneByEtsId(ExerAnswer exerAnswer,
			HttpServletResponse response, HttpServletRequest request)
			throws IOException {
		noCache(response); // Ajax方法设定无缓存
		Person person = (Person) request.getSession().getAttribute(// 获取当前用户
				SysConstant.CURRENT_USER);// 获取当前用户
		if (person != null) {// 如果当前用户不为null 
			String number = person.getNumber();// 获取学号
			exerAnswer.setStudNumber(number);// 设置学号
			examinationService.updExerAnswerByEtsId(exerAnswer); // 修改数据
			Integer etid = exerAnswer.getEtid();
			ExerTopic exerTopic = examinationService// 查询结果集
					.schExerTopicById(etid);
			if (exerTopic != null) {
				exerTopic.setHasDone(1);
				examinationService.updExerTopic(exerTopic);
				
				StudyStatus stusta = new StudyStatus();
				stusta.setNumber(number);
				stusta.setBookId(Integer.valueOf(exerTopic.getBookId()));
				List<StudyStatus> stuss = studyService.schStudyStatus(stusta);
				if(stuss == null || stuss.size() == 0){
					stusta.setStatus("1");
					studyService.addStudyStatus(stusta);
				}
			}
			
		}
	}

	/**
	 * 添加或修改的提交方法
	 * @param exerAnswer
	 * 数据表单Bean
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/updExerAnswerByEtId.action")
	public void updExerAnswerByEtId(@RequestBody List<ExerAnswer> paramArr,
			HttpServletResponse response, HttpServletRequest request)
			throws IOException {
		noCache(response); // Ajax方法设定无缓存
		examinationService.updExerAnswerByEtId(paramArr); // 修改数据
	}

	
	
	@RequestMapping(value = "/updExerAnswerByEtIdd.action")
	public void updExerAnswerByEtIdd(@RequestBody List<ExerAnswer> paramArr,
			HttpServletResponse response, HttpServletRequest request)
			throws IOException {
		noCache(response); // Ajax方法设定无缓存
		examinationService.updExerAnswerByEtIdd(paramArr); // 修改数据
	}
	
	/**
	 * 删除方法
	 * @param ids
	 * 待删除数据的主键数组
	 * @param response
	 */
	@RequestMapping(value = "/delExerAnswer.action")
	public void delExerAnswer(@RequestParam(value = "ids[]") Integer[] ids,
			HttpServletResponse response) {
		noCache(response); // Ajax方法设定无缓存
		examinationService.delExerAnswer(ids); // 删除数据
	}

	/**
	 * 拼接录音文件
	 * @param filename
	 * @param request
	 * @param response
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value = "/appendUrl.action")
	public Map<String,Object> appendUrl(String eid,String etid,String esid,String studid, 
			HttpServletRequest request,HttpServletResponse response) {
		Map<String,Object> result = new HashMap<String,Object>();
		try {
			if( eid != null && etid != null && esid != null && studid != null &&
					!"".equals(eid) && !"".equals(etid) && !"".equals(esid) && !"".equals(studid)){
				String midPath = File.separator + eid +File.separator + studid +File.separator; 
				String fileName = etid + "@@" + esid + ".wav";
				String realPath = FilePath.getFilePath("stuanswerpath") + midPath + fileName;// 拼接文件路径
				File file = new File(realPath);
				if(file.exists()){
					// 缓存文件路径（页面显示用：使用录音播放软件时可拖拽）
					String webRoot = request.getSession().getServletContext().getRealPath("");
					String webPath= File.separator+"data"+File.separator+"voice"+File.separator
							+"answer"+File.separator + midPath;
					File newFile = new File(webRoot+webPath);
					if(!newFile.isDirectory()){
						newFile.mkdirs();// 创建文件夹
					}
					FileUpload.copyFile(realPath, webRoot+webPath+fileName);
					result.put("success", true);
					result.put("path", webPath+fileName);
				}else{
					result.put("success", false);
					result.put("msg", "文件不存在");
					result.put("path", "");// TODO 可设置一个默认录音的路径
				}
			}else{
				result.put("success", false);
				result.put("msg", "必要参数缺失");
				result.put("path", "");// TODO 可设置一个默认录音的路径
			}
		} catch (Exception e) {
			logger.error("获取学生答案录音异常");
			result.put("success", false);
			result.put("msg", "服务器异常");
			result.put("path", "");// TODO 可设置一个默认录音的路径
			e.printStackTrace();// 打印异常信息
		}finally{
			return result;
		}
	}

	/**
	 * 保存分数
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value="updScoreByTeacher.action")
	public Map<String,Object> updScoreByTeacher(@RequestBody List<ExerAnswer> paramArr){
		Map<String,Object> result = new HashMap<String,Object>();
		try{
			examinationService.updScoreByTeacher(paramArr);
			result.put("success", true);
		}catch(Exception e){
			e.printStackTrace();
			result.put("success", false);
			result.put("msg", "服务器异常");
		}finally{
			return result;
		}
	}
	
	/**
	 * JSP页面跳转
	 * @param page
	 * jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value = "/page.action")
	public String goPage(String page) { // 放过本Ctrl的页面，不合法的跳转到error页。
		if ("index".equals(page) || "exerAnswer_sch".equals(page)//判断页面
				|| "exerAnswer_vie".equals(page)//判断页面
				|| "exerAnswer_upd".equals(page)) {//判断页面
			return "/examination/" + page;// 跳转页面
		} else {
			return "/error";// 跳转错误页面
		}
	}
}
