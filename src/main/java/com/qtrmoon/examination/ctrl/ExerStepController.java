package com.qtrmoon.examination.ctrl;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.qtrmoon.book.util.FilePath;
import com.qtrmoon.common.BaseController;
import com.qtrmoon.examination.pojo.ExerAnswer;
import com.qtrmoon.examination.pojo.ExerStep;
import com.qtrmoon.examination.serdao.IExaminationService;
import com.qtrmoon.manager.pojo.Person;
import com.qtrmoon.sysmanage.SysConstant;

/**
 * 试卷步骤控制器
 */
@Controller
@RequestMapping("/examination/exerStep")
public class ExerStepController extends BaseController {
	@Autowired
	private IExaminationService examinationService; // 声明模块Service实例

	/**
	 * 查询方法，自带分页设置。
	 * @param exerStep
	 * 承载查询条件的Bean
	 * @param request
	 * 可用来获取非Bean内的参数
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schExerStep.action")
	public void schExerStep(ExerStep exerStep, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		noCache(response);// ajax设置无缓存
		exerStep.setCondition(""); // 防sql注入
		exerStep.assLike("step", "type", "title");// 拼接%
		List<ExerStep> list = examinationService.schExerStep(exerStep); // 调用Service查询数据
		/* 将数据Bean的集合转换为Json集合 */
		JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
		JSONObject obj; // 声明用于构造Bean的Json对象
		for (ExerStep u : list) { // 循环查询的数据集构造Json数据集
			obj = u.getJsonInDict(); // 将Bean转换为Json对象
			obj.put("_oper", u.getId()); // 为"操作列"设定主键值
			datas.add(obj); // 添加Json对象到Json集合
		}
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		JSONObject res = new JSONObject(); // 声明页面返回Json
		res.put("total", exerStep.getDatasize()); // 设置数据总记录数
		res.put("rows", datas); // 设置数据集
		response.getWriter().print(res.toJSONString()); // 输出到页面
	}

	/**
	 * 查询方法，自带分页设置。
	 * @param exerStep
	 * 承载查询条件的Bean
	 * @param request
	 * 可用来获取非Bean内的参数
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/schExerStepById.action")
	public void schExerStepById(Integer stepid, Model model,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		noCache(response);
		ExerStep exerStep = examinationService.schExerStepById(stepid); // 调用Service查询数据
		/* 将数据Bean的集合转换为Json集合 */
		JSONObject obj = exerStep.getJsonInDict();
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		response.getWriter().print(obj.toJSONString()); // 输出到页面
	}

	/**
	 * 预添加or预修改or查看方法
	 * @param exerStep
	 * 预添加时可传入一些初始值；预修改和查看时用来承载主键值
	 * @param toL
	 * 挂字典的字段是否需要转为字典Label值。true则进行转换
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/vieExerStep.action")
	public void vieExerStep(ExerStep exerStep, Boolean toL,
			HttpServletResponse response) throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (toL != null && toL) { // 查看页面加载数据时调用，返回的对象已进行字典转换
			if (exerStep.getId() != null && exerStep.getId() != 0) { // 检测主键非空
				exerStep = examinationService.schExerStepById(exerStep.getId()); // 按主键查询数据
				response.getWriter().print(exerStep.getJsonInDict()); // 输出经过字典转换Json对象到查看页(vie_)
			}
		} else { // 修改页面加载数据时调用
			if (exerStep.getId() != null && exerStep.getId() != 0) { // 检查主键非空
				exerStep = examinationService.schExerStepById(exerStep.getId()); // 按主键查询数据
				response.getWriter().print(exerStep.getJson()); // 输出Json对象到修改页(upd_)
			} else { // 添加页面加载数据时调用，可设置添加页面中一些属性的预设值。
				response.getWriter().print(new ExerStep().getJson()); // 输出Json对象到添加页(upd_)
			}
		}
	}

	/**
	 * 添加或修改的提交方法
	 * @param exerStep
	 * 数据表单Bean
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/updExerStep.action")
	public void updExerStep(ExerStep exerStep, HttpServletResponse response)
			throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (exerStep.getId() == null) { // 检测主键为空则
			examinationService.addExerStep(exerStep); // 添加数据
		} else { // 主键非空则
			examinationService.updExerStep(exerStep); // 修改数据
		}
	}

	/**
	 * 删除方法
	 * @param ids
	 * 待删除数据的主键数组
	 * @param response
	 */
	@RequestMapping(value = "/delExerStep.action")
	public void delExerStep(@RequestParam(value = "ids[]") Integer[] ids,
			HttpServletResponse response) {
		noCache(response); // Ajax方法设定无缓存
		examinationService.delExerStep(ids); // 删除数据
	}

	/**
	 * JSP页面跳转
	 * @param page
	 * jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value = "/page.action")
	public String goPage(String page) { // 放过本Ctrl的页面，不合法的跳转到error页。
		if ("index".equals(page) || "exerStep_sch".equals(page)// 判断跳转页面
				|| "exerStep_vie".equals(page) || "exerStep_upd".equals(page)) {// 判断跳转页面
			return "/examination/" + page;// 跳转页面
		} else {
			return "/error";// 跳转错误页面
		}
	}

	/**
	 * 上传录音文件
	 * @param file
	 * @param request
	 */
	@RequestMapping(value = "/uploadVoice.action")
	@ResponseBody
	public Map<String, Object> uploadVoice(
			@RequestParam(value = "audioData", required = false) CommonsMultipartFile file,
			HttpServletRequest request) {
		Map<String, Object> map = new HashMap<String, Object>();// 实例化返回
		String path = FilePath.getFilePath("stuanswerpath");// 获取配置文件参数
		String eid = request.getParameter("eid");// 设置试卷id
		Person person = (Person) request.getSession().getAttribute(
				SysConstant.CURRENT_USER);// 获取当前用户
		String studid = person.getNumber();// 获取学号
		path = path + File.separator + eid + File.separator + studid;// 拼接路径
		String etid = request.getParameter("etid");// 获取etid参数
		String esid = request.getParameter("esid");// 获取esid参数
		String fileName = ".wav";// 初始化文件名称
		fileName = etid + "@@" + esid + fileName;// 文件名称拼接
		File targetFile = new File(path, fileName);// 实例化文件对象
		if (!targetFile.exists()) {// 判断路径
			targetFile.mkdirs();// 创建路径
		}
		try {
			file.transferTo(targetFile);// 上传文件
		} catch (Exception e) {
			e.printStackTrace();// 打印异常信息
		}
		map.put("fileUrl", targetFile.getAbsolutePath());// 赋值fileUrl属性
		return map;// 返回结果集
	}

	/**
	 * 根据topicid查询step
	 * @param exerStep
	 * @param request
	 * @param response
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value = "/getStepByTopicId.action")
	public Map<String, Object> getStepByTopicId(ExerStep exerStep,
			HttpServletRequest request, HttpServletResponse response) {

		Map<String, Object> result = new HashMap<String, Object>();// 实例化返回结果集
		try {
			Person person = (Person) request.getSession(true).getAttribute(// 获取当前用户
					SysConstant.CURRENT_USER);// 获取当前用户
			List<ExerStep> list = examinationService.schExerStepList(exerStep);// 查询数据
			if (list != null && list.size() > 0) {// 判断集合是否为空
				if (person != null) {// 判断当前用户是否为空
					for(ExerStep step : list ){
						ExerAnswer exerAnswer = new ExerAnswer();// 
						exerAnswer.setEsid(step.getId());// 设置参数
						exerAnswer.setStudNumber(person.getNumber());// 设置参数
						List<ExerAnswer> anslist = examinationService// 查询数据
								.schExerAnswer(exerAnswer);// 查询数据
						if (anslist != null && anslist.size() > 0) {// 判断集合是否为空
							result.put("answerId", anslist.get(0).getId());// 设置参数
							step.setStandardAnswer(// 设置参数
									anslist.get(0).getAnswer());// 设置参数
						}
					}
				}
			}
			result.put("data", list);// 返回数据
			result.put("success", true);// 返回成功状态
			result.put("msg", "查询成功");// 返回提示信息
		} catch (Exception e) {
			e.printStackTrace();// 打印异常信息
			result.put("success", false);// 返回成功状态
			result.put("msg", "查询异常");// 返回提示信息
		} finally {
			return result;// 返回结果集
		}
	}

	/**
	 * 保存step
	 * @param exerStep
	 * @param answerId
	 * @param request
	 * @param response
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value = "/saveSxStepAndAnswer.action")
	public Map<String, Object> saveSxStepAndAnswer(@RequestBody List<ExerStep> exerSteps, Integer answerId,
			HttpServletRequest request, HttpServletResponse response) {

		Map<String, Object> result = new HashMap<String, Object>();// 初始化返回结果集
		try {
			Person person = (Person) request.getSession(true).getAttribute(// 获取当前用户
					SysConstant.CURRENT_USER);// 获取当前用户
			if (person != null) {// 判断当前用户是否为空
				List<ExerStep> steps = examinationService.saveSxStepAndAnswer(exerSteps,person.getNumber());
				result.put("success", true);
				result.put("data", steps);
			} else {
				result.put("success", false);// 返回成功状态
				result.put("msg", "请先登录");// 返回提示信息
			}
		} catch (Exception e) {
			e.printStackTrace();// 打印异常信息
			result.put("success", false);// 返回成功状态
			result.put("msg", "更新异常");// 返回提示信息
		} finally {
			return result;// 返回结果
		}
	}

	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value = "/delSxStepAndAnswer.action")
	public Map<String, Object> delSxStepAndAnswer(ExerStep exerStep,HttpServletRequest request){
		Map<String, Object> result = new HashMap<String, Object>();// 初始化返回结果集
		try {
			Person person = (Person) request.getSession(true).getAttribute(SysConstant.CURRENT_USER);// 获取当前用户
			if(exerStep != null && person != null && person.getNumber() != null){
				examinationService.delSxStepAndAnswer(exerStep,person.getNumber());
				result.put("success", true);
			}else{
				result.put("success", false);
				result.put("msg", "未登录或参数错误，请刷新后再试");
			}
		}catch(Exception e){
			e.printStackTrace();
			result.put("success", false);
			result.put("msg", "服务器异常");
		}finally{
			return result;
		}
	}
	
}
