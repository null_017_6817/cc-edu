package com.qtrmoon.examination.ctrl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qtrmoon.common.BaseController;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.dictionary.bsd.DictBean;
import com.qtrmoon.examination.constant.MarkStatusConstant;
import com.qtrmoon.examination.pojo.Exer;
import com.qtrmoon.examination.pojo.RExerStud;
import com.qtrmoon.examination.serdao.IExaminationService;

/**
 * 关系表控制器
 */
@Controller
@RequestMapping("/examination/rExerStud")
public class RExerStudController extends BaseController {

	@Autowired
	private IExaminationService examinationService; // 声明模块Service实例

	/**
	 * 查询方法，自带分页设置。
	 * 
	 * @param rExerStud
	 *            承载查询条件的Bean
	 * @param request
	 *            可用来获取非Bean内的参数
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schRExerStud.action")
	public void schRExerStud(RExerStud rExerStud, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		noCache(response);// ajax设置无缓存
		rExerStud.setCondition(""); // 防sql注入
		rExerStud.assLike("name", "markTeacher", "remark");// 拼接%
		List<RExerStud> list = examinationService.schRExerStud(rExerStud); // 调用Service查询数据
		/* 将数据Bean的集合转换为Json集合 */
		JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
		JSONObject obj; // 声明用于构造Bean的Json对象
		for (RExerStud u : list) { // 循环查询的数据集构造Json数据集
			obj = u.getJsonInDict(); // 将Bean转换为Json对象
			obj.put("_oper", u.getId()); // 为"操作列"设定主键值
			datas.add(obj); // 添加Json对象到Json集合
		}
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		JSONObject res = new JSONObject(); // 声明页面返回Json
		res.put("total", rExerStud.getDatasize()); // 设置数据总记录数
		res.put("rows", datas); // 设置数据集
		response.getWriter().print(res.toJSONString()); // 输出到页面
	}

	/**
	 * 查询方法，自带分页设置。
	 * 
	 * @param rExerStud
	 *            承载查询条件的Bean
	 * @param request
	 *            可用来获取非Bean内的参数
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schMarkRExerStud.action")
	public void schMarkRExerStud(RExerStud rExerStud, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		noCache(response);// ajax设置无缓存
		rExerStud.setCondition(""); // 防sql注入
		rExerStud.assLike("name", "markTeacher", "remark", "studName");// 拼接%

		if (MarkStatusConstant.MARK_NO.equals(rExerStud.getMarkStatus())) {
			rExerStud.setCondition("AND (MARK_PROGRESS is null OR MARK_PROGRESS = 0) ");
		}
		if (MarkStatusConstant.MARK_ING.equals(rExerStud.getMarkStatus())) {
			rExerStud.setCondition("AND (MARK_PROGRESS > 0  AND MARK_PROGRESS < 100 )");
		}
		if (MarkStatusConstant.MARK_FINISH.equals(rExerStud.getMarkStatus())) {
			rExerStud.setCondition("AND MARK_PROGRESS = 100 ");
		}

		List<RExerStud> list = examinationService.schMarkRExerStud(rExerStud); // 调用Service查询数据
		/* 将数据Bean的集合转换为Json集合 */
		JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
		JSONObject obj; // 声明用于构造Bean的Json对象
		for (RExerStud u : list) { // 循环查询的数据集构造Json数据集
			obj = u.getJsonInDict(); // 将Bean转换为Json对象
			obj.put("_oper", u.getId()); // 为"操作列"设定主键值
			datas.add(obj); // 添加Json对象到Json集合
		}
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		JSONObject res = new JSONObject(); // 声明页面返回Json
		res.put("total", rExerStud.getDatasize()); // 设置数据总记录数
		res.put("rows", datas); // 设置数据集
		response.getWriter().print(res.toJSONString()); // 输出到页面
	}

	/**
	 * 预添加or预修改or查看方法
	 * 
	 * @param rExerStud
	 *            预添加时可传入一些初始值；预修改和查看时用来承载主键值
	 * @param toL
	 *            挂字典的字段是否需要转为字典Label值。true则进行转换
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/vieRExerStud.action")
	public void vieRExerStud(RExerStud rExerStud, Boolean toL, HttpServletResponse response) throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (toL != null && toL) { // 查看页面加载数据时调用，返回的对象已进行字典转换
			if (rExerStud.getId() != null && rExerStud.getId() != 0) { // 检测主键非空
				rExerStud = examinationService.schRExerStudById(rExerStud.getId()); // 按主键查询数据
				response.getWriter().print(rExerStud.getJsonInDict()); // 输出经过字典转换Json对象到查看页(vie_)
			}
		} else { // 修改页面加载数据时调用
			if (rExerStud.getId() != null && rExerStud.getId() != 0) { // 检查主键非空
				rExerStud = examinationService.schRExerStudById(rExerStud.getId()); // 按主键查询数据
				response.getWriter().print(rExerStud.getJson()); // 输出Json对象到修改页(upd_)
			} else { // 添加页面加载数据时调用，可设置添加页面中一些属性的预设值。
				response.getWriter().print(new RExerStud().getJson()); // 输出Json对象到添加页(upd_)
			}
		}
	}

	/**
	 * 添加或修改的提交方法
	 * 
	 * @param rExerStud
	 *            数据表单Bean
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/updRExerStud.action")
	public void updRExerStud(RExerStud rExerStud, HttpServletResponse response) throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (rExerStud.getId() == null) { // 检测主键为空则
			examinationService.addRExerStud(rExerStud); // 添加数据
		} else { // 主键非空则
			examinationService.updRExerStud(rExerStud); // 修改数据
		}
	}

	/**
	 * 保存学生练习关系
	 * 
	 * @param paramList
	 * @return
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping(value = "/saveGx.action")
	public void saveGx(String stuIds, String eid, String classId, HttpServletResponse response) throws IOException {
		noCache(response); // Ajax方法设定无缓存
		JSONObject obj = new JSONObject();// 实例化json对象
		try {
			examinationService.addGx(stuIds, eid, classId);// 查询数据
			obj.put("success", true);// 返回成功状态
			obj.put("msg", "保存成功");// 返回提示信息
		} catch (Exception e) {
			e.printStackTrace();// 打印异常信息
			obj.put("success", true);// 返回成功状态
			obj.put("msg", "保存异常");// 返回提示信息
		} finally {
			response.getWriter().print(obj.toJSONString());// 输出json 数据
		}
	}

	/**
	 * 删除方法
	 * 
	 * @param ids
	 *            待删除数据的主键数组
	 * @param response
	 */
	@RequestMapping(value = "/delRExerStud.action")
	public void delRExerStud(@RequestParam(value = "ids[]") Integer[] ids, HttpServletResponse response) {
		noCache(response); // Ajax方法设定无缓存
		examinationService.delRExerStud(ids); // 删除数据
	}

	/**
	 * 获取试卷及学生列表
	 * 
	 * @param exer
	 * @param request
	 * @return
	 */
	@SuppressWarnings({ "finally", "unchecked" })
	@ResponseBody
	@RequestMapping(value = "getExerAndStud.action")
	public Map<String, Object> getExerAndStud(Exer exer, HttpServletRequest request) {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			if (exer != null && exer.getId() != null) {
				Map<String, Object> teachersMap = null;
				// 查询组装试卷结果集
				Exer exerResult = examinationService.schExerById(exer.getId());
				if (exerResult != null) {
					String teachers = exerResult.getTeachers();
					teachersMap = this.strTransTeacehrMap(teachers);
				} else {
					exerResult = exer;
				}

				// 查询已分配学生结果集
				RExerStud rExerStud = new RExerStud();
				rExerStud.setEid(exer.getId());
				List<RExerStud> rs = examinationService.schRExerStudList(rExerStud);
				// 获取所有已分配学生的班级（班级去重）
				Map<String, Object> classMap = new HashMap<String, Object>();
				JSONArray rsArray = new JSONArray();
				if (rs != null) {
					for (RExerStud reStu : rs) {
						if (reStu != null) {
							if (reStu.getClassid() != null && !"".equals(reStu.getClassid())) {
								// 把班级的号放到map中（去重效果）
								classMap.put(reStu.getClassid(), reStu.getClassid());
							}
							if (reStu.getStudid() != null) {
								JSONObject obj = new JSONObject();
								obj.put("studNumber", reStu.getStudid());// 学号
								obj.put("studName", DictBuffer.getLabel("ZD_STUD", reStu.getStudid().toString()));// 姓名
								obj.put("studCnumber", reStu.getClassid());// 班级
								rsArray.add(obj);
							}
						}
					}
				} else {
					rs = new ArrayList<RExerStud>();
				}

				List<DictBean> clas = DictBuffer.getDict("CC_CLASSES");
				JSONObject classAll = new JSONObject();
				if (clas != null && clas.size() > 0) {
					for (DictBean cla : clas) {
						if (cla != null && cla.getId() != null && cla.getLabel() != null) {
							classAll.put(cla.getId(), cla.getLabel());

						}
					}
				}

				if (teachersMap == null) {
					teachersMap = new HashMap<String, Object>();
				}
				result.put("success", true);// 成功标识
				result.put("exer", exerResult.getJsonInDict());// 试卷集合
				result.put("teachersMap", teachersMap);// 老师集合
				result.put("classMap", classMap);// 班级集合
				result.put("rStus", rsArray);// 已分配学生集合
				result.put("classAll", classAll);// 全部班级集合
			} else {
				result.put("success", false);
				result.put("msg", "必要参数缺失，请刷新后再试");
			}
		} catch (Exception e) {
			result.put("success", false);
			result.put("msg", "服务器异常！");
			e.printStackTrace();
		} finally {
			return result;
		}
	}

	/**
	 * str 转 map（老师）
	 * 
	 * @param str
	 * @return
	 */
	private Map<String, Object> strTransTeacehrMap(String str) {
		Map<String, Object> result = new HashMap<String, Object>();
		if (str != null && !"".equals(str)) {
			String[] strArray = str.split(",");
			if (strArray != null) {
				for (String item : strArray) {
					if (item != null && !"".equals(item)) {
						result.put(item, DictBuffer.getLabel("ZD_TEACHER", item));
					}
				}
			}
		}
		return result;
	}

	/**
	 * 获取发卷和撤卷的学号集合
	 * 
	 * @param param
	 * @param eid
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value = "/getDiffStud.action")
	public Map<String, Object> getDiffStud(@RequestBody String[] param, String eid) {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<String> addList = new ArrayList<String>();
			List<String> delList = new ArrayList<String>();
			List<String> cache = new ArrayList<String>();
			if (param != null && eid != null && !"".equals(eid)) {
				// 数据库中现存的数据
				RExerStud rExerStud = new RExerStud();
				rExerStud.setEid(Integer.valueOf(eid));
				List<RExerStud> rsDB = examinationService.schRExerStudList(rExerStud);
				if (rsDB != null) {
					for (RExerStud re : rsDB) {
						if (re != null && re.getStudid() != null && !"".equals(re.getStudid())) {
							delList.add(re.getStudid());
						}
					}
				}
				// 页面端传递的学号集合参数
				ArrayList<String> arrayList = new ArrayList<String>(Arrays.asList(param));
				addList.addAll(arrayList);

				// 去除掉页面端和数据库中共有的数据
				cache.addAll(addList);
				addList.removeAll(delList);
				delList.removeAll(cache);

				result.put("success", true);
				result.put("addList", addList);
				result.put("delList", delList);
			} else {
				result.put("success", false);
				result.put("msg", "必要参数缺失，请刷新后再试！");
			}
		} catch (Exception e) {
			result.put("success", false);
			result.put("msg", "服务器异常");
			e.printStackTrace();
		} finally {
			return result;
		}
	}

	
	
	
	
	/**
	 * 发卷和撤卷操作
	 * 
	 * @param addStr
	 * @param delStr
	 * @param eid
	 * @param teachers
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value = "/addAndRemove.action")
	public Map<String, Object> addAndRemove(String addStr, String delStr, String eid, String teachers) {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			if (addStr != null && delStr != null
					&& eid != null) {
				examinationService.updAddAndDelRExer(addStr, delStr, eid, teachers);
				result.put("success", true);

				// 更新缓存
				DictBuffer.updCache("ZD_SJ");// 更新缓存
			} else {
				result.put("success", false);
				// result.put("msg", "必要参数缺失，请刷新后再试");
				result.put("msg", "阅卷老师和考试学生不允许为空！");
				logger.debug("发卷和撤卷操作失败，必要参数缺失,addStr:{}, delStr:{}, eid:{}", addStr, delStr, eid);
			}
		} catch (Exception e) {
			logger.error("发卷和撤卷操作异常");
			e.printStackTrace();
		} finally {
			return result;
		}
	}

	/**
	 * 获取统计数据：参加考试的人数，已完成评分的人数，未完成评分的人数
	 * 
	 * @param rExerStud
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value = "getCountData.action")
	public Map<String, Object> getCountData(RExerStud rExerStud) {
		Map<String, Object> result = new HashMap<String, Object>();
		Integer totalNum = 0;// 初始化参加考试的总人数
		Integer finishNum = 0;// 初始化已结束评分的人数
		Integer ingNum = 0;// 初始化未结束评分的人数
		try {
			if (rExerStud != null && rExerStud.getEid() != null) {
				RExerStud param = new RExerStud();
				Exer exer = examinationService.schExerById(rExerStud.getEid());
				result.put("exer", exer);
				param.setEid(rExerStud.getEid());
				List<RExerStud> rs = examinationService.schRExerStudList(param);
				if (rs != null && rs.size() > 0) {
					for (int i = 0; i < rs.size(); i++) {
						RExerStud item = rs.get(i);
						if (item != null) {
							totalNum++;// 总数加一
							if (item.getMarkProgress() != null && item.getMarkProgress() == 100) {
								finishNum++;// 已阅卷完成加一
							} else {
								ingNum++;// 正在阅卷加一
							}
						}
					}
				}
			}
			result.put("success", true);
			result.put("total", totalNum);
			result.put("finish", finishNum);
			result.put("ing", ingNum);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
			result.put("msg", "服务器异常");
		} finally {
			return result;
		}
	}

	/**
	 * JSP页面跳转
	 * 
	 * @param page
	 *            jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value = "/page.action")
	public String goPage(String page) { // 放过本Ctrl的页面，不合法的跳转到error页。
		if ("index".equals(page) || "rExerStud_sch".equals(page)// 判断跳转页面
				|| "rExerStud_vie".equals(page) || "rExerStud_upd".equals(page)) {// 判断跳转页面
			return "/examination/" + page;// 跳转页面
		} else {
			return "/error";// 跳转错误页面
		}
	}
	
	
	/**
	 * 批量发卷操作
	 * 
	 * @param param
	 * @param eid
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value = "/batchGetDiffStud.action")
	public Map<String, Object> batchGetDiffStud(@RequestBody String[] param, String eid,String teachers) {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			if(eid != null && !"".equals(eid)){
				String[] idArr = eid.split(",");
				if(idArr != null && idArr.length > 0 ){
					for(String id : idArr){
						if(id != null && !"".equals(id) ){
							List<String> addList = new ArrayList<String>();
							List<String> delList = new ArrayList<String>();
							List<String> cache = new ArrayList<String>();
							if (param != null && eid != null && !"".equals(eid)) {
								// 数据库中现存的数据
								RExerStud rExerStud = new RExerStud();
								rExerStud.setEid(Integer.valueOf(id));
								List<RExerStud> rsDB = examinationService.schRExerStudList(rExerStud);
								if (rsDB != null) {
									for (RExerStud re : rsDB) {
										if (re != null && re.getStudid() != null && !"".equals(re.getStudid())) {
											delList.add(re.getStudid());
										}
									}
								}
								// 页面端传递的学号集合参数
								ArrayList<String> arrayList = new ArrayList<String>(Arrays.asList(param));
								addList.addAll(arrayList);

								
								// 去除掉页面端和数据库中共有的数据
								cache.addAll(addList);
								addList.removeAll(delList);
								delList.removeAll(cache);
								
								String addListStr = "";
								if(addList != null && addList.size() > 0){
									for( String add : addList ){
										if(add != null && !"".equals(add)){
											if( !"".equals(addListStr) ){
												addListStr += ","+add;
											}else{
												addListStr = add;
											}
										}
									}
								}
								
								String delListStr = "";
								if(delList != null && delList.size() > 0){
									for( String del : delList ){
										if(del != null && !"".equals(del)){
											if( !"".equals(delListStr) ){
												delListStr += ","+del;
											}else{
												delListStr = del;
											}
										}
									}
								}
								batchAddAndRemove(addListStr,delListStr, id,teachers);
							}
						}
					}
				}
			
				result.put("success", true);
				result.put("msg", "操作成功");
			} else {
				result.put("success", false);
				result.put("msg", "必要参数缺失，请刷新后再试！");
			}
		} catch (Exception e) {
			result.put("success", false);
			result.put("msg", "服务器异常");
			e.printStackTrace();
		} finally {
			return result;
		}
	}
	
	
	@SuppressWarnings({ "finally", "unchecked" })
	@ResponseBody
	@RequestMapping(value = "batchGetExerAndStud.action")
	public Map<String, Object> batchGetExerAndStud(Exer exer, String ids ,HttpServletRequest request) {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String exerName = "";
			if( ids != null && !"".equals(ids) ){
				String[] idArr = ids.split(",");
				for(String idStr : idArr ){
					if(idStr != null && !"".equals(idStr)){
						// 查询组装试卷结果集
						Exer ex = examinationService.schExerById(Integer.valueOf(idStr));
						if(ex != null && ex.getName() != null && !"".equals(ex.getName())){
							if( !"".equals(exerName) ){
								exerName = exerName + "," + ex.getName();
							}else{
								exerName = ex.getName();
							}
						}
					}
				}
			}
			
			List<DictBean> clas = DictBuffer.getDict("CC_CLASSES");
			JSONObject classAll = new JSONObject();
			if (clas != null && clas.size() > 0) {
				for (DictBean cla : clas) {
					if (cla != null && cla.getId() != null && cla.getLabel() != null) {
						classAll.put(cla.getId(), cla.getLabel());

					}
				}
			}
			result.put("success", true);// 成功标识
			result.put("exerName", exerName);// 试卷集合
			result.put("classAll", classAll);// 全部班级集合
		} catch (Exception e) {
			result.put("success", false);
			result.put("msg", "服务器异常！");
			e.printStackTrace();
		} finally {
			return result;
		}
	}
	
	
	
	/**
	 * 发卷和撤卷操作
	 * 
	 * @param addStr
	 * @param delStr
	 * @param eid
	 * @param teachers
	 * @return
	 */
	private Map<String, Object> batchAddAndRemove(String addStr, String delStr, String eid, String teachers) {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			if (addStr != null && delStr != null
					&& eid != null) {
				examinationService.updAddAndDelRExer(addStr, delStr, eid, teachers);
				result.put("success", true);

				// 更新缓存
				DictBuffer.updCache("ZD_SJ");// 更新缓存
			} else {
				result.put("success", false);
				// result.put("msg", "必要参数缺失，请刷新后再试");
				result.put("msg", "阅卷老师和考试学生不允许为空！");
				logger.debug("发卷和撤卷操作失败，必要参数缺失,addStr:{}, delStr:{}, eid:{}", addStr, delStr, eid);
			}
		} catch (Exception e) {
			logger.error("发卷和撤卷操作异常");
			e.printStackTrace();
		} finally {
			return result;
		}
	}
	
}
