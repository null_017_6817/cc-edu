package com.qtrmoon.examination.ctrl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qtrmoon.common.BaseController;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.dictionary.bsd.DictBean;
import com.qtrmoon.examination.pojo.ExamCategory;
import com.qtrmoon.examination.pojo.RExerStud;
import com.qtrmoon.examination.serdao.IExaminationService;

@Controller
@RequestMapping("/show")
public class DataDisplayController extends BaseController{
	@Autowired
	private IExaminationService examinationService;								//声明模块Service实例

	/**
	 * 个人完成情况查询方法
	 * @param eid 试卷id
	 * @param classid 班级id
	 * @param wcqk 完成情况
	 * @param request 可用来获取非Bean内的参数
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/schPersonalCompletion.action")
	public void schPersonalCompletion(Integer eid,Integer classid,String  wcqk,HttpServletRequest request,HttpServletResponse response) throws IOException{
		noCache(response);
		List<Map<String,Object>> list = examinationService.schPersonalCompletion(eid, classid, wcqk);//调用Service查询数据
		if(list.size()>0){
			for (Map<String, Object> map : list) {
				map.put("stuName", DictBuffer.getLabel("ZD_STUD", map.get("studid").toString())); //结果集添加学生姓名
			}
		}
		JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
		datas.addAll(list);
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		JSONObject res = new JSONObject(); // 声明页面返回Json
		res.put("total", list.size()); // 设置数据总记录数
		res.put("rows", datas); // 设置数据集
		response.getWriter().print(res.toJSONString()); // 输出到页面
	}
	
	/**
	 * 任务统计查询人数方法
	 * @param request 可用来获取非Bean内的参数
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value="/schTaskStatistics.action")
	public void schTaskStatistics(Integer eid,Integer classid,HttpServletRequest request,HttpServletResponse response) throws IOException{
		noCache(response);
		List<Map<String,Object>> list = examinationService.schTaskStatistics(eid,classid);		//调用Service查询数据
		StringBuilder json = new StringBuilder();
		StringBuilder legendStr = new StringBuilder();
		StringBuilder dataStr = new StringBuilder();
		json.append("{");
		json.append("\"title\":\"任务统计\",");
		json.append("\"legend\":").append("[");
		int i = 0;
		for(Map<String,Object> m:list) {
			if(i != 0) {
				dataStr.append(",");
				legendStr.append(",");
			}
			legendStr.append("\"").append(m.get("name")).append("\"");
			dataStr.append("{").append("\"name\"").append(":").append("\"").append(m.get("name")).append("\"").append(",");
			if(i==0){
				dataStr.append("\"value\"").append(":").append("\"").append(m.get("value")).append("\"").append(",\"itemStyle\": {\"normal\":{\"color\":'#47b2e1'}}}");
			}else{
				dataStr.append("\"value\"").append(":").append("\"").append(m.get("value")).append("\"").append(",\"itemStyle\": {\"normal\":{\"color\":'#fbd44c'}}}");
			}
			i++;
		}
		json.append(legendStr).append("],\"xAxis\":[],");
		json.append("\"series\":{").append("\"name\":\"任务统计\",\"data\":[");
		json.append(dataStr);
		json.append("]}}");
		response.setCharacterEncoding("UTF-8");
		if("jsonp".contentEquals(request.getParameter("getType"))) {
			response.getWriter().print(request.getParameter("jsonpCallback")+"("+json.toString()+")");		//输出到页面
		}else {
			response.getWriter().write(json.toString());					//输出到页面
		}
	}
	
	/***
	 * 返回班级列表
	 * @param request 可用来获取非Bean内的参数
	 * @return JSON数组
	 */
	@ResponseBody
	@RequestMapping(method=RequestMethod.GET,value="getClasses.action")
	public JSONArray getClasses(HttpServletRequest request) {
		List<DictBean> list = DictBuffer.getDictById("CC_CLASSES",
				DictBuffer.dummyRootId).getChildren("CC_CLASSES"); // 从字典中获取学期列表
		JSONArray xls = new JSONArray(); // 构造业务数据的JSON集合
		for (DictBean u : list) { // 循环查询的数据集构造Json数据集
			JSONObject obj = new JSONObject(); // 申明JSONObject对象
			obj.put("id", u.getId()); // JSONObject设置ID
			obj.put("pid", u.getPid()); // JSONObject设置pid
			obj.put("label", u.getLabel()); // JSONObject设置label
			xls.add(obj); // 添加Json对象到Json集合
		}
		return xls; // 返回结果
	}
	
	/**
	 * 根据班级号查询试卷方法
	 * @param classid 班级号
	 * @param request 可用来获取非Bean内的参数
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/schRExerStudBjExer.action")
	public void schRExerStudBjExer(String classid,String name,HttpServletResponse response) throws IOException{
		List<RExerStud> list = examinationService.schRExerStudBjExer(classid,name);// 查询数据
		
		List<Map<String,Object>> lists =new ArrayList<Map<String,Object>>();
		if(list.size()>0){
			for (RExerStud u : list) {
				Map<String,Object> exer=new HashMap<String,Object>();
				exer.put("eid", u.getEid());
				exer.put("name", u.getName());
				lists.add(exer);
			}
		}
		JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
		datas.addAll(lists);
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		JSONObject res = new JSONObject(); // 声明页面返回Json
		res.put("total", lists.size()); // 设置数据总记录数
		res.put("rows", datas); // 设置数据集
		response.getWriter().print(res.toJSONString()); // 输出到页面
	}
	
	/**
	 * 学习过程分析查询方法
	 * @param classid 班级id
	 * @param studid 学号
	 * @param request 可用来获取非Bean内的参数
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/schLearningProcess.action")
	public void schLearningProcess(Integer classid,Integer studid,HttpServletRequest request,HttpServletResponse response) throws IOException{
		noCache(response);
		List<Map<String,Object>> list = examinationService.schLearningProcess(classid, studid);//调用Service查询数据
		JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
		datas.addAll(list);
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		JSONObject res = new JSONObject(); // 声明页面返回Json
		res.put("total", list.size()); // 设置数据总记录数
		res.put("rows", datas); // 设置数据集
		response.getWriter().print(res.toJSONString()); // 输出到页面
	}
	
	/**
	 * 任务统计查询任务方法
	 * @param request 可用来获取非Bean内的参数
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value="/schOrderStatistics.action")
	public void schOrderStatistics(Integer classid,Integer studid,HttpServletRequest request,HttpServletResponse response) throws IOException{
		noCache(response);
		List<Map<String,Object>> list = examinationService.schOrderStatistics(classid,studid);		//调用Service查询数据
		StringBuilder json = new StringBuilder();
		StringBuilder legendStr = new StringBuilder();
		StringBuilder dataStr = new StringBuilder();
		json.append("{");
		json.append("\"title\":\"任务统计\",");
		json.append("\"legend\":").append("[");
		int i = 0;
		for(Map<String,Object> m:list) {
			if(i != 0) {
				dataStr.append(",");
				legendStr.append(",");
			}
			legendStr.append("\"").append(m.get("name")).append("\"");
			dataStr.append("{").append("\"name\"").append(":").append("\"").append(m.get("name")).append("\"").append(",");
			if(i==0){
				dataStr.append("\"value\"").append(":").append("\"").append(m.get("value")).append("\"").append(",\"itemStyle\": {\"normal\":{\"color\":'#47b2e1'}}}");
			}else{
				dataStr.append("\"value\"").append(":").append("\"").append(m.get("value")).append("\"").append(",\"itemStyle\": {\"normal\":{\"color\":'#fbd44c'}}}");
			}
			i++;
		}
		json.append(legendStr).append("],\"xAxis\":[],");
		json.append("\"series\":{").append("\"name\":\"任务统计\",\"data\":[");
		json.append(dataStr);
		json.append("]}}");
		response.setCharacterEncoding("UTF-8");
		if("jsonp".contentEquals(request.getParameter("getType"))) {
			response.getWriter().print(request.getParameter("jsonpCallback")+"("+json.toString()+")");		//输出到页面
		}else {
			response.getWriter().write(json.toString());					//输出到页面
		}
	}
	
	/***
	 * 返回学生列表
	 * @param request 可用来获取非Bean内的参数
	 * @return JSON数组
	 */
	@ResponseBody
	@RequestMapping(method=RequestMethod.GET,value="getStuList.action")
	public JSONArray getStuList(String classid,HttpServletRequest request) {
		List<DictBean> list = DictBuffer.getDictOneLayer("ZD_STUD",classid); // 从字典中获取学期列表
		JSONArray xls = new JSONArray(); // 构造业务数据的JSON集合
		for (DictBean u : list) { // 循环查询的数据集构造Json数据集
			JSONObject obj = new JSONObject(); // 申明JSONObject对象
			obj.put("id", u.getId()); // JSONObject设置ID
			obj.put("pid", u.getPid()); // JSONObject设置pid
			obj.put("label", u.getLabel()); // JSONObject设置label
			xls.add(obj); // 添加Json对象到Json集合
		}
		return xls; // 返回结果
	}
	
	/**
	 * 班级完成情况查询方法
	 * @param eid 试卷id
	 * @param request 可用来获取非Bean内的参数
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/schClassCompletion.action")
	public void schClassCompletion(Integer eid,HttpServletRequest request,HttpServletResponse response) throws IOException{
		noCache(response);
		List<Map<String,Object>> list = examinationService.schClassCompletion(eid);//调用Service查询数据
		List<Map<String,Object>> lists =new ArrayList<Map<String,Object>>(); //创建新的map集合
		if(list.size()>0){
			Set<java.lang.String> set = new  HashSet<java.lang.String>(); //创建新的set
			for (Map<String, Object> map : list) { //遍历集合
				Map<String,Object> bjwcqk=new HashMap<String, Object>(); //创建新的map对象
				if(set.add(map.get("classid").toString())){	//set判断重复，班级不重复的情况下
					bjwcqk.put("bjbh", map.get("classid"));	//添加班级编号
					bjwcqk.put("bjmc", DictBuffer.getLabel("CC_CLASSES", map.get("classid").toString()));//添加班级名称
					bjwcqk.put("cjrs", map.get("zrs")); //添加参加人数
					bjwcqk.put("pjwcl", map.get("pjwcl")); //添加平均完成率
					if("已完成".equals(map.get("wcqk"))){ //如果是已完成的
						bjwcqk.put("wcrs", map.get("rs")); //添加完成人数
						bjwcqk.put("wwcrs", 0); //添加未完成人数0 （查询结果中可能存在所有人都完成，没有未完成人数的信息，先给个默认值）
					}else{	//未完成的
						bjwcqk.put("wcrs", 0); //添加完成人数0 （查询结果中可能存在所有人都未完成，没有完成人数的信息，先给个默认值）
						bjwcqk.put("wwcrs", map.get("rs")); //添加未完成人数
					}
					lists.add(bjwcqk);
				}else{	//班级重复的情况
					for (Map<String, Object> m : lists) {
						if(map.get("classid").equals(m.get("bjbh"))){
							if("已完成".equals(map.get("wcqk"))){	//由于查询出的记录重复只存在已完成和未完成的，如果之前是未完成的，这里就会是已完成的
								m.put("wcrs", map.get("rs")); //覆盖已完成人数的数值
							}else{	//之前是完成的，这里是未完成的
								m.put("wwcrs", map.get("rs"));//覆盖未完成人数的数值
							}
						}
					}
				}
			}
		}
		JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
		datas.addAll(lists);
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		JSONObject res = new JSONObject(); // 声明页面返回Json
		res.put("total", list.size()); // 设置数据总记录数
		res.put("rows", datas); // 设置数据集
		response.getWriter().print(res.toJSONString()); // 输出到页面
	}
	
	/***
	 * 返回试卷列表
	 * @param request 可用来获取非Bean内的参数
	 * @return JSON数组
	 */
	@ResponseBody
	@RequestMapping(method=RequestMethod.GET,value="getTestPaperList.action")
	public JSONArray getTestPaperList(String kw,HttpServletRequest request) {
		List<DictBean> list = DictBuffer.getDictById("ZD_SJ",
				DictBuffer.dummyRootId).getChildren("ZD_SJ"); // 从字典中获取学期列表
		JSONArray xls = new JSONArray(); // 构造业务数据的JSON集合
		if("".equals(kw)||kw==null){
			for (DictBean u : list) { // 循环查询的数据集构造Json数据集
				JSONObject obj = new JSONObject(); // 申明JSONObject对象
				obj.put("id", u.getId()); // JSONObject设置ID
				obj.put("pid", u.getPid()); // JSONObject设置pid
				obj.put("label", u.getLabel()); // JSONObject设置label
				xls.add(obj); // 添加Json对象到Json集合
			}
		}else{
			for (DictBean u : list) { // 循环查询的数据集构造Json数据集
				if(u.getLabel().contains(kw)){
					JSONObject obj = new JSONObject(); // 申明JSONObject对象
					obj.put("id", u.getId()); // JSONObject设置ID
					obj.put("pid", u.getPid()); // JSONObject设置pid
					obj.put("label", u.getLabel()); // JSONObject设置label
					xls.add(obj); // 添加Json对象到Json集合
				}
			}
		}
		
		return xls; // 返回结果
	}
}
