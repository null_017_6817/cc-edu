package com.qtrmoon.examination.ctrl;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.dom4j.DocumentException;
import org.jdom.JDOMException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qtrmoon.common.BaseController;
import com.qtrmoon.examination.pojo.Exer;
import com.qtrmoon.examination.pojo.ExerAnswer;
import com.qtrmoon.examination.pojo.ExerTopic;
import com.qtrmoon.examination.pojo.StepInfo;
import com.qtrmoon.examination.serdao.IExaminationService;
import com.qtrmoon.examination.util.TopicTypeTransUtil;
import com.qtrmoon.manager.constant.YesOrNoConstant;
import com.qtrmoon.manager.pojo.BankTopic;
import com.qtrmoon.manager.pojo.Person;
import com.qtrmoon.manager.pojo.Stud;
import com.qtrmoon.manager.serdao.IManagerService;
import com.qtrmoon.sysmanage.SysConstant;

import freemarker.template.TemplateException;

/**
 * 试卷题目控制器
 */
@Controller
@RequestMapping("/examination/exerTopic")
public class ExerTopicController extends BaseController {
	@Autowired
	private IExaminationService examinationService; // 声明模块Service实例
	@Autowired
	private IManagerService managerService; // 声明模块Service实例

	/**
	 * 查询方法，自带分页设置。
	 * @param exerTopic
	 * 承载查询条件的Bean
	 * @param request
	 * 可用来获取非Bean内的参数
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schExerTopic.action")
	public void schExerTopic(ExerTopic exerTopic, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		noCache(response);// ajax设置无缓存
		exerTopic.setCondition(""); // 防sql注入
		exerTopic.assLike("remark", "title", "level", "profession",// 拼接%
				"difficulty", "author", "beginStepId", "docATitle",// 拼接%
				"docBTitle", "docCTitle", "docDTitle", "docETitle");// 拼接%
		List<ExerTopic> list = examinationService.schExerTopic(exerTopic); // 调用Service查询数据
		/* 将数据Bean的集合转换为Json集合 */
		JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
		JSONObject obj; // 声明用于构造Bean的Json对象
		for (ExerTopic u : list) { // 循环查询的数据集构造Json数据集
			obj = u.getJsonInDict(); // 将Bean转换为Json对象
			obj.put("_oper", u.getId()); // 为"操作列"设定主键值
			datas.add(obj); // 添加Json对象到Json集合
		}
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		JSONObject res = new JSONObject(); // 声明页面返回Json
		res.put("total", exerTopic.getDatasize()); // 设置数据总记录数
		res.put("rows", datas); // 设置数据集
		response.getWriter().print(res.toJSONString()); // 输出到页面
	}

	/**
	 * 预添加or预修改or查看方法
	 * @param exerTopic
	 * 预添加时可传入一些初始值；预修改和查看时用来承载主键值
	 * @param toL
	 * 挂字典的字段是否需要转为字典Label值。true则进行转换
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/vieExerTopic.action")
	public void vieExerTopic(ExerTopic exerTopic, Boolean toL,
			HttpServletResponse response) throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (toL != null && toL) { // 查看页面加载数据时调用，返回的对象已进行字典转换
			if (exerTopic.getId() != null && exerTopic.getId() != 0) { // 检测主键非空
				exerTopic = examinationService.schExerTopicById(exerTopic
						.getId()); // 按主键查询数据
				response.getWriter().print(exerTopic.getJsonInDict()); // 输出经过字典转换Json对象到查看页(vie_)
			}
		} else { // 修改页面加载数据时调用
			if (exerTopic.getId() != null && exerTopic.getId() != 0) { // 检查主键非空
				exerTopic = examinationService.schExerTopicById(exerTopic// 按主键查询数据
						.getId()); // 按主键查询数据
				response.getWriter().print(exerTopic.getJson()); // 输出Json对象到修改页(upd_)
			} else { // 添加页面加载数据时调用，可设置添加页面中一些属性的预设值。
				response.getWriter().print(new ExerTopic().getJson()); // 输出Json对象到添加页(upd_)
			}
		}
	}

	/**
	 * 添加或修改的提交方法
	 * @param exerTopic
	 * 数据表单Bean
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/updExerTopic.action")
	public void updExerTopic(ExerTopic exerTopic, HttpServletResponse response)
			throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (exerTopic.getId() == null) { // 检测主键为空则
			examinationService.addExerTopic(exerTopic); // 添加数据
		} else { // 主键非空则
			examinationService.updExerTopic(exerTopic); // 修改数据
		}
	}

	/**
	 * 复制数据（由bank_topic复制到exer_topic ）
	 * @param ids
	 * @param eid
	 * @param response
	 * @throws IOException
	 * @throws JDOMException
	 * @throws TemplateException
	 * @throws DocumentException
	 * @throws ParseException
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 */
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping(value = "/copyTopic.action")
	public void copyTopic(String ids, String eid, HttpServletResponse response,
			HttpServletRequest request) throws Exception {
		noCache(response); // Ajax方法设定无缓存
		JSONObject result = new JSONObject();// 初始化json对象
		try {
			if (ids != null) {// 判断参数是否为空
				String[] idArg = ids.split(",");// 根据逗号拆分字符串
				String inParam = "";// 初始化参数
				for (int i = 0; i < idArg.length; i++) {// 循环
					if (idArg[i] != null && !"".equals(idArg[i])) {// 判断必要参数是否为空
						if (!"".equals(inParam)) {// 判断必要参数是否为空
							inParam = inParam + ",'" + idArg[i] + "'";// 拼接参数
						} else {
							inParam = "'" + idArg[i] + "'";// 拼接参数
						}
					}
				}
				BankTopic bankTopic = new BankTopic();// 实例化BankTopic对象
				bankTopic.setCondition(" and topic_id in (" + inParam + ")");// 设置动态sql
				List<BankTopic> bankTopicList = managerService// 查询数据
						.schBankTopicList(bankTopic);// 查询数据
				examinationService.updateToCopy(bankTopicList,// 调用service方法
						Integer.valueOf(eid), request, "");// 调用service方法
				result.put("success", true);// 设置返回成功状态
				result.put("msg", "操作成功");// 设置返回提示信息
			}
		} catch (IOException e) {
			e.printStackTrace();// 打印异常信息
			result.put("success", false);// 设置返回成功状态
			result.put("msg", "操作异常");// 设置返回提示信息
		} finally {
			response.getWriter().print(result.toJSONString());// 返回json数据
		}
	}

	/**
	 * 删除方法
	 * @param ids
	 * 待删除数据的主键数组
	 * @param response
	 */
	@RequestMapping(value = "/delExerTopic.action")
	public void delExerTopic(@RequestParam(value = "ids[]") Integer[] ids,
			HttpServletResponse response) {
		noCache(response); // Ajax方法设定无缓存
		examinationService.delExerTopic(ids); // 删除数据
	}

	/**
	 * 删除练习时，同时删除习题和步骤
	 * @param ids
	 * @param response
	 * @throws IOException 
	 */
	@ResponseBody
	@RequestMapping(value = "/delExerTopicAndStep.action")
	public void delExerTopicAndStep(
			@RequestParam(value = "ids[]") Integer[] ids,
			HttpServletResponse response) throws IOException {
		noCache(response); // Ajax方法设定无缓存
		examinationService.delExerTopicAndStep(ids); // 删除数据
		JSONObject result = new JSONObject();// 初始化json对象
		result.put("success", true);
		response.getWriter().print(result);// 返回json数据
	}
	
	/**
	 * 删除练习时，同时删除习题和步骤
	 * @param ids
	 * @param response
	 * @throws IOException 
	 */
	@ResponseBody
	@RequestMapping(value = "/updBookIdByIds.action")
	public void updBookIdByIds(
			@RequestParam(value = "ids[]") Integer[] ids,String bookId,
			HttpServletResponse response) throws IOException {
		noCache(response); // Ajax方法设定无缓存
		Integer eid = null;
		if( ids != null ){
			for( Integer id : ids  ){
				if(id != null){
					ExerTopic topic = examinationService.schExerTopicById(id);
					if(topic != null){
						if(topic.getEid() != null){
							eid = topic.getEid();
						}
						
						topic.setBookId(bookId);
						examinationService.updExerTopic(topic);
					}
				}
			}
			if(eid != null){
				Exer exer = examinationService.schExerById(eid);
				if( exer != null ){
					exer.setIsSave("1");
					examinationService.updExer(exer);
				}
			}
		}
		JSONObject result = new JSONObject();// 初始化json对象
		result.put("success", true);
		response.getWriter().print(result);// 返回json数据
	}
	

	/**
	 * JSP页面跳转
	 * @param page
	 * jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value = "/page.action")
	public String goPage(String page, Model model, Integer id, Integer eid,
			HttpServletRequest request) { // 放过本Ctrl的页面，不合法的跳转到error页。
		Person person = (Person) request.getSession(true).getAttribute(
				SysConstant.CURRENT_USER);// 获取当前用户
		if (person != null) {
			model.addAttribute("person", person);// 设置参数到页面
			model.addAttribute("state", person.getState());// 设置参数到页面
			model.addAttribute("agentId", person.getAgentId());// 设置参数到页面
		}
		// 判断跳转页面
		if ("index".equals(page) || "exerTopic_sch".equals(page)// 判断跳转页面
				|| "exerTopic_vie".equals(page) || "exerTopic_upd".equals(page)// 判断跳转页面
				|| "exerTopicExam_sch".equals(page)|| "topicPremise".equals(page)) {// 判断跳转页面
			return "/examination/" + page;// 跳转页面
		} else if ("exerTopicExam_vie".equals(page)) {// 判断跳转页面
			Exer exer = examinationService.schExerById(eid);// 根据id查询数据
			if (exer != null && exer.getEndTime() != null// 判断必要参数是否为null
					&& exer.getStartTime() != null) {// 判断必要参数是否为null
				long diff = (exer.getEndTime().getTime())// 计算时间差
						- (exer.getStartTime().getTime());// 计算时间差
				long diffs = diff / (1000 * 60);// 转换为分钟
				model.addAttribute("diff", diffs);// 考试时间分钟
			}
			if(exer != null){
				model.addAttribute("exer", exer.getJsonInDict());// 设置参数到页面
			}
			model.addAttribute("etid", id);// 设置参数到页面
			model.addAttribute("eid", eid);// 设置参数到页面
			boolean flg = stuIsExam(person);// 设置考生考试状态
			if (flg) {// 成功标识为true时
				return "/examination/" + page;// 跳转页面
			} else {
				return "/error";// 跳转错误页面
			}
		} else {
			return "/error";// 挑战错误页面
		}
	}

	/**
	 * 修改学生状态为正在考试 
	 * @param person
	 */
	private boolean stuIsExam(Person person) {
		if (person != null) {// 判断用户是否为空
			Stud stu = managerService.schStudById(person.getId());// 查询数据
			stu.setIsExam(YesOrNoConstant.YES);// 设置考试状态
			managerService.updStudData(stu);// 更新数据
			return true;// 返回结果状态
		} else {
			return false;// 返回结果状态
		}
	}

	/**
	 * JSP页面跳转
	 * @param page
	 * jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value = "/getExerStepByEid.action")
	public @ResponseBody List<Map<Integer, List<Integer>>> getExerStepByEid(
			Integer eid, Model model, HttpServletRequest request) { // 放过本Ctrl的页面，不合法的跳转到error页。
		List<Map<Integer, List<Integer>>> topicStepIdsMap = examinationService// 查询结果
				.getTopicIdsAndStepIds(eid);// 查询结果
		return topicStepIdsMap;// 返回结果集
	}

	/**
	 * JSP页面跳转
	 * @param page
	 * jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value = "/getExerStepTeacherByEid.action")
	public @ResponseBody Map<Integer, List<Integer>> getExerStepTeacherByEid(
			Integer eid, Model model, HttpServletRequest request) { // 放过本Ctrl的页面，不合法的跳转到error页。
		model.addAttribute("eid", eid);// 设置eid到页面
		Map<Integer, List<Integer>> topicStepIdsMap = examinationService// 查询结果集
				.getTopicIdsAndStepIdsTeacher(eid);// 查询结果集
		return topicStepIdsMap;// 返回结果集
	}

	/**
	 * 查询方法，自带分页设置。
	 * @param exerTopic
	 * 承载查询条件的Bean
	 * @param request
	 * 可用来获取非Bean内的参数
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schExerTopicTeacher.action")
	public void schExerTopicTeacher(ExerTopic exerTopic,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		noCache(response);// ajax设置无缓存
		exerTopic.setCondition(""); // 防sql注入
		exerTopic.assLike("remark", "title", "type", "level", "profession",// 拼接%
				"difficulty", "author", "beginStepId", "docATitle",// 拼接%
				"docBTitle", "docCTitle", "docDTitle", "docETitle");// 拼接%
		List<ExerTopic> list = examinationService // 调用Service查询数据
				.schExerTopicTeacher(exerTopic); // 调用Service查询数据
		/* 将数据Bean的集合转换为Json集合 */
		JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
		JSONObject obj; // 声明用于构造Bean的Json对象
		for (ExerTopic u : list) { // 循环查询的数据集构造Json数据集
			obj = u.getJsonInDict(); // 将Bean转换为Json对象
			obj.put("_oper", u.getId()); // 为"操作列"设定主键值
			datas.add(obj); // 添加Json对象到Json集合
		}
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		JSONObject res = new JSONObject(); // 声明页面返回Json
		res.put("total", exerTopic.getDatasize()); // 设置数据总记录数
		res.put("rows", datas); // 设置数据集
		response.getWriter().print(res.toJSONString()); // 输出到页面
	}

	/**
	 * JSP页面跳转
	 * @param page
	 * jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value = "/getExerStepByBook.action")
	public @ResponseBody Map<Integer, List<Integer>> getExerStepByBook(
			Integer bookId, Model model, HttpServletRequest request) { // 放过本Ctrl的页面，不合法的跳转到error页。
		Map<Integer, List<Integer>> topicStepIdsMap = examinationService// 查询结果集
				.getTopicIdsAndStepIdsByBookId(bookId);// 查询结果集
		return topicStepIdsMap;// 返回结果集
	}

	/**
	 * 根据bookid和type查询
	 * @param exerTopic
	 * @param request
	 * @param response
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value = "/getTopicsByBookId.action")
	public Map<String, Object> getTopicsByBookId(ExerTopic exerTopic,
			HttpServletRequest request, HttpServletResponse response) {
		exerTopic.setCondition(" and (type='实训营地' or type='4')");
		Map<String, Object> result = new HashMap<String, Object>();// 实例化结果集
		try {
			List<ExerTopic> list = examinationService// 查询结果集
					.schExerTopicList(exerTopic);// 查询结果集
			result.put("data", list);// 设置返回数据
			result.put("success", true);// 设置返回成功状态
			result.put("msg", "查询成功");// 设置返回提示消息
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);// 设置返回成功状态
			result.put("msg", "查询异常");// 设置返回提示消息
		} finally {
			return result;// 返回数据
		}
	}

	/**
	 * 根据bookid和type查询
	 * @param exerTopic
	 * @param request
	 * @param response
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value = "/getTopicsNotCampByBookId.action")
	public Map<String, Object> getTopicsNotCampByBookId(ExerTopic exerTopic,
			HttpServletRequest request, HttpServletResponse response) {
		Person person = (Person) request.getSession(true).getAttribute(
				SysConstant.CURRENT_USER); // 获取当前用户
		Map<String, Object> result = new HashMap<String, Object>();// 实例化结果集
		try {
			List<ExerTopic> list = examinationService// 查询结果集
					.schExerTopicNotCampList(exerTopic);// 查询结果集
			if(list != null && person != null && person.getNumber() != null ){
				for(ExerTopic item : list ){
					item.setHasDo("false");
					if( item != null && item.getId() != null ){
						Integer hasDone = item.getHasDone();
						if (hasDone != null && hasDone.intValue() == 1) {
							item.setHasDo("true");
						}
//						ExerAnswer exerAnswer = new ExerAnswer();
//						exerAnswer.setStudNumber(person.getNumber());
//						exerAnswer.setEtid(item.getId());
//						List<ExerAnswer> ans = examinationService.schExerAnswerList(exerAnswer);
//						if(ans != null && ans.size() > 0){
//							item.setHasDo("true");
//						}
					}
				}
			}
			result.put("data", list);// 设置返回数据
			result.put("success", true);// 设置返回成功状态
			result.put("msg", "查询成功");// 设置返回提示消息
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);// 设置返回成功状态
			result.put("msg", "查询异常");// 设置返回提示消息
		} finally {
			return result;// 返回数据
		}
	}

	/**
	 * 查询所有topic
	 * @param exerTopic
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "schAllExerTopic")
	public void schAllExerTopic(ExerTopic exerTopic,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		List<ExerTopic> list = examinationService.schAllExerTopic(exerTopic); // 调用Service查询数据
		/* 将数据Bean的集合转换为Json集合 */
		JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
		JSONObject obj; // 声明用于构造Bean的Json对象
		for (ExerTopic u : list) { // 循环查询的数据集构造Json数据集
			obj = u.getJsonInDict(); // 将Bean转换为Json对象
			obj.put("_oper", u.getId()); // 为"操作列"设定主键值
			datas.add(obj); // 添加Json对象到Json集合
		}
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		JSONObject res = new JSONObject(); // 声明页面返回Json
		res.put("total", exerTopic.getDatasize()); // 设置数据总记录数
		res.put("rows", datas); // 设置数据集
		response.getWriter().print(res.toJSONString()); // 输出到页面
	}

	/**
	 * 预览题目
	 * @param exerTopic
	 * @param request
	 * @param response
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value="previewTopic.action")
	public Map<String,Object> previewTopic(ExerTopic exerTopic ,
			HttpServletRequest request, HttpServletResponse response){
		// 初始化返回结果集
		Map<String,Object> result = new HashMap<String,Object>();
		try{
			if(exerTopic != null && exerTopic.getId() != null){
				String projectName = request.getContextPath();// 获取参数
				ExerTopic topic = examinationService.schExerTopicById(exerTopic.getId());
				List<StepInfo> steps = examinationService.previewTopic(topic, projectName);
				result.put("topicTypeMap", TopicTypeTransUtil.getTopicTypeToMap());// 题目类型集合
				result.put("stepTypeMap", TopicTypeTransUtil.getStepTypeToMap());// 步骤类型集合
				result.put("topic", topic);
				result.put("steps", steps);
				result.put("success", true);
			}else{
				result.put("success", false);
				result.put("msg", "必要参数缺失");
			}
		}catch(Exception e){
			e.printStackTrace();
			result.put("success", false);
			result.put("msg", "服务器异常");
		}finally{
			return result;
		}
	}
}
