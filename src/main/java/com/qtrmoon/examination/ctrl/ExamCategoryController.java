package com.qtrmoon.examination.ctrl;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qtrmoon.common.BaseController;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.examination.pojo.ExamCategory;
import com.qtrmoon.examination.pojo.Exer;
import com.qtrmoon.examination.serdao.IExaminationService;

/**
 * 控制器
 */
@Controller
@RequestMapping("/examination/examCategory")
public class ExamCategoryController extends BaseController{
	@Autowired
	private IExaminationService examinationService;								//声明模块Service实例

	/**
	 * 查询方法，自带分页设置。
	 * @param examCategory 承载查询条件的Bean
	 * @param request 可用来获取非Bean内的参数
	 * @param response 返回请求,输出Json
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/schExamCategory.action")
	public void schExamCategory(ExamCategory examCategory,HttpServletRequest request,HttpServletResponse response) throws IOException{
		noCache(response);
		examCategory.setCondition("");											//防sql注入
		examCategory.assLike("id","name");

		List<ExamCategory> list=examinationService.schExamCategory(examCategory);		//调用Service查询数据
		/*将数据Bean的集合转换为Json集合*/
		JSONArray datas=new JSONArray();									//构造业务数据的JSON集合
		JSONObject obj;														//声明用于构造Bean的Json对象
		for(ExamCategory u:list){												//循环查询的数据集构造Json数据集
			obj=u.getJsonInDict();											//将Bean转换为Json对象
			obj.put("_oper",u.getId());								//为"操作列"设定主键值
			datas.add(obj);													//添加Json对象到Json集合
		}
		/*构造分页数据的JSON对象，并包装到返回页面的Json中*/
		JSONObject res=new JSONObject();									//声明页面返回Json
		res.put("total",examCategory.getDatasize());							//设置数据总记录数
		res.put("rows", datas);												//设置数据集
		response.getWriter().print(res.toJSONString());						//输出到页面
	}
	
	/**
	 * 预添加or预修改or查看方法
	 * @param examCategory 预添加时可传入一些初始值；预修改和查看时用来承载主键值
	 * @param toL 挂字典的字段是否需要转为字典Label值。true则进行转换
	 * @param response 返回请求,输出Json
	 * @throws IOException
	 */
	@RequestMapping(value="/vieExamCategory.action")
	public void vieExamCategory(ExamCategory examCategory,Boolean toL,HttpServletResponse response) throws IOException{
		noCache(response);													//Ajax方法设定无缓存
		if(toL!=null&&toL){													//查看页面加载数据时调用，返回的对象已进行字典转换
			if(examCategory.getId()!=null&&examCategory.getId()!=0){					//检测主键非空
				examCategory=examinationService.schExamCategoryById(examCategory.getId());			//按主键查询数据
				response.getWriter().print(examCategory.getJsonInDict());						//输出经过字典转换Json对象到查看页(vie_)
			}
		}else{																//修改页面加载数据时调用
			if(examCategory.getId()!=null&&examCategory.getId()!=0){	//检查主键非空
				examCategory=examinationService.schExamCategoryById(examCategory.getId());		//按主键查询数据
				response.getWriter().print(examCategory.getJson());			//输出Json对象到修改页(upd_)
			}else{															//添加页面加载数据时调用，可设置添加页面中一些属性的预设值。
				response.getWriter().print(new ExamCategory().getJson());		//输出Json对象到添加页(upd_)
			}
		}
	}
	
	/**
	 * 添加或修改的提交方法
	 * @param examCategory 数据表单Bean
	 * @param response 返回请求
	 * @throws IOException
	 */
	@RequestMapping(value="/updExamCategory.action")
	public void updExamCategory(ExamCategory examCategory,HttpServletResponse response) throws IOException{
		noCache(response);													//Ajax方法设定无缓存
		if(examCategory.getId()==null){									//检测主键为空则
			examinationService.addExamCategory(examCategory);						//添加数据
		}else{																//主键非空则
			examinationService.updExamCategory(examCategory);						//修改数据
		}
	}
	
	/**
	 * 删除方法
	 * @param ids 待删除数据的主键数组
	 * @param response 返回请求
	 */
	@RequestMapping(value="/delExamCategory.action")
	public void delExamCategory(@RequestParam(value = "ids[]") Integer[] ids,HttpServletResponse response){
		noCache(response);													//Ajax方法设定无缓存
		examinationService.delExamCategory(ids);									//删除数据
	}
	
	/**
	 * JSP页面跳转
	 * @param page jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value="/page.action")
	public String goPage(String page){										//放过本Ctrl的页面，不合法的跳转到error页。
		if("index".equals(page)||"examCategory_sch".equals(page)||"examCategory_vie".equals(page)||"examCategory_upd".equals(page)){
			return "/examination/"+page;
		}else{
			return "/error";
		}
	}
	
	/**
	 * 拖拽节点
	 * @param targetId（目标ID）
	 * @param sourceId（原数据ID）
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value="dropNode.action")
	public Map<String,Object> dropNode(Integer targetId , Integer sourceId){
		// 初始化返回结果集
		Map<String,Object> result = new HashMap<String,Object>();
		try{
			// 判断必要参数是否为空
			if(targetId != null && sourceId != null){
				// 查询原数据
				ExamCategory examCategory = examinationService.schExamCategoryById(sourceId);
				// 判断原数据的父ID是否和新的目标ID一致
				if( examCategory != null && examCategory.getPid() != targetId ){
					examCategory.setPid(targetId);
					examinationService.updExamCategory(examCategory);// 更新数据
					DictBuffer.updCache("ZD_EXAM_CATEGORY");// 更新缓存
				}
				result.put("success", true);
			}else{
				result.put("success", false);
				result.put("msg", "必要参数缺失！");
			}
		}catch(Exception e){
			e.printStackTrace();
			result.put("success", false);
			result.put("msg", "服务器异常！");
		}finally{
			return result ;
		}
	}
	
	/**
	 * 拖拽节点
	 * @param id（主键）
	 * @param text（字典内容）
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value="editNode.action")
	public Map<String,Object> editNode(Integer id , String text){
		// 初始化返回结果集
		Map<String,Object> result = new HashMap<String,Object>();
		try{
			// 判断必要参数是否为空
			if(id != null && text != null && !"".equals(text)){
				// 查询原数据
				ExamCategory examCategory = examinationService.schExamCategoryById(id);
				// 判断原数据的父ID是否和新的目标ID一致
				if( examCategory != null && examCategory.getName() != text ){
					examCategory.setName(text);
					examinationService.updExamCategory(examCategory);// 更新数据
					DictBuffer.updCache("ZD_EXAM_CATEGORY");// 更新缓存
				}
				result.put("success", true);
			}else{
				result.put("success", false);
				result.put("msg", "必要参数缺失！");
			}
		}catch(Exception e){
			e.printStackTrace();
			result.put("success", false);
			result.put("msg", "服务器异常！");
		}finally{
			return result ;
		}
	}
	
	/**
	 * 添加节点
	 * @param id（主键）
	 * @param text（字典内容）
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value="addNode.action")
	public Map<String,Object> addNode(Integer pid){
		// 初始化返回结果集
		Map<String,Object> result = new HashMap<String,Object>();
		try{
			// 判断必要参数是否为空
			if( pid != null ){
				ExamCategory param = new ExamCategory();
				param.setPid(pid);
				param.setName("目录名称 ");
				// 查询原数据
				examinationService.addExamCategory(param);
				DictBuffer.updCache("ZD_EXAM_CATEGORY");// 更新缓存
				result.put("data", param);
				result.put("success", true);
			}else{
				result.put("success", false);
				result.put("msg", "必要参数缺失！");
			}
		}catch(Exception e){
			e.printStackTrace();
			result.put("success", false);
			result.put("msg", "服务器异常！");
		}finally{
			return result ;
		}
	}
	
	/**
	 * 删除节点
	 * @param id（主键）
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value="delNode.action")
	public Map<String,Object> delNode(Integer id){
		// 初始化返回结果集
		Map<String,Object> result = new HashMap<String,Object>();
		try{
			// 判断必要参数是否为空
			if( id != null ){
				ExamCategory examCategory = new ExamCategory();
				examCategory.setPid(id);;
				List<ExamCategory> examCategorys = examinationService.schExamCategoryList(examCategory);
				if(examCategorys == null || examCategorys.size() == 0){
					Exer exer = new Exer();
					exer.setShortcut(id.toString());;
					List<Exer> exers = examinationService.schExerList(exer);
					if(exers == null || exers.size() == 0){
						// 删除数据
						examinationService.delExamCategory(id);
						DictBuffer.updCache("ZD_EXAM_CATEGORY");// 更新缓存
						result.put("success", true);
					}else{
						result.put("success", false);
						result.put("msg", "请先删除关联试卷");
					}
				}else{
					result.put("success", false);
					result.put("msg", "请先删除子节点");
				}
			}else{
				result.put("success", false);
				result.put("msg", "必要参数缺失！");
			}
		}catch(Exception e){
			e.printStackTrace();
			result.put("success", false);
			result.put("msg", "服务器异常！");
		}finally{
			return result ;
		}
	}
	
}
