package com.qtrmoon.examination.constant;

/**
 * 是否保存常量类（与字典配置YesOrNo.xml对应）
 */
public interface IsSaveConstant {

	public static String YES_CODE = "1";// 是
	public static String NO_CODE = "0";// 否
	
	
}
