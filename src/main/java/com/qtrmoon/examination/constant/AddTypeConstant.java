package com.qtrmoon.examination.constant;

/**
 * 组卷类型常量类
 */
public interface AddTypeConstant {

	public static String HAND = "0";// 手动组卷
	public static String AUTOMATIC = "1";// 自动组卷
	
}
