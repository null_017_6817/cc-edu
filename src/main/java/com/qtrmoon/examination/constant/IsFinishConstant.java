package com.qtrmoon.examination.constant;

/**
 * 是否完成标识
 */
public class IsFinishConstant {

	public static String YES_FINISH_CODE = "1";// 是
	public static String NO_FINISH_CODE = "0";// 否
	
	public static String YES_FINISH_STR = "已完成";// 是
	public static String NO_FINISH_STR = "未完成";// 否
}
