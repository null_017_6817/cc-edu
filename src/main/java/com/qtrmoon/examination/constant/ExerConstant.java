package com.qtrmoon.examination.constant;

import com.qtrmoon.book.util.FilePath;

/**
 * 试卷常量
 */
public class ExerConstant {
	public static final String TOPIC_STEP_TYPE_REPEAT = "repeat";//复读陈述题
	public static final String TOPIC_STEP_TYPE_STATE = "state";//应答陈述题
	public static final String TOPIC_STEP_TYPE_VOICE = "voice";//语音提示题
	public static final String TOPIC_STEP_TYPE_WEB = "web";//web应用题
	public static final String TOPIC_STEP_TYPE_QUESTION = "question";//问题
	public static final String TOPIC_STEP_TYPE_QUESTION_JUDGE = "judge";//问题-判断题
	public static final String TOPIC_STEP_TYPE_QUESTION_CLOZE = "cloze";//问题-填空题
	public static final String TOPIC_STEP_TYPE_QUESTION_ANSWER = "answer";//问题-问答题
	public static final String TOPIC_STEP_TYPE_QUESTION_CHOICE_SINGLE = "choice_single";//问题-单选题
	public static final String TOPIC_STEP_TYPE_QUESTION_CHOICE_MULTI = "choice_multi";//问题-多选题
	public static final String TOPIC_STEP_TYPE_DOCUMENT = "document";//文档题 20130625
	public static final String TOPIC_STEP_TYPE_PRACTICE = "practice";//实训对话 20130727
	
	//定义FREEMAKER模板名称常量
	public static final String FREEMAKER_READ_MEDIA_RECORD = "readMediaRecord.txt";//读取录音media下
	public static final String FREEMAKER_TOPIC_STEP_TYPE_REPEAT = "repeat.txt";//复读陈述题模板
	public static final String FREEMAKER_TOPIC_STEP_TYPE_STATE = "state.txt";//应答陈述题模板
	public static final String FREEMAKER_TOPIC_STEP_TYPE_WEB = "topicStepWeb.txt";//WEB应用题模板
	public static final String FREEMAKER_TOPIC_STEP_TYPE_QUESTION_JUDGE = "judge.txt";//判断题模板
	public static final String FREEMAKER_TOPIC_STEP_TYPE_QUESTION_CLOZE = "cloze.txt";//听打录入模板
	public static final String FREEMAKER_TOPIC_STEP_TYPE_QUESTION_NOVOICECLOZE = "novoicecloze.txt";//填空题模板
	public static final String FREEMAKER_TOPIC_STEP_TYPE_QUESTION_ANSWER = "answer.txt";//问答题模板
	public static final String FREEMAKER_TOPIC_STEP_TYPE_QUESTION_NOFILEANSWER = "nofileanswer.txt";//问答题模板(无附件操作)
	public static final String FREEMAKER_TOPIC_STEP_TYPE_QUESTION_PROMPT ="promptvoice.txt";//语音类型模板
	public static final String FREEMAKER_TOPIC_STEP_TYPE_QUESTION_CHOICE_SINGLE = "singlechoice.txt";//单项选择题模板
	public static final String FREEMAKER_TOPIC_STEP_TYPE_QUESTION_CHOICE_MULTI = "multichoice.txt";//多项选择题模板
	public static final String FREEMAKER_TOPIC_STEP_TYPE_DOCUMENT = "document.txt";//文档模板 20130625
	public static final String FREEMAKER_TOPIC_STEP_TYPE_PRACTICE = "practice.txt";//实训对话模板 20130727
	public static final String FREEMAKER_TOPIC_STEP_TYPE_PRACTICE_OTHER = "topicStepPracticeOther.txt";//实训对话模板 20130731
	public static final String FREEMAKER_FILE_PATH = "/freemakertemp";//模板路径
	public static final String HTML_FILE_PATH = FilePath.getFilePath("freemarkerHtml"); //"/export";//生成html文件的路径
	public static final String AUDIO_FILE_PATH = "/upload/data/";
	
	//定义标签名称
	public static final String STEP_TITLE = "title";// 定义title
	public static final String STEP_PROMPT = "prompt";// 定义prompt
	public static final String STEP_AUDIO = "audio";// 定义audio
	public static final String STEP_REPEAT = "repeat";// 定义repeat
	public static final String STEP_ID = "id";// 定义id
	public static final String STEP_ANSWER = "answer";// 定义answer
	public static final String STEP_TYPE = "type";// 定义type
	public static final String STEP_STANDARD_ANSWER = "standard_answer";// 定义standard_answer
	public static final String STEP_GOTO = "goto";// 定义goto
	public static final String STEP_NEXT = "next";// 定义next
	public static final String STEP_CHOICE = "choice";// 定义next
	public static final String STEP_PROMPT_FILENAME="fileName";//实操题zip系统文件名称
	public static final String STEP_PROMPT_ORIGINAME="origiName";//实操题zip系统文件名称

	public static final String ID = "id";// 定义id
	public static final String AUDIO = "audio";// 定义audio
	public static final String SCORE = "score";// 定义score
	
	//开始和结束标签标记
	public static final String STEP_BEGIN_ID = "BEGIN";// 定义BEGIN
	public static final String STEP_END_ID = "END";// 定义END
}
