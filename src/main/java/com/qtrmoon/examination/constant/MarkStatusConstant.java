package com.qtrmoon.examination.constant;

/**
 * 阅卷状态常量类（与字典配置zd_mark_srarus对应）
 */
public interface MarkStatusConstant {

	public static String MARK_NO = "0";// 未阅卷
	public static String MARK_ING = "1";// 阅卷中
	public static String MARK_FINISH = "2";// 已卷中
	
}
