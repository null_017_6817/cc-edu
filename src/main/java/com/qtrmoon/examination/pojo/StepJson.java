package com.qtrmoon.examination.pojo;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.HashMap;
import java.io.ByteArrayInputStream;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.jdom.Document;
import org.jdom.Element;
// import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import com.qtrmoon.examination.constant.ExerConstant;
import com.qtrmoon.manager.constant.TopicTypeConstant;
import com.qtrmoon.manager.pojo.BankStep;

import com.qtrmoon.toolkit.XmlUtil;

/**
 * 步骤信息
 */
public class StepJson {
    private String stepId; // 步骤主键
	private String title; // 步骤名称
	private String score;  //步骤总分数
    // private Prompt prompt; //题干
    private String promptContent;
    private String promptAudio;
    // private List<ChoiceQuestion> choiceQuestionList;//选择题集合
    private Map<String, String> multiChoiceAudio;
    private Map<String, String> multiChoiceContent;
    private List<String> multiChoiceAnswer;
    private Map<String, String> singleChoiceContent;
    private Map<String, String> singleChoiceAudio;
    private Map<String, String> clozedAnswerContent;
    private Map<String, String> clozedAnswerScore;
    private Map<String, String> clozePromptContent;
    private Map<String, String> clozeAnswerContent;
    private Map<String, String> clozeAnswerScore;
	private String type;//类型
	private String standardAnswer;//标准答案
	// private String next;//下一步
	// private String projectName;//项目名称
	private String bankId;//题库id
    private String topicId;//题目id
    private String origiName;
    private String fileName;
    
    public StepJson(BankStep step) {
        this.stepId = step.getId()+"";
        this.bankId = step.getBankId();
        this.topicId = step.getTopicId();
        this.title = step.getTitle(); 
        this.type = step.getType();
        this.score = step.getScore();
        this.promptContent = step.getParam();
        this.standardAnswer = step.getStandardAnswer();
        this.singleChoiceAudio = new HashMap<String, String>();
        this.singleChoiceContent = new HashMap<String, String>();
        this.multiChoiceAnswer = new ArrayList<String>();
        this.multiChoiceAudio = new HashMap<String, String>();
        this.multiChoiceContent = new HashMap<String, String>();
        this.clozedAnswerContent = new HashMap<String, String>();
        this.clozedAnswerScore = new HashMap<String, String>();
        this.clozePromptContent = new HashMap<String, String>();
        this.clozeAnswerContent = new HashMap<String, String>();
        this.clozeAnswerScore = new HashMap<String, String>();

        xmlToStepJson(step.getParam(), step.getType());
        // if(step.getType().equals(TopicTypeConstant.SINGLE)) {
            
        // }
        // if(step.getType().equals(TopicTypeConstant.MULTI)) {

        // }
    }

    // TODO
    @SuppressWarnings("unchecked")
    private void xmlToStepJson(String xmlParam, String stepType) {
        // 实训营地
        if(stepType.equals(TopicTypeConstant.STEP_CAMPING)) {
            return;                
        }
        XmlUtil xml=new XmlUtil();// 定义对象
        xml.setCharset("UTF-8");// 设置字符编码
        SAXBuilder sb = new SAXBuilder();// 定义对象
        try{
		    ByteArrayInputStream bais=new ByteArrayInputStream(xmlParam.getBytes("UTF-8"));// 定义流
            Document doc = sb.build(bais);// 定义对象
            Element stepRoot = doc.getRootElement();// 获取节点
            // 题干
            Element promptEle = stepRoot.getChild(ExerConstant.STEP_PROMPT);
            this.promptAudio = promptEle.getAttributeValue(ExerConstant.AUDIO);
            this.promptContent = promptEle.getText();
            // 单选
            if(stepType.equals(TopicTypeConstant.SINGLE)) {
                // 单选选项
                Element answerEle = stepRoot.getChild(ExerConstant.STEP_ANSWER);
                List<Element> choiceEles = answerEle.getChildren(ExerConstant.STEP_CHOICE);
                for(Element choiceEle : choiceEles) {
                    String choiceId = choiceEle.getAttributeValue(ExerConstant.ID);
                    String choiceAudio = choiceEle.getAttributeValue(ExerConstant.AUDIO);
                    String choiceContent = choiceEle.getText();
                    this.singleChoiceAudio.put(choiceId, choiceAudio);
                    this.singleChoiceContent.put(choiceId, choiceContent);
                } 
                // 单选答案
                Element standardAnswerEle = stepRoot.getChild(ExerConstant.STEP_STANDARD_ANSWER);
                this.standardAnswer = standardAnswerEle.getText();
            }
            // 多选
            if(stepType.equals(TopicTypeConstant.MULTI)) {
                // 多选选项
                Element answerEle = stepRoot.getChild(ExerConstant.STEP_ANSWER);
                List<Element> choiceEles = answerEle.getChildren(ExerConstant.STEP_CHOICE);
                for(Element choiceEle : choiceEles) {
                    String choiceId = choiceEle.getAttributeValue(ExerConstant.ID);
                    String choiceAudio = choiceEle.getAttributeValue(ExerConstant.AUDIO);
                    String choiceContent = choiceEle.getText();
                    this.multiChoiceAudio.put(choiceId, choiceAudio);
                    this.multiChoiceContent.put(choiceId, choiceContent);
                }
                // 多选答案
                Element standardAnswerEle = stepRoot.getChild(ExerConstant.STEP_STANDARD_ANSWER);
                String multiAnswer = standardAnswerEle.getText();
                this.standardAnswer = multiAnswer;
                String[] multiAnswerOption = multiAnswer.split(",");
                for(String opt : multiAnswerOption) {
                    multiChoiceAnswer.add(opt);
                }
            }
            // 填空
            if(stepType.equals(TopicTypeConstant.CLOZED)) {
                // 填空
                Element clozedScoreEle = stepRoot.getChild(ExerConstant.SCORE);
                String everyScore = clozedScoreEle.getText();
                this.score = everyScore;
                // String clozedScores_ = clozedScoreEle.getText();
                Element clozedAnswerEle = stepRoot.getChild(ExerConstant.STEP_STANDARD_ANSWER);
                String clozedStandardAnswers_ = clozedAnswerEle.getText();
                // String[] clozedScores = clozedScores_.split(",");
                String[] clozedStandardAnswers = clozedStandardAnswers_.split(",");
                // if(clozedScores.length == clozedStandardAnswers.length) {
                    for(int i = 0; i < clozedStandardAnswers.length; i++) {
                        // this.clozedAnswerScore.put(TopicTypeConstant.OPTIONS[i]+"", clozedScores[i]);
                        this.clozedAnswerContent.put(TopicTypeConstant.OPTIONS[i]+"", clozedStandardAnswers[i]);
                    }
                // }
            }
            // 判断
            if(stepType.equals(TopicTypeConstant.JUDGE)) {
                Element standardAnswerEle = stepRoot.getChild(ExerConstant.STEP_STANDARD_ANSWER);
                String judgeStandardAnswer = standardAnswerEle.getText();
                this.standardAnswer = judgeStandardAnswer;
            }
            // 问答
            if(stepType.equals(TopicTypeConstant.ANSWER)) {
                this.origiName = promptEle.getAttributeValue(ExerConstant.STEP_PROMPT_ORIGINAME);
                this.fileName = promptEle.getAttributeValue(ExerConstant.STEP_PROMPT_FILENAME);
                
                Element standardAnswerEle = stepRoot.getChild(ExerConstant.STEP_STANDARD_ANSWER);
                String answerStandardAnswer = standardAnswerEle.getText();
                this.standardAnswer = answerStandardAnswer;
            }
            // 复读陈述题
            if(stepType.equals(TopicTypeConstant.REPEAT)) {
                Element standardAnswerEle = stepRoot.getChild(ExerConstant.STEP_STANDARD_ANSWER);
                String repeatStandardAnswer = standardAnswerEle.getText();
                this.standardAnswer = repeatStandardAnswer;                
            }
            // 应答陈述题
            if(stepType.equals(TopicTypeConstant.STATE)) {
                Element standardAnswerEle = stepRoot.getChild(ExerConstant.STEP_STANDARD_ANSWER);
                String stateStandardAnswer = standardAnswerEle.getText();
                this.standardAnswer = stateStandardAnswer;                
            }
            // 语音提示
            if(stepType.equals(TopicTypeConstant.VOICE)) {
                Element standardAnswerEle = stepRoot.getChild(ExerConstant.STEP_STANDARD_ANSWER);
                String voiceStandardAnswer = standardAnswerEle.getText();
                this.standardAnswer = voiceStandardAnswer;                
            }
            // 听打录入
            if(stepType.equals(TopicTypeConstant.CLOZE)) {
                Element clozeScoreEle = stepRoot.getChild(ExerConstant.SCORE);
                String clozeScores_ = clozeScoreEle.getText();
                Element clozeAnswerEle = stepRoot.getChild(ExerConstant.STEP_STANDARD_ANSWER);
                String clozeStandardAnswers_ = clozeAnswerEle.getText();
                Element clozePromptEle = stepRoot.getChild(ExerConstant.STEP_PROMPT);
                String clozePromptContents_ = clozePromptEle.getText();
                // String[] clozeScores = clozeScores_.split(",");
                String[] clozeStandardAnswers = clozeStandardAnswers_.split(",");
                // String[] clozePromptContents = clozePromptContents_.split("[]");
                String clozePromptContents__ = clozePromptContents_.replace("[]", ",");
                String[] clozePromptContents = clozePromptContents__.split(",");
                // if(clozePromptContents.length == clozeStandardAnswers.length) {
                    for(int i = 0; i < clozeStandardAnswers.length; i++) {
                        if(StringUtils.isNotBlank(clozeStandardAnswers[i])) {
                            this.clozeAnswerScore.put(TopicTypeConstant.OPTIONS[i]+"", clozeStandardAnswers[i].trim().length()+"");
                        }else {
                            this.clozeAnswerScore.put(TopicTypeConstant.OPTIONS[i]+"", "0");
                        }
                        // this.clozeAnswerScore.put(TopicTypeConstant.OPTIONS[i]+"", clozeScores[i]);
                        this.clozeAnswerContent.put(TopicTypeConstant.OPTIONS[i]+"", clozeStandardAnswers[i]);
                        // this.clozePromptContent.put(TopicTypeConstant.OPTIONS[i]+"", clozePromptContents[i]);
                    }
                    for(int j = 0; j < clozePromptContents.length; j++) {
                        this.clozePromptContent.put(TopicTypeConstant.OPTIONS[j]+"", clozePromptContents[j]);
                    }
                // }
            }
            // 数据分析题
            if(stepType.equals(TopicTypeConstant.ANALYZE)) {
                this.origiName = promptEle.getAttributeValue(ExerConstant.STEP_PROMPT_ORIGINAME);
                this.fileName = promptEle.getAttributeValue(ExerConstant.STEP_PROMPT_FILENAME);
                
                Element standardAnswerEle = stepRoot.getChild(ExerConstant.STEP_STANDARD_ANSWER);
                String answerStandardAnswer = standardAnswerEle.getText();
                this.standardAnswer = answerStandardAnswer;
            }
            // 话术编写
            if(stepType.equals(TopicTypeConstant.VERBAL)) {
                this.origiName = promptEle.getAttributeValue(ExerConstant.STEP_PROMPT_ORIGINAME);
                this.fileName = promptEle.getAttributeValue(ExerConstant.STEP_PROMPT_FILENAME);
                
                Element standardAnswerEle = stepRoot.getChild(ExerConstant.STEP_STANDARD_ANSWER);
                String answerStandardAnswer = standardAnswerEle.getText();
                this.standardAnswer = answerStandardAnswer;
            }
            // 连线画图
            if(stepType.equals(TopicTypeConstant.STEP_DRAW)) {
                this.origiName = promptEle.getAttributeValue(ExerConstant.STEP_PROMPT_ORIGINAME);
                this.fileName = promptEle.getAttributeValue(ExerConstant.STEP_PROMPT_FILENAME);
                
                Element standardAnswerEle = stepRoot.getChild(ExerConstant.STEP_STANDARD_ANSWER);
                String answerStandardAnswer = standardAnswerEle.getText();
                this.standardAnswer = answerStandardAnswer;
            }

       }catch (Exception e) {
            e.printStackTrace();
        }

    }

    public String getStepId() {
        return stepId;
    }

    public void setStepId(String stepId) {
        this.stepId = stepId;
    }
	
	/**
	 * 获取标题
	 * @return
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * 设置标题
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * 获取分数
	 * @return
	 */
	public String getScore() {
		return score;
	}
	/**
	 * 设置分数
	 * @param score
	 */
	public void setScore(String score) {
		this.score = score;
	}
	/**
	 * 获取题干
	 * @return
	 */
	public String getPromptContent() {
		return this.promptContent;
	}
	/**
	 * 设置题干
	 * @param prompt
	 */
	public void setPromptContent(String promptContent) {
		this.promptContent = promptContent;
    }
    
    public String getPromptAudio() {
        return this.promptAudio;
    }

    public void setPromptAudio(String promptAudio) {
        this.promptAudio = promptAudio;
    }

	/**
	 * 获取选择问题列表
	 * @return
	 */
	// public List<ChoiceQuestion> getChoiceQuestionList() {
	// 	return choiceQuestionList;
	// }
	/**
	 * 设置选择问题列表
	 * @param choiceQuestionList
	 */
	// public void setChoiceQuestionList(List<ChoiceQuestion> choiceQuestionList) {
	// 	this.choiceQuestionList = choiceQuestionList;
    // }
    
    // TODO
    public Map<String, String> getMultiChoiceAudio() {
        return this.multiChoiceAudio;
    }

    public void setMultiChoiceAudio(Map<String, String> multiChoiceAudio) {
        this.multiChoiceAudio = multiChoiceAudio;
    }

    public Map<String, String> getMultiChoiceContent() {
        return this.multiChoiceContent;
    }

    public void setMultiChoiceContent(Map<String, String> multiChoiceContent) {
        this.multiChoiceContent = multiChoiceContent;
    }

    public List<String> getMultiChoiceAnswer() {
        return this.multiChoiceAnswer;
    }

    public void setMultiChoiceAnswer(List<String> multiChoiceAnswer) {
        this.multiChoiceAnswer = multiChoiceAnswer;
    }

    public Map<String, String> getSingleChoiceContent() {
        return this.singleChoiceContent;
    }

    public void setSingleChoiceContent(Map<String, String> singleChoiceContent) {
        this.singleChoiceContent = singleChoiceContent;
    }

    public Map<String, String> getSingleChoiceAudio() {
        return this.singleChoiceAudio;
    }

    public void setSingleChoiceAudio(Map<String, String> singleChoiceAudio) {
        this.singleChoiceAudio = singleChoiceAudio;
    }

    public Map<String, String> getClozedAnswerContent() {
        return this.clozedAnswerContent;
    }

    public void setClozedAnswerContent(Map<String, String> clozedAnswerContent) {
        this.clozedAnswerContent = clozedAnswerContent;
    }

    public Map<String, String> getClozedAnswerScore() {
        return this.clozedAnswerScore;
    }

    public void setClozedAnswerScore(Map<String, String> clozedAnswerScore) {
        this.clozedAnswerScore = clozedAnswerScore;
    }

    public Map<String, String> getClozePromptContent() {
        return this.clozePromptContent;
    }

    public void setClozePromptContent(Map<String, String> clozePromptContent) {
        this.clozePromptContent = clozePromptContent;
    }

    public Map<String, String> getClozeAnswerContent() {
        return this.clozeAnswerContent;
    }

    public void setClozeAnswerContent(Map<String, String> clozeAnswerContent) {
        this.clozeAnswerContent = clozeAnswerContent;
    }

    public Map<String, String> getClozeAnswerScore() {
        return this.clozeAnswerScore;
    }

    public void setClozeAnswerScore(Map<String, String> clozeAnswerScore) {
        this.clozeAnswerScore = clozeAnswerScore;
    }

	/**
	 * 获取类型
	 * @return
	 */
	public String getType() {
		return type;
	}
	/**
	 * 设置类型
	 * @param type
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * 获取标准答案
	 * @return
	 */
	public String getStandardAnswer() {
		return standardAnswer;
	}
	/**
	 * 设置标准答案
	 * @param standardAnswer
	 */
	public void setStandardAnswer(String standardAnswer) {
		this.standardAnswer = standardAnswer;
	}
	/**
	 * 获取下一步
	 * @return
	 */
	// public String getNext() {
	// 	return next;
	// }
	/**
	 * 设置下一步
	 * @param next
	 */
	// public void setNext(String next) {
	// 	this.next = next;
	// }
	/**
	 * 获取项目名称
	 * @return
	 */
	// public String getProjectName() {
	// 	return projectName;
	// }
	/**
	 * 设置项目名称
	 * @param projectName
	 */
	// public void setProjectName(String projectName) {
	// 	this.projectName = projectName;
	// }
	public String getBankId() {
		return bankId;
	}
	public void setBankId(String bankId) {
		this.bankId = bankId;
	}
	public String getTopicId() {
		return topicId;
	}
	public void setTopicId(String topicId) {
		this.topicId = topicId;
    }
    
    // JSON
    @SuppressWarnings("unchecked")
    public JSONObject getStepJson() {
        JSONObject stepJson = new JSONObject();
        stepJson.put("stepId", this.stepId);
        stepJson.put("stepName", this.title);
        stepJson.put("score", this.score);
        stepJson.put("promptContent", this.promptContent);
        stepJson.put("promptAudio", this.promptAudio);
        stepJson.put("stepType", this.type);
        stepJson.put("bankId", this.bankId);
        stepJson.put("topicId", this.topicId);
        stepJson.put("standardAnswer", this.standardAnswer);
        stepJson.put("origiName", this.origiName);
        stepJson.put("fileName", this.fileName);
        // 单选
        JSONArray singleContentArray = new JSONArray();
        JSONArray singleAudioArray = new JSONArray();
        Set<String> singleContentKeys = this.singleChoiceContent.keySet();
        for(String singleContentKey : singleContentKeys) {
            JSONObject singleContent = new JSONObject();
            JSONObject singleAudio = new JSONObject();
            singleContent.put(singleContentKey, this.singleChoiceContent.get(singleContentKey));
            singleAudio.put(singleContentKey, this.singleChoiceAudio.get(singleContentKey));
            singleContentArray.add(singleContent);
            singleAudioArray.add(singleAudio);
        }
        stepJson.put("singleChoiceContent", singleContentArray);
        stepJson.put("singleChoiceAudio", singleAudioArray);
        // 多选
        JSONArray multiContentArray = new JSONArray();
        JSONArray multiAudioArray = new JSONArray();
        Set<String> multiContentKeys = this.multiChoiceContent.keySet();
        for(String multiContentKey : multiContentKeys) {
            JSONObject multiContent = new JSONObject();
            JSONObject multiAudio = new JSONObject();
            multiContent.put(multiContentKey, this.multiChoiceContent.get(multiContentKey));
            multiAudio.put(multiContentKey, this.multiChoiceAudio.get(multiContentKey));
            multiContentArray.add(multiContent);
            multiAudioArray.add(multiAudio);
        }
        stepJson.put("multiChoiceContent", multiContentArray);
        stepJson.put("multiChoiceAudio", multiAudioArray);
        JSONArray multiAnswerArray = new JSONArray();
        for(String multiAnswerOpt: multiChoiceAnswer) {
            multiAnswerArray.add(multiAnswerOpt);
        }
        stepJson.put("multiChoiceAnswer", multiAnswerArray);
        // 填空
        JSONArray clozedContentArray = new JSONArray();
        JSONArray clozedScoreArray = new JSONArray();
        Set<String> clozedContentKeys = this.clozedAnswerContent.keySet();
        // Set<String> clozedScoreKeys = this.clozedAnswerScore.keySet();
        // if(clozedContentKeys.size() == clozedScoreKeys.size()) {
            for(String clozedContentKey : clozedContentKeys) {
                JSONObject clozedContent = new JSONObject();
                JSONObject clozedScore = new JSONObject();
                clozedContent.put(clozedContentKey, this.clozedAnswerContent.get(clozedContentKey));
                clozedScore.put(clozedContentKey, this.clozedAnswerScore.get(clozedContentKey));
                clozedContentArray.add(clozedContent);
                clozedScoreArray.add(clozedScore);
            }
        // }
        stepJson.put("clozedAnswerContent", clozedContentArray);
        stepJson.put("clozedAnswerScore", clozedScoreArray);
        // 听打录入
        JSONArray clozePromptArray = new JSONArray();
        JSONArray clozeAnswerArray = new JSONArray();
        JSONArray clozeScoreArray = new JSONArray();
        Set<String> clozePromptKeys = this.clozePromptContent.keySet();
        Set<String> clozeAnswerKeys = this.clozeAnswerContent.keySet();
        Set<String> clozeScoreKeys = this.clozeAnswerScore.keySet();
        if(clozePromptKeys.size() == clozeAnswerKeys.size() && clozePromptKeys.size() == clozeScoreKeys.size()) {
            for(String clozePromptKey : clozePromptKeys) {
                JSONObject clozePrompt = new JSONObject();
                JSONObject clozeAnswer = new JSONObject();
                JSONObject clozeScore = new JSONObject();
                clozePrompt.put(clozePromptKey, this.clozePromptContent.get(clozePromptKey));
                clozeAnswer.put(clozePromptKey, this.clozeAnswerContent.get(clozePromptKey));
                clozeScore.put(clozePromptKey, this.clozeAnswerScore.get(clozePromptKey));
                clozePromptArray.add(clozePrompt);
                clozeAnswerArray.add(clozeAnswer);
                clozeScoreArray.add(clozeScore);
            }
        }
        stepJson.put("clozePromptContent", clozePromptArray);
        stepJson.put("clozeAnswerContent", clozeAnswerArray);
        stepJson.put("clozeAnswerScore", clozeScoreArray);
        return stepJson;
    }
}
