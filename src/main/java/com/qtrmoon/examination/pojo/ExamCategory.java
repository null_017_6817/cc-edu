package com.qtrmoon.examination.pojo;
import java.util.Date;
import java.util.Set;
import java.util.HashSet;
import org.json.simple.JSONObject;
import com.qtrmoon.common.PageForm;
import com.qtrmoon.toolkit.DateTransfer;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.dictionary.bsd.DictBean;

/** 实体类 */
public class ExamCategory extends PageForm{
	//Fields
	
	private Integer id;// id
	private Integer pid;// 父ID
	private String name;// 名称
	private String type;// 类型
	private String ord;// 排序
	private String info;// 说明
	//Constructors
	/** default constructor */
	public ExamCategory() {
	
	}	
	//getter and setter
	/** 获取id */
	public Integer getId() {
		return this.id;
	}
	/** 设置id */
	public void setId(Integer id) {
		this.id = id;
	}
	/** 获取父ID */
	public Integer getPid() {
		return this.pid;
	}
	/** 设置父ID */
	public void setPid(Integer pid) {
		this.pid = pid;
	}
	/** 获取名称 */
	public String getName() {
		return this.name;
	}
	/** 设置名称 */
	public void setName(String name) {
		this.name = name;
	}
	/** 获取类型 */
	public String getType() {
		return this.type;
	}
	/** 设置类型 */
	public void setType(String type) {
		this.type = type;
	}
	/** 获取排序 */
	public String getOrd() {
		return this.ord;
	}
	/** 设置排序 */
	public void setOrd(String ord) {
		this.ord = ord;
	}
	/** 获取说明 */
	public String getInfo() {
		return this.info;
	}
	/** 设置说明 */
	public void setInfo(String info) {
		this.info = info;
	}
	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("pid", pid);
		obj.put("name", name);
		obj.put("type", type);
		obj.put("ord", ord);
		obj.put("info", info);
		return obj;
	}
	
	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("pid", pid);
		obj.put("name", name);
		obj.put("type", type);
		obj.put("ord", ord);
		obj.put("info", info);
		return obj;
	}
}
