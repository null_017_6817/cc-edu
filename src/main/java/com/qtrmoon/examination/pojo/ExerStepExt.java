package com.qtrmoon.examination.pojo;
/**
 * 步骤扩展
 */
public class ExerStepExt extends ExerStep{

	private String answerId;//答案id
	private String stuAnswer;//学生答案
	private StepInfo stepEntity;//步骤实体
	/**
	 * 获取步骤实体
	 * @return
	 */
	public StepInfo getStepEntity() {
		return stepEntity;
	}
	/**
	 * 设置步骤实体
	 * @param stepEntity
	 */
	public void setStepEntity(StepInfo stepEntity) {
		this.stepEntity = stepEntity;
	}
	/**
	 * 获取答案id
	 * @return
	 */
	public String getAnswerId() {
		return answerId;
	}
	/**
	 * 设置答案id
	 * @param answerId
	 */
	public void setAnswerId(String answerId) {
		this.answerId = answerId;
	}
	/**
	 * 获取答案
	 * @return
	 */
	public String getStuAnswer() {
		return stuAnswer;
	}
	/**
	 * 设置答案
	 * @param stuAnswer
	 */
	public void setStuAnswer(String stuAnswer) {
		this.stuAnswer = stuAnswer;
	}
}
