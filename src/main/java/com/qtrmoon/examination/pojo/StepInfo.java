package com.qtrmoon.examination.pojo;

import java.util.List;
/**
 * 步骤信息
 */
public class StepInfo {
	private String title;//标题
	private String score;//分数
	private Prompt prompt;//题干
	private List<ChoiceQuestion> choiceQuestionList;//选择题集合
	private String type;//类型
	private String standardAnswer;//标准答案
	private String next;//下一布
	private String projectName;//项目名称
	private String bankId;//题库id
	private String topicId;//题目id
	private Integer esid ;// 试卷步骤ID
	
	
	/**
	 * 获取标题
	 * @return
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * 设置标题
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * 获取分数
	 * @return
	 */
	public String getScore() {
		return score;
	}
	/**
	 * 设置分数
	 * @param score
	 */
	public void setScore(String score) {
		this.score = score;
	}
	/**
	 * 获取题干
	 * @return
	 */
	public Prompt getPrompt() {
		return prompt;
	}
	/**
	 * 设置题干
	 * @param prompt
	 */
	public void setPrompt(Prompt prompt) {
		this.prompt = prompt;
	}
	/**
	 * 获取选择问题列表
	 * @return
	 */
	public List<ChoiceQuestion> getChoiceQuestionList() {
		return choiceQuestionList;
	}
	/**
	 * 设置选择问题列表
	 * @param choiceQuestionList
	 */
	public void setChoiceQuestionList(List<ChoiceQuestion> choiceQuestionList) {
		this.choiceQuestionList = choiceQuestionList;
	}
	/**
	 * 获取类型
	 * @return
	 */
	public String getType() {
		return type;
	}
	/**
	 * 设置类型
	 * @param type
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * 获取标准答案
	 * @return
	 */
	public String getStandardAnswer() {
		return standardAnswer;
	}
	/**
	 * 设置标准答案
	 * @param standardAnswer
	 */
	public void setStandardAnswer(String standardAnswer) {
		this.standardAnswer = standardAnswer;
	}
	/**
	 * 获取下一步
	 * @return
	 */
	public String getNext() {
		return next;
	}
	/**
	 * 设置下一步
	 * @param next
	 */
	public void setNext(String next) {
		this.next = next;
	}
	/**
	 * 获取项目名称
	 * @return
	 */
	public String getProjectName() {
		return projectName;
	}
	/**
	 * 设置项目名称
	 * @param projectName
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getBankId() {
		return bankId;
	}
	public void setBankId(String bankId) {
		this.bankId = bankId;
	}
	public String getTopicId() {
		return topicId;
	}
	public void setTopicId(String topicId) {
		this.topicId = topicId;
	}
	public Integer getEsid() {
		return esid;
	}
	public void setEsid(Integer esid) {
		this.esid = esid;
	}
}
