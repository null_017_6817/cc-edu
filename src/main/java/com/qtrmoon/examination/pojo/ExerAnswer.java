package com.qtrmoon.examination.pojo;
import java.util.Date;

import org.json.simple.JSONObject;

import com.qtrmoon.common.PageForm;
	/** 学生答题卡Object类 */
public class ExerAnswer extends PageForm{
	private Integer id;// 主键
	private Integer eid;// 练习编号
	private Integer etid;// 考题编号
	private Integer esid;// 步骤编号
	private String studNumber;// 学号
	private String remark;// 备注
	private String answer;// 答题内容
	private Float score;
	
	private String fileUrl;// 文件路径
	private String fileOriginalName;//文件原名称
	private Date uploadTime;// 上传时间
	
	
	public Date getUploadTime() {
		return uploadTime;
	}
	public void setUploadTime(Date uploadTime) {
		this.uploadTime = uploadTime;
	}
	//Constructors
	/** default constructor */
	public ExerAnswer() {
	
	}	
	//getter and setter
	/** 获取主键 */
	public Integer getId() {
		return this.id;
	}
	/** 设置主键 */
	public void setId(Integer id) {
		this.id = id;
	}
	/** 获取练习编号 */
	public Integer getEid() {
		return this.eid;
	}
	/** 设置练习编号 */
	public void setEid(Integer eid) {
		this.eid = eid;
	}
	/** 获取考题编号 */
	public Integer getEtid() {
		return this.etid;
	}
	/** 设置考题编号 */
	public void setEtid(Integer etid) {
		this.etid = etid;
	}
	/** 获取步骤编号 */
	public Integer getEsid() {
		return this.esid;
	}
	/** 设置步骤编号 */
	public void setEsid(Integer esid) {
		this.esid = esid;
	}
	/** 获取学号 */
	public String getStudNumber() {
		return this.studNumber;
	}
	/** 设置学号 */
	public void setStudNumber(String studNumber) {
		this.studNumber = studNumber;
	}
	/** 获取备注 */
	public String getRemark() {
		return this.remark;
	}
	/** 设置备注 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/** 获取答题内容 */
	public String getAnswer() {
		return this.answer;
	}
	/** 设置答题内容 */
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	/**
	 * 获取分数
	 * @return
	 */
	public Float getScore() {
		return score;
	}
	/**
	 * 设置分数
	 * @param score
	 */
	public void setScore(Float score) {
		this.score = score;
	}
	
	public String getFileUrl() {
		return fileUrl;
	}
	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}
	public String getFileOriginalName() {
		return fileOriginalName;
	}
	public void setFileOriginalName(String fileOriginalName) {
		this.fileOriginalName = fileOriginalName;
	}
	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("eid", eid);
		obj.put("etid", etid);
		obj.put("esid", esid);
		obj.put("studNumber", studNumber);
		obj.put("remark", remark);
		obj.put("answer", answer);
		obj.put("score", score);
		obj.put("fileUrl", fileUrl);
		obj.put("fileOriginalName", fileOriginalName);
		return obj;
	}
	
	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("eid", eid);
		obj.put("etid", etid);
		obj.put("esid", esid);
		obj.put("studNumber", studNumber);
		obj.put("remark", remark);
		obj.put("answer", answer);
		obj.put("score", score);
		obj.put("fileUrl", fileUrl);
		obj.put("fileOriginalName", fileOriginalName);
		return obj;
	}
}
