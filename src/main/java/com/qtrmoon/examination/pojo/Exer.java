package com.qtrmoon.examination.pojo;
import java.util.Date;
import java.util.Map;

import org.json.simple.JSONObject;

import com.qtrmoon.common.PageForm;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.examination.constant.MarkStatusConstant;
import com.qtrmoon.toolkit.DateTransfer;

	/** 练习Object类 */
	public class Exer extends PageForm{
	//Fields
	
	private Integer id;// 主键
	private String name;// 试卷名称
	private String type;// 类型 1-练习，2-问卷
	private String shortcut;// 分类目录
	private String department;// 教研室
	private String teachers;// 被分配的阅卷老师格式如：（,id,id,）
	private String teacher;// 发起教师
	private String bankId;// 题库编号
	private String bankName;// 题库名
	private Date createTime;// 创建时间
	private String createTimeBeg,createTimeEnd;//时间条件的跨度查询属性
	private Date startTime;// 练习开始时间
	private String startTimeBeg,startTimeEnd;//时间条件的跨度查询属性
	private Date endTime;// 练习结束时间
	private String endTimeBeg,endTimeEnd;//时间条件的跨度查询属性
	private String expireTime;// 有效期
	private Integer totalNum;// 题数
	private Integer totalScore;// 总分值
	private Integer passScore;// 及格分值
	private Integer showStandard;// 是否显示标准答案
	private Integer canRepeat;// 是否可重复做题
	private String remark;// 备注
	private Integer bookId;// 课本
	private String addType;// 添加类型  (class: AddTypeConstant.java)
	private String isSave;// 是否保存 (是否保存)
	private String typeSortStr;// 题目排序字符串
	private String bankIdsStr;// 题库ID排序字符串
	private String numParamStr;// 数量参数字符串
	private String answerReq;// 答题要求
	
	private String number;//学号
	private String statusReady;//答题状态
	
	private Date serverTime;//服务器时间
	private Integer[] topicIds; //题目ids(手动组卷用)
	private Map<String,Object> numParam;// 题目数量（自动组卷用）
	private String[] typeSort;// 题目排序（自动组卷用）
	private String[] bankIds;// 题库ids（自动组卷用）
	
	private Integer markProgress;// 阅卷进度
	private String markStatus;// 阅卷状态（页面检索用）
	
	public Integer getMarkProgress() {
		return markProgress;
	}
	public void setMarkProgress(Integer markProgress) {
		this.markProgress = markProgress;
	}
	/** default constructor */
	public Exer() {
	
	}	
	//getter and setter
	/** 获取主键 */
	public Integer getId() {
		return this.id;
	}
	public Date getServerTime() {
		return new Date();
	}

	public void setServerTime(Date serverTime) {
		this.serverTime = new Date();
	}

	/** 设置主键 */
	public void setId(Integer id) {
		this.id = id;
	}
	/** 获取试卷名称 */
	public String getName() {
		return this.name;
	}
	/** 设置试卷名称 */
	public void setName(String name) {
		this.name = name;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	/** 获取分类目录 */
	public String getShortcut() {
		return this.shortcut;
	}
	/** 设置分类目录 */
	public void setShortcut(String shortcut) {
		this.shortcut = shortcut;
	}
	/** 获取教研室 */
	public String getDepartment() {
		return this.department;
	}
	/** 设置教研室 */
	public void setDepartment(String department) {
		this.department = department;
	}
	/** 获取被分配老师 */
	public String getTeachers() {
		return this.teachers;
	}
	/** 设置被分配老师 */
	public void setTeachers(String teachers) {
		this.teachers = teachers;
	}
	/** 获取发起教师 */
	public String getTeacher() {
		return this.teacher;
	}
	/** 设置发起教师 */
	public void setTeacher(String teacher) {
		this.teacher = teacher;
	}
	/** 获取题库编号 */
	public String getBankId() {
		return this.bankId;
	}
	/** 设置题库编号 */
	public void setBankId(String bankId) {
		this.bankId = bankId;
	}
	/** 获取题库名 */
	public String getBankName() {
		return this.bankName;
	}
	/** 设置题库名 */
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	/** 获取创建时间 */
	public Date getCreateTime() {
		return this.createTime;
	}
	/** 设置创建时间 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/** 设定[创建时间]时间转载字段值，页面提交时由springmvc自动装载 */
	public void setCreateTimestr(String tm) {
		this.createTime=_getTime(tm);
	}
	/** 获取[创建时间]时间起始条件 */
	public String getCreateTimeBeg() {
		return createTimeBeg;
	}
	/** 获取[创建时间]时间起始条件(Dao/Mapper查询时调用) */
	public Date getCreateTimeBegDate() {
		return _getBegDate(createTimeBeg);
	}
	/** 设置[创建时间]时间起始条件(表单提交时自动装载) */
	public void setCreateTimeBeg(String createTimeBeg) {
		this.createTimeBeg = createTimeBeg;
	}
	
	/** 获取[创建时间]时间结束条件 */
	public String getCreateTimeEnd() {
		return createTimeEnd;
	}
	/** 获取[创建时间]时间结束条件(Dao/Mapper查询时调用) */
	public Date getCreateTimeEndDate() {
		return _getEndDate(createTimeEnd);
	}
	/** 设置[创建时间]时间结束条件(表单提交时自动装载) */
	public void setCreateTimeEnd(String createTimeEnd) {
		this.createTimeEnd = createTimeEnd;
	}
	/** 获取练习开始时间 */
	public Date getStartTime() {
		return this.startTime;
	}
	/** 设置练习开始时间 */
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	/** 设定[练习开始时间]时间转载字段值，页面提交时由springmvc自动装载 */
	public void setStartTimestr(String tm) {
		this.startTime=_getTime(tm);
	}
	/** 获取[练习开始时间]时间起始条件 */
	public String getStartTimeBeg() {
		return startTimeBeg;
	}
	/** 获取[练习开始时间]时间起始条件(Dao/Mapper查询时调用) */
	public Date getStartTimeBegDate() {
		return _getBegDate(startTimeBeg);
	}
	/** 设置[练习开始时间]时间起始条件(表单提交时自动装载) */
	public void setStartTimeBeg(String startTimeBeg) {
		this.startTimeBeg = startTimeBeg;
	}
	
	/** 获取[练习开始时间]时间结束条件 */
	public String getStartTimeEnd() {
		return startTimeEnd;
	}
	/** 获取[练习开始时间]时间结束条件(Dao/Mapper查询时调用) */
	public Date getStartTimeEndDate() {
		return _getEndDate(startTimeEnd);
	}
	/** 设置[练习开始时间]时间结束条件(表单提交时自动装载) */
	public void setStartTimeEnd(String startTimeEnd) {
		this.startTimeEnd = startTimeEnd;
	}
	/** 获取练习结束时间 */
	public Date getEndTime() {
		return this.endTime;
	}
	/** 设置练习结束时间 */
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	/** 设定[练习结束时间]时间转载字段值，页面提交时由springmvc自动装载 */
	public void setEndTimestr(String tm) {
		this.endTime=_getTime(tm);
	}
	/** 获取[练习结束时间]时间起始条件 */
	public String getEndTimeBeg() {
		return endTimeBeg;
	}
	/** 获取[练习结束时间]时间起始条件(Dao/Mapper查询时调用) */
	public Date getEndTimeBegDate() {
		return _getBegDate(endTimeBeg);
	}
	/** 设置[练习结束时间]时间起始条件(表单提交时自动装载) */
	public void setEndTimeBeg(String endTimeBeg) {
		this.endTimeBeg = endTimeBeg;
	}
	
	/** 获取[练习结束时间]时间结束条件 */
	public String getEndTimeEnd() {
		return endTimeEnd;
	}
	/** 获取[练习结束时间]时间结束条件(Dao/Mapper查询时调用) */
	public Date getEndTimeEndDate() {
		return _getEndDate(endTimeEnd);
	}
	/** 设置[练习结束时间]时间结束条件(表单提交时自动装载) */
	public void setEndTimeEnd(String endTimeEnd) {
		this.endTimeEnd = endTimeEnd;
	}
	/** 获取有效期 */
	public String getExpireTime() {
		return this.expireTime;
	}
	/** 设置有效期 */
	public void setExpireTime(String expireTime) {
		this.expireTime = expireTime;
	}
	/** 获取题数 */
	public Integer getTotalNum() {
		return this.totalNum;
	}
	/** 设置题数 */
	public void setTotalNum(Integer totalNum) {
		this.totalNum = totalNum;
	}
	/** 获取总分值 */
	public Integer getTotalScore() {
		return this.totalScore;
	}
	/** 设置总分值 */
	public void setTotalScore(Integer totalScore) {
		this.totalScore = totalScore;
	}
	/** 获取及格分值 */
	public Integer getPassScore() {
		return this.passScore;
	}
	/** 设置及格分值 */
	public void setPassScore(Integer passScore) {
		this.passScore = passScore;
	}
	/** 获取是否显示标准答案 */
	public Integer getShowStandard() {
		return this.showStandard;
	}
	/** 设置是否显示标准答案 */
	public void setShowStandard(Integer showStandard) {
		this.showStandard = showStandard;
	}
	/** 获取是否可重复做题 */
	public Integer getCanRepeat() {
		return this.canRepeat;
	}
	/** 设置是否可重复做题 */
	public void setCanRepeat(Integer canRepeat) {
		this.canRepeat = canRepeat;
	}
	/** 获取备注 */
	public String getRemark() {
		return this.remark;
	}
	/** 设置备注 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取课本id
	 * @return
	 */
	public Integer getBookId() {
		return bookId;
	}
	/**
	 * 设置课本id
	 * @param bookId
	 */
	public void setBookId(Integer bookId) {
		this.bookId = bookId;
	}
	/**
	 * 获取学号
	 * @return
	 */
	public String getNumber() {
		return number;
	}
	/**
	 * 设置学号
	 * @param number
	 */
	public void setNumber(String number) {
		this.number = number;
	}
	/**
	 * 获取答题状态
	 * @return
	 */
	public String getStatusReady() {
		return statusReady;
	}
	/**
	 * 设置答题状态
	 * @param statusReady
	 */
	public void setStatusReady(String statusReady) {
		this.statusReady = statusReady;
	}
	
	/**
	 * 添加类型
	 * @return
	 */
	public String getAddType() {
		return addType;
	}
	
	/**
	 * 添加类型
	 */
	public void setAddType(String addType) {
		this.addType = addType;
	}
	
	/**
	 * 是否保存
	 * @return
	 */
	public String getIsSave() {
		return isSave;
	}
	
	/**
	 * 是否保存
	 */
	public void setIsSave(String isSave) {
		this.isSave = isSave;
	}
	
	public Integer[] getTopicIds() {
		return topicIds;
	}
	public void setTopicIds(Integer[] topicIds) {
		this.topicIds = topicIds;
	}
	
	public Map<String, Object> getNumParam() {
		return numParam;
	}
	public void setNumParam(Map<String, Object> numParam) {
		this.numParam = numParam;
	}
	
	public String[] getTypeSort() {
		return typeSort;
	}
	public void setTypeSort(String[] typeSort) {
		this.typeSort = typeSort;
	}
	public String[] getBankIds() {
		return bankIds;
	}
	public void setBankIds(String[] bankIds) {
		this.bankIds = bankIds;
	}
	public String getTypeSortStr() {
		return typeSortStr;
	}
	public void setTypeSortStr(String typeSortStr) {
		this.typeSortStr = typeSortStr;
	}
	public String getBankIdsStr() {
		return bankIdsStr;
	}
	public void setBankIdsStr(String bankIdsStr) {
		this.bankIdsStr = bankIdsStr;
	}
	
	public String getNumParamStr() {
		return numParamStr;
	}
	public void setNumParamStr(String numParamStr) {
		this.numParamStr = numParamStr;
	}
	
	public String getMarkStatus() {
		return markStatus;
	}
	public void setMarkStatus(String markStatus) {
		this.markStatus = markStatus;
	}
	
	public String getAnswerReq() {
		return answerReq;
	}
	public void setAnswerReq(String answerReq) {
		this.answerReq = answerReq;
	}
	
	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("name", name);
		obj.put("shortcut", shortcut);
		obj.put("department", department);
		obj.put("teacher", teacher);
		obj.put("bankId", bankId);
		obj.put("bankName", bankName);
		//时间字段处理。createTime查询列表页使用，createTimestr修改页使用
		obj.put("createTime", DateTransfer.toString(createTime,"yyyy-MM-dd HH:mm:ss"));
		obj.put("createTimestr", DateTransfer.toString(createTime,"yyyy-MM-dd HH:mm:ss"));
		//时间字段处理。startTime查询列表页使用，startTimestr修改页使用
		obj.put("startTime", DateTransfer.toString(startTime,"yyyy-MM-dd HH:mm:ss"));
		obj.put("startTimestr", DateTransfer.toString(startTime,"yyyy-MM-dd HH:mm:ss"));
		//时间字段处理。endTime查询列表页使用，endTimestr修改页使用
		obj.put("endTime", DateTransfer.toString(endTime,"yyyy-MM-dd HH:mm:ss"));
		obj.put("endTimestr", DateTransfer.toString(endTime,"yyyy-MM-dd HH:mm:ss"));
		obj.put("serverTime", DateTransfer.toString(new Date(),"yyyy-MM-dd HH:mm:ss"));
		obj.put("expireTime", expireTime);
		obj.put("totalNum", totalNum);
		obj.put("totalScore", totalScore);
		obj.put("passScore", passScore);
		obj.put("showStandard", showStandard);
		//obj.put("canRepeat", canRepeat);
		obj.put("remark", remark);
		obj.put("bookId", bookId);
		obj.put("number", number);
		obj.put("addType", addType);
		obj.put("isSave", isSave);
		obj.put("typeSort", typeSort);
		obj.put("bankIds", bankIds);
		obj.put("typeSortStr", typeSortStr);
		obj.put("bankIdsStr", bankIdsStr);
		obj.put("numParam", numParam);
		obj.put("numParamStr", numParamStr);
		obj.put("statusReady", statusReady);
		obj.put("markProgress", markProgress);
		obj.put("answerReq", answerReq);
		return obj;
	}
	
	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("name", name);
		if(shortcut != null && !"".equals(shortcut)){
			obj.put("shortcut", DictBuffer.getLabel("ZD_EXAM_CATEGORY", shortcut.toString()));
		}
		obj.put("department", department);
		obj.put("teacher", teacher);
		
		if(teacher != null && !"".equals(teacher)){
			obj.put("teacherName", DictBuffer.getLabel("ZD_TEACHER", teacher.toString()));
		}else{
			obj.put("teacherName", "");
		}
		
		obj.put("bankId", bankId);
		obj.put("bankName", bankName);
		//时间字段处理。createTime查询列表页使用，createTimestr修改页使用
		obj.put("createTime", DateTransfer.toString(createTime,"yyyy-MM-dd HH:mm:ss"));
		obj.put("createTimestr", DateTransfer.toString(createTime,"yyyy-MM-dd HH:mm:ss"));
		//时间字段处理。startTime查询列表页使用，startTimestr修改页使用
		obj.put("startTime", DateTransfer.toString(startTime,"yyyy-MM-dd HH:mm:ss"));
		obj.put("start_time", DateTransfer.toString(startTime,"yyyy-MM-dd HH:mm:ss"));
		obj.put("startTimestr", DateTransfer.toString(startTime,"yyyy-MM-dd HH:mm:ss"));
		//时间字段处理。endTime查询列表页使用，endTimestr修改页使用
		obj.put("endTime", DateTransfer.toString(endTime,"yyyy-MM-dd HH:mm:ss"));
		obj.put("end_time", DateTransfer.toString(endTime,"yyyy-MM-dd HH:mm:ss"));
		obj.put("endTimestr", DateTransfer.toString(endTime,"yyyy-MM-dd HH:mm:ss"));
		obj.put("serverTime", DateTransfer.toString(new Date(),"yyyy-MM-dd HH:mm:ss"));
		obj.put("expireTime", expireTime);
		obj.put("totalNum", totalNum);
		obj.put("totalScore", totalScore);
		obj.put("passScore", passScore);
		if(showStandard != null){
			obj.put("showStandard", DictBuffer.getLabel("ZD_SFBZ", showStandard.toString()));
		}
		//obj.put("canRepeat", DictBuffer.getLabel("ZD_SFBZ", canRepeat.toString()));
		obj.put("remark", remark);
		obj.put("bookId", bookId);
		obj.put("number", number);
		obj.put("addType", addType);
		if(addType != null){
			obj.put("addTypeName", DictBuffer.getLabel("ZD_EXER_ADDTYPE", addType.toString()));
		}
		obj.put("isSave", isSave);
		obj.put("typeSort", typeSort);
		obj.put("bankIds", bankIds);
		obj.put("typeSortStr", typeSortStr);
		obj.put("bankIdsStr", bankIdsStr);
		obj.put("numParamStr", numParamStr);
		obj.put("statusReady", statusReady);
		if(markProgress != null){
			if(markProgress == 0){
				obj.put("markProgress", DictBuffer.getLabel("ZD_MARK_STATUS", MarkStatusConstant.MARK_NO.toString()));
			}
			if(markProgress > 0 && markProgress < 100){
				obj.put("markProgress", DictBuffer.getLabel("ZD_MARK_STATUS", MarkStatusConstant.MARK_ING.toString()));
			}
			if(markProgress == 100){
				obj.put("markProgress", DictBuffer.getLabel("ZD_MARK_STATUS", MarkStatusConstant.MARK_FINISH.toString()));
			}
		}else{
			obj.put("markProgress", DictBuffer.getLabel("ZD_MARK_STATUS", MarkStatusConstant.MARK_NO.toString()));
		}
		obj.put("answerReq", answerReq);
		return obj;
	}
}
