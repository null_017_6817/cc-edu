package com.qtrmoon.examination.pojo;
import java.util.Date;

import org.json.simple.JSONObject;

import com.qtrmoon.common.PageForm;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.examination.util.TopicTypeTransUtil;
import com.qtrmoon.toolkit.DateTransfer;

/** 习题Object类 */
public class ExerTopic extends PageForm{
	
	private Integer id;// 主键
	private Integer eid;// 练习编号
	private Integer orderNo;// 在练习中的序号
	private String bankId;// 题库编号
	private String topicId;// 考题编号
	private String remark;// 备注
	private String title;// 标题
	private String type;// 类型
	private String level;// 级别
	private String profession;// 行业
	private String difficulty;// 难度
	private String author;// 作者
	private Date creattime;// 时间
	private String creattimeBeg,creattimeEnd;//时间条件的跨度查询属性
	private Integer mergeVoice;// 合并录音
	private Integer totalScore;// 总分
	private Integer passScore;// 及格分
	private String beginStepId;// 开始步骤
	private Date createTime;// 创建时间
	private String createTimeBeg,createTimeEnd;//时间条件的跨度查询属性
	private Integer status;// 状态
	private String docATitle;// 文档A标题
	private String docBTitle;// 文档B标题
	private String docCTitle;// 文档C标题
	private String docDTitle;// 文档D标题
	private String docETitle;// 文档E标题
	private String docAText;// 文档A答案
	private String docBText;// 文档B答案
	private String docCText;// 文档C答案
	private String docDText;// 文档D答案
	private String docEText;// 文档E答案
	private String customer;// 文档E答案
	private String keyWords;// 关键字
	private String bookmodul;// 能力类型
	private String capability;// 能力类型
	private String talkSkill;// 话术
	private Integer statusReady;//答题状态
	
	private String studid;// 学生id
	private String stepId;// 步骤id
	private String bookId;// 课本
	private Integer templateId; //评分模板ID
	private Integer hasDone; // 已做标志
	private String hasDo; // 已做标识
	
	/** default constructor */
	public ExerTopic() {
	
	}	
	//getter and setter
	/** 获取主键 */
	public Integer getId() {
		return this.id;
	}
	/** 设置主键 */
	public void setId(Integer id) {
		this.id = id;
	}
	/** 获取练习编号 */
	public Integer getEid() {
		return this.eid;
	}
	/** 设置练习编号 */
	public void setEid(Integer eid) {
		this.eid = eid;
	}
	/** 获取在练习中的序号 */
	public Integer getOrderNo() {
		return this.orderNo;
	}
	/** 设置在练习中的序号 */
	public void setOrderNo(Integer orderNo) {
		this.orderNo = orderNo;
	}
	/** 获取题库编号 */
	public String getBankId() {
		return this.bankId;
	}
	/** 设置题库编号 */
	public void setBankId(String bankId) {
		this.bankId = bankId;
	}
	/** 获取考题编号 */
	public String getTopicId() {
		return this.topicId;
	}
	/** 设置考题编号 */
	public void setTopicId(String topicId) {
		this.topicId = topicId;
	}
	/** 获取备注 */
	public String getRemark() {
		return this.remark;
	}
	/** 设置备注 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/** 获取标题 */
	public String getTitle() {
		return this.title;
	}
	/** 设置标题 */
	public void setTitle(String title) {
		this.title = title;
	}
	/** 获取类型 */
	public String getType() {
		return this.type;
	}
	/** 设置类型 */
	public void setType(String type) {
		this.type = type;
	}
	/** 获取级别 */
	public String getLevel() {
		return this.level;
	}
	/** 设置级别 */
	public void setLevel(String level) {
		this.level = level;
	}
	/** 获取行业 */
	public String getProfession() {
		return this.profession;
	}
	/** 设置行业 */
	public void setProfession(String profession) {
		this.profession = profession;
	}
	/** 获取难度 */
	public String getDifficulty() {
		return this.difficulty;
	}
	/** 设置难度 */
	public void setDifficulty(String difficulty) {
		this.difficulty = difficulty;
	}
	/** 获取作者 */
	public String getAuthor() {
		return this.author;
	}
	/** 设置作者 */
	public void setAuthor(String author) {
		this.author = author;
	}
	/** 获取时间 */
	public Date getCreattime() {
		return this.creattime;
	}
	/** 设置时间 */
	public void setCreattime(Date creattime) {
		this.creattime = creattime;
	}
	/** 设定[时间]时间转载字段值，页面提交时由springmvc自动装载 */
	public void setCreattimestr(String tm) {
		this.creattime=_getTime(tm);
	}
	/** 获取[时间]时间起始条件 */
	public String getCreattimeBeg() {
		return creattimeBeg;
	}
	/** 获取[时间]时间起始条件(Dao/Mapper查询时调用) */
	public Date getCreattimeBegDate() {
		return _getBegDate(creattimeBeg);
	}
	/** 设置[时间]时间起始条件(表单提交时自动装载) */
	public void setCreattimeBeg(String creattimeBeg) {
		this.creattimeBeg = creattimeBeg;
	}
	
	/** 获取[时间]时间结束条件 */
	public String getCreattimeEnd() {
		return creattimeEnd;
	}
	/** 获取[时间]时间结束条件(Dao/Mapper查询时调用) */
	public Date getCreattimeEndDate() {
		return _getEndDate(creattimeEnd);
	}
	/** 设置[时间]时间结束条件(表单提交时自动装载) */
	public void setCreattimeEnd(String creattimeEnd) {
		this.creattimeEnd = creattimeEnd;
	}
	/** 获取合并录音 */
	public Integer getMergeVoice() {
		return this.mergeVoice;
	}
	/** 设置合并录音 */
	public void setMergeVoice(Integer mergeVoice) {
		this.mergeVoice = mergeVoice;
	}
	/** 获取总分 */
	public Integer getTotalScore() {
		return this.totalScore;
	}
	/** 设置总分 */
	public void setTotalScore(Integer totalScore) {
		this.totalScore = totalScore;
	}
	/** 获取及格分 */
	public Integer getPassScore() {
		return this.passScore;
	}
	/** 设置及格分 */
	public void setPassScore(Integer passScore) {
		this.passScore = passScore;
	}
	/** 获取开始步骤 */
	public String getBeginStepId() {
		return this.beginStepId;
	}
	/** 设置开始步骤 */
	public void setBeginStepId(String beginStepId) {
		this.beginStepId = beginStepId;
	}
	/** 获取创建时间 */
	public Date getCreateTime() {
		return this.createTime;
	}
	/** 设置创建时间 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/** 设定[创建时间]时间转载字段值，页面提交时由springmvc自动装载 */
	public void setCreateTimestr(String tm) {
		this.createTime=_getTime(tm);
	}
	/** 获取[创建时间]时间起始条件 */
	public String getCreateTimeBeg() {
		return createTimeBeg;
	}
	/** 获取[创建时间]时间起始条件(Dao/Mapper查询时调用) */
	public Date getCreateTimeBegDate() {
		return _getBegDate(createTimeBeg);
	}
	/** 设置[创建时间]时间起始条件(表单提交时自动装载) */
	public void setCreateTimeBeg(String createTimeBeg) {
		this.createTimeBeg = createTimeBeg;
	}
	
	/** 获取[创建时间]时间结束条件 */
	public String getCreateTimeEnd() {
		return createTimeEnd;
	}
	/** 获取[创建时间]时间结束条件(Dao/Mapper查询时调用) */
	public Date getCreateTimeEndDate() {
		return _getEndDate(createTimeEnd);
	}
	/** 设置[创建时间]时间结束条件(表单提交时自动装载) */
	public void setCreateTimeEnd(String createTimeEnd) {
		this.createTimeEnd = createTimeEnd;
	}
	/** 获取状态 */
	public Integer getStatus() {
		return this.status;
	}
	/** 设置状态 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/** 获取文档A标题 */
	public String getDocATitle() {
		return this.docATitle;
	}
	/** 设置文档A标题 */
	public void setDocATitle(String docATitle) {
		this.docATitle = docATitle;
	}
	/** 获取文档B标题 */
	public String getDocBTitle() {
		return this.docBTitle;
	}
	/** 设置文档B标题 */
	public void setDocBTitle(String docBTitle) {
		this.docBTitle = docBTitle;
	}
	/** 获取文档C标题 */
	public String getDocCTitle() {
		return this.docCTitle;
	}
	/** 设置文档C标题 */
	public void setDocCTitle(String docCTitle) {
		this.docCTitle = docCTitle;
	}
	/** 获取文档D标题 */
	public String getDocDTitle() {
		return this.docDTitle;
	}
	/** 设置文档D标题 */
	public void setDocDTitle(String docDTitle) {
		this.docDTitle = docDTitle;
	}
	/** 获取文档E标题 */
	public String getDocETitle() {
		return this.docETitle;
	}
	/** 设置文档E标题 */
	public void setDocETitle(String docETitle) {
		this.docETitle = docETitle;
	}
	/** 获取文档A答案 */
	public String getDocAText() {
		return this.docAText;
	}
	/** 设置文档A答案 */
	public void setDocAText(String docAText) {
		this.docAText = docAText;
	}
	/** 获取文档B答案 */
	public String getDocBText() {
		return this.docBText;
	}
	/** 设置文档B答案 */
	public void setDocBText(String docBText) {
		this.docBText = docBText;
	}
	/** 获取文档C答案 */
	public String getDocCText() {
		return this.docCText;
	}
	/** 设置文档C答案 */
	public void setDocCText(String docCText) {
		this.docCText = docCText;
	}
	/** 获取文档D答案 */
	public String getDocDText() {
		return this.docDText;
	}
	/** 设置文档D答案 */
	public void setDocDText(String docDText) {
		this.docDText = docDText;
	}
	/** 获取文档E答案 */
	public String getDocEText() {
		return this.docEText;
	}
	/** 设置文档E答案 */
	public void setDocEText(String docEText) {
		this.docEText = docEText;
	}
	/**
	 * 获取自定义内容
	 * @return
	 */
	public String getCustomer() {
		return customer;
	}
	/**
	 * 设置自定义内容
	 * @param customer
	 */
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	/**
	 * 获取关键字
	 * @return
	 */
	public String getKeyWords() {
		return keyWords;
	}
	/**
	 * 设置关键字
	 * @param keyWords
	 */
	public void setKeyWords(String keyWords) {
		this.keyWords = keyWords;
	}
	/**
	 * 获取课本模块
	 * @return
	 */
	public String getBookmodul() {
		return bookmodul;
	}
	/**
	 * 设置课本模块
	 * @param bookmodul
	 */
	public void setBookmodul(String bookmodul) {
		this.bookmodul = bookmodul;
	}
	/**
	 * 获取能力
	 * @return
	 */
	public String getCapability() {
		return capability;
	}
	/**
	 * 设置能力
	 * @param capability
	 */
	public void setCapability(String capability) {
		this.capability = capability;
	}
	/**
	 * 获取话术
	 * @return
	 */
	public String getTalkSkill() {
		return talkSkill;
	}
	/**
	 * 设置话术
	 * @param talkSkill
	 */
	public void setTalkSkill(String talkSkill) {
		this.talkSkill = talkSkill;
	}
	/**
	 * 获取答题状态
	 * @return
	 */
	public Integer getStatusReady() {
		return statusReady;
	}
	/**
	 * 设置答题状态
	 * @param statusReady
	 */
	public void setStatusReady(Integer statusReady) {
		this.statusReady = statusReady;
	}
	/**
	 * 获取学生id
	 * @return
	 */
	public String getStudid() {
		return studid;
	}
	/**
	 * 设置学生id
	 * @param studid
	 */
	public void setStudid(String studid) {
		this.studid = studid;
	}
	/**
	 * 获取步骤id
	 * @return
	 */
	public String getStepId() {
		return stepId;
	}
	/**
	 * 设置步骤id
	 * @param stepId
	 */
	public void setStepId(String stepId) {
		this.stepId = stepId;
	}
	/**
	 * 获取课本id
	 * @return
	 */
	public String getBookId() {
		return bookId;
	}
	/**
	 * 设置课本id
	 * @param bookId
	 */
	public void setBookId(String bookId) {
		this.bookId = bookId;
	}
	
	/**
	 * 获取评分模板ID
	 */
	public Integer getTemplateId() {
		return templateId;
	}
	/**
	 * 设置评分模板ID
	 * @param templateId
	 */
	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}
	
	public Integer getHasDone() {
		return hasDone;
	}
	public void setHasDone(Integer hasDone) {
		this.hasDone = hasDone;
	}
	public String getHasDo() {
		return hasDo;
	}
	public void setHasDo(String hasDo) {
		this.hasDo = hasDo;
	}
	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("eid", eid);
		obj.put("orderNo", orderNo);
		obj.put("bankId", bankId);
		obj.put("topicId", topicId);
		obj.put("remark", remark);
		obj.put("title", title);
		obj.put("type", type);
		obj.put("level", level);
		obj.put("profession", profession);
		obj.put("keyWords", keyWords);
		obj.put("bookmodul", bookmodul);
		obj.put("difficulty", difficulty);
		obj.put("author", author);
		//时间字段处理。creattime查询列表页使用，creattimestr修改页使用
		obj.put("creattime", DateTransfer.toString(creattime,"yyyy-MM-dd HH:mm:ss"));
		obj.put("creattimestr", DateTransfer.toString(creattime,"yyyy-MM-dd HH:mm:ss"));
		obj.put("mergeVoice", mergeVoice);
		obj.put("customer", customer);
		obj.put("talkSkill", talkSkill);
		obj.put("totalScore", totalScore);
		obj.put("passScore", passScore);
		obj.put("beginStepId", beginStepId);
		//时间字段处理。createTime查询列表页使用，createTimestr修改页使用
		obj.put("createTime", DateTransfer.toString(createTime,"yyyy-MM-dd HH:mm:ss"));
		obj.put("createTimestr", DateTransfer.toString(createTime,"yyyy-MM-dd HH:mm:ss"));
		obj.put("status", status);
		obj.put("capability", capability);
		obj.put("docATitle", docATitle);
		obj.put("docBTitle", docBTitle);
		obj.put("docCTitle", docCTitle);
		obj.put("docDTitle", docDTitle);
		obj.put("docETitle", docETitle);
		obj.put("docAText", docAText);
		obj.put("docBText", docBText);
		obj.put("docCText", docCText);
		obj.put("docDText", docDText);
		obj.put("docEText", docEText);
		obj.put("statusReady", statusReady);
		obj.put("stepId", stepId);
		obj.put("bookId", bookId);
		obj.put("hasDo", hasDo);
		return obj;
	}
	
	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("eid", eid);
		obj.put("orderNo", orderNo);
		obj.put("bankId", bankId);
		obj.put("topicId", topicId);
		obj.put("remark", remark);
		obj.put("title", title);
		obj.put("type", type);
		if(type != null){
			obj.put("typeLabel",TopicTypeTransUtil.topicCodeTransLabel(type));
		}
		if(obj.get("typeLabel") == null || "".equals(obj.get("typeLabel"))){
			obj.put("typeLabel",type);
		}
		obj.put("level", level);
		obj.put("profession", profession);
		if(profession != null){
			obj.put("professionLabel", DictBuffer.getLabel("ZD_REPOSITORY", profession.toString()));
		}
		obj.put("keyWords", keyWords);
		obj.put("bookmodul", bookmodul);
		obj.put("difficulty", difficulty);
		if(difficulty != null){
			obj.put("difficultyLabel", DictBuffer.getLabel("ZD_DIFFICULT", difficulty.toString()));
		}
		obj.put("author", author);
		//时间字段处理。creattime查询列表页使用，creattimestr修改页使用
		obj.put("creattime", DateTransfer.toString(creattime,"yyyy-MM-dd HH:mm:ss"));
		obj.put("creattimestr", DateTransfer.toString(creattime,"yyyy-MM-dd HH:mm:ss"));
		obj.put("mergeVoice", mergeVoice);
		obj.put("customer", customer);
		obj.put("talkSkill", talkSkill);
		obj.put("totalScore", totalScore);
		obj.put("passScore", passScore);
		obj.put("beginStepId", beginStepId);
		//时间字段处理。createTime查询列表页使用，createTimestr修改页使用
		obj.put("createTime", DateTransfer.toString(createTime,"yyyy-MM-dd HH:mm:ss"));
		obj.put("createTimestr", DateTransfer.toString(createTime,"yyyy-MM-dd HH:mm:ss"));
		if(status!=null&&!status.equals("")){//字典id值转label
			obj.put("status", DictBuffer.getLabel("ZD_TESTSTATUS", status.toString()));
		}
		obj.put("capability", capability);
		obj.put("docATitle", docATitle);
		obj.put("docBTitle", docBTitle);
		obj.put("docCTitle", docCTitle);
		obj.put("docDTitle", docDTitle);
		obj.put("docETitle", docETitle);
		obj.put("docAText", docAText);
		obj.put("docBText", docBText);
		obj.put("docCText", docCText);
		obj.put("docDText", docDText);
		obj.put("docEText", docEText);
		if(statusReady!=null&&!statusReady.equals("")){//字典id值转label
			obj.put("statusReady", DictBuffer.getLabel("ZD_SFBZ", statusReady.toString()));
		}
		obj.put("stepId", stepId);
		obj.put("bookId", bookId);
		if(bookId != null && !"".equals(bookId)){
			obj.put("bookLabel", DictBuffer.getLabel("CC_BOOK", bookId.toString()));
		}
		obj.put("hasDo", hasDo);
		return obj;
	}
	
}
