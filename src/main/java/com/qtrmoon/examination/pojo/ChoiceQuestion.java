package com.qtrmoon.examination.pojo;
/**
 * 选泽题
 * @author HuaTang_DYC
 *
 */
public class ChoiceQuestion {
	private String id;//主键
	private String audio;//录音路径
	private String audioHid;// 只存录音名（页面放hidden值用）
	private String context;//正文
	private String next;//下一步
	/**
	 * 获取主键
	 * @return
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置主键
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取录音路径
	 * @return
	 */
	public String getAudio() {
		return audio;
	}
	/**
	 * 设置录音路径
	 * @param audio
	 */
	public void setAudio(String audio) {
		this.audio = audio;
	}
	/**
	 * 获取正文
	 * @return
	 */
	public String getContext() {
		return context;
	}
	/**
	 * 设置正文
	 * @param context
	 */
	public void setContext(String context) {
		this.context = context;
	}
	/**
	 * 获取下一步
	 * @return
	 */
	public String getNext() {
		return next;
	}
	/**
	 * 设置下一步
	 * @param next
	 */
	public void setNext(String next) {
		this.next = next;
	}
	/**
	 * 获取录音文件名
	 * @return
	 */
	public String getAudioHid() {
		return audioHid;
	}
	/**
	 * 设置录音文件名
	 * @param audioHid
	 */
	public void setAudioHid(String audioHid) {
		this.audioHid = audioHid;
	}
}
