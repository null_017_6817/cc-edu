package com.qtrmoon.examination.pojo;
/**
 * 录音题
 */
public class Prompt {
	private String audio;//录音路径
	private String audioHid;// 只存录音名（页面放hidden值用）
	private String repeat;//重复
	private String context;//正文
	private String origiName;// 实操题压缩包原文件名称
	private String fileName;// 实操题压缩包系统中的名称
	
	
	/** 无参构造方法 */
	public Prompt() {
	}
	
	/** 有参构造方法 */
	public Prompt(String context, String audio,String repeat,String audioHid
			,String origiName, String fileName) {
		this.context=context;
		this.audio=audio;
		this.audioHid=audioHid;
		this.repeat=repeat;
		this.origiName = origiName;
		this.fileName = fileName;
	}
	/**
	 * 获取录音
	 * @return
	 */
	public String getAudio() {
		return audio;
	}
	/**
	 * 设置录音
	 * @param audio
	 */
	public void setAudio(String audio) {
		this.audio = audio;
	}
	/**
	 * 获取重复
	 * @return
	 */
	public String getRepeat() {
		return repeat;
	}
	/**
	 * 设置重复
	 * @param repeat
	 */
	public void setRepeat(String repeat) {
		this.repeat = repeat;
	}
	/**
	 * 获取正文
	 * @return
	 */
	public String getContext() {
		return context;
	}
	/**
	 * 设置正文
	 * @param context
	 */
	public void setContext(String context) {
		this.context = context;
	}
	/**
	 * 获取录音名
	 * @return
	 */
	public String getAudioHid() {
		return audioHid;
	}
	/**
	 * 设置录音名
	 * @param audioHid
	 */
	public void setAudioHid(String audioHid) {
		this.audioHid = audioHid;
	}

	public String getOrigiName() {
		return origiName;
	}

	public void setOrigiName(String origiName) {
		this.origiName = origiName;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
