package com.qtrmoon.examination.pojo;
import org.json.simple.JSONObject;

import com.qtrmoon.common.PageForm;
/** 答题步骤 Object类 */
public class ExerStep extends PageForm{
	
	private Integer id;// 主键
	private Integer eid;// 练习编号
	private Integer etid;// 考题编号
	private String step;// 步骤标识
	private String type;// 步骤类型
	private String title;// 标题
	private String score;// 分值
	private String stepinfo;// 步骤内容
	private String standardAnswer;// 标准答案
	private Integer orderNo;//排序编号
	//Constructors
	/** default constructor */
	public ExerStep() {
	
	}	
	//getter and setter
	/** 获取步骤内容*/
	public String getStepinfo() {
		return stepinfo;
	}
	/** 设置步骤内容*/
	public void setStepinfo(String stepinfo) {
		this.stepinfo = stepinfo;
	}
	/** 获取标准答案 */
	public String getStandardAnswer() {
		return standardAnswer;
	}
	/** 设置标准答案 */
	public void setStandardAnswer(String standardAnswer) {
		this.standardAnswer = standardAnswer;
	}
	/** 获取主键 */
	public Integer getId() {
		return this.id;
	}
	/** 设置主键 */
	public void setId(Integer id) {
		this.id = id;
	}
	/** 获取练习编号 */
	public Integer getEid() {
		return this.eid;
	}
	/** 设置练习编号 */
	public void setEid(Integer eid) {
		this.eid = eid;
	}
	/** 获取考题编号 */
	public Integer getEtid() {
		return this.etid;
	}
	/** 设置考题编号 */
	public void setEtid(Integer etid) {
		this.etid = etid;
	}
	/** 获取步骤标识 */
	public String getStep() {
		return this.step;
	}
	/** 设置步骤标识 */
	public void setStep(String step) {
		this.step = step;
	}
	/** 获取步骤类型 */
	public String getType() {
		return this.type;
	}
	/** 设置步骤类型 */
	public void setType(String type) {
		this.type = type;
	}
	/** 获取标题 */
	public String getTitle() {
		return this.title;
	}
	/** 设置标题 */
	public void setTitle(String title) {
		this.title = title;
	}
	/** 获取分值 */
	public String getScore() {
		return this.score;
	}
	/** 设置分值 */
	public void setScore(String score) {
		this.score = score;
	}
	/**
	 * 获取顺序
	 * @return
	 */
	public Integer getOrderNo() {
		return orderNo;
	}
	/**
	 * 设置顺序
	 * @param orderNo
	 */
	public void setOrderNo(Integer orderNo) {
		this.orderNo = orderNo;
	}
	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("eid", eid);
		obj.put("etid", etid);
		obj.put("step", step);
		obj.put("type", type);
		obj.put("title", title);
		obj.put("score", score);
		obj.put("stepinfo", stepinfo);
		obj.put("standardAnswer", standardAnswer);
		return obj;
	}
	
	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("eid", eid);
		obj.put("etid", etid);
		obj.put("step", step);
		obj.put("type", type);
		obj.put("title", title);
		obj.put("score", score);
		obj.put("stepinfo", stepinfo);
		obj.put("standardAnswer", standardAnswer);
		return obj;
	}
}
