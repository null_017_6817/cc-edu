package com.qtrmoon.examination.pojo;

import java.util.Date;

import org.json.simple.JSONObject;

import com.qtrmoon.common.PageForm;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.toolkit.DateTransfer;

/** 练习与班级关系表Object类 */
public class RExerStud extends PageForm {

	private Integer id; // 主键
	private Integer eid; // 练习编号
	private String studid; // 学生
	private String classid; // 班级
	private Integer type; // 类型
	private String name; // 标题
	private String markTeacher; // 阅卷老师
	private Integer status; // 阅卷状态
	private Float mark; // 得分
	private String remark; // 备注
	private Integer statusReady; // 是否答完(0:否,1:是)
	private Date createTime; // 创建时间
	private Float subjective; // 主观题
	private Float objective; // 主观题
	private Float markProgress; // 阅卷进度
	private String markStatus;// 阅卷状态（页面检索用）

	private String studName;// 学生姓名（页面检索用）
	private String classNum;// 班级号（页面检索用）
	
	
	/** default constructor */
	public RExerStud() {

	}

	// getter and setter
	/** 获取主键 */
	public Integer getId() {
		return this.id;
	}

	/** 设置主键 */
	public void setId(Integer id) {
		this.id = id;
	}

	/** 获取练习编号 */
	public Integer getEid() {
		return this.eid;
	}

	/** 设置练习编号 */
	public void setEid(Integer eid) {
		this.eid = eid;
	}

	/** 获取学生 */
	public String getStudid() {
		return this.studid;
	}

	/** 设置学生 */
	public void setStudid(String studid) {
		this.studid = studid;
	}

	/** 获取班级 */
	public String getClassid() {
		return this.classid;
	}

	/** 设置班级 */
	public void setClassid(String classid) {
		this.classid = classid;
	}

	/** 获取类型 */
	public Integer getType() {
		return this.type;
	}

	/** 设置类型 */
	public void setType(Integer type) {
		this.type = type;
	}

	/** 获取标题 */
	public String getName() {
		return this.name;
	}

	/** 设置标题 */
	public void setName(String name) {
		this.name = name;
	}

	/** 获取阅卷老师 */
	public String getMarkTeacher() {
		return this.markTeacher;
	}

	/** 设置阅卷老师 */
	public void setMarkTeacher(String markTeacher) {
		this.markTeacher = markTeacher;
	}

	/** 获取阅卷状态 */
	public Integer getStatus() {
		return this.status;
	}

	/** 设置阅卷状态 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/** 获取得分 */
	public Float getMark() {
		return this.mark;
	}

	/** 设置得分 */
	public void setMark(Float mark) {
		this.mark = mark;
	}

	/** 获取备注 */
	public String getRemark() {
		return this.remark;
	}

	/** 设置备注 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * 获取答题状态
	 * 
	 * @return
	 */
	public Integer getStatusReady() {
		return statusReady;
	}

	/**
	 * 设置答题状态
	 * 
	 * @param statusReady
	 */
	public void setStatusReady(Integer statusReady) {
		this.statusReady = statusReady;
	}

	/**
	 * 获取创建时间
	 * 
	 * @return
	 */
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * 设置创建时间
	 * 
	 * @param createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * 获取主观题
	 * 
	 * @return
	 */
	public Float getSubjective() {
		return subjective;
	}

	/**
	 * 设置主观题
	 * 
	 * @param subjective
	 */
	public void setSubjective(Float subjective) {
		this.subjective = subjective;
	}

	/**
	 * 获取客观题
	 * 
	 * @return
	 */
	public Float getObjective() {
		return objective;
	}

	/**
	 * 设置客观题
	 * 
	 * @param objective
	 */
	public void setObjective(Float objective) {
		this.objective = objective;
	}

	
	public Float getMarkProgress() {
		return markProgress;
	}

	public void setMarkProgress(Float markProgress) {
		this.markProgress = markProgress;
	}

	public String getMarkStatus() {
		return markStatus;
	}

	public void setMarkStatus(String markStatus) {
		this.markStatus = markStatus;
	}

	public String getStudName() {
		return studName;
	}

	public void setStudName(String studName) {
		this.studName = studName;
	}

	public String getClassNum() {
		return classNum;
	}

	public void setClassNum(String classNum) {
		this.classNum = classNum;
	}

	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj = new JSONObject();
		obj.put("id", id);
		obj.put("eid", eid);
		obj.put("studid", studid);
		obj.put("classid", classid);
		obj.put("type", type);
		obj.put("name", name);
		obj.put("markTeacher", markTeacher);
		obj.put("status", status);
		obj.put("mark", mark);
		obj.put("remark", remark);
		obj.put("statusReady", statusReady);
		obj.put("createTime", createTime);
		obj.put("subjective", subjective);
		obj.put("objective", objective);
		obj.put("markProgress", markProgress);
		obj.put("studName", studName);
		obj.put("classNum", classNum);
		return obj;
	}

	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj = new JSONObject();
		obj.put("id", id);
		obj.put("eid", eid);
		obj.put("studid", studid);
		if(studid != null && !"".equals(studid)){
			obj.put("studidName", DictBuffer.getLabel("ZD_STUD", studid.toString()));
		}
		obj.put("classidCode",classid);
		if (classid != null && !classid.equals("")) {// 字典id值转label
			obj.put("classid",
					DictBuffer.getLabel("CC_CLASSES", classid.toString()));
		}
		obj.put("type", type);
		obj.put("name", name);
		obj.put("markTeacher", markTeacher);
		if (status != null && !status.equals("")) {// 字典id值转label
			obj.put("status", DictBuffer.getLabel("ZD_SFBZ", status.toString()));
		}
		obj.put("mark", mark);
		obj.put("remark", remark);
		if(createTime != null){
			obj.put("createTime", DateTransfer.toString(createTime,"yyyy-MM-dd HH:mm:ss"));
		}
		if (statusReady != null && !statusReady.equals("")) {// 字典id值转label
			obj.put("statusReady",
					DictBuffer.getLabel("ZD_SFBZ", statusReady.toString()));
		}
		obj.put("subjective", subjective);
		obj.put("objective", objective);
		if(markProgress == null){
			markProgress = 0f;
		}
		obj.put("markProgress", markProgress);
		
		obj.put("studName", studName);
		obj.put("classNum", classNum);
		return obj;
	}
}
