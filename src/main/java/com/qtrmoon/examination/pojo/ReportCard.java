package com.qtrmoon.examination.pojo;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONObject;

import com.qtrmoon.common.PageForm;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.dictionary.bsd.KeyVal;

/** 练习与班级关系表Object类 */
public class ReportCard extends PageForm {
	// Fields
	private Integer id; // 主键
	private Integer eid; // 练习编号
	private String studid; // 学生
	private String classid; // 班级
	private Integer type; // 类型
	private String name; // 标题
	private String markTeacher; // 阅卷老师
	private Integer status; // 阅卷状态
	private Float mark; // 得分
	private String remark; // 备注
	private Integer statusReady; // 是否答完(0:否,1:是)
	private String sname; // 学生名称

	/** default constructor */
	public ReportCard() {

	}

	// getter and setter
	/** 获取主键 */
	public Integer getId() {
		return this.id;
	}

	/** 设置主键 */
	public void setId(Integer id) {
		this.id = id;
	}

	/** 获取练习编号 */
	public Integer getEid() {
		return this.eid;
	}

	/** 设置练习编号 */
	public void setEid(Integer eid) {
		this.eid = eid;
	}

	/** 获取学生 */
	public String getStudid() {
		return this.studid;
	}

	/** 设置学生 */
	public void setStudid(String studid) {
		this.studid = studid;
	}

	/** 获取班级 */
	public String getClassid() {
		return this.classid;
	}

	/** 设置班级 */
	public void setClassid(String classid) {
		this.classid = classid;
	}

	/** 获取类型 */
	public Integer getType() {
		return this.type;
	}

	/** 设置类型 */
	public void setType(Integer type) {
		this.type = type;
	}

	/** 获取标题 */
	public String getName() {
		return this.name;
	}

	/** 设置标题 */
	public void setName(String name) {
		this.name = name;
	}

	/** 获取阅卷老师 */
	public String getMarkTeacher() {
		return this.markTeacher;
	}

	/** 设置阅卷老师 */
	public void setMarkTeacher(String markTeacher) {
		this.markTeacher = markTeacher;
	}

	/** 获取阅卷状态 */
	public Integer getStatus() {
		return this.status;
	}

	/** 设置阅卷状态 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/** 获取得分 */
	public Float getMark() {
		return this.mark;
	}

	/** 设置得分 */
	public void setMark(Float mark) {
		this.mark = mark;
	}

	/** 获取备注 */
	public String getRemark() {
		return this.remark;
	}

	/** 设置备注 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * 获取答题状态
	 * @return
	 */
	public Integer getStatusReady() {
		return statusReady;
	}

	/**
	 * 设置答题状态
	 * @param statusReady
	 */
	public void setStatusReady(Integer statusReady) {
		this.statusReady = statusReady;
	}

	/** 获取学生名称 */
	public String getSname() {
		return sname;
	}

	/** 设置学生名称 */
	public void setSname(String sname) {
		this.sname = sname;
	}

	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj = new JSONObject();
		obj.put("id", id);
		obj.put("eid", eid);
		obj.put("studid", studid);
		obj.put("classid", classid);
		obj.put("type", type);
		obj.put("name", name);
		obj.put("markTeacher", markTeacher);
		obj.put("status", status);
		obj.put("mark", mark);
		obj.put("remark", remark);
		obj.put("statusReady", statusReady);
		obj.put("sname", sname);
		return obj;
	}

	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj = new JSONObject();
		obj.put("id", id);
		obj.put("eid", eid);
		obj.put("studid", studid);
		if (classid != null && !classid.equals("")) {// 字典id值转label
			obj.put("classid",
					DictBuffer.getLabel("CC_CLASSES", classid.toString()));
		}
		obj.put("type", type);
		obj.put("name", name);
		obj.put("markTeacher", markTeacher);
		if (status != null && !status.equals("")) {// 字典id值转label
			obj.put("status", DictBuffer.getLabel("ZD_SFBZ", status.toString()));
		}
		obj.put("mark", mark);
		obj.put("remark", remark);
		obj.put("sname", sname);
		if (statusReady != null && !statusReady.equals("")) {// 字典id值转label
			obj.put("statusReady",
					DictBuffer.getLabel("ZD_SFBZ", statusReady.toString()));
		}
		return obj;
	}

	/**
	 * 获取表头
	 * @return
	 */
	public static List<KeyVal> getHeads() {
		List<KeyVal> heads = new ArrayList<KeyVal>();
		heads.add(new KeyVal("classid", "班級"));
		heads.add(new KeyVal("studid", "学号"));
		heads.add(new KeyVal("sname", "姓名"));
		heads.add(new KeyVal("status", "是否答题"));
		heads.add(new KeyVal("mark", "总分"));
		return heads;
	}
}
