package com.qtrmoon.examination.pojo;
import org.json.simple.JSONObject;
import com.qtrmoon.common.PageForm;

/** 学生答题试卷Object类 */
public class ExerPaper extends PageForm{
	
	private Integer id;// 主键
	private String exerId;// 练习编号
	private String studNumber;// 学号
	private String scoredetail;// 得分详情
	private Float score;
	/** default constructor */
	public ExerPaper() {
	
	}	
	//getter and setter
	/** 获取主键 */
	public Integer getId() {
		return this.id;
	}
	/** 设置主键 */
	public void setId(Integer id) {
		this.id = id;
	}
	/** 获取练习编号 */
	public String getExerId() {
		return this.exerId;
	}
	/** 设置练习编号 */
	public void setExerId(String exerId) {
		this.exerId = exerId;
	}
	/** 获取学号 */
	public String getStudNumber() {
		return this.studNumber;
	}
	/** 设置学号 */
	public void setStudNumber(String studNumber) {
		this.studNumber = studNumber;
	}
	/** 获取得分详情 */
	public String getScoredetail() {
		return this.scoredetail;
	}
	/** 设置得分详情 */
	public void setScoredetail(String scoredetail) {
		this.scoredetail = scoredetail;
	}
	/**
	 * 获取分数
	 * @return
	 */
	public Float getScore() {
		return score;
	}
	/**
	 * 设置分数
	 * @param score
	 */
	public void setScore(Float score) {
		this.score = score;
	}
	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("exerId", exerId);
		obj.put("studNumber", studNumber);
		obj.put("score", score);
		obj.put("scoredetail", scoredetail);
		return obj;
	}
	
	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("exerId", exerId);
		obj.put("studNumber", studNumber);
		obj.put("score", score);
		obj.put("scoredetail", scoredetail);
		return obj;
	}
}
