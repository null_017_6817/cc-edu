package com.qtrmoon.examination.ex;

import java.util.List;

/**
 * 试卷shutcut的拓展类
 */
public class ExamShutCut {
	String id; // id
	String pid; // 父id
	String label; // label
	List<ExamShutCut> children; // 子节点集合
	
	/* 获取id */
	public String getId() {
		return id;
	}
	
	/* 设置id */
	public void setId(String id) {
		this.id = id;
	}
	
	/* 获取父id */
	public String getPid() {
		return pid;
	}
	
	/* 设置父id */
	public void setPid(String pid) {
		this.pid = pid;
	}
	
	/* 获取label */
	public String getLabel() {
		return label;
	}
	
	/* 设置label */
	public void setLabel(String label) {
		this.label = label;
	}
	
	/* 获取子节点集合  */
	public List<ExamShutCut> getChildren() {
		return children;
	}
	
	/* 设置子节点 */
	public void setChildren(List<ExamShutCut> children) {
		this.children = children;
	}
}
