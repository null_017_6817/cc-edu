package com.qtrmoon.template.dto;

import java.util.ArrayList;
import java.util.List;

import com.qtrmoon.template.pojo.Template;
import com.qtrmoon.template.pojo.TemplateCategory;

/**
 * 模板DTO
 * @author huatang8
 */
public class TemplateDTO  extends Template{
	
	private List<TemplateCategory> templateCategoryList = new ArrayList<TemplateCategory>(); //模板分类集合
	
	/** 获取模板分类集合 */
	public List<TemplateCategory> getTemplateCategoryList() {
		return templateCategoryList;
	}

	/** 设置模板分类集合 */
	public void setTemplateCategoryList(List<TemplateCategory> templateCategoryList) {
		this.templateCategoryList = templateCategoryList;
	} 
}
