package com.qtrmoon.template.dto;

/** 模板分类指标DTO */
public class TemplateIndexDTO {
	private Integer serialNumber;// 序号;
	private String indexContent; //指标内容
	
	/** 获取指标序号 */
	public Integer getSerialNumber() {
		return serialNumber;
	}
	/** 设置指标序号 */
	public void setSerialNumber(Integer serialNumber) {
		this.serialNumber = serialNumber;
	}
	/** 获取指标内容 */
	public String getIndexContent() {
		return indexContent;
	}
	/** 设置指标内容 */
	public void setIndexContent(String indexContent) {
		this.indexContent = indexContent;
	}
}
