package com.qtrmoon.template.pojo;
import java.util.Date;
import java.util.Set;
import java.util.HashSet;
import org.json.simple.JSONObject;
import com.qtrmoon.common.PageForm;
import com.qtrmoon.toolkit.DateTransfer;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.dictionary.bsd.DictBean;

/** 模板表实体类 */
public class Template extends PageForm{
	//Fields
	
	private Integer id;// 主键
	private String templateName;// 模板名称
	private String totalScoreRatio;// 总分比例
	private String score;// 分值
	private String memo;// 说明
	private String founderId;// 创建人ID
	private Date creationTime;// 创建时间
	private String creationTimeBeg,creationTimeEnd;//时间条件的跨度查询属性
	//Constructors
	/** default constructor */
	public Template() {
	
	}	
	//getter and setter
	/** 获取主键 */
	public Integer getId() {
		return this.id;
	}
	/** 设置主键 */
	public void setId(Integer id) {
		this.id = id;
	}
	/** 获取模板名称 */
	public String getTemplateName() {
		return this.templateName;
	}
	/** 设置模板名称 */
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}
	/** 获取总分比例 */
	public String getTotalScoreRatio() {
		return this.totalScoreRatio;
	}
	/** 设置总分比例 */
	public void setTotalScoreRatio(String totalScoreRatio) {
		this.totalScoreRatio = totalScoreRatio;
	}
	/** 获取分值 */
	public String getScore() {
		return this.score;
	}
	/** 设置分值 */
	public void setScore(String score) {
		this.score = score;
	}
	/** 获取说明 */
	public String getMemo() {
		return this.memo;
	}
	/** 设置说明 */
	public void setMemo(String memo) {
		this.memo = memo;
	}
	/** 获取创建人ID */
	public String getFounderId() {
		return this.founderId;
	}
	/** 设置创建人ID */
	public void setFounderId(String founderId) {
		this.founderId = founderId;
	}
	/** 获取创建时间 */
	public Date getCreationTime() {
		return this.creationTime;
	}
	/** 设置创建时间 */
	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}
	/** 设定[创建时间]时间转载字段值，页面提交时由springmvc自动装载 */
	public void setCreationTimestr(String tm) {
		this.creationTime=_getTime(tm);
	}
	/** 获取[创建时间]时间起始条件 */
	public String getCreationTimeBeg() {
		return creationTimeBeg;
	}
	/** 获取[创建时间]时间起始条件(Dao/Mapper查询时调用) */
	public Date getCreationTimeBegDate() {
		return _getBegDate(creationTimeBeg);
	}
	/** 设置[创建时间]时间起始条件(表单提交时自动装载) */
	public void setCreationTimeBeg(String creationTimeBeg) {
		this.creationTimeBeg = creationTimeBeg;
	}
	
	/** 获取[创建时间]时间结束条件 */
	public String getCreationTimeEnd() {
		return creationTimeEnd;
	}
	/** 获取[创建时间]时间结束条件(Dao/Mapper查询时调用) */
	public Date getCreationTimeEndDate() {
		return _getEndDate(creationTimeEnd);
	}
	/** 设置[创建时间]时间结束条件(表单提交时自动装载) */
	public void setCreationTimeEnd(String creationTimeEnd) {
		this.creationTimeEnd = creationTimeEnd;
	}
	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("templateName", templateName);
		obj.put("totalScoreRatio", totalScoreRatio);
		obj.put("score", score);
		obj.put("memo", memo);
		obj.put("founderId", founderId);
		//时间字段处理。creationTime查询列表页使用，creationTimestr修改页使用
		obj.put("creationTime", DateTransfer.toString(creationTime,"yyyy/MM/dd"));
		obj.put("creationTimestr", DateTransfer.toString(creationTime,"yyyy/MM/dd"));
		return obj;
	}
	
	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("templateName", templateName);
		obj.put("totalScoreRatio", totalScoreRatio);
		obj.put("score", score);
		obj.put("memo", memo);
		if(founderId!=null){
			obj.put("founderId", DictBuffer.getLabel("ZD_TEACHER", founderId));
		}
		//时间字段处理。creationTime查询列表页使用，creationTimestr修改页使用
		obj.put("creationTime", DateTransfer.toString(creationTime,"yyyy/MM/dd"));
		obj.put("creationTimestr", DateTransfer.toString(creationTime,"yyyy/MM/dd"));
		return obj;
	}
}
