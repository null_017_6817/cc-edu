package com.qtrmoon.template.pojo;
import java.util.Date;
import java.util.Set;
import java.util.HashSet;
import org.json.simple.JSONObject;
import com.qtrmoon.common.PageForm;
import com.qtrmoon.toolkit.DateTransfer;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.dictionary.bsd.DictBean;

/** 模板分类表实体类 */
public class TemplateCategory extends PageForm{
	//Fields
	
	private Integer id;// 主键
	private Integer templateId;// 模板ID
	private Integer serialNumber;// 序号
	private String classificationName;// 分类名称
	private Integer calculationProportion;// 计算比例
	private Integer indexQuantity;// 指标数量
	private String indexContent;// 指标内容
	//Constructors
	/** default constructor */
	public TemplateCategory() {
	
	}	
	//getter and setter
	/** 获取主键 */
	public Integer getId() {
		return this.id;
	}
	/** 设置主键 */
	public void setId(Integer id) {
		this.id = id;
	}
	/** 获取模板ID */
	public Integer getTemplateId() {
		return this.templateId;
	}
	/** 设置模板ID */
	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}
	/** 获取序号 */
	public Integer getSerialNumber() {
		return this.serialNumber;
	}
	/** 设置序号 */
	public void setSerialNumber(Integer serialNumber) {
		this.serialNumber = serialNumber;
	}
	/** 获取分类名称 */
	public String getClassificationName() {
		return this.classificationName;
	}
	/** 设置分类名称 */
	public void setClassificationName(String classificationName) {
		this.classificationName = classificationName;
	}
	/** 获取计算比例 */
	public Integer getCalculationProportion() {
		return this.calculationProportion;
	}
	/** 设置计算比例 */
	public void setCalculationProportion(Integer calculationProportion) {
		this.calculationProportion = calculationProportion;
	}
	/** 获取指标数量 */
	public Integer getIndexQuantity() {
		return this.indexQuantity;
	}
	/** 设置指标数量 */
	public void setIndexQuantity(Integer indexQuantity) {
		this.indexQuantity = indexQuantity;
	}
	/** 获取指标内容 */
	public String getIndexContent() {
		return this.indexContent;
	}
	/** 设置指标内容 */
	public void setIndexContent(String indexContent) {
		this.indexContent = indexContent;
	}
	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("templateId", templateId);
		obj.put("serialNumber", serialNumber);
		obj.put("classificationName", classificationName);
		obj.put("calculationProportion", calculationProportion);
		obj.put("indexQuantity", indexQuantity);
		obj.put("indexContent", indexContent);
		return obj;
	}
	
	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("templateId", templateId);
		obj.put("serialNumber", serialNumber);
		obj.put("classificationName", classificationName);
		obj.put("calculationProportion", calculationProportion);
		obj.put("indexQuantity", indexQuantity);
		obj.put("indexContent", indexContent);
		return obj;
	}
}
