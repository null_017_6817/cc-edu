package com.qtrmoon.template.serdao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.qtrmoon.template.dto.TemplateDTO;
import com.qtrmoon.template.pojo.Template;
/** 模板表实体映射 */
@Component
public interface TemplateMapper {
	/**
	 * Template的条件查询方法
	 * @param template 承载查询条件
	 * @return 返回Template的集合
	 */
	public List<Template> schTemplate(Template template);
	
	/**
	 * Template的主键查询方法
	 * @param id 主键值
	 * @return 返回Template实体
	 */
	public TemplateDTO schTemplateById(Integer id);
	
	/**
	 * Template的添加方法
	 * @param template 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addTemplate(Template template);
	
	/**
	 * Template的修改方法
	 * @param template 承载数据的Bean
	 */
	public void updTemplate(Template template);
	
	/**
	 * Template的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delTemplate(Integer id);
	
	/**
	 * Template的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delTemplates(Integer[] ids);
}
