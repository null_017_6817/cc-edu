package com.qtrmoon.template.serdao;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.qtrmoon.template.dto.TemplateDTO;
import com.qtrmoon.template.pojo.Template;
import com.qtrmoon.template.pojo.TemplateCategory;

/** 服务实现类 */
@Service("templateService")
public class TemplateServiceImpl implements ITemplateService {
	@Autowired
	private TemplateMapper templateMapper;


	/***************** Template方法组 *****************/
	/**
	 * Template的条件查询方法
	 * @param template 承载查询条件的Bean
	 * @return 返回Template的集合
	 */
	@Override
	public List<Template> schTemplate(Template template) {
		PageHelper.startPage(template.getPage(), template.getPagesize());//设置分页显示
		List<Template> list=templateMapper.schTemplate(template);
		Page<Template> page = (Page<Template>)list;//为了设置总记录数先类型强转
		template.setDatasize(page.getTotal());
		return list;
	}
	
	/**
	 * Template的主键查询方法
	 * @param id 主键值
	 * @return 返回Template实体
	 */
	@Override
	public TemplateDTO schTemplateById(Integer id) {
		TemplateDTO templateDTO = templateMapper.schTemplateById(id);
		return templateDTO;
	}
	
	/**
	 * Template的添加方法
	 * @param template 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	@Override
	public Integer addTemplate(TemplateDTO templateDTO) {
		templateDTO.setCreationTime(new Date());
		Template template = new Template();
		try {
			BeanUtils.copyProperties(template,templateDTO);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		//保存模板
		int eff = templateMapper.addTemplate(template);
		List<TemplateCategory> templateCategorys = templateDTO.getTemplateCategoryList();
		if(templateCategorys != null && templateCategorys.size() > 0) {
			for(int i = 0 ;i < templateCategorys.size();i++){
				templateCategorys.get(i).setTemplateId(template.getId());
				if(templateCategorys.get(i).getIndexQuantity()==null){
					templateCategorys.get(i).setIndexQuantity(0);
				}
			}
		}
		//保存模板指标
		templateCategoryMapper.addBatchTemplateCategory(templateCategorys);
		return eff;
	}

	/**
	 * Template的修改方法
	 * @param template 承载数据的Bean
	 */
	@Override
	public void updTemplate(TemplateDTO templateDTO) {
		//1 更新模板信息
		modifyTemplate(templateDTO);
		//2 模板分类列表筛选出新增的、修改的
		List<TemplateCategory> templateCategories = templateDTO.getTemplateCategoryList();
		List<TemplateCategory> addTemplateCategories = new ArrayList<TemplateCategory>();
		List<TemplateCategory> updTemplateCategories = new ArrayList<TemplateCategory>();
		
		//记录新提交数据中含有的原有分类ID
		List<Integer> idsReady = new ArrayList<Integer>();
		
		if(templateCategories != null && templateCategories.size() >0) {
			for(int i = 0 ;i < templateCategories.size();i++){
				TemplateCategory tc = templateCategories.get(i);
				tc.setTemplateId(templateDTO.getId());
				if(tc.getIndexQuantity()==null){
					tc.setIndexQuantity(0);
				}
				if(tc.getId()==null||tc.getId()==0){
					addTemplateCategories.add(tc);
				}else {
					idsReady.add(tc.getId());
					updTemplateCategories.add(tc);
				}
			}
		}
		
		//3 模板分类列表筛选出删除的
		List<TemplateCategory> delTemplateCategories = getDelTemplateCategories(idsReady,templateDTO.getId());
		
		//4 执行保存操作
		updateTemplateCategory(updTemplateCategories);
		addTemplateCategory(addTemplateCategories);
		delTemplateCategory(delTemplateCategories);
	}

	/***
	 * 获取需要删除的分类列表
	 * @param idsReady 需要存在的分类Id集合
	 * @param templateId 模板ID
	 * @return
	 */
	private List<TemplateCategory> getDelTemplateCategories(List<Integer> idsReady,Integer templateId){
		List<TemplateCategory> delTemplateCategories = new ArrayList<TemplateCategory>();
		TemplateCategory templateCategoryQuery = new TemplateCategory();
		templateCategoryQuery.setTemplateId(templateId);
		//数据库中已经存在的分类列表
		List<TemplateCategory> templateCategoriesDb = templateCategoryMapper.schTemplateCategory(templateCategoryQuery);
		
		//本次提交还含有上次提交的分类
		if(idsReady!=null && idsReady.size() > 0) {
			if(templateCategoriesDb!=null && templateCategoriesDb.size() >0){
				for(int i =0 ;i < templateCategoriesDb.size();i++){
					//删除数据库中不存在的记录
					if(!idsReady.contains(templateCategoriesDb.get(i).getId())){
						delTemplateCategories.add(templateCategoriesDb.get(i));
					}
				}
			}
		}else { //本次提交不含有上次提交的分类，则当前数据库保持的分类都要被删除
			delTemplateCategories = templateCategoriesDb;
		}
		return delTemplateCategories;
	}
	
	/**
	 * 更新模板信息
	 * @param templateDTO
	 */
	private void modifyTemplate(TemplateDTO templateDTO){
		templateDTO.setCreationTime(new Date());
		Template template = new Template();
		try {
			BeanUtils.copyProperties(template,templateDTO);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		templateMapper.updTemplate(template);
	}
	
	
	/***
	 * 添加模板分类
	 * @param templateCategories
	 */
	private void addTemplateCategory(List<TemplateCategory> addTemplateCategories){
		if(addTemplateCategories != null && addTemplateCategories.size() > 0) {
			templateCategoryMapper.addBatchTemplateCategory(addTemplateCategories);
		}
	}
	
	/***
	 * 更新模板分类
	 * @param templateCategories
	 */
	private void updateTemplateCategory(List<TemplateCategory> updTemplateCategories){
		if(updTemplateCategories != null && updTemplateCategories.size() > 0) {
			templateCategoryMapper.updBatchTemplateCategory(updTemplateCategories);
		}
	}
	
	/***
	 * 删除模板分类
	 * @param templateCategories
	 */
	private void delTemplateCategory(List<TemplateCategory> delTemplateCategories){
		if(delTemplateCategories!=null && delTemplateCategories.size() > 0) {
			Integer ids[] = new Integer[delTemplateCategories.size()];
			for(int i = 0 ;i < delTemplateCategories.size();i++){
				ids[i] = delTemplateCategories.get(i).getId();
			}
			templateCategoryMapper.delTemplateCategorys(ids);
		}
	}
	
	
	/**
	 * Template的单记录删除方法
	 * @param id 要删除的主键值
	 */
	@Override
	public void delTemplate(Integer id) {
		templateMapper.delTemplate(id);
	}

	/**
	 * Template的批量删除方法
	 * @param ids 主键值的数组
	 */
	@Override
	public void delTemplate(Integer[] ids) {
		templateMapper.delTemplates(ids);
	}


	@Autowired
	private TemplateCategoryMapper templateCategoryMapper;


	/***************** TemplateCategory方法组 *****************/
	/**
	 * TemplateCategory的条件查询方法
	 * @param templateCategory 承载查询条件的Bean
	 * @return 返回TemplateCategory的集合
	 */
	@Override
	public List<TemplateCategory> schTemplateCategory(TemplateCategory templateCategory) {
		PageHelper.startPage(templateCategory.getPage(), templateCategory.getPagesize());//设置分页显示
		List<TemplateCategory> list=templateCategoryMapper.schTemplateCategory(templateCategory);
		Page<TemplateCategory> page = (Page<TemplateCategory>)list;//为了设置总记录数先类型强转
		templateCategory.setDatasize(page.getTotal());
		return list;
	}
	
	/**
	 * TemplateCategory的主键查询方法
	 * @param id 主键值
	 * @return 返回TemplateCategory实体
	 */
	@Override
	public TemplateCategory schTemplateCategoryById(Integer id) {
		return templateCategoryMapper.schTemplateCategoryById(id);
	}
	
	/**
	 * TemplateCategory的添加方法
	 * @param templateCategory 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	@Override
	public Integer addTemplateCategory(TemplateCategory templateCategory) {
		return templateCategoryMapper.addTemplateCategory(templateCategory);
	}

	/**
	 * TemplateCategory的修改方法
	 * @param templateCategory 承载数据的Bean
	 */
	@Override
	public void updTemplateCategory(TemplateCategory templateCategory) {
		templateCategoryMapper.updTemplateCategory(templateCategory);
	}

	/**
	 * TemplateCategory的单记录删除方法
	 * @param id 要删除的主键值
	 */
	@Override
	public void delTemplateCategory(Integer id) {
		templateCategoryMapper.delTemplateCategory(id);
	}

	/**
	 * TemplateCategory的批量删除方法
	 * @param ids 主键值的数组
	 */
	@Override
	public void delTemplateCategory(Integer[] ids) {
		templateCategoryMapper.delTemplateCategorys(ids);
	}

	/***
	 * 更新分类指标数量和内容
	 * @param templateCategory
	 */
	@Override
	public void updIndexCountAndContent(TemplateCategory templateCategory) {
		templateCategoryMapper.updIndexCountAndContent(templateCategory);
	}

}//end
