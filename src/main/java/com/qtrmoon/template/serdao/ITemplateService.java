package com.qtrmoon.template.serdao;
import java.util.List;
import java.util.Map;

import com.qtrmoon.template.dto.TemplateDTO;
import com.qtrmoon.template.pojo.*;
/** 服务接口 */
public interface ITemplateService {
	
	/***************** Template方法组 *****************/
	/**
	 * Template的条件查询方法
	 * @param template 承载查询条件的Bean
	 * @return 返回Template的集合
	 */
	public List<Template> schTemplate(Template template);
	
	/**
	 * Template的主键查询方法
	 * @param id 主键值
	 * @return 返回Template实体
	 */
	public TemplateDTO schTemplateById(Integer id);
	
	/**
	 * Template的添加方法
	 * @param templateDTO 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addTemplate(TemplateDTO templateDTO);
	
	/**
	 * Template的修改方法
	 * @param template 承载数据的Bean
	 */
	public void updTemplate(TemplateDTO templateDTO);
	
	/**
	 * Template的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delTemplate(Integer id);
	
	/**
	 * Template的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delTemplate(Integer[] ids);


	
	/***************** TemplateCategory方法组 *****************/
	/**
	 * TemplateCategory的条件查询方法
	 * @param templateCategory 承载查询条件的Bean
	 * @return 返回TemplateCategory的集合
	 */
	public List<TemplateCategory> schTemplateCategory(TemplateCategory templateCategory);
	
	/**
	 * TemplateCategory的主键查询方法
	 * @param id 主键值
	 * @return 返回TemplateCategory实体
	 */
	public TemplateCategory schTemplateCategoryById(Integer id);
	
	/**
	 * TemplateCategory的添加方法
	 * @param templateCategory 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addTemplateCategory(TemplateCategory templateCategory);
	
	/**
	 * TemplateCategory的修改方法
	 * @param templateCategory 承载数据的Bean
	 */
	public void updTemplateCategory(TemplateCategory templateCategory);
	
	/**
	 * TemplateCategory的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delTemplateCategory(Integer id);
	
	/**
	 * TemplateCategory的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delTemplateCategory(Integer[] ids);

	/***
	 * 更新分类指标数量和内容
	 * @param templateCategory
	 */
	public void updIndexCountAndContent(TemplateCategory templateCategory);

}//end
