package com.qtrmoon.template.serdao;

import java.util.List;
import org.springframework.stereotype.Component;

import com.qtrmoon.template.pojo.TemplateCategory;
/** 模板分类表实体映射 */
@Component
public interface TemplateCategoryMapper {
	/**
	 * TemplateCategory的条件查询方法
	 * @param templateCategory 承载查询条件
	 * @return 返回TemplateCategory的集合
	 */
	public List<TemplateCategory> schTemplateCategory(TemplateCategory templateCategory);
	
	/**
	 * TemplateCategory的主键查询方法
	 * @param id 主键值
	 * @return 返回TemplateCategory实体
	 */
	public TemplateCategory schTemplateCategoryById(Integer id);
	
	/**
	 * TemplateCategory的添加方法
	 * @param templateCategory 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addTemplateCategory(TemplateCategory templateCategory);
	
	/**
	 * TemplateCategory的修改方法
	 * @param templateCategory 承载数据的Bean
	 */
	public void updTemplateCategory(TemplateCategory templateCategory);
	
	/**
	 * TemplateCategory的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delTemplateCategory(Integer id);
	
	/**
	 * TemplateCategory的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delTemplateCategorys(Integer[] ids);
	
	/**
	 * TemplateCategory的批量添加方法
	 * @param templateCategory 承载数据的Bean，执行添加后会更新入库后的主键值
	 */
	public void addBatchTemplateCategory(List<TemplateCategory> templateCategorys);

	/***
	 * 批量更新
	 * @param updTemplateCategories
	 */
	public void updBatchTemplateCategory(List<TemplateCategory> updTemplateCategories);

	/***
	 * 更新分类指标数量和内容
	 * @param templateCategory
	 */
	public void updIndexCountAndContent(TemplateCategory templateCategory);
}
