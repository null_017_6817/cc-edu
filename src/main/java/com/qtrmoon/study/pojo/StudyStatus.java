package com.qtrmoon.study.pojo;
import java.util.Date;
import java.util.Set;
import java.util.HashSet;
import org.json.simple.JSONObject;
import com.qtrmoon.common.PageForm;
import com.qtrmoon.toolkit.DateTransfer;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.dictionary.bsd.DictBean;

/** 实体类 */
public class StudyStatus extends PageForm{
	//Fields
	
	private Integer id;// 
	private Integer bookId;// 章节id
	private String number;// 学号
	private String status;// 状态
	//Constructors
	/** default constructor */
	public StudyStatus() {
	
	}	
	//getter and setter
	/** 获取 */
	public Integer getId() {
		return this.id;
	}
	/** 设置 */
	public void setId(Integer id) {
		this.id = id;
	}
	/** 获取章节id */
	public Integer getBookId() {
		return this.bookId;
	}
	/** 设置章节id */
	public void setBookId(Integer bookId) {
		this.bookId = bookId;
	}
	/** 获取学号 */
	public String getNumber() {
		return this.number;
	}
	/** 设置学号 */
	public void setNumber(String number) {
		this.number = number;
	}
	/** 获取状态 */
	public String getStatus() {
		return this.status;
	}
	/** 设置状态 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
 * 判断主键有值
 * @return 有值返回true
 */
	public boolean hasKey() {
		if(id!=null&&id>0)return true;
		return false;
	}
	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("bookId", bookId);
		obj.put("number", number);
		obj.put("status", status);
		return obj;
	}
	
	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("bookId", bookId);
		obj.put("number", number);
		obj.put("status", status);
		return obj;
	}
}
