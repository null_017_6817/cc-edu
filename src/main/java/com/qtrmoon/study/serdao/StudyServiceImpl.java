package com.qtrmoon.study.serdao;
import java.util.List;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qtrmoon.study.pojo.*;

/** study服务实现类 */
@Service("studyService")
public class StudyServiceImpl implements IStudyService {
	@Autowired
	private StudyStatusMapper studyStatusMapper;


	/***************** StudyStatus方法组 *****************/
	/**
	 * StudyStatus的条件查询方法
	 * @param studyStatus 承载查询条件的Bean
	 * @return 返回StudyStatus的集合
	 */
	@Override
	public List<StudyStatus> schStudyStatus(StudyStatus studyStatus) {
		if(studyStatus.getPagesize()>0)PageHelper.startPage(studyStatus.getPage(), studyStatus.getPagesize());//设置分页显示
		List<StudyStatus> list=studyStatusMapper.schStudyStatus(studyStatus);
		if(studyStatus.getPagesize()>0){
			Page<StudyStatus> page = (Page<StudyStatus>)list;//为了设置总记录数先类型强转
			studyStatus.setDatasize(page.getTotal());
		}else{
			studyStatus.setDatasize(list.size());
		}
		return list;
	}
	
	/**
	 * StudyStatus的主键查询方法
	 * @param id 主键值
	 * @return 返回StudyStatus实体
	 */
	@Override
	public StudyStatus schStudyStatusById(Integer id) {
		return studyStatusMapper.schStudyStatusById(id);
	}
	
	/**
	 * StudyStatus的添加方法
	 * @param studyStatus 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	@Override
	public Integer addStudyStatus(StudyStatus studyStatus) {
		return studyStatusMapper.addStudyStatus(studyStatus);
	}

	/**
	 * StudyStatus的修改方法
	 * @param studyStatus 承载数据的Bean
	 */
	@Override
	public void updStudyStatus(StudyStatus studyStatus) {
		studyStatusMapper.updStudyStatus(studyStatus);
	}

	/**
	 * StudyStatus的单记录删除方法
	 * @param id 要删除的主键值
	 */
	@Override
	public void delStudyStatus(Integer id) {
		studyStatusMapper.delStudyStatus(id);
	}

	/**
	 * StudyStatus的批量删除方法
	 * @param ids 主键值的数组
	 */
	@Override
	public void delStudyStatus(Integer[] ids) {
		studyStatusMapper.delStudyStatuss(ids);
	}

}//end
