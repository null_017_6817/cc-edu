package com.qtrmoon.study.serdao;
import java.util.List;
import java.util.Map;
import com.qtrmoon.study.pojo.*;
/** study服务接口 */
public interface IStudyService {
	
	/***************** StudyStatus方法组 *****************/
	/**
	 * StudyStatus的条件查询方法
	 * @param studyStatus 承载查询条件的Bean
	 * @return 返回StudyStatus的集合
	 */
	public List<StudyStatus> schStudyStatus(StudyStatus studyStatus);
	
	/**
	 * StudyStatus的主键查询方法
	 * @param id 主键值
	 * @return 返回StudyStatus实体
	 */
	public StudyStatus schStudyStatusById(Integer id);
	
	/**
	 * StudyStatus的添加方法
	 * @param studyStatus 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addStudyStatus(StudyStatus studyStatus);
	
	/**
	 * StudyStatus的修改方法
	 * @param studyStatus 承载数据的Bean
	 */
	public void updStudyStatus(StudyStatus studyStatus);
	
	/**
	 * StudyStatus的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delStudyStatus(Integer id);
	
	/**
	 * StudyStatus的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delStudyStatus(Integer[] ids);

}//end
