package com.qtrmoon.manager.serdao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.qtrmoon.manager.pojo.BankStep;
import com.qtrmoon.manager.pojo.BankTopic;
/**
 * 题库步骤持久层
 */
@Component
public interface BankStepMapper {
	/**
	 * BankStep的条件查询方法
	 * @param bankStep 承载查询条件
	 * @return 返回BankStep的集合
	 */
	public List<BankStep> schBankStep(BankStep bankStep);
	
	/**
	 * BankStep的主键查询方法
	 * @param id 主键值
	 * @return 返回BankStep实体
	 */
	public BankStep schBankStepById(Integer id);
	
	/**
	 * BankStep的添加方法
	 * @param bankStep 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addBankStep(BankStep bankStep);
	
	/**
	 * BankStep的修改方法
	 * @param bankStep 承载数据的Bean
	 */
	public void updBankStep(BankStep bankStep);
	
	/**
	 * BankStep的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delBankStep(Integer id);
	
	/**
	 * BankStep的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delBankSteps(Integer[] ids);

	/**
	 * 批量新增
	 * @param list
	 */
	public void addBatch(List<BankStep> list);
	
	/**
	 * 根据参数查询step(自动组卷用)
	 * @param param
	 * @return
	 */
	public List<BankTopic> schStepByAutoPage(Map<String,Object> param);

	public void addBatchTemp(List<BankStep> steps);
}
