package com.qtrmoon.manager.serdao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import com.qtrmoon.manager.pojo.Bank;
import com.qtrmoon.manager.pojo.DirTree;
/**
 * 上传题目持久层
 */
@Component
public interface DirTreeMapper {
	/**
	 * DirTree的条件查询方法
	 * @param md5Sign 上传题目批次号
	 * @return 返回DirTree的集合
	 */
	public List<DirTree> schDirTree(@Param("md5Sign") String md5Sign);
	
	/**
	 * DirTree的主键查询方法
	 * @param id 主键值
	 * @return 返回DirTree实体
	 */
	public DirTree schDirTreeById(@Param("id") Integer id);
	
	/**
	 * DirTree的添加方法
	 * @param DirTree 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addDirTree(DirTree tree);
	
	/**
	 * DirTree的更新方法，在xml解析后更新解析成功与否
	 * @param tree
	 * @return 返回影响的行数
	 */
	public Integer updDirTree(DirTree tree);
	
}
