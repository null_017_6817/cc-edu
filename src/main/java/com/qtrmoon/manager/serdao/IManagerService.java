package com.qtrmoon.manager.serdao;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.multipart.MultipartFile;

import com.qtrmoon.manager.pojo.Bank;
import com.qtrmoon.manager.pojo.BankStep;
import com.qtrmoon.manager.pojo.BankTopic;
import com.qtrmoon.manager.pojo.Classes;
import com.qtrmoon.manager.pojo.Course;
import com.qtrmoon.manager.pojo.DirTree;
import com.qtrmoon.manager.pojo.Record;
import com.qtrmoon.manager.pojo.Repository;
import com.qtrmoon.manager.pojo.Stud;
import com.qtrmoon.manager.pojo.ZdRepository;
import com.qtrmoon.manager.pojo.BankTheory;
/**
 * 管理类接口
 * @author HuaTang_DYC
 *
 */
public interface IManagerService {
	
	/***************** Classes方法组 *****************/
	/**
	 * Classes的条件查询方法
	 * @param classes 承载查询条件的Bean
	 * @return 返回Classes的集合
	 */
	public List<Classes> schClasses(Classes classes);
	
	/**
	 * Classes的主键查询方法
	 * @param id 主键值
	 * @return 返回Classes实体
	 */
	public Classes schClassesById(Integer id);
	
	/**
	 * Classes的添加方法
	 * @param classes 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addClasses(Classes classes);
	
	/**
	 * Classes的修改方法
	 * @param classes 承载数据的Bean
	 */
	public void updClasses(Classes classes);
	
	/**
	 * Classes的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delClasses(Integer id);
	
	/**
	 * Classes的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delClasses(Integer[] ids);



	
	/***************** Stud方法组 *****************/
	/**
	 * Stud的条件查询方法
	 * @param stud 承载查询条件的Bean
	 * @return 返回Stud的集合
	 */
	public List<Stud> schStud(Stud stud);
	
	/**
	 * Stud的主键查询方法
	 * @param id 主键值
	 * @return 返回Stud实体
	 */
	public Stud schStudById(Integer id);
	
	/**
	 * 根据学号查询学生
	 * @param number
	 * @return
	 */
	public Stud schStudByXh(String number);
	
	/**
	 * Stud的添加方法
	 * @param stud 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addStud(Stud stud);
	
	/**
	 * Stud的修改方法
	 * @param stud 承载数据的Bean
	 */
	public void updStud(Stud stud);
	
	/**
	 * 
	 * @param stud
	 */
	public void updStudData(Stud stud);
	
	/**
	 * Stud的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delStud(Integer id);
	
	/**
	 * Stud的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delStud(Integer[] ids);

	/**
	 * 清除学生考试状态
	 * @param ids
	 */
	public void updClearExamStu(Integer[] ids);
	
	/***************** Repository方法组 *****************/
	/**
	 * Repository的条件查询方法
	 * @param repository 承载查询条件的Bean
	 * @return 返回Repository的集合
	 */
	public List<Repository> schRepository(Repository repository);
	/**
	 * Repository的主键查询方法
	 * @param id 主键值
	 * @return 返回Repository实体
	 */
	public Repository schRepositoryById(Integer id);
	
	/**
	 * Repository的查询方法（通过标题或者目录）
	 * @param repository 
	 * @return 返回Repository的集合
	 */
	public List<Repository> schRepositoryByCutOrTitle(Repository repository);
	/**
	 * Repository的添加方法
	 * @param id 
	 * @param repository 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @param session 
	 * @param file 
	 * @param request 
	 * @param file 
	 * @param request 
	 * @param response 
	 * @param model 
	 * @return 返回影响的行数
	 * @throws IOException 
	 */
	public Integer addRepository(Integer id, Repository repository, MultipartFile file, HttpSession session, HttpServletResponse response, HttpServletRequest request) throws IOException;
	
	/**
	 * Repository的修改方法
	 * @param id 
	 * @param repository 承载数据的Bean
	 * @param session 
	 * @param file 
	 * @param request 
	 * @param file 
	 * @param request 
	 * @param response 
	 * @param model 
	 * @throws IOException 
	 * @throws IllegalStateException 
	 */
	public void updRepository(Integer id, Repository repository, MultipartFile file, HttpSession session, HttpServletResponse response, HttpServletRequest request) throws IllegalStateException, IOException;
	
	/**
	 * Repository的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delRepository(Integer id);
	
	/**
	 * Repository的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delRepository(Integer[] ids);


	
	/***************** Course方法组 *****************/
	/**
	 * Course的条件查询方法
	 * @param course 承载查询条件的Bean
	 * @return 返回Course的集合
	 */
	public List<Course> schCourse(Course course);
	
	/**
	 * Course的主键查询方法
	 * @param id 主键值
	 * @return 返回Course实体
	 */
	public Course schCourseById(Integer id);
	
	/**
	 * Course的添加方法
	 * @param id 
	 * @param course 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @param request 
	 * @param response 
	 * @param session 
	 * @param file 
	 * @return 返回影响的行数
	 * @throws IOException 
	 * @throws IllegalStateException 
	 */
	public Integer addCourse(Integer id, Course course, MultipartFile file, HttpSession session, HttpServletResponse response, HttpServletRequest request) throws IllegalStateException, IOException;
	
	/**
	 * Course的修改方法
	 * @param id 
	 * @param course 承载数据的Bean
	 * @param request 
	 * @param response 
	 * @param session 
	 * @param file 
	 * @throws IOException 
	 */
	public void updCourse(Integer id, Course course, MultipartFile file, HttpSession session, HttpServletResponse response, HttpServletRequest request) throws IOException;
	
	/**
	 * Course的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delCourse(Integer id);
	
	/**
	 * Course的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delCourse(Integer[] ids);




	
	/***************** Bank方法组 *****************/
	/**
	 * Bank的条件查询方法
	 * @param bank 承载查询条件的Bean
	 * @return 返回Bank的集合
	 */
	public List<Bank> schBank(Bank bank);
	
	/**
	 * Bank的条件查询方法(无分页)
	 * @param bank 承载查询条件的Bean
	 * @return 返回Bank的集合
	 */
	public List<Bank> schBankList(Bank bank);
	
	/**
	 * Bank的主键查询方法
	 * @param string 主键值
	 * @return 返回Bank实体
	 */
	public Bank schBankById(String string);
	
	/**
	 * Bank的添加方法
	 * @param bank 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addBank(Bank bank);
	
	/**
	 * Bank的修改方法
	 * @param bank 承载数据的Bean
	 */
	public void updBank(Bank bank);
	
	/**
	 * Bank的单记录删除方法
	 * @param ids 要删除的主键值
	 */
	public void delBank(String id);
	
	/**
	 * Bank的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delBank(String[] ids);


	
	/***************** BankTopic方法组 *****************/
	/**
	 * BankTopic的条件查询方法
	 * @param bankTopic 承载查询条件的Bean
	 * @return 返回BankTopic的集合
	 */
	public List<BankTopic> schBankTopic(BankTopic bankTopic);
	
	/**
	 * 查询列表
	 * @param bankTopic
	 * @return
	 */
	public List<BankTopic> schBankTopicList(BankTopic bankTopic);
	
	/**
	 * BankTopic的主键查询方法
	 * @param id 主键值
	 * @return 返回BankTopic实体
	 */
	public BankTopic schBankTopicById(String id);
	
	/**
	 * BankTopic的添加方法
	 * @param bankTopic 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addBankTopic(BankTopic bankTopic);
	
	/**
	 * 上传导入题目所用的添加方法
	 * @param bankTopic
	 * @return
	 */
	public Integer addBankTopicByUpload(BankTopic bankTopic);
	
	/**
	 * BankTopic的修改方法
	 * @param bankTopic 承载数据的Bean
	 */
	public void updBankTopic(BankTopic bankTopic);
	
	/**
	 * BankTopic的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delBankTopic(String id);
	
	/**
	 * BankTopic的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delBankTopic(String[] ids);

	/**
	 * 批量新增topic
	 */
	public void addBankTopicBatch(List<BankTopic> list);

	
	/***************** BankStep方法组 *****************/
	/**
	 * BankStep的条件查询方法
	 * @param bankStep 承载查询条件的Bean
	 * @return 返回BankStep的集合
	 */
	public List<BankStep> schBankStep(BankStep bankStep);
	
	/**
	 * BankStep的条件查询方法(无分页)
	 * @param bankStep 承载查询条件的Bean
	 * @return 返回BankStep的集合
	 */
	public List<BankStep> schBankStepList(BankStep bankStep);
	
	/**
	 * BankStep的主键查询方法
	 * @param id 主键值
	 * @return 返回BankStep实体
	 */
	public BankStep schBankStepById(Integer id);
	
	/**
	 * BankStep的添加方法
	 * @param bankStep 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addBankStep(BankStep bankStep);
	
	/**
	 * BankStep的修改方法
	 * @param bankStep 承载数据的Bean
	 */
	public void updBankStep(BankStep bankStep);
	
	/**
	 * BankStep的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delBankStep(Integer id);
	
	/**
	 * BankStep的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delBankStep(Integer[] ids);

	/**
	 * 批量添加step
	 */
	public void addBankStepBatch(List<BankStep> list);
	
	/**
	 * 根据id重置密码
	 * @param ids
	 */
	public void resetPwd(Integer[] ids);
	
	/**
	 * 导入学生账号
	 * @param cnumber
	 * @param list
	 * @return 
	 */
	public String addStudFile(String cnumber, List<Stud> list);
	/**
	 * 根据机构id查询知识库
	 * @param organids
	 * @return
	 */
	public ZdRepository schZdschRepositoryById(String organids);
	/**
	 * 根据父id查询知识库
	 * @param pid
	 * @return
	 */
	public ZdRepository schZdschRepositoryByPid(Integer pid);
	/**
	 * 获取课本id
	 * @param bookId
	 * @return
	 */
	public List<String> getId(String bookId);
	/**
	 * 获取除本人外的学生
	 * @param stud
	 * @return
	 */
	public List<Stud> schStudNotMe(Stud stud);
	/**
	 * 获取正在通话的学生
	 * @return
	 */
	public List<Map<String, Object>> schStudCalling();
	
	

	
	/***************** Record方法组 *****************/
	
	public List<Record> schRecordList(Record record);
	
	/**
	 * Record的条件查询方法
	 * @param record 承载查询条件的Bean
	 * @return 返回Record的集合
	 */
	public List<Record> schRecord(Record record);
	
	/**
	 * Record的主键查询方法
	 * @param id 主键值
	 * @return 返回Record实体
	 */
	public Record schRecordById(Integer id);
	
	/**
	 * Record的添加方法
	 * @param record 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addRecord(Record record);
	
	/**
	 * Record的修改方法
	 * @param record 承载数据的Bean
	 */
	public void updRecord(Record record);
	
	/**
	 * Record的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delRecord(Integer id);
	
	/**
	 * Record的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delRecord(Integer[] ids);
	/**
	 * 通过任务获取录音
	 * @param record
	 * @return
	 */
	public List<Record> schRecordByTask(Record record);
	/**
	 * 隐藏已添加题目
	 * @param bankTopic
	 * @return
	 */
	public List<BankTopic> schBankTopicNotExer(BankTopic bankTopic);
	/**
	 * 查询所有学生
	 * @param stud
	 * @return
	 */
	public List<Stud> schStudAll(Stud stud);
	
	/**
	 * pojo持久化到数据库
	 * @param tree
	 * @return 返回影响的行数
	 */
	public Integer addDirTree(DirTree tree);
	
	/**
	 * 根据上传批次号查询同批次上传的目录及文件
	 * @param sign
	 * @return 返回实体列表
	 */
	public List<DirTree> schDirTree(String sign);
	
	/**
	 * 根据主键查询对象
	 * @param id
	 * @return 返回查询实体
	 */
	public DirTree schDirTreeById(Integer id);
	
	/**
	 * 更新xml解析成功标识
	 * @param tree
	 * @return 返回影响的行数
	 */
	public Integer updDirTree(DirTree tree);
	
	/**
	 * 分题型统计数量
	 * @return
	 */
	public Map<String,Object> updGroupNum(Map<String,Object> param);

	/**
	 * 修改题目总分
	 */
	public void updBankTopicTotalScore(BankTopic bankTopic);

	/**
	 * 查询理论题
	 */
	public List<BankTheory> schTopicAndStepByBankId(String bankId);

	/**
	 * 数据清洗用查询
	 */
	public List<BankTopic> schRemainTopic(BankTopic topic);

	public void addTopicTempBatch(List<BankTopic> topics);

	public void addStepTempBatch(List<BankStep> steps);

}//end