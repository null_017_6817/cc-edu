package com.qtrmoon.manager.serdao;

import java.util.List;
import org.springframework.stereotype.Component;

import com.qtrmoon.manager.pojo.Bank;
/**
 * 题库持久层
 */
@Component
public interface BankMapper {
	/**
	 * Bank的条件查询方法
	 * @param bank 承载查询条件
	 * @return 返回Bank的集合
	 */
	public List<Bank> schBank(Bank bank);
	
	/**
	 * Bank的主键查询方法
	 * @param id 主键值
	 * @return 返回Bank实体
	 */
	public Bank schBankById(String id);
	
	/**
	 * Bank的添加方法
	 * @param bank 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addBank(Bank bank);
	
	/**
	 * Bank的修改方法
	 * @param bank 承载数据的Bean
	 */
	public void updBank(Bank bank);
	
	/**
	 * Bank的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delBank(String id);
	
	/**
	 * Bank的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delBanks(String[] ids);
}
