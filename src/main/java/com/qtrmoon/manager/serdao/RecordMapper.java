package com.qtrmoon.manager.serdao;

import java.util.List;
import org.springframework.stereotype.Component;

import com.qtrmoon.manager.pojo.Record;
/** 实体映射 */
@Component
public interface RecordMapper {
	/**
	 * Record的条件查询方法
	 * @param record 承载查询条件
	 * @return 返回Record的集合
	 */
	public List<Record> schRecord(Record record);
	
	/**
	 * Record的根据cTime,taskId ,fromNum,toNum分组统计
	 * @param record 承载查询条件
	 * @return 返回Record的集合
	 */
	public List<Record> schRecordGroupBy(Record record);
	/**
	 * Record的主键查询方法
	 * @param id 主键值
	 * @return 返回Record实体
	 */
	public Record schRecordById(Integer id);
	
	/**
	 * Record的添加方法
	 * @param record 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addRecord(Record record);
	
	/**
	 * Record的修改方法
	 * @param record 承载数据的Bean
	 */
	public void updRecord(Record record);
	
	/**
	 * Record的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delRecord(Integer id);
	
	/**
	 * Record的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delRecords(Integer[] ids);
	/**
	 * 根据任务查询录音
	 * @param record
	 * @return
	 */
	public List<Record> schRecordByTask(Record record);
	
	/**
	 * 根据任务查询录音
	 * 并根据cTime,taskId ,fromNum,toNum分组统计
	 * @param record
	 * @return
	 */
	public List<Record> schRecordByTaskGroupBy(Record record);
}
