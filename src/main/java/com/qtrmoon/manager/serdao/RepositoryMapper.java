package com.qtrmoon.manager.serdao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import com.qtrmoon.manager.pojo.Repository;
import com.qtrmoon.manager.pojo.ZdRepository;
/**
 * 知识库持久层
 */
@Component
public interface RepositoryMapper {
	/**
	 * Repository的条件查询方法
	 * @param repository 承载查询条件
	 * @return 返回Repository的集合
	 */
	public List<Repository> schRepository(Repository repository);
	
	/**
	 * Repository的主键查询方法
	 * @param id 主键值
	 * @return 返回Repository实体
	 */
	public Repository schRepositoryById(Integer id);
	
	/**
	 * Repository的查询方法
	 * @param repository 
	 * @return 返回Repository集合
	 */
	public List<Repository> schRepositoryByCutOrTitle(Repository repository);
	
	/**
	 * Repository的添加方法
	 * @param repository 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addRepository(Repository repository);
	
	/**
	 * Repository的修改方法
	 * @param repository 承载数据的Bean
	 */
	public void updRepository(Repository repository);
	
	/**
	 * Repository的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delRepository(Integer id);
	
	/**
	 * Repository的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delRepositorys(Integer[] ids);
	
	/**
	 * 修改路径
	 * @param repository
	 */
	public void updPart(Repository repository);
	/**
	 * 根据id查询知识库
	 * @param organids
	 * @return
	 */
	public ZdRepository schZdschRepositoryById(@Param("id") String organids);
	/**
	 * 根据父id查询知识库
	 * @param pid
	 * @return
	 */
	public ZdRepository schZdschRepositoryByPid(@Param("id") Integer pid);

}
