package com.qtrmoon.manager.serdao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import com.qtrmoon.book.pojo.Book;
import com.qtrmoon.manager.pojo.Course;
/**
 * 课程持久层
 */
@Component
public interface CourseMapper {
	/**
	 * Course的条件查询方法
	 * @param course 承载查询条件
	 * @return 返回Course的集合
	 */
	public List<Course> schCourse(Course course);
	
	/**
	 * Course的主键查询方法
	 * @param id 主键值
	 * @return 返回Course实体
	 */
	public Course schCourseById(Integer id);
	
	/**
	 * Course的添加方法
	 * @param course 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addCourse(Course course);
	
	/**
	 * Course的修改方法
	 * @param course 承载数据的Bean
	 */
	public void updCourse(Course course);
	
	/**
	 * Course的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delCourse(Integer id);
	
	/**
	 * Course的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delCourses(Integer[] ids);
	
	/**
	 * 修改路径
	 * @param course
	 */
	public void updPart(Course course);

	/**
	 * 根据父id查询子id
	 * @param bookId
	 */
	public List<Book> schId(@Param("id") String bookId);
}
