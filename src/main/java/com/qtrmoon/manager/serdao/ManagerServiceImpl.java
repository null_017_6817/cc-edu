package com.qtrmoon.manager.serdao;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.qtrmoon.book.pojo.Book;
import com.qtrmoon.book.util.FilePath;
import com.qtrmoon.common.OrderCache;
import com.qtrmoon.common.OrderEntity;
import com.qtrmoon.manager.constant.TopicTypeConstant;
import com.qtrmoon.manager.pojo.Bank;
import com.qtrmoon.manager.pojo.BankStep;
import com.qtrmoon.manager.pojo.BankTopic;
import com.qtrmoon.manager.pojo.Classes;
import com.qtrmoon.manager.pojo.Course;
import com.qtrmoon.manager.pojo.DirTree;
import com.qtrmoon.manager.pojo.Person;
import com.qtrmoon.manager.pojo.Record;
import com.qtrmoon.manager.pojo.Repository;
import com.qtrmoon.manager.pojo.Stud;
import com.qtrmoon.manager.pojo.ZdRepository;
import com.qtrmoon.manager.pojo.BankTheory;
import com.qtrmoon.sysmanage.SysConstant;
import com.qtrmoon.toolkit.MD5;
/**
 * 管理实现类业务层
 * @author HuaTang_DYC
 *
 */
@Service("managerService")
public class ManagerServiceImpl implements IManagerService {
	
	@Autowired
	private ClassesMapper classesMapper; // 实例化Mapper调用

	
	/***************** Classes方法组 *****************/
	/**
	 * Classes的条件查询方法
	 * @param classes 承载查询条件的Bean
	 * @return 返回Classes的集合
	 */
	@Override
	public List<Classes> schClasses(Classes classes) {
		PageHelper.startPage(classes.getPage(), classes.getPagesize());//设置分页显示
		List<Classes> list=classesMapper.schClasses(classes);// 查询数据
		Page<Classes> page = (Page<Classes>)list;//为了设置总记录数先类型强转
		classes.setDatasize(page.getTotal());// 设置长度
		return list;// 返回结果
	}
	
	/**
	 * Classes的主键查询方法
	 * @param id 主键值
	 * @return 返回Classes实体
	 */
	@Override
	public Classes schClassesById(Integer id) {
		return classesMapper.schClassesById(id);// 返回结果
	}
	
	/**
	 * Classes的添加方法
	 * @param classes 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	@Override
	public Integer addClasses(Classes classes) {
		return classesMapper.addClasses(classes);// 返回结果
	}

	/**
	 * Classes的修改方法
	 * @param classes 承载数据的Bean
	 */
	@Override
	public void updClasses(Classes classes) {
		classesMapper.updClasses(classes);// 更新数据
	}

	/**
	 * Classes的单记录删除方法
	 * @param id 要删除的主键值
	 */
	@Override
	public void delClasses(Integer id) {
		classesMapper.delClasses(id);// 删除数据
	}

	/**
	 * Classes的批量删除方法
	 * @param ids 主键值的数组
	 */
	@Override
	public void delClasses(Integer[] ids) {
		classesMapper.delClassess(ids);// 删除数据
	}



	@Autowired
	private StudMapper studMapper;// mapper调用


	/***************** Stud方法组 *****************/
	/**
	 * Stud的条件查询方法
	 * @param stud 承载查询条件的Bean
	 * @return 返回Stud的集合
	 */
	@Override
	public List<Stud> schStud(Stud stud) {
		PageHelper.startPage(stud.getPage(), stud.getPagesize());//设置分页显示
		List<Stud> list=studMapper.schStud(stud);//查询数据
		Page<Stud> page = (Page<Stud>)list;//为了设置总记录数先类型强转
		stud.setDatasize(page.getTotal());// 设置长度
		return list;// 返回结果
	}
	
	/**
	 * Stud的主键查询方法
	 * @param id 主键值
	 * @return 返回Stud实体
	 */
	@Override
	public Stud schStudById(Integer id) {
		return studMapper.schStudById(id);// // 返回结果
	}
	
	/**
	 * 根据学号查询学生
	 */
	@Override
	public Stud schStudByXh(String number){
		return studMapper.schStudByXh(number);// 返回结果
	}
	
	/**
	 * Stud的添加方法
	 * @param stud 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	@Override
	public Integer addStud(Stud stud) {
		String passwd = stud.getPasswd();// 获取值
		stud.setAgentId(null);// 设置值
		stud.setPasswd(MD5.getInstance().getMD5to32(passwd));// 设置值
		return studMapper.addStud(stud);// 添加数据
	}

	/**
	 * Stud的修改方法
	 * @param stud 承载数据的Bean
	 */
	@Override
	public void updStud(Stud stud) {
		String passwd = stud.getPasswd();// 获取值
		stud.setPasswd(MD5.getInstance().getMD5to32(passwd));// 设置值
		studMapper.updStud(stud);// 更新数据
	}
	/**
	 * 更新学生
	 */
	public void updStudData(Stud stud) {
		studMapper.updStud(stud);// 更新数据
	}

	/**
	 * Stud的单记录删除方法
	 * @param id 要删除的主键值
	 */
	@Override
	public void delStud(Integer id) {
		studMapper.delStud(id);// 删除数据
	}

	/**
	 * Stud的批量删除方法
	 * @param ids 主键值的数组
	 */
	@Override
	public void delStud(Integer[] ids) {
		studMapper.delStuds(ids);// 删除数据
	}
	/**
	 * 清除考试状态
	 * @param ids
	 */
	@Override
	public void updClearExamStu(Integer[] ids){
		if(ids != null){// 判断
			for(int i = 0; i < ids.length; i++){// 循环
				Stud stu = studMapper.schStudById(ids[i]);// 查询
				stu.setIsExam("0");// 设值
				studMapper.updStud(stu);// 更新数据
			}
		}
	}

	@Autowired
	private RepositoryMapper repositoryMapper;// Mapper调用


	/***************** Repository方法组 *****************/
	/**
	 * Repository的条件查询方法
	 * @param repository 承载查询条件的Bean
	 * @return 返回Repository的集合
	 */
	@Override
	public List<Repository> schRepository(Repository repository) {
		PageHelper.startPage(repository.getPage(), repository.getPagesize());//设置分页显示
		List<Repository> list=repositoryMapper.schRepository(repository);// 查询数据
		Page<Repository> page = (Page<Repository>)list;//为了设置总记录数先类型强转
		repository.setDatasize(page.getTotal());
		return list;// 返回结果
	}
	
	/**
	 * Repository的条件查询方法（包含搜索框）
	 * @param repository 主键值
	 * @return 返回Repository集合
	 */
	@Override
	public List<Repository> schRepositoryByCutOrTitle(Repository repository) {
		PageHelper.startPage(repository.getPage(), repository.getPagesize());//设置分页显示
		List<Repository> list=repositoryMapper.schRepositoryByCutOrTitle(repository);// 查询结果
		Page<Repository> page = (Page<Repository>)list;//为了设置总记录数先类型强转
		repository.setDatasize(page.getTotal());// 设置长度
		return list;// 返回结果
	}
	/**
	 * Repository的主键查询方法
	 * @param id 主键值
	 * @return 返回Repository实体
	 */
	@Override
	public Repository schRepositoryById(Integer id) {
		return repositoryMapper.schRepositoryById(id);// 返回结果
	}
	
	/**
	 * Repository的添加方法
	 * @param repository 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 * @throws IOException 
	 */
	@Override
	public Integer addRepository(Integer id,Repository repository, MultipartFile file, HttpSession session, HttpServletResponse response, HttpServletRequest request) throws IOException {
		if(file.getSize()>0){
			String filename = file.getOriginalFilename();  //获取文件名字作为保存到服务器的名称
			if(filename.endsWith("pdf")||filename.endsWith("xls")||filename.endsWith("jpg")||// 判断参数
				filename.endsWith("jpge")||filename.endsWith("gif")||filename.endsWith("png")||// 判断参数
				filename.endsWith("mht")||filename.endsWith("ppt")||filename.endsWith("docx")||// 判断参数
				filename.endsWith("doc")||filename.endsWith("xlsx")||filename.endsWith("pptx")){// 判断参数
				String tzm = "";// 定义常量
				if ((filename != null) && (filename.length() > 0)) { // 判断
					int dot = filename.lastIndexOf('.');   // 截取
					if ((dot >-1) && (dot < (filename.length() - 1))) {  // 判断 
						tzm = filename.substring(dot + 1);   // 截取
					}
				}
				filename = (new Date()).getTime()+"-"+((int)(Math.random()*10)+10)+"."+tzm;// 拼接
				String path = FilePath.getFilePath("repositoryAddress");// 拼接
				File f = new File(path,filename);     //进行路径拼接 前半部分路径+文件名称
				file.transferTo(f);// 上传附件
				repository.setAttachPath(filename);// 设置附件url 和附件名称
				//repository.setId(id);// 设置id，调用修改方法时有用
			}
			else
			{
				response.getWriter().write("文件格式不对！");// 提示信息
			}
		}
		Person person = (Person)request.getSession(true).getAttribute(SysConstant.CURRENT_USER);// 当前用户
		repository.setChangePeople(person.getName());// 设置属性
		repository.setChangeTime(new Date());// 设置属性
		return repositoryMapper.addRepository(repository);// 添加数据
	}
	

	/**
	 * Repository的修改方法
	 * @param repository 承载数据的Bean
	 * @throws IOException 
	 * @throws IllegalStateException 
	 */
	@Override
	public void updRepository(Integer id,Repository repository, MultipartFile file, HttpSession session, HttpServletResponse response, HttpServletRequest request) throws IllegalStateException, IOException {
		if(file.getSize()>0){
			String filename = file.getOriginalFilename();  //获取文件名字作为保存到服务器的名称
			if(filename.endsWith("pdf")||filename.endsWith("xls")||filename.endsWith("jpg")||// 判断参数
				filename.endsWith("jpge")||filename.endsWith("gif")||filename.endsWith("png")||// 判断参数
				filename.endsWith("mht")||filename.endsWith("ppt")||filename.endsWith("docx")||// 判断参数
				filename.endsWith("doc")||filename.endsWith("xlsx")||filename.endsWith("pptx")){// 判断参数
				String tzm = "";// 定义常量
				if ((filename != null) && (filename.length() > 0)) {  // 判断 
					int dot = filename.lastIndexOf('.');  // 截取 
					if ((dot >-1) && (dot < (filename.length() - 1))) {  // 判断 
						tzm = filename.substring(dot + 1); // 截取  
					}
				}
				filename = (new Date()).getTime()+"-"+((int)(Math.random()*10)+10)+"."+tzm;// 拼接
				String path = FilePath.getFilePath("repositoryAddress");// 拼接
				File f = new File(path,filename);     //进行路径拼接 前半部分路径+文件名称
				file.transferTo(f);// 上传附件
				repository.setAttachPath(filename);// 设置附件url 和附件名称
				repository.setId(id);// 设置id，调用修改方法时有用
			}else{
				response.getWriter().write("文件格式不对！");// 提示信息
			}
		}else{
			// 设置文件路径为原来的文件路径
			Repository oldrep = repositoryMapper.schRepositoryById(id);
			repository.setAttachPath(oldrep.getAttachPath());
		}
		Person person = (Person)request.getSession(true).getAttribute(SysConstant.CURRENT_USER);// 当前用户
		repository.setChangePeople(person.getName());// 设置属性
		repository.setChangeTime(new Date());// 设置属性
		repositoryMapper.updRepository(repository);// 更新数据
	}

	/**
	 * Repository的单记录删除方法
	 * @param id 要删除的主键值
	 */
	@Override
	public void delRepository(Integer id) {
		repositoryMapper.delRepository(id);//删除数据
	}

	/**
	 * Repository的批量删除方法
	 * @param ids 主键值的数组
	 */
	@Override
	public void delRepository(Integer[] ids) {
		repositoryMapper.delRepositorys(ids);//删除数据
	}


	@Autowired
	private CourseMapper courseMapper;// mapper调用


	/***************** Course方法组 *****************/
	/**
	 * Course的条件查询方法
	 * @param course 承载查询条件的Bean
	 * @return 返回Course的集合
	 */
	@Override
	public List<Course> schCourse(Course course) {
		PageHelper.startPage(course.getPage(), course.getPagesize());//设置分页显示
		List<Course> result = courseMapper.schCourse(course);// 查询数据
		Page<Course> page = (Page<Course>)result;//为了设置总记录数先类型强转
		course.setDatasize(page.getTotal());// 设置长度
		return result;// 返回结果
	}
	
	/**
	 * 根据页面传来的cc_book的pid的值 查询其所有的子id
	 * @param bookId
	 * @return
	 */
	List<String> list = new ArrayList<String>();
	public List<String> getId(String bookId) {
		@SuppressWarnings("unused")
		String id = null;// 定义变量
		if(bookId!=null){// 判断
			list.add(bookId);// 添加
		}
		List<Book> book = courseMapper.schId(bookId);//根据pid查询所有的id
		for (Book book2 : book) {// 循环
			if(book!=null&&book2.getId()!=null){// 判断
				id = String.valueOf(book2.getId());// 获取值
				if(courseMapper.schId(String.valueOf(book2.getId()))!=null){//如果book2.getId()还有子Id则调用自己
					getId(String.valueOf(book2.getId()));// 获取值
				}
			} 
		}
		return list;// 返回结果
	}
	
	/**
	 * Course的主键查询方法
	 * @param id 主键值
	 * @return 返回Course实体
	 */
	@Override
	public Course schCourseById(Integer id) {
		return courseMapper.schCourseById(id);// 返回结果
	}
	
	/**
	 * Course的添加方法
	 * @param course 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 * @throws IOException 
	 * @throws IllegalStateException 
	 */
	@Override
	public Integer addCourse(Integer id,Course course, MultipartFile file, HttpSession session, HttpServletResponse response, HttpServletRequest request) throws IllegalStateException, IOException {
		if(file.getSize()>0){
			
			Calendar calendar = Calendar.getInstance();// 获取时间
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSSS");// 时间格式
			String realName = sdf.format(calendar.getTime());// 格式化时间
			int ran = (int) (Math.random() * 10) + 10;// 两位随机数
			realName = realName + ran;// 拼接
			
			String filename = file.getOriginalFilename();  //获取文件名字作为保存到服务器的名称
			if(filename.endsWith("pdf")||filename.endsWith("xls")||filename.endsWith("jpg")||//判断
				filename.endsWith("jpge")||filename.endsWith("gif")||filename.endsWith("png")||//判断
				filename.endsWith("mht")||filename.endsWith("ppt")||filename.endsWith("docx")||//判断
				filename.endsWith("doc")||filename.endsWith("xlsx")||filename.endsWith("pptx")){//判断
				
				String extName = "";
				if (filename != null && filename.lastIndexOf(".") >= 0) {// 定义常量
					extName = filename.substring(filename.lastIndexOf("."));// 截取
				}
				
				String path = FilePath.getFilePath("courseware")+File.separator+"data"+File.separator+"doc";// 拼接
				File f = new File(path,realName + extName);     //进行路径拼接 前半部分路径+文件名称
				file.transferTo(f);// 上传附件
				String fileUrl1 = "data/doc/" + realName+extName;   //查看路径
				course.setAttachPath(fileUrl1);// 设置附件url 和附件名称
				response.getWriter().write("成功!");
			}else if(filename.endsWith("swf") || filename.endsWith("SWF") ||
					filename.endsWith("mp4") || filename.endsWith("MP4")
					|| filename.endsWith("flv") || filename.endsWith("FLV")
					|| filename.endsWith("wav") || filename.endsWith("WAV")
					|| filename.endsWith("mp3") || filename.endsWith("MP3")){
				
				String extName = "";
				if (filename != null && filename.lastIndexOf(".") >= 0) {// 定义常量
					extName = filename.substring(filename.lastIndexOf("."));// 截取
				}
				
				String path = FilePath.getFilePath("courseware")+File.separator+"data"+File.separator+"video";// 拼接
				File f = new File(path,realName + extName);     //进行路径拼接 前半部分路径+文件名称
				file.transferTo(f);// 上传附件
				String fileUrl1 = "data/video/" + realName+extName;   //查看路径
				course.setAttachPath(fileUrl1);// 设置附件url 和附件名称
				response.getWriter().write("成功!");// 提示
			}else{
				response.getWriter().write("文件格式不对！");// 提示
			}
		}
		Runtime.getRuntime().exec("chmod 777 -R " + FilePath.getFilePath("courseware"));
		return courseMapper.addCourse(course);// 添加数据
	}

	/**
	 * Course的修改方法
	 * @param course 承载数据的Bean
	 * @throws IOException 
	 */
	@Override
	public void updCourse(Integer id,Course course, MultipartFile file, HttpSession session, HttpServletResponse response, HttpServletRequest request) throws IOException {
		if(file.getSize()>0){
			
			Calendar calendar = Calendar.getInstance();// 获取时间
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSSS");// 时间格式
			String realName = sdf.format(calendar.getTime());// 格式化时间
			int ran = (int) (Math.random() * 10) + 10;// 两位随机数
			realName = realName + ran;// 拼接
			
			String filename = file.getOriginalFilename();  //获取文件名字作为保存到服务器的名称
			if(filename.endsWith("pdf")||filename.endsWith("xls")||filename.endsWith("jpg")||//判断
				filename.endsWith("jpge")||filename.endsWith("gif")||filename.endsWith("png")||//判断
				filename.endsWith("mht")||filename.endsWith("ppt")||filename.endsWith("docx")||//判断
				filename.endsWith("doc")||filename.endsWith("xlsx")||filename.endsWith("pptx")){//判断
				
				String extName = "";
				if (filename != null && filename.lastIndexOf(".") >= 0) {// 定义常量
					extName = filename.substring(filename.lastIndexOf("."));// 截取
				}
				
				String path = FilePath.getFilePath("courseware")+File.separator+"data"+File.separator+"doc";// 拼接
				File f = new File(path,realName+extName);     //进行路径拼接 前半部分路径+文件名称
				file.transferTo(f);// 上传附件
				String fileUrl1 = "data/doc/" + realName+extName;   //查看路径
				course.setAttachPath(fileUrl1);// 设置附件url 和附件名称
				response.getWriter().write("成功!");
			}else if(filename.endsWith("swf") || filename.endsWith("SWF") ||
					filename.endsWith("mp4") || filename.endsWith("MP4")
					|| filename.endsWith("flv") || filename.endsWith("FLV")
					|| filename.endsWith("wav") || filename.endsWith("WAV")
					|| filename.endsWith("mp3") || filename.endsWith("MP3")){
				
				String extName = "";
				if (filename != null && filename.lastIndexOf(".") >= 0) {// 定义常量
					extName = filename.substring(filename.lastIndexOf("."));// 截取
				}
				
				String path = FilePath.getFilePath("courseware")+File.separator+"data"+File.separator+"video";// 拼接
				File f = new File(path,realName+extName);     //进行路径拼接 前半部分路径+文件名称
				file.transferTo(f);// 上传附件
				String fileUrl1 = "data/video/" + realName+extName;   //查看路径
				course.setAttachPath(fileUrl1);// 设置附件url 和附件名称
				response.getWriter().write("成功!");
			}else{
				response.getWriter().write("文件格式不对！");
			}
		}
		Runtime.getRuntime().exec("chmod 777 -R " + FilePath.getFilePath("courseware"));
		Course entity = courseMapper.schCourseById(course.getId());// 查询数据
		if(course.getAttachPath() == null || "".equals(course.getAttachPath())){// 判断
			course.setAttachPath(entity.getAttachPath());// 判断
		}
		courseMapper.updCourse(course);// 更新数据
	}

	/**
	 * Course的单记录删除方法
	 * @param id 要删除的主键值
	 */
	@Override
	public void delCourse(Integer id) {
		courseMapper.delCourse(id);// 删除数据
	}

	/**
	 * Course的批量删除方法
	 * @param ids 主键值的数组
	 */
	@Override
	public void delCourse(Integer[] ids) {
		courseMapper.delCourses(ids);// 删除数据
	}




	@Autowired
	private BankMapper bankMapper;// Mapper调用


	/***************** Bank方法组 *****************/
	/**
	 * Bank的条件查询方法
	 * @param bank 承载查询条件的Bean
	 * @return 返回Bank的集合
	 */
	@Override
	public List<Bank> schBank(Bank bank) {
		PageHelper.startPage(bank.getPage(), bank.getPagesize());//设置分页显示
		List<Bank> list=bankMapper.schBank(bank);// 查询数据
		Page<Bank> page = (Page<Bank>)list;//为了设置总记录数先类型强转
		bank.setDatasize(page.getTotal());// 设置长度
		return list;// 返回结果
	}
	
	/**
	 * Bank的条件查询方法(无分页)
	 * @param bank 承载查询条件的Bean
	 * @return 返回Bank的集合
	 */
	@Override
	public List<Bank> schBankList(Bank bank){
		List<Bank> list=bankMapper.schBank(bank);// 查询数据
		return list;// 返回结果
	}
	
	/**
	 * Bank的主键查询方法
	 * @param id 主键值
	 * @return 返回Bank实体
	 */
	@Override
	public Bank schBankById(String id) {
		return bankMapper.schBankById(id);// 返回结果
	}
	
	/**
	 * Bank的添加方法
	 * @param bank 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	@Override
	public Integer addBank(Bank bank) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");//设置时间
		bank.setBankId(sdf.format(new Date()));//设置时间
		return bankMapper.addBank(bank);// 返回结果
	}

	/**
	 * Bank的修改方法
	 * @param bank 承载数据的Bean
	 */
	@Override
	public void updBank(Bank bank) {
		bankMapper.updBank(bank);// 更新数据
	}

	/**
	 * Bank的单记录删除方法
	 * @param id 要删除的主键值
	 */
	@Override
	public void delBank(String id) {
		bankMapper.delBank(id);// 删除数据
	}

	/**
	 * Bank的批量删除方法
	 * @param ids 主键值的数组
	 */
	@Override
	public void delBank(String[] ids) {
		bankMapper.delBanks(ids);// 删除数据
	}

	@Autowired
	private BankTopicMapper bankTopicMapper;// Mapper调用


	/***************** BankTopic方法组 *****************/
	/**
	 * BankTopic的条件查询方法
	 * @param bankTopic 承载查询条件的Bean
	 * @return 返回BankTopic的集合
	 */
	@Override
	public List<BankTopic> schBankTopic(BankTopic bankTopic) {
		PageHelper.startPage(bankTopic.getPage(), bankTopic.getPagesize());//设置分页显示
		List<BankTopic> list=bankTopicMapper.schBankTopic(bankTopic);// 查询数据
		Page<BankTopic> page = (Page<BankTopic>)list;//为了设置总记录数先类型强转
		bankTopic.setDatasize(page.getTotal());// 设置长度
		return list;// 返回结果
	}
	
	/**
	 * 查询列表，无分页
	 */
	public List<BankTopic> schBankTopicList(BankTopic bankTopic){
		List<BankTopic> list=bankTopicMapper.schBankTopic(bankTopic);// 查询数据
		return list;// 返回结果
	}
	
	/**
	 * BankTopic的主键查询方法
	 * @param id 主键值
	 * @return 返回BankTopic实体
	 */
	@Override
	public BankTopic schBankTopicById(String id) {
		return bankTopicMapper.schBankTopicById(id);// 返回结果
	}
	
	/**
	 * BankTopic的添加方法
	 * @param bankTopic 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	@Override
	public Integer addBankTopic(BankTopic bankTopic) {
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmssSSSS");// 定义时间
		String topicId = sdf.format(calendar.getTime());// 定义时间
		bankTopic.setTopicId(topicId);// 定义时间
		return bankTopicMapper.addBankTopic(bankTopic);// 添加数据
	}

	/**
	 * 上传导入题目所用的添加方法
	 */
	public Integer addBankTopicByUpload(BankTopic bankTopic) {
		return bankTopicMapper.addBankTopic(bankTopic);// 返回结果
	}
	
	/**
	 * BankTopic的修改方法
	 * @param bankTopic 承载数据的Bean
	 */
	@Override
	public void updBankTopic(BankTopic bankTopic) {
		bankTopicMapper.updBankTopic(bankTopic);// 更新数据
	}

	/**
	 * BankTopic的单记录删除方法
	 * @param id 要删除的主键值
	 */
	@Override
	public void delBankTopic(String id) {
		bankTopicMapper.delBankTopic(id);// 删除数据
	}

	/**
	 * 批量新增topic
	 */
	@Override
	public void addBankTopicBatch(List<BankTopic> list) {
		bankTopicMapper.addBatch(list);
	}

	/**
	 * BankTopic的批量删除方法
	 * @param ids 主键值的数组
	 */
	@Override
	public void delBankTopic(String [] ids) {
		if( ids != null && ids.length > 0 ){// 判断
			for( String id : ids ){//循环
				if(id != null ){// 判断
					BankTopic bankTopic = bankTopicMapper.schBankTopicById(id);// 查询
					if(bankTopic != null){// 判断
						BankStep bankStep = new BankStep();// 定义对象
						bankStep.setBankId(bankTopic.getBankId());// 设置参数
						bankStep.setTopicId(bankTopic.getTopicId());// 设置参数
						List<BankStep> stepList = bankStepMapper.schBankStep(bankStep);// 查询
						if(stepList != null && stepList.size() > 0){// 判断
							for(int i = 0; i < stepList.size(); i++){//循环
								if(stepList.get(i) != null && stepList.get(i).getId() != null){// 判断
									bankStepMapper.delBankStep(stepList.get(i).getId());// 根据stepId删除step
								}
							}
						}
						bankTopicMapper.delBankTopic(id);// 根据topicId删除topic
					}
				}
			}
		}
	}


	@Autowired
	private BankStepMapper bankStepMapper;// Mapper调用


	/***************** BankStep方法组 *****************/
	/**
	 * BankStep的条件查询方法
	 * @param bankStep 承载查询条件的Bean
	 * @return 返回BankStep的集合
	 */
	@Override
	public List<BankStep> schBankStep(BankStep bankStep) {
		PageHelper.startPage(bankStep.getPage(), bankStep.getPagesize());//设置分页显示
		List<BankStep> list=bankStepMapper.schBankStep(bankStep);// 查询数据
		Page<BankStep> page = (Page<BankStep>)list;//为了设置总记录数先类型强转
		bankStep.setDatasize(page.getTotal());// 设置长度
		return list;//返回结果
	}
	
	/**
	 * 查询步骤列表（无分页）
	 * @param bankStep
	 * @return
	 */
	@Override
	public List<BankStep> schBankStepList(BankStep bankStep) {
		List<BankStep> list=bankStepMapper.schBankStep(bankStep);// 查询数据
		return list;//返回结果
	}
	
	/**
	 * BankStep的主键查询方法
	 * @param id 主键值
	 * @return 返回BankStep实体
	 */
	@Override
	public BankStep schBankStepById(Integer id) {
		return bankStepMapper.schBankStepById(id);//返回结果
	}
	
	/**
	 * BankStep的添加方法
	 * @param bankStep 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	@Override
	public Integer addBankStep(BankStep bankStep) {
		return bankStepMapper.addBankStep(bankStep);//返回结果
	}

	/**
	 * BankStep的修改方法
	 * @param bankStep 承载数据的Bean
	 */
	@Override
	public void updBankStep(BankStep bankStep) {
		bankStepMapper.updBankStep(bankStep);//更新数据
	}

	/**
	 * BankStep的单记录删除方法
	 * @param id 要删除的主键值
	 */
	@Override
	public void delBankStep(Integer id) {
		bankStepMapper.delBankStep(id);// 删除数据
	}

	/**
	 * 批量新增step
	 */
	@Override
	public void addBankStepBatch(List<BankStep> list) {
		bankStepMapper.addBatch(list);
	}

	/**
	 * BankStep的批量删除方法
	 * @param ids 主键值的数组
	 */
	@Override
	public void delBankStep(Integer[] ids) {
		bankStepMapper.delBankSteps(ids);// 删除数据
	}
	
	/**
	 * 重置密码
	 */
	@Override
	public void resetPwd(Integer [] ids) {
		String passwd = MD5.getInstance().getMD5to32(Stud.PASSWORD);//MD5加密
		studMapper.resetPwd(ids,passwd);// 更新数据
	}

	/**
	 * 导入学生账号
	 */
	@Override
	public String addStudFile(String cnumber,List<Stud> list) {
		List<Stud> studNum = studMapper.schStudFile();	//查询全表学生的班级和学号
		StringBuffer sb = new StringBuffer();// 对象
		String number2 = null;// 常量
		String number = null;// 常量
		StringBuffer number3 = null;// 常量
		String mes ="";// 常量
		if(cnumber!=null&&cnumber!=""){					//如果传入的班级不为空
			int i = 0;									//设置坐席号每次自增1
			for (Stud stud : list) {//循环
				i++;			// 加一						
				if(cnumber!=stud.getCnumber()){// 判断
					number2 = stud.getNumber();// 常量
					stud.setCnumber(cnumber);		//设置excel的班级为传来的班级
				}
				for (Stud stud1: studNum) {// 循环
					if(stud.getNumber() != null && stud.getNumber().equals(stud1.getNumber())){//如果传来的和 表中的学号相等
						number = stud.getNumber();// 常量
						stud.setRoles("10");// 常量
						stud.setPartnerClass(cnumber);// 常量
						studMapper.updStudFile(stud,number,cnumber);//修改其他字段
						number3 =sb.append(number).append("--");// 常量
					}
				}
				
				stud.setAgentId(null);// 设置常量
				if(number2!=number){// 判断
					stud.setRoles("10");//设置角色
					stud.setPartnerClass(cnumber);//设置父班级
					stud.setPasswd(MD5.getInstance().getMD5to32(stud.PASSWORD));// 设置密码
					studMapper.addStudExcel(stud);//如果Excel的学号不等于表里的学号 直接导入
				}
			}
			if(number3 != null){// 判断
				String ss = number3==null?"":number3.toString();// 常量
				mes ="由于学号:"+ss+"在数据库中已存在,所以只修改该学号的其他信息，不新增";// 提示信息
			}
			mes = "操作成功！"+mes;// 提示信息
		}
		return mes;// 提示信息
	}
	/**
	 * 根据id查询知识库
	 * @param organids
	 */
	@Override
	public ZdRepository schZdschRepositoryById(String organids) {
		return repositoryMapper.schZdschRepositoryById(organids);// 返回结果
	}
	/**
	 * 根据父id查询数据库
	 * @param pid
	 */
	@Override
	public ZdRepository schZdschRepositoryByPid(Integer pid) {
		return repositoryMapper.schZdschRepositoryByPid(pid);// 返回结果
	}
	/**
	 * 查询除本人外的学生
	 * @param stud
	 */
	@Override
	public List<Stud> schStudNotMe(Stud stud) {
		List<Stud> list=studMapper.schStudNotMe(stud);// 查询数据
		return list;// 返回结果
	}
	/**
	 * 查询正在通话的学生
	 */
	@Override
	public List<Map<String, Object>> schStudCalling() {
		List<Map<String,Object>> result =  new ArrayList<Map<String,Object>>();// 返回集合
		List<OrderEntity> cacheList = OrderCache.cacheList;//缓存工单集合
		if(cacheList != null){// 判断
			for( OrderEntity item : cacheList){// 循环
				Map<String,Object> obj = new HashMap<String,Object>();// 返回结合
				if(item != null){// 判断
					Stud fromStu = studMapper.schStudByXh(item.getFromUser());//根据学号查询学生
					Stud st = new Stud();// 学生实体
					st.setAgentId(item.getToUser());//设置去电坐席号
					List<Stud> toStus = studMapper.schStud(st);// 查询
					if(fromStu != null && toStus != null && toStus.size() == 1){// 判断
						Stud toStu = toStus.get(0);// 获取元素
						obj.put("fromStu", fromStu);	// 封装对象拨打人 fromStu
						obj.put("toStu", toStu);		// 封装对象接听人 toStu
					}else{
						continue;// 继续循环
					}
					result.add(obj);//填充对象
				}else{
					continue;// 继续循环
				}
			}
		}
		return null;// 返回结果
	}
	


	@Autowired
	private RecordMapper recordMapper;// mapper 调用


	/***************** Record方法组 *****************/
	
	public List<Record> schRecordList(Record record){
		List<Record> list=recordMapper.schRecord(record);// 查询数据
		return list;// 返回结果
	}
	
	/**
	 * Record的条件查询方法
	 * @param record 承载查询条件的Bean
	 * @return 返回Record的集合
	 */
	@Override
	public List<Record> schRecord(Record record) {
		PageHelper.startPage(record.getPage(), record.getPagesize());//设置分页显示
		//List<Record> list=recordMapper.schRecord(record);// 查询数据
		List<Record> list=recordMapper.schRecordGroupBy(record);// 查询数据
		Page<Record> page = (Page<Record>)list;//为了设置总记录数先类型强转
		record.setDatasize(page.getTotal());// 设置长度
		return list;// 返回结果
	}
	/**
	 * Record的条件查询方法
	 * @param record 承载查询条件的Bean
	 * @return 返回Record的集合
	 */
	@Override
	public List<Record> schRecordByTask(Record record) {
		PageHelper.startPage(record.getPage(), record.getPagesize());//设置分页显示
//		List<Record> list=recordMapper.schRecordByTask(record);// 查询数据
		List<Record> list=recordMapper.schRecordByTaskGroupBy(record);// 查询数据
		Page<Record> page = (Page<Record>)list;//为了设置总记录数先类型强转
		record.setDatasize(page.getTotal());// 设置长度
		return list;// 返回结果
	}
	
	/**
	 * Record的主键查询方法
	 * @param id 主键值
	 * @return 返回Record实体
	 */
	@Override
	public Record schRecordById(Integer id) {
		return recordMapper.schRecordById(id);// 返回结果
	}
	
	/**
	 * Record的添加方法
	 * @param record 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	@Override
	public Integer addRecord(Record record) {
		return recordMapper.addRecord(record);// 添加数据
	}

	/**
	 * Record的修改方法
	 * @param record 承载数据的Bean
	 */
	@Override
	public void updRecord(Record record) {
		recordMapper.updRecord(record);// 更新数据
	}

	/**
	 * Record的单记录删除方法
	 * @param id 要删除的主键值
	 */
	@Override
	public void delRecord(Integer id) {
		recordMapper.delRecord(id);// 删除数据
	}

	/**
	 * Record的批量删除方法
	 * @param ids 主键值的数组
	 */
	@Override
	public void delRecord(Integer[] ids) {
		recordMapper.delRecords(ids);// 删除数据
	}
	/**
	 * 隐藏已加入题目
	 * @param bankTopic
	 */
	@Override
	public List<BankTopic> schBankTopicNotExer(BankTopic bankTopic) {

		PageHelper.startPage(bankTopic.getPage(), bankTopic.getPagesize());//设置分页显示
		List<BankTopic> list=bankTopicMapper.schBankTopicNotExer(bankTopic);// 查询数据
		Page<BankTopic> page = (Page<BankTopic>)list;//为了设置总记录数先类型强转
		bankTopic.setDatasize(page.getTotal());//设置长度
		return list;// 返回结果
	}
	/**
	 * 查询所有学生
	 * @param stud
	 */
	@Override
	public List<Stud> schStudAll(Stud stud) {
		List<Stud> list=studMapper.schStud(stud);// 查询数据
		return list;// 返回结果
	}
	
	@Autowired
	private DirTreeMapper dirTreeMapper;
	
	@Override
	public Integer addDirTree(DirTree tree) {
		Integer id = dirTreeMapper.addDirTree(tree);
		return id;
	}
	
	@Override
	public List<DirTree> schDirTree(String sign) {
		List<DirTree> list = new ArrayList<DirTree>();
		list = dirTreeMapper.schDirTree(sign);
		return list;
	}
	
	@Override
	public DirTree schDirTreeById(Integer id) {
		DirTree dt = dirTreeMapper.schDirTreeById(id);
		return dt;
	}
	
	@Override
	public Integer updDirTree(DirTree tree){
		Integer updNum = dirTreeMapper.updDirTree(tree);
		return updNum;
	}

	@Override
	public Map<String,Object> updGroupNum(Map<String,Object> param ){
		Map<String,Object> result = new HashMap<String,Object>();
		List<BankTopic> stepList = bankStepMapper.schStepByAutoPage(param);
		result = countStepNum(stepList);
		return result;
	}
	
	/**
	 * 统计题目数目
	 * @param bankTopics
	 * @return
	 */
	private Map<String,Object> countStepNum(List<BankTopic> topics){
		int singleG = 0, singleY = 0, singleZ = 0, singleN = 0; // 单选总数，初级数目，中级数目，高级数目
		int multiG = 0, multiY = 0, multiZ = 0, multiN = 0; // 多选总数，初级数目，中级数目，高级数目
		int answerG = 0, answerY = 0, answerZ = 0, answerN = 0; // 问答总数，初级数目，中级数目，高级数目
		int judgeG = 0, judgeY = 0, judgeZ = 0, judgeN = 0; // 判断总数，初级数目，中级数目，高级数目
		int clozedG = 0, clozedY = 0, clozedZ = 0, clozedN = 0; // 填空总数，初级数目，中级数目，高级数目
		for (BankTopic item : topics) {
			if (TopicTypeConstant.SINGLE.equals(item.getStepType())) {// 单选题
				singleG++;
				if (TopicTypeConstant.PRIMARY// 判断
						.equals(item.getDifficulty())) {// 初级
					singleY++;// 数量加一
				}
				if (TopicTypeConstant.MIDDLE.equals(item// 判断
						.getDifficulty())) {// 中级
					singleZ++;// 数量加一
				}
				if (TopicTypeConstant.HIGH.equals(item.getDifficulty())) {// 高级
					singleN++;// 数量加一
				}
			}
			if (TopicTypeConstant.MULTI.equals(item.getStepType())) {// 多选题
				multiG++;
				if (TopicTypeConstant.PRIMARY// 判断
						.equals(item.getDifficulty())) {// 初级
					multiY++;// 数量加一
				}
				if (TopicTypeConstant.MIDDLE.equals(item// 判断
						.getDifficulty())) {// 中级
					multiZ++;// 数量加一
				}
				if (TopicTypeConstant.HIGH.equals(item.getDifficulty())) {// 高级
					multiN++;// 数量加一
				}
			}
			if (TopicTypeConstant.ANSWER.equals(item.getStepType())) {// 问答题
				answerG++;
				if (TopicTypeConstant.PRIMARY// 判断
						.equals(item.getDifficulty())) {// 初级
					answerY++;// 数量加一
				}
				if (TopicTypeConstant.MIDDLE.equals(item// 判断
						.getDifficulty())) {// 中级
					answerZ++;// 数量加一
				}
				if (TopicTypeConstant.HIGH.equals(item.getDifficulty())) {// 高级
					answerN++;// 数量加一
				}
			}
			if (TopicTypeConstant.JUDGE.equals(item.getStepType())) {// 判断题
				judgeG++;
				if (TopicTypeConstant.PRIMARY.equals(item.getDifficulty())) {// 初级
					judgeY++;// 数量加一
				}
				if (TopicTypeConstant.MIDDLE.equals(item// 判断
						.getDifficulty())) {// 中级
					judgeZ++;// 数量加一
				}
				if (TopicTypeConstant.HIGH.equals(item.getDifficulty())) {// 高级
					judgeN++;// 数量加一
				}
			}
			if (TopicTypeConstant.CLOZED.equals(item.getStepType())) {// 填空题
				clozedG++;
				if (TopicTypeConstant.PRIMARY.equals(item.getDifficulty())) {// 初级
					clozedY++;// 数量加一
				}
				if (TopicTypeConstant.MIDDLE.equals(item.getDifficulty())) {// 中级
					clozedZ++;// 数量加一
				}
				if (TopicTypeConstant.HIGH.equals(item.getDifficulty())) {// 高级
					clozedN++;// 数量加一
				}
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();// 定义对象
		Map<String, Integer> single = new HashMap<String, Integer>();// 定义对象
		single.put("singleG", singleG);// 设置属性 单选共
		single.put("singleY", singleY);// 设置属性 单选易
		single.put("singleZ", singleZ);// 设置属性 单选中
		single.put("singleN", singleN);// 设置属性 单选难
		result.put("single", single);// 设置属性
		Map<String, Integer> multi = new HashMap<String, Integer>();// 定义对象
		multi.put("multiG", multiG);// 设置属性 多选共
		multi.put("multiY", multiY);// 设置属性 多选易
		multi.put("multiZ", multiZ);// 设置属性 多选中
		multi.put("multiN", multiN);// 设置属性 多选难
		result.put("multi", multi);// 设置属性
		Map<String, Integer> answer = new HashMap<String, Integer>();// 定义对象
		answer.put("answerG", answerG);// 设置属性 问答共
		answer.put("answerY", answerY);// 设置属性 问答易
		answer.put("answerZ", answerZ);// 设置属性 问答中
		answer.put("answerN", answerN);// 设置属性 问答难
		result.put("answer", answer);// 设置属性
		Map<String, Integer> judge = new HashMap<String, Integer>();// 定义对象
		judge.put("judgeG", judgeG);// 设置属性 判断共
		judge.put("judgeY", judgeY);// 设置属性 判断易
		judge.put("judgeZ", judgeZ);// 设置属性 判断中
		judge.put("judgeN", judgeN);// 设置属性 判断难
		result.put("judge", judge);// 设置属性
		Map<String, Integer> clozed = new HashMap<String, Integer>();// 定义对象
		clozed.put("clozedG", clozedG);// 设置属性 填空共
		clozed.put("clozedY", clozedY);// 设置属性 填空易
		clozed.put("clozedZ", clozedZ);// 设置属性 填空中
		clozed.put("clozedN", clozedN);// 设置属性 填空难
		result.put("clozed", clozed);// 设置属性
		return result;
	}

	/**
	 * 修改题目总分
	 */
	public void updBankTopicTotalScore(BankTopic bankTopic) {
		bankTopicMapper.updBankTopicTotalScore(bankTopic);		
	}

	public List<BankTheory> schTopicAndStepByBankId(String bankId) {
		return bankTopicMapper.schTopicAndStepByBankId(bankId);
	}

	public List<BankTopic> schRemainTopic(BankTopic topic) {
		return bankTopicMapper.schRemainTopic(topic);
	}

	public void addTopicTempBatch(List<BankTopic> topics) {
		bankTopicMapper.addBatchTemp(topics);
	}

	public void addStepTempBatch(List<BankStep> steps) {
		bankStepMapper.addBatchTemp(steps);
	}
	
}//end
