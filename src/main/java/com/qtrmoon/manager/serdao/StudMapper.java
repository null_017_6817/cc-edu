package com.qtrmoon.manager.serdao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import com.qtrmoon.manager.pojo.Stud;
/**
 * 学生持久层
 */
@Component
public interface StudMapper {
	/**
	 * Stud的条件查询方法
	 * @param stud 承载查询条件
	 * @return 返回Stud的集合
	 */
	public List<Stud> schStud(Stud stud);
	
	/**
	 * Stud的主键查询方法
	 * @param id 主键值
	 * @return 返回Stud实体
	 */
	public Stud schStudById(Integer id);
	
	/**
	 * Stud的添加方法
	 * @param stud 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addStud(Stud stud);
	
	/**
	 * Stud的修改方法
	 * @param stud 承载数据的Bean
	 */
	public void updStud(Stud stud);
	
	/**
	 * Stud的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delStud(Integer id);
	
	/**
	 * Stud的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delStuds(Integer[] ids);
	
	/**
	 * Stud根据id重置密码
	 * @param ids
	 * @param passwd 
	 */
	public void resetPwd(@Param("ids") Integer[] ids, @Param("passwd") String passwd);
	
	/**
	 * 查询所以班级和学号
	 */
	public List<Stud> schStudFile();
	
	/**
	 * 查询当前最大的坐席号
	 * @param stud
	 */
	public Stud schStudAgentId(@Param("stud") Stud stud);
	
	/**
	 * 修改字段
	 * @param stud 
	 * @param cnumber 
	 * @param number2 
	 */
	public void updStudFile(@Param("stud") Stud stud, @Param("number") String number, @Param("cnumber") String cnumber);
	
	/**
	 * 导入学生账号
	 * @param stud
	 */
	public void addStudExcel(@Param("stud") Stud stud);
	
	/**
	 * 根据学号查询学生
	 * @param number
	 * @return
	 */
	public Stud schStudByXh(String number);
	/**
	 * 筛选学生不是自己
	 * @param stud
	 * @return
	 */
	public List<Stud> schStudNotMe(Stud stud);
}
