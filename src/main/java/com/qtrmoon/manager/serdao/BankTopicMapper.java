package com.qtrmoon.manager.serdao;

import java.util.List;
import org.springframework.stereotype.Component;

import com.qtrmoon.manager.pojo.BankTopic;
import com.qtrmoon.manager.pojo.BankTheory;
/**
 * 题目持久层
 */
@Component
public interface BankTopicMapper {
	/**
	 * BankTopic的条件查询方法
	 * @param bankTopic 承载查询条件
	 * @return 返回BankTopic的集合
	 */
	public List<BankTopic> schBankTopic(BankTopic bankTopic);
	
	/**
	 * BankTopic的主键查询方法
	 * @param id 主键值
	 * @return 返回BankTopic实体
	 */
	public BankTopic schBankTopicById(String id);
	
	/**
	 * BankTopic的添加方法
	 * @param bankTopic 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addBankTopic(BankTopic bankTopic);
	
	/**
	 * BankTopic的修改方法
	 * @param bankTopic 承载数据的Bean
	 */
	public void updBankTopic(BankTopic bankTopic);
	
	/**
	 * BankTopic的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delBankTopic(String id);
	
	/**
	 * BankTopic的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delBankTopics(String[] ids);

	/**
	 * 查询数据
	 * @param bankTopic
	 * @return
	 */
	public List<BankTopic> schBankTopicNotExer(BankTopic bankTopic);

	/**
	 * 批量新增topic
	 */
	public void addBatch(List<BankTopic> list);

	/**
	 * 清洗数据 批量新增topic
	 */
	public void addBatchTemp(List<BankTopic> list);
	
	/**
	 * 修改topic totalScore
	 */
	public void updBankTopicTotalScore(BankTopic bankTopic);

	/**
	 * 查询理论题
	 */
	public List<BankTheory> schTopicAndStepByBankId(String bankId);

	/**
	 * 数据清洗用查询
	 */
	public List<BankTopic> schRemainTopic(BankTopic topic);

}
