package com.qtrmoon.manager.serdao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.qtrmoon.manager.pojo.Classes;
/**
 * 班级持久层
 */
@Component
public interface ClassesMapper {
	/**
	 * Classes的条件查询方法
	 * @param classes 承载查询条件
	 * @return 返回Classes的集合
	 */
	public List<Classes> schClasses(Classes classes);
	
	/**
	 * Classes的主键查询方法
	 * @param id 主键值
	 * @return 返回Classes实体
	 */
	public Classes schClassesById(Integer id);
	
	/**
	 * Classes的添加方法
	 * @param classes 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addClasses(Classes classes);
	
	/**
	 * Classes的修改方法
	 * @param classes 承载数据的Bean
	 */
	public void updClasses(Classes classes);
	
	/**
	 * Classes的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delClasses(Integer id);
	
	/**
	 * Classes的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delClassess(Integer[] ids);
	
}
