package com.qtrmoon.manager.constant;

/**
 * topic类型
 * @Class Name TopicTypeConstant
 */
public interface TopicTypeConstant {

	public static String SINGLE = "1";	// 单选
	public static String MULTI = "2";	// 多选
	public static String ANSWER = "3";	// 问答
	public static String JUDGE = "4";	// 判断
	public static String CLOZE = "5";	// 听打录入
	public static String REPEAT = "6";	// 复读
	public static String STATE = "7";	// 应答
	public static String VOICE = "8";	// 语音
	public static String CLOZED = "9";	// 填空
	public static String ZHTX = "10";	// 综合
	public static String ANALYZE = "11"; // 数据分析
	public static String VERBAL = "12"; // 话术编写
	public static String STEP_CAMPING = "13"; // 实训营地
	public static String STEP_DRAW = "14"; // 画图连线题

	public static String SINGLE_STR = "单选";	// 单选
	public static String MULTI_STR = "多选";	// 多选
	public static String ANSWER_STR = "问答";	// 问答
	public static String JUDGE_STR = "判断";	// 判断
	public static String CLOZE_STR = "听打录入";	// 听打录入
	public static String REPEAT_STR = "复读";	// 复读
	public static String STATE_STR = "应答";	// 应答
	public static String VOICE_STR = "语音";	// 语音
	public static String CLOZED_STR = "填空";	// 填空
	public static String ZHTX_STR = "综合";	// 综合
	public static String ANALYZE_STR = "数据分析"; // 数据分析
	public static String VERBAL_STR = "话术编写"; // 话术编写
	public static String STEP_CAMPING_STR = "实训营地"; // 实训营地
	public static String STEP_DRAW_STR = "画图连线题"; // 画图连线题
	
	public static final String THEORY = "1";	// 理论题
	public static final String PRACTICAL = "2"; // 实训题
	public static final String SKILL = "3"; // 技能训练题
	public static final String CAMPING = "4"; // 实训营地题

	public static final String THEORY_STR = "理论题";	// 理论题
	public static final String PRACTICAL_STR = "实训题"; // 实训题
	public static final String SKILL_STR = "技能训练题"; // 技能训练题
	public static final String CAMPING_STR = "实训营地题"; // 实训营地题
	
	public static final String PRACTICAL_SECTION = "1"; // 课程实训
	public static final String PRACTICAL_PROFESSION = "2"; // 行业实训

	public static String PRIMARY = "1"; // 难度
	public static String MIDDLE = "2"; // 难度
	public static String HIGH = "3"; // 难度

	public static String PRIMARY_STR = "初级";	// 难度
	public static String MIDDLE_STR = "中级";	// 难度
	public static String HIGH_STR = "高级";	// 难度
	
	public static String JUDGE_TRUE = "对"; // 判断题答案
	public static String JUDGE_FALSE = "错"; // 判断题答案

	public static char[] OPTIONS = {'A', 'B', 'C', 'D', 
		'E', 'F', 'G', 'H', 'H', 'I', 'J', 'K', 'L', 
		'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 
		'V', 'W', 'X', 'Y', 'Z'};
}
