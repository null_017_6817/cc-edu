package com.qtrmoon.manager.constant;

/**
 * 是和否的常量类（需要与字典yesOrNo.xml同步）
 */
public interface YesOrNoConstant {

	public static String YES = "1";// 是
	public static String NO = "0";// 否
}
