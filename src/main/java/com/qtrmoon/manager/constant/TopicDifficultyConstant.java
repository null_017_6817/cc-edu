package com.qtrmoon.manager.constant;

/**
 * topic难度等级
 * @Class Name TopicDifficultyConstant
 */
public interface TopicDifficultyConstant {

	public static String PRIMARY = "1"; // 初级
	public static String INTERMEDIATE = "2"; // 中级
	public static String SENIOR = "3"; // 高级
}
