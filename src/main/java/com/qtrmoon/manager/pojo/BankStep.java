package com.qtrmoon.manager.pojo;
import org.json.simple.JSONObject;

import com.qtrmoon.common.PageForm;
/** 答题步骤Object类 */
public class BankStep extends PageForm{
	//Fields
	
	private String vccId;// 分校编号
	private Integer id;// 主键
	private String bankId;// 题库编号
	private String topicId;// 考题编号
	private String stepId;// 步骤编号
	private String type;// 步骤类型
	private String title;// 标题
	private String score;// 分值
	private String preStepId;// 上一步
	private String param;// 参数
	private String standardAnswer;// 标准答案
	private Integer orderNo;// 排序编号
	
	private String difficulty;// 难度(自动组卷用)
	
	//Constructors
	/** default constructor */
	public BankStep() {
	
	}	
	//getter and setter
	/** 获取分校编号 */
	public String getVccId() {
		return this.vccId;
	}
	/** 设置分校编号 */
	public void setVccId(String vccId) {
		this.vccId = vccId;
	}
	/** 获取主键 */
	public Integer getId() {
		return this.id;
	}
	/** 设置主键 */
	public void setId(Integer id) {
		this.id = id;
	}
	/** 获取题库编号 */
	public String getBankId() {
		return this.bankId;
	}
	/** 设置题库编号 */
	public void setBankId(String bankId) {
		this.bankId = bankId;
	}
	/** 获取考题编号 */
	public String getTopicId() {
		return this.topicId;
	}
	/** 设置考题编号 */
	public void setTopicId(String topicId) {
		this.topicId = topicId;
	}
	/** 获取步骤编号 */
	public String getStepId() {
		return this.stepId;
	}
	/** 设置步骤编号 */
	public void setStepId(String stepId) {
		this.stepId = stepId;
	}
	/** 获取步骤类型 */
	public String getType() {
		return this.type;
	}
	/** 设置步骤类型 */
	public void setType(String type) {
		this.type = type;
	}
	/** 获取标题 */
	public String getTitle() {
		return this.title;
	}
	/** 设置标题 */
	public void setTitle(String title) {
		this.title = title;
	}
	/** 获取分值 */
	public String getScore() {
		return score;
	}
	public void setScore(String score) {
		this.score = score;
	}
	/** 获取上一步 */
	public String getPreStepId() {
		return this.preStepId;
	}
	/** 设置上一步 */
	public void setPreStepId(String preStepId) {
		this.preStepId = preStepId;
	}
	/** 获取参数 */
	public String getParam() {
		return param;
	}
	/** 设置参数 */
	public void setParam(String param) {
		this.param = param;
	}
	/** 获得标准答案 */
	public String getStandardAnswer() {
		return standardAnswer;
	}
	/** 设置标准答案 */
	public void setStandardAnswer(String standardAnswer) {
		this.standardAnswer = standardAnswer;
	}
	/** 获取排序编号 */
	public Integer getOrderNo() {
		return orderNo;
	}
	/** 设置排序编号 */
	public void setOrderNo(Integer orderNo) {
		this.orderNo = orderNo;
	}
	
	public String getDifficulty() {
		return difficulty;
	}
	public void setDifficulty(String difficulty) {
		this.difficulty = difficulty;
	}
	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj=new JSONObject();
		obj.put("vccId", vccId);
		obj.put("id", id);
		obj.put("bankId", bankId);
		obj.put("topicId", topicId);
		obj.put("stepId", stepId);
		obj.put("type", type);
		obj.put("title", title);
		obj.put("score", score);
		obj.put("param", param);
		obj.put("preStepId", preStepId);
		obj.put("standardAnswer", standardAnswer);
		return obj;
	}
	
	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj=new JSONObject();
		obj.put("vccId", vccId);
		obj.put("id", id);
		obj.put("bankId", bankId);
		obj.put("topicId", topicId);
		obj.put("stepId", stepId);
		obj.put("type", type);
		obj.put("title", title);
		obj.put("score", score);
		obj.put("param", param);
		obj.put("preStepId", preStepId);
		obj.put("standardAnswer", standardAnswer);
		return obj;
	}
}
