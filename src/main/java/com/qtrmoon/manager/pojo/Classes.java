package com.qtrmoon.manager.pojo;
import java.util.Date;
import java.util.Set;
import java.util.HashSet;
import org.json.simple.JSONObject;
import com.qtrmoon.common.PageForm;
import com.qtrmoon.toolkit.DateTransfer;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.dictionary.bsd.DictBean;
	/** 班级Object类 */
	public class Classes extends PageForm{
	//Fields
	
	private Integer id;// 
	private String number;// 班级号
	private String name;// 班级名称
	private Date createTime;// 创建时间
	private String createTimeBeg,createTimeEnd;//时间条件的跨度查询属性
	private String remark;// 备注
	//Constructors
	/** default constructor */
	public Classes() {
	
	}	
	//getter and setter
	/** 获取 */
	public Integer getId() {
		return this.id;
	}
	/** 设置 */
	public void setId(Integer id) {
		this.id = id;
	}
	/** 获取班级号 */
	public String getNumber() {
		return this.number;
	}
	/** 设置班级号 */
	public void setNumber(String number) {
		this.number = number;
	}
	/** 获取班级名称 */
	public String getName() {
		return this.name;
	}
	/** 设置班级名称 */
	public void setName(String name) {
		this.name = name;
	}
	/** 获取创建时间 */
	public Date getCreateTime() {
		return this.createTime;
	}
	/** 设置创建时间 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/** 设定[创建时间]时间转载字段值，页面提交时由springmvc自动装载 */
	public void setCreateTimestr(String tm) {
		this.createTime=_getTime(tm);
	}
	/** 获取[创建时间]时间起始条件 */
	public String getCreateTimeBeg() {
		return createTimeBeg;
	}
	/** 获取[创建时间]时间起始条件(Dao/Mapper查询时调用) */
	public Date getCreateTimeBegDate() {
		return _getBegDate(createTimeBeg);
	}
	/** 设置[创建时间]时间起始条件(表单提交时自动装载) */
	public void setCreateTimeBeg(String createTimeBeg) {
		this.createTimeBeg = createTimeBeg;
	}
	
	/** 获取[创建时间]时间结束条件 */
	public String getCreateTimeEnd() {
		return createTimeEnd;
	}
	/** 获取[创建时间]时间结束条件(Dao/Mapper查询时调用) */
	public Date getCreateTimeEndDate() {
		return _getEndDate(createTimeEnd);
	}
	/** 设置[创建时间]时间结束条件(表单提交时自动装载) */
	public void setCreateTimeEnd(String createTimeEnd) {
		this.createTimeEnd = createTimeEnd;
	}
	/** 获取备注 */
	public String getRemark() {
		return this.remark;
	}
	/** 设置备注 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("number", number);
		obj.put("name", name);
		//时间字段处理。createTime查询列表页使用，createTimestr修改页使用
		obj.put("createTime", DateTransfer.toString(createTime,"yyyy-MM-dd"));
		obj.put("createTimestr", DateTransfer.toString(createTime,"yyyy-MM-dd"));
		obj.put("remark", remark);
		return obj;
	}
	
	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("number", number);
		obj.put("name", name);
		//时间字段处理。createTime查询列表页使用，createTimestr修改页使用
		obj.put("createTime", DateTransfer.toString(createTime,"yyyy-MM-dd"));
		obj.put("createTimestr", DateTransfer.toString(createTime,"yyyy-MM-dd"));
		obj.put("remark", remark);
		return obj;
	}
}
