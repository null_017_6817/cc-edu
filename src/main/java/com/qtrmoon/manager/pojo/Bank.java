package com.qtrmoon.manager.pojo;
import org.json.simple.JSONObject;

import com.qtrmoon.common.PageForm;
import com.qtrmoon.dictionary.DictBuffer;
/** 题库Object类 */
public class Bank extends PageForm{
	//Fields
	
	private String vccId;		// 分校编号
	private String bankId;		// 题库编号
	private String bankName;	// 题库名
	private String level;		// 级别
	private String profession;	// 行业
	private String remark;		// 备注
	private String createTime;	// 创建时间
	private Integer status;		// 状态
	private Integer bookId;		// 课本
	private String type;		// 类型
	private String isTheory;	// 是否理论题库
	
	//Constructors
	/** default constructor */
	public Bank() {
	
	}	
	//getter and setter
	/** 获取分校编号 */
	public String getVccId() {
		return this.vccId;
	}
	/** 设置分校编号 */
	public void setVccId(String vccId) {
		this.vccId = vccId;
	}
	/** 获取题库编号 */
	public String getBankId() {
		return this.bankId;
	}
	/** 设置题库编号 */
	public void setBankId(String bankId) {
		this.bankId = bankId;
	}
	/** 获取题库名 */
	public String getBankName() {
		return this.bankName;
	}
	/** 设置题库名 */
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	/** 获取级别 */
	public String getLevel() {
		return this.level;
	}
	/** 设置级别 */
	public void setLevel(String level) {
		this.level = level;
	}
	/** 获取行业 */
	public String getProfession() {
		return this.profession;
	}
	/** 设置行业 */
	public void setProfession(String profession) {
		this.profession = profession;
	}
	/** 获取备注 */
	public String getRemark() {
		return this.remark;
	}
	/** 设置备注 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/** 获取创建时间 */
	public String getCreateTime() {
		return this.createTime;
	}
	/** 设置创建时间 */
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	/** 获取状态 */
	public Integer getStatus() {
		return this.status;
	}
	/** 设置状态 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/** 设置BookId*/
	public Integer getBookId() {
		return bookId;
	}
	/** 获取BookId*/
	public void setBookId(Integer bookId) {
		this.bookId = bookId;
	}
	/** 获取Type*/
	public String getType() {
		return type;
	}
	/** 设置Type*/
	public void setType(String type) {
		this.type = type;
	}
	// 获取是否理论题库
	public String getIsTheory() {
		return isTheory;
	}
	// 设置是否理论题库
	public void setIsTheory(String isTheory) {
		this.isTheory = isTheory;
	}
	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj=new JSONObject();
		obj.put("vccId", vccId);
		obj.put("bankId", bankId);
		obj.put("bankName", bankName);
		obj.put("level", level);
		obj.put("profession", profession);
		obj.put("remark", remark);
		obj.put("createTime", createTime);
		obj.put("status", status);
		obj.put("bookId", bookId);
		obj.put("type", type);
		obj.put("isTheory", isTheory);
		return obj;
	}
	
	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj=new JSONObject();
		obj.put("vccId", vccId);
		obj.put("bankId", bankId);
		obj.put("bankName", bankName);
		if(level!=null&&!level.equals("")){//字典id值转label
			obj.put("level", DictBuffer.getLabel("ZD_BANKLEVEL", level.toString()));
		}
		obj.put("profession", profession);
		obj.put("remark", remark);
		obj.put("createTime", createTime);
		obj.put("status", status);
		obj.put("bookId", bookId);
		if(status!=null&&!status.equals("")){//字典id值转label
			obj.put("status", DictBuffer.getLabel("ZD_BANKSTATUS", status.toString()));
		}
		//obj.put("type", type);
		if(type!=null&&!type.equals("")){//字典id值转label
			obj.put("type", DictBuffer.getLabel("ZD_BANKTYPE", type.toString()));
		}
		if(isTheory != null && !"".equals(isTheory)){
			obj.put("isTheory", DictBuffer.getLabel("ZD_SFBZ", isTheory.toString()));
		}else{
			obj.put("isTheory", "否");
		}
		return obj;
	}
	
	
	
}
