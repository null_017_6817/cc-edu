package com.qtrmoon.manager.pojo;

import java.util.List;

import org.json.simple.JSONObject;

import com.qtrmoon.sysmanage.pojo.Function;
import com.qtrmoon.sysmanage.pojo.Organ;
/**
 * 用户接口
 * @author HuaTang_DYC
 *
 */
public interface Person {
	/**
	 * 获取主键
	 * @return
	 */
	public Integer getId();
	/**
	 * 获取学号
	 * @return
	 */
	public String getNumber();
	/**
	 * 获取姓名
	 * @return
	 */
	public String getName();
	/**
	 * 设置姓名
	 * @param name
	 */
	public void setName(String name);
	/**
	 * 获取状态
	 * @return
	 */
	public String getState();
	/**
	 * 设置状态
	 * @param state
	 */
	public void setState(String state);
	public String getPasswd();// 密码
	public String getCnumber();// 班级号
	/**
	 * 获取角色
	 * @return
	 */
	public String getRoles();
	/**
	 * 设置功能菜单
	 * @param flist
	 */
	public void setMenu(List<Function> flist);
	/**
	 * 获取功能菜单
	 * @return
	 */
	public List<Function> getMenu();
	/**
	 * 设置登录ip
	 * @param ip
	 */
	public void setLoginIp(String ip);
	/**
	 * 获取登录ip
	 * @return
	 */
	public String getLoginIp();
	/** 老师有所属机构,学生没有所属机构 */
	public void setOrgan(Organ organ);
	/**
	 * 获取机构
	 * @return
	 */
	public Organ getOrgan();
	/** 老师返回所属Organid,学生返回所属classid */
	public Integer getOrganid();
	/**
	 * 获取坐席号
	 * @return
	 */
	public String getAgentId();
	/**
	 * 设置坐席号
	 * @param agentId
	 */
	
	public void setAgentId(String agentId);
	/**
	 * 获取章节
	 * @return
	 */
	public Integer getTaskId();
	/**
	 * 设置章节
	 * @param taskId
	 */
	public void setTaskId(Integer taskId);
	
	/**
	 * 获取sessionid
	 * @return
	 */
	public String getSessionId();
	/**
	 * 设置sessionid
	 * @param sessionId
	 */
	public void setSessionId(String sessionId);
	
	//通话状态
	public Integer getCallState();
	/**
	 * 设置通话状态
	 * @param callState
	 */
	public void setCallState(Integer callState);
	/**
	 * 获取行业
	 * @return
	 */
	public String getProfession();
	/**
	 * 设置行业
	 * @param profession
	 */
	public void setProfession(String profession);
	/**
	 * 获取字典转换后的json
	 * @return
	 */
	public JSONObject getJsonInDict();
	/**
	 * 获取对方信息
	 * @return
	 */
	public List<String> getTo();
	/**
	 * 设置对方信息
	 * @param to
	 */
	public void setTo(List<String> to);
	/**
	 * 获取应答状态
	 * @return
	 */
	public Integer getAnswerState();
	/**
	 * 设置应答状态
	 * @param answerState
	 */
	public void setAnswerState(Integer answerState);
	/**
	 * 获取打电话时间
	 * @return
	 */
	public String getCallTime();
	/**
	 * 设置打电话时间
	 * @param callTime
	 */
	public void setCallTime(String callTime);
	
	public void setTaskType(String type);
	
	public String getTaskType();
}
