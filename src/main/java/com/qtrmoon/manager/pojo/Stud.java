package com.qtrmoon.manager.pojo;
import java.util.List;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.json.simple.JSONObject;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.qtrmoon.common.PageForm;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.sysmanage.Online;
import com.qtrmoon.sysmanage.pojo.Function;
import com.qtrmoon.sysmanage.pojo.Organ;
import com.qtrmoon.util.LoginStudUtil;
/** 学生Object类 */
public class Stud extends PageForm implements Person, HttpSessionBindingListener{
	//Fields
	
	private Integer id;// 主键
	private String number;// 学号
	private String name;// 姓名
	private String cnumber;// 班级号
	private String sex;// 性别
	private String info;// 资料
	private String passwd;// 密码
	private String agentId;// CC座席号
	private String login;// 登录
	private String callExpired;// 通话过期时刻
	private Integer partnerType;// 人员类型(备用)
	private String partnerClass;// 班级/教研室
	private String partnerId;// 学号/教师
	private String partnerAgent;// 陪练坐席号
	private String curExercise;// 当前做题进度
	private String myExercise;// 我的试卷
	private String remark;// 备注
	public static String PASSWORD="111111";//设置重置密码常量
	private String maxagentid;//当前最大坐席号
	private String isExam;// 是否正在考试{0:未考试，1:正在考试}
	private String photoUrl;// 头像链接
	private String isLogin;// 是否已登录
	
	private String roles="10";// 学生角色
	private String state="3";// 学生3
	private String curRoles="10";
	
	private String sessionId;//session标识
	private Integer taskId;//任务id
	private Integer callState = 0; //0 未通话 1拨打2:接听
	private String profession;//行业
	private List<String> to;//去电信息
	private Integer answerState;//应答状态
	private String callTime;//打电话时间
	private String taskType;// 任务类型
	private String cnumbers;// 班级号（逗号隔开，前台检索用）
	
	//Constructors
	/** default constructor */
	public Stud() {
	
	}	
	//getter and setter
	/** 获取主键 */
	public Integer getId() {
		return this.id;
	}
	/** 设置主键 */
	public void setId(Integer id) {
		this.id = id;
	}
	/** 获取学号 */
	public String getNumber() {
		return this.number;
	}
	/** 设置学号 */
	public void setNumber(String number) {
		this.number = number;
	}
	/** 获取姓名 */
	public String getName() {
		return this.name;
	}
	/** 设置姓名 */
	public void setName(String name) {
		this.name = name;
	}
	/** 获取班级号 */
	public String getCnumber() {
		return this.cnumber;
	}
	/** 设置班级号 */
	public void setCnumber(String cnumber) {
		this.cnumber = cnumber;
	}
	/** 获取性别 */
	public String getSex() {
		return this.sex;
	}
	/** 设置性别 */
	public void setSex(String sex) {
		this.sex = sex;
	}
	/** 获取资料 */
	public String getInfo() {
		return this.info;
	}
	/** 设置资料 */
	public void setInfo(String info) {
		this.info = info;
	}
	/** 获取密码 */
	public String getPasswd() {
		return this.passwd;
	}
	/** 设置密码 */
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	/** 获取学生角色 */
	public String getRoles() {
		return "10";
	}
	/** 设置学生角色 */
	public void setRoles(String roles) {
		this.roles = roles;
	}
	/** 获取CC座席号 */
	public String getAgentId() {
		return this.agentId;
	}
	/** 设置CC座席号 */
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	/** 获取登录 */
	public String getLogin() {
		return this.login;
	}
	/** 设置登录 */
	public void setLogin(String login) {
		this.login = login;
	}
	/** 获取通话过期时刻 */
	public String getCallExpired() {
		return this.callExpired;
	}
	/** 设置通话过期时刻 */
	public void setCallExpired(String callExpired) {
		this.callExpired = callExpired;
	}
	/** 获取人员类型(备用) */
	public Integer getPartnerType() {
		return this.partnerType;
	}
	/** 设置人员类型(备用) */
	public void setPartnerType(Integer partnerType) {
		this.partnerType = partnerType;
	}
	/** 获取班级/教研室 */
	public String getPartnerClass() {
		return this.partnerClass;
	}
	/** 设置班级/教研室 */
	public void setPartnerClass(String partnerClass) {
		this.partnerClass = partnerClass;
	}
	/** 获取学号/教师 */
	public String getPartnerId() {
		return this.partnerId;
	}
	/** 设置学号/教师 */
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	/** 获取陪练坐席号 */
	public String getPartnerAgent() {
		return this.partnerAgent;
	}
	/** 设置陪练坐席号 */
	public void setPartnerAgent(String partnerAgent) {
		this.partnerAgent = partnerAgent;
	}
	/** 获取当前做题进度 */
	public String getCurExercise() {
		return this.curExercise;
	}
	/** 设置当前做题进度 */
	public void setCurExercise(String curExercise) {
		this.curExercise = curExercise;
	}
	/** 获取我的试卷 */
	public String getMyExercise() {
		return this.myExercise;
	}
	/** 设置我的试卷 */
	public void setMyExercise(String myExercise) {
		this.myExercise = myExercise;
	}
	/** 获取备注 */
	public String getRemark() {
		return this.remark;
	}
	/** 设置备注 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/** 获取重置密码常量*/
	public static String getPASSWORD() {
		return PASSWORD;
	}
	/** 设置重置密码常量*/
	public static void setPASSWORD(String pASSWORD) {
		PASSWORD = pASSWORD;
	}
	/**获取当前最大坐席号*/
	public String getMaxagentid() {
		return maxagentid;
	}
	/**设置当前最大坐席号*/
	public void setMaxagentid(String maxagentid) {
		this.maxagentid = maxagentid;
	}
	/**
	 * 获取是否练习
	 * @return
	 */
	public String getIsExam() {
		return isExam;
	}
	/**
	 * 设置是否练习
	 * @param isExam
	 */
	public void setIsExam(String isExam) {
		this.isExam = isExam;
	}
	
	public String getPhotoUrl() {
		return photoUrl;
	}
	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}
	
	public String getIsLogin() {
		return isLogin;
	}
	public void setIsLogin(String isLogin) {
		this.isLogin = isLogin;
	}
	
	public String getCurRoles() {
		return curRoles;
	}
	public void setCurRoles(String curRoles) {
		this.curRoles = curRoles;
	}
	
	public String getCnumbers() {
		return cnumbers;
	}
	public void setCnumbers(String cnumbers) {
		this.cnumbers = cnumbers;
	}
	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("number", number);
		obj.put("name", name);
		obj.put("cnumber", cnumber);
		obj.put("sex", sex);
		obj.put("info", info);
		obj.put("passwd", passwd);
		obj.put("roles", roles);
		obj.put("agentId", agentId);
		obj.put("login", login);
		obj.put("callExpired", callExpired);
		obj.put("partnerType", partnerType);
		obj.put("partnerClass", partnerClass);
		obj.put("partnerId", partnerId);
		obj.put("partnerAgent", partnerAgent);
		obj.put("curExercise", curExercise);
		obj.put("myExercise", myExercise);
		obj.put("remark", remark);
		obj.put("isExam", isExam);
		obj.put("sessionId", sessionId);
		obj.put("taskId", taskId);
		obj.put("callState", callState);
		obj.put("profession", profession);
		obj.put("photoUrl", photoUrl);
		obj.put("isLogin", isLogin);
		return obj;
	}
	
	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("number", number);
		obj.put("name", name);
		obj.put("cnumber", cnumber);
		if(sex!=null&&!sex.equals("")){//字典id值转label
			obj.put("sex", DictBuffer.getLabel("ZD_XB", sex.toString()));
		}
		obj.put("info", info);
		obj.put("passwd", passwd);
		if(roles!=null&&!roles.equals("")){//字典id值转label
			obj.put("roles", DictBuffer.getLabel("ZD_PARTNER", roles.toString()));
		}
		obj.put("agentId", agentId);
		obj.put("login", login);
		obj.put("callExpired", callExpired);
		obj.put("partnerType", partnerType);
		if(cnumber!=null&&!cnumber.equals("")){//字典id值转label
			obj.put("partnerClass", DictBuffer.getLabel("CC_CLASSES", cnumber.toString()));
		}
		obj.put("partnerId", partnerId);
		obj.put("partnerAgent", partnerAgent);
		obj.put("curExercise", curExercise);
		obj.put("myExercise", myExercise);
		obj.put("remark", remark);
		if(isExam!=null&&!isExam.equals("")){//字典id值转label
			obj.put("isExam", DictBuffer.getLabel("STU_ISEXAM", isExam.toString()));
		}
		obj.put("sessionId", sessionId);
		obj.put("taskId", taskId);
		obj.put("callState", callState);
		obj.put("profession", profession);
		obj.put("photoUrl", photoUrl);
		obj.put("isLogin", isLogin);
		return obj;
	}
	//-----------------------
	List<Function> menu;// 菜单
	String loginIp;// 登录ip
	/**
	 * 设置State
	 */
	@Override
	public void setState(String state) {
		 this.state = state;
	}
	/**
	 * 获取State
	 */
	public String getState() {
		return state;
	}
	/**
	 * 设置Menu
	 */
	@Override
	public void setMenu(List<Function> flist) {
		this.menu=flist;
	}
	/**
	 * 获取Menu
	 */
	@Override
	@JsonIgnore
	public List<Function> getMenu() {
		return menu;
	}
	/**
	 * 设置LoginIp
	 */
	@Override
	public void setLoginIp(String ip) {
		loginIp=ip;
	}
	/**
	 * 获取LoginIp
	 */
	@Override
	public String getLoginIp() {
		return loginIp;
	}
	/**
	 * 设置Organ
	 */
	@Override
	public void setOrgan(Organ organ) {
	}
	/**
	 * 获取Organ
	 */
	@Override
	public Organ getOrgan() {
		return null;
	}
	/**
	 * 获取Organid
	 */
	@Override
	public Integer getOrganid() {
		return null;
	}
	/**
	 * 获取SessionId
	 */
	public String getSessionId() {
		return sessionId;
	}
	/**
	 * 设置SessionId
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	/**
	 * 获取TaskId
	 */
	public Integer getTaskId() {
		return taskId;
	}
	/**
	 * 设置TaskId
	 */
	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}
	/**
	 * 获取CallState
	 */
	public Integer getCallState() {
		return callState;
	}
	/**
	 * 设置State
	 */
	public void setCallState(Integer callState) {
		this.callState = callState;
	}
	/**
	 * 获取Profession
	 */
	public String getProfession() {
		return profession;
	}
	/**
	 * 设置Profession
	 */
	public void setProfession(String profession) {
		this.profession = profession;
	}
	/**
	 * 获取To
	 */
	public List<String> getTo() {
		return to;
	}
	/**
	 * 设置To
	 */
	public void setTo(List<String> to) {
		this.to = to;
	}
	/**
	 * 获取State
	 */
	public Integer getAnswerState() {
		return answerState;
	}
	/**
	 * 设置State
	 */
	public void setAnswerState(Integer answerState) {
		this.answerState = answerState;
	}
	/**
	 * 获取CallTime
	 */
	public String getCallTime() {
		return callTime;
	}
	/**
	 * 设置CallTime
	 */
	public void setCallTime(String callTime) {
		this.callTime = callTime;
	}
	
	public String getTaskType() {
		return taskType;
	}
	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}
	
	/**
	 * 数据绑定
	 */
	@Override
	public void valueBound(HttpSessionBindingEvent event) {
		String id = agentId;// 定义常量
		Person person = Online.getOnlinePerson().get(id);// 添加数据
		
		if(person != null) {// 判断
			person.setSessionId(id);// 设置属性
		}else{
			this.setSessionId(id);// 设置属性
			Online.addPerson(this);// 添加属性
		}
		//LoginStudUtil.studLogin(this);
	}

	/**
	 * 数据帮顶
	 */
	@Override
	public void valueUnbound(HttpSessionBindingEvent event) {
		String id = agentId;// 定义常量
		Person person = Online.getOnlinePerson().get(id);// 获取数据
		if(person != null && person.getSessionId().equals(id)){// 判断
			Online.delPerson(id);// 删除是数据
		}
		//LoginStudUtil.studLogout(this);
	}
}
