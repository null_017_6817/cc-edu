package com.qtrmoon.manager.pojo;
import java.util.Date;
import org.json.simple.JSONObject;
import com.qtrmoon.common.PageForm;
import com.qtrmoon.toolkit.DateTransfer;
import com.qtrmoon.dictionary.DictBuffer;

/** 实体类 */
public class Record extends PageForm{
	//Fields
	
	private Integer id;// id
	private String rid;// rid
	private Integer taskId;// 任务ID
	private String fromNum;// 拨打人
	private String toNum;// 接听人
	private String voice;// 录音名称
	private String voicePath;// 录音名称
	private String status;// 状态
	private String type;// 类型
	private String typeName;// 类型
	private Date createTime;// 创建时间
	private String createTimeBeg,createTimeEnd;//时间条件的跨度查询属性
	private String fromAgent;//来电坐席号
	private String toAgent;//去电坐席号
	private String taskName;//任务名称 用于前台展示
	private String cTime;//后加cTime字段，用于日期统计
	
	
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public String getcTime() {
		return cTime;
	}
	public void setcTime(String cTime) {
		this.cTime = cTime;
	}
	public String getVoicePath() {
		return voicePath;
	}
	public void setVoicePath(String voicePath) {
		this.voicePath = voicePath;
	}
	//Constructors
	/** default constructor */
	public Record() {
	
	}	
	/**
	 * 获取任务名称
	 * @return
	 */
	public String getTaskName() {
		return taskName;
	}
	/**
	 * 设置任务名称
	 * @param taskName
	 */
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	//getter and setter
	/** 获取id */
	public Integer getId() {
		return this.id;
	}
	/** 设置id */
	public void setId(Integer id) {
		this.id = id;
	}
	/** 获取任务ID */
	public Integer getTaskId() {
		return this.taskId;
	}
	/** 设置任务ID */
	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}
	/** 获取拨打人 */
	public String getFromNum() {
		return this.fromNum;
	}
	/** 设置拨打人 */
	public void setFromNum(String fromNum) {
		this.fromNum = fromNum;
	}
	/** 获取接听人 */
	public String getToNum() {
		return this.toNum;
	}
	/** 设置接听人 */
	public void setToNum(String toNum) {
		this.toNum = toNum;
	}
	/** 获取录音名称 */
	public String getVoice() {
		return this.voice;
	}
	/** 设置录音名称 */
	public void setVoice(String voice) {
		this.voice = voice;
	}
	/** 获取状态 */
	public String getStatus() {
		return this.status;
	}
	/** 设置状态 */
	public void setStatus(String status) {
		this.status = status;
	}
	/** 获取类型 */
	public String getType() {
		return this.type;
	}
	/** 设置类型 */
	public void setType(String type) {
		this.type = type;
	}
	/** 获取创建时间 */
	public Date getCreateTime() {
		return this.createTime;
	}
	/** 设置创建时间 */
	
	/** 设定[创建时间]时间转载字段值，页面提交时由springmvc自动装载 */
	public void setCreateTimestr(String tm) {
		this.createTime=_getTime(tm);
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/** 获取[创建时间]时间起始条件 */
	public String getCreateTimeBeg() {
		return createTimeBeg;
	}
	/** 获取[创建时间]时间起始条件(Dao/Mapper查询时调用) */
	public Date getCreateTimeBegDate() {
		return _getBegDate(createTimeBeg);
	}
	/** 设置[创建时间]时间起始条件(表单提交时自动装载) */
	public void setCreateTimeBeg(String createTimeBeg) {
		this.createTimeBeg = createTimeBeg;
	}
	
	/** 获取[创建时间]时间结束条件 */
	public String getCreateTimeEnd() {
		return createTimeEnd;
	}
	/** 获取[创建时间]时间结束条件(Dao/Mapper查询时调用) */
	public Date getCreatetimeEndDate() {
		return _getEndDate(createTimeEnd);
	}
	/** 设置[创建时间]时间结束条件(表单提交时自动装载) */
	public void setCreateTimeEnd(String createTimeEnd) {
		this.createTimeEnd = createTimeEnd;
	}
	/**
	 * 获取录音id
	 * @return
	 */
	public String getRid() {
		return rid;
	}
	/**
	 * 设置录音id
	 * @param rid
	 */
	public void setRid(String rid) {
		this.rid = rid;
	}
	/**
	 * 获取去电坐席号
	 * @return
	 */
	public String getToAgent() {
		return toAgent;
	}
	/**
	 * 设置去电坐席号
	 * @param toAgent
	 */
	public void setToAgent(String toAgent) {
		this.toAgent = toAgent;
	}
	/**
	 * 获取来电坐席号
	 * @return
	 */
	public String getFromAgent() {
		return fromAgent;
	}
	/**
	 * 设置来电坐席号
	 * @param fromAgent
	 */
	public void setFromAgent(String fromAgent) {
		this.fromAgent = fromAgent;
	}
	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("rid", rid);
		obj.put("taskId", taskId);
		obj.put("taskName", taskName);
		obj.put("fromNum", fromNum);
		obj.put("toNum", toNum);
		obj.put("voice", voice);
		obj.put("status", status);
		obj.put("type", type);
		if(type!=null){
			obj.put("typeName", DictBuffer.getLabel("ZD_RECORDTYPE", type.toString()));
		}
		obj.put("fromAgent", fromAgent);
		obj.put("toAgent", toAgent);
		//时间字段处理。createtime查询列表页使用，createtimestr修改页使用
		obj.put("createTime", DateTransfer.toString(createTime,"yyyy-MM-dd HH:mm:ss"));
		obj.put("createTimestr", DateTransfer.toString(createTime,"yyyy-MM-dd HH:mm:ss"));
		obj.put("cTime", cTime);
		return obj;
	}
	
	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("rid", rid);
		obj.put("taskId", taskId);
		/*if(taskId!=null&&!taskId.equals("")){//字典id值转label
			obj.put("taskId", DictBuffer.getLabel("ZD_EXER_TOPIC", taskId.toString()));
		}*/
		obj.put("taskName", taskName);
		obj.put("fromNum", fromNum);
		obj.put("toNum", toNum);
		obj.put("voice", voice);
		obj.put("status", status);
		obj.put("type", type);
		if(type!=null){
			obj.put("typeName", DictBuffer.getLabel("ZD_RECORDTYPE", type.toString()));
		}
		obj.put("fromAgent", fromAgent);
		obj.put("toAgent", toAgent);
		//时间字段处理。createtime查询列表页使用，createtimestr修改页使用
		obj.put("createTime", DateTransfer.toString(createTime,"yyyy-MM-dd HH:mm:ss"));
		obj.put("createTimestr", DateTransfer.toString(createTime,"yyyy-MM-dd HH:mm:ss"));
		obj.put("cTimestr",cTime);
		return obj;
	}
}
