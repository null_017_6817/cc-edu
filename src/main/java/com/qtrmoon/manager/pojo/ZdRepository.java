package com.qtrmoon.manager.pojo;

import com.qtrmoon.common.PageForm;
/**
 * 知识库字典
 */
public class ZdRepository extends PageForm{
	
	private String label;//标签
	private Integer id;//主键
	private Integer pid;//父id
	
	/**
	 * 构造函数
	 */
	public ZdRepository() {
	}
	/**
	 * 获取标签
	 * @return
	 */
	public String getLabel() {
		return label;
	}
	/**
	 * 设置标签
	 * @param label
	 */
	public void setLabel(String label) {
		this.label = label;
	}
	/**
	 * 获取主键
	 * @return
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置主键
	 * @param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取父id
	 * @return
	 */
	public Integer getPid() {
		return pid;
	}
	/**
	 * 设置父id
	 * @param pid
	 */
	public void setPid(Integer pid) {
		this.pid = pid;
	}
	
}
