package com.qtrmoon.manager.pojo;
import org.json.simple.JSONObject;

import com.qtrmoon.common.PageForm;
import com.qtrmoon.dictionary.DictBuffer;
/** Excel校验Object类 */
public class ExcelError extends PageForm{
    //Fields
    private String sheetName;  // 页签名
    private String serial; // 序号
    private String row; // 行号
    private String column; // 列
    private String cellData; // 内容
    private String msg; // 提示信息
		
	//Constructors
	/** default constructor */
	public ExcelError() {
	
    }	

    public String getSheetName() {
        return this.sheetName;
    }

    public void setSheetName(String sheetName) {
        this.sheetName = sheetName;
    }

    public String getSerial() {
        return this.serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getColumn() {
        return this.column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public String getCellData() {
        return this.cellData;
    }

    public void setCellData(String cellData) {
        this.cellData = cellData;
    }

    public String getRow() {
        return this.row;
    }

    public void setRow(String row) {
        this.row = row;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj=new JSONObject();
		return obj;
	}
	
	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj=new JSONObject();
		return obj;
	}
}
