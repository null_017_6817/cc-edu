package com.qtrmoon.manager.pojo;

import java.sql.Timestamp;

import org.json.simple.JSONObject;

import com.qtrmoon.common.PageForm;
import com.qtrmoon.dictionary.DictBuffer;

/** 目录树Entity类 */
public class DirTree extends PageForm {
	// Fields

	private Integer id; // 主键
	private Integer pid; // 父目录id
	private int type; // 0-xml 1-dir 9-other
	private int isLeaf; // 是否叶子借点 0-否 1-是
	private int isBlank; // 是否空目录 0-否 1-是
	private String dirName; // 目录名
	private String rawPath; // 路径
	private String md5Sign; // 创建时间戳，确保唯一 MD5(createTime + createrIp);
	private Timestamp createTime; // 创建时间戳
	private String createrIp; // 创建者IP
	private int parseFlag; // 0-失败 1-成功
	private String parseDesc; // xml解析说明
	private String bankId;	// 题库
	private String topicId;  // 题目

	// Constructors
	/** default constructor */
	public DirTree() {

	}

	// getter and setter
	/** 获取主键 */
	public Integer getId() {
		return this.id;
	}

	/** 设置分校编号 */
	public void setId(Integer id) {
		this.id = id;
	}

	/** 获取Type */
	public int getType() {
		return type;
	}

	/** 设置Type */
	public void setType(int type) {
		this.type = type;
	}
	
	public Integer getPid() {
		return pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

	public int getIsLeaf() {
		return isLeaf;
	}

	public void setIsLeaf(int isLeaf) {
		this.isLeaf = isLeaf;
	}

	public int getIsBlank() {
		return isBlank;
	}

	public void setIsBlank(int isBlank) {
		this.isBlank = isBlank;
	}

	public String getDirName() {
		return dirName;
	}

	public void setDirName(String dirName) {
		this.dirName = dirName;
	}

	public String getMd5Sign() {
		return md5Sign;
	}

	public void setMd5Sign(String md5Sign) {
		this.md5Sign = md5Sign;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public String getCreaterIp() {
		return createrIp;
	}

	public void setCreaterIp(String createrIp) {
		this.createrIp = createrIp;
	}

	public String getRawPath() {
		return rawPath;
	}

	public void setRawPath(String rawPath) {
		this.rawPath = rawPath;
	}
	
	public int getParseFlag() {
		return parseFlag;
	}

	public void setParseFlag(int parseFlag) {
		this.parseFlag = parseFlag;
	}

	public String getParseDesc() {
		return parseDesc;
	}

	public void setParseDesc(String parseDesc) {
		this.parseDesc = parseDesc;
	}

	public String getBankId() {
		return bankId;
	}

	public void setBankId(String bankId) {
		this.bankId = bankId;
	}

	public String getTopicId() {
		return topicId;
	}

	public void setTopicId(String topicId) {
		this.topicId = topicId;
	}

	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj = new JSONObject();
		obj.put("id", id);
		obj.put("pid", pid);
		obj.put("type", type);
		obj.put("isLeaf", isLeaf);
		obj.put("isBlank", isBlank);
		obj.put("md5Sign", md5Sign);
		obj.put("createTime", createTime);
		obj.put("createrIp", createrIp);
		return obj;
	}

	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj = new JSONObject();
		obj.put("id", id);
		obj.put("pid", pid);
		obj.put("type", type);
		obj.put("isLeaf", isLeaf);
		obj.put("isBlank", isBlank);
		obj.put("md5Sign", md5Sign);
		obj.put("createTime", createTime);
		obj.put("createrIp", createrIp);
		return obj;
	}
}
