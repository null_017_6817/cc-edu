package com.qtrmoon.manager.pojo;
import org.json.simple.JSONObject;

import com.qtrmoon.common.PageForm;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.examination.util.TopicTypeTransUtil;
	/** 题目Object类 */
	public class BankTopic extends PageForm{
	//Fields
	
	private String vccId;// 分校编号
	private String bankId;// 题库编号
	private String topicId;// 考题编号
	private String title;// 标题
	private String type;// 类型
	private String level;// 级别
	private String profession;// 行业
	private String difficulty;// 难度
	private String author;// 作者
	private String creattime;// 时间
	private String remark;// 备注
	private Integer mergeVoice;// 合并录音
	private Integer totalScore;// 总分
	private Integer passScore;// 及格分
	private String beginStepId;// 第一个有效步骤
	private String createTime;// 入库时间
	private Integer status;// 状态
	private String docATitle;// 文档A标题
	private String docATmplt;// 文档A答案
	private String docBTitle;// 文档B标题
	private String docBTmplt;// 文档B答案
	private String docCTitle;// 文档C标题
	private String docCTmplt;// 文档C答案
	private String docDTitle;// 文档D标题
	private String docDTmplt;// 文档D答案
	private String docETitle;// 文档E标题
	private String docETmplt;// 文档E答案
	private String keyWords;//关键字
	private String talkSkill;//话术
	private String customer;//客户资料
	private String capability;//能力
	private String bookmodul;//课程模块
	private Integer bookId;// 课本
	private Integer exerId;//练习id
	private String stepType;// 步骤类型
	private Integer templateId; // 评分模板ID
	
	/**
	 * 获取练习id
	 * @return
	 */
	public Integer getExerId() {
		return exerId;
	}
	/**
	 * 设置练习id
	 * @param exerId
	 */
	public void setExerId(Integer exerId) {
		this.exerId = exerId;
	}
	/**
	 * 获取关键字
	 * @return
	 */
	public String getKeyWords() {
		return keyWords;
	}
	/**
	 * 设置关键字
	 * @param keyWords
	 */
	public void setKeyWords(String keyWords) {
		this.keyWords = keyWords;
	}
	/**
	 * 获取话术
	 * @return
	 */
	public String getTalkSkill() {
		return talkSkill;
	}
	/**
	 * 设置话术
	 * @param talkSkill
	 */
	public void setTalkSkill(String talkSkill) {
		this.talkSkill = talkSkill;
	}
	/**
	 * 获取客户资料
	 * @return
	 */
	public String getCustomer() {
		return customer;
	}
	/**
	 * 设置客户资料
	 * @param customer
	 */
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	/**
	 * 获取能力
	 * @return
	 */
	public String getCapability() {
		return capability;
	}
	/**
	 * 设置能力
	 * @param capability
	 */
	public void setCapability(String capability) {
		this.capability = capability;
	}
	/**
	 * 获取课程模块
	 * @return
	 */
	public String getBookmodul() {
		return bookmodul;
	}
	/**
	 * 设置课程模块
	 * @param bookmodul
	 */
	public void setBookmodul(String bookmodul) {
		this.bookmodul = bookmodul;
	}
	//Constructors
	/** default constructor */
	public BankTopic() {
	
	}	
	//getter and setter
	/** 获取分校编号 */
	public String getVccId() {
		return this.vccId;
	}
	/** 设置分校编号 */
	public void setVccId(String vccId) {
		this.vccId = vccId;
	}
	/** 获取题库编号 */
	public String getBankId() {
		return this.bankId;
	}
	/** 设置题库编号 */
	public void setBankId(String bankId) {
		this.bankId = bankId;
	}
	/** 获取考题编号 */
	public String getTopicId() {
		return this.topicId;
	}
	/** 设置考题编号 */
	public void setTopicId(String topicId) {
		this.topicId = topicId;
	}
	/** 获取标题 */
	public String getTitle() {
		return this.title;
	}
	/** 设置标题 */
	public void setTitle(String title) {
		this.title = title;
	}
	/** 获取类型 */
	public String getType() {
		return this.type;
	}
	/** 设置类型 */
	public void setType(String type) {
		this.type = type;
	}
	/** 获取级别 */
	public String getLevel() {
		return this.level;
	}
	/** 设置级别 */
	public void setLevel(String level) {
		this.level = level;
	}
	/** 获取行业 */
	public String getProfession() {
		return this.profession;
	}
	/** 设置行业 */
	public void setProfession(String profession) {
		this.profession = profession;
	}
	/** 获取难度 */
	public String getDifficulty() {
		return this.difficulty;
	}
	/** 设置难度 */
	public void setDifficulty(String difficulty) {
		this.difficulty = difficulty;
	}
	/** 获取作者 */
	public String getAuthor() {
		return this.author;
	}
	/** 设置作者 */
	public void setAuthor(String author) {
		this.author = author;
	}
	/** 获取时间 */
	public String getCreattime() {
		return this.creattime;
	}
	/** 设置时间 */
	public void setCreattime(String creattime) {
		this.creattime = creattime;
	}
	/** 获取备注 */
	public String getRemark() {
		return this.remark;
	}
	/** 设置备注 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/** 获取合并录音 */
	public Integer getMergeVoice() {
		return this.mergeVoice;
	}
	/** 设置合并录音 */
	public void setMergeVoice(Integer mergeVoice) {
		this.mergeVoice = mergeVoice;
	}
	/** 获取总分 */
	public Integer getTotalScore() {
		return this.totalScore;
	}
	/** 设置总分 */
	public void setTotalScore(Integer totalScore) {
		this.totalScore = totalScore;
	}
	/** 获取及格分 */
	public Integer getPassScore() {
		return this.passScore;
	}
	/** 设置及格分 */
	public void setPassScore(Integer passScore) {
		this.passScore = passScore;
	}
	/** 获取第一个有效步骤 */
	public String getBeginStepId() {
		return this.beginStepId;
	}
	/** 设置第一个有效步骤 */
	public void setBeginStepId(String beginStepId) {
		this.beginStepId = beginStepId;
	}
	/** 获取入库时间 */
	public String getCreateTime() {
		return this.createTime;
	}
	/** 设置入库时间 */
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	/** 获取状态 */
	public Integer getStatus() {
		return this.status;
	}
	/** 设置状态 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/** 获取文档A标题 */
	public String getDocATitle() {
		return this.docATitle;
	}
	/** 设置文档A标题 */
	public void setDocATitle(String docATitle) {
		this.docATitle = docATitle;
	}

	/** 获取文档B标题 */
	public String getDocBTitle() {
		return this.docBTitle;
	}
	/** 设置文档B标题 */
	public void setDocBTitle(String docBTitle) {
		this.docBTitle = docBTitle;
	}
	/** 获取文档C标题 */
	public String getDocCTitle() {
		return this.docCTitle;
	}
	/** 设置文档C标题 */
	public void setDocCTitle(String docCTitle) {
		this.docCTitle = docCTitle;
	}
	/** 获取文档D标题 */
	public String getDocDTitle() {
		return this.docDTitle;
	}
	/** 设置文档D标题 */
	public void setDocDTitle(String docDTitle) {
		this.docDTitle = docDTitle;
	}
	/** 获取文档E标题 */
	public String getDocETitle() {
		return this.docETitle;
	}
	/** 设置DocETitle */
	public void setDocETitle(String docETitle) {
		this.docETitle = docETitle;
	}
	/** 获取DocETitle */
	public String getDocATmplt() {
		return docATmplt;
	}
	/** 设置DocATmplt */
	public void setDocATmplt(String docATmplt) {
		this.docATmplt = docATmplt;
	}
	/** 获取DocATmplt */
	public String getDocBTmplt() {
		return docBTmplt;
	}
	/** 设置DocBTmplt */
	public void setDocBTmplt(String docBTmplt) {
		this.docBTmplt = docBTmplt;
	}
	/** 获取DocBTmplt */
	public String getDocCTmplt() {
		return docCTmplt;
	}
	/** 设置DocCTmplt */
	public void setDocCTmplt(String docCTmplt) {
		this.docCTmplt = docCTmplt;
	}
	/** 获取DocCTmplt */
	public String getDocDTmplt() {
		return docDTmplt;
	}
	/** 设置DocDTmplt */
	public void setDocDTmplt(String docDTmplt) {
		this.docDTmplt = docDTmplt;
	}
	/** 获取DocDTmplt */
	public String getDocETmplt() {
		return docETmplt;
	}
	/** 设置DocETmplt */
	public void setDocETmplt(String docETmplt) {
		this.docETmplt = docETmplt;
	}
	/** 获取bookid */
	public Integer getBookId() {
		return bookId;
	}
	/** 设置BookId */
	public void setBookId(Integer bookId) {
		this.bookId = bookId;
	}
	
	public String getStepType() {
		return stepType;
	}
	public void setStepType(String stepType) {
		this.stepType = stepType;
	}

	public Integer getTemplateId() {
		return this.templateId;
	}

	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}

	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj=new JSONObject();
		obj.put("vccId", vccId);
		obj.put("bankId", bankId);
		obj.put("topicId", topicId);
		obj.put("title", title);
		obj.put("type", type);
		obj.put("level", level);
		obj.put("profession", profession);
		obj.put("keyWords", keyWords);
		obj.put("difficulty", difficulty);
		obj.put("author", author);
		obj.put("creattime", creattime);
		obj.put("remark", remark);
		obj.put("mergeVoice", mergeVoice);
		obj.put("customer", customer);
		obj.put("talkSkill", talkSkill);
		obj.put("totalScore", totalScore);
		obj.put("passScore", passScore);
		obj.put("beginStepId", beginStepId);
		obj.put("createTime", createTime);
		obj.put("status", status);
		obj.put("capability", capability);
		obj.put("bookmodul", bookmodul);
		obj.put("docATitle", docATitle);
		obj.put("docATmplt", docATmplt);
		obj.put("docBTitle", docBTitle);
		obj.put("docBTmplt", docBTmplt);
		obj.put("docCTitle", docCTitle);
		obj.put("docCTmplt", docCTmplt);
		obj.put("docDTitle", docDTitle);
		obj.put("docDTmplt", docDTmplt);
		obj.put("docETitle", docETitle);
		obj.put("docETmplt", docETmplt);
		obj.put("bookId", bookId);
		obj.put("templateId", templateId);
		return obj;
	}
	
	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj=new JSONObject();
		obj.put("vccId", vccId);
		obj.put("bankid", bankId);
		if(bankId!=null&&!bankId.equals("")){//label
			obj.put("bankId", DictBuffer.getLabel("ZD_BANK", bankId.toString()));
		}
		obj.put("topicId", topicId);
		obj.put("title", title);
		obj.put("type", type);
		if(type != null){
			obj.put("typeName", TopicTypeTransUtil.topicCodeTransLabel(type));
		}
		obj.put("level", level);
		obj.put("profession", profession);
		if(profession != null){
			obj.put("professionLabel", DictBuffer.getLabel("ZD_REPOSITORY", profession.toString()));
		}
		if(obj.get("professionLabel")== null || "".equals(obj.get("professionLabel")) ){
			obj.put("professionLabel",profession);
		}
		obj.put("keyWords", keyWords);
		obj.put("difficulty", difficulty);
		if(difficulty != null){
			obj.put("difficultyLabel", DictBuffer.getLabel("ZD_DIFFICULT", difficulty.toString()));
		}
		obj.put("author", author);
		obj.put("creattime", creattime);
		obj.put("remark", remark);
		obj.put("mergeVoice", mergeVoice);
		obj.put("customer", customer);
		obj.put("talkSkill", talkSkill);
		obj.put("totalScore", totalScore);
		obj.put("passScore", passScore);
		obj.put("beginStepId", beginStepId);
		obj.put("createTime", createTime);
		obj.put("status", status);
		obj.put("capability", capability);
		obj.put("bookmodul", bookmodul);
		obj.put("docATitle", docATitle);
		obj.put("docATmplt", docATmplt);
		obj.put("docBTitle", docBTitle);
		obj.put("docBTmplt", docBTmplt);
		obj.put("docCTitle", docCTitle);
		obj.put("docCTmplt", docCTmplt);
		obj.put("docDTitle", docDTitle);
		obj.put("docDTmplt", docDTmplt);
		obj.put("docETitle", docETitle);
		obj.put("docETmplt", docETmplt);
		obj.put("bookId", bookId);
		if( bookId !=  null){
			obj.put("bookName", DictBuffer.getLabel("CC_BOOK", bookId.toString()));
		}
		obj.put("templateId", templateId);
		return obj;
	}
}
