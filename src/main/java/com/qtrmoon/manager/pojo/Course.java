package com.qtrmoon.manager.pojo;

import java.util.Date;
import java.util.Set;
import java.util.HashSet;
import org.json.simple.JSONObject;
import com.qtrmoon.common.PageForm;
import com.qtrmoon.toolkit.DateTransfer;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.dictionary.bsd.DictBean;

/** 课件Object类 */
public class Course extends PageForm {
	private Integer id;// 主键
	private String shortCut;// 目录
	private String courTitle;// 标题
	private String courSynopsis;// 简要
	private String attachPath;// PDF附件
	private String uploadPeople;// 上传人
	private Date uploadTime;// 上传时间
	private String uploadTimeBeg, uploadTimeEnd;// 时间条件的跨度查询属性
	private Integer readCount;// 浏览数
	private String changePeople;// 修改人
	private Date changeTime;// 修改时间
	private String changeTimeBeg, changeTimeEnd;// 时间条件的跨度查询属性
	private String courText;//正文
	private String type;//类型
	private String bookId;//课本id

	// Constructors
	/** default constructor */
	public Course() {

	}
	/**
	 * 获取正文
	 * @return
	 */
	public String getCourText() {
		return courText;
	}
	/**
	 * 设置正文
	 * @param courText
	 */
	public void setCourText(String courText) {
		this.courText = courText;
	}

	// getter and setter
	/** 获取主键 */
	public Integer getId() {
		return this.id;
	}

	/** 设置主键 */
	public void setId(Integer id) {
		this.id = id;
	}

	/** 获取目录 */
	public String getShortCut() {
		return this.shortCut;
	}

	/** 设置目录 */
	public void setShortCut(String shortCut) {
		this.shortCut = shortCut;
	}

	/** 获取标题 */
	public String getCourTitle() {
		return this.courTitle;
	}

	/** 设置标题 */
	public void setCourTitle(String courTitle) {
		this.courTitle = courTitle;
	}

	/** 获取简要 */
	public String getCourSynopsis() {
		return this.courSynopsis;
	}

	/** 设置简要 */
	public void setCourSynopsis(String courSynopsis) {
		this.courSynopsis = courSynopsis;
	}

	/** 获取PDF附件 */
	public String getAttachPath() {
		return this.attachPath;
	}

	/** 设置PDF附件 */
	public void setAttachPath(String attachPath) {
		this.attachPath = attachPath;
	}

	/** 获取上传人 */
	public String getUploadPeople() {
		return this.uploadPeople;
	}

	/** 设置上传人 */
	public void setUploadPeople(String uploadPeople) {
		this.uploadPeople = uploadPeople;
	}

	/** 获取上传时间 */
	public Date getUploadTime() {
		return this.uploadTime;
	}

	/** 设置上传时间 */
	public void setUploadTime(Date uploadTime) {
		this.uploadTime = uploadTime;
	}

	/** 设定[上传时间]时间转载字段值，页面提交时由springmvc自动装载 */
	public void setUploadTimestr(String tm) {
		this.uploadTime = _getTime(tm);
	}

	/** 获取[上传时间]时间起始条件 */
	public String getUploadTimeBeg() {
		return uploadTimeBeg;
	}

	/** 获取[上传时间]时间起始条件(Dao/Mapper查询时调用) */
	public Date getUploadTimeBegDate() {
		return _getBegDate(uploadTimeBeg);
	}

	/** 设置[上传时间]时间起始条件(表单提交时自动装载) */
	public void setUploadTimeBeg(String uploadTimeBeg) {
		this.uploadTimeBeg = uploadTimeBeg;
	}

	/** 获取[上传时间]时间结束条件 */
	public String getUploadTimeEnd() {
		return uploadTimeEnd;
	}

	/** 获取[上传时间]时间结束条件(Dao/Mapper查询时调用) */
	public Date getUploadTimeEndDate() {
		return _getEndDate(uploadTimeEnd);
	}

	/** 设置[上传时间]时间结束条件(表单提交时自动装载) */
	public void setUploadTimeEnd(String uploadTimeEnd) {
		this.uploadTimeEnd = uploadTimeEnd;
	}

	/** 获取浏览数 */
	public Integer getReadCount() {
		return this.readCount;
	}

	/** 设置浏览数 */
	public void setReadCount(Integer readCount) {
		this.readCount = readCount;
	}

	/** 获取修改人 */
	public String getChangePeople() {
		return this.changePeople;
	}

	/** 设置修改人 */
	public void setChangePeople(String changePeople) {
		this.changePeople = changePeople;
	}

	/** 获取修改时间 */
	public Date getChangeTime() {
		return this.changeTime;
	}

	/** 设置修改时间 */
	public void setChangeTime(Date changeTime) {
		this.changeTime = changeTime;
	}

	/** 设定[修改时间]时间转载字段值，页面提交时由springmvc自动装载 */
	public void setChangeTimestr(String tm) {
		this.changeTime = _getTime(tm);
	}

	/** 获取[修改时间]时间起始条件 */
	public String getChangeTimeBeg() {
		return changeTimeBeg;
	}

	/** 获取[修改时间]时间起始条件(Dao/Mapper查询时调用) */
	public Date getChangeTimeBegDate() {
		return _getBegDate(changeTimeBeg);
	}

	/** 设置[修改时间]时间起始条件(表单提交时自动装载) */
	public void setChangeTimeBeg(String changeTimeBeg) {
		this.changeTimeBeg = changeTimeBeg;
	}

	/** 获取[修改时间]时间结束条件 */
	public String getChangeTimeEnd() {
		return changeTimeEnd;
	}

	/** 获取[修改时间]时间结束条件(Dao/Mapper查询时调用) */
	public Date getChangeTimeEndDate() {
		return _getEndDate(changeTimeEnd);
	}

	/** 设置[修改时间]时间结束条件(表单提交时自动装载) */
	public void setChangeTimeEnd(String changeTimeEnd) {
		this.changeTimeEnd = changeTimeEnd;
	}

	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj = new JSONObject();
		obj.put("id", id);
		obj.put("shortCut", shortCut);
		obj.put("bookId", bookId);
		obj.put("courTitle", courTitle);
		obj.put("courSynopsis", courSynopsis);
		obj.put("courText", courText);
		obj.put("attachPath", attachPath);
		obj.put("uploadPeople", uploadPeople);
		// 时间字段处理。uploadTime查询列表页使用，uploadTimestr修改页使用
		obj.put("uploadTime", DateTransfer.toString(uploadTime, "yyyy-MM-dd"));
		obj.put("uploadTimestr",
				DateTransfer.toString(uploadTime, "yyyy-MM-dd HH:mm:ss"));
		obj.put("readCount", readCount);
		obj.put("changePeople", changePeople);
		// 时间字段处理。changeTime查询列表页使用，changeTimestr修改页使用
		obj.put("changeTime", DateTransfer.toString(changeTime, "yyyy-MM-dd"));
		obj.put("changeTimestr",
				DateTransfer.toString(changeTime, "yyyy-MM-dd"));
		obj.put("type",type);
		return obj;
	}

	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj = new JSONObject();
		obj.put("id", id);
		obj.put("shortCut", shortCut);
		if(bookId!=null&&!bookId.equals("")){//字典id值转label
			obj.put("bookId", DictBuffer.getLabel("CC_BOOK", bookId.toString()));
		}
		obj.put("courTitle", courTitle);
		obj.put("courSynopsis", courSynopsis);
		obj.put("courText", courText);
		obj.put("attachPath", attachPath);
		obj.put("uploadPeople", uploadPeople);
		// 时间字段处理。uploadTime查询列表页使用，uploadTimestr修改页使用
		obj.put("uploadTime", DateTransfer.toString(uploadTime, "yyyy-MM-dd"));
		obj.put("uploadTimestr",
				DateTransfer.toString(uploadTime, "yyyy-MM-dd HH:mm:ss"));
		obj.put("readCount", readCount);
		obj.put("changePeople", changePeople);
		// 时间字段处理。changeTime查询列表页使用，changeTimestr修改页使用
		obj.put("changeTime", DateTransfer.toString(changeTime, "yyyy-MM-dd"));
		obj.put("changeTimestr",
				DateTransfer.toString(changeTime, "yyyy-MM-dd"));
		if(type != null && !"".equals(type)){
			obj.put("type", DictBuffer.getLabel("ZD_COURSETYPE", type.toString()));
		}
		return obj;
	}
	/**获取book的id*/
	public String getBookId() {
		return bookId;
	}
	/**设置book的id*/
	public void setBookId(String bookId) {
		this.bookId = bookId;
	}
	/**获取类型*/
	public String getType() {
		return type;
	}
	/**设置类型*/
	public void setType(String type) {
		this.type = type;
	}
}
