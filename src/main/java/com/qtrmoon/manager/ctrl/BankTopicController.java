package com.qtrmoon.manager.ctrl;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.qtrmoon.book.pojo.Book;
import com.qtrmoon.book.serdao.IBookService;
import com.qtrmoon.common.BaseController;
import com.qtrmoon.examination.constant.ExerConstant;
import com.qtrmoon.manager.constant.TopicTypeConstant;
import com.qtrmoon.manager.pojo.BankTopic;
import com.qtrmoon.manager.pojo.BankStep;
import com.qtrmoon.manager.pojo.BankTheory;
import com.qtrmoon.manager.serdao.IManagerService;
import com.qtrmoon.examination.pojo.StepJson;
import com.qtrmoon.toolkit.XmlUtil;

/**
 * 题目类控制层
 */
@Controller
@RequestMapping("/manager/bankTopic")
public class BankTopicController extends BaseController {
	@Autowired
	private IManagerService managerService; // 声明模块Service实例

	@Autowired
	private IBookService bookService; // 声明模块Service实例


	/**
	 * 查询方法，自带分页设置。
	 * @param bankTopic
	 * 承载查询条件的Bean
	 * @param request
	 * 可用来获取非Bean内的参数
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schBankTopic.action")
	public void schBankTopic(BankTopic bankTopic, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		noCache(response);// ajax设置无缓存
		bankTopic.setCondition(""); // 防sql注入
		bankTopic.assLike("vccId", "topicId", "title", "type", "level",// 拼接%
				"difficulty", "author", "creattime", "remark", "beginStepId",// 拼接%
				"createTime", "docATitle", "docBTitle", "docCTitle",// 拼接%
				"docDTitle", "docETitle");// 拼接%

		List<BankTopic> list = managerService.schBankTopic(bankTopic); // 调用Service查询数据
		/* 将数据Bean的集合转换为Json集合 */
		JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
		JSONObject obj; // 声明用于构造Bean的Json对象
		for (BankTopic u : list) { // 循环查询的数据集构造Json数据集
			obj = u.getJsonInDict(); // 将Bean转换为Json对象
			obj.put("_oper", u.getTopicId()); // 为"操作列"设定主键值
			datas.add(obj); // 添加Json对象到Json集合
		}
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		JSONObject res = new JSONObject(); // 声明页面返回Json
		res.put("total", bankTopic.getDatasize()); // 设置数据总记录数
		res.put("rows", datas); // 设置数据集
		response.getWriter().print(res.toJSONString()); // 输出到页面
	}

	/**
	 * 查询方法，自带分页设置。
	 * @param bankTopic
	 * 承载查询条件的Bean
	 * @param request
	 * 可用来获取非Bean内的参数
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schBankTopicNotExer.action")
	public void schBankTopicNotExer(BankTopic bankTopic,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		noCache(response);// ajax设置无缓存
		bankTopic.setCondition(""); // 防sql注入
		bankTopic.assLike("vccId", "topicId", "title", "type", "level",// 拼接%
				"difficulty", "author", "creattime", "remark", "beginStepId",// 拼接%
				"createTime", "docATitle", "docBTitle", "docCTitle",// 拼接%
				"docDTitle", "docETitle");// 拼接%
		String col = bankTopic.getOrderCol();
		if(StringUtils.isEmpty(col) || ",".equals(col)){
			bankTopic.setOrderCol("title");
		}
		List<BankTopic> list = managerService.schBankTopicNotExer(bankTopic); // 调用Service查询数据
		/* 将数据Bean的集合转换为Json集合 */
		JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
		JSONObject obj; // 声明用于构造Bean的Json对象
		for (BankTopic u : list) { // 循环查询的数据集构造Json数据集
			obj = u.getJsonInDict(); // 将Bean转换为Json对象
			obj.put("_oper", u.getTopicId()); // 为"操作列"设定主键值
			datas.add(obj); // 添加Json对象到Json集合
		}
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		JSONObject res = new JSONObject(); // 声明页面返回Json
		res.put("total", bankTopic.getDatasize()); // 设置数据总记录数
		res.put("rows", datas); // 设置数据集
		response.getWriter().print(res.toJSONString()); // 输出到页面
	}

	/**
	 * 查询方法，自带分页设置。
	 * @param bankTopic
	 * 承载查询条件的Bean
	 * @param request
	 * 可用来获取非Bean内的参数
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schBankTopicById.action")
	public void schBankTopicById(BankTopic bankTopic, String bankId,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		noCache(response);// ajax设置无缓存
		bankTopic.setCondition(""); // 防sql注入
		bankTopic.assLike("vccId", "topicId", "title", "type", "level",// 拼接%
				"difficulty", "author", "creattime", "remark", "beginStepId",// 拼接%
				"createTime", "docATitle", "docBTitle", "docCTitle",// 拼接%
				"docDTitle", "docETitle");// 拼接%
		bankTopic.setBankId(bankId);// 设置参数
		List<BankTopic> list = managerService.schBankTopic(bankTopic); // 调用Service查询数据
		/* 将数据Bean的集合转换为Json集合 */
		JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
		JSONObject obj; // 声明用于构造Bean的Json对象
		for (BankTopic u : list) { // 循环查询的数据集构造Json数据集
			obj = u.getJsonInDict(); // 将Bean转换为Json对象
			obj.put("_oper", u.getTopicId()); // 为"操作列"设定主键值
			datas.add(obj); // 添加Json对象到Json集合
		}
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		JSONObject res = new JSONObject(); // 声明页面返回Json
		res.put("total", bankTopic.getDatasize()); // 设置数据总记录数
		res.put("rows", datas); // 设置数据集
		response.getWriter().print(res.toJSONString()); // 输出到页面
	}

	/**
	 * 预添加or预修改or查看方法
	 * @param bankTopic
	 * 预添加时可传入一些初始值；预修改和查看时用来承载主键值
	 * @param toL
	 * 挂字典的字段是否需要转为字典Label值。true则进行转换
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/vieBankTopic.action")
	public void vieBankTopic(BankTopic bankTopic, Boolean toL,
			HttpServletResponse response) throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (toL != null && toL) { // 查看页面加载数据时调用，返回的对象已进行字典转换
			if (bankTopic.getTopicId() != null// 检测主键非空
					&& !"".equals(bankTopic.getTopicId())// 检测主键非空
					&& !"0".equals(bankTopic.getTopicId())) { // 检测主键非空
				bankTopic = managerService.schBankTopicById(bankTopic// 按主键查询数据
						.getTopicId()); // 按主键查询数据
				response.getWriter().print(bankTopic.getJsonInDict()); // 输出经过字典转换Json对象到查看页(vie_)
			}
		} else { // 修改页面加载数据时调用
			if (bankTopic.getTopicId() != null// 检测主键非空
					&& !"".equals(bankTopic.getTopicId())// 检测主键非空
					&& !"0".equals(bankTopic.getTopicId())) { // 检查主键非空
				bankTopic = managerService.schBankTopicById(bankTopic // 按主键查询数据
						.getTopicId()); // 按主键查询数据
				response.getWriter().print(bankTopic.getJson()); // 输出Json对象到修改页(upd_)
			} else { // 添加页面加载数据时调用，可设置添加页面中一些属性的预设值。
				response.getWriter().print(new BankTopic().getJson()); // 输出Json对象到添加页(upd_)
			}
		}
	}

	/**
	 * 添加或修改的提交方法
	 * @param bankTopic
	 * 数据表单Bean
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/updBankTopic.action")
	public void updBankTopic(BankTopic bankTopic, HttpServletResponse response)
			throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (StringUtils.isBlank(bankTopic.getTopicId())) { // 检测主键为空则
			managerService.addBankTopic(bankTopic); // 添加数据
		} else { // 主键非空则
			managerService.updBankTopic(bankTopic); // 修改数据
		}
	}

	/**
	 * 添加或修改的提交方法
	 * @param bankTopic
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@Deprecated
	@RequestMapping(value = "/saveBankTopic.action", method = RequestMethod.POST)
	public void saveBankTopic(@RequestParam("formData") String formData, HttpServletResponse response) throws IOException {
		noCache(response);
		BankTopic topic = new BankTopic();
		JSONObject formJSON = formatForm2Json(formData);
		// String formStr = formData;
		// System.out.println(formData);
		// System.out.println(formJSON);
		String bankId = (String) formJSON.get("bankId");
		String topicId = (String) formJSON.get("topicId");
		String topicName = (String) formJSON.get("topicName");
		String topicType = (String) formJSON.get("topicType");
		String difficulty = (String) formJSON.get("difficulty");
		String remark = (String) formJSON.get("remark");
		topic.setBankId(bankId);
		topic.setTopicId(topicId);
		topic.setTitle(topicName);
		topic.setType(topicType);
		topic.setDifficulty(difficulty);
		topic.setRemark(remark);
		if(TopicTypeConstant.THEORY.equals(topicType)) {
			String section = (String) formJSON.get("section");
			if(StringUtils.isNotBlank(section)) {
				try{
					int bookId = Integer.parseInt(section);
					topic.setBookId(bookId);
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
		}
		if(TopicTypeConstant.PRACTICAL.equals(topicType)) {
			String trainType = (String) formJSON.get("trainType");
			if(TopicTypeConstant.PRACTICAL_SECTION.equals(trainType)) {
				String section = (String) formJSON.get("section");
				if(StringUtils.isNotBlank(section)) {
					try{
						int bookId = Integer.parseInt(section);
						topic.setBookId(bookId);
					}catch(Exception e) {
						e.printStackTrace();
					}
				}
			}
			if(TopicTypeConstant.PRACTICAL_PROFESSION.equals(trainType)) {
				String profession = (String) formJSON.get("profession");
				topic.setProfession(profession);
			}
		}
		if(StringUtils.isBlank(topic.getTopicId())) {
			managerService.addBankTopic(topic);
		} else {
			managerService.updBankTopic(topic);
		}
		JSONObject result = new JSONObject();
		result.put("topicId", topic.getTopicId());
		result.put("status", "1");
		response.getWriter().write(result.toJSONString());
	}

	/**
	 * 添加或修改topic&step
	 * @param bankTopic
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/saveBankTopicAndStep.action", method = RequestMethod.POST)
	public void saveBankTopicAndStep(@RequestParam("topicFormData") String topicFormData, @RequestParam("stepFormData") String stepFormData,
		HttpServletResponse response) throws IOException {
		noCache(response);
		JSONObject result = new JSONObject();
		JSONObject topicFormJSON = formatForm2Json(topicFormData);
		JSONObject stepFormJSON = formatForm2Json(stepFormData);
		try{
			BankTopic topic = new BankTopic();
			if(StringUtils.isNotBlank(topicFormData)) {
				String bankId = (String) topicFormJSON.get("bankId");
				String topicId = (String) topicFormJSON.get("topicId");
				String topicName = (String) topicFormJSON.get("topicName");
				String topicType = (String) topicFormJSON.get("topicType");
				String difficulty = (String) topicFormJSON.get("difficulty");
				String remark = (String) topicFormJSON.get("remark");
				String keyWords = (String) topicFormJSON.get("keyWords");
				topic.setBankId(bankId);
				topic.setTopicId(topicId);
				topic.setTitle(topicName);
				topic.setType(topicType);
				topic.setDifficulty(difficulty);
				topic.setRemark(remark);
				topic.setKeyWords(keyWords);
				if(TopicTypeConstant.THEORY.equals(topicType)) {
					String section = (String) topicFormJSON.get("section");
					if(StringUtils.isNotBlank(section)) {
						try{
							int bookId = Integer.parseInt(section);
							topic.setBookId(bookId);
						}catch(Exception e) {
							e.printStackTrace();
						}
					}
				}
				if(TopicTypeConstant.PRACTICAL.equals(topicType)) {
					String trainType = (String) topicFormJSON.get("trainType");
					if(TopicTypeConstant.PRACTICAL_SECTION.equals(trainType)) {
						String section = (String) topicFormJSON.get("section");
						if(StringUtils.isNotBlank(section)) {
							try{
								int bookId = Integer.parseInt(section);
								topic.setBookId(bookId);
							}catch(Exception e) {
								e.printStackTrace();
							}
						}
					}
					if(TopicTypeConstant.PRACTICAL_PROFESSION.equals(trainType)) {
						String profession = (String) topicFormJSON.get("profession");
						topic.setProfession(profession);
					}
				}
				if(TopicTypeConstant.SKILL.equals(topicType)) {
					String profession = (String) topicFormJSON.get("profession");
					topic.setProfession(profession);
				}
				if(TopicTypeConstant.CAMPING.equals(topicType)) {
					String section = (String) topicFormJSON.get("section");
					String template = (String) topicFormJSON.get("template");
					try{
						int bookId = Integer.parseInt(section);
						topic.setBookId(bookId);
						Integer templateId = Integer.parseInt(template);
						topic.setTemplateId(templateId);
					}catch (Exception e) {
						e.printStackTrace();
					}
				}
				if(StringUtils.isBlank(topic.getTopicId())) {
					managerService.addBankTopic(topic);
				} else {
					managerService.updBankTopic(topic);
				}
				result.put("topicId", topic.getTopicId());
			}
			BankStep step = new BankStep();
			if(StringUtils.isNotBlank(stepFormData)) {
				String stepId = (String) stepFormJSON.get("stepId");
				String stepTopicId = (String) stepFormJSON.get("stepTopicId");
				String stepBankId = (String) stepFormJSON.get("stepBankId");
				String stepName = (String) stepFormJSON.get("stepName");
				String stepType = (String) stepFormJSON.get("stepType");
				// TODO 序号
				String topicType = topic.getType();
				if(TopicTypeConstant.THEORY.equals(topicType)) {
					step.setOrderNo(1);
				}
				try{
					step.setId(Integer.parseInt(stepId));
				}catch(Exception e) {
					step.setId(null);
				}
				step.setBankId(stepBankId);
				step.setTopicId(stepTopicId);
				step.setTitle(stepName);
				step.setType(stepType);
				// 单选
				if(TopicTypeConstant.SINGLE.equals(stepType)) {
					String score = (String) stepFormJSON.get("score");
					step.setScore(score);
					String singleXml = assembleSingleStepContent(stepFormJSON);
					step.setParam(singleXml);
					String standardAnswer = StringUtils.substringBetween(singleXml, "<standard_answer>", "</standard_answer>");
					step.setStandardAnswer(standardAnswer);
				}
				// 多选
				if(TopicTypeConstant.MULTI.equals(stepType)) {
					String score = (String) stepFormJSON.get("score");
					step.setScore(score);
					String multiXml = assembleMultiStepContent(stepFormJSON);
					step.setParam(multiXml);
					String standardAnswer = StringUtils.substringBetween(multiXml, "<standard_answer>", "</standard_answer>");
					step.setStandardAnswer(standardAnswer);
				}
				// 判断
				if(TopicTypeConstant.JUDGE.equals(stepType)) {
					String score = (String) stepFormJSON.get("score");
					step.setScore(score);
					String judgeXml = assembleJudgeStepContent(stepFormJSON);
					step.setParam(judgeXml);

					String standardAnswer = StringUtils.substringBetween(judgeXml, "<standard_answer>", "</standard_answer>");
					step.setStandardAnswer(standardAnswer);
				}
				// 填空
				if(TopicTypeConstant.CLOZED.equals(stepType)) {
					String score = computeClozedStepScore(stepFormJSON);
					step.setScore(score);
					String clozedXml = assembleClozedStepContent(stepFormJSON);
					step.setParam(clozedXml);
					String standardAnswer = StringUtils.substringBetween(clozedXml, "<standard_answer>", "</standard_answer>");
					step.setStandardAnswer(standardAnswer);
				}
				// 问答
				if(TopicTypeConstant.ANSWER.equals(stepType)) {
					String score = (String) stepFormJSON.get("score");
					step.setScore(score);
					String answerXml = assembleAnswerStepContent(stepFormJSON);
					step.setParam(answerXml);
					String standardAnswer = StringUtils.substringBetween(answerXml, "<standard_answer>", "</standard_answer>");
					step.setStandardAnswer(standardAnswer);
				}
				// 复读
				if(TopicTypeConstant.REPEAT.equals(stepType)) {
					String score = (String) stepFormJSON.get("score");
					step.setScore(score);
					String repeatXml = assembleRepeatStepContent(stepFormJSON);
					step.setParam(repeatXml);
					String standardAnswer = StringUtils.substringBetween(repeatXml, "<standard_answer>", "</standard_answer>");
					step.setStandardAnswer(standardAnswer);
				}
				// 应答
				if(TopicTypeConstant.STATE.equals(stepType)) {
					String score = (String) stepFormJSON.get("score");
					step.setScore(score);
					String stateXml = assembleStateStepContent(stepFormJSON);
					step.setParam(stateXml);
					String standardAnswer = StringUtils.substringBetween(stateXml, "<standard_answer>", "</standard_answer>");
					step.setStandardAnswer(standardAnswer);
				}
				// 语音
				if(TopicTypeConstant.VOICE.equals(stepType)) {
					String score = (String) stepFormJSON.get("score");
					step.setScore(score);
					String voiceXml = assembleVoiceStepContent(stepFormJSON);
					step.setParam(voiceXml);
					String standardAnswer = StringUtils.substringBetween(voiceXml, "<standard_answer>", "</standard_answer>");
					step.setStandardAnswer(standardAnswer);
				}
				// 听打录入
				if(TopicTypeConstant.CLOZE.equals(stepType)) {
					String score = computeClozeStepScore(stepFormJSON);
					step.setScore(score);
					String clozeXml = assembleClozeStepContent(stepFormJSON);
					step.setParam(clozeXml);
					String standardAnswer = StringUtils.substringBetween(clozeXml, "<standard_answer>", "</standard_answer>");
					step.setStandardAnswer(standardAnswer);
				}
				// 数据分析
				if(TopicTypeConstant.ANALYZE.equals(stepType)) {
					String score = (String) stepFormJSON.get("score");
					step.setScore(score);
					String analyzeXml = assembleAnalyzeStepContent(stepFormJSON);
					step.setParam(analyzeXml);
					String standardAnswer = StringUtils.substringBetween(analyzeXml, "<standard_answer>", "</standard_answer>");
					step.setStandardAnswer(standardAnswer);
				}
				// 话术编写
				if(TopicTypeConstant.VERBAL.equals(stepType)) {
					String score = (String) stepFormJSON.get("score");
					step.setScore(score);
					String verbalXml = assembleVerbalStepContent(stepFormJSON);
					step.setParam(verbalXml);
					String standardAnswer = StringUtils.substringBetween(verbalXml, "<standard_answer>", "</standard_answer>");
					step.setStandardAnswer(standardAnswer);
				}
				// 连线画图
				if(TopicTypeConstant.STEP_DRAW.equals(stepType)) {
					String score = (String) stepFormJSON.get("score");
					step.setScore(score);
					String verbalXml = assembleDrawStepContent(stepFormJSON);
					step.setParam(verbalXml);
					String standardAnswer = StringUtils.substringBetween(verbalXml, "<standard_answer>", "</standard_answer>");
					step.setStandardAnswer(standardAnswer);
				}
				// 实训营地
				if(TopicTypeConstant.STEP_CAMPING.equals(stepType)) {
					String promptContent = (String) stepFormJSON.get("campPrompt");
					step.setParam(promptContent);
					String standardAnswer = (String) stepFormJSON.get("campAnswer");
					step.setStandardAnswer(standardAnswer);
				}
				if(StringUtils.isNotBlank(step.getTopicId()) && StringUtils.isNotBlank(step.getBankId())
					&& StringUtils.isNotBlank(step.getType()) && StringUtils.isNotBlank(step.getTitle())) {
					if(step.getId() == null) {
						managerService.addBankStep(step);
					}else {
						managerService.updBankStep(step);
					}
				}
				result.put("stepId", step.getId());
			}
			if(StringUtils.isNotBlank(topic.getTopicId())) {
				managerService.updBankTopicTotalScore(topic);
				BankTopic bankTopic = managerService.schBankTopicById(topic.getTopicId());
				if(bankTopic != null) {
					result.put("totalScore", bankTopic.getTotalScore());
				}else {
					result.put("totalScore", 0);
				}
			}
			result.put("status", "1");
		}catch (Exception e) {
			result.put("status", "0");
		}
		response.getWriter().write(result.toJSONString());
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/loadBankTopicAndStep.action", method = RequestMethod.GET)
	public void loadBankTopicAndStep(@RequestParam("topicId") String topicId_, @RequestParam("stepId") String stepId_,
		HttpServletResponse response) throws IOException {
		JSONObject result = new JSONObject();
		if(StringUtils.isNotBlank(topicId_)) {

			result.put("topic", "");
		}
		if(StringUtils.isNotBlank(stepId_)) {
			try{
				Integer stepId = Integer.parseInt(stepId_);
				BankStep step = managerService.schBankStepById(stepId);
				StepJson stepJson = new StepJson(step);
				JSONObject step2json = stepJson.getStepJson();
				result.put("step", step2json);
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		response.getWriter().write(result.toJSONString());
	}

	/**
	 * 加载topic and step id
	 * @param topicId
	 * topicId
	 * @param response
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/loadTopicAndSteps.action")
	public void loadTopicAndSteps(@RequestParam(value = "topicId") String topicId,
			HttpServletResponse response) throws IOException {
		noCache(response); // Ajax方法设定无缓存
		JSONObject res = new JSONObject();
		JSONArray resArray = new JSONArray();
		// managerService.delBankStep(ids); // 删除数据
		BankTopic topic = managerService.schBankTopicById(topicId);
		BankStep schStep = new BankStep();
		if(topic != null) {
			schStep.setTopicId(topic.getTopicId());
			schStep.setCondition("ORDER BY CAST(title as SIGNED)");
			List<BankStep> steps = managerService.schBankStepList(schStep);
			if(steps != null) {
				for(BankStep step : steps) {
					JSONObject stepJson = new JSONObject();
					stepJson.put("stepId", step.getId());
					stepJson.put("stepName", step.getTitle());
					resArray.add(stepJson);
				}
			}
			res.put("topic", topic.getJson());
		}else {
			res.put("topic", "");
		}
		res.put("steps", resArray);
		response.getWriter().write(res.toJSONString());
		// topic.jj
		// managerService.sch
	}

	private String difficultyV2T(String value) {
		String text = "";
		if(StringUtils.isNotBlank(value)) {
			if(value.equals(TopicTypeConstant.PRIMARY)) {
				text = TopicTypeConstant.PRIMARY_STR;
			}
			if(value.equals(TopicTypeConstant.MIDDLE)) {
				text = TopicTypeConstant.MIDDLE_STR;
			}
			if(value.equals(TopicTypeConstant.HIGH)) {
				text = TopicTypeConstant.HIGH_STR;
			}
		}
		return text;
	}

	private String bookId2Name(String bookId) {
		String name = "";
		if(StringUtils.isNotBlank(bookId)) {
			try{
				int id = Integer.parseInt(bookId);
				Book book = bookService.schBookById(id);
				name = book.getName();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		return name;
	}

	/**
	 * 加载topic and step id
	 * @param bankId
	 * topicId
	 * @param response
	 */
	// @SuppressWarnings("unchecked")
	@RequestMapping(value = "/exportTheory.action")
	public void exportTheoryTopic(@RequestParam(value = "bankId") String bankId, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		List<BankTheory> theorys = managerService.schTopicAndStepByBankId(bankId);
		List<BankTheory> singles = new ArrayList<BankTheory>();
		List<BankTheory> multis = new ArrayList<BankTheory>();
		List<BankTheory> judges = new ArrayList<BankTheory>();
		List<BankTheory> clozeds = new ArrayList<BankTheory>();
		List<BankTheory> answers = new ArrayList<BankTheory>();
		if(theorys != null && theorys.size() > 0) {
			for(BankTheory theory : theorys) {
				if(TopicTypeConstant.SINGLE.equals(theory.getType())) {
					singles.add(theory);
				}
				if(TopicTypeConstant.MULTI.equals(theory.getType())) {
					multis.add(theory);
				}
				if(TopicTypeConstant.JUDGE.equals(theory.getType())) {
					judges.add(theory);
				}
				if(TopicTypeConstant.CLOZED.equals(theory.getType())) {
					clozeds.add(theory);
				}
				if(TopicTypeConstant.ANSWER.equals(theory.getType())) {
					answers.add(theory);
				}
			}
		}
		String path = request.getSession().getServletContext().getRealPath("/data/doc"); // 获取服务器路径
		SimpleDateFormat sdfTimeForPath = new SimpleDateFormat("yyyyMMddhhmm");// 格式刷时间
		String tempPath = sdfTimeForPath.format(new Date());// 格式刷时间
		String excelPath = path + File.separator + tempPath + ".xls";
		String excelName = tempPath + ".xls";
		HSSFWorkbook hssfWorkbook = new HSSFWorkbook();
		// HSSFSheet hssfSheet = hssfWorkbook.createSheet("错误日志");
		// HSSFRow firstRow = hssfSheet.createRow(0);
		if(singles.size() > 0) {
			String[] singleTitle = {"序号", "题目名称", "题目", "难度", "章节", "答案", "分值", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O",
				"P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
			HSSFSheet singleSheet = hssfWorkbook.createSheet("单选");
			HSSFRow firstRow = singleSheet.createRow(0);
			for(int i = 0; i < singleTitle.length; i++) {
				HSSFCell titleCell = firstRow.createCell(i);
				titleCell.setCellValue(singleTitle[i]);
			}
			for(int i = 0; i < singles.size(); i++) {
				HSSFRow nrow = singleSheet.createRow(i + 1);
				HSSFCell ncell = nrow.createCell(0);
				ncell.setCellValue(i + 1);
				// nCell.setCellValue(singles.get(i).getTitle());
				BankTheory theory = singles.get(i);
				BankStep step = new BankStep();
				String stepId = theory.getStepId();
				try{
					int stepId_ = Integer.parseInt(stepId);
					step.setId(stepId_);
				}catch (Exception e) {

				}
				String bookId = theory.getBookId();
				String bookName = bookId2Name(bookId);
				String difficulty = theory.getDifficulty();
				String difficultyValue = difficultyV2T(difficulty);

				step.setTitle(theory.getTitle());
				step.setBankId(theory.getBankId());
				step.setTopicId(theory.getTopicId());
				step.setType(theory.getType());
				step.setParam(theory.getParam());
				step.setDifficulty(theory.getDifficulty());
				step.setScore(theory.getScore());
				step.setStandardAnswer(theory.getStandardAnswer());
				StepJson stepJson = new StepJson(step);

				ncell.setCellValue(i + 1);
				ncell = nrow.createCell(1);
				ncell.setCellValue(stepJson.getTitle());
				ncell = nrow.createCell(2);
				ncell.setCellValue(stepJson.getPromptContent());
				ncell = nrow.createCell(3);
				ncell.setCellValue(difficultyValue);
				ncell = nrow.createCell(4);
				ncell.setCellValue(bookName);
				ncell = nrow.createCell(5);
				ncell.setCellValue(stepJson.getStandardAnswer());
				ncell = nrow.createCell(6);
				ncell.setCellValue(stepJson.getScore());
				
				Map<String, String> singleOptions = stepJson.getSingleChoiceContent();
				if(singleOptions != null) {
					Set<String> optionKeys = singleOptions.keySet();
					int j = 0;
					for(String optionKey : optionKeys) {
						ncell = nrow.createCell(7 + j);
						ncell.setCellValue(singleOptions.get(optionKey));
						++j;
					}
				}
			}
		}
		if(multis.size() > 0) {
			String[] multiTitle = {"序号", "题目名称", "题目", "难度", "章节", "答案", "分值", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K",
				"L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
			HSSFSheet multiSheet = hssfWorkbook.createSheet("多选");
			HSSFRow firstRow = multiSheet.createRow(0);
			for(int i = 0; i < multiTitle.length; i++) {
				HSSFCell titleCell = firstRow.createCell(i);
				titleCell.setCellValue(multiTitle[i]);
			}
			for(int i = 0; i < multis.size(); i++) {
				HSSFRow nrow = multiSheet.createRow(i + 1);
				HSSFCell ncell = nrow.createCell(0);
				ncell.setCellValue(i + 1);
				BankTheory theory = multis.get(i);
				BankStep step = new BankStep();
				String stepId = theory.getStepId();
				try{
					int stepId_ = Integer.parseInt(stepId);
					step.setId(stepId_);
				}catch(Exception e) {

				}
				String bookId = theory.getBookId();
				String bookName = bookId2Name(bookId);
				String difficulty = theory.getDifficulty();
				String difficultyValue = difficultyV2T(difficulty);

				step.setTitle(theory.getTitle());
				step.setBankId(theory.getBankId());
				step.setTopicId(theory.getTopicId());
				step.setType(theory.getType());
				step.setParam(theory.getParam());
				step.setDifficulty(theory.getDifficulty());
				step.setScore(theory.getScore());
				step.setStandardAnswer(theory.getStandardAnswer());
				StepJson stepJson = new StepJson(step);

				ncell.setCellValue(i + 1);
				ncell = nrow.createCell(1);
				ncell.setCellValue(stepJson.getTitle());
				ncell = nrow.createCell(2);
				ncell.setCellValue(stepJson.getPromptContent());
				ncell = nrow.createCell(3);
				ncell.setCellValue(difficultyValue);
				ncell = nrow.createCell(4);
				ncell.setCellValue(bookName);
				ncell = nrow.createCell(5);
				ncell.setCellValue(stepJson.getStandardAnswer());
				ncell = nrow.createCell(6);
				ncell.setCellValue(stepJson.getScore());
	
				Map<String, String> multiOptions = stepJson.getMultiChoiceContent();
				if(multiOptions != null) {
					Set<String> optionKeys = multiOptions.keySet();
					int j = 0;
					for(String optionKey : optionKeys) {
						ncell = nrow.createCell(7 + j);
						ncell.setCellValue(multiOptions.get(optionKey));
						++j;
					}
				}
	
			}
		}
		if(judges.size() > 0) {
			String[] judgeTitle = {"序号", "题目名称", "题目", "难度", "章节", "答案", "分值"};
			HSSFSheet judgeSheet = hssfWorkbook.createSheet("判断");
			HSSFRow firstRow = judgeSheet.createRow(0);
			for(int i = 0; i < judgeTitle.length; i++) {
				HSSFCell titleCell = firstRow.createCell(i);
				titleCell.setCellValue(judgeTitle[i]);
			}
			for(int i = 0; i < judges.size(); i++) {
				HSSFRow nrow = judgeSheet.createRow(i + 1);
				HSSFCell ncell = nrow.createCell(0);
				ncell.setCellValue(i + 1);
				BankTheory theory = judges.get(i);
				BankStep step = new BankStep();
				String stepId = theory.getStepId();
				try{
					int stepId_ = Integer.parseInt(stepId);
					step.setId(stepId_);
				}catch(Exception e) {

				}
				String bookId = theory.getBookId();
				String bookName = bookId2Name(bookId);
				String difficulty = theory.getDifficulty();
				String difficultyValue = difficultyV2T(difficulty);

				step.setTitle(theory.getTitle());
				step.setBankId(theory.getBankId());
				step.setTopicId(theory.getTopicId());
				step.setType(theory.getType());
				step.setParam(theory.getParam());
				step.setDifficulty(theory.getDifficulty());
				step.setScore(theory.getScore());
				step.setStandardAnswer(theory.getStandardAnswer());
				StepJson stepJson = new StepJson(step);

				ncell.setCellValue(i + 1);
				ncell = nrow.createCell(1);
				ncell.setCellValue(stepJson.getTitle());
				ncell = nrow.createCell(2);
				ncell.setCellValue(stepJson.getPromptContent());
				ncell = nrow.createCell(3);
				ncell.setCellValue(difficultyValue);
				ncell = nrow.createCell(4);
				ncell.setCellValue(bookName);
				ncell = nrow.createCell(5);
				ncell.setCellValue(stepJson.getStandardAnswer());
				ncell = nrow.createCell(6);
				ncell.setCellValue(stepJson.getScore());
			}
		}
		if(clozeds.size() > 0) {
			String[] clozedTitle = {"序号", "题目名称", "题目", "难度", "章节", "答案", "分值"};
			HSSFSheet clozedSheet = hssfWorkbook.createSheet("填空");
			HSSFRow firstRow = clozedSheet.createRow(0);
			for(int i = 0; i < clozedTitle.length; i++) {
				HSSFCell titleCell = firstRow.createCell(i);
				titleCell.setCellValue(clozedTitle[i]);
			}
			for(int i = 0; i < clozeds.size(); i++) {
				HSSFRow nrow = clozedSheet.createRow(i + 1);
				HSSFCell ncell = nrow.createCell(0);
				ncell.setCellValue(i + 1);
				BankTheory theory = clozeds.get(i);
				BankStep step = new BankStep();
				String stepId = theory.getStepId();
				try{
					int stepId_ = Integer.parseInt(stepId);
					step.setId(stepId_);
				}catch(Exception e) {

				}
				String bookId = theory.getBookId();
				String bookName = bookId2Name(bookId);
				String difficulty = theory.getDifficulty();
				String difficultyValue = difficultyV2T(difficulty);

				step.setTitle(theory.getTitle());
				step.setBankId(theory.getBankId());
				step.setTopicId(theory.getTopicId());
				step.setType(theory.getType());
				step.setParam(theory.getParam());
				step.setDifficulty(theory.getDifficulty());
				step.setScore(theory.getScore());
				step.setStandardAnswer(theory.getStandardAnswer());
				StepJson stepJson = new StepJson(step);

				ncell.setCellValue(i + 1);
				ncell = nrow.createCell(1);
				ncell.setCellValue(stepJson.getTitle());
				ncell = nrow.createCell(2);
				ncell.setCellValue(stepJson.getPromptContent());
				ncell = nrow.createCell(3);
				ncell.setCellValue(difficultyValue);
				ncell = nrow.createCell(4);
				ncell.setCellValue(bookName);
				ncell = nrow.createCell(5);
				ncell.setCellValue(stepJson.getStandardAnswer());
				ncell = nrow.createCell(6);
				ncell.setCellValue(stepJson.getScore());
			}
		}
		if(answers.size() > 0) {
			String[] answerTitle = {"序号", "题目名称", "题目", "难度", "章节", "答案", "分值"};
			HSSFSheet answerSheet = hssfWorkbook.createSheet("问答");
			HSSFRow firstRow = answerSheet.createRow(0);
			for(int i = 0; i < answerTitle.length; i++) {
				HSSFCell titleCell = firstRow.createCell(i);
				titleCell.setCellValue(answerTitle[i]);
			}
			for(int i = 0; i < answers.size(); i++) {
				HSSFRow nrow = answerSheet.createRow(i + 1);
				HSSFCell ncell = nrow.createCell(0);
				ncell.setCellValue(i + 1);
				BankTheory theory = answers.get(i);
				BankStep step = new BankStep();
				String stepId = theory.getStepId();
				try{
					int stepId_ = Integer.parseInt(stepId);
					step.setId(stepId_);
				}catch(Exception e) {

				}
				String bookId = theory.getBookId();
				String bookName = bookId2Name(bookId);
				String difficulty = theory.getDifficulty();
				String difficultyValue = difficultyV2T(difficulty);

				step.setTitle(theory.getTitle());
				step.setBankId(theory.getBankId());
				step.setTopicId(theory.getTopicId());
				step.setType(theory.getType());
				step.setParam(theory.getParam());
				step.setDifficulty(theory.getDifficulty());
				step.setScore(theory.getScore());
				step.setStandardAnswer(theory.getStandardAnswer());
				StepJson stepJson = new StepJson(step);

				ncell.setCellValue(i + 1);
				ncell = nrow.createCell(1);
				ncell.setCellValue(stepJson.getTitle());
				ncell = nrow.createCell(2);
				ncell.setCellValue(stepJson.getPromptContent());
				ncell = nrow.createCell(3);
				ncell.setCellValue(difficultyValue);
				ncell = nrow.createCell(4);
				ncell.setCellValue(bookName);
				ncell = nrow.createCell(5);
				ncell.setCellValue(stepJson.getStandardAnswer());
				ncell = nrow.createCell(6);
				ncell.setCellValue(stepJson.getScore());
			}
		}
		try {
			File uploadFile = new File(excelPath);
			uploadFile.createNewFile();
			FileOutputStream stream = FileUtils.openOutputStream(uploadFile);
			hssfWorkbook.write(stream);
			stream.close();
		}catch (IOException e) {
			e.printStackTrace();
		}
		try {
			File downFile = new File(excelPath);// 文件对象
			BufferedInputStream fis = new BufferedInputStream(
					new FileInputStream(downFile));// 输入流
			byte[] buffer = new byte[fis.available()];// 定义字节
			fis.read(buffer);// 读
			fis.close();// 关闭
			// 清空response
			response.reset();
			// 设置response的Header
			response.addHeader("Content-Disposition", "attachment;filename="
					+ new String(excelName.getBytes()));// 设置response的Header
			response.addHeader("Content-Length", "" + downFile.length());// 设置response的Header
			OutputStream toClient = new BufferedOutputStream(
					response.getOutputStream());// 输出流
			response.setContentType("application/vnd.ms-excel;charset=gb2312");// 设置属性
			toClient.write(buffer);// 写
			toClient.flush();// 刷新
			toClient.close();// 关闭
		} catch (Exception e) {
			e.printStackTrace();// 打印异常
		}
	}

	/**
	 * 组装话术编写xml
	 */
	private String assembleVerbalStepContent(JSONObject stepJson) {
		StringBuilder sb = new StringBuilder();
		String stepName = (String) stepJson.get("stepName");
		// String stepType = (String) stepJson.get("stepType");
		String score = (String) stepJson.get("score");
		String prompt = (String) stepJson.get("prompt");
		String promptVoice = (String) stepJson.get("promptVoice");
		String standardAnswer = (String) stepJson.get("standardAnswer");
		String origiName = (String) stepJson.get("origiName");
		String fileName = (String) stepJson.get("fileName");
		sb.append("<step type='");
		sb.append(ExerConstant.TOPIC_STEP_TYPE_QUESTION);
		sb.append("'>");
			sb.append("<title>");
				sb.append(stepName);
			sb.append("</title>");
			sb.append("<score>");
				sb.append(score);
			sb.append("</score>");
			sb.append("<prompt audio='");
			sb.append(promptVoice);
			sb.append("' repeat='' origiName='");
			sb.append(origiName);
			sb.append("' fileName='");
			sb.append(fileName);
			sb.append("'>");
				sb.append(prompt);
			sb.append("</prompt>");
			sb.append("<answer type='");
			sb.append(ExerConstant.TOPIC_STEP_TYPE_QUESTION_ANSWER);
			sb.append("' length='' />");
			sb.append("<standard_answer>");
				sb.append(standardAnswer);
			sb.append("</standard_answer>");
		sb.append("</step>");

		return sb.toString();
	}


	/** 
	 * 组装数据分析xml
	 */
	private String assembleAnalyzeStepContent(JSONObject stepJson) {
		StringBuilder sb = new StringBuilder();
		String stepName = (String) stepJson.get("stepName");
		// String stepType = (String) stepJson.get("stepType");
		String score = (String) stepJson.get("score");
		String prompt = (String) stepJson.get("prompt");
		String promptVoice = (String) stepJson.get("promptVoice");
		String standardAnswer = (String) stepJson.get("standardAnswer");
		String origiName = (String) stepJson.get("origiName");
		String fileName = (String) stepJson.get("fileName");
		sb.append("<step type='");
		sb.append(ExerConstant.TOPIC_STEP_TYPE_QUESTION);
		sb.append("'>");
			sb.append("<title>");
				sb.append(stepName);
			sb.append("</title>");
			sb.append("<score>");
				sb.append(score);
			sb.append("</score>");
			sb.append("<prompt audio='");
			sb.append(promptVoice);
			sb.append("' repeat='' origiName='");
			sb.append(origiName);
			sb.append("' fileName='");
			sb.append(fileName);
			sb.append("'>");
				sb.append(prompt);
			sb.append("</prompt>");
			sb.append("<answer type='");
			sb.append(ExerConstant.TOPIC_STEP_TYPE_QUESTION_ANSWER);
			sb.append("' length='' />");
			sb.append("<standard_answer>");
				sb.append(standardAnswer);
			sb.append("</standard_answer>");
		sb.append("</step>");

		return sb.toString();
	}

	/**
	 * 组装连线画图xml
	 */
	private String assembleDrawStepContent(JSONObject stepJson) {
		StringBuilder sb = new StringBuilder();
		String stepName = (String) stepJson.get("stepName");
		// String stepType = (String) stepJson.get("stepType");
		String score = (String) stepJson.get("score");
		String prompt = (String) stepJson.get("prompt");
		String promptVoice = (String) stepJson.get("promptVoice");
		String standardAnswer = (String) stepJson.get("standardAnswer");
		String origiName = (String) stepJson.get("origiName");
		String fileName = (String) stepJson.get("fileName");
		sb.append("<step type='");
		sb.append(ExerConstant.TOPIC_STEP_TYPE_QUESTION);
		sb.append("'>");
			sb.append("<title>");
				sb.append(stepName);
			sb.append("</title>");
			sb.append("<score>");
				sb.append(score);
			sb.append("</score>");
			sb.append("<prompt audio='");
			sb.append(promptVoice);
			sb.append("' repeat='' origiName='");
			sb.append(origiName);
			sb.append("' fileName='");
			sb.append(fileName);
			sb.append("'>");
				sb.append(prompt);
			sb.append("</prompt>");
			sb.append("<answer type='");
			sb.append(ExerConstant.TOPIC_STEP_TYPE_QUESTION_ANSWER);
			sb.append("' length='' />");
			sb.append("<standard_answer>");
				sb.append(standardAnswer);
			sb.append("</standard_answer>");
		sb.append("</step>");

		return sb.toString();
	}


	/**
	 * 组装听打录入xml
	 * @param stepJson
	 * @return
	 */
	private String assembleClozeStepContent(JSONObject stepJson) {
		StringBuilder sb = new StringBuilder();

		String stepName = (String) stepJson.get("stepName");
		String promptVoice = (String) stepJson.get("promptVoice");
		// String prompt = (String) stepJson.get("prompt");
		String prompt = "";
		String score = "";
		String standardAnswer = "";
		Map<String, String> clozePrompts = new HashMap<String, String>();
		Map<String, String> clozeContent = new HashMap<String, String>();
		Map<String, String> clozeScore = new HashMap<String, String>();
		Set<String> keys = stepJson.keySet();
		for(String key : keys) {
			if(!key.contains("prompt_") && !key.contains("standardAnswer_") && !key.contains("score_")) {
				continue;
			}
			if(key.contains("prompt_")) {
				String clozePrompt = StringUtils.substringBetween(key, "_");
				clozePrompts.put(clozePrompt, (String) stepJson.get(key));
				continue;
			}
			if(key.contains("standardAnswer_")) {
				String clozedOption = StringUtils.substringBetween(key, "_");
				clozeContent.put(clozedOption, (String) stepJson.get(key));
				continue;
			}
			if(key.contains("score_")) {
				String scoreOption = StringUtils.substringBetween(key, "_");
				clozeScore.put(scoreOption, (String) stepJson.get(key));
				continue;
			}
		}
		Set<String> promptKeys = clozePrompts.keySet();
		for(String promptKey : promptKeys) {
			if(StringUtils.isBlank(prompt)) {
				prompt = clozePrompts.get(promptKey) + "[]";
				// prompt = clozePrompts.get(promptKey);
				continue;
			}
			prompt = prompt + clozePrompts.get(promptKey) + "[]";
			// prompt = prompt + "," + clozePrompts.get(promptKey);
		}
		Set<String> clozeKeys = clozeContent.keySet();
		for(String clozedKey : clozeKeys) {
			if(StringUtils.isBlank(standardAnswer)) {
				standardAnswer = clozeContent.get(clozedKey);
				continue;
			}
			standardAnswer = standardAnswer + "," + clozeContent.get(clozedKey);
		}
		Set<String> scoreKeys = clozeScore.keySet();
		int stepScore = 0;
		for(String scoreKey : scoreKeys) {
			// if(StringUtils.isBlank(score)) {
			// 	score = clozeScore.get(scoreKey);
			// 	continue;
			// }
			// score = score + "," + clozeScore.get(scoreKey);

			String optionScore = clozeScore.get(scoreKey);
			try{
				int optionScoreNum = Integer.parseInt(optionScore);
				stepScore = stepScore + optionScoreNum;
			}catch (Exception e) {
				stepScore = stepScore + 0;
				e.printStackTrace();
			}
		}
		sb.append("<step type='");
		sb.append(ExerConstant.TOPIC_STEP_TYPE_QUESTION);
		sb.append("'>");
			sb.append("<title>");
				sb.append(stepName);
			sb.append("</title>");
			sb.append("<score>");
				// sb.append(score);
				sb.append(stepScore+"");
			sb.append("</score>");
			sb.append("<prompt audio='");
			sb.append(promptVoice);
			sb.append("' repeat=''>");
				sb.append(prompt);
			sb.append("</prompt>");
			sb.append("<answer type='");
			sb.append(ExerConstant.TOPIC_STEP_TYPE_QUESTION_CLOZE);
			sb.append("' length='' >");
			sb.append("</answer>");
			sb.append("<standard_answer>");
				sb.append(standardAnswer);
			sb.append("</standard_answer>");
		sb.append("</step>");

		return sb.toString();
	}

	/**
	 * 组装语音提示xml
	 * @param stepJson
	 * @return
	 */
	private String assembleVoiceStepContent(JSONObject stepJson) {
		StringBuilder sb = new StringBuilder();
		String stepName = (String) stepJson.get("stepName");
		// String stepType = (String) stepJson.get("stepType");
		String score = (String) stepJson.get("score");
		String prompt = (String) stepJson.get("prompt");
		String promptVoice = (String) stepJson.get("promptVoice");
		String standardAnswer = (String) stepJson.get("standardAnswer");
		sb.append("<step type='");
		sb.append(ExerConstant.TOPIC_STEP_TYPE_VOICE);
		sb.append("'>");
			sb.append("<title>");
				sb.append(stepName);
			sb.append("</title>");
			sb.append("<score>");
				sb.append(score);
			sb.append("</score>");
			sb.append("<prompt audio='");
			sb.append(promptVoice);
			sb.append("' repeat=''");
			sb.append(">");
				sb.append(prompt);
			sb.append("</prompt>");
			sb.append("<answer type='' length='' />");
			sb.append("<standard_answer>");
				sb.append(standardAnswer);
			sb.append("</standard_answer>");
		sb.append("</step>");
		return sb.toString();
	}

	/**
	 * 组装应答xml
	 * @param stepJson
	 * @return
	 */
	private String assembleStateStepContent(JSONObject stepJson) {
		StringBuilder sb = new StringBuilder();
		String stepName = (String) stepJson.get("stepName");
		// String stepType = (String) stepJson.get("stepType");
		String score = (String) stepJson.get("score");
		String prompt = (String) stepJson.get("prompt");
		String promptVoice = (String) stepJson.get("promptVoice");
		String standardAnswer = (String) stepJson.get("standardAnswer");
		sb.append("<step type='");
		sb.append(ExerConstant.TOPIC_STEP_TYPE_STATE);
		sb.append("'>");
			sb.append("<title>");
				sb.append(stepName);
			sb.append("</title>");
			sb.append("<score>");
				sb.append(score);
			sb.append("</score>");
			sb.append("<prompt audio='");
			sb.append(promptVoice);
			sb.append("' repeat=''");
			sb.append(">");
				sb.append(prompt);
			sb.append("</prompt>");
			sb.append("<answer type='' length='' />");
			sb.append("<standard_answer>");
				sb.append(standardAnswer);
			sb.append("</standard_answer>");
		sb.append("</step>");
		return sb.toString();
	}

	/**
	 * 组装复读xml
	 * @param stepJson
	 * @return
	 */
	private String assembleRepeatStepContent(JSONObject stepJson) {
		StringBuilder sb = new StringBuilder();
		String stepName = (String) stepJson.get("stepName");
		// String stepType = (String) stepJson.get("stepType");
		String score = (String) stepJson.get("score");
		String prompt = (String) stepJson.get("prompt");
		String promptVoice = (String) stepJson.get("promptVoice");
		String standardAnswer = (String) stepJson.get("standardAnswer");
		sb.append("<step type='");
		sb.append(ExerConstant.TOPIC_STEP_TYPE_REPEAT);
		sb.append("'>");
			sb.append("<title>");
				sb.append(stepName);
			sb.append("</title>");
			sb.append("<score>");
				sb.append(score);
			sb.append("</score>");
			sb.append("<prompt audio='");
			sb.append(promptVoice);
			sb.append("' repeat=''");
			sb.append(">");
				sb.append(prompt);
			sb.append("</prompt>");
			sb.append("<answer type='' length='' />");
			sb.append("<standard_answer>");
				sb.append(standardAnswer);
			sb.append("</standard_answer>");
		sb.append("</step>");
		return sb.toString();
	}

	/**
	 * 组装问答xml
	 */
	// @SuppressWarnings("unchecked")
	private String assembleAnswerStepContent(JSONObject stepJson) {
		StringBuilder sb = new StringBuilder();
		String stepName = (String) stepJson.get("stepName");
		// String stepType = (String) stepJson.get("stepType");
		String score = (String) stepJson.get("score");
		String prompt = (String) stepJson.get("prompt");
		String promptVoice = (String) stepJson.get("promptVoice");
		String standardAnswer = (String) stepJson.get("standardAnswer");
		String origiName = (String) stepJson.get("origiName");
		String fileName = (String) stepJson.get("fileName");
		sb.append("<step type='");
		sb.append(ExerConstant.TOPIC_STEP_TYPE_QUESTION);
		sb.append("'>");
			sb.append("<title>");
				sb.append(stepName);
			sb.append("</title>");
			sb.append("<score>");
				sb.append(score);
			sb.append("</score>");
			sb.append("<prompt audio='");
			sb.append(promptVoice);
			sb.append("' repeat='' origiName='");
			sb.append(origiName);
			sb.append("' fileName='");
			sb.append(fileName);
			sb.append("'>");
				sb.append(prompt);
			sb.append("</prompt>");
			sb.append("<answer type='");
			sb.append(ExerConstant.TOPIC_STEP_TYPE_QUESTION_ANSWER);
			sb.append("' length='' />");
			sb.append("<standard_answer>");
				sb.append(standardAnswer);
			sb.append("</standard_answer>");
		sb.append("</step>");
		return sb.toString();
	}

	/**
	 * 组装填空题xml
	 */
	@SuppressWarnings("unchecked")
	private String assembleClozedStepContent(JSONObject stepJson) {
		StringBuilder sb = new StringBuilder();
		String stepName = (String) stepJson.get("stepName");
		String prompt = (String) stepJson.get("prompt");
		String score = "";
		String standardAnswer = "";
		Map<String, String> clozedContent = new HashMap<String, String>();
		Map<String, String> clozedScore = new HashMap<String, String>();
		Set<String> keys = stepJson.keySet();
		for(String key : keys) {
			if(!key.contains("standardAnswer_") && !key.contains("everyScore")) {
				continue;
			}
			if(key.contains("standardAnswer_")) {
				String clozedOption = StringUtils.substringBetween(key, "_");
				clozedContent.put(clozedOption, (String) stepJson.get(key));
				continue;
			}
			if(key.contains("everyScore")) {
				// String scoreOption = StringUtils.substringBetween(key, "_");
				// clozedScore.put(scoreOption, (String) stepJson.get(key));
				score = (String) stepJson.get(key);
				continue;
			}
		}
		Set<String> clozedKeys = clozedContent.keySet();
		for(String clozedKey : clozedKeys) {
			if(StringUtils.isBlank(standardAnswer)) {
				standardAnswer = clozedContent.get(clozedKey);
				continue;
			}
			standardAnswer = standardAnswer + "," + clozedContent.get(clozedKey);
		}
		Set<String> scoreKeys = clozedScore.keySet();
		for(String scoreKey : scoreKeys) {
			if(StringUtils.isBlank(score)) {
				score = clozedScore.get(scoreKey);
				continue;
			}
			score = score + "," + clozedScore.get(scoreKey);
		}
		sb.append("<step type='");
		sb.append(ExerConstant.TOPIC_STEP_TYPE_QUESTION);
		sb.append("'>");
			sb.append("<title>");
				sb.append(stepName);
			sb.append("</title>");
			sb.append("<score>");
				sb.append(score);
			sb.append("</score>");
			sb.append("<prompt audio='' repeat=''>");
				sb.append(prompt);
			sb.append("</prompt>");
			sb.append("<answer type='");
			sb.append(ExerConstant.TOPIC_STEP_TYPE_QUESTION_CLOZE);
			sb.append("' length='' >");
			sb.append("</answer>");
			sb.append("<standard_answer>");
				sb.append(standardAnswer);
			sb.append("</standard_answer>");
		sb.append("</step>");
		return sb.toString();
	}

	/**
	 * 计算填空题步骤总分
	 * @param stepJson
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private String computeClozedStepScore(JSONObject stepJson) {
		Set<String> keys = stepJson.keySet();
		int score = 0;
		int clozedNum = 0;
		for(String key : keys) {
			if(key.contains("everyScore")) {
				String optionScore = (String) stepJson.get(key);
				if(StringUtils.isNotBlank(optionScore)) {
					try{
						int optionScoreNum = Integer.parseInt(optionScore);
						score = optionScoreNum;
					}catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			if(key.contains("standardAnswer_")) {
				clozedNum = clozedNum + 1;
			}
		}
		int clozedTotalScore = score * clozedNum;
		return clozedTotalScore + "";
	}

	/**
	 * 计算挺大录入题目总分
	 */
	@SuppressWarnings("unchecked")
	private String computeClozeStepScore(JSONObject stepJson) {
		Set<String> keys = stepJson.keySet();
		int score = 0;
		for(String key : keys) {
			if(key.contains("score_")) {
				String optionScore = (String) stepJson.get(key);
				if(StringUtils.isNotBlank(optionScore)) {
					try{
						int optionScoreNum = Integer.parseInt(optionScore);
						score = score + optionScoreNum;
					}catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
		return score + "";
	}

	/**
	 * 组装判断题xml
	 */
	private String assembleJudgeStepContent(JSONObject stepJson) {
		StringBuilder sb = new StringBuilder();
		String stepName = (String) stepJson.get("stepName");
		String score = (String) stepJson.get("score");
		String prompt = (String) stepJson.get("prompt");
		String promptVoice = (String) stepJson.get("promptVoice");
		// String standardAnswerTrue = (String) stepJson.get("standardAnswer_TRUE_");
		// String standardAnswerFalse = (String) stepJson.get("standardAnswer_FALSE_");
		String standardAnswer = (String) stepJson.get("standardAnswer");
		boolean standardAnswerValue = Boolean.parseBoolean(standardAnswer);
		// Set<String> jsonKeys = stepJson.keySet();
		sb.append("<step type='");
		sb.append(ExerConstant.TOPIC_STEP_TYPE_QUESTION);
		sb.append("'>");
			sb.append("<title>");
				sb.append(stepName);
			sb.append("</title>");
			sb.append("<score>");
				sb.append(score);
			sb.append("</score>");
			sb.append("<prompt audio='");
			sb.append(promptVoice);
			sb.append("' repeat=''>");
				sb.append(prompt);
			sb.append("</prompt>");
			sb.append("<answer type='");
			sb.append(ExerConstant.TOPIC_STEP_TYPE_QUESTION_JUDGE);
			sb.append("' />");
			sb.append("<standard_answer>");
			// if(StringUtils.isNotBlank(standardAnswerTrue) && StringUtils.isBlank(standardAnswerFalse)) {
			// 	sb.append(true);
			// }
			if(standardAnswerValue) {
				sb.append(true);
			}else {
				sb.append(false);
			}
			// if(StringUtils.isBlank(standardAnswerTrue) && StringUtils.isNotBlank(standardAnswerFalse)) {
			// 	sb.append(false);
			// }
			sb.append("</standard_answer>");
		sb.append("</step>");
		return sb.toString();
	}

	/**
	 * 组装多选step xml
	 */
	@SuppressWarnings("unchecked")
	private String assembleMultiStepContent(JSONObject stepJson) {
		StringBuilder sb = new StringBuilder();
		Map<String, String> choiceContent = new HashMap<String, String>();
		Map<String, String> choiceVoice = new HashMap<String, String>();
		Map<String, String> choiceAnswer = new HashMap<String, String>();
		Set<String> jsonKeys = stepJson.keySet();
		for(String jsonKey : jsonKeys) {
			if(jsonKey.contains("choice_") && !jsonKey.contains("_voice")) {
				String option = StringUtils.substringBetween(jsonKey, "_");
				choiceContent.put(option, (String) stepJson.get(jsonKey));
				continue;
			}
			if(jsonKey.contains("choice_") && jsonKey.contains("_voice")) {
				String option = StringUtils.substringBetween(jsonKey, "_");
				choiceVoice.put(option, (String) stepJson.get(jsonKey));
				continue;
			}
			if(jsonKey.contains("standardAnswer_")) {
				String option = StringUtils.substringBetween(jsonKey, "_");
				choiceAnswer.put(option, (String) stepJson.get(jsonKey));
				continue;
			}
		}
		// String stepId = (String) stepJson.get("stepId");
		// String stepTopicId = (String) stepJson.get("stepTopicId");
		// String stepBankId = (String) stepJson.get("stepBankId");
		String stepName = (String) stepJson.get("stepName");
		// String stepType = (String) stepJson.get("stepType");
		String score = (String) stepJson.get("score");
		String prompt = (String) stepJson.get("prompt");
		String promptVoice = (String) stepJson.get("promptVoice");
		sb.append("<step type='");
		sb.append(ExerConstant.TOPIC_STEP_TYPE_QUESTION);
		sb.append("'>");
			sb.append("<title>");
				sb.append(stepName);
			sb.append("</title>");
			sb.append("<score>");
				sb.append(score);
			sb.append("</score>");
			sb.append("<prompt audio='");
			sb.append(promptVoice);
			sb.append("' repeat=''>");
				sb.append(prompt);
			sb.append("</prompt>");
			sb.append("<answer type='");
			sb.append(ExerConstant.TOPIC_STEP_TYPE_QUESTION_CHOICE_MULTI);
			sb.append("'>");
			Set<String> choiceKeys = choiceContent.keySet();
			for(String key : choiceKeys) {
				sb.append("<choice id='");
				sb.append(key);
				sb.append("' audio='");
				sb.append(choiceVoice.get(key));
				sb.append("'>");
					sb.append(choiceContent.get(key));
				sb.append("</choice>");
			}
			sb.append("</answer>");
			Set<String> answerKeys = choiceAnswer.keySet();
			String standardAnswer = "";
			// int keySize = answerKeys.size();
			// int keyIndex = 0;
			for(String answerKey : answerKeys) {
				// ++keyIndex;
				if(StringUtils.isBlank(standardAnswer)) {
					standardAnswer = choiceAnswer.get(answerKey);
					continue;
				}
				standardAnswer = standardAnswer + "," + choiceAnswer.get(answerKey);
			}
			sb.append("<standard_answer>");
				sb.append(standardAnswer);
			sb.append("</standard_answer>");
		sb.append("</step>");
		return sb.toString();
	}

	/**
	 * 组装单选xml
	 */
	@SuppressWarnings("unchecked")
	private String assembleSingleStepContent(JSONObject stepJson) {
		String prompt = (String) stepJson.get("prompt");
		String promptVoice = (String) stepJson.get("promptVoice");
		String answer = "";
		StringBuilder sb = new StringBuilder();
		Map<String, String> choiceContent = new HashMap<String, String>();
		Map<String, String> choiceVoice = new HashMap<String, String>();
		Set<String> jsonKeys = stepJson.keySet();
		for(String jsonKey : jsonKeys) {
			if(jsonKey.contains("choice_") && !jsonKey.contains("_voice")) {
				String option = StringUtils.substringBetween(jsonKey, "_");
				choiceContent.put(option, (String) stepJson.get(jsonKey));
				continue;
			}
			if(jsonKey.contains("choice_") && jsonKey.contains("_voice")) {
				String option = StringUtils.substringBetween(jsonKey, "_");
				choiceVoice.put(option, (String) stepJson.get(jsonKey));
				continue;
			}
			if(jsonKey.contains("standardAnswer")) {
				answer = (String) stepJson.get(jsonKey);
				continue;
			}
		}
		sb.append("<step type='");
		sb.append(ExerConstant.TOPIC_STEP_TYPE_QUESTION);
		sb.append("'>");
			sb.append("<title>");
				sb.append(stepJson.get("stepName"));
			sb.append("</title>");
			sb.append("<score>");
				sb.append(stepJson.get("score"));
			sb.append("</score>");
			sb.append("<prompt audio='");
			sb.append(promptVoice);
			sb.append("' repeat='' >");
				sb.append(prompt);
			sb.append("</prompt>");
			sb.append("<answer type='");
			sb.append(ExerConstant.TOPIC_STEP_TYPE_QUESTION_CHOICE_SINGLE);
			sb.append("'>");
				Set<String> choiceSet = choiceContent.keySet();
				for(String choice : choiceSet) {
					sb.append("<choice id='" + choice + "' audio='" + choiceVoice.get(choice) + "'>" + choiceContent.get(choice) + "</choice>");
				}
			sb.append("</answer>");
		sb.append("<standard_answer>");
		sb.append(answer);
		sb.append("</standard_answer>");
		sb.append("</step>");
		return sb.toString();
	}

	/**
	 * 删除方法
	 * @param ids
	 * 待删除数据的主键数组
	 * @param response
	 */
	@RequestMapping(value = "/delBankTopic.action")
	public void delBankTopic(@RequestParam(value = "ids[]") String[] ids,
			HttpServletResponse response) {
		noCache(response); // Ajax方法设定无缓存
		managerService.delBankTopic(ids); // 删除数据
	}

	@RequestMapping(value = "/cleansingTopicAndStep.action")
	public void cleansingTopicAndStep(HttpServletRequest request, HttpServletResponse response) throws IOException {
		noCache(response);
		JSONObject result = new JSONObject();

		List<BankTopic> tarTopics = new ArrayList<BankTopic>();
		List<BankStep> tarSteps = new ArrayList<BankStep>();

		BankTopic schTopic = new BankTopic();
		schTopic.setPagesize(10000);
		// List<BankTopic> oriTopics = managerService.schBankTopic(schTopic);
		List<BankTopic> oriTopics = managerService.schRemainTopic(schTopic);
		if(oriTopics != null && oriTopics.size() > 0) {
			for(BankTopic topic : oriTopics) {
				String topicTitle = topic.getTitle();
				String topicType = topic.getType();
				String topicLevel = topic.getLevel();
				String keyWords = topic.getKeyWords();
				if(StringUtils.isNotBlank(topicTitle)) {
					if(topicTitle.startsWith("T09")) {
						topic.setType(TopicTypeConstant.THEORY);
						tarTopics.add(topic);
						continue;
					}
					if(topicTitle.startsWith("T01") || topicTitle.startsWith("T02") || 
						topicTitle.startsWith("T03") || topicTitle.startsWith("T04") || 
						topicTitle.startsWith("T05") || topicTitle.startsWith("T06")) {
						topic.setType(TopicTypeConstant.PRACTICAL);
						tarTopics.add(topic);
						continue;
					}
					if(topicTitle.startsWith("T07") || topicTitle.startsWith("T08") || 
						topicTitle.contains("听打录入")) {
						topic.setType(TopicTypeConstant.SKILL);
						tarTopics.add(topic);
						continue;
					}
					if(topicTitle.contains("邮寄业务") || topicTitle.contains("邮件业务") 
						|| topicTitle.contains("短信业务") || topicTitle.contains("传真业务")
						|| topicTitle.contains("综合实训") || topicTitle.contains("销售业务")
						|| topicTitle.contains("呼叫中心") || topicTitle.contains("技巧") 
						|| topicTitle.contains("市场调研") || topicTitle.contains("会议邀请")
						|| topicTitle.contains("认识") || topicTitle.contains("职业素养")
						|| topicTitle.contains("心理素质") || topicTitle.contains("处理流程")
						|| topicTitle.contains("设计") || topicTitle.contains("KPI") 
						|| topicTitle.contains("填空题") || topicTitle.contains("多选题")
						|| topicTitle.contains("单选题") || topicTitle.contains("判断题")
						|| topicTitle.contains("问答题") || topicTitle.contains("呼出业务试卷")) {
							topic.setType(TopicTypeConstant.THEORY);
							tarTopics.add(topic);
							continue;
					}
					if(topicTitle.contains("T0Y") || topicTitle.contains("理解分析") ||
						topicTitle.contains("广臣") || topicTitle.contains("艺龙项目")
						|| topicTitle.contains("语音互动题") || topicTitle.contains("听不清楚")
						|| topicTitle.contains("呼入投诉业务") || topicTitle.contains("客户信息服务")
						|| topicTitle.contains("语言互动题")) {
						topic.setType(TopicTypeConstant.PRACTICAL);
						tarTopics.add(topic);
						continue;
					}
					if(topicTitle.contains("呼出电话销售") || topicTitle.contains("数据分析及运用")
						|| topicTitle.contains("填表画图")) {
						topic.setType(TopicTypeConstant.SKILL);
						tarTopics.add(topic);
						continue;
					}

					// 座席员必备常识选择题
					if(topicTitle.contains("坐席员必备常识选择题")) {
						topic.setType(TopicTypeConstant.THEORY);
						tarTopics.add(topic);
						continue;
					}
					if(StringUtils.isNotBlank(topicType)) {
						if(topicType.contains("实训题")) {
							topic.setType(TopicTypeConstant.PRACTICAL);
							tarTopics.add(topic);
							continue;
						}
						if(topicType.contains("实训营地")) {
							topic.setType(TopicTypeConstant.CAMPING);
							tarTopics.add(topic);
							continue;
						}
					}
				}
				if(StringUtils.isNotBlank(topicLevel)) {
					// 业务实训
					if(topicLevel.contains("业务实训")) {
						topic.setType(TopicTypeConstant.PRACTICAL);
						tarTopics.add(topic);
						continue;
					}
				}
				if(StringUtils.isNotBlank(keyWords)) {
					if(keyWords.contains("密云")) {
						topic.setType(TopicTypeConstant.THEORY);
						tarTopics.add(topic);
						continue;
					}
				}
			}
		}
		
		for(BankTopic bt : tarTopics) {
			String topicId = bt.getTopicId();
			String topicType = bt.getType();
			if(StringUtils.isNotBlank(topicId)) {
				BankStep schStep = new BankStep();
				schStep.setTopicId(topicId);
				schStep.setPagesize(1000);
				List<BankStep> oriStep = managerService.schBankStep(schStep);
				if(oriStep != null && oriStep.size() > 0) {
					for(BankStep step : oriStep) {
						String stepParam = step.getParam();
						if(StringUtils.isNotBlank(stepParam)) {
							if(stepParam.contains("type=\"choice_single\"")) {
								step.setType(TopicTypeConstant.SINGLE);
								tarSteps.add(step);
								continue;
							}
							if(stepParam.contains("type=\"choice_multi\"")) {
								step.setType(TopicTypeConstant.MULTI);
								tarSteps.add(step);
								continue;
							}
							if(stepParam.contains("type=\"judge\"")) {
								step.setType(TopicTypeConstant.JUDGE);
								tarSteps.add(step);
								continue;
							}
							if(stepParam.contains("type=\"cloze\"")) {
								if(StringUtils.isNotBlank(topicType)) {
									if(
										// topicType.equals(TopicTypeConstant.THEORY) || 
										// topicType.equals(TopicTypeConstant.PRACTICAL) 
										!stepParam.contains(".wav") &&
										!stepParam.contains(".mp3")
										) {
										step.setType(TopicTypeConstant.CLOZED);
										String param = analysisClozedScore(step.getParam());
										step.setParam(param);
										tarSteps.add(step);
										continue;
									}
									if(topicType.equals(TopicTypeConstant.SKILL) ||
										stepParam.contains(".wav") || 
										stepParam.contains(".mp3")) {
										step.setType(TopicTypeConstant.CLOZE);
										String param = analysisClozeScore(step.getParam());
										step.setParam(param);
										tarSteps.add(step);
										continue;
									}
								}
							}
							if(stepParam.contains("type=\"answer\"")) {
								if(StringUtils.isNotBlank(topicType)) {
									if(topicType.equals(TopicTypeConstant.THEORY) || 
										topicType.equals(TopicTypeConstant.PRACTICAL)) {
										step.setType(TopicTypeConstant.ANSWER);
										tarSteps.add(step);
										continue;
									}
									if(topicType.equals(TopicTypeConstant.SKILL)) {
										// step.setType(TopicTypeConstant.ANALYZE);
										// step.setType(TopicTypeConstant.VERBAL);
									}
								}
							}
							if(stepParam.contains("type=\"repeat\"")) {
								step.setType(TopicTypeConstant.REPEAT);
								tarSteps.add(step);
								continue;
							}
							if(stepParam.contains("type=\"state\"")) {
								step.setType(TopicTypeConstant.STATE);
								tarSteps.add(step);
								continue;
							}
							if(stepParam.contains("type=\"voice\"")) {
								step.setType(TopicTypeConstant.VOICE);
								tarSteps.add(step);
								continue;
							}
						}
					}
				}
			}
		}
		if(tarTopics != null && tarTopics.size() > 0) {
			managerService.addTopicTempBatch(tarTopics);
		}
		if(tarSteps != null && tarSteps.size() > 0) {
			managerService.addStepTempBatch(tarSteps);
		}
		result.put("status", "1");
		response.getWriter().write(result.toJSONString());
	}

	private String analysisClozedScore(String questionStem) {
		XmlUtil xmlUtil = new XmlUtil(); // 调用xml工具栏
		xmlUtil.setCharset("utf-8"); // 解决解析后乱码
		Element step = xmlUtil.loadXmlStr(questionStem);
		Element score = step.getChild("score");
		Element prompt = step.getChild("prompt");
		String scoreText = score.getText();
		String promptText = prompt.getText();
		if(StringUtils.isNotBlank(scoreText) && scoreText.contains(",")) {
			if(StringUtils.isNotBlank(promptText) && promptText.contains("[]")) {
				String[] scores = scoreText.split(",");
				int scoreTotal = 0;
				int clozedLength = promptText.length() - promptText.replaceAll("\\[]", "").length();
				for(int i = 0; i < scores.length; i++) {
					String sc = scores[i];
					if(StringUtils.isNotBlank(sc)) {
						try{
							int scNumber = Integer.parseInt(sc);
							scoreTotal = scoreTotal + scNumber;
						}catch(Exception e) {
							scoreTotal = scoreTotal + 0;
						}
					}
				}
				int everyScore = scoreTotal/clozedLength;
				score.setText("");
				score.setText(""+everyScore);
			}
		}
		
		// String stepStr = step.;
		XMLOutputter xmlToStr = new XMLOutputter();
		String stepStr = xmlToStr.outputString(step);
		System.out.println(stepStr);
		return stepStr;
	}

	private String analysisClozeScore(String questionStem) {
		XmlUtil xmlUtil = new XmlUtil(); // 调用xml工具栏
		xmlUtil.setCharset("utf-8"); // 解决解析后乱码
		Element step = xmlUtil.loadXmlStr(questionStem);
		Element score = step.getChild("score");
		Element standardAnswer = step.getChild("standard_answer");
		String standardAnswerText = standardAnswer.getText();
		int scoreTotal = 0;
		if(StringUtils.isNotBlank(standardAnswerText) && standardAnswerText.contains(",")) {
			// String[] sdas = standardAnswerText.split(",");
			// scoreTotal = standardAnswerText.length() - standardAnswerText.replaceAll(",", "").length();
			scoreTotal = standardAnswerText.replaceAll(",", "").length();
		}
		score.setText("");
		score.setText("" + scoreTotal);
		XMLOutputter xmlToStr = new XMLOutputter();
		String stepStr = xmlToStr.outputString(step);
		System.out.println(stepStr);
		return stepStr;
	}

	@SuppressWarnings("unchecked")
	private JSONObject formatForm2Json(String formData) {
		if(StringUtils.isBlank(formData)) {
			return null;
		}

		Map<String, String> map = new HashMap<String, String>();
		JSONObject formatResult = new JSONObject();
		String[] splitByAnd = formData.split("&");
		if(splitByAnd != null && splitByAnd.length > 0) {
			for(int i = 0; i < splitByAnd.length; i++) {
				String[] splitByEqual = null;
				if(splitByAnd[i].contains("camp")) {
					String first = splitByAnd[i].substring(0, splitByAnd[i].indexOf("="));
					String second = splitByAnd[i].substring(splitByAnd[i].indexOf("=") + 1, splitByAnd[i].length());
					String[] campContent = {first, second};
					splitByEqual = campContent;
				}else {
					splitByEqual = splitByAnd[i].split("=");
				}
				if(splitByEqual != null) {
					if(splitByEqual.length == 0) {
						continue;
					}
					if(splitByEqual.length == 1) {
						map.put(splitByEqual[0], "");
					}
					if(splitByEqual.length > 1) {
						map.put(splitByEqual[0], splitByEqual[1]);
					}
				}
			}
		}
		Set<String> keys = map.keySet();
		for(String key : keys) {
			String formSerialize = "";
			try{
				formSerialize = java.net.URLDecoder.decode(map.get(key), "UTF-8");
				System.out.println(formSerialize);
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
				return null;
			}
			formatResult.put(key, formSerialize);
		}
		return formatResult;
	}

	/**
	 * JSP页面跳转
	 * @param page
	 * jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value = "/page.action")
	public String goPage(String page) { // 放过本Ctrl的页面，不合法的跳转到error页。
		if ("index".equals(page) || "bankTopic_sch".equals(page)// 判断跳转页面
				|| "bankTopic_vie".equals(page) || "bankTopic_upd".equals(page)) {// 判断跳转页面
			return "/manager/" + page;// 跳转页面
		} else {
			return "/error";// 跳转错误页面
		}
	}
}
