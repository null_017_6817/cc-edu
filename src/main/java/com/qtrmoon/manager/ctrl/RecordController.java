package com.qtrmoon.manager.ctrl;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qtrmoon.book.util.FilePath;
import com.qtrmoon.camp.pojo.Hytopic;
import com.qtrmoon.camp.serdao.ICampService;
import com.qtrmoon.common.BaseController;
import com.qtrmoon.examination.pojo.ExerTopic;
import com.qtrmoon.examination.serdao.IExaminationService;
import com.qtrmoon.manager.pojo.Record;
import com.qtrmoon.manager.pojo.Stud;
import com.qtrmoon.manager.serdao.IManagerService;

/**
 * 录音控制器
 */
@Controller
@RequestMapping("/manager/record")
public class RecordController extends BaseController {
	@Autowired
	private IManagerService managerService; // 声明模块Service实例
	@Autowired
	private IExaminationService examinationService; // 声明模块Service实例
	@Autowired
	private ICampService campService; // 声明模块Service实例

	/**
	 * 查询方法，自带分页设置。
	 * @param record
	 * 承载查询条件的Bean
	 * @param request
	 * 可用来获取非Bean内的参数
	 * @param response
	 * 返回请求,输出Json
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schRecord.action")
	public void schRecord(Record record, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		noCache(response);// Ajax方法设定无缓存
		record.setCondition(""); // 防sql注入
		List<Record> list = new ArrayList<Record>();// 定义对象
		if (StringUtils.isNotBlank(record.getTaskName())) {// 判断
			list = managerService.schRecordByTask(record);// 调用Service查询数据
		} else {
			list = managerService.schRecord(record); // 调用Service查询数据
		}
		/* 将数据Bean的集合转换为Json集合 */
		JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
		JSONObject obj; // 声明用于构造Bean的Json对象
		for (Record u : list) { // 循环查询的数据集构造Json数据集
			obj = u.getJsonInDict(); // 将Bean转换为Json对象
			obj.put("_oper", u.getId()); // 为"操作列"设定主键值
			if (obj.get("taskId") != null) {// 判断
				if (StringUtils.isNotBlank(obj.get("taskId").toString())) {// 判断
					if(obj.get("type") != null && "1".equals(obj.get("type").toString())){
						ExerTopic topic = examinationService// 查询数据
								.schExerTopicById(Integer.valueOf(obj.get("taskId")// 查询数据
										.toString()));// 查询数据
						if (topic != null) {// 判断
							obj.put("taskName", topic.getTitle());// put数据
							
						}
					}
					if(obj.get("type") != null && "2".equals(obj.get("type").toString())){
						Hytopic hytopic = campService
								.schHytopicById( Integer.valueOf( obj.get("taskId").toString() ));
						if (hytopic != null) {// 判断
							obj.put("taskName", hytopic.getTitle());// put数据
						} 
					}
				}
			}
			System.out.println(obj.get("taskName"));
			//所有taskName为空的值均显示为fromAgent_toAgent_createTimestr
			if(obj.get("taskName")==null||StringUtils.isBlank(obj.get("taskName").toString())){
				obj.put("taskName", obj.get("fromAgent")+"_"+obj.get("toAgent")+"_"+obj.get("createTimestr"));// put数据
			}
			datas.add(obj); // 添加Json对象到Json集合
		}
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		JSONObject res = new JSONObject(); // 声明页面返回Json
		res.put("total", record.getDatasize()); // 设置数据总记录数
		res.put("rows", datas); // 设置数据集
		response.getWriter().print(res.toJSONString()); // 输出到页面
	}

	/**
	 * 查询录音列表 
	 * @param record
	 * @param request
	 * @param response
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value = "/getRecords.action")
	public Map<String, Object> getRecords(Record record,
			HttpServletRequest request, HttpServletResponse response) {
		noCache(response); // Ajax方法设定无缓存
		Map<String, Object> result = new HashMap<String, Object>();// 定义对象
		try {
			if (record != null) {// 判断
				record.setCondition("");// 防止sql注入
				List<Record> records = managerService.schRecordList(record);// 查询
			
				
				//重新拼接，设置voice信息
				if (records != null && records.size() > 0) {// 判断
					for (int i = 0; i < records.size(); i++) {// 循环
						Record re = records.get(i);// 获取元素
						if (re != null) {// 判断
							if (re.getVoice() != null) {// 判断
								String projectName = request.getContextPath();// 获取参数
								re.setVoicePath(projectName// 设置参数
										+ "/camp/trainCamp/downloadVoice.action?wav="// 设置参数
										+ re.getVoice());// 设置参数
							}
						}
					}
				}
				
				
				// 设置基本信息
				//检索学生姓名,设置到record中
				Stud stud = new Stud();
				stud.setNumber(record.getFromNum());
				List<Stud> fromlist =	managerService.schStud(stud);
				stud.setNumber(record.getToNum());
				List<Stud> tolist =	managerService.schStud(stud);
				if(fromlist.size()>0){
					record.setFromNum(record.getFromNum()+","+fromlist.get(0).getName());
				}
				if(fromlist.size()>0){
					record.setToNum(record.getToNum()+","+tolist.get(0).getName());
				}
				
				result.put("infodata",record );
				result.put("success", true);// 设置属性
				result.put("data", records);// 设置属性
				
			} else {
				result.put("success", false);// 设置属性
				result.put("msg", "必要参数为空");// 设置属性
			}
		} catch (Exception e) {
			e.printStackTrace();// 打印异常
			result.put("success", false);// 设置属性
			result.put("msg", "服务器异常");// 设置属性
		} finally {
			return result;// 返回结果
		}
	}

	/**
	 * 预添加or预修改or查看方法
	 * @param record
	 * 预添加时可传入一些初始值；预修改和查看时用来承载主键值
	 * @param toL
	 * 挂字典的字段是否需要转为字典Label值。true则进行转换
	 * @param response
	 * 返回请求,输出Json
	 * @throws IOException
	 */
	@RequestMapping(value = "/vieRecord.action")
	public void vieRecord(Record record, Boolean toL,
			HttpServletResponse response) throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (toL != null && toL) { // 查看页面加载数据时调用，返回的对象已进行字典转换
			if (record.getId() != null && record.getId() != 0) { // 检测主键非空
				record = managerService.schRecordById(record.getId()); // 按主键查询数据
				response.getWriter().print(record.getJsonInDict()); // 输出经过字典转换Json对象到查看页(vie_)
			}
		} else { // 修改页面加载数据时调用
			if (record.getId() != null && record.getId() != 0) { // 检查主键非空
				record = managerService.schRecordById(record.getId()); // 按主键查询数据
				response.getWriter().print(record.getJson()); // 输出Json对象到修改页(upd_)
			} else { // 添加页面加载数据时调用，可设置添加页面中一些属性的预设值。
				response.getWriter().print(new Record().getJson()); // 输出Json对象到添加页(upd_)
			}
		}
	}

	/**
	 * 添加或修改的提交方法
	 * @param record
	 * 数据表单Bean
	 * @param response
	 * 返回请求
	 * @throws IOException
	 */
	@RequestMapping(value = "/updRecord.action")
	public void updRecord(Record record, HttpServletResponse response)
			throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (record.getId() == null) { // 检测主键为空则
			managerService.addRecord(record); // 添加数据
		} else { // 主键非空则
			managerService.updRecord(record); // 修改数据
		}
	}

	/**
	 * 删除方法
	 * @param ids
	 * 待删除数据的主键数组
	 * @param response
	 * 返回请求
	 */
	@RequestMapping(value = "/delRecord.action")
	public void delRecord(@RequestParam(value = "ids[]") Integer[] ids,
			HttpServletResponse response) {
		noCache(response); // Ajax方法设定无缓存
		managerService.delRecord(ids); // 删除数据
	}

	/**
	 * JSP页面跳转
	 * @param page
	 * jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value = "/page.action")
	public String goPage(String page) { // 放过本Ctrl的页面，不合法的跳转到error页。
		if ("index".equals(page) || "record_sch".equals(page)// 判断页面
				|| "record_vie".equals(page) || "record_upd".equals(page)// 判断页面
				|| "record_video".equals(page)) {// 判断页面
			return "/manager/" + page;// 跳转页面
		} else {
			return "/error";// 跳转错误页面
		}
	}
	
	/**
	 * 下载录音文件
	 * @param request
	 * @param response
	 * @throws IOException 
	 */
	@RequestMapping(value = "/downVoiceFile.action")
	public void downVoiceFile(HttpServletRequest request, HttpServletResponse response) throws IOException{
		String fileName = request.getParameter("fileName");
		BufferedInputStream fis = null;
		OutputStream toClient = null;// 输出流
		try{
			if(fileName != null){
				String fullPath = FilePath.getFilePath("recordServer")+fileName;
				File file = new File(fullPath);
				if(file.exists()){
					fis = new BufferedInputStream(
							new FileInputStream(file));// 输入流
					byte[] buffer = new byte[fis.available()];// 定义字节
					fis.read(buffer);// 读
					// 清空response
					response.reset();
					// 设置response的Header
					response.addHeader("Content-Disposition", "attachment;filename="
							+ new String(fileName.getBytes()));// 设置response的Header
					response.addHeader("Content-Length", "" + file.length());// 设置response的Header
					response.setContentType("octet-stream;charset=utf-8");// 设置属性
					
					toClient = new BufferedOutputStream(
							response.getOutputStream());// 输出流
					toClient.write(buffer);// 写
					toClient.flush();// 刷新
				}else{
					String msg = "<div style='text-align: center;padding-top: 15%;'><span style='text-align: center;"
							+ "color: red;font-size: 20px;'>文件不存在，下载失败!<span></div>";
					response.setContentType("text/html");//设置contentType属性
					toClient = new BufferedOutputStream(
							response.getOutputStream());// 输出流
					toClient.write(msg.getBytes());// 写
					toClient.flush();// 刷新
				}
			}else{
				String msg = "<div style='text-align: center;padding-top: 15%;'><span style='text-align: center;"
						+ "color: red;font-size: 20px;'>文件名为空，下载失败 !<span></div>";
				response.setContentType("text/plain");//设置contentType属性
				toClient = new BufferedOutputStream(
						response.getOutputStream());// 输出流
				toClient.write(msg.getBytes());// 写
				toClient.flush();// 刷新
			}
		} catch ( Exception e ){
			e.printStackTrace();
		}finally{
			if(fis != null){
				fis.close();// 关闭
			}
			if(toClient != null ){
				toClient.flush();
				toClient.close();
			}
		}
	}
}
