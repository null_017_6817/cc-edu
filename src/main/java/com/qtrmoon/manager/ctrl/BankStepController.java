package com.qtrmoon.manager.ctrl;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qtrmoon.common.BaseController;
import com.qtrmoon.manager.constant.TopicTypeConstant;
import com.qtrmoon.manager.pojo.BankStep;
import com.qtrmoon.manager.serdao.IManagerService;

/**
 * 题目步骤类控制层
 */
@Controller
@RequestMapping("/manager/bankStep")
public class BankStepController extends BaseController {
	@Autowired
	private IManagerService managerService; // 声明模块Service实例

	/**
	 * 查询方法，自带分页设置。
	 * @param bankStep
	 * 承载查询条件的Bean
	 * @param request
	 * 可用来获取非Bean内的参数
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schBankStep.action")
	public void schBankStep(BankStep bankStep, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		noCache(response);// Ajax方法设定无缓存
		bankStep.setCondition(""); // 防sql注入
		bankStep.assLike("vccId", "type", "title", "score");// 拼接%

		List<BankStep> list = managerService.schBankStep(bankStep); // 调用Service查询数据
		/* 将数据Bean的集合转换为Json集合 */
		JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
		JSONObject obj; // 声明用于构造Bean的Json对象
		for (BankStep u : list) { // 循环查询的数据集构造Json数据集
			obj = u.getJsonInDict(); // 将Bean转换为Json对象
			obj.put("_oper", u.getId()); // 为"操作列"设定主键值
			datas.add(obj); // 添加Json对象到Json集合
		}
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		JSONObject res = new JSONObject(); // 声明页面返回Json
		res.put("total", bankStep.getDatasize()); // 设置数据总记录数
		res.put("rows", datas); // 设置数据集
		response.getWriter().print(res.toJSONString()); // 输出到页面
	}

	/**
	 * 预添加or预修改or查看方法
	 * @param bankStep
	 * 预添加时可传入一些初始值；预修改和查看时用来承载主键值
	 * @param toL
	 * 挂字典的字段是否需要转为字典Label值。true则进行转换
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/vieBankStep.action")
	public void vieBankStep(BankStep bankStep, Boolean toL,
			HttpServletResponse response) throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (toL != null && toL) { // 查看页面加载数据时调用，返回的对象已进行字典转换
			if (bankStep.getId() != null && bankStep.getId() != 0) { // 检测主键非空
				bankStep = managerService.schBankStepById(bankStep.getId()); // 按主键查询数据
				response.getWriter().print(bankStep.getJsonInDict()); // 输出经过字典转换Json对象到查看页(vie_)
			}
		} else { // 修改页面加载数据时调用
			if (bankStep.getId() != null && bankStep.getId() != 0) { // 检查主键非空
				bankStep = managerService.schBankStepById(bankStep.getId()); // 按主键查询数据
				response.getWriter().print(bankStep.getJson()); // 输出Json对象到修改页(upd_)
			} else { // 添加页面加载数据时调用，可设置添加页面中一些属性的预设值。
				response.getWriter().print(new BankStep().getJson()); // 输出Json对象到添加页(upd_)
			}
		}
	}

	/**
	 * 添加或修改的提交方法
	 * @param bankStep
	 * 数据表单Bean
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/updBankStep.action")
	public void updBankStep(BankStep bankStep, HttpServletResponse response)
			throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (bankStep.getId() == null) { // 检测主键为空则
			managerService.addBankStep(bankStep); // 添加数据
		} else { // 主键非空则
			managerService.updBankStep(bankStep); // 修改数据
		}
	}

	/**
	 * 删除方法
	 * @param ids
	 * 待删除数据的主键数组
	 * @param response
	 */
	@RequestMapping(value = "/delBankStep.action")
	public void delBankStep(@RequestParam(value = "ids[]") Integer[] ids,
			HttpServletResponse response) {
		noCache(response); // Ajax方法设定无缓存
		managerService.delBankStep(ids); // 删除数据
	}

	/**
	 * 分类统计各个题型的数量
	 * @param bankIds ,request
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value="countTopicNum.action")
	public Map<String,Object> countTopicNum(@RequestBody String[] bankIds,
			HttpServletRequest request){
		Map<String,Object> result = new HashMap<String,Object>();
		try{
			if(bankIds != null && bankIds.length > 0){
				Map<String,Object> param = new HashMap<String,Object>();
				// 题录类型
				param.put("topicType", TopicTypeConstant.THEORY);//题目类型为【理论题】 
				// 步骤类型
				StringBuilder stepType = new StringBuilder();
				stepType.append("('");
				stepType.append(TopicTypeConstant.SINGLE+"','");// 步骤类型：单选
				stepType.append(TopicTypeConstant.MULTI+"','");// 步骤类型：多选
				stepType.append(TopicTypeConstant.ANSWER+"','");// 步骤类型：问答
				stepType.append(TopicTypeConstant.JUDGE+"','");// 步骤类型：判断
				stepType.append(TopicTypeConstant.CLOZED+"'");// 步骤类型：填空
				stepType.append(")");
				param.put("stepType", stepType.toString());
				// 题库ID
				String bankId = "";
				for( int i = 0; i < bankIds.length; i++ ){
					String itemId = bankIds[i];
					if(itemId != null){
						if( !"".equals(bankId) ){
							bankId = bankId + ",'"+itemId+"'";
						}else{
							bankId = "'"+itemId+"'";
						}
					}
				}
				if( !"".equals(bankId)){
					param.put("bankId", "("+bankId +")");
					Map<String,Object> data = managerService.updGroupNum(param);
					result.put("data", data);
				}
			}
			result.put("success", true);
		}catch(Exception e){
			e.printStackTrace();
			result.put("success", false);
			result.put("msg", "服务器异常,请稍后再试！");
		}finally{
			return result;
		}
	}
	
	/**
	 * JSP页面跳转
	 * @param page
	 * jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value = "/page.action")
	public String goPage(String page) { // 放过本Ctrl的页面，不合法的跳转到error页。
		if ("index".equals(page) || "bankStep_sch".equals(page)// 判断页面
				|| "bankStep_vie".equals(page) || "bankStep_upd".equals(page)) {// 判断页面
			return "/manager/" + page;// 跳转页面
		} else {
			return "/error";// 错误页面
		}
	}
}
