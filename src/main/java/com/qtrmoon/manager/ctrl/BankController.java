package com.qtrmoon.manager.ctrl;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.text.DecimalFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.aspectj.util.FileUtil;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.qtrmoon.book.pojo.Book;
import com.qtrmoon.book.serdao.IBookService;
import com.qtrmoon.book.util.FilePath;
import com.qtrmoon.common.BaseController;
import com.qtrmoon.examination.constant.ExerConstant;
import com.qtrmoon.manager.constant.TopicDifficultyConstant;
import com.qtrmoon.manager.constant.TopicTypeConstant;
import com.qtrmoon.manager.pojo.Bank;
import com.qtrmoon.manager.pojo.BankStep;
import com.qtrmoon.manager.pojo.BankTopic;
import com.qtrmoon.manager.pojo.DirTree;
import com.qtrmoon.manager.pojo.ExcelError;
import com.qtrmoon.manager.serdao.IManagerService;
import com.qtrmoon.sysmanage.SysConstant;
import com.qtrmoon.toolkit.MD5;
import com.qtrmoon.toolkit.XmlUtil;

/**
 * 题库类控制层
 */
@Controller
@RequestMapping("/manager/bank")
public class BankController extends BaseController {
	@Autowired
	private IManagerService managerService; // 声明模块Service实例

	@Autowired
	private IBookService bookService;

	/**
	 * 查询方法，自带分页设置。
	 * 
	 * @param bank
	 *            承载查询条件的Bean
	 * @param request
	 *            可用来获取非Bean内的参数
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schBank.action")
	public void schBank(Bank bank, HttpServletRequest request, HttpServletResponse response) throws IOException {
		noCache(response);// ajax 无缓存
		bank.setCondition(""); // 防sql注入
		bank.assLike("vccId", "bankId", "bankName", "profession", "remark", // 拼接%
				"createTime");// 拼接%

		List<Bank> list = managerService.schBank(bank); // 调用Service查询数据
		/* 将数据Bean的集合转换为Json集合 */
		JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
		JSONObject obj; // 声明用于构造Bean的Json对象
		for (Bank u : list) { // 循环查询的数据集构造Json数据集
			obj = u.getJsonInDict(); // 将Bean转换为Json对象
			obj.put("_oper", u.getBankId()); // 为"操作列"设定主键值
			datas.add(obj); // 添加Json对象到Json集合
		}
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		JSONObject res = new JSONObject(); // 声明页面返回Json
		res.put("total", bank.getDatasize()); // 设置数据总记录数
		res.put("rows", datas); // 设置数据集
		response.getWriter().print(res.toJSONString()); // 输出到页面
	}

	/**
	 * 预添加or预修改or查看方法
	 * 
	 * @param bank
	 *            预添加时可传入一些初始值；预修改和查看时用来承载主键值
	 * @param toL
	 *            挂字典的字段是否需要转为字典Label值。true则进行转换
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/vieBank.action")
	public void vieBank(Bank bank, Boolean toL, HttpServletResponse response) throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (toL != null && toL) { // 查看页面加载数据时调用，返回的对象已进行字典转换
			if (bank.getBankId() != null && !"".equals(bank.getBankId())// 检测主键非空
					&& !"0".equals(bank.getBankId())) { // 检测主键非空
				bank = managerService.schBankById(bank.getBankId()); // 按主键查询数据
				response.getWriter().print(bank.getJsonInDict()); // 输出经过字典转换Json对象到查看页(vie_)
			}
		} else { // 修改页面加载数据时调用
			if (bank.getBankId() != null && !"".equals(bank.getBankId())// 检查主键非空
					&& !"0".equals(bank.getBankId())) { // 检查主键非空
				bank = managerService.schBankById(bank.getBankId()); // 按主键查询数据
				response.getWriter().print(bank.getJson()); // 输出Json对象到修改页(upd_)
			} else { // 添加页面加载数据时调用，可设置添加页面中一些属性的预设值。
				response.getWriter().print(new Bank().getJson()); // 输出Json对象到添加页(upd_)
			}
		}
	}

	/**
	 * 添加或修改的提交方法
	 * 
	 * @param bank
	 *            数据表单Bean
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/updBank.action")
	public void updBank(Bank bank, HttpServletResponse response) throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (StringUtils.isBlank(bank.getBankId())) { // 检测主键为空则
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 获取时间
			String now = sdf.format(new Date());// 格式化时间
			bank.setCreateTime(now);// 设置时间
			managerService.addBank(bank); // 添加数据
		} else { // 主键非空则
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 获取时间
			String now = sdf.format(new Date());// 格式化时间
			bank.setCreateTime(now);// 设置时间
			managerService.updBank(bank); // 修改数据
		}
	}

	/**
	 * 删除方法
	 * 
	 * @param ids
	 *            待删除数据的主键数组
	 * @param response
	 */
	@RequestMapping(value = "/delBank.action")
	public void delBank(@RequestParam(value = "ids[]") String[] ids, HttpServletResponse response) {
		noCache(response); // Ajax方法设定无缓存
		managerService.delBank(ids); // 删除数据
	}

	/**
	 * JSP页面跳转
	 * 
	 * @param page
	 *            jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value = "/page.action")
	public String goPage(String page, String bankId, Model model) { // 放过本Ctrl的页面，不合法的跳转到error页。
		if ("index".equals(page) || "bank_sch".equals(page)// 判断跳转
				|| "bank_vie".equals(page) || "bank_upd".equals(page)// 判断跳转
				|| "newbankTopic_sch".equals(page)// 判断跳转
				|| "bankTopic_file".equals(page)) {// 判断跳转
			model.addAttribute("bankId", bankId);// 放置参数
			return "/manager/" + page;// 返回页面
		} else {
			return "/error";// 返回错误页面
		}
	}

	// 接收上传文件
	private Integer bankId;// 接收上传文件

	/**
	 * 获取 bankid
	 * 
	 * @return
	 */
	public Integer getBankId() {
		return bankId;
	}

	/**
	 * 上传文件
	 * 
	 * @param bankId
	 * @param file
	 * @param req
	 * @param response
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/uploadFile.action", method = RequestMethod.POST, produces = "text/html;charset=utf-8")
	public void uploadFile(@RequestParam("bankId") String bankId, @RequestParam("file") CommonsMultipartFile file,
			HttpServletRequest req, HttpServletResponse response) throws Exception {
		if (file.getSize() > 0) {// 判断参数
			String filename = file.getOriginalFilename(); // 获取文件名字作为保存到服务器的名称
			String path = req.getSession().getServletContext().getRealPath("/data/doc"); // 获取服务器路径
			SimpleDateFormat sdfTimeForPath = new SimpleDateFormat("yyyyMMddhhmm");// 格式刷时间
			String tempPath = sdfTimeForPath.format(new Date());// 格式刷时间
			String uploadPath = path + File.separator + tempPath;
			File f = new File(uploadPath, filename); // 进行路径拼接 前半部分路径+文件名称
			if(!f.getParentFile().exists()) {
				f.getParentFile().mkdirs();
			}
			file.transferTo(f);// 上传附件
			// response.getWriter().write("成功!");// 打印
			String newPath = uploadPath.toString() + File.separator + filename;// 拼接名称
			if (filename.endsWith("xml")) {
				parsexml(bankId, newPath);
				
				JSONObject res = new JSONObject();
				res.put("status", "1");
				res.put("msg", "成功!");
				
//				response.getWriter().write("成功!");// 打印
				response.getWriter().write(res.toJSONString());// 打印
			}else if (filename.endsWith("zip")) {

				// ZipUtil.unZip(newPath, path); // 用unZip解压文件或者文件夹
				// 解决解压文件中包含中文目录时，在Win系列服务器下显示乱码的问题
				ZipUtil.unZipDecode(newPath, uploadPath);
				
				String newfilename = filename.toString().substring(0, filename.length() - 4);// 获取没有.zip后缀的字符串
				String pathZip = uploadPath.toString() + File.separator + newfilename; // 获取解压后文件夹路径
				File dir = new File(pathZip);// 文件对象
				// 生成目录树根节点，并保存到数据库，根节点pid默认为0
				String ipAddr = req.getRemoteAddr();
				Timestamp currentTime = new Timestamp((new Date()).getTime());
				String md5Sign = MD5.getInstance().getMD5ofStr(ipAddr + currentTime.toString());
				String rawPath = dir.getPath();
				DirTree root = new DirTree();
				root.setPid(0);
				root.setType(1);
				root.setIsLeaf(0);
				root.setIsBlank(0);
				root.setDirName(dir.getName());
				root.setRawPath(rawPath);
				root.setMd5Sign(md5Sign);
				root.setCreateTime(currentTime);
				root.setCreaterIp(ipAddr);
				managerService.addDirTree(root);
				// 根据根节点，递归生成目录树
				generateDirTree(dir, ipAddr, currentTime, md5Sign, root.getId());
				// 解析目录中包含的xml(Topic),并将xml同目录下的文件输出到对应路径下
				String targetPath = FilePath.getFilePath("dataAudioAddress") + bankId;
				List<DirTree> lists = managerService.schDirTree(md5Sign);
				for (DirTree dt : lists) {
					if (dt.getType() != 0) {
						continue;
					}
					String xmlPath = dt.getRawPath();
					String topicId = "";
					try {
						topicId = parsexmlForDB(bankId, xmlPath);
						// xml解析成功，更新xml解析标识 1-成功
						dt.setBankId(bankId);
						dt.setTopicId(topicId);
						dt.setParseFlag(1);
						dt.setParseDesc("success");
						managerService.updDirTree(dt);
					}catch(Exception e) {
						// xml解析异常，更新xml解析标识 0-失败
						dt.setParseFlag(0);
						dt.setParseDesc(e.getMessage());
						managerService.updDirTree(dt);
						continue;
					}
					// 将解析成功的xml及其同目录下的文件输出到对应目录中去
					String finalPath = targetPath + File.separator + topicId + File.separator;
					DirTree parentDir = managerService.schDirTreeById(dt.getPid());
					if (parentDir != null && parentDir.getType() == 1) {
						File rawDir = new File(parentDir.getRawPath());
						File[] rawFiles = rawDir.listFiles();
						for (int i = 0; i < rawFiles.length; i++) {
							if (rawFiles[i].isDirectory()) {
								continue;
							}
							String rawFileName = rawFiles[i].getName();
							// 读取文件(缓存字节流)
							BufferedInputStream in = new BufferedInputStream(new FileInputStream(rawFiles[i]));
							// 写入相应的文件
							File finalFile = new File(finalPath + rawFileName);
							if(!finalFile.getParentFile().exists()) {
								finalFile.getParentFile().mkdirs();
							}
							finalFile.createNewFile();
							BufferedOutputStream out = new BufferedOutputStream(
									new FileOutputStream(finalFile));
							// 读取数据
							// 一次性取多少字节
							byte[] bytes = new byte[2048];
							// 接受读取的内容(n就代表的相关数据，只不过是数字的形式)
							int n = -1;
							// 循环取出数据
							while ((n = in.read(bytes, 0, bytes.length)) != -1) {
								// 转换成字符串
								String str = new String(bytes, 0, n, "GBK");
								System.out.println(str);
								// 写入相关文件
								out.write(bytes, 0, n);
							}
							// 清楚缓存
							out.flush();
							// 关闭流
							in.close();
							out.close();
						}
					}
				}
				//  数据库中保存目录树后 删除解压后的目录
				try{
					FileUtils.deleteDirectory(dir);
				}catch(Exception e) {
					e.printStackTrace();
				}

				JSONObject res = new JSONObject();
				res.put("status", "1");
				res.put("msg", "成功!");
				response.getWriter().write(res.toJSONString());// 打印
			}else if(filename.endsWith("xls") || filename.endsWith("xlsx")) {
				boolean parseFlag = parseExcel(bankId, newPath, uploadPath, filename);
				JSONObject res = new JSONObject();
				if(parseFlag) {
					res.put("status", "1");
					// response.getWriter().write("1");// 成功
					response.getWriter().write(res.toJSONString());
				}else {
					res.put("status", "0");
					String logfile = tempPath + "%2FERROR-" + filename;
					res.put("logfile", logfile);
					// response.getWriter().write("0");// 失败
					response.getWriter().write(res.toJSONString());
				}
				
			}else{
				JSONObject res = new JSONObject();
				res.put("status", "2");
				res.put("msg", "请正确上传文件！！！");
				response.getWriter().write(res.toJSONString());
			}
		}
	}

	/**
	 * 根据父节点，递归生成目录树，保存到cc_dir_tree数据表中
	 * @param dir
	 * @param ip
	 * @param time
	 * @param sign
	 * @param parentId
	 */
	private void generateDirTree(File dir, String ip, Timestamp time, String sign, Integer parentId) {
		File[] files = dir.listFiles();
		for (int i = 0; i < files.length; i++) {
			if (!files[i].isDirectory()) {
				String fileName = files[i].toString();
				DirTree dtf = new DirTree();
				dtf.setCreaterIp(ip);
				dtf.setCreateTime(time);
				dtf.setPid(parentId);
				dtf.setMd5Sign(sign);
				dtf.setDirName(fileName);
				if (fileName.endsWith(".xml")) {
					dtf.setType(0);
				} else {
					dtf.setType(9);
				}
				dtf.setIsLeaf(0);
				dtf.setIsBlank(0);
				dtf.setRawPath(files[i].getPath());
				managerService.addDirTree(dtf);
			} else {
				String dirName = files[i].toString();
				File dir1 = new File(dirName);

				File[] files1 = dir1.listFiles();
				int fileNum = files1.length;
				DirTree dtd = new DirTree();
				dtd.setCreaterIp(ip);
				dtd.setCreateTime(time);
				dtd.setPid(parentId);
				dtd.setMd5Sign(sign);
				dtd.setDirName(dirName);
				dtd.setType(1);
				if (fileNum > 0) {
					dtd.setIsBlank(0);
					dtd.setIsLeaf(0);
				} else {
					dtd.setIsBlank(1);
					dtd.setIsLeaf(1);
				}
				dtd.setRawPath(dir1.getPath());
				managerService.addDirTree(dtd);
				generateDirTree(dir1, ip, time, sign, dtd.getId());
			}
		}
	}

	/**
	 * 考虑兼容性，复制parsexml方法代码，添加topic返回值
	 * @param bankId
	 * @param newPath
	 * @return topic
	 */
	@SuppressWarnings({ "unchecked" })
	private String parsexmlForDB(String bankId, String newPath) {
		XmlUtil xmlUtil = new XmlUtil(); // 调用xml工具栏
		xmlUtil.setCharset("utf-8"); // 解决解析后乱码
		BankTopic b = new BankTopic(); // 新建题目
		Calendar calendar = Calendar.getInstance();// 调用Instance
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSSS");// 格式刷时间
		String topicId = sdf.format(calendar.getTime());// 格式刷时间
		Element loadXmlNoFeature = xmlUtil.loadXmlNoFeature(newPath);// 获取元素
		Element intro = loadXmlNoFeature.getChild("intro");// 获取子元素
		Element info = intro.getChild("info");// 获取子元素
		Element option = intro.getChild("option");// 获取子元素
		Element totalScoreEle = intro.getChild("total_score");// 获取子元素
		Integer totalScore = 0;
		int i = 0;// 定义常量
		
		// 题目类型
		String topicTitle = info.getChildText("title");
		String topicType = "";
		String stepType = "";
		if(topicTitle.startsWith("T09")) {
			topicType = TopicTypeConstant.THEORY;
		}
		if(topicTitle.startsWith("T01") || topicTitle.startsWith("T02")
			|| topicTitle.startsWith("T03") || topicTitle.startsWith("T04")
			|| topicTitle.startsWith("T05") || topicTitle.startsWith("T06")) {
			topicType = TopicTypeConstant.PRACTICAL;
		}
		if(topicTitle.startsWith("T07") || topicTitle.startsWith("T08")
			|| topicTitle.startsWith("T10") || topicTitle.contains("听打录入")) {
			topicType = TopicTypeConstant.SKILL;
		}
		if(topicTitle.contains("实训营地")) {
			topicType = TopicTypeConstant.CAMPING;
		}

		Element procedure = loadXmlNoFeature.getChild("procedure");// 获取对应元素
		BankStep bs = null;// 定义常量

		List<Element> children = procedure.getChildren();// 获取所有子元素
		for (Element step : children) {// 循环
			String xmlType = step.getAttributeValue("type");
			if(xmlType != null &&( "begin".equalsIgnoreCase(xmlType)
					|| "开始".equals(xmlType) || "end".equalsIgnoreCase(xmlType)
					|| "结束".equals(xmlType) )){
				continue;
			}
			bs = new BankStep();// 设置属性
			bs.setVccId("444444");// 设置属性
			bs.setBankId(bankId);// 设置题库id
			bs.setTopicId(topicId);// 设置属性
			bs.setStepId(step.getAttributeValue("id"));// 设置属性

			// 步骤类型
			String stepEleType = step.getAttributeValue("type");
			if(StringUtils.isNotBlank(stepEleType)) {
				if(stepEleType.contains("repeat")) {
					stepType = TopicTypeConstant.REPEAT;
				}
				if(stepEleType.contains("state")) {
					stepType = TopicTypeConstant.STATE;
				}
				if(stepEleType.contains("voice")) {
					stepType = TopicTypeConstant.VOICE;
				}
			}
			Element answerEle = step.getChild("answer");
			Element promptEle = step.getChild("prompt");
			if(answerEle != null) {
				String answerType = answerEle.getAttributeValue("type");
				if(StringUtils.isNotBlank(answerType)) {
					if(answerType.contains("choice_single")) {
						stepType = TopicTypeConstant.SINGLE;
					}
					if(answerType.contains("choice_multi")) {
						stepType = TopicTypeConstant.MULTI;
					}
					if(answerType.contains("answer")) {
						stepType = TopicTypeConstant.ANSWER;
					}
					if(answerType.contains("judge")) {
						stepType = TopicTypeConstant.JUDGE;
					}
					if(answerType.contains("cloze")) {
						if(promptEle != null) {
							String promptAudio = promptEle.getAttributeValue("audio");
							if(StringUtils.isNotBlank(promptAudio)) {
								stepType = TopicTypeConstant.CLOZE;
							}else {
								stepType = TopicTypeConstant.CLOZED;
							}
						}
					}
				}

				if(topicTitle.startsWith("T07")) {
					stepType = TopicTypeConstant.ANALYZE;
				}
				if(topicTitle.startsWith("T08")) {
					stepType = TopicTypeConstant.VERBAL;
				}
				if(topicTitle.startsWith("T10")) {
					stepType = TopicTypeConstant.STEP_DRAW;
				}
				if(topicType.equals(TopicTypeConstant.CAMPING)) {
					stepType = TopicTypeConstant.STEP_CAMPING;
				}
			}
			// bs.setType(step.getAttributeValue("type"));// 设置属性
			bs.setType(stepType);
			bs.setTitle(step.getChildText("title"));// 设置属性
			if(step.getChildText("score") != null && !"".equals(step.getChildText("score"))){
				bs.setScore((step.getChildText("score")));// 设置分数
				if(step.getChildText("score").indexOf(",") > -1){
					String[] scoreArr = step.getChildText("score").split(",");
					for(String scoreStr : scoreArr ){
						if(scoreStr != null && !"".equals(scoreStr)){
							totalScore += Integer.valueOf(scoreStr);
						}
					}
				}else{
					totalScore += Integer.valueOf(step.getChildText("score"));
				}
			}else{
				bs.setScore("0");// 设置分数
			}
			
			
			/* doCloze(step); */
			XMLOutputter xmlout = new XMLOutputter(Format.getRawFormat());// 输出xml
			bs.setParam(xmlout.outputString(step));// 设置属性
			bs.setStandardAnswer(step.getChildText("standard_answer"));// 设置正确答案
			if (step.getChildText("title").equals("开始")// 判断
					|| step.getChildText("title").equals("结束")) {// 判断
				bs.setOrderNo(null);// 设置属性
			} else {
				i++;// i加一
				bs.setOrderNo(i);// 设置排序
			}
			managerService.addBankStep(bs);// 添加题库步骤

		}
		b.setVccId("444444");// 设置属性
		b.setBankId(bankId);// 设置属性
		b.setTopicId(topicId);// 设置属性
		if(totalScoreEle != null &&
				totalScoreEle.getText() != null && !"".equals(totalScoreEle.getText())){
			b.setTotalScore(Integer.valueOf(totalScoreEle.getText()));
		}else{
			b.setTotalScore(totalScore);
		}
		b.setTitle(info.getChildText("title"));// 设置属性
		// TODO 题目类型
		// String totype = doType(children, info.getChildText("type"));// 设置属性
		// b.setType(totype);// 设置属性
		b.setType(topicType);
		b.setLevel(info.getChildText("level"));// 设置属性
		b.setProfession(info.getChildText("profession"));// 设置属性
		b.setKeyWords(info.getChildText("key_words"));// 设置属性
		String dif = doDiffcult(info.getChildText("difficulty"));// 设置属性
		b.setDifficulty(dif);// 设置属性
		b.setCreateTime(info.getChildText("createTime"));// 设置属性
		b.setAuthor(info.getChildText("author"));// 设置属性
		b.setRemark(info.getChildText("remark"));// 设置属性
		b.setCapability(info.getChildText("capability"));// 设置属性
		b.setCustomer(info.getChildText("customerType"));// 设置属性
		b.setBookmodul(info.getChildText("bookmodul"));// 设置属性
		b.setMergeVoice(Integer.parseInt(option.getChildText("merge_voice")));// 设置属性
		b.setCustomer(intro.getChildText("customer"));// 设置属性
		b.setTalkSkill(intro.getChildText("talk_skill"));// 设置属性
		b.setBeginStepId(procedure.getChild("step").getAttributeValue("id"));// 设置属性
		managerService.addBankTopicByUpload(b);// 添加上传题库
		return topicId;
	}

	/**
	 * 解析xml并放入数据库
	 * 
	 * @param bankId
	 * @param newPath
	 */
	@SuppressWarnings({ "unchecked" })
	private void parsexml(String bankId, String newPath) {
		XmlUtil xmlUtil = new XmlUtil(); // 调用xml工具栏
		xmlUtil.setCharset("utf-8"); // 解决解析后乱码
		BankTopic b = new BankTopic(); // 新建题目
		Calendar calendar = Calendar.getInstance();// 调用Instance
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSSS");// 格式刷时间
		String topicId = sdf.format(calendar.getTime());// 格式刷时间
		Element loadXmlNoFeature = xmlUtil.loadXmlNoFeature(newPath);// 获取元素
		Element intro = loadXmlNoFeature.getChild("intro");// 获取子元素
		Element info = intro.getChild("info");// 获取子元素
		Element option = intro.getChild("option");// 获取子元素
		int i = 0;// 定义常量
		
		// 题目类型
		String topicTitle = info.getChildText("title");
		String topicType = "";
		String stepType = "";
		if(topicTitle.startsWith("T09")) {
			topicType = TopicTypeConstant.THEORY;
		}
		if(topicTitle.startsWith("T01") || topicTitle.startsWith("T02")
			|| topicTitle.startsWith("T03") || topicTitle.startsWith("T04")
			|| topicTitle.startsWith("T05") || topicTitle.startsWith("T06")) {
			topicType = TopicTypeConstant.PRACTICAL;
		}
		if(topicTitle.startsWith("T07") || topicTitle.startsWith("T08")
			|| topicTitle.startsWith("T10") || topicTitle.contains("听打录入")) {
			topicType = TopicTypeConstant.SKILL;
		}
		if(topicTitle.contains("实训营地")) {
			topicType = TopicTypeConstant.CAMPING;
		}

		Element procedure = loadXmlNoFeature.getChild("procedure");// 获取对应元素
		BankStep bs = null;// 定义常量
		List<Element> children = procedure.getChildren();// 获取所有子元素
		for (Element step : children) {// 循环
			String xmlType = step.getAttributeValue("type");
			if(xmlType != null &&( "begin".equalsIgnoreCase(xmlType)
					|| "开始".equals(xmlType) || "end".equalsIgnoreCase(xmlType)
					|| "结束".equals(xmlType) )){
				continue;
			}
			
			bs = new BankStep();// 设置属性
			bs.setVccId("444444");// 设置属性
			bs.setBankId(bankId);// 设置题库id
			bs.setTopicId(topicId);// 设置属性
			bs.setStepId(step.getAttributeValue("id"));// 设置属性

			// 步骤类型
			String stepEleType = step.getAttributeValue("type");
			if(StringUtils.isNotBlank(stepEleType)) {
				if(stepEleType.contains("repeat")) {
					stepType = TopicTypeConstant.REPEAT;
				}
				if(stepEleType.contains("state")) {
					stepType = TopicTypeConstant.STATE;
				}
				if(stepEleType.contains("voice")) {
					stepType = TopicTypeConstant.VOICE;
				}
			}
			Element answerEle = step.getChild("answer");
			Element promptEle = step.getChild("prompt");
			if(answerEle != null) {
				String answerType = answerEle.getAttributeValue("type");
				if(StringUtils.isNotBlank(answerType)) {
					if(answerType.contains("choice_single")) {
						stepType = TopicTypeConstant.SINGLE;
					}
					if(answerType.contains("choice_multi")) {
						stepType = TopicTypeConstant.MULTI;
					}
					if(answerType.contains("answer")) {
						stepType = TopicTypeConstant.ANSWER;
					}
					if(answerType.contains("judge")) {
						stepType = TopicTypeConstant.JUDGE;
					}
					if(answerType.contains("cloze")) {
						if(promptEle != null) {
							String promptAudio = promptEle.getAttributeValue("audio");
							if(StringUtils.isNotBlank(promptAudio)) {
								stepType = TopicTypeConstant.CLOZE;
							}else {
								stepType = TopicTypeConstant.CLOZED;
							}
						}
					}
				}

				if(topicTitle.startsWith("T07")) {
					stepType = TopicTypeConstant.ANALYZE;
				}
				if(topicTitle.startsWith("T08")) {
					stepType = TopicTypeConstant.VERBAL;
				}
				if(topicTitle.startsWith("T10")) {
					stepType = TopicTypeConstant.STEP_DRAW;
				}
				if(topicType.equals(TopicTypeConstant.CAMPING)) {
					stepType = TopicTypeConstant.STEP_CAMPING;
				}
			}

			// bs.setType(step.getAttributeValue("type"));// 设置属性
			bs.setType(stepType);
			bs.setTitle(step.getChildText("title"));// 设置属性
			bs.setScore(String.valueOf((step.getChildText("score"))));// 设置分数
			/* doCloze(step); */
			XMLOutputter xmlout = new XMLOutputter(Format.getRawFormat());// 输出xml
			bs.setParam(xmlout.outputString(step));// 设置属性
			bs.setStandardAnswer(step.getChildText("standard_answer"));// 设置正确答案
			if (step.getChildText("title").equals("开始")// 判断
					|| step.getChildText("title").equals("结束")) {// 判断
				bs.setOrderNo(null);// 设置属性
			} else {
				i++;// i加一
				bs.setOrderNo(i);// 设置排序
			}
			managerService.addBankStep(bs);// 添加题库步骤

		}
		b.setVccId("444444");// 设置属性
		b.setBankId(bankId);// 设置属性
		b.setTopicId(topicId);// 设置属性
		b.setTitle(info.getChildText("title"));// 设置属性
		// String totype = doType(children, info.getChildText("type"));// 设置属性
		// b.setType(totype);// 设置属性
		b.setType(topicType);
		b.setLevel(info.getChildText("level"));// 设置属性
		b.setProfession(info.getChildText("profession"));// 设置属性
		b.setKeyWords(info.getChildText("key_words"));// 设置属性
		String dif = doDiffcult(info.getChildText("difficulty"));// 设置属性
		b.setDifficulty(dif);// 设置属性
		b.setCreateTime(info.getChildText("createTime"));// 设置属性
		b.setAuthor(info.getChildText("author"));// 设置属性
		b.setRemark(info.getChildText("remark"));// 设置属性
		b.setCapability(info.getChildText("capability"));// 设置属性
		b.setCustomer(info.getChildText("customerType"));// 设置属性
		b.setBookmodul(info.getChildText("bookmodul"));// 设置属性
		b.setMergeVoice(Integer.parseInt(option.getChildText("merge_voice")));// 设置属性
		b.setCustomer(intro.getChildText("customer"));// 设置属性
		b.setTalkSkill(intro.getChildText("talk_skill"));// 设置属性
		b.setBeginStepId(procedure.getChild("step").getAttributeValue("id"));// 设置属性
		managerService.addBankTopicByUpload(b);// 添加上传题库
	}

	/**
	 * 解析题目Excel,并存入数据库
	 */
	private boolean parseExcel(final String bankId, final String path, 
		final String uploadPath, final String fileName) {
		try {
			List<BankTopic> topics = new ArrayList<BankTopic>();
			List<BankStep> steps = new ArrayList<BankStep>();
			List<ExcelError> errors = new ArrayList<ExcelError>();

			Bank bank = managerService.schBankById(bankId);
			String profession = bank.getProfession();

			File file = new File(path);
			Workbook wb = WorkbookFactory.create(file);

			int sheetNumber = wb.getNumberOfSheets();
			for(int i = 0; i < sheetNumber; i++) {
				Sheet sht = wb.getSheetAt(i);
				String sheetName = sht.getSheetName();
				String shtNameFormat = sheetName.trim();
				if(sheetName != null) {
					if(TopicTypeConstant.SINGLE_STR.equals(shtNameFormat)) {
						assembleSingleTopic(bankId, profession, sht, topics, steps, errors);
					}
					if(TopicTypeConstant.MULTI_STR.equals(shtNameFormat)) {
						assembleMultiTopic(bankId, profession, sht, topics, steps, errors);
					}
					if(TopicTypeConstant.JUDGE_STR.equals(shtNameFormat)) {
						assembleJudgeTopic(bankId, profession, sht, topics, steps, errors);
					}
					if(TopicTypeConstant.CLOZED_STR.equals(shtNameFormat)) {
						assembleClozedTopic(bankId, profession, sht, topics, steps, errors);
					}
					if(TopicTypeConstant.ANSWER_STR.equals(shtNameFormat)) {
						assembleAnswerTopic(bankId, profession, sht, topics, steps, errors);
					}
				}
			}
			if(errors.size() > 0) {
				// TODO Excel解析日志
				String[] title = {"页签", "序号", "行", "列", "单元格数据", "提示"};
				HSSFWorkbook hssfWorkbook = new HSSFWorkbook();
				HSSFSheet hssfSheet = hssfWorkbook.createSheet("错误日志");
				HSSFRow firstRow = hssfSheet.createRow(0);
				HSSFCell cell = null;
				for(int i = 0; i < title.length; i++) {
					cell = firstRow.createCell(i);
					cell.setCellValue(title[i]);
				}
				for(int j = 0; j < errors.size(); j++) {
					HSSFRow nrow = hssfSheet.createRow(j + 1);
					HSSFCell nCell = nrow.createCell(0);
					nCell.setCellValue(errors.get(j).getSheetName());
					nCell = nrow.createCell(1);
					nCell.setCellValue(errors.get(j).getSerial());
					nCell = nrow.createCell(2);
					nCell.setCellValue(errors.get(j).getRow());
					nCell = nrow.createCell(3);
					nCell.setCellValue(errors.get(j).getColumn());
					nCell = nrow.createCell(4);
					nCell.setCellValue(errors.get(j).getCellData());
					nCell = nrow.createCell(5);
					nCell.setCellValue(errors.get(j).getMsg());
				}
				String fileNamePrefix = fileName.substring(0, fileName.lastIndexOf("."));
				String logName = "ERROR-" + fileNamePrefix + ".xls";
				File logFile = new File(uploadPath, logName);
				try {
					file.createNewFile();
					FileOutputStream stream = FileUtils.openOutputStream(logFile);
					hssfWorkbook.write(stream);
					stream.close();
				}catch (IOException e) {
					e.printStackTrace();
				}
				return false;
			}
			managerService.addBankTopicBatch(topics);
			managerService.addBankStepBatch(steps);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	/**
	 * 组装单选题
	 * @param bankId
	 * @param sheet
	 * @param topics
	 * @param steps
	 */
	private void assembleSingleTopic(final String bankId, final String profession, final Sheet sheet, 
		final List<BankTopic> topics, final List<BankStep> steps, final List<ExcelError> errors) {
		DecimalFormat df = new DecimalFormat("#");// 定义对象
		for(Row row : sheet) {
			int rowNumber = row.getRowNum();
			if(rowNumber < 3) {
				continue;
			}
			short colNumber = row.getLastCellNum();
			if(colNumber >= 0) {
				Cell indexCell = row.getCell(0);
				if(indexCell == null) {
					break;
				}
				int indexCellType = indexCell.getCellType();
				if(indexCellType != HSSFCell.CELL_TYPE_NUMERIC) {
					break;
				}
				String indexCellVal;	
				indexCellVal = df.format(indexCell.getNumericCellValue())
					.toString();
				if(indexCellVal == null || "".equals(indexCellVal)) {
					break;
				}
				try {
					Integer.parseInt(indexCellVal);
				}catch (Exception e) {
					e.printStackTrace();
					int rowNumber_ = rowNumber + 1;	

					ExcelError error = new ExcelError();
					error.setSheetName(sheet.getSheetName());
					error.setSerial(indexCellVal);
					error.setRow(rowNumber_ + "");
					error.setColumn("1");
					error.setCellData(indexCellVal);
					error.setMsg("序号需为数值格式的数据");
					errors.add(error);
					continue;
				}

			} 

			BankTopic singleTopic = new BankTopic();
			singleTopic.setBankId(bankId);
			singleTopic.setProfession(profession);
			Calendar calendar = Calendar.getInstance();// 调用Instance
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSSS");// 格式刷时间
			// Random random = new Random();
			// int randomNum = random.nextInt(999999);
			String dateFormat =  sdf.format(calendar.getTime());
			String topicId = dateFormat + rowNumber;

			singleTopic.setTopicId(topicId);
			singleTopic.setType(TopicTypeConstant.THEORY);

			BankStep singleStep = new BankStep();
			singleStep.setBankId(bankId);
			singleStep.setTopicId(topicId);
			singleStep.setType(TopicTypeConstant.SINGLE);

			StringBuilder stepXml = new StringBuilder();				
			stepXml.append("<step type=\"" + ExerConstant.TOPIC_STEP_TYPE_QUESTION + "\">");
			stepXml.append("<title>");

			int serialNumber = -1;

			// String bookName = "";
			// String sectionName = "";
			// String taskName = "";
			List<Book> books = null;
			List<Book> sections = null;
			List<Book> tasks = null;

			for(int i = 0; i < colNumber; i++) {
				Cell cell = row.getCell(i);
				String cellVal = "";
			
				if(cell != null && cell.getCellType() != cell.CELL_TYPE_BLANK) {
					int cellType = cell.getCellType();
					switch (cellType) {
						case HSSFCell.CELL_TYPE_STRING:
							cellVal = cell.getRichStringCellValue()
								.getString().trim();
							break;
						case HSSFCell.CELL_TYPE_NUMERIC:
							cellVal = df.format(cell.getNumericCellValue())
								.toString();
							break;
						default:
							cellVal = "";
					}
				}
				if(i == 0) {
					try {
						serialNumber = Integer.parseInt(cellVal);
					}catch (Exception e) {
						e.printStackTrace();
						int rowNumber_ = rowNumber + 1;
						ExcelError error = new ExcelError();
						error.setSheetName(sheet.getSheetName());
						error.setSerial(cellVal);
						error.setRow(rowNumber_ + "");
						int col = i + 1;
						error.setColumn(col + "");
						error.setCellData(cellVal);
						error.setMsg("序号需为数值格式的数据");
					}
					continue;
				}
				
				if(i == 1) {
					singleTopic.setTitle(cellVal);
					singleStep.setTitle(cellVal + "步骤1");
					stepXml.append(cellVal + "步骤1");
					stepXml.append("</title>");
					continue;
				}
				if(i == 2) {
					stepXml.append("<prompt audio=\"\" repeat=\"\">");
					stepXml.append(cellVal);
					stepXml.append("</prompt>");
					continue;
				}
				if(i == 3) {
					if(TopicTypeConstant.PRIMARY_STR.equals(cellVal)) {
						singleTopic.setDifficulty(TopicTypeConstant.PRIMARY);
					}else if(TopicTypeConstant.MIDDLE_STR.equals(cellVal)) {
						singleTopic.setDifficulty(TopicTypeConstant.MIDDLE);
					}else if(TopicTypeConstant.HIGH_STR.equals(cellVal)) {
						singleTopic.setDifficulty(TopicTypeConstant.HIGH);
					}else {
						singleTopic.setDifficulty(TopicTypeConstant.PRIMARY);
					}
					continue;
				}
				// 章节
				if(i == 4) {
					// bookName = cellVal;
					// Book schBook = new Book();
					if(StringUtils.isBlank(cellVal)) {
						continue;
					}
					String[] bookNameAndId = cellVal.split("\\^");
					if(bookNameAndId == null || bookNameAndId.length != 2) {
						continue;
					}
					// String bookName = bookNameAndId[0];
					String bookId = bookNameAndId[1];
					try{
						singleTopic.setBookId(Integer.parseInt(bookId));
					}catch (Exception e) {
						e.printStackTrace();
						continue;
					}
				}
				if(i == 5) {
					singleStep.setStandardAnswer(cellVal);
					stepXml.append("<standard_answer>");
					stepXml.append(cellVal);
					stepXml.append("</standard_answer>");
					continue;
				}
				if(i == 6) {
					int totalScore = 0;
					try{
						totalScore = Integer.parseInt(cellVal);
					}catch (Exception e) {
						e.printStackTrace();
						int rowNumber_ = rowNumber + 1;
						ExcelError error = new ExcelError();
						error.setSheetName(sheet.getSheetName());
						error.setSerial(serialNumber + "");
						error.setRow(rowNumber_ + "");
						int col = i + 1;
						error.setColumn(col + "");
						error.setCellData(cellVal);
						error.setMsg("分值需为数值格式");
						errors.add(error);
						continue;
					}
					singleTopic.setTotalScore(totalScore);
					singleStep.setScore(cellVal);
					stepXml.append("<score>");
					stepXml.append(cellVal);
					stepXml.append("</score>");
					continue;
				}
				if(i > 6) {
					if(i == 7) {
						stepXml.append("<answer type=\"" + ExerConstant.TOPIC_STEP_TYPE_QUESTION_CHOICE_SINGLE + "\">");
					}
					int optionIndex = i - 7;
					stepXml.append("<choice id=\"" + TopicTypeConstant.OPTIONS[optionIndex] +"\" audio=\"\">");
					stepXml.append(cellVal);
					stepXml.append("</choice>");
					if(i == colNumber - 1) {
						stepXml.append("</answer>");
					}
				}
			}
			stepXml.append("</step>");
			singleStep.setParam(stepXml.toString());
			topics.add(singleTopic);
			steps.add(singleStep);
		}
	}

	private void assembleMultiTopic(final String bankId, final String profession, final Sheet sheet, 
		final List<BankTopic> topics, final List<BankStep> steps, final List<ExcelError> errors) {
		DecimalFormat df = new DecimalFormat("#");// 定义对象
		for(Row row : sheet) {
			int rowNumber = row.getRowNum();
			if(rowNumber < 3) {
				continue;
			}
			short colNumber = row.getLastCellNum();
			if(colNumber >= 0) {
				Cell indexCell = row.getCell(0);
				if(indexCell == null) {
					break;
				}
				int indexCellType = indexCell.getCellType();
				if(indexCellType != HSSFCell.CELL_TYPE_NUMERIC) {
					break;
				}
				String indexCellVal;	
				indexCellVal = df.format(indexCell.getNumericCellValue())
					.toString();
				if(indexCellVal == null) {
					break;
				}
				try {
					Integer.parseInt(indexCellVal);
				}catch (Exception e) {
					e.printStackTrace();
					int rowNumber_ = rowNumber + 1;	

					ExcelError error = new ExcelError();
					error.setSheetName(sheet.getSheetName());
					error.setSerial(indexCellVal);
					error.setRow(rowNumber_ + "");
					error.setColumn("1");
					error.setCellData(indexCellVal);
					error.setMsg("序号需为数值格式的数据");
					errors.add(error);
					continue;
				}

			} 

			BankTopic multiTopic = new BankTopic();
			multiTopic.setBankId(bankId);
			multiTopic.setProfession(profession);
			Calendar calendar = Calendar.getInstance();// 调用Instance
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSSS");// 格式刷时间
			// Random random = new Random();
			// int randomNum = random.nextInt(999999);
			String dateFormat =  sdf.format(calendar.getTime());
			String topicId = dateFormat + rowNumber;

			multiTopic.setTopicId(topicId);
			multiTopic.setType(TopicTypeConstant.THEORY);

			BankStep multiStep = new BankStep();
			multiStep.setBankId(bankId);
			multiStep.setTopicId(topicId);
			multiStep.setType(TopicTypeConstant.MULTI);

			StringBuilder stepXml = new StringBuilder();				
			stepXml.append("<step type=\"" + ExerConstant.TOPIC_STEP_TYPE_QUESTION + "\">");
			stepXml.append("<title>");

			int serialNumber = -1;
			for(int i = 0; i < colNumber; i++) {
				Cell cell = row.getCell(i);
				String cellVal = "";
			
				if(cell != null && cell.getCellType() != cell.CELL_TYPE_BLANK) {
					int cellType = cell.getCellType();
					switch (cellType) {
						case HSSFCell.CELL_TYPE_STRING:
							cellVal = cell.getRichStringCellValue()
								.getString().trim();
							break;
						case HSSFCell.CELL_TYPE_NUMERIC:
							cellVal = df.format(cell.getNumericCellValue())
								.toString();
							break;
						default:
							cellVal = "";
					}
				}
				if(i == 0) {
					try {
						serialNumber = Integer.parseInt(cellVal);
					}catch (Exception e) {
						int rowNumber_ = rowNumber + 1;
						ExcelError error = new ExcelError();
						error.setSheetName(sheet.getSheetName());
						error.setSerial(cellVal);
						error.setRow(rowNumber_ + "");
						int col = i + 1;
						error.setColumn(col + "");
						error.setCellData(cellVal);
						error.setMsg("序号需为数值格式的数据");
						errors.add(error);
					}
					continue;
				}
				
				if(i == 1) {
					multiTopic.setTitle(cellVal);
					multiStep.setTitle(cellVal + "步骤1");
					stepXml.append(cellVal + "步骤1");
					stepXml.append("</title>");
					continue;
				}
				if(i == 2) {
					stepXml.append("<prompt audio=\"\" repeat=\"\">");
					stepXml.append(cellVal);
					stepXml.append("</prompt>");
					continue;
				}
				if(i == 3) {
					if(TopicTypeConstant.PRIMARY_STR.equals(cellVal)) {
						multiTopic.setDifficulty(TopicTypeConstant.PRIMARY);
					}else if(TopicTypeConstant.MIDDLE_STR.equals(cellVal)) {
						multiTopic.setDifficulty(TopicTypeConstant.MIDDLE);
					}else if(TopicTypeConstant.HIGH_STR.equals(cellVal)) {
						multiTopic.setDifficulty(TopicTypeConstant.HIGH);
					}else {
						multiTopic.setDifficulty(TopicTypeConstant.PRIMARY);
					}
					continue;
				}
				if(i == 4) {
					if(StringUtils.isBlank(cellVal)) {
						continue;
					}
					String[] bookNameAndId = cellVal.split("\\^");
					if(bookNameAndId == null || bookNameAndId.length != 2) {
						continue;
					}
					// String bookName = bookNameAndId[0];
					String bookId = bookNameAndId[1];
					try{
						multiTopic.setBookId(Integer.parseInt(bookId));
					}catch (Exception e) {
						e.printStackTrace();
						continue;
					}

				}

				if(i == 5) {
					multiStep.setStandardAnswer(cellVal);
					stepXml.append("<standard_answer>");
					stepXml.append(cellVal);
					stepXml.append("</standard_answer>");
					continue;
				}
				if(i == 6) {
					int totalScore = 0;
					try {
						totalScore = Integer.parseInt(cellVal);
					}catch (Exception e) {
						e.printStackTrace();
						int rowNumber_ = rowNumber + 1;
						ExcelError error = new ExcelError();
						error.setSheetName(sheet.getSheetName());
						error.setSerial(serialNumber + "");
						error.setRow(rowNumber_ + "");
						int col = i + 1;
						error.setColumn(col + "");
						error.setCellData(cellVal);
						error.setMsg("分值需为数值格式");
						errors.add(error);
						continue;
					}
					multiTopic.setTotalScore(totalScore);
					multiStep.setScore(cellVal);
					stepXml.append("<score>");
					stepXml.append(cellVal);
					stepXml.append("</score>");
					continue;
				}
				if(i > 6) {
					if(i == 7) {
						stepXml.append("<answer type=\"" + ExerConstant.TOPIC_STEP_TYPE_QUESTION_CHOICE_MULTI + "\">");
					}
					int optionIndex = i - 7;
					stepXml.append("<choice id=\"" + TopicTypeConstant.OPTIONS[optionIndex] +"\" audio=\"\">");
					stepXml.append(cellVal);
					stepXml.append("</choice>");
					if(i == colNumber - 1) {
						stepXml.append("</answer>");
					}
				}
			}
			stepXml.append("</step>");
			multiStep.setParam(stepXml.toString());
			topics.add(multiTopic);
			steps.add(multiStep);
		}
	}

	private void assembleJudgeTopic(final String bankId, final String profession, final Sheet sheet,
		final List<BankTopic> topics, final List<BankStep> steps, final List<ExcelError> errors) {
		DecimalFormat df = new DecimalFormat("#");// 定义对象
		for(Row row : sheet) {
			int rowNumber = row.getRowNum();
			if(rowNumber < 3) {
				continue;
			}
			short colNumber = row.getLastCellNum();
			if(colNumber >= 0) {
				Cell indexCell = row.getCell(0);
				if(indexCell == null) {
					break;
				}
				int indexCellType = indexCell.getCellType();
				if(indexCellType != HSSFCell.CELL_TYPE_NUMERIC) {
					break;
				}
				String indexCellVal;	
				indexCellVal = df.format(indexCell.getNumericCellValue())
					.toString();
				if(indexCellVal == null) {
					break;
				}
				try {
					Integer.parseInt(indexCellVal);
				}catch (Exception e) {
					e.printStackTrace();
					int rowNumber_ = rowNumber + 1;	

					ExcelError error = new ExcelError();
					error.setSheetName(sheet.getSheetName());
					error.setSerial(indexCellVal);
					error.setRow(rowNumber_ + "");
					error.setColumn("1");
					error.setCellData(indexCellVal);
					error.setMsg("序号需为数值格式的数据");
					errors.add(error);
					continue;
				}
			} 

			BankTopic judgeTopic = new BankTopic();
			judgeTopic.setBankId(bankId);
			judgeTopic.setProfession(profession);
			Calendar calendar = Calendar.getInstance();// 调用Instance
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSSS");// 格式刷时间
			// Random random = new Random();
			// int randomNum = random.nextInt(999999);
			String dateFormat =  sdf.format(calendar.getTime());
			String topicId = dateFormat + rowNumber;

			judgeTopic.setTopicId(topicId);
			judgeTopic.setType(TopicTypeConstant.THEORY);

			BankStep judgeStep = new BankStep();
			judgeStep.setBankId(bankId);
			judgeStep.setTopicId(topicId);
			judgeStep.setType(TopicTypeConstant.JUDGE);

			StringBuilder stepXml = new StringBuilder();				
			stepXml.append("<step type=\"" + ExerConstant.TOPIC_STEP_TYPE_QUESTION + "\">");
			stepXml.append("<answer type=\"" + ExerConstant.TOPIC_STEP_TYPE_QUESTION_JUDGE + "\" />");
			stepXml.append("<title>");
			int serialNumber = -1;
			for(int i = 0; i < colNumber; i++) {
				Cell cell = row.getCell(i);
				String cellVal = "";
			
				if(cell != null && cell.getCellType() != cell.CELL_TYPE_BLANK) {
					int cellType = cell.getCellType();
					switch (cellType) {
						case HSSFCell.CELL_TYPE_STRING:
							cellVal = cell.getRichStringCellValue()
								.getString().trim();
							break;
						case HSSFCell.CELL_TYPE_NUMERIC:
							cellVal = df.format(cell.getNumericCellValue())
								.toString();
							break;
						default:
							cellVal = "";
					}
				}
				if(i == 0) {
					try {
						serialNumber = Integer.parseInt(cellVal);
					}catch (Exception e) {
						int rowNumber_ = rowNumber + 1;
						ExcelError error = new ExcelError();
						error.setSheetName(sheet.getSheetName());
						error.setSerial(cellVal);
						error.setRow(rowNumber_ + "");
						int col = i + 1;
						error.setColumn(col + "");
						error.setCellData(cellVal);
						error.setMsg("序号需为数值格式的数据");
						errors.add(error);
					}
					continue;
				}
				
				if(i == 1) {
					judgeTopic.setTitle(cellVal);
					judgeStep.setTitle(cellVal + "步骤1");
					stepXml.append(cellVal + "步骤1");
					stepXml.append("</title>");
					continue;
				}
				if(i == 2) {
					stepXml.append("<prompt audio=\"\" repeat=\"\">");
					stepXml.append(cellVal);
					stepXml.append("</prompt>");
					continue;
				}
				if(i == 3) {
					if(TopicTypeConstant.PRIMARY_STR.equals(cellVal)) {
						judgeTopic.setDifficulty(TopicTypeConstant.PRIMARY);
					}else if(TopicTypeConstant.MIDDLE_STR.equals(cellVal)) {
						judgeTopic.setDifficulty(TopicTypeConstant.MIDDLE);
					}else if(TopicTypeConstant.HIGH_STR.equals(cellVal)) {
						judgeTopic.setDifficulty(TopicTypeConstant.HIGH);
					}else {
						judgeTopic.setDifficulty(TopicTypeConstant.PRIMARY);
					}
					continue;
				}

				if(i == 4) {
					if(StringUtils.isBlank(cellVal)) {
						continue;
					}
					String[] bookNameAndId = cellVal.split("\\^");
					if(bookNameAndId == null || bookNameAndId.length != 2) {
						continue;
					}
					// String bookName = bookNameAndId[0];
					String bookId = bookNameAndId[1];
					try{
						judgeTopic.setBookId(Integer.parseInt(bookId));
					}catch (Exception e) {
						e.printStackTrace();
						continue;
					}

				}
				if(i == 5) {
					Boolean standardAnswer;
					if(TopicTypeConstant.JUDGE_TRUE.equals(cellVal)) {
						standardAnswer = true;	
					}else if(TopicTypeConstant.JUDGE_FALSE.equals(cellVal)) {
						standardAnswer = false;
					}else {
						standardAnswer = false;
					}
					judgeStep.setStandardAnswer(standardAnswer.toString());
					stepXml.append("<standard_answer>");
					stepXml.append(standardAnswer.toString());
					stepXml.append("</standard_answer>");
					continue;
				}
				if(i == 6) {
					int totalScore = 0;
					try {
						totalScore = Integer.parseInt(cellVal);
					}catch (Exception e) {
						e.printStackTrace();
						int rowNumber_ = rowNumber + 1;
						ExcelError error = new ExcelError();
						error.setSheetName(sheet.getSheetName());
						error.setSerial(serialNumber + "");
						error.setRow(rowNumber_ + "");
						int col = i + 1;
						error.setColumn(col + "");
						error.setCellData(cellVal);
						error.setMsg("分值需为数值格式");
						errors.add(error);
						continue;
					}
					judgeTopic.setTotalScore(totalScore);
					judgeStep.setScore(cellVal);
					stepXml.append("<score>");
					stepXml.append(cellVal);
					stepXml.append("</score>");
					continue;
				}
			}
			stepXml.append("</step>");
			judgeStep.setParam(stepXml.toString());
			topics.add(judgeTopic);
			steps.add(judgeStep);
		}
	}


	private void assembleClozedTopic(final String bankId, final String profession, final Sheet sheet, 
		final List<BankTopic> topics, final List<BankStep> steps, final List<ExcelError> errors) {
		DecimalFormat df = new DecimalFormat("#");// 定义对象
		for(Row row : sheet) {
			int rowNumber = row.getRowNum();
			if(rowNumber < 3) {
				continue;
			}
			short colNumber = row.getLastCellNum();
			if(colNumber >= 0) {
				Cell indexCell = row.getCell(0);
				if(indexCell == null) {
					break;
				}
				int indexCellType = indexCell.getCellType();
				if(indexCellType != HSSFCell.CELL_TYPE_NUMERIC) {
					break;
				}
				String indexCellVal;	
				indexCellVal = df.format(indexCell.getNumericCellValue())
					.toString();
				if(indexCellVal == null) {
					break;
				}
				try {
					Integer.parseInt(indexCellVal);
				}catch (Exception e) {
					e.printStackTrace();
					int rowNumber_ = rowNumber + 1;	

					ExcelError error = new ExcelError();
					error.setSheetName(sheet.getSheetName());
					error.setSerial(indexCellVal);
					error.setRow(rowNumber_ + "");
					error.setColumn("1");
					error.setCellData(indexCellVal);
					error.setMsg("序号需为数值格式的数据");
					errors.add(error);
					continue;
				}

			} 

			BankTopic clozedTopic = new BankTopic();
			clozedTopic.setBankId(bankId);
			clozedTopic.setProfession(profession);
			Calendar calendar = Calendar.getInstance();// 调用Instance
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSSS");// 格式刷时间
			// Random random = new Random();
			// int randomNum = random.nextInt(999999);
			String dateFormat =  sdf.format(calendar.getTime());
			String topicId = dateFormat + rowNumber;

			clozedTopic.setTopicId(topicId);
			clozedTopic.setType(TopicTypeConstant.THEORY);

			BankStep clozedStep = new BankStep();
			clozedStep.setBankId(bankId);
			clozedStep.setTopicId(topicId);
			clozedStep.setType(TopicTypeConstant.CLOZED);

			StringBuilder stepXml = new StringBuilder();				
			stepXml.append("<step type=\"" + ExerConstant.TOPIC_STEP_TYPE_QUESTION + "\">");
			stepXml.append("<answer type=\"" + ExerConstant.TOPIC_STEP_TYPE_QUESTION_CLOZE + "\" />");
			stepXml.append("<title>");
			int serialNumber = -1;
			for(int i = 0; i < colNumber; i++) {
				Cell cell = row.getCell(i);
				String cellVal = "";
			
				if(cell != null && cell.getCellType() != cell.CELL_TYPE_BLANK) {
					int cellType = cell.getCellType();
					switch (cellType) {
						case HSSFCell.CELL_TYPE_STRING:
							cellVal = cell.getRichStringCellValue()
								.getString().trim();
							break;
						case HSSFCell.CELL_TYPE_NUMERIC:
							cellVal = df.format(cell.getNumericCellValue())
								.toString();
							break;
						default:
							cellVal = "";
					}
				}
				if(i == 0) {
					try {
						serialNumber = Integer.parseInt(cellVal);
					}catch (Exception e) {
						int rowNumber_ = rowNumber + 1;
						ExcelError error = new ExcelError();
						error.setSheetName(sheet.getSheetName());
						error.setSerial(cellVal);
						error.setRow(rowNumber_ + "");
						int col = i + 1;
						error.setColumn(col + "");
						error.setCellData(cellVal);
						error.setMsg("序号需为数值格式的数据");
						errors.add(error);
					}
					continue;
				}
				
				if(i == 1) {
					clozedTopic.setTitle(cellVal);
					clozedStep.setTitle(cellVal + "步骤1");
					stepXml.append(cellVal + "步骤1");
					stepXml.append("</title>");
					continue;
				}
				if(i == 2) {
					stepXml.append("<prompt audio=\"\" repeat=\"\">");
					stepXml.append(cellVal);
					stepXml.append("</prompt>");
					continue;
				}
				if(i == 3) {
					if(TopicTypeConstant.PRIMARY_STR.equals(cellVal)) {
						clozedTopic.setDifficulty(TopicTypeConstant.PRIMARY);
					}else if(TopicTypeConstant.MIDDLE_STR.equals(cellVal)) {
						clozedTopic.setDifficulty(TopicTypeConstant.MIDDLE);
					}else if(TopicTypeConstant.HIGH_STR.equals(cellVal)) {
						clozedTopic.setDifficulty(TopicTypeConstant.HIGH);
					}else {
						clozedTopic.setDifficulty(TopicTypeConstant.PRIMARY);
					}
					continue;
				}
				if(i == 4) {
					if(StringUtils.isBlank(cellVal)) {
						continue;
					}
					String[] bookNameAndId = cellVal.split("\\^");
					if(bookNameAndId == null || bookNameAndId.length != 2) {
						continue;
					}
					// String bookName = bookNameAndId[0];
					String bookId = bookNameAndId[1];
					try{
						clozedTopic.setBookId(Integer.parseInt(bookId));
					}catch (Exception e) {
						e.printStackTrace();
						continue;
					}

				}
				if(i == 5) {
					clozedStep.setStandardAnswer(cellVal);
					stepXml.append("<standard_answer>");
					stepXml.append(cellVal);
					stepXml.append("</standard_answer>");
					continue;
				}
				if(i == 6) {
					String[] scores = cellVal.split(",");
					int totalSocres = 0;
					if(scores != null && scores.length > 0) {
						for(String score : scores) {
							int scoreInt = 0;
							try {
								scoreInt = Integer.parseInt(score);
								totalSocres = totalSocres + scoreInt;
							}catch (Exception e) {
								e.printStackTrace();
								int rowNumber_ = rowNumber + 1;
								ExcelError error = new ExcelError();
								error.setSheetName(sheet.getSheetName());
								error.setSerial(serialNumber + "");
								error.setRow(rowNumber_ + "");
								int col = i + 1;
								error.setColumn(col + "");
								error.setCellData(cellVal);
								error.setMsg("分值需为数值格式");
								errors.add(error);
								break;
							}
						}
					}
					if(totalSocres == 0) {
						continue;
					}
					clozedTopic.setTotalScore(totalSocres);
					clozedStep.setScore(cellVal);
					stepXml.append("<score>");
					stepXml.append(cellVal);
					stepXml.append("</score>");
					continue;
				}
			}
			stepXml.append("</step>");
			clozedStep.setParam(stepXml.toString());
			topics.add(clozedTopic);
			steps.add(clozedStep);
		}

	}


	private void assembleAnswerTopic(final String bankId, final String profession, final Sheet sheet, 
		final List<BankTopic> topics, final List<BankStep> steps, final List<ExcelError> errors) {
		DecimalFormat df = new DecimalFormat("#");// 定义对象
		for(Row row : sheet) {
			int rowNumber = row.getRowNum();
			if(rowNumber < 3) {
				continue;
			}
			short colNumber = row.getLastCellNum();
			if(colNumber >= 0) {
				Cell indexCell = row.getCell(0);
				if(indexCell == null) {
					break;
				}
				int indexCellType = indexCell.getCellType();
				if(indexCellType != HSSFCell.CELL_TYPE_NUMERIC) {
					break;
				}
				String indexCellVal;	
				indexCellVal = df.format(indexCell.getNumericCellValue())
					.toString();
				if(indexCellVal == null) {
					break;
				}
				try {
					Integer.parseInt(indexCellVal);
				}catch (Exception e) {
					e.printStackTrace();
					int rowNumber_ = rowNumber + 1;	

					ExcelError error = new ExcelError();
					error.setSheetName(sheet.getSheetName());
					error.setSerial(indexCellVal);
					error.setRow(rowNumber_ + "");
					error.setColumn("1");
					error.setCellData(indexCellVal);
					error.setMsg("序号需为数值格式的数据");
					errors.add(error);
					continue;
				}

			} 

			BankTopic answerTopic = new BankTopic();
			answerTopic.setBankId(bankId);
			answerTopic.setProfession(profession);
			Calendar calendar = Calendar.getInstance();// 调用Instance
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSSS");// 格式刷时间
			// Random random = new Random();
			// int randomNum = random.nextInt(999999);
			String dateFormat =  sdf.format(calendar.getTime());
			String topicId = dateFormat + rowNumber;

			answerTopic.setTopicId(topicId);
			answerTopic.setType(TopicTypeConstant.THEORY);

			BankStep answerStep = new BankStep();
			answerStep.setBankId(bankId);
			answerStep.setTopicId(topicId);
			answerStep.setType(TopicTypeConstant.ANSWER);

			StringBuilder stepXml = new StringBuilder();				
			stepXml.append("<step type=\"" + ExerConstant.TOPIC_STEP_TYPE_QUESTION + "\">");
			stepXml.append("<answer type=\"" + ExerConstant.TOPIC_STEP_TYPE_QUESTION_ANSWER + "\" />");
			stepXml.append("<title>");
			int serialNumber = -1;
			for(int i = 0; i < colNumber; i++) {
				Cell cell = row.getCell(i);
				String cellVal = "";
			
				if(cell != null && cell.getCellType() != cell.CELL_TYPE_BLANK) {
					int cellType = cell.getCellType();
					switch (cellType) {
						case HSSFCell.CELL_TYPE_STRING:
							cellVal = cell.getRichStringCellValue()
								.getString().trim();
							break;
						case HSSFCell.CELL_TYPE_NUMERIC:
							cellVal = df.format(cell.getNumericCellValue())
								.toString();
							break;
						default:
							cellVal = "";
					}
				}
				if(i == 0) {
					try {
						serialNumber = Integer.parseInt(cellVal);
					}catch (Exception e) {
						int rowNumber_ = rowNumber + 1;
						ExcelError error = new ExcelError();
						error.setSheetName(sheet.getSheetName());
						error.setSerial(cellVal);
						error.setRow(rowNumber_ + "");
						int col = i + 1;
						error.setColumn(col + "");
						error.setCellData(cellVal);
						error.setMsg("序号需为数值格式的数据");
						errors.add(error);
					}
					continue;
				}
				
				if(i == 1) {
					answerTopic.setTitle(cellVal);
					answerStep.setTitle(cellVal + "步骤1");
					stepXml.append(cellVal + "步骤1");
					stepXml.append("</title>");
					continue;
				}
				if(i == 2) {
					stepXml.append("<prompt audio=\"\" repeat=\"\" origiName=\"\" fileName=\"\" >");
					stepXml.append(cellVal);
					stepXml.append("</prompt>");
					continue;
				}
				if(i == 3) {
					if(TopicTypeConstant.PRIMARY_STR.equals(cellVal)) {
						answerTopic.setDifficulty(TopicTypeConstant.PRIMARY);
					}else if(TopicTypeConstant.MIDDLE_STR.equals(cellVal)) {
						answerTopic.setDifficulty(TopicTypeConstant.MIDDLE);
					}else if(TopicTypeConstant.HIGH_STR.equals(cellVal)) {
						answerTopic.setDifficulty(TopicTypeConstant.HIGH);
					}else {
						answerTopic.setDifficulty(TopicTypeConstant.PRIMARY);
					}
					continue;
				}
				if(i == 4) {
					if(StringUtils.isBlank(cellVal)) {
						continue;
					}
					String[] bookNameAndId = cellVal.split("\\^");
					if(bookNameAndId == null || bookNameAndId.length != 2) {
						continue;
					}
					// String bookName = bookNameAndId[0];
					String bookId = bookNameAndId[1];
					try{
						answerTopic.setBookId(Integer.parseInt(bookId));
					}catch (Exception e) {
						e.printStackTrace();
						continue;
					}

				}
				if(i == 5) {
					answerStep.setStandardAnswer(cellVal);
					stepXml.append("<standard_answer>");
					stepXml.append(cellVal);
					stepXml.append("</standard_answer>");
					continue;
				}
				if(i == 6) {
					int totalScore = 0;
					try{
						totalScore = Integer.parseInt(cellVal);
					}catch (Exception e) {
						e.printStackTrace();
						int rowNumber_ = rowNumber + 1;
						ExcelError error = new ExcelError();
						error.setSheetName(sheet.getSheetName());
						error.setSerial(serialNumber + "");
						error.setRow(rowNumber_ + "");
						int col = i + 1;
						error.setColumn(col + "");
						error.setCellData(cellVal);
						error.setMsg("分值需为数值格式");
						errors.add(error);
						continue;
					}

					answerTopic.setTotalScore(totalScore);
					answerStep.setScore(cellVal);
					stepXml.append("<score>");
					stepXml.append(cellVal);
					stepXml.append("</score>");
					continue;
				}
			}
			stepXml.append("</step>");
			answerStep.setParam(stepXml.toString());
			topics.add(answerTopic);
			steps.add(answerStep);
		}
	}

	/**
	 * 下载Excel解析日志 
	 * @param logfile
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/download.action")
	public void downloadParseLog(String logfile, HttpServletRequest request,
			HttpServletResponse response) {
		String filename = logfile;
		String path = request.getSession().getServletContext().getRealPath("/data/doc"); // 获取服务器路径
		String filePrefix = filename.substring(0, filename.lastIndexOf("."));
		String downloadName = filePrefix + ".xls";
		String filePath = path + File.separator + filePrefix + ".xls";// 拼接名称
		try {
			File file = new File(filePath);// 文件对象
			BufferedInputStream fis = new BufferedInputStream(
					new FileInputStream(filePath));// 输入流
			byte[] buffer = new byte[fis.available()];// 定义字节
			fis.read(buffer);// 读
			fis.close();// 关闭
			// 清空response
			response.reset();
			// 设置response的Header
			response.addHeader("Content-Disposition", "attachment;filename="
					+ new String(downloadName.getBytes()));// 设置response的Header
			response.addHeader("Content-Length", "" + file.length());// 设置response的Header
			OutputStream toClient = new BufferedOutputStream(
					response.getOutputStream());// 输出流
			response.setContentType("application/vnd.ms-excel;charset=gb2312");// 设置属性
			toClient.write(buffer);// 写
			toClient.flush();// 刷新
			toClient.close();// 关闭
		} catch (Exception e) {
			e.printStackTrace();// 打印异常
		}
	}

	/**
	 * 导出Excel题目模板
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/exportTemplate.action")
	public void exportTemplate(HttpServletRequest request,
			HttpServletResponse response) {
		String filename = "BankTopicTemplate.xlsx";// 设置文件名
		String path = request.getSession().getServletContext().getRealPath("");// 设置路径
		String filePath = path + File.separator + "template" + File.separator
				+ filename;// 拼接名称
		try {
			File file = new File(filePath);// 文件对象
			BufferedInputStream fis = new BufferedInputStream(
					new FileInputStream(filePath));// 输入流
			byte[] buffer = new byte[fis.available()];// 定义字节
			fis.read(buffer);// 读
			fis.close();// 关闭
			// 清空response
			response.reset();
			// 设置response的Header
			response.addHeader("Content-Disposition", "attachment;filename="
					+ new String(filename.getBytes()));// 设置response的Header
			response.addHeader("Content-Length", "" + file.length());// 设置response的Header
			OutputStream toClient = new BufferedOutputStream(
					response.getOutputStream());// 输出流
			response.setContentType("application/vnd.ms-excel;charset=gb2312");// 设置属性
			toClient.write(buffer);// 写
			toClient.flush();// 刷新
			toClient.close();// 关闭
		} catch (Exception e) {
			e.printStackTrace();// 打印异常
		}
	}


	/**
	 * 判断type
	 * 
	 * @param b
	 * @param type
	 */
	private String doType(List<Element> children, String type) {
		String result = type;// 设置type
		if (children != null) {// 判断
			Map<String, String> stepTypeMap = new HashMap<String, String>();// 实例化对象
			Map<String, String> answerTypeMap = new HashMap<String, String>();// 实例化对象
			for (int i = 0; i < children.size(); i++) {// 循环
				if (children.get(i) != null) {// 判断
					Element element = children.get(i);// 获取元素
					String steType = element.getAttributeValue("type");// 获取步骤类型
					if (ExerConstant.STEP_BEGIN_ID.equalsIgnoreCase(steType)// 判断
							|| ExerConstant.STEP_END_ID.equalsIgnoreCase(steType)) {// 判断
						continue;// 忽略开始结束节点
					}
					if (steType != null && !"".equalsIgnoreCase(steType.trim())) {// 判断
						stepTypeMap.put(steType.trim(), steType);// 存储步骤类型
					}
					Element ele = element;// 定义常量
					Element answer = ele.getChild(ExerConstant.STEP_ANSWER);// 获取答案节点
					if (answer != null) {// 判断
						String antype = answer.getAttributeValue("type");// 获取答案类型
						if (antype != null// 判断
								&& !"".equalsIgnoreCase(antype.trim())) {// 判断
							answerTypeMap.put(antype.trim(), antype);// put值
						}
					}
				}
			}

			if (stepTypeMap.size() == 1 && answerTypeMap.size() == 1) {// 判断
				for (int i = 0; i < children.size(); i++) {// 循环
					if (children.get(i) != null) {// 判断
						String steType = children.get(i).getAttributeValue("type");// 获取属性
						if (ExerConstant.STEP_BEGIN_ID.equalsIgnoreCase(steType)// 判断
								|| ExerConstant.STEP_END_ID.equalsIgnoreCase(steType)) {// 判断
							continue;// 忽略开始结束节点
						}
						if (ExerConstant.TOPIC_STEP_TYPE_QUESTION.equals(steType)) {// 判断
							Element ele = children.get(i);// 获取元素
							Element answer = ele// 获取步骤答案
									.getChild(ExerConstant.STEP_ANSWER);// 获取步骤答案
							if (answer != null) {// 判断
								String antype = answer// 获取属性
										.getAttributeValue("type");// 获取属性

								if (antype.equalsIgnoreCase(ExerConstant.TOPIC_STEP_TYPE_QUESTION_CHOICE_SINGLE)) {// 判断
									result = TopicTypeConstant.SINGLE;// 单选
									break;// 停止循环
								}
								if (antype.equalsIgnoreCase(ExerConstant.TOPIC_STEP_TYPE_QUESTION_CHOICE_MULTI)) {// 判断
									result = TopicTypeConstant.MULTI;// 多选
									break;// 停止循环
								}
								if (antype.equalsIgnoreCase(ExerConstant.TOPIC_STEP_TYPE_QUESTION_ANSWER)) {// 判断
									result = TopicTypeConstant.ANSWER;// 问答题
									break;// 停止循环
								}
								if (antype.equalsIgnoreCase(ExerConstant.TOPIC_STEP_TYPE_QUESTION_CLOZE)) {// 判断
									result = TopicTypeConstant.CLOZE;// 听打录入题
									Element pro = children.get(i).getChild("prompt");// 获取子元素
									if (pro != null) {// 填空题
										String aud = pro.getAttributeValue("audio");// 获取参数
										if (aud == null// 判断
												|| "".equals(aud.trim())) {// 判断
											result = TopicTypeConstant.CLOZED;// 设置参数
										}
									}
									break;// 停止循环
								}
								if (antype.equalsIgnoreCase(ExerConstant.TOPIC_STEP_TYPE_QUESTION_JUDGE)) {// 判断
									result = TopicTypeConstant.JUDGE;// 判断
									break;// 停止循环
								}
							}
						} else {
							if (steType.equalsIgnoreCase(ExerConstant.TOPIC_STEP_TYPE_REPEAT)) {// 判断
								result = TopicTypeConstant.REPEAT;// 复读陈述题
								break;// 停止循环
							}
							if (steType.equalsIgnoreCase(ExerConstant.TOPIC_STEP_TYPE_STATE)) {// 判断
								result = TopicTypeConstant.STATE;// 应答题
								break;// 停止循环
							}
							if (steType.equalsIgnoreCase(ExerConstant.TOPIC_STEP_TYPE_VOICE)) {// 判断
								result = TopicTypeConstant.VOICE;// 语音提示题
								break;// 停止循环
							}
						}
					}
				}
			} else {
				result = TopicTypeConstant.ZHTX;// 综合题型
			}
		}
		return result;// 返回结果
	}

	/**
	 * 转换难度
	 * 
	 * @param difficulty
	 * @return
	 */
	private String doDiffcult(String difficulty) {
		String result = difficulty;// 设置参数
		if (difficulty != null) {// 判断
			if ("初级".equals(difficulty)) {// 判断
				result = TopicDifficultyConstant.PRIMARY;// 设置属性
			} else if ("中级".equals(difficulty)) {// 判断
				result = TopicDifficultyConstant.INTERMEDIATE;// 设置属性
			} else if ("高级".equals(difficulty)) {// 判断
				result = TopicDifficultyConstant.SENIOR;// 设置属性
			}
		}
		return result;// 返回数据
	}
}
