package com.qtrmoon.manager.ctrl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.qtrmoon.book.util.FilePath;
import com.qtrmoon.common.BaseController;
import com.qtrmoon.manager.pojo.Course;
import com.qtrmoon.manager.serdao.IManagerService;
import com.qtrmoon.util.FileUpload;

/**
 * 课程类控制层
 */
@Controller
@RequestMapping("/manager/course")
public class CourseController extends BaseController {
	@Autowired
	private IManagerService managerService; // 声明模块Service实例

	/**
	 * 查询方法，自带分页设置。
	 * @param course
	 * 承载查询条件的Bean
	 * @param request
	 * 可用来获取非Bean内的参数
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schCourse.action")
	public void schCourse(Course course, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		noCache(response);// Ajax方法设定无缓存
		course.setCondition(""); // 防sql注入
		course.assLike("shortCut", "courTitle", "courSynopsis", "attachPath",// 拼接%
				"uploadPeople", "changePeople");// 拼接%
		String bookId = course.getBookId();// 根据对象获取传来的bookid---》cc_book里面的pid
		List<String> bookids = managerService.getId(bookId);// 查询
		String param = "";// 定义参数
		for (String bookid : bookids) {// 循环
			if (bookid != null && !"".equals(bookid)) {// 判断
				if ("".equals(param)) {// 判断
					param = "'" + bookid + "'";// 获取bookid字符串
				} else {
					param = param + ",'" + bookid + "'";// 获取bookid字符串
				}
			}
		}
		if (param != null && !"".equals(param)) {// 判断
			course.setCondition("AND book_id in (" + param + ")");// 获取bookid字符串
			course.setBookId("");// 获取bookid字符串
		}
		bookids.clear();//清空
		List<Course> list = managerService.schCourse(course); // 调用Service查询数据
		/* 将数据Bean的集合转换为Json集合 */
		JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
		JSONObject obj; // 声明用于构造Bean的Json对象
		for (Course u : list) { // 循环查询的数据集构造Json数据集
			obj = u.getJsonInDict(); // 将Bean转换为Json对象
			obj.put("_oper", u.getId()); // 为"操作列"设定主键值
			obj.put("videoSourceIP", FilePath.getFilePath("videoSourceIP"));
			datas.add(obj); // 添加Json对象到Json集合
		}
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		JSONObject res = new JSONObject(); // 声明页面返回Json
		res.put("total", course.getDatasize()); // 设置数据总记录数
		res.put("rows", datas); // 设置数据集
		response.getWriter().print(res.toJSONString()); // 输出到页面
	}

	/**
	 * 查询方法，自带分页设置。
	 * @param course
	 * 承载查询条件的Bean
	 * @param request
	 * 可用来获取非Bean内的参数
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schCourseByBookId.action")
	public void schCourseByBookId(Course course, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		noCache(response);// Ajax方法设定无缓存
		course.setCondition(" and book_id = " + course.getBookId()// 防sql注入
				+ " and type=" + course.getType()); // 防sql注入
		course.assLike("shortCut", "courTitle", "courSynopsis", "attachPath",// 拼接%
				"uploadPeople", "changePeople");// 拼接%

		List<Course> list = managerService.schCourse(course); // 调用Service查询数据
		/* 将数据Bean的集合转换为Json集合 */
		JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
		JSONObject obj; // 声明用于构造Bean的Json对象
		for (Course u : list) { // 循环查询的数据集构造Json数据集
			String fileName = u.getAttachPath();
			if(fileName != null && !"".equals(fileName) ){
				String path = FilePath.getFilePath("courseware")+fileName;
				File file = new File(path);
				if(!file.exists()){
					u.setAttachPath("");
				}
			}
			
			obj = u.getJsonInDict(); // 将Bean转换为Json对象
			obj.put("_oper", u.getId()); // 为"操作列"设定主键值
			obj.put("videoSourceIP", FilePath.getFilePath("videoSourceIP"));
			datas.add(obj); // 添加Json对象到Json集合
		}
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		JSONObject res = new JSONObject(); // 声明页面返回Json
		res.put("total", course.getDatasize()); // 设置数据总记录数
		res.put("rows", datas); // 设置数据集
		response.getWriter().print(res.toJSONString()); // 输出到页面
	}

	/**
	 * 预添加or预修改or查看方法
	 * @param course
	 * 预添加时可传入一些初始值；预修改和查看时用来承载主键值
	 * @param toL
	 * 挂字典的字段是否需要转为字典Label值。true则进行转换
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/vieCourse.action")
	public void vieCourse(Course course, Boolean toL,
			HttpServletResponse response) throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (toL != null && toL) { // 查看页面加载数据时调用，返回的对象已进行字典转换
			if (course.getId() != null && course.getId() != 0) { // 检测主键非空
				course = managerService.schCourseById(course.getId()); // 按主键查询数据
				response.getWriter().print(course.getJsonInDict()); // 输出经过字典转换Json对象到查看页(vie_)
			}
		} else { // 修改页面加载数据时调用
			if (course.getId() != null && course.getId() != 0) { // 检查主键非空
				course = managerService.schCourseById(course.getId()); // 按主键查询数据
				response.getWriter().print(course.getJson()); // 输出Json对象到修改页(upd_)
			} else { // 添加页面加载数据时调用，可设置添加页面中一些属性的预设值。
				response.getWriter().print(new Course().getJson()); // 输出Json对象到添加页(upd_)
			}
		}
	}

	/**
	 * 添加或修改的提交方法
	 * @param course
	 * 数据表单Bean
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/updCourse.action")
	public void updCourse(Integer id, Course course,
			HttpServletResponse response, MultipartFile file,
			HttpSession session, HttpServletRequest request) throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (course.getId() == null) { // 检测主键为空则
			managerService.addCourse(id, course, file, session, response,
					request); // 添加数据
		} else { // 主键非空则
			managerService.updCourse(id, course, file, session, response,
					request); // 修改数据
		}
	}

	/**
	 * 删除方法
	 * @param ids
	 * 待删除数据的主键数组
	 * @param response
	 */
	@RequestMapping(value = "/delCourse.action")
	public void delCourse(@RequestParam(value = "ids[]") Integer[] ids,
			HttpServletResponse response) {
		noCache(response); // Ajax方法设定无缓存
		managerService.delCourse(ids); // 删除数据
	}

	/**
	 * JSP页面跳转
	 * @param page
	 * jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value = "/page.action")
	public String goPage(String page) { // 放过本Ctrl的页面，不合法的跳转到error页。
		if ("index".equals(page) || "course_sch".equals(page)// 判断页面
				|| "course_vie".equals(page) || "course_upd".equals(page)) {// 判断页面
			return "/manager/" + page;// 跳转页面
		} else {
			return "/error";// 跳转错误页面
		}
	}
	
	/**
	 * 传输文件流
	 * @param filename
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/appendUrl.action")
	public void appendUrl(String filename, HttpServletRequest request,
			HttpServletResponse response) {
		if (filename != null) {// 判断
			String path = FilePath.getFilePath("courseware") + filename;// 拼接文件路径
			try {
				File file = new File(path);
				if (file.exists()) {// 判断文件是否存在
					FileInputStream in = new FileInputStream(path);// 文件对象输入流
					ServletOutputStream out = response.getOutputStream();// 输出流
					IOUtils.copy(in, out);// 以流的形式展示到前台
					IOUtils.closeQuietly(in);// 关闭流
					IOUtils.closeQuietly(out);// 关闭流
				} else {
					logger.info("文件不存在！不产生数据流输出到前台页面。缺失文件及路径为：" + path);
				}
			} catch (Exception e) {
				e.printStackTrace();// 打印异常信息
			}
		}
	}
	
	/**
	 * 传输文件流
	 * @param filename
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/appendPdfUrl/{filename}.action")
	public void appendPdfUrl(@PathVariable("filename")String filename, HttpServletRequest request,
			HttpServletResponse response) {
		if (filename != null) {// 判断
			String path = FilePath.getFilePath("courseware") +"data"+File.separator+"doc"+File.separator+ filename;// 拼接文件路径
			try {
				File file = new File(path);
				if (file.exists()) {// 判断文件是否存在
					FileInputStream in = new FileInputStream(path);// 文件对象输入流
					ServletOutputStream out = response.getOutputStream();// 输出流
					IOUtils.copy(in, out);// 以流的形式展示到前台
					IOUtils.closeQuietly(in);// 关闭流
					IOUtils.closeQuietly(out);// 关闭流
				} else {
					logger.info("文件不存在！不产生数据流输出到前台页面。缺失文件及路径为：" + path);
				}
			} catch (Exception e) {
				e.printStackTrace();// 打印异常信息
			}
		}
	}
	
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value = "/fileToLoatal.action")
	public Map<String,Object> fileToLoatal(String fileName, HttpServletRequest request,
			HttpServletResponse response){
		Map<String,Object> result = new HashMap<String,Object>();
		try{
			if(fileName != null && !"".equals(fileName)){
				String realPath = FilePath.getFilePath("courseware") + fileName;// 拼接文件路径
				File file = new File(realPath);
				if(file.exists()){
					String webRoot = request.getSession().getServletContext().getRealPath("");
					String webPath= File.separator+"data"+File.separator+"courseCache"+File.separator;
					File dir = new File(webRoot+webPath+"data"+File.separator+"doc"+File.separator);
					if(!dir.isDirectory()){
						dir.mkdirs();
					}
					File fi = new File(webRoot+webPath+fileName);
					if(!fi.exists()){
						FileUpload.copyFile(realPath, webRoot+webPath+fileName);
					}
					result.put("success", true);
					result.put("path", webPath+fileName);
				}else{
					result.put("success", false);
					result.put("msg", "文件不存在");
				}
			}else{
				result.put("success", false);
				result.put("msg", "文件不存在");
			}
		}catch(Exception e){
			e.printStackTrace();
			result.put("success", false);
			result.put("msg", "服务器异常");
		}finally{
			return result;
		}
	}
	
	
}
