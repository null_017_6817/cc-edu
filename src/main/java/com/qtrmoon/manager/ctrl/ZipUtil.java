package com.qtrmoon.manager.ctrl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.util.Enumeration;

import org.apache.commons.io.IOUtils;
import org.apache.tools.zip.ZipEntry;
import org.apache.tools.zip.ZipFile;
import org.apache.tools.zip.ZipOutputStream;
  
/** 
 * ZipUtil工具类
 */  
public class ZipUtil {  

	public static final String EXT = ".zip"; // 定义常量
	
	private static final String PATH = "/";// 定义常量

	public static void main(String[] args) {  // 定义常量
		unZip("d:\\呼入业务试卷（第1套A卷）.zip", "d:\\xxx");// 定义常量
	}

	/**
	 * 压缩
	 * @param srcPath 源路径
	 */
	public static void compress(String srcPath) {
		File srcFile = new File(srcPath);//获取源文件
		String name = srcFile.getName();// 获取文件名
		name = name.substring(0,name.lastIndexOf("."));//获取文件名
		String basePath = srcFile.getParent();//获取父目录
		String destPath = basePath + PATH + name + EXT;//获取目标路径
		compress(new String[]{srcPath}, destPath);//调用方法
	}
	
	/**
	 * 压缩
	 * @param srcPath
	 * @param destPath
	 */
	public static void compress(String srcPath,String destPath) {
		compress(new String[]{srcPath}, destPath);// 调用compress
	}
	
	/**
	 * 压缩
	 * @param srcFiles 源文件
	 * @param descFile 目标文件
	 */
	public static void compress(String[] srcFiles,String descFile){
		try {
			ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(descFile));//压缩文件
			zos.setEncoding("GBK");// 设置编码格式
			for(String file:srcFiles){// 循环
				compress(new File(file),zos,descFile );// 调用compress
			}
			zos.close();// 关闭
		} catch (Exception e) {
			e.printStackTrace();// 打印异常
		}
	}
	
	/**
	 * 解压 为保持兼容性，复制unZip方法代码，添加操作系统判断，解决
	 * 中文目录显示乱码问题
	 * @param zipFilePath
	 * @param unzipDirectory
	 */
	public static void unZipDecode(String zipFilePath,String unzipDirectory){
		try {
			File file=new File(zipFilePath);// 定义文件对象
			ZipFile zipFile = null;
			String osName = System.getProperty("os.name");
//			if(osName.toLowerCase().startsWith("win")) {
				zipFile = new ZipFile(file,"GBK");//定义对象 
//			}else {
//				zipFile = new ZipFile(file,"UTF-8");//定义对象 
//			}
			 
			String name=file.getName();//获取文件名
			name=name.substring(0,name.lastIndexOf("."));// 截取
			File unzipFile=new File(unzipDirectory+"/"+name);// 定义文件对象
			if(unzipFile.exists()){//判断
				unzipFile.delete();// 删除
			}
			unzipFile.mkdirs();//创建目录
			Enumeration zipEnum=zipFile.getEntries();//获取zip文件列表
			InputStream input=null;// 定义常量
			OutputStream output=null;// 定义常量
			ZipEntry entry=null;// 定义常量
			
			while(zipEnum.hasMoreElements()){// 循环
				entry=(ZipEntry)zipEnum.nextElement();//获取下一个实体
				String entryName=entry.getName();// 设置常量
				String[] names=entryName.split("\\/");//获取名称
				String path=unzipFile.getAbsolutePath();//获取绝对路径
				for(int v=0;v<names.length;v++){// 循环
					if(v<names.length-1){// 判断
						path+="/"+names[v]+"/";// 拼接
						new File(path).mkdirs();// 拼接
					}else{
						if(entryName.endsWith("/")){// 判断
							new File(unzipFile.getAbsolutePath()+"/"+entryName).mkdirs();//创建目录
						}else{
							input = zipFile.getInputStream(entry);//获取输入流
							output=new FileOutputStream(new File(unzipFile.getAbsolutePath()+"/"+entryName));//获取输出流
							byte[] buffer=new byte[1024*8];// 定义对象
							int readLen=0;// 定义常量
							while((readLen=input.read(buffer,0,1024*8))!=-1){// 循环
								output.write(buffer, 0, readLen);//写入文件
							}
							input.close();// 关闭
							output.flush();// 刷新
							output.close();// 关闭
						}
					}
				}
			}
//			file.delete();
		} catch (IOException e) {
			e.printStackTrace();// 打印异常
		}
	}

	/**
	 * 解压
	 * @param zipFilePath
	 * @param unzipDirectory
	 */
	public static void unZip(String zipFilePath,String unzipDirectory){
		try {
			File file=new File(zipFilePath);// 定义文件对象
			ZipFile zipFile = new ZipFile(file,"UTF-8");//定义对象
			
			String name=file.getName();//获取文件名
			name=name.substring(0,name.lastIndexOf("."));// 截取
			File unzipFile=new File(unzipDirectory+"/"+name);// 定义文件对象
			if(unzipFile.exists()){//判断
				unzipFile.delete();// 删除
			}
			unzipFile.mkdirs();//创建目录
			Enumeration zipEnum=zipFile.getEntries();//获取zip文件列表
			InputStream input=null;// 定义常量
			OutputStream output=null;// 定义常量
			ZipEntry entry=null;// 定义常量
			
			while(zipEnum.hasMoreElements()){// 循环
				entry=(ZipEntry)zipEnum.nextElement();//获取下一个实体
				String entryName=entry.getName();// 设置常量
				String[] names=entryName.split("\\/");//获取名称
				String path=unzipFile.getAbsolutePath();//获取绝对路径
				for(int v=0;v<names.length;v++){// 循环
					if(v<names.length-1){// 判断
						path+="/"+names[v]+"/";// 拼接
						new File(path).mkdirs();// 拼接
					}else{
						if(entryName.endsWith("/")){// 判断
							new File(unzipFile.getAbsolutePath()+"/"+entryName).mkdirs();//创建目录
						}else{
							input = zipFile.getInputStream(entry);//获取输入流
							output=new FileOutputStream(new File(unzipFile.getAbsolutePath()+"/"+entryName));//获取输出流
							byte[] buffer=new byte[1024*8];// 定义对象
							int readLen=0;// 定义常量
							while((readLen=input.read(buffer,0,1024*8))!=-1){// 循环
								output.write(buffer, 0, readLen);//写入文件
							}
							input.close();// 关闭
							output.flush();// 刷新
							output.close();// 关闭
						}
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();// 打印异常
		}
	}

	/**
	 * 压缩
	 * @param srcFile
	 * @param zos
	 * @param base
	 */
	private static void compress(File srcFile, ZipOutputStream zos,String base) {
		File baseFile = new File(base);// 文件对象
		compress(srcFile,zos,baseFile);// 调用方法
	}
	
	/**
	 * 压缩
	 * @param srcFile
	 * 源文件路径
	 * @param zos
	 * ZipOutputStream
	 * @param base
	 * 压缩包路径（目标路径）
	 * @throws Exception
	 */
	private static void compress(File srcFile, ZipOutputStream zos,File baseFile) {
		URI rel = baseFile.toURI().relativize(srcFile.toURI());
		String relPath = rel.getPath();// 获取参数
		String entryName = relPath.substring(relPath.lastIndexOf("/")+1);// 截取
		try {
			if(srcFile.isDirectory()){//压缩目录
				ZipEntry entry = new ZipEntry(entryName);// 定义对象
				entry.setUnixMode(755);// 设置属性
				zos.putNextEntry(entry);// 设置属性
				zos.closeEntry();// 设置属性
				File[] files = srcFile.listFiles();// 设置属性
				for(File file:files){//递归压缩
					compress(file,zos,baseFile);//递归压缩
				}
			}else{//压缩文件
				ZipEntry entry = new ZipEntry(entryName);// 定义对象
				entry.setUnixMode(644);// 设置属性
				zos.putNextEntry(entry);// 设置属性
				FileInputStream is = new FileInputStream(srcFile);// 设置属性
				zos.write(IOUtils.toByteArray(is));// 设置属性
				is.close();// 关闭
				zos.closeEntry();// 关闭
			}
		} catch (Exception e) {
			e.printStackTrace();// 打印异常
		}
	}
}