package com.qtrmoon.manager.ctrl;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.qtrmoon.common.BaseController;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.manager.pojo.Classes;
import com.qtrmoon.manager.serdao.IManagerService;

/**
 * 班级类控制层
 * 
 * @author HuaTang_DYC
 *
 */
@Controller
@RequestMapping("/manager/classes")
public class ClassesController extends BaseController {
	@Autowired
	private IManagerService managerService; // 声明模块Service实例

	/**
	 * 查询方法，自带分页设置。
	 * @param classes
	 * 承载查询条件的Bean
	 * @param request
	 * 可用来获取非Bean内的参数
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schClasses.action")
	public void schClasses(Classes classes, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		noCache(response);// Ajax方法设定无缓存
		classes.setCondition(""); // 防sql注入
		classes.assLike("name", "number");// 拼接%

		List<Classes> list = managerService.schClasses(classes); // 调用Service查询数据
		/* 将数据Bean的集合转换为Json集合 */
		JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
		JSONObject obj; // 声明用于构造Bean的Json对象
		for (Classes u : list) { // 循环查询的数据集构造Json数据集
			obj = u.getJsonInDict(); // 将Bean转换为Json对象
			obj.put("_oper", u.getId()); // 为"操作列"设定主键值
			datas.add(obj); // 添加Json对象到Json集合
		}
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		JSONObject res = new JSONObject(); // 声明页面返回Json
		res.put("total", classes.getDatasize()); // 设置数据总记录数
		res.put("rows", datas); // 设置数据集
		response.getWriter().print(res.toJSONString()); // 输出到页面
	}

	/**
	 * 预添加or预修改or查看方法
	 * @param classes
	 * 预添加时可传入一些初始值；预修改和查看时用来承载主键值
	 * @param toL
	 * 挂字典的字段是否需要转为字典Label值。true则进行转换
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/vieClasses.action")
	public void vieClasses(Classes classes, Boolean toL,
			HttpServletResponse response) throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (toL != null && toL) { // 查看页面加载数据时调用，返回的对象已进行字典转换
			if (classes.getId() != null && classes.getId() != 0) { // 检测主键非空
				classes = managerService.schClassesById(classes.getId()); // 按主键查询数据
				response.getWriter().print(classes.getJsonInDict()); // 输出经过字典转换Json对象到查看页(vie_)
			}
		} else { // 修改页面加载数据时调用
			if (classes.getId() != null && classes.getId() != 0) { // 检查主键非空
				classes = managerService.schClassesById(classes.getId()); // 按主键查询数据
				response.getWriter().print(classes.getJson()); // 输出Json对象到修改页(upd_)
			} else { // 添加页面加载数据时调用，可设置添加页面中一些属性的预设值。
				response.getWriter().print(new Classes().getJson()); // 输出Json对象到添加页(upd_)
			}
		}
	}

	/**
	 * 添加或修改的提交方法
	 * @param classes
	 * 数据表单Bean
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/updClasses.action")
	public void updClasses(Classes classes, HttpServletResponse response)
			throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (classes.getId() == null) { // 检测主键为空则
			managerService.addClasses(classes); // 添加数据
		} else { // 主键非空则
			managerService.updClasses(classes); // 修改数据
		}
		DictBuffer.updCache("CC_CLASSES");// 更新缓存
	}

	/**
	 * 删除方法
	 * @param ids
	 * 待删除数据的主键数组
	 * @param response
	 */
	@RequestMapping(value = "/delClasses.action")
	public void delClasses(@RequestParam(value = "ids[]") Integer[] ids,
			HttpServletResponse response) {
		noCache(response); // Ajax方法设定无缓存
		managerService.delClasses(ids); // 删除数据
		DictBuffer.updCache("CC_CLASSES");// 更新缓存
	}

	/**
	 * JSP页面跳转
	 * @param page
	 * jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value = "/page.action")
	public String goPage(String page) { // 放过本Ctrl的页面，不合法的跳转到error页。
		if ("index".equals(page) || "classes_sch".equals(page)// 判断跳转页面
				|| "classes_vie".equals(page) || "classes_upd".equals(page)) {// 判断跳转页面
			return "/manager/" + page;// 跳转页面
		} else {
			return "/error";// 跳转错误页面
		}
	}
}
