package com.qtrmoon.manager.ctrl;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.qtrmoon.common.BaseController;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.mail.constants.MailConstants;
import com.qtrmoon.mail.pojo.Message;
import com.qtrmoon.mail.serdao.IMailService;
import com.qtrmoon.mail.util.ListSortUtil;
import com.qtrmoon.manager.pojo.Person;
import com.qtrmoon.manager.pojo.Stud;
import com.qtrmoon.manager.serdao.IManagerService;
import com.qtrmoon.sysmanage.SysConstant;
import com.qtrmoon.sysmanage.pojo.User;
import com.qtrmoon.sysmanage.serdao.ISysmanageService;
import com.qtrmoon.util.DateUtil;
import com.qtrmoon.util.FileUpload;
import com.qtrmoon.util.PropertyUitls;
import com.qtrmoon.util.UuidUtil;

/**
 * 学生类控制器
 * 
 *
 */
@Controller
@RequestMapping("/manager/stud")
public class StudController extends BaseController {
	@Autowired
	private IManagerService managerService; // 声明模块Service实例
	@Autowired
	private IMailService mailService;// 声明模块Service实例
	@Autowired
	private ISysmanageService sysmanageService;//注入service
	
	
	/**
	 * 查询方法，自带分页设置。
	 * @param stud
	 * 承载查询条件的Bean
	 * @param request
	 * 可用来获取非Bean内的参数
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schStud.action")
	public void schStud(Stud stud, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		noCache(response);
		stud.setCondition(""); // 防sql注入
		stud.assLike("name");// 拼接%
		String condition = "";
		String cnumbers = stud.getCnumbers();
		if( cnumbers != null && !"".equals(cnumbers) ){
			condition += "  AND cnumber in ("+cnumbers+") ";
		}
		stud.setCondition(condition);
		
		List<Stud> list = managerService.schStud(stud); // 调用Service查询数据
		/* 将数据Bean的集合转换为Json集合 */
		JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
		JSONObject obj; // 声明用于构造Bean的Json对象
		for (Stud u : list) { // 循环查询的数据集构造Json数据集
			obj = u.getJsonInDict(); // 将Bean转换为Json对象
			obj.put("_oper", u.getId()); // 为"操作列"设定主键值
			datas.add(obj); // 添加Json对象到Json集合
		}
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		JSONObject res = new JSONObject(); // 声明页面返回Json
		res.put("total", stud.getDatasize()); // 设置数据总记录数
		res.put("rows", datas); // 设置数据集
		response.getWriter().print(res.toJSONString()); // 输出到页面
	}
	
	/**
	 * 查询方法，自带分页设置。
	 * @param stud
	 * 承载查询条件的Bean
	 * @param request
	 * 可用来获取非Bean内的参数
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schStudExertoPerson.action")
	public void schStudExertoPerson(Stud stud, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		noCache(response);
		stud.setCondition(""); // 防sql注入
		stud.assLike("name");// 拼接%
		String condition = "";
		String cnumbers = stud.getCnumbers();
		String[] cnumberArray =cnumbers.split(",");
		
		List<Stud> list = new ArrayList<Stud>();
		//后点击勾选的显示在列表前面，将按勾选顺序传回来的数组，倒序查询，合并进list中
		for (int i = cnumberArray.length-1; i >= 0; i--) {
			if( cnumberArray[i] != null && !"".equals(cnumberArray[i]) ){
				condition = "  AND cnumber in ("+cnumberArray[i]+") ";
			}
			stud.setCondition(condition);
			stud.setPagesize(9000);//去掉分页
			List<Stud> tmplist = managerService.schStud(stud); // 调用Service查询数据
			list.addAll(tmplist);
		}
		
		
		/* 将数据Bean的集合转换为Json集合 */
		JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
		JSONObject obj; // 声明用于构造Bean的Json对象
		for (Stud u : list) { // 循环查询的数据集构造Json数据集
			obj = u.getJsonInDict(); // 将Bean转换为Json对象
			obj.put("_oper", u.getId()); // 为"操作列"设定主键值
			datas.add(obj); // 添加Json对象到Json集合
		}
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		JSONObject res = new JSONObject(); // 声明页面返回Json
		res.put("total", list.size()); // 设置数据总记录数
		res.put("rows", datas); // 设置数据集
		response.getWriter().print(res.toJSONString()); // 输出到页面
	}

	/**
	 * 根据班级查询学生列表
	 * @param stud
	 * @param request
	 * @return
	 */
	@SuppressWarnings({ "finally", "unchecked" })
	@ResponseBody
	@RequestMapping(value="schStudListByCnumber.action")
	public Map<String,Object> schStudListByCnumber(Stud stud, HttpServletRequest request){
		// 初始化结果集
		Map<String,Object> result = new HashMap<String,Object>();
		try{
			JSONArray studArray = new JSONArray();
			List<Stud> studs = null;
			if(stud != null && stud.getCnumber() != null){
				Stud param = new Stud();
				param.setCnumber(stud.getCnumber());
				studs = managerService.schStudAll(param);
			}
			if(studs != null){
				for( Stud stuItem : studs ){
					if(stuItem != null && stuItem.getNumber() != null
							&& stuItem.getName() != null){
						JSONObject obj = new JSONObject();
						obj.put("studNumber", stuItem.getNumber());// 学号
						obj.put("studName", stuItem.getName());// 姓名
						obj.put("studCnmber", stud.getCnumber());// 班级
						studArray.add(obj);
					}
				}
			}else{
				studs = new ArrayList<Stud>();
			}
			result.put("data", studArray);
			result.put("success", true);
		}catch(Exception e){
			e.printStackTrace();
			result.put("success", false);
			result.put("msg", "服务器异常！");
		}finally{
			return result;
		}
	}
	
	
	
	/**
	 * 预添加or预修改or查看方法 
	 * @param stud
	 * @param toL
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/vieStud.action")
	public void vieStud(Stud stud, Boolean toL, HttpServletResponse response)
			throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (toL != null && toL) { // 查看页面加载数据时调用，返回的对象已进行字典转换
			if (stud.getId() != null && stud.getId() != 0) { // 检测主键非空
				stud = managerService.schStudById(stud.getId()); // 按主键查询数据
				response.getWriter().print(stud.getJsonInDict()); // 输出经过字典转换Json对象到查看页(vie_)
			}
		} else { // 修改页面加载数据时调用
			if (stud.getId() != null && stud.getId() != 0) { // 检查主键非空
				stud = managerService.schStudById(stud.getId()); // 按主键查询数据
				response.getWriter().print(stud.getJson()); // 输出Json对象到修改页(upd_)
			} else { // 添加页面加载数据时调用，可设置添加页面中一些属性的预设值。
				response.getWriter().print(new Stud().getJson()); // 输出Json对象到添加页(upd_)
			}
		}
	}

	/**
	 * 添加的提交方法
	 * @param stud
	 * 数据表单Bean
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/updStud.action")
	public void updStud(Stud stud, HttpServletResponse response)
			throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (stud.getId() == null) { // 检测主键为空则
			managerService.addStud(stud); // 添加数据
		}
		DictBuffer.updCache("ZD_USER");// 更新缓存
		DictBuffer.updCache("ZD_STUD");// 更新缓存
		DictBuffer.updCache("ZD_TEACHER");// 更新缓存
	}

	/**
	 * 修改的提交方法 
	 * @param stud
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/updStud_upd.action")
	public void updStud_upd(Stud stud, HttpServletResponse response)
			throws IOException {
		noCache(response); // Ajax方法设定无缓存
		Stud stu = managerService.schStudById(stud.getId());// 查询数据
		stud.setPasswd(stu.getPasswd());// 设置密码
		stud.setPartnerClass(stud.getCnumber());// 设置班级号
		managerService.updStudData(stud); // 修改数据
		DictBuffer.updCache("ZD_USER");// 更新缓存
		DictBuffer.updCache("ZD_STUD");// 更新缓存
		DictBuffer.updCache("ZD_TEACHER");// 更新缓存
	}

	/**
	 * 添加学生 
	 * @param stud
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("static-access")
	@RequestMapping(value = "/addStud.action")
	public void addStud(Stud stud, HttpServletResponse response)
			throws IOException {
		noCache(response); // Ajax方法设定无缓存
		stud.setPasswd(stud.getPASSWORD());// 设置密码
		stud.setPartnerClass(stud.getCnumber());// 设置班级号
		managerService.addStud(stud); // 修改数据
		DictBuffer.updCache("ZD_USER");// 更新缓存
		DictBuffer.updCache("ZD_STUD");// 更新缓存
		DictBuffer.updCache("ZD_TEACHER");// 更新缓存
	}

	/**
	 * 删除方法
	 * @param ids
	 * 待删除数据的主键数组
	 * @param response
	 */
	@RequestMapping(value = "/delStud.action")
	public void delStud(@RequestParam(value = "ids[]") Integer[] ids,
			HttpServletResponse response) {
		noCache(response); // Ajax方法设定无缓存
		managerService.delStud(ids); // 删除数据
		DictBuffer.updCache("ZD_USER");// 更新缓存
		DictBuffer.updCache("ZD_STUD");// 更新缓存
		DictBuffer.updCache("ZD_TEACHER");// 更新缓存
	}

	/**
	 * 清除学生考试状态
	 * @param ids
	 * 待删除数据的主键数组
	 * @param response
	 */
	@RequestMapping(value = "/clearExamSta.action")
	public void clearExamSta(@RequestParam(value = "ids[]") Integer[] ids,
			HttpServletResponse response) {
		noCache(response); // Ajax方法设定无缓存
		managerService.updClearExamStu(ids); // 删除数据
	}

	/**
	 * JSP页面跳转
	 * @param page
	 * jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value = "/page.action")
	public String goPage(String page) { // 放过本Ctrl的页面，不合法的跳转到error页。
		if ("index".equals(page) || "stud_sch".equals(page)// 判断页面
				|| "stud_vie".equals(page) || "stud_upd".equals(page)// 判断页面
				|| "stud_file".equals(page) || "stud_upd_upd".equals(page)) {// 判断页面
			return "/manager/" + page;// 跳转页面
		} else {
			return "/error";// 错误页面
		}
	}

	/**
	 * 重置密码
	 * @param id
	 * @param response
	 */
	@RequestMapping(value = "/resetPwd.action")
	public void resetPwd(@RequestParam(value = "ids[]") Integer[] ids,
			HttpServletResponse response) {
		noCache(response); // Ajax方法设定无缓存
		managerService.resetPwd(ids); // 删除数据
	}

	/**
	 * 批量导入方法 
	 * @param cnumber
	 * @param dataFile
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/uploadFile", method = RequestMethod.POST, produces = "text/html;charset=utf-8")
	public String uploadFile(String cnumber,
			@RequestParam("dataFile") CommonsMultipartFile dataFile,
			HttpServletRequest request) {
		String result = "";// 定义常量
		String ffile = DateUtil.getDays();// 获取时间
		// 文件完整路径
		String filePath = "";
		// 文件名
		String fileName = UuidUtil.get32UUID();
		// 文件后缀名 extName
		String extName = "";
		String fileRealPath = "";// 定义常量
		String mse = null;// 定义常量
		if (dataFile != null// 定义常量
				&& dataFile.getOriginalFilename().lastIndexOf(".") >= 0) {// 定义常量
			extName = dataFile.getOriginalFilename().substring(
					dataFile.getOriginalFilename().lastIndexOf("."));// 截取
		}
		try {
			if (null != dataFile && !dataFile.isEmpty() && !"".equals(extName)) {// 判断
				filePath = PropertyUitls.getSysConfigSet("uploadFilePath")// 文件上传路径
						+ ffile; // 文件上传路径
				fileRealPath = filePath + File.separator + fileName + extName;// 拼接
				FileUpload.fileUp(dataFile, filePath, fileName);// 上传
				File path = new File(filePath);// 定义对象
				path.setReadable(true);// 设置属性
				path.setWritable(true);// 设置属性
				path.setExecutable(true);// 设置属性
				File file = new File(fileRealPath);// 定义对象
				file.setReadable(true);// 设置属性
				file.setWritable(true);// 设置属性
				file.setExecutable(true);// 设置属性
				List<Stud> list = parseExcel(dataFile.getInputStream(), null,
						null);// 调用parseExcel
				if (list.size() >= 1) {// 判断
					mse = managerService.addStudFile(cnumber, list);// 添加数据
					result = mse;// 返回结果
				} else {
					result = "文件中无数据";// 返回结果
				}
			} else {
				result = "没有上传文件";// 返回结果
			}
		} catch (Exception e) {
			e.printStackTrace();// 异常信息
		}
		return result;// 返回结果
	}

	/**
	 * 解析Excel 添加到List集合
	 * @param fileRealPath
	 * @param sheetnum
	 * @param rownum
	 * @return
	 * @throws EncryptedDocumentException
	 * @throws InvalidFormatException
	 * @throws IOException
	 */
	public static List<Stud> parseExcel(InputStream filepath, String sheetnum,
			String rownum) {
		List<Stud> list = new ArrayList<Stud>();
		try {
			
			// 根据指定的文件输入流导入Excel从而产生Workbook对象
			Workbook wb0 = WorkbookFactory.create(filepath);
			
			// 获取Excel文档中的每一个表单
			String reg = "^\\d{1}$";
			int sheetnumber;// 定义常量
			if (sheetnum != null && !"".equals(sheetnum)// 判断
					&& sheetnum.matches(reg)) {// 判断
				// 有传sheetnum值，只解析前面制定数量的sheet表
				sheetnumber = Integer.parseInt(sheetnum);// 设置
			} else {
				// 有传sheetnum值，解析所有的sheet表
				sheetnumber = wb0.getNumberOfSheets();
			}
			for (int j = 0; j < sheetnumber; j++) {// 循环
				Sheet sht0 = wb0.getSheetAt(j);// 获取属性
				int rows = 0;// 定义属性
				// 对Sheet中的每一行进行迭代
				for (Row r : sht0) {// 循环
					// 创建实体类
					Stud stud = new Stud();// 定义对象
					// 如果当前行的行号（从0开始）,第一行为0行；
					if (r.getRowNum() < 1) {
						rows = r.getLastCellNum();// 获取属性
						continue;//继续循环
					}
					// 定义一个标签位置，用于判断此行是否需要入库；
					boolean isEffect = true;
					// 定义跳出双层循环的标签；
					lableA:
					// 循环遍历每一个单元格；
					for (int i = 0; i < rows; i++) {
						// 获取单元格
						Cell cell = r.getCell(i);
						// 定义单元格类型
						int cellType;
						// 定义单元格值
						String cellVal;
						String regex = "";
						if (cell != null
								&& cell.getCellType() != cell.CELL_TYPE_BLANK) {
							// 获取单元格类型
							cellType = cell.getCellType();
							DecimalFormat df = new DecimalFormat("#");// 定义对象
							switch (cell.getCellType()) {// 循环
							case HSSFCell.CELL_TYPE_STRING:// case判断
								cellVal = cell.getRichStringCellValue()// 获取值
										.getString().trim();// 获取值
								break;
							case HSSFCell.CELL_TYPE_NUMERIC:// case判断
								cellVal = df.format(cell.getNumericCellValue())// 获取值
										.toString();// 获取值
								break;
							case HSSFCell.CELL_TYPE_BOOLEAN:// case判断
								cellVal = String.valueOf(// 获取值
										cell.getBooleanCellValue()).trim();// 获取值
								break;
							case HSSFCell.CELL_TYPE_FORMULA:// case判断
								cellVal = cell.getCellFormula();// 获取值
								break;
							default:
								cellVal = "";// 赋值空
							}
						} else {
							cellVal = "";//// 赋值空
							continue;// 继续
							// 如果有任意一个单元格为空即跳出循环，此行数据不入库；
							// break lableA;
						}
						// 进入此处数据不为空，获取每一行数据并设置到stud属性中；
						switch (i) {
						case 0: {// case判断
							// 接入码为8～12位数字；
							stud.setCnumber(cellVal);
							break;//停止循环
						}
						case 1: {// case判断
							// 集团标识判断是否为六位数字（只能为六位纯数字）；
							stud.setNumber(cellVal);
							break;//停止循环
						}
						case 2: {// case判断
							//设置名称
							stud.setName(cellVal);
							break;
						}
						case 3: {// case判断
							// 设置性别
							stud.setSex(cellVal);
							break;//停止循环
						}
						case 4: {// case判断
							// 资料；
							stud.setRemark(cellVal);
							break;//停止循环
						}
						default: {// case判断
							cellVal = "";
						}
						}
					}
					if (isEffect) {// 判断
						list.add(stud);// 添加数据
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();// 打印异常
		}
		return list;// 返回结果
	}

	/**
	 * ajax验证是否可以新增 
	 * @param stud
	 * @param request
	 * @param cnumber
	 * @param agentId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/check", method = RequestMethod.POST)
	public String check(Stud stud, HttpServletRequest request, String cnumber,
			String agentId) {
		boolean isOk = false;// 设置标识
		List<Stud> schStud = managerService.schStud(stud);// 查询数据
		for (Stud stud2 : schStud) {// 循环
			String cnumber2 = stud2.getCnumber();// 获取属性
			String agentId2 = stud2.getAgentId();// 获取属性
			if (cnumber.equals(cnumber2) && agentId.equals(agentId2)) {// 判断
				isOk = true;// 设置标识
			}
		}
		return isOk + "";// 返回结果
	}

	/**
	 * 导出模板（学生导入用）
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/exportTemplate.action")
	public void exportTemplate(HttpServletRequest request,
			HttpServletResponse response) {
		String filename = "StudentTemplate.xlsx";// 设置文件名
		String path = request.getSession().getServletContext().getRealPath("");// 设置路径
		String filePath = path + File.separator + "template" + File.separator
				+ filename;// 拼接名称
		try {
			File file = new File(filePath);// 文件对象
			BufferedInputStream fis = new BufferedInputStream(
					new FileInputStream(filePath));// 输入流
			byte[] buffer = new byte[fis.available()];// 定义字节
			fis.read(buffer);// 读
			fis.close();// 关闭
			// 清空response
			response.reset();
			// 设置response的Header
			response.addHeader("Content-Disposition", "attachment;filename="
					+ new String(filename.getBytes()));// 设置response的Header
			response.addHeader("Content-Length", "" + file.length());// 设置response的Header
			OutputStream toClient = new BufferedOutputStream(
					response.getOutputStream());// 输出流
			response.setContentType("application/vnd.ms-excel;charset=gb2312");// 设置属性
			toClient.write(buffer);// 写
			toClient.flush();// 刷新
			toClient.close();// 关闭
		} catch (Exception e) {
			e.printStackTrace();// 打印异常
		}
	}
	
	/**
	 * 查询学生数据(学号和姓名)(邮件实训用)
	 * @param request
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value = "/getAllStudByMail.action")
	public Map<String,Object> getAllStudByMail(Stud stu, HttpServletRequest request ){
		
		Map<String,Object> result = new HashMap<String,Object>();
		Person person = (Person)request.getSession(true).getAttribute(SysConstant.CURRENT_USER);
		try{
			if(stu != null && !StringUtils.isEmpty(stu.getCnumber()) ){
				String sql = "AND number != '"+ person.getNumber()+"'";
				User user = new User();
				//user.setCnumber(stu.getCnumber());
				if(stu != null && !StringUtils.isEmpty(stu.getName())
						&& !StringUtils.isEmpty(stu.getNumber())){
					sql = sql + " AND (name like '%"+stu.getName()+"%' or number like '%"+stu.getNumber()+"%') ";
				}
				sql = sql + " AND cnumber like '%"+stu.getCnumber()+"%'";
				user.setCondition(sql);
				List<User> users = sysmanageService.schUserList(user);
				result.put("success", true);
				result.put("msg", "查询成功");
				result.put("data", users);
			}else{
				result.put("success", false);
				result.put("msg", "查询失败");
			}
		}catch(Exception e){
			e.printStackTrace();
			result.put("success", false);
			result.put("msg", "查询失败，服务器异常");
		}finally{
			return result;
		}
	}
	
	/**
	 * 查询学生数据(学号和姓名)(短信实训用)
	 * @param request
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value = "/getAllStudByMsg.action")
	public Map<String,Object> getAllStudByMsg( String cnumber, HttpServletRequest request ){
		
		Map<String,Object> result = new HashMap<String,Object>();
		Person person = (Person)request.getSession(true).getAttribute(SysConstant.CURRENT_USER);
		try{
			if(cnumber != null && !StringUtils.isEmpty(cnumber) ){
				User user = new User();
				user.setCondition(" AND cnumber like '%"+cnumber+"%'");
				List<User> users = sysmanageService.schUserList(user);
				List<Map<String,Object>> stuLis = new ArrayList<Map<String,Object>>();
				for(User stu : users){
					if( !person.getNumber().equals(stu.getNumber()) ){
						Message param = new Message();
						Map<String,Object> msgObj = new HashMap<String,Object>();
						
						String sendNum = stu.getNumber();
						String receiveNum = person.getNumber();
						String sql = "AND ( (SEND_NUM = '"+sendNum+"' AND RECEIVE_NUM = '"+receiveNum+"')"
								+ " || (SEND_NUM = '"+receiveNum+"' AND RECEIVE_NUM = '"+sendNum+"') )";
						param.setCondition(sql);
						param.setOrderCol("CREATE_TIME DESC limit 1");
						List<Message> msgs = mailService.schMessageList(param);
						
						Map<String,Object> cou = new HashMap<String,Object>();
						cou.put("sendNum", sendNum);
						cou.put("receiveNum", receiveNum);
						cou.put("isRead", MailConstants.UN_READ);
						Integer unreadNum = mailService.schMsgNum(cou);
						
						if(msgs != null && msgs.size() > 0){
							Message newMsg = msgs.get(0);
							msgObj.put("unReadNum", unreadNum);
							msgObj.put("newMsg", newMsg.getContent());
							msgObj.put("creatTime", newMsg.getCreateTime());
						}else{
							msgObj.put("isRead", "");
							msgObj.put("newMsg", "");
							msgObj.put("creatTime", null);
						}
						msgObj.put("sendPhotoUrl", stu.getPhotoUrl());
						msgObj.put("sendCNum", stu.getCnumber());
						msgObj.put("sendNum", stu.getNumber());
						msgObj.put("sendName", stu.getName());
						msgObj.put("receiveNum", person.getNumber());
						
						stuLis.add(msgObj);
					}
				}
				// 排序
				List<Map<String,Object>> data = ListSortUtil.sort(stuLis);
				result.put("success", true);
				result.put("msg", "查询成功");
				result.put("data", data);
			}else{
				result.put("success", false);
				result.put("msg", "查询失败，当前登录状态异常");
			}
		}catch(Exception e){
			e.printStackTrace();
			result.put("success", false);
			result.put("msg", "查询失败，服务器异常");
		}finally{
			return result;
		}
	}
	
	/**
	 * 根据班级查询学生
	 * @param request
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value = "/getStudDictByCnumber.action")
	public Map<String,Object> getStudDictByCnumber( HttpServletRequest request ){
		Map<String,Object> result = new HashMap<String,Object>();// 返回结果集
		String cnumber = (String)request.getParameter("cnumber");
		try{
			if( !StringUtils.isEmpty(cnumber) ){
				User user = new User();
				user.setCondition(" AND cnumber like '%"+cnumber+"%'");;
				List<User> users = sysmanageService.schUserList(user);
				if(users != null && users.size() > 0){
					result.put("success", true);
					result.put("data", users);
				}else{
					result.put("success", false);
					result.put("msg", "未查询到数据");
				}
			}else{
				result.put("success", false);
				result.put("msg", "检索必要条件为空");
			}
		}catch(Exception e){
			e.printStackTrace();
			result.put("success", false);
			result.put("msg", "服务器异常");
		}finally{
			return result;
		}
	}
}
