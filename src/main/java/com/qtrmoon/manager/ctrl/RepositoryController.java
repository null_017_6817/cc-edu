package com.qtrmoon.manager.ctrl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.qtrmoon.book.util.FilePath;
import com.qtrmoon.common.BaseController;
import com.qtrmoon.manager.pojo.Repository;
import com.qtrmoon.manager.pojo.ZdRepository;
import com.qtrmoon.manager.serdao.IManagerService;
import com.qtrmoon.util.FileUpload;

/**
 * 知识库控制器
 *
 */
@Controller
@RequestMapping("/manager/repository")
public class RepositoryController extends BaseController {
	@Autowired
	private IManagerService managerService; // 声明模块Service实例

	/**
	 * 查询方法，自带分页设置。
	 * @param repository
	 * 承载查询条件的Bean
	 * @param request
	 * 可用来获取非Bean内的参数
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schRepository.action")
	public void schRepository(Repository repository,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		noCache(response);// Ajax方法设定无缓存
		
		// 如果title不为空则设置树ID为空
		if(repository != null && repository.getRepoTitle() != null
				&& !"".equals(repository.getRepoTitle())){
			repository.assLike("shortCut","repoTitle");// 拼接%
			repository.setOrganids(null);
		}
		
		List<Repository> list = null;// 定义常量
		String organids = repository.getOrganids();// 获取机构
		if (organids != null && organids != "") {// 判断
			ZdRepository zr = managerService.schZdschRepositoryById(organids); // 根据字典id查询label值
			String label = zr.getLabel(); // 获取lable得知
			Integer pid = zr.getPid(); // 获取pid的值
			if (pid == null) {
				list = managerService.schRepository(repository); // 调用Service查询数据
			}
			if (pid != null) {
				ZdRepository zpr = managerService.schZdschRepositoryByPid(pid); // 根据字典pid查询label值
				String label2 = zpr.getLabel();
				repository.setCondition("and short_cut like '%" + label2
						+ "%' && short_cut like '%" + label + "%'"); // 模糊查询目录
			}
			if (pid != null && pid == 1) {
				repository.setCondition("and short_cut like '%" + label + "%'"); // 模糊查询目录
			}
		}
		list = managerService.schRepository(repository); // 调用Service查询数据
		/* 将数据Bean的集合转换为Json集合 */
		JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
		JSONObject obj; // 声明用于构造Bean的Json对象
		for (Repository u : list) { // 循环查询的数据集构造Json数据集
			obj = u.getJson(); // 将Bean转换为Json对象
			obj.put("_oper", u.getId()); // 为"操作列"设定主键值
			datas.add(obj); // 添加Json对象到Json集合
		}
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		JSONObject res = new JSONObject(); // 声明页面返回Json
		res.put("total", repository.getDatasize()); // 设置数据总记录数
		res.put("pageSize", repository.getPagesize()); // 设置数据总记录数
		res.put("rows", datas); // 设置数据集
		response.getWriter().print(res.toJSONString()); // 输出到页面
	}

	/**
	 * 预添加or预修改or查看方法
	 * @param repository
	 * 预添加时可传入一些初始值；预修改和查看时用来承载主键值
	 * @param toL
	 * 挂字典的字段是否需要转为字典Label值。true则进行转换
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/vieRepository.action")
	public void vieStud(Repository repository, Boolean toL,
			HttpServletResponse response) throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (toL != null && toL) { // 查看页面加载数据时调用，返回的对象已进行字典转换
			if (repository.getId() != null && repository.getId() != 0) { // 检测主键非空
				repository = managerService.schRepositoryById(repository// 按主键查询数据
						.getId()); // 按主键查询数据
				response.getWriter().print(repository.getJsonInDict()); // 输出经过字典转换Json对象到查看页(vie_)
			}
		} else { // 修改页面加载数据时调用
			if (repository.getId() != null && repository.getId() != 0) { // 检查主键非空
				repository = managerService.schRepositoryById(repository
						.getId()); // 按主键查询数据
				response.getWriter().print(repository.getJson()); // 输出Json对象到修改页(upd_)
			} else { // 添加页面加载数据时调用，可设置添加页面中一些属性的预设值。
				response.getWriter().print(new Repository().getJson()); // 输出Json对象到添加页(upd_)
			}
		}
	}

	/**
	 * 添加或修改的提交方法
	 * @param repository
	 * 数据表单Bean
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/updRepository.action")
	public void updRepository(Integer id, Repository repository,
			HttpServletResponse response, MultipartFile file,
			HttpSession session, HttpServletRequest request) throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (repository.getId() == null) {// 判断
			setShortCut(repository);// 设置属性
			managerService.addRepository(id, repository, file, session,// 添加数据,上传文件
					response, request); // 添加数据,上传文件
		} else {
			setShortCut(repository);// 设置属性
			managerService.updRepository(id, repository, file, session,// 修改数据
					response, request); // 修改数据
		}
	}

	/**
	 *  设置shortCut的值
	 * @param repository
	 */
	private void setShortCut(Repository repository) {
		StringBuffer sb = new StringBuffer();// 定义对象
		String shortCut = repository.getShortCut(); // 获取页面传来树的id
		StringBuffer shortCut2 = null;// 设置常量
		String label = null;// 设置常量

		if (shortCut != null && shortCut != ""// 判断
				&& managerService.schZdschRepositoryById(shortCut) != null) {// 判断
			ZdRepository zr1 = managerService.schZdschRepositoryById(shortCut);// 根据id查询对象
			Integer pid = zr1.getPid();// 根据树对象获取pid值
			label = zr1.getLabel();// 根据树对象获取label值
			if (pid == null) {// 判断
				repository.setShortCut(label);// 设置常量
				return;// 返回
			}
			if (pid == 1) {// 判断
				shortCut2 = sb.append("/").append(label);// 当前id的pid为1时新增
				repository.setShortCut(shortCut2.toString());// 设置属性
			} else {
				shortCut2 = getShortCut2(pid, shortCut2);// 获取属性
				repository.setShortCut(sb.append("/").append(shortCut2)// 设置属性
						.append(label).toString());// 设置shortcut的值
			}
		}
	}

	StringBuffer append = null;// 如果pid不等于1则插入
	StringBuffer shortCut3 = null;// 如果pid==1则返回

	/**
	 * 获取值
	 * @param pid
	 * @param shortCut2
	 * @return
	 */
	private StringBuffer getShortCut2(Integer pid, StringBuffer shortCut2) {
		StringBuffer sb = new StringBuffer();// 定义对象
		if (shortCut2 != null) {// 判断
			append = sb.append(shortCut2);// 拼接参数
		}
		Integer pids = managerService.schZdschRepositoryByPid(pid).getPid();//查询数据
		String label = null;// 设置常量
		if (pid != null && pid != 1// 判断
				&& managerService.schZdschRepositoryByPid(pid).getPid() != null) {// 判断
			ZdRepository zr = managerService.schZdschRepositoryByPid(pid);// 查询数据
			label = zr.getLabel();// 获取数据

			if (pids != null// 判断
					&& pids != 1// 判断
					&& managerService.schZdschRepositoryByPid(pids).getPid() != null) {// 判断
				shortCut2 = sb.insert(0, "/").insert(0, label);
			}
			if (pid != null// 判断
					&& managerService.schZdschRepositoryByPid(pid).getPid() == 1) {// 判断
				shortCut3 = sb.insert(0, "/").insert(0, label);
			}
			if (pids != null// 判断
					&& pids != 1// 判断
					&& managerService.schZdschRepositoryByPid(pids).getPid() != null) {// 判断
				getShortCut2(pids, shortCut2);// 调用getShortCut2
			}
		}
		if (pid != null// 判断
				&& managerService.schZdschRepositoryByPid(pid).getPid() == 1) {// 判断
			return shortCut3;// 返回数据
		}
		return append;// 返回数据
	}

	/**
	 * 删除方法
	 * @param ids
	 * 待删除数据的主键数组
	 * @param response
	 */
	@RequestMapping(value = "/delRepository.action")
	public void delRepository(@RequestParam(value = "ids[]") Integer[] ids,
			HttpServletResponse response) {
		noCache(response); // Ajax方法设定无缓存
		managerService.delRepository(ids); // 删除数据
	}

	/**
	 * JSP页面跳转
	 * @param page
	 * jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value = "/page.action")
	public String goPage(String page) { // 放过本Ctrl的页面，不合法的跳转到error页。
		if ("index".equals(page) || "repository_sch".equals(page)// 判断页面
				|| "repository_vie".equals(page)// 判断页面
				|| "repository_upd".equals(page)) {// 判断页面
			return "/manager/" + page;// 跳转页面
		} else {
			return "/error";// 跳转错误页面
		}
	}

	/**
	 * 查询方法，自带分页设置。
	 * @param repository
	 * 承载查询条件的Bean
	 * @param request
	 * 可用来获取非Bean内的参数
	 * @param response
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/schRepositoryByCutOrTitle.action")
	public void schRepositoryByCutOrTitle(Repository repository,
			HttpServletResponse response, String bookid, Model model)
			throws IOException {
		noCache(response);// ajax 无缓存
		List<Repository> list = managerService// 调用Service查询数据
				.schRepositoryByCutOrTitle(repository); // 调用Service查询数据
		/* 将数据Bean的集合转换为Json集合 */
		JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
		JSONObject obj; // 声明用于构造Bean的Json对象
		for (Repository u : list) { // 循环查询的数据集构造Json数据集
			obj = u.getJson(); // 将Bean转换为Json对象
			datas.add(obj); // 添加Json对象到Json集合
		}
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		JSONObject res = new JSONObject(); // 声明页面返回Json
		res.put("pageSize", repository.getPagesize()); // 设置数据总记录数
		res.put("total", repository.getDatasize()); // 设置数据总记录数
		res.put("rows", datas); // 设置数据集
		response.getWriter().print(res.toJSONString()); // 输出到页面
	}

	/**
	 * 获取文件流
	 * @param filename
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/appendUrl/{filename}.action")
	public void appendUrl(@PathVariable("filename")String filename, HttpServletRequest request,
			HttpServletResponse response) {
		if (filename != null) {// 判断
			String path = FilePath.getFilePath("repositoryAddress") + filename;// 拼接文件路径
			try {
				FileInputStream in = new FileInputStream(path);// 文件对象输入流
				ServletOutputStream out = response.getOutputStream();// 输出流
				IOUtils.copy(in, out);// 以流的形式展示到前台
				IOUtils.closeQuietly(in);// 关闭流
				IOUtils.closeQuietly(out);// 关闭流
			} catch (Exception e) {
				e.printStackTrace();// 打印异常
			}
		}
	}
	
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value = "/fileToLoatal.action")
	public Map<String,Object> fileToLoatal(String fileName, HttpServletRequest request,
			HttpServletResponse response){
		Map<String,Object> result = new HashMap<String,Object>();
		try{
			if(fileName != null && !"".equals(fileName)){
				String realPath = FilePath.getFilePath("repositoryAddress") + fileName;// 拼接文件路径
				File file = new File(realPath);
				if(file.exists()){
					String webRoot = request.getSession().getServletContext().getRealPath("");
					String webPath= File.separator+"data"+File.separator+"repositor"+File.separator;
					File dir = new File(webRoot+webPath);
					if(!dir.isDirectory()){
						dir.mkdirs();
					}
					FileUpload.copyFile(realPath, webRoot+webPath+fileName);
					result.put("success", true);
					result.put("path", webPath+fileName);
				}else{
					result.put("success", false);
					result.put("msg", "文件不存在");
				}
			}else{
				result.put("success", false);
				result.put("msg", "文件不存在");
			}
		}catch(Exception e){
			e.printStackTrace();
			result.put("success", false);
			result.put("msg", "服务器异常");
		}finally{
			return result;
		}
	}
}
