package com.qtrmoon.practical.pojo;
import org.json.simple.JSONObject;
import com.qtrmoon.common.PageForm;

	/** 客户卡片Object类 */
	public class TrainClient extends PageForm{
	//Fields
	
	private Integer id;// 主键
	private String userid;// 用户
	private String name;// 姓名
	private String sex;// 性别
	private String telphone;// 电话
	private String company;// 公司名称
	private String profession;// 行业类别
	private String addr;// 地址
	private String zipcode;// 邮编
	private String email;// 电子邮箱
	private String belong;// 归属地
	private String certType;// 证件类型
	private String certNum;// 证件号码
	private String cusNum;// 客户编号
	private String cusLevel;// 客户编号
	private String proBelong;// 产品归属地
	
	//Constructors
	/** default constructor */
	public TrainClient() {
	
	}	
	//getter and setter
	/** 获取主键 */
	public Integer getId() {
		return this.id;
	}
	/** 设置主键 */
	public void setId(Integer id) {
		this.id = id;
	}
	/** 获取用户 */
	public String getUserid() {
		return this.userid;
	}
	/** 设置用户 */
	public void setUserid(String userid) {
		this.userid = userid;
	}
	/** 获取姓名 */
	public String getName() {
		return this.name;
	}
	/** 设置姓名 */
	public void setName(String name) {
		this.name = name;
	}
	/** 获取性别 */
	public String getSex() {
		return this.sex;
	}
	/** 设置性别 */
	public void setSex(String sex) {
		this.sex = sex;
	}
	/** 获取电话 */
	public String getTelphone() {
		return this.telphone;
	}
	/** 设置电话 */
	public void setTelphone(String telphone) {
		this.telphone = telphone;
	}
	/** 获取公司名称 */
	public String getCompany() {
		return this.company;
	}
	/** 设置公司名称 */
	public void setCompany(String company) {
		this.company = company;
	}
	/** 获取行业类别 */
	public String getProfession() {
		return this.profession;
	}
	/** 设置行业类别 */
	public void setProfession(String profession) {
		this.profession = profession;
	}
	/** 获取地址 */
	public String getAddr() {
		return this.addr;
	}
	/** 设置地址 */
	public void setAddr(String addr) {
		this.addr = addr;
	}
	/** 获取邮编 */
	public String getZipcode() {
		return this.zipcode;
	}
	/** 设置邮编 */
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	/** 获取电子邮箱 */
	public String getEmail() {
		return this.email;
	}
	/** 设置电子邮箱 */
	public void setEmail(String email) {
		this.email = email;
	}
	/** 获取归属地 */
	public String getBelong() {
		return this.belong;
	}
	/** 设置归属地 */
	public void setBelong(String belong) {
		this.belong = belong;
	}

	/**
	 * 获取证件类型
	 * @return
	 */
	public String getCertType() {
		return certType;
	}
	/**
	 * 设置证件类型
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType;
	}
	/**
	 * 获取证件号码
	 * @return
	 */
	public String getCertNum() {
		return certNum;
	}
	/**
	 * 设置证件号码
	 * @param certNum
	 */
	public void setCertNum(String certNum) {
		this.certNum = certNum;
	}
	/**
	 * 获取客户编号
	 * @return
	 */
	public String getCusNum() {
		return cusNum;
	}
	/**
	 * 设置客户编号
	 * @param cusNum
	 */
	public void setCusNum(String cusNum) {
		this.cusNum = cusNum;
	}
	/**
	 * 获取客户等级
	 * @return
	 */
	public String getCusLevel() {
		return cusLevel;
	}
	/**
	 * 设置客户等级
	 * @param cusLevel
	 */
	public void setCusLevel(String cusLevel) {
		this.cusLevel = cusLevel;
	}
	/**
	 * 获取产品归属地
	 * @return
	 */
	public String getProBelong() {
		return proBelong;
	}
	/**
	 * 设置产品归属地
	 * @param proBelong
	 */
	public void setProBelong(String proBelong) {
		this.proBelong = proBelong;
	}
	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("userid", userid);
		obj.put("name", name);
		obj.put("sex", sex);
		obj.put("telphone", telphone);
		obj.put("company", company);
		obj.put("profession", profession);
		obj.put("addr", addr);
		obj.put("zipcode", zipcode);
		obj.put("email", email);
		obj.put("belong", belong);
		obj.put("certType", certType);
		obj.put("certNum", certNum);
		obj.put("cusNum", cusNum);
		obj.put("cusLevel", cusLevel);
		obj.put("proBelong", proBelong);
		return obj;
	}
	
	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("userid", userid);
		obj.put("name", name);
		obj.put("sex", sex);
		obj.put("telphone", telphone);
		obj.put("company", company);
		obj.put("profession", profession);
		obj.put("addr", addr);
		obj.put("zipcode", zipcode);
		obj.put("email", email);
		obj.put("belong", belong);
		obj.put("certType", certType);
		obj.put("certNum", certNum);
		obj.put("cusNum", cusNum);
		obj.put("cusLevel", cusLevel);
		obj.put("proBelong", proBelong);
		return obj;
	}
}
