package com.qtrmoon.practical.pojo;
import org.json.simple.JSONObject;

import com.qtrmoon.common.PageForm;
	/** 问卷题目Object类 */
	public class QuestTopic extends PageForm{
	//Fields
	
	private Integer id;// 主键
	private String title;// 标题
	private Integer score;// 得分
	private String content;//内容
	//Constructors
	/** default constructor */
	public QuestTopic() {
	
	}	
	//getter and setter
	/** 获取主键 */
	public Integer getId() {
		return this.id;
	}
	/** 设置主键 */
	public void setId(Integer id) {
		this.id = id;
	}
	/** 获取标题 */
	public String getTitle() {
		return this.title;
	}
	/** 设置标题 */
	public void setTitle(String title) {
		this.title = title;
	}
	/** 获取得分 */
	public Integer getScore() {
		return this.score;
	}
	/** 设置得分 */
	public void setScore(Integer score) {
		this.score = score;
	}
	
	/** 设置内容 */
	public String getContent() {
		return content;
	}
	/** 设置内容 */
	public void setContent(String content) {
		this.content = content;
	}
	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("title", title);
		obj.put("content", content);
		obj.put("score", score);
		return obj;
	}
	
	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("title", title);
		obj.put("content", content);
		obj.put("score", score);
		return obj;
	}
}
