package com.qtrmoon.practical.pojo;
import java.util.Date;

import org.json.simple.JSONObject;

import com.qtrmoon.common.PageForm;
import com.qtrmoon.toolkit.DateTransfer;
	/** 工单Object类 */
	public class TrainBill extends PageForm{
	//Fields
	
	private Integer id;// 主键 (共通字段)
	private Integer taskId;// 任务id(共通字段)
	private String type;// 类型(共通字段)
	private String number;// 工单号(共通字段)
	private String userid;// 学号(共通字段)
	private Date createTime;// 创建时间(共通字段)
	private Date tm;// 呼入时间(共通字段)
	private String callTime;// 呼入时间(共通字段)
	private String tmBeg,tmEnd;//时间条件的跨度查询属性(共通字段)
	private String dialing;// 主叫(共通字段)
	private String contact;// 联系电话(共通字段)
	private String customer;// 客户名称（--）
	private String business;// 业务类型(工单类型-通用工单用)
	private String result;// 处理结果(共通字段)
	private String descrip;// 描述(共通字段)
	private String orderAllot;// 工单分配(共通字段)
	private String acceptType;// 受理方式(通用工单用)
	private String directoryFir;// 一级目录(共通字段)
	private String directorySec;// 二级目录(共通字段)
	private String department;// 所属部门(共通字段)
	private String departureCity; //出发城市(航空工单用)
	private String arriveCity; //到达城市(航空工单用)
	private Date departureDate; //出发日期(航空工单用)
	private String departureDateStr;//出发日期字符串(航空工单用)
	private String airlineComp; //航空公司(航空工单用)
	private String customerTel; //客户电话(航空工单用)
	private String customerLevel; //客户级别(航空工单用)
	private String orderInfo; //订单信息（物流工单用）
	
	//Constructors
	/** default constructor */
	public TrainBill() {
	
	}	
	//getter and setter
	/** 获取主键 */
	public Integer getId() {
		return this.id;
	}
	/** 设置主键 */
	public void setId(Integer id) {
		this.id = id;
	}
	/** 获取工单号 */
	public String getNumber() {
		return this.number;
	}
	/** 设置工单号 */
	public void setNumber(String number) {
		this.number = number;
	}
	/** 获取用户 */
	public String getUserid() {
		return this.userid;
	}
	/** 设置用户 */
	public void setUserid(String userid) {
		this.userid = userid;
	}
	/** 获取呼入时间 */
	public Date getTm() {
		return this.tm;
	}
	/** 设置呼入时间 */
	public void setTm(Date tm) {
		this.tm = tm;
	}
	/** 设定[呼入时间]时间转载字段值，页面提交时由springmvc自动装载 */
	public void setTmstr(String tm) {
		this.tm=_getTime(tm);
	}
	/** 获取[呼入时间]时间起始条件 */
	public String getTmBeg() {
		return tmBeg;
	}
	/** 获取[呼入时间]时间起始条件(Dao/Mapper查询时调用) */
	public Date getTmBegDate() {
		return _getBegDate(tmBeg);
	}
	/** 设置[呼入时间]时间起始条件(表单提交时自动装载) */
	public void setTmBeg(String tmBeg) {
		this.tmBeg = tmBeg;
	}
	
	/** 获取[呼入时间]时间结束条件 */
	public String getTmEnd() {
		return tmEnd;
	}
	/** 获取[呼入时间]时间结束条件(Dao/Mapper查询时调用) */
	public Date getTmEndDate() {
		return _getEndDate(tmEnd);
	}
	/** 设置[呼入时间]时间结束条件(表单提交时自动装载) */
	public void setTmEnd(String tmEnd) {
		this.tmEnd = tmEnd;
	}
	/** 获取主叫 */
	public String getDialing() {
		return this.dialing;
	}
	/** 设置主叫 */
	public void setDialing(String dialing) {
		this.dialing = dialing;
	}
	/** 获取联系电话 */
	public String getContact() {
		return this.contact;
	}
	/** 设置联系电话 */
	public void setContact(String contact) {
		this.contact = contact;
	}
	/** 获取客户名称 */
	public String getCustomer() {
		return this.customer;
	}
	/** 设置客户名称 */
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	/** 获取业务类型 */
	public String getBusiness() {
		return this.business;
	}
	/** 设置业务类型 */
	public void setBusiness(String business) {
		this.business = business;
	}
	/** 获取处理结果 */
	public String getResult() {
		return this.result;
	}
	/** 设置处理结果 */
	public void setResult(String result) {
		this.result = result;
	}
	/** 获取描述 */
	public String getDescrip() {
		return this.descrip;
	}
	/** 设置描述 */
	public void setDescrip(String descrip) {
		this.descrip = descrip;
	}
	
	/**
	 * 获取工单分配
	 * @return
	 */
	public String getOrderAllot() {
		return orderAllot;
	}
	/**
	 * 设置工单分配
	 * @param orderAllot
	 */
	public void setOrderAllot(String orderAllot) {
		this.orderAllot = orderAllot;
	}
	/**
	 * 获取受理方式
	 * @return
	 */
	public String getAcceptType() {
		return acceptType;
	}
	/**
	 * 设置受理方式
	 * @param acceptType
	 */
	public void setAcceptType(String acceptType) {
		this.acceptType = acceptType;
	}
	/**
	 * 获取一级目录
	 * @return
	 */
	public String getDirectoryFir() {
		return directoryFir;
	}
	/**
	 * 设置一级目录
	 * @param directoryFir
	 */
	public void setDirectoryFir(String directoryFir) {
		this.directoryFir = directoryFir;
	}
	/**
	 * 获取二级目录
	 * @return
	 */
	public String getDirectorySec() {
		return directorySec;
	}
	/**
	 * 设置二级目录
	 * @param directorySec
	 */
	public void setDirectorySec(String directorySec) {
		this.directorySec = directorySec;
	}
	/**
	 * 获取所属部门
	 * @return
	 */
	public String getDepartment() {
		return department;
	}
	/**
	 * 设置所属部门
	 * @param department
	 */
	public void setDepartment(String department) {
		this.department = department;
	}
	/**
	 * 获取打电话时间
	 * @return
	 */
	public String getCallTime() {
		return callTime;
	}
	/**
	 * 设置打电话时间
	 * @param callTime
	 */
	public void setCallTime(String callTime) {
		this.callTime = callTime;
	}
	/**
	 * 获取任务id
	 * @return
	 */
	public Integer getTaskId() {
		return taskId;
	}
	/**
	 * 设置任务id
	 * @param taskId
	 */
	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}
	/**
	 * 获取类型
	 * @return
	 */
	public String getType() {
		return type;
	}
	/**
	 * 设置类型
	 * @param type
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * 获取创建时间
	 * @return
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置创建时间
	 * @param createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	/**
	 * 获取出发城市
	 * @return
	 */
	public String getDepartureCity() {
		return departureCity;
	}
	
	/**
	 * 设置出发城市
	 * @param departureCity
	 */
	public void setDepartureCity(String departureCity) {
		this.departureCity = departureCity;
	}
	
	/**
	 * 获取到达城市
	 * @return
	 */
	public String getArriveCity() {
		return arriveCity;
	}
	
	/**
	 * 设置到达城市
	 * @param arriveCity
	 */
	public void setArriveCity(String arriveCity) {
		this.arriveCity = arriveCity;
	}
	
	/**
	 * 获取出发时间
	 * @return
	 */
	public Date getDepartureDate() {
		return departureDate;
	}
	
	/**
	 * 设置出发时间
	 * @param departureDate
	 */
	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}
	
	/**
	 * 获取航空公司
	 * @return
	 */
	public String getAirlineComp() {
		return airlineComp;
	}
	
	/**
	 * 设置航空公司
	 * @param airlineComp
	 */
	public void setAirlineComp(String airlineComp) {
		this.airlineComp = airlineComp;
	}
	
	/**
	 * 获取客户电话
	 * @return
	 */
	public String getCustomerTel() {
		return customerTel;
	}
	
	/**
	 * 设置客户电话
	 * @param customerTel
	 */
	public void setCustomerTel(String customerTel) {
		this.customerTel = customerTel;
	}
	
	/**
	 * 获取客户等级
	 * @return
	 */
	public String getCustomerLevel() {
		return customerLevel;
	}
	
	/**
	 * 设置客户等级
	 * @param customerLevel
	 */
	public void setCustomerLevel(String customerLevel) {
		this.customerLevel = customerLevel;
	}
	
	/**
	 * 获取订单信息
	 * @return
	 */
	public String getOrderInfo() {
		return orderInfo;
	}
	
	/**
	 * 设置订单信息
	 * @param orderInfo
	 */
	public void setOrderInfo(String orderInfo) {
		this.orderInfo = orderInfo;
	}
	
	/**
	 * 获取时间出发str
	 * @return
	 */
	public String getDepartureDateStr() {
		return departureDateStr;
	}
	
	/**
	 * 设置时间出发str
	 * @param departureDateStr
	 */
	public void setDepartureDateStr(String departureDateStr) {
		this.departureDateStr = departureDateStr;
	}
	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("number", number);
		obj.put("userid", userid);
		obj.put("taskId", taskId);
		obj.put("type", type);
		//时间字段处理。tm查询列表页使用，tmstr修改页使用
		obj.put("tm", DateTransfer.toString(tm,"yyyy-MM-dd HH:mm:ss"));
		obj.put("tmstr", DateTransfer.toString(tm,"yyyy-MM-dd HH:mm:ss"));
		obj.put("createTime", DateTransfer.toString(createTime,"yyyy-MM-dd HH:mm:ss"));
		obj.put("dialing", dialing);
		obj.put("contact", contact);
		obj.put("customer", customer);
		obj.put("business", business);
		obj.put("result", result);
		obj.put("descrip", descrip);
		obj.put("orderAllot", orderAllot);// 工单分配
		obj.put("acceptType", acceptType);// 受理方式
		obj.put("directoryFir", directoryFir);// 一级目录
		obj.put("directorySec",directorySec);// 二级目录
		obj.put("department",department);// 所属部门
		obj.put("departureCity",departureCity);// 出发城市(航空工单用)
		obj.put("arriveCity",arriveCity);// 到达城市(航空工单用)
		obj.put("departureDate",departureDate);// 出发日期(航空工单用)
		obj.put("departureDateStr",departureDateStr);// 出发日期(航空工单用)
		obj.put("airlineComp",airlineComp);// 航空公司(航空工单用)
		obj.put("customerTel",customerTel);// 客户电话(航空工单用)
		obj.put("customerLevel",customerLevel);// 客户级别(航空工单用)
		obj.put("orderInfo",orderInfo);// 订单信息(航空工单用)
		
		return obj;
	}
	
	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("number", number);
		obj.put("userid", userid);
		obj.put("taskId", taskId);
		obj.put("type", type);
		//时间字段处理。tm查询列表页使用，tmstr修改页使用
		obj.put("tm", DateTransfer.toString(tm,"yyyy-MM-dd HH:mm:ss"));
		obj.put("tmstr", DateTransfer.toString(tm,"yyyy-MM-dd HH:mm:ss"));
		obj.put("tmYear", DateTransfer.toString(tm,"yyyy-MM-dd"));
		obj.put("tmHour", DateTransfer.toString(tm,"HH:mm:ss"));
		obj.put("createTime", DateTransfer.toString(createTime,"yyyy-MM-dd HH:mm:ss"));
		obj.put("dialing", dialing);
		obj.put("contact", contact);
		obj.put("customer", customer);
		obj.put("business", business);
		obj.put("result", result);
		obj.put("descrip", descrip);
		obj.put("orderAllot", orderAllot);// 工单分配
		obj.put("acceptType", acceptType);// 受理方式
		obj.put("directoryFir", directoryFir);// 一级目录
		obj.put("directorySec",directorySec);// 二级目录
		obj.put("department",department);// 所属部门
		
		obj.put("departureCity",departureCity);// 出发城市(航空工单用)
		obj.put("arriveCity",arriveCity);// 到达城市(航空工单用)
		obj.put("departureDate",departureDate);// 出发日期(航空工单用)
		obj.put("departureDateStr",DateTransfer.toString(departureDate,"yyyy-MM-dd HH:mm:ss"));
		obj.put("airlineComp",airlineComp);// 航空公司(航空工单用)
		obj.put("customerTel",customerTel);// 客户电话(航空工单用)
		obj.put("customerLevel",customerLevel);// 客户级别(航空工单用)
		obj.put("orderInfo",orderInfo);// 订单信息(航空工单用)
		
		return obj;
	}
}
