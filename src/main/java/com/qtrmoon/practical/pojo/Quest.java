package com.qtrmoon.practical.pojo;
import java.util.Date;
import java.util.Set;
import java.util.HashSet;
import org.json.simple.JSONObject;
import com.qtrmoon.common.PageForm;
import com.qtrmoon.toolkit.DateTransfer;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.dictionary.bsd.DictBean;
	/** 问卷Object类 */
	public class Quest extends PageForm{
	//Fields
	
	private Integer id;// 主键
	private String title;// 标题
	private Integer totalscore;// 总分
	private Integer effectivescore;// 有效分值
	private String remark;// 备注
	//Constructors
	/** default constructor */
	public Quest() {
	
	}	
	//getter and setter
	/** 获取主键 */
	public Integer getId() {
		return this.id;
	}
	/** 设置主键 */
	public void setId(Integer id) {
		this.id = id;
	}
	/** 获取标题 */
	public String getTitle() {
		return this.title;
	}
	/** 设置标题 */
	public void setTitle(String title) {
		this.title = title;
	}
	/** 获取总分 */
	public Integer getTotalscore() {
		return this.totalscore;
	}
	/** 设置总分 */
	public void setTotalscore(Integer totalscore) {
		this.totalscore = totalscore;
	}
	/** 获取有效分值 */
	public Integer getEffectivescore() {
		return this.effectivescore;
	}
	/** 设置有效分值 */
	public void setEffectivescore(Integer effectivescore) {
		this.effectivescore = effectivescore;
	}
	/** 获取备注 */
	public String getRemark() {
		return this.remark;
	}
	/** 设置备注 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("title", title);
		obj.put("totalscore", totalscore);
		obj.put("effectivescore", effectivescore);
		obj.put("remark", remark);
		return obj;
	}
	
	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("title", title);
		obj.put("totalscore", totalscore);
		obj.put("effectivescore", effectivescore);
		obj.put("remark", remark);
		return obj;
	}
}
