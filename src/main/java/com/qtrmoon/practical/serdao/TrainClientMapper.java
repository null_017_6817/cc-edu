package com.qtrmoon.practical.serdao;

import java.util.List;
import org.springframework.stereotype.Component;

import com.qtrmoon.practical.pojo.TrainClient;
/**
 * 客户信息持久层
 *
 */
@Component
public interface TrainClientMapper {
	/**
	 * TrainClient的条件查询方法
	 * @param trainClient 承载查询条件
	 * @return 返回TrainClient的集合
	 */
	public List<TrainClient> schTrainClient(TrainClient trainClient);
	
	/**
	 * TrainClient的主键查询方法
	 * @param id 主键值
	 * @return 返回TrainClient实体
	 */
	public TrainClient schTrainClientById(Integer id);
	
	/**
	 * TrainClient的添加方法
	 * @param trainClient 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addTrainClient(TrainClient trainClient);
	
	/**
	 * TrainClient的修改方法
	 * @param trainClient 承载数据的Bean
	 */
	public void updTrainClient(TrainClient trainClient);
	
	/**
	 * TrainClient的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delTrainClient(Integer id);
	
	/**
	 * TrainClient的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delTrainClients(Integer[] ids);
}
