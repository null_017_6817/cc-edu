package com.qtrmoon.practical.serdao;

import java.util.List;
import org.springframework.stereotype.Component;

import com.qtrmoon.practical.pojo.Quest;
/**
 * 问卷持久层
 *
 */
@Component
public interface QuestMapper {
	/**
	 * Quest的条件查询方法
	 * @param quest 承载查询条件
	 * @return 返回Quest的集合
	 */
	public List<Quest> schQuest(Quest quest);
	
	/**
	 * Quest的主键查询方法
	 * @param id 主键值
	 * @return 返回Quest实体
	 */
	public Quest schQuestById(Integer id);
	
	/**
	 * Quest的添加方法
	 * @param quest 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addQuest(Quest quest);
	
	/**
	 * Quest的修改方法
	 * @param quest 承载数据的Bean
	 */
	public void updQuest(Quest quest);
	
	/**
	 * Quest的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delQuest(Integer id);
	
	/**
	 * Quest的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delQuests(Integer[] ids);
}
