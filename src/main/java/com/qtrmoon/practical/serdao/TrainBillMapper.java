package com.qtrmoon.practical.serdao;

import java.util.List;
import org.springframework.stereotype.Component;

import com.qtrmoon.practical.pojo.TrainBill;
/**
 * 工单持久层
 *
 */
@Component
public interface TrainBillMapper {
	/**
	 * TrainBill的条件查询方法
	 * @param trainBill 承载查询条件
	 * @return 返回TrainBill的集合
	 */
	public List<TrainBill> schTrainBill(TrainBill trainBill);
	
	/**
	 * TrainBill的主键查询方法
	 * @param id 主键值
	 * @return 返回TrainBill实体
	 */
	public TrainBill schTrainBillById(Integer id);
	
	/**
	 * TrainBill的添加方法
	 * @param trainBill 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addTrainBill(TrainBill trainBill);
	
	/**
	 * TrainBill的修改方法
	 * @param trainBill 承载数据的Bean
	 */
	public void updTrainBill(TrainBill trainBill);
	
	/**
	 * TrainBill的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delTrainBill(Integer id);
	
	/**
	 * TrainBill的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delTrainBills(Integer[] ids);
}
