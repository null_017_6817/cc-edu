package com.qtrmoon.practical.serdao;

import java.util.List;
import org.springframework.stereotype.Component;

import com.qtrmoon.practical.pojo.QuestTopic;
/**
 * 问卷题目类持久层
 *
 */
@Component
public interface QuestTopicMapper {
	/**
	 * QuestTopic的条件查询方法
	 * @param questTopic 承载查询条件
	 * @return 返回QuestTopic的集合
	 */
	public List<QuestTopic> schQuestTopic(QuestTopic questTopic);
	
	/**
	 * QuestTopic的主键查询方法
	 * @param id 主键值
	 * @return 返回QuestTopic实体
	 */
	public QuestTopic schQuestTopicById(Integer id);
	
	/**
	 * QuestTopic的添加方法
	 * @param questTopic 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addQuestTopic(QuestTopic questTopic);
	
	/**
	 * QuestTopic的修改方法
	 * @param questTopic 承载数据的Bean
	 */
	public void updQuestTopic(QuestTopic questTopic);
	
	/**
	 * QuestTopic的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delQuestTopic(Integer id);
	
	/**
	 * QuestTopic的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delQuestTopics(Integer[] ids);
}
