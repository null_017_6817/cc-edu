package com.qtrmoon.practical.serdao;
import java.util.List;

import com.qtrmoon.practical.pojo.Quest;
import com.qtrmoon.practical.pojo.QuestTopic;
import com.qtrmoon.practical.pojo.TrainBill;
import com.qtrmoon.practical.pojo.TrainClient;
/**
 * 实训接口
 *
 */
public interface IPracticalService {
	
	/***************** Quest方法组 *****************/
	/**
	 * Quest的条件查询方法
	 * @param quest 承载查询条件的Bean
	 * @return 返回Quest的集合
	 */
	public List<Quest> schQuest(Quest quest);
	
	/**
	 * Quest的主键查询方法
	 * @param id 主键值
	 * @return 返回Quest实体
	 */
	public Quest schQuestById(Integer id);
	
	/**
	 * Quest的添加方法
	 * @param quest 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addQuest(Quest quest);
	
	/**
	 * Quest的修改方法
	 * @param quest 承载数据的Bean
	 */
	public void updQuest(Quest quest);
	
	/**
	 * Quest的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delQuest(Integer id);
	
	/**
	 * Quest的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delQuest(Integer[] ids);


	
	/***************** QuestTopic方法组 *****************/
	/**
	 * QuestTopic的条件查询方法
	 * @param questTopic 承载查询条件的Bean
	 * @return 返回QuestTopic的集合
	 */
	public List<QuestTopic> schQuestTopic(QuestTopic questTopic);
	
	/**
	 * QuestTopic的主键查询方法
	 * @param id 主键值
	 * @return 返回QuestTopic实体
	 */
	public QuestTopic schQuestTopicById(Integer id);
	
	/**
	 * QuestTopic的添加方法
	 * @param questTopic 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addQuestTopic(QuestTopic questTopic);
	
	/**
	 * QuestTopic的修改方法
	 * @param questTopic 承载数据的Bean
	 */
	public void updQuestTopic(QuestTopic questTopic);
	
	/**
	 * QuestTopic的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delQuestTopic(Integer id);
	
	/**
	 * QuestTopic的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delQuestTopic(Integer[] ids);


	
	/***************** TrainBill方法组 *****************/
	/**
	 * TrainBill的条件查询方法
	 * @param trainBill 承载查询条件的Bean
	 * @return 返回TrainBill的集合
	 */
	public List<TrainBill> schTrainBill(TrainBill trainBill);
	/**
	 * TrainBill的条件查询方法(无分页)
	 * @param trainBill 承载查询条件的Bean
	 * @return 返回TrainBill的集合
	 */
	public List<TrainBill> schTrainBillList(TrainBill trainBill);
	/**
	 * TrainBill的主键查询方法
	 * @param id 主键值
	 * @return 返回TrainBill实体
	 */
	public TrainBill schTrainBillById(Integer id);
	
	/**
	 * TrainBill的添加方法
	 * @param trainBill 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addTrainBill(TrainBill trainBill);
	
	/**
	 * TrainBill的修改方法
	 * @param trainBill 承载数据的Bean
	 */
	public void updTrainBill(TrainBill trainBill);
	
	/**
	 * TrainBill的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delTrainBill(Integer id);
	
	/**
	 * TrainBill的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delTrainBill(Integer[] ids);


	
	/***************** TrainClient方法组 *****************/
	/**
	 * TrainClient的条件查询方法
	 * @param trainClient 承载查询条件的Bean
	 * @return 返回TrainClient的集合
	 */
	public List<TrainClient> schTrainClient(TrainClient trainClient);
	
	/**
	 * TrainClient的主键查询方法
	 * @param id 主键值
	 * @return 返回TrainClient实体
	 */
	public TrainClient schTrainClientById(Integer id);
	
	/**
	 * TrainClient的添加方法
	 * @param trainClient 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addTrainClient(TrainClient trainClient);
	
	/**
	 * TrainClient的修改方法
	 * @param trainClient 承载数据的Bean
	 */
	public void updTrainClient(TrainClient trainClient);
	
	/**
	 * TrainClient的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delTrainClient(Integer id);
	
	/**
	 * TrainClient的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delTrainClient(Integer[] ids);



}//end