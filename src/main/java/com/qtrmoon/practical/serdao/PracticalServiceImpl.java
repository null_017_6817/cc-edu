package com.qtrmoon.practical.serdao;
import java.util.List;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qtrmoon.practical.pojo.*;
/**
 * 实训实现类
 *
 */
@Service("practicalService")
public class PracticalServiceImpl implements IPracticalService {
	@Autowired
	private QuestMapper questMapper;//声明mapper


	/***************** Quest方法组 *****************/
	/**
	 * Quest的条件查询方法
	 * @param quest 承载查询条件的Bean
	 * @return 返回Quest的集合
	 */
	@Override
	public List<Quest> schQuest(Quest quest) {
		PageHelper.startPage(quest.getPage(), quest.getPagesize());//设置分页显示
		List<Quest> list=questMapper.schQuest(quest);//查询问卷
		Page<Quest> page = (Page<Quest>)list;//为了设置总记录数先类型强转
		quest.setDatasize(page.getTotal());//设置总页
		return list;//返回list
	}
	
	/**
	 * Quest的主键查询方法
	 * @param id 主键值
	 * @return 返回Quest实体
	 */
	@Override
	public Quest schQuestById(Integer id) {
		return questMapper.schQuestById(id);
	}
	
	/**
	 * Quest的添加方法
	 * @param quest 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	@Override
	public Integer addQuest(Quest quest) {
		return questMapper.addQuest(quest);
	}

	/**
	 * Quest的修改方法
	 * @param quest 承载数据的Bean
	 */
	@Override
	public void updQuest(Quest quest) {
		questMapper.updQuest(quest);
	}

	/**
	 * Quest的单记录删除方法
	 * @param id 要删除的主键值
	 */
	@Override
	public void delQuest(Integer id) {
		questMapper.delQuest(id);
	}

	/**
	 * Quest的批量删除方法
	 * @param ids 主键值的数组
	 */
	@Override
	public void delQuest(Integer[] ids) {
		questMapper.delQuests(ids);
	}


	@Autowired
	private QuestTopicMapper questTopicMapper;


	/***************** QuestTopic方法组 *****************/
	/**
	 * QuestTopic的条件查询方法
	 * @param questTopic 承载查询条件的Bean
	 * @return 返回QuestTopic的集合
	 */
	@Override
	public List<QuestTopic> schQuestTopic(QuestTopic questTopic) {
		PageHelper.startPage(questTopic.getPage(), questTopic.getPagesize());//设置分页显示
		List<QuestTopic> list=questTopicMapper.schQuestTopic(questTopic);//查询题目
		Page<QuestTopic> page = (Page<QuestTopic>)list;//为了设置总记录数先类型强转
		questTopic.setDatasize(page.getTotal());//设置总页
		return list;//返回list
	}
	
	/**
	 * QuestTopic的主键查询方法
	 * @param id 主键值
	 * @return 返回QuestTopic实体
	 */
	@Override
	public QuestTopic schQuestTopicById(Integer id) {
		return questTopicMapper.schQuestTopicById(id);
	}
	
	/**
	 * QuestTopic的添加方法
	 * @param questTopic 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	@Override
	public Integer addQuestTopic(QuestTopic questTopic) {
		return questTopicMapper.addQuestTopic(questTopic);
	}

	/**
	 * QuestTopic的修改方法
	 * @param questTopic 承载数据的Bean
	 */
	@Override
	public void updQuestTopic(QuestTopic questTopic) {
		questTopicMapper.updQuestTopic(questTopic);
	}

	/**
	 * QuestTopic的单记录删除方法
	 * @param id 要删除的主键值
	 */
	@Override
	public void delQuestTopic(Integer id) {
		questTopicMapper.delQuestTopic(id);
	}

	/**
	 * QuestTopic的批量删除方法
	 * @param ids 主键值的数组
	 */
	@Override
	public void delQuestTopic(Integer[] ids) {
		questTopicMapper.delQuestTopics(ids);
	}


	@Autowired
	private TrainBillMapper trainBillMapper;


	/***************** TrainBill方法组 *****************/
	/**
	 * TrainBill的条件查询方法
	 * @param trainBill 承载查询条件的Bean
	 * @return 返回TrainBill的集合
	 */
	@Override
	public List<TrainBill> schTrainBill(TrainBill trainBill) {
		PageHelper.startPage(trainBill.getPage(), trainBill.getPagesize());//设置分页显示
		List<TrainBill> list=trainBillMapper.schTrainBill(trainBill);//查询工单
		Page<TrainBill> page = (Page<TrainBill>)list;//为了设置总记录数先类型强转
		trainBill.setDatasize(page.getTotal());//设置总页
		return list;//返回list
	}
	/**
	 * TrainBill的条件查询方法（无分页）
	 * @param trainBill 承载查询条件的Bean
	 * @return 返回TrainBill的集合
	 */
	@Override
	public List<TrainBill> schTrainBillList(TrainBill trainBill) {
		List<TrainBill> list=trainBillMapper.schTrainBill(trainBill);//查询工单
		return list;
	}
	
	/**
	 * TrainBill的主键查询方法
	 * @param id 主键值
	 * @return 返回TrainBill实体
	 */
	@Override
	public TrainBill schTrainBillById(Integer id) {
		return trainBillMapper.schTrainBillById(id);
	}
	
	/**
	 * TrainBill的添加方法
	 * @param trainBill 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	@Override
	public Integer addTrainBill(TrainBill trainBill) {
		return trainBillMapper.addTrainBill(trainBill);
	}

	/**
	 * TrainBill的修改方法
	 * @param trainBill 承载数据的Bean
	 */
	@Override
	public void updTrainBill(TrainBill trainBill) {
		trainBillMapper.updTrainBill(trainBill);
	}

	/**
	 * TrainBill的单记录删除方法
	 * @param id 要删除的主键值
	 */
	@Override
	public void delTrainBill(Integer id) {
		trainBillMapper.delTrainBill(id);
	}

	/**
	 * TrainBill的批量删除方法
	 * @param ids 主键值的数组
	 */
	@Override
	public void delTrainBill(Integer[] ids) {
		trainBillMapper.delTrainBills(ids);
	}


	@Autowired
	private TrainClientMapper trainClientMapper;


	/***************** TrainClient方法组 *****************/
	/**
	 * TrainClient的条件查询方法
	 * @param trainClient 承载查询条件的Bean
	 * @return 返回TrainClient的集合
	 */
	@Override
	public List<TrainClient> schTrainClient(TrainClient trainClient) {
		PageHelper.startPage(trainClient.getPage(), trainClient.getPagesize());//设置分页显示
		List<TrainClient> list=trainClientMapper.schTrainClient(trainClient);//查询客户信息
		Page<TrainClient> page = (Page<TrainClient>)list;//为了设置总记录数先类型强转
		trainClient.setDatasize(page.getTotal());//设置总页
		return list;//返回list
	}
	
	/**
	 * TrainClient的主键查询方法
	 * @param id 主键值
	 * @return 返回TrainClient实体
	 */
	@Override
	public TrainClient schTrainClientById(Integer id) {
		return trainClientMapper.schTrainClientById(id);
	}
	
	/**
	 * TrainClient的添加方法
	 * @param trainClient 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	@Override
	public Integer addTrainClient(TrainClient trainClient) {
		return trainClientMapper.addTrainClient(trainClient);
	}

	/**
	 * TrainClient的修改方法
	 * @param trainClient 承载数据的Bean
	 */
	@Override
	public void updTrainClient(TrainClient trainClient) {
		trainClientMapper.updTrainClient(trainClient);
	}

	/**
	 * TrainClient的单记录删除方法
	 * @param id 要删除的主键值
	 */
	@Override
	public void delTrainClient(Integer id) {
		trainClientMapper.delTrainClient(id);
	}

	/**
	 * TrainClient的批量删除方法
	 * @param ids 主键值的数组
	 */
	@Override
	public void delTrainClient(Integer[] ids) {
		trainClientMapper.delTrainClients(ids);
	}



}//end