package com.qtrmoon.practical.ctrl;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qtrmoon.common.BaseController;
import com.qtrmoon.common.OrderCache;
import com.qtrmoon.dialog.serdao.IDialogService;
import com.qtrmoon.manager.pojo.Person;
import com.qtrmoon.manager.serdao.IManagerService;
import com.qtrmoon.practical.pojo.TrainBill;
import com.qtrmoon.practical.pojo.TrainClient;
import com.qtrmoon.practical.serdao.IPracticalService;
import com.qtrmoon.sysmanage.Online;
import com.qtrmoon.sysmanage.SysConstant;
import com.qtrmoon.toolkit.DateTransfer;

/**
 * 客服/客户工单
 * 
 * @Class Name WorkOrderController
 */
@Controller
@RequestMapping("/practical/workorder")
public class WorkOrderController extends BaseController {

	@Autowired
	private IManagerService managerService;// 声明模块Service实例
	@Autowired
	private IPracticalService practicalService; // 声明模块Service实例
	@Autowired
	private IDialogService dialogService;// 声明模块Service实例

	/**
	 * 客户端查询客户信息
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/serchCusOrder.action")
	public void serchCusOrder(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		noCache(response);// 清空缓存
		Person person = (Person) request.getSession(true).getAttribute(
				SysConstant.CURRENT_USER);// 获取登陆人
		String profession = (String) request.getParameter("profession");// 获取行业
		String xh = person.getNumber();// 获取学号
		toPageJson(xh, profession, request, response);// 返回view数据
	}

	/**
	 * 服务端查询客户信息
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/serSearchCus.action")
	public void serSearchCus(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		noCache(response);// 清空缓存
		Person person = (Person) request.getSession(true).getAttribute(
				SysConstant.CURRENT_USER);// 获取登录人
		if (person != null) {
			String agentId = person.getAgentId();// 获取坐席号
			String fromNum = "";// 来电学号
			String profession = "";// 专业
			Map<String, String> map = getFromByTo(agentId);// 获取来电信息
			fromNum = map.get("fromNum");// 获取来电学号
			profession = map.get("profession")==null?"":map.get("profession");// 来电行业
			toPageJson(fromNum, profession, request, response);// 返回数据
		}
	}

	/**
	 * 返回界面数据
	 * 
	 * @param id
	 * @param profession
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	private void toPageJson(String xh, String profession,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		JSONObject obj = null;// json对象
		TrainClient trainClient = new TrainClient();// 客户信息
		if (xh != null && profession != null && !"".equals(xh)) {
			trainClient.setUserid(xh);// 设置学号
			trainClient.setProfession(profession);// 设置专业
			List<TrainClient> clientLis = practicalService
					.schTrainClient(trainClient);// 查询客户信息
			if (clientLis != null && clientLis.size() > 0
					&& clientLis.get(0) != null) {
				obj = clientLis.get(0).getJsonInDict();// 获取json对象
			}
		}
		JSONObject res = new JSONObject();// 创建json对象
		res.put("data", obj);// 填充对象
		res.put("timeStamp", getTimeStamp());// 填充时间
		Person person = (Person) request.getSession(true).getAttribute(
				SysConstant.CURRENT_USER);// 获取登陆人
		res.put("currentNum", person.getNumber());// 填充学号
		res.put("callTime", person.getCallTime());// 填充时间
		response.getWriter().print(res.toJSONString());// 返回数据
	}

	/**
	 * 获取时间戳
	 * @return
	 */
	private String getTimeStamp() {
		String time = DateTransfer.toString(new Date(), "yyyyMMddHHmmss");// 格式化
		return time;
	}

	/**
	 * 查询bill
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping(value = "/serchBill.action")
	public JSONObject serchBill(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		noCache(response);
		JSONObject res = new JSONObject(); // 声明页面返回Json
		Person person = (Person) request.getSession(true).getAttribute(
				SysConstant.CURRENT_USER);// 获取登陆人
		
		String taskId = request.getParameter("taskId");
		String type = request.getParameter("type");
		
		if (person != null) {
			String toNum = person.getNumber();// 获取去电学号
			String agentId = person.getAgentId();// 获取坐席号
			Map<String, String> map = getFromByTo(agentId);// 获取来电map
			String fromNum = map.get("fromNum");// 获取来电学号
			JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
			if (fromNum != null && !"".equals(fromNum)) {
				TrainBill trainBill = new TrainBill();// 工单
				trainBill.setUserid(toNum);// 设置学号
				trainBill.setDialing(fromNum);// 设置拨打人
				if(!StringUtils.isEmpty(taskId)){
					trainBill.setTaskId(Integer.valueOf(taskId));
				}
				if(!StringUtils.isEmpty(type)){
					trainBill.setType(type);
				}
				trainBill.setOrderCol("createTime DESC");
				List<TrainBill> list = practicalService
						.schTrainBillList(trainBill); // 调用Service查询数据
				JSONObject obj; // 声明用于构造Bean的Json对象
				for (TrainBill u : list) { // 循环查询的数据集构造Json数据集
					obj = u.getJsonInDict(); // 将Bean转换为Json对象
					datas.add(obj); // 添加Json对象到Json集合
				}
			}
			/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
			res.put("rows", datas); // 设置数据集
		}
		return res;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/serchBillByStuNum.action")
	public void serchBillByStuNum(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		noCache(response);
		String stuNum = request.getParameter("stuNum");// 获取学号
		String taskId = request.getParameter("taskId");// 获取任务id
		String type = request.getParameter("type");// 获取类型
		if (stuNum != null && taskId != null) {
			JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
			TrainBill trainBill = new TrainBill();// 创建工单
			trainBill.setTaskId(Integer.valueOf(taskId));// 设置任务id
			trainBill.setUserid(stuNum);// 设置学号
			trainBill.setType(type);//设置类型
			List<TrainBill> list = practicalService.schTrainBillList(trainBill); // 调用Service查询数据
			JSONObject obj; // 声明用于构造Bean的Json对象
			for (TrainBill u : list) { // 循环查询的数据集构造Json数据集
				obj = u.getJsonInDict(); // 将Bean转换为Json对象
				datas.add(obj); // 添加Json对象到Json集合
			}
			/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
			JSONObject res = new JSONObject(); // 声明页面返回Json
			res.put("rows", datas); // 设置数据集
			response.getWriter().print(res.toJSONString()); // 输出到页面
		}
	}

	@ResponseBody
	@RequestMapping(value = "/serchBillById")
	public JSONObject serchBillById(TrainBill trainBill, HttpServletResponse response)
			throws IOException {
		noCache(response);//清空缓存
		JSONObject res = new JSONObject();//创建json对象
		if (trainBill != null) {
			Integer id = trainBill.getId();//获取工单id
			TrainBill result = practicalService.schTrainBillById(id);//查询工单
			if (result != null) {
				res = result.getJsonInDict();//获取json串
			}
		}
		return res;
	}

	/**
	 *查询工单数量 
	 * */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/serchBillSzie.action")
	public void serchBillSzie(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		noCache(response);
		String stuNum = request.getParameter("stuNum");// 获取学号
		String taskId = request.getParameter("taskId");// 获取任务id
		String type = request.getParameter("type");// 获取类型
		if (stuNum != null && taskId != null) {
			TrainBill trainBill = new TrainBill();// 创建工单
			trainBill.setTaskId(Integer.valueOf(taskId));// 设置任务id
			trainBill.setUserid(stuNum);// 设置学号
			trainBill.setType(type);//设置类型
			List<TrainBill> list = practicalService.schTrainBillList(trainBill); // 调用Service查询数据
			JSONObject res = new JSONObject(); // 声明页面返回Json
			res.put("size", list.size()); // 设置数据集
			response.getWriter().print(res.toJSONString()); // 输出到页面
		}
	}
	
	/**
	 * 保存数据（客户端客户信息）
	 * 
	 * @param trainClient
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/saveClient.action")
	public void saveClient(TrainClient trainClient, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		noCache(response);//清空缓存
		JSONObject obj = new JSONObject();//创建json对象
		try {
			if (trainClient != null) {
				if (trainClient.getId() != null) {
					TrainClient train = practicalService
							.schTrainClientById(trainClient.getId());//查询客户信息
					trainClient.setCusLevel(train.getCusLevel());//设置客户等级
					trainClient.setCusNum(train.getCusNum());//设置客户号码
					// trainClient.setBelong(train.getBelong());//设置归属地
					// 更新数据
					practicalService.updTrainClient(trainClient);
					obj.put("msg", "更新成功");//返回页面信息
				} else {
					// 添加数据
					Person person = (Person) request.getSession(true)
							.getAttribute(SysConstant.CURRENT_USER);//获取登陆人
					trainClient.setUserid(person.getNumber());
					Date now = new Date();//创建时间
					String cusNum = Long.toString(now.getTime());//客户编号
					trainClient.setCusNum(cusNum);//设置客户编号
					practicalService.addTrainClient(trainClient);//添加客户信息
					obj.put("msg", "添加成功");//返回页面信息
					obj.put("id", trainClient.getId());//返回页面信息
				}
				obj.put("success", true);//返回页面信息
			}
		} catch (Exception e) {
			e.printStackTrace();//打印异常
			obj.put("success", false);//返回页面信息
			obj.put("msg", "保存失败");//返回页面信息
		} finally {
			response.getWriter().print(obj.toJSONString());//返回页面信息
		}
	}

	/**
	 * 保存数据（客户端客户信息） 
	 * 
	 * @param trainClient
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/changeLevel.action")
	public void changeLevel(TrainClient trainClient,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		noCache(response);//清空缓存
		JSONObject obj = new JSONObject();//创建json对象
		try {
			if (trainClient != null) {
				if (trainClient.getId() != null) {
					TrainClient train = practicalService
							.schTrainClientById(trainClient.getId());//查询客户信息
					train.setCusLevel(trainClient.getCusLevel());//设置客户等级
					practicalService.updTrainClient(train);// 更新数据
					obj.put("success", true);//返回页面信息
					obj.put("msg", "更新成功");//返回页面信息
				} else {
					obj.put("success", false);//返回页面信息
					obj.put("msg", "没有ID");//返回页面信息
				}
			} else {
				obj.put("success", false);//返回页面信息
				obj.put("msg", "参数错误");//返回页面信息
			}
		} catch (Exception e) {
			e.printStackTrace();//打印异常
			obj.put("success", false);//返回页面信息
			obj.put("msg", "保存失败");//返回页面信息
		} finally {
			response.getWriter().print(obj.toJSONString());//返回页面信息
		}
	}

	/**
	 * 保存
	 * @param trainBill
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings({ "unchecked", "finally" })
	@ResponseBody
	@RequestMapping(value = "/saveBill.action")
	public JSONObject saveBill(TrainBill trainBill, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		noCache(response);//清空
		JSONObject obj = new JSONObject();//创建json对象
		try {
			if (trainBill != null) {
				if (trainBill.getId() != null) {
					practicalService.updTrainBill(trainBill);//更新工单信息
					obj.put("success", true);//返回页面信息
					obj.put("msg", "更新成功");//返回页面信息
				} else {
					Person person = (Person) request.getSession(true)
							.getAttribute(SysConstant.CURRENT_USER);//获取登陆人
					trainBill.setUserid(person.getNumber());//设置学号
					String agentId = person.getAgentId();//设置坐席号
					Map<String, String> map = getFromByTo(agentId);//获取来电信息
					String fromNum = map.get("fromNum");//获取来电学号
					trainBill.setDialing(fromNum);//设置拨打人
					trainBill.setCreateTime(new Date());//设置时间
					practicalService.addTrainBill(trainBill);//添加工单
					obj.put("success", true);//返回页面信息
					obj.put("msg", "添加成功");//返回页面信息
					obj.put("data", trainBill);//返回页面信息
				}
			}
		} catch (Exception e) {
			e.printStackTrace();//打印异常
			obj.put("success", false);//返回页面信息
			obj.put("msg", "保存失败");//返回页面信息
		} finally {
			return obj;
		}
	}
	
	/**
	 * 获取来电信息
	 * @param agentId
	 * @return
	 */
	public Map<String, String> getFromByTo(String agentId) {
		String profession = "";//行业
		Map<String, Person> onlinePerson = Online.getOnlinePerson();//获取在线人
		Map<String, String> map = new HashMap<String, String>();//创建map
		Set<Entry<String, Person>> entrySet = onlinePerson.entrySet();//获取在线人
		for (Entry<String, Person> entry : entrySet) {
			Person person1 = entry.getValue();//获取人
			if (person1.getAgentId() != null && person1.getAgentId().equals(agentId)) {//本人
				List<String> to = person1.getTo();//获取去电
				profession = person1.getProfession();//获取行业
				if (to != null && to.size() >= 2) {
					map.put("fromNum", to.get(1));//填充来电号码
				}
				map.put("profession", profession);//填充行业
			}
		}

		return map;//返回map
	}

	/**
	 * 打电话前
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/preCall.action")
	public void preCall(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		noCache(response);//清空缓存
		JSONObject obj = new JSONObject();//创建json对象
		try {
			obj.put("success", true);//返回页面信息
			obj.put("msg", "拨号中...");//返回页面信息
		} catch (Exception e) {
			e.printStackTrace();//打印异常
			obj.put("success", false);//返回页面信息
			obj.put("msg", "拨号异常");//返回页面信息
		} finally {
			response.getWriter().print(obj.toJSONString());//返回页面信息
		}
	}

	/**
	 * 呼出前准备 
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/preHungUp.action")
	public void preHungUp(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		noCache(response);//清空缓存
		JSONObject obj = new JSONObject();//创建json对象
		try {
			Person person = (Person) request.getSession(true).getAttribute(
					SysConstant.CURRENT_USER);//获取登陆人
			String number = person.getAgentId();//获取坐席号
			boolean flg = OrderCache.removeByTo(number);//删除数据
			if (flg) {
				obj.put("success", true);//返回页面信息
				obj.put("msg", "挂断成功");//返回页面信息
			} else {
				obj.put("success", false);//返回页面信息
				obj.put("msg", "挂断失败");//返回页面信息
			}
		} catch (Exception e) {
			e.printStackTrace();//打印异常
			obj.put("success", false);//返回页面信息
			obj.put("msg", "预处理失败");//返回页面信息
		} finally {
			response.getWriter().print(obj.toJSONString());//返回页面信息
		}
	}

	/**
	 * JSP页面跳转
	 * 
	 * @param page
	 *            jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value = "/page.action")
	public String goPage(String page) { // 放过本Ctrl的页面，不合法的跳转到error页。
		if ("index".equals(page) || "workorder_cus".equals(page)
				|| "workorder_ser".equals(page)) {
			return "/practical/" + page;//返回相应页面
		} else {
			return "/error";//返回相应页面
		}
	}
}
