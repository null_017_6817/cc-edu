package com.qtrmoon.slwldx.serdao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.qtrmoon.slwldx.pojo.WlddxxVo;
/** 实体映射 */
@Component
public interface WlddxxVoMapper {
	/**
	 * Wlxx的条件查询方法
	 * @param wlxx 承载查询条件
	 * @return 返回Wlxx的集合
	 */
	public List<WlddxxVo> schWlxx(WlddxxVo wlddxxVo);
}
