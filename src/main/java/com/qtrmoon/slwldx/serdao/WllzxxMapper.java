package com.qtrmoon.slwldx.serdao;

import java.util.List;
import org.springframework.stereotype.Component;

import com.qtrmoon.slwldx.pojo.Wllzxx;
/** 物流流转信息实体映射 */
@Component
public interface WllzxxMapper {
	/**
	 * Wllzxx的条件查询方法
	 * @param wllzxx 承载查询条件
	 * @return 返回Wllzxx的集合
	 */
	public List<Wllzxx> schWllzxx(Wllzxx wllzxx);
	
	/**
	 * Wllzxx的主键查询方法
	 * @param id 主键值
	 * @return 返回Wllzxx实体
	 */
	public Wllzxx schWllzxxById(Integer id);
	
	/**
	 * Wllzxx的添加方法
	 * @param wllzxx 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addWllzxx(Wllzxx wllzxx);
	
	/**
	 * Wllzxx的修改方法
	 * @param wllzxx 承载数据的Bean
	 */
	public void updWllzxx(Wllzxx wllzxx);
	
	/**
	 * Wllzxx的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delWllzxx(Integer id);
	
	/**
	 * Wllzxx的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delWllzxxs(Integer[] ids);
}
