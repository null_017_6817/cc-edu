package com.qtrmoon.slwldx.serdao;
import java.util.List;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qtrmoon.slwldx.pojo.*;

/** slwldx服务实现类 */
@Service("slwldxService")
public class SlwldxServiceImpl implements ISlwldxService {
	@Autowired
	private HbxxMapper hbxxMapper;


	/***************** Hbxx方法组 *****************/
	/**
	 * Hbxx的条件查询方法
	 * @param hbxx 承载查询条件的Bean
	 * @return 返回Hbxx的集合
	 */
	@Override
	public List<Hbxx> schHbxx(Hbxx hbxx) {
		List<Hbxx> list=hbxxMapper.schHbxx(hbxx);//查询列表
		return list;
	}
	
	/**
	 * Hbxx的主键查询方法
	 * @param id 主键值
	 * @return 返回Hbxx实体
	 */
	@Override
	public Hbxx schHbxxById(Integer id) {
		return hbxxMapper.schHbxxById(id);
	}
	
	/**
	 * Hbxx的添加方法
	 * @param hbxx 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	@Override
	public Integer addHbxx(Hbxx hbxx) {
		return hbxxMapper.addHbxx(hbxx);
	}

	/**
	 * Hbxx的修改方法
	 * @param hbxx 承载数据的Bean
	 */
	@Override
	public void updHbxx(Hbxx hbxx) {
		hbxxMapper.updHbxx(hbxx);
	}

	/**
	 * Hbxx的单记录删除方法
	 * @param id 要删除的主键值
	 */
	@Override
	public void delHbxx(Integer id) {
		hbxxMapper.delHbxx(id);
	}

	/**
	 * Hbxx的批量删除方法
	 * @param ids 主键值的数组
	 */
	@Override
	public void delHbxx(Integer[] ids) {
		hbxxMapper.delHbxxs(ids);
	}


	@Autowired
	private JpddxxMapper jpddxxMapper;


	/***************** Jpddxx方法组 *****************/
	/**
	 * Jpddxx的条件查询方法
	 * @param jpddxx 承载查询条件的Bean
	 * @return 返回Jpddxx的集合
	 */
	@Override
	public List<Jpddxx> schJpddxx(Jpddxx jpddxx) {
		PageHelper.startPage(jpddxx.getPage(), jpddxx.getPagesize());//设置分页显示
		List<Jpddxx> list=jpddxxMapper.schJpddxx(jpddxx);//查询列表
		Page<Jpddxx> page = (Page<Jpddxx>)list;//为了设置总记录数先类型强转
		jpddxx.setDatasize(page.getTotal());//设置列表长度
		return list;
	}
	
	/**
	 * Jpddxx的主键查询方法
	 * @param id 主键值
	 * @return 返回Jpddxx实体
	 */
	@Override
	public Jpddxx schJpddxxById(Integer id) {
		return jpddxxMapper.schJpddxxById(id);
	}
	
	/**
	 * Jpddxx的添加方法
	 * @param jpddxx 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	@Override
	public Integer addJpddxx(Jpddxx jpddxx) {
		return jpddxxMapper.addJpddxx(jpddxx);
	}

	/**
	 * Jpddxx的修改方法
	 * @param jpddxx 承载数据的Bean
	 */
	@Override
	public void updJpddxx(Jpddxx jpddxx) {
		jpddxxMapper.updJpddxx(jpddxx);
	}

	/**
	 * Jpddxx的单记录删除方法
	 * @param id 要删除的主键值
	 */
	@Override
	public void delJpddxx(Integer id) {
		jpddxxMapper.delJpddxx(id);
	}

	/**
	 * Jpddxx的批量删除方法
	 * @param ids 主键值的数组
	 */
	@Override
	public void delJpddxx(Integer[] ids) {
		jpddxxMapper.delJpddxxs(ids);
	}


	@Autowired
	private WlzfxxMapper wlzfxxMapper;


	/***************** Wlzfxx方法组 *****************/
	/**
	 * Wlzfxx的条件查询方法
	 * @param wlzfxx 承载查询条件的Bean
	 * @return 返回Wlzfxx的集合
	 */
	@Override
	public List<Wlzfxx> schWlzfxx(Wlzfxx wlzfxx) {
		PageHelper.startPage(wlzfxx.getPage(), wlzfxx.getPagesize());//设置分页显示
		List<Wlzfxx> list=wlzfxxMapper.schWlzfxx(wlzfxx);//查询列表
		Page<Wlzfxx> page = (Page<Wlzfxx>)list;//为了设置总记录数先类型强转
		wlzfxx.setDatasize(page.getTotal());//设置列表长度
		return list;
	}
	
	/**
	 * Wlzfxx的主键查询方法
	 * @param id 主键值
	 * @return 返回Wlzfxx实体
	 */
	@Override
	public Wlzfxx schWlzfxxById(Integer id) {
		return wlzfxxMapper.schWlzfxxById(id);
	}
	
	/**
	 * Wlzfxx的添加方法
	 * @param wlzfxx 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	@Override
	public Integer addWlzfxx(Wlzfxx wlzfxx) {
		return wlzfxxMapper.addWlzfxx(wlzfxx);
	}

	/**
	 * Wlzfxx的修改方法
	 * @param wlzfxx 承载数据的Bean
	 */
	@Override
	public void updWlzfxx(Wlzfxx wlzfxx) {
		wlzfxxMapper.updWlzfxx(wlzfxx);
	}

	/**
	 * Wlzfxx的单记录删除方法
	 * @param id 要删除的主键值
	 */
	@Override
	public void delWlzfxx(Integer id) {
		wlzfxxMapper.delWlzfxx(id);
	}

	/**
	 * Wlzfxx的批量删除方法
	 * @param ids 主键值的数组
	 */
	@Override
	public void delWlzfxx(Integer[] ids) {
		wlzfxxMapper.delWlzfxxs(ids);
	}


	@Autowired
	private WlddxxMapper wlddxxMapper;


	/***************** Wlddxx方法组 *****************/
	/**
	 * Wlddxx的条件查询方法
	 * @param wlddxx 承载查询条件的Bean
	 * @return 返回Wlddxx的集合
	 */
	@Override
	public List<Wlddxx> schWlddxx(Wlddxx wlddxx) {
		PageHelper.startPage(wlddxx.getPage(), wlddxx.getPagesize());//设置分页显示
		List<Wlddxx> list=wlddxxMapper.schWlddxx(wlddxx);//查询列表
		Page<Wlddxx> page = (Page<Wlddxx>)list;//为了设置总记录数先类型强转
		wlddxx.setDatasize(page.getTotal());//设置列表长度
		return list;
	}
	
	/**
	 * Wlddxx的主键查询方法
	 * @param id 主键值
	 * @return 返回Wlddxx实体
	 */
	@Override
	public Wlddxx schWlddxxById(Integer id) {
		return wlddxxMapper.schWlddxxById(id);
	}
	
	/**
	 * Wlddxx的添加方法
	 * @param wlddxx 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	@Override
	public Integer addWlddxx(Wlddxx wlddxx) {
		return wlddxxMapper.addWlddxx(wlddxx);
	}

	/**
	 * Wlddxx的修改方法
	 * @param wlddxx 承载数据的Bean
	 */
	@Override
	public void updWlddxx(Wlddxx wlddxx) {
		wlddxxMapper.updWlddxx(wlddxx);
	}

	/**
	 * Wlddxx的单记录删除方法
	 * @param id 要删除的主键值
	 */
	@Override
	public void delWlddxx(Integer id) {
		wlddxxMapper.delWlddxx(id);
	}

	/**
	 * Wlddxx的批量删除方法
	 * @param ids 主键值的数组
	 */
	@Override
	public void delWlddxx(Integer[] ids) {
		wlddxxMapper.delWlddxxs(ids);
	}


	@Autowired
	private WllzxxMapper wllzxxMapper;


	/***************** Wllzxx方法组 *****************/
	/**
	 * Wllzxx的条件查询方法
	 * @param wllzxx 承载查询条件的Bean
	 * @return 返回Wllzxx的集合
	 */
	@Override
	public List<Wllzxx> schWllzxx(Wllzxx wllzxx) {
		PageHelper.startPage(wllzxx.getPage(), wllzxx.getPagesize());//设置分页显示
		List<Wllzxx> list=wllzxxMapper.schWllzxx(wllzxx);//查询列表
		Page<Wllzxx> page = (Page<Wllzxx>)list;//为了设置总记录数先类型强转
		wllzxx.setDatasize(page.getTotal());//设置列表长度
		return list;
	}
	
	/**
	 * Wllzxx的主键查询方法
	 * @param id 主键值
	 * @return 返回Wllzxx实体
	 */
	@Override
	public Wllzxx schWllzxxById(Integer id) {
		return wllzxxMapper.schWllzxxById(id);
	}
	
	/**
	 * Wllzxx的添加方法
	 * @param wllzxx 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	@Override
	public Integer addWllzxx(Wllzxx wllzxx) {
		return wllzxxMapper.addWllzxx(wllzxx);
	}

	/**
	 * Wllzxx的修改方法
	 * @param wllzxx 承载数据的Bean
	 */
	@Override
	public void updWllzxx(Wllzxx wllzxx) {
		wllzxxMapper.updWllzxx(wllzxx);
	}

	/**
	 * Wllzxx的单记录删除方法
	 * @param id 要删除的主键值
	 */
	@Override
	public void delWllzxx(Integer id) {
		wllzxxMapper.delWllzxx(id);
	}

	/**
	 * Wllzxx的批量删除方法
	 * @param ids 主键值的数组
	 */
	@Override
	public void delWllzxx(Integer[] ids) {
		wllzxxMapper.delWllzxxs(ids);
	}


	@Autowired
	private TcxxMapper tcxxMapper;


	/***************** Tcxx方法组 *****************/
	/**
	 * Tcxx的条件查询方法
	 * @param tcxx 承载查询条件的Bean
	 * @return 返回Tcxx的集合
	 */
	@Override
	public List<Tcxx> schTcxx(Tcxx tcxx) {
		//PageHelper.startPage(tcxx.getPage(), tcxx.getPagesize());//设置分页显示
		List<Tcxx> list=tcxxMapper.schTcxx(tcxx);//查询列表
		//Page<Tcxx> page = (Page<Tcxx>)list;//为了设置总记录数先类型强转
		//tcxx.setDatasize(page.getTotal());
		return list;
	}
	
	/**
	 * Tcxx的主键查询方法
	 * @param id 主键值
	 * @return 返回Tcxx实体
	 */
	@Override
	public Tcxx schTcxxById(Integer id) {
		return tcxxMapper.schTcxxById(id);
	}
	
	/**
	 * Tcxx的添加方法
	 * @param tcxx 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	@Override
	public Integer addTcxx(Tcxx tcxx) {
		return tcxxMapper.addTcxx(tcxx);
	}

	/**
	 * Tcxx的修改方法
	 * @param tcxx 承载数据的Bean
	 */
	@Override
	public void updTcxx(Tcxx tcxx) {
		tcxxMapper.updTcxx(tcxx);
	}

	/**
	 * Tcxx的单记录删除方法
	 * @param id 要删除的主键值
	 */
	@Override
	public void delTcxx(Integer id) {
		tcxxMapper.delTcxx(id);
	}

	/**
	 * Tcxx的批量删除方法
	 * @param ids 主键值的数组
	 */
	@Override
	public void delTcxx(Integer[] ids) {
		tcxxMapper.delTcxxs(ids);
	}


	@Autowired
	private GrtcylxxMapper grtcylxxMapper;


	/***************** Grtcylxx方法组 *****************/
	/**
	 * Grtcylxx的条件查询方法
	 * @param grtcylxx 承载查询条件的Bean
	 * @return 返回Grtcylxx的集合
	 */
	@Override
	public List<Grtcylxx> schGrtcylxx(Grtcylxx grtcylxx) {
		PageHelper.startPage(grtcylxx.getPage(), grtcylxx.getPagesize());//设置分页显示
		List<Grtcylxx> list=grtcylxxMapper.schGrtcylxx(grtcylxx);//查询列表
		Page<Grtcylxx> page = (Page<Grtcylxx>)list;//为了设置总记录数先类型强转
		grtcylxx.setDatasize(page.getTotal());//设置列表长度
		return list;
	}
	
	/**
	 * Grtcylxx的主键查询方法
	 * @param id 主键值
	 * @return 返回Grtcylxx实体
	 */
	@Override
	public Grtcylxx schGrtcylxxById(Integer id) {
		return grtcylxxMapper.schGrtcylxxById(id);
	}
	
	/**
	 * Grtcylxx的添加方法
	 * @param grtcylxx 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	@Override
	public Integer addGrtcylxx(Grtcylxx grtcylxx) {
		return grtcylxxMapper.addGrtcylxx(grtcylxx);
	}

	/**
	 * Grtcylxx的修改方法
	 * @param grtcylxx 承载数据的Bean
	 */
	@Override
	public void updGrtcylxx(Grtcylxx grtcylxx) {
		grtcylxxMapper.updGrtcylxx(grtcylxx);
	}

	/**
	 * Grtcylxx的单记录删除方法
	 * @param id 要删除的主键值
	 */
	@Override
	public void delGrtcylxx(Integer id) {
		grtcylxxMapper.delGrtcylxx(id);
	}

	/**
	 * Grtcylxx的批量删除方法
	 * @param ids 主键值的数组
	 */
	@Override
	public void delGrtcylxx(Integer[] ids) {
		grtcylxxMapper.delGrtcylxxs(ids);
	}

	@Autowired
	private WlddxxVoMapper wlddxxVoMapper;
	/***************** WlxxVo方法组 *****************/
	/**
	 * WlxxVo的条件查询方法
	 * @param WlxxVo 承载查询条件的Bean
	 * @return 返回WlxxVo的集合
	 */
	public List<WlddxxVo> schWlxx(WlddxxVo wlddxxVo) {
		PageHelper.startPage(wlddxxVo.getPage(), wlddxxVo.getPagesize());//设置分页显示
		List<WlddxxVo> list=wlddxxVoMapper.schWlxx(wlddxxVo);//查询列表
		Page<WlddxxVo> page = (Page<WlddxxVo>)list;//为了设置总记录数先类型强转
		wlddxxVo.setDatasize(page.getTotal());//设置列表长度
		return list;
	}

	/**
	 * 查询tcmc最大和最小
	 */
	@Override
	public List<Tcxx> schMinTcmc(Tcxx tcxx) {
		return tcxxMapper.schMinTcmc(tcxx);
	}

	/**
	 * 添加乘客信息
	 */
	@Override
	public void saveThisP(AddFare addFare) {
		hbxxMapper.saveThisP(addFare);
	}
	
}//end