package com.qtrmoon.slwldx.serdao;

import java.util.List;
import org.springframework.stereotype.Component;

import com.qtrmoon.slwldx.pojo.Grtcylxx;
/** 个人套餐用量信息实体映射 */
@Component
public interface GrtcylxxMapper {
	/**
	 * Grtcylxx的条件查询方法
	 * @param grtcylxx 承载查询条件
	 * @return 返回Grtcylxx的集合
	 */
	public List<Grtcylxx> schGrtcylxx(Grtcylxx grtcylxx);
	
	/**
	 * Grtcylxx的主键查询方法
	 * @param id 主键值
	 * @return 返回Grtcylxx实体
	 */
	public Grtcylxx schGrtcylxxById(Integer id);
	
	/**
	 * Grtcylxx的添加方法
	 * @param grtcylxx 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addGrtcylxx(Grtcylxx grtcylxx);
	
	/**
	 * Grtcylxx的修改方法
	 * @param grtcylxx 承载数据的Bean
	 */
	public void updGrtcylxx(Grtcylxx grtcylxx);
	
	/**
	 * Grtcylxx的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delGrtcylxx(Integer id);
	
	/**
	 * Grtcylxx的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delGrtcylxxs(Integer[] ids);
}
