package com.qtrmoon.slwldx.serdao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import com.qtrmoon.slwldx.pojo.AddFare;
import com.qtrmoon.slwldx.pojo.Hbxx;
/** 航班信息实体映射 */
@Component
public interface HbxxMapper {
	/**
	 * Hbxx的条件查询方法
	 * @param hbxx 承载查询条件
	 * @return 返回Hbxx的集合
	 */
	public List<Hbxx> schHbxx(Hbxx hbxx);
	
	/**
	 * Hbxx的主键查询方法
	 * @param id 主键值
	 * @return 返回Hbxx实体
	 */
	public Hbxx schHbxxById(Integer id);
	
	/**
	 * Hbxx的添加方法
	 * @param hbxx 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addHbxx(Hbxx hbxx);
	
	/**
	 * Hbxx的修改方法
	 * @param hbxx 承载数据的Bean
	 */
	public void updHbxx(Hbxx hbxx);
	
	/**
	 * Hbxx的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delHbxx(Integer id);
	
	/**
	 * Hbxx的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delHbxxs(Integer[] ids);

	/**
	 * 添加乘客信息
	 * 2017年10月30日
	 * By 蔚昕
	 * @param addFare
	 */
	public void saveThisP(@Param("addFare") AddFare addFare);
}
