package com.qtrmoon.slwldx.serdao;
import java.util.List;

import com.qtrmoon.slwldx.pojo.AddFare;
import com.qtrmoon.slwldx.pojo.Grtcylxx;
import com.qtrmoon.slwldx.pojo.Hbxx;
import com.qtrmoon.slwldx.pojo.Jpddxx;
import com.qtrmoon.slwldx.pojo.Tcxx;
import com.qtrmoon.slwldx.pojo.Wlddxx;
import com.qtrmoon.slwldx.pojo.WlddxxVo;
import com.qtrmoon.slwldx.pojo.Wllzxx;
import com.qtrmoon.slwldx.pojo.Wlzfxx;
/** slwldx服务接口 */
public interface ISlwldxService {
	
	/***************** Hbxx方法组 *****************/
	/**
	 * Hbxx的条件查询方法
	 * @param hbxx 承载查询条件的Bean
	 * @return 返回Hbxx的集合
	 */
	public List<Hbxx> schHbxx(Hbxx hbxx);
	
	/**
	 * Hbxx的主键查询方法
	 * @param id 主键值
	 * @return 返回Hbxx实体
	 */
	public Hbxx schHbxxById(Integer id);
	
	/**
	 * Hbxx的添加方法
	 * @param hbxx 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addHbxx(Hbxx hbxx);
	
	/**
	 * Hbxx的修改方法
	 * @param hbxx 承载数据的Bean
	 */
	public void updHbxx(Hbxx hbxx);
	
	/**
	 * Hbxx的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delHbxx(Integer id);
	
	/**
	 * Hbxx的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delHbxx(Integer[] ids);


	
	/***************** Jpddxx方法组 *****************/
	/**
	 * Jpddxx的条件查询方法
	 * @param jpddxx 承载查询条件的Bean
	 * @return 返回Jpddxx的集合
	 */
	public List<Jpddxx> schJpddxx(Jpddxx jpddxx);
	
	/**
	 * Jpddxx的主键查询方法
	 * @param id 主键值
	 * @return 返回Jpddxx实体
	 */
	public Jpddxx schJpddxxById(Integer id);
	
	/**
	 * Jpddxx的添加方法
	 * @param jpddxx 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addJpddxx(Jpddxx jpddxx);
	
	/**
	 * Jpddxx的修改方法
	 * @param jpddxx 承载数据的Bean
	 */
	public void updJpddxx(Jpddxx jpddxx);
	
	/**
	 * Jpddxx的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delJpddxx(Integer id);
	
	/**
	 * Jpddxx的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delJpddxx(Integer[] ids);


	
	/***************** Wlzfxx方法组 *****************/
	/**
	 * Wlzfxx的条件查询方法
	 * @param wlzfxx 承载查询条件的Bean
	 * @return 返回Wlzfxx的集合
	 */
	public List<Wlzfxx> schWlzfxx(Wlzfxx wlzfxx);
	
	/**
	 * Wlzfxx的主键查询方法
	 * @param id 主键值
	 * @return 返回Wlzfxx实体
	 */
	public Wlzfxx schWlzfxxById(Integer id);
	
	/**
	 * Wlzfxx的添加方法
	 * @param wlzfxx 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addWlzfxx(Wlzfxx wlzfxx);
	
	/**
	 * Wlzfxx的修改方法
	 * @param wlzfxx 承载数据的Bean
	 */
	public void updWlzfxx(Wlzfxx wlzfxx);
	
	/**
	 * Wlzfxx的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delWlzfxx(Integer id);
	
	/**
	 * Wlzfxx的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delWlzfxx(Integer[] ids);


	
	/***************** Wlddxx方法组 *****************/
	/**
	 * Wlddxx的条件查询方法
	 * @param wlddxx 承载查询条件的Bean
	 * @return 返回Wlddxx的集合
	 */
	public List<Wlddxx> schWlddxx(Wlddxx wlddxx);
	
	/**
	 * Wlddxx的主键查询方法
	 * @param id 主键值
	 * @return 返回Wlddxx实体
	 */
	public Wlddxx schWlddxxById(Integer id);
	
	/**
	 * Wlddxx的添加方法
	 * @param wlddxx 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addWlddxx(Wlddxx wlddxx);
	
	/**
	 * Wlddxx的修改方法
	 * @param wlddxx 承载数据的Bean
	 */
	public void updWlddxx(Wlddxx wlddxx);
	
	/**
	 * Wlddxx的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delWlddxx(Integer id);
	
	/**
	 * Wlddxx的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delWlddxx(Integer[] ids);


	
	/***************** Wllzxx方法组 *****************/
	/**
	 * Wllzxx的条件查询方法
	 * @param wllzxx 承载查询条件的Bean
	 * @return 返回Wllzxx的集合
	 */
	public List<Wllzxx> schWllzxx(Wllzxx wllzxx);
	
	/**
	 * Wllzxx的主键查询方法
	 * @param id 主键值
	 * @return 返回Wllzxx实体
	 */
	public Wllzxx schWllzxxById(Integer id);
	
	/**
	 * Wllzxx的添加方法
	 * @param wllzxx 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addWllzxx(Wllzxx wllzxx);
	
	/**
	 * Wllzxx的修改方法
	 * @param wllzxx 承载数据的Bean
	 */
	public void updWllzxx(Wllzxx wllzxx);
	
	/**
	 * Wllzxx的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delWllzxx(Integer id);
	
	/**
	 * Wllzxx的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delWllzxx(Integer[] ids);


	
	/***************** Tcxx方法组 *****************/
	/**
	 * Tcxx的条件查询方法
	 * @param tcxx 承载查询条件的Bean
	 * @return 返回Tcxx的集合
	 */
	public List<Tcxx> schTcxx(Tcxx tcxx);
	
	/**
	 * Tcxx的主键查询方法
	 * @param id 主键值
	 * @return 返回Tcxx实体
	 */
	public Tcxx schTcxxById(Integer id);
	
	/**
	 * Tcxx的添加方法
	 * @param tcxx 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addTcxx(Tcxx tcxx);
	
	/**
	 * Tcxx的修改方法
	 * @param tcxx 承载数据的Bean
	 */
	public void updTcxx(Tcxx tcxx);
	
	/**
	 * Tcxx的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delTcxx(Integer id);
	
	/**
	 * Tcxx的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delTcxx(Integer[] ids);


	
	/***************** Grtcylxx方法组 *****************/
	/**
	 * Grtcylxx的条件查询方法
	 * @param grtcylxx 承载查询条件的Bean
	 * @return 返回Grtcylxx的集合
	 */
	public List<Grtcylxx> schGrtcylxx(Grtcylxx grtcylxx);
	
	/**
	 * Grtcylxx的主键查询方法
	 * @param id 主键值
	 * @return 返回Grtcylxx实体
	 */
	public Grtcylxx schGrtcylxxById(Integer id);
	
	/**
	 * Grtcylxx的添加方法
	 * @param grtcylxx 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addGrtcylxx(Grtcylxx grtcylxx);
	
	/**
	 * Grtcylxx的修改方法
	 * @param grtcylxx 承载数据的Bean
	 */
	public void updGrtcylxx(Grtcylxx grtcylxx);
	
	/**
	 * Grtcylxx的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delGrtcylxx(Integer id);
	
	/**
	 * Grtcylxx的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delGrtcylxx(Integer[] ids);

	/***************** Wlxx方法组 *****************/
	/**
	 * Wlxx的条件查询方法
	 * @param wlxx 承载查询条件的Bean
	 * @return 返回Wlxx的集合
	 */
	public List<WlddxxVo> schWlxx(WlddxxVo wlddxxVo);

	/**
	 * 查询tcmc最小和最大的
	 * 2017年10月19日
	 * By 蔚昕
	 * @param tcxx 
	 */
	public List<Tcxx> schMinTcmc(Tcxx tcxx);

	/**
	 * 添加乘客信息
	 * 2017年10月30日
	 * By 蔚昕
	 * @param addFare
	 */
	public void saveThisP(AddFare addFare);
}//end