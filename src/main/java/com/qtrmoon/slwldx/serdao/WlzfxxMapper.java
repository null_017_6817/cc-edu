package com.qtrmoon.slwldx.serdao;

import java.util.List;
import org.springframework.stereotype.Component;

import com.qtrmoon.slwldx.pojo.Wlzfxx;
/** 物流资费信息实体映射 */
@Component
public interface WlzfxxMapper {
	/**
	 * Wlzfxx的条件查询方法
	 * @param wlzfxx 承载查询条件
	 * @return 返回Wlzfxx的集合
	 */
	public List<Wlzfxx> schWlzfxx(Wlzfxx wlzfxx);
	
	/**
	 * Wlzfxx的主键查询方法
	 * @param id 主键值
	 * @return 返回Wlzfxx实体
	 */
	public Wlzfxx schWlzfxxById(Integer id);
	
	/**
	 * Wlzfxx的添加方法
	 * @param wlzfxx 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addWlzfxx(Wlzfxx wlzfxx);
	
	/**
	 * Wlzfxx的修改方法
	 * @param wlzfxx 承载数据的Bean
	 */
	public void updWlzfxx(Wlzfxx wlzfxx);
	
	/**
	 * Wlzfxx的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delWlzfxx(Integer id);
	
	/**
	 * Wlzfxx的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delWlzfxxs(Integer[] ids);
}
