package com.qtrmoon.slwldx.serdao;

import java.util.List;
import org.springframework.stereotype.Component;

import com.qtrmoon.slwldx.pojo.Jpddxx;
/** 机票订单信息实体映射 */
@Component
public interface JpddxxMapper {
	/**
	 * Jpddxx的条件查询方法
	 * @param jpddxx 承载查询条件
	 * @return 返回Jpddxx的集合
	 */
	public List<Jpddxx> schJpddxx(Jpddxx jpddxx);
	
	/**
	 * Jpddxx的主键查询方法
	 * @param id 主键值
	 * @return 返回Jpddxx实体
	 */
	public Jpddxx schJpddxxById(Integer id);
	
	/**
	 * Jpddxx的添加方法
	 * @param jpddxx 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addJpddxx(Jpddxx jpddxx);
	
	/**
	 * Jpddxx的修改方法
	 * @param jpddxx 承载数据的Bean
	 */
	public void updJpddxx(Jpddxx jpddxx);
	
	/**
	 * Jpddxx的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delJpddxx(Integer id);
	
	/**
	 * Jpddxx的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delJpddxxs(Integer[] ids);
}
