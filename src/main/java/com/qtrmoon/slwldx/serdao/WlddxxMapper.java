package com.qtrmoon.slwldx.serdao;

import java.util.List;
import org.springframework.stereotype.Component;

import com.qtrmoon.slwldx.pojo.Wlddxx;
/** 物流订单信息实体映射 */
@Component
public interface WlddxxMapper {
	/**
	 * Wlddxx的条件查询方法
	 * @param wlddxx 承载查询条件
	 * @return 返回Wlddxx的集合
	 */
	public List<Wlddxx> schWlddxx(Wlddxx wlddxx);
	
	/**
	 * Wlddxx的主键查询方法
	 * @param id 主键值
	 * @return 返回Wlddxx实体
	 */
	public Wlddxx schWlddxxById(Integer id);
	
	/**
	 * Wlddxx的添加方法
	 * @param wlddxx 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addWlddxx(Wlddxx wlddxx);
	
	/**
	 * Wlddxx的修改方法
	 * @param wlddxx 承载数据的Bean
	 */
	public void updWlddxx(Wlddxx wlddxx);
	
	/**
	 * Wlddxx的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delWlddxx(Integer id);
	
	/**
	 * Wlddxx的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delWlddxxs(Integer[] ids);
}
