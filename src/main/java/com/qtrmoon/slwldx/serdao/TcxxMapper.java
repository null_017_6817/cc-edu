package com.qtrmoon.slwldx.serdao;

import java.util.List;
import org.springframework.stereotype.Component;

import com.qtrmoon.slwldx.pojo.Tcxx;
/** 套餐信息实体映射 */
@Component
public interface TcxxMapper {
	/**
	 * Tcxx的条件查询方法
	 * @param tcxx 承载查询条件
	 * @return 返回Tcxx的集合
	 */
	public List<Tcxx> schTcxx(Tcxx tcxx);
	
	/**
	 * Tcxx的主键查询方法
	 * @param id 主键值
	 * @return 返回Tcxx实体
	 */
	public Tcxx schTcxxById(Integer id);
	
	/**
	 * Tcxx的添加方法
	 * @param tcxx 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addTcxx(Tcxx tcxx);
	
	/**
	 * Tcxx的修改方法
	 * @param tcxx 承载数据的Bean
	 */
	public void updTcxx(Tcxx tcxx);
	
	/**
	 * Tcxx的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delTcxx(Integer id);
	
	/**
	 * Tcxx的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delTcxxs(Integer[] ids);

	/**
	 * 查询tcmc最大最小
	 * 2017年10月19日
	 * By 蔚昕
	 * @param tcxx 
	 */
	public List<Tcxx> schMinTcmc(Tcxx tcxx);
}
