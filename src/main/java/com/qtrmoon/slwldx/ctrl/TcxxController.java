package com.qtrmoon.slwldx.ctrl;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.qtrmoon.common.BaseController;
import com.qtrmoon.slwldx.pojo.Grtcylxx;
import com.qtrmoon.slwldx.pojo.Tcxx;
import com.qtrmoon.slwldx.serdao.ISlwldxService;

/**
 * 套餐信息控制器
 */
@Controller
@RequestMapping("/slwldx/tcxx")
public class TcxxController extends BaseController{
	@Autowired
	private ISlwldxService slwldxService;								//声明模块Service实例

	/**
	 * 查询方法，自带分页设置。
	 * @param tcxx 承载查询条件的Bean
	 * @param request 可用来获取非Bean内的参数
	 * @param response 返回请求,输出Json
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/schTcxx.action")
	public void schTcxx(Tcxx tcxx,HttpServletRequest request,HttpServletResponse response) throws IOException{
		noCache(response);
		tcxx.setCondition("");											//防sql注入
		tcxx.assLike("tcxy","tcmc","xxsm");

		List<Tcxx> list=slwldxService.schTcxx(tcxx);		//调用Service查询数据
		/*将数据Bean的集合转换为Json集合*/
		JSONArray datas=new JSONArray();									//构造业务数据的JSON集合
		JSONObject obj;														//声明用于构造Bean的Json对象
		for(Tcxx u:list){												//循环查询的数据集构造Json数据集
			obj=u.getJsonInDict();											//将Bean转换为Json对象
			obj.put("_oper",u.getId());								//为"操作列"设定主键值
			datas.add(obj);													//添加Json对象到Json集合
		}
		/*构造分页数据的JSON对象，并包装到返回页面的Json中*/
		JSONObject res=new JSONObject();									//声明页面返回Json
		res.put("total",tcxx.getDatasize());							//设置数据总记录数
		res.put("rows", datas);												//设置数据集
		response.getWriter().print(res.toJSONString());						//输出到页面
	}
	
	/**
	 * 预添加or预修改or查看方法
	 * @param tcxx 预添加时可传入一些初始值；预修改和查看时用来承载主键值
	 * @param toL 挂字典的字段是否需要转为字典Label值。true则进行转换
	 * @param response 返回请求,输出Json
	 * @throws IOException
	 */
	@RequestMapping(value="/vieTcxx.action")
	public void vieTcxx(Tcxx tcxx,Boolean toL,HttpServletResponse response) throws IOException{
		noCache(response);													//Ajax方法设定无缓存
		if(toL!=null&&toL){													//查看页面加载数据时调用，返回的对象已进行字典转换
			if(tcxx.getId()!=null&&tcxx.getId()!=0){					//检测主键非空
				tcxx=slwldxService.schTcxxById(tcxx.getId());			//按主键查询数据
				response.getWriter().print(tcxx.getJsonInDict());						//输出经过字典转换Json对象到查看页(vie_)
			}
		}else{																//修改页面加载数据时调用
			if(tcxx.getId()!=null&&tcxx.getId()!=0){	//检查主键非空
				tcxx=slwldxService.schTcxxById(tcxx.getId());		//按主键查询数据
				response.getWriter().print(tcxx.getJson());			//输出Json对象到修改页(upd_)
			}else{															//添加页面加载数据时调用，可设置添加页面中一些属性的预设值。
				response.getWriter().print(new Tcxx().getJson());		//输出Json对象到添加页(upd_)
			}
		}
	}
	
	/**
	 * 添加或修改的提交方法
	 * @param tcxx 数据表单Bean
	 * @param response 返回请求
	 * @throws IOException
	 */
	@RequestMapping(value="/updTcxx.action")
	public void updTcxx(Tcxx tcxx,HttpServletResponse response) throws IOException{
		noCache(response);													//Ajax方法设定无缓存
		if(tcxx.getId()==null){									//检测主键为空则
			slwldxService.addTcxx(tcxx);						//添加数据
		}else{																//主键非空则
			slwldxService.updTcxx(tcxx);						//修改数据
		}
	}
	
	/**
	 * 删除方法
	 * @param ids 待删除数据的主键数组
	 * @param response 返回请求
	 */
	@RequestMapping(value="/delTcxx.action")
	public void delTcxx(@RequestParam(value = "ids[]") Integer[] ids,HttpServletResponse response){
		noCache(response);													//Ajax方法设定无缓存
		slwldxService.delTcxx(ids);									//删除数据
	}
	
	/**
	 * JSP页面跳转
	 * @param page jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value="/page.action")
	public String goPage(String page){										//放过本Ctrl的页面，不合法的跳转到error页。
		if("index".equals(page)||"tcxx_sch".equals(page)||"tcxx_vie".equals(page)||"tcxx_upd".equals(page)){
			return "/slwldx/"+page;
		}else{
			return "/error";
		}
	}
	
	/**
	 * 办理套餐
	 * 2017年10月17日
	 * By 蔚昕
	 * @param hbxxid
	 * @param jpddxx
	 * @param response
	 * @return
	 * @throws ParseException
	 */
	@RequestMapping(value="/buyTc.action")
	public String buyTc(Integer tid,Grtcylxx grtcylxx,HttpServletResponse response) throws ParseException{
		if(tid!=null){
			Tcxx schtcxx = slwldxService.schTcxxById(tid);//查询schtcxx对象
			grtcylxx.setTcid(schtcxx.getId());//设置grtcylxx对象属性
			grtcylxx.setDxsy(schtcxx.getDx());//设置grtcylxx对象属性
			grtcylxx.setGnllsy(schtcxx.getGnll());//设置grtcylxx对象属性
			grtcylxx.setBdllsy(schtcxx.getBdll());//设置grtcylxx对象属性
			grtcylxx.setBdthsy(schtcxx.getBdth());//设置grtcylxx对象属性
			grtcylxx.setQgthsy(schtcxx.getQgth());//设置grtcylxx对象属性
			grtcylxx.setHfye(Float.valueOf(schtcxx.getQgth()));//设置grtcylxx对象属性
			slwldxService.addGrtcylxx(grtcylxx);//增加grtcylxx对象
		}
		return "redirect:/slwldx/aadx/list.jsp";//返回jsp页面
	}
	
	/**
	 * 查询方法，自带分页设置。
	 * @param tcxx 承载查询条件的Bean
	 * @param request 可用来获取非Bean内的参数
	 * @param response 返回请求,输出Json
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/schTcxxMinMax.action")
	public void schTcxxMinMax(Tcxx tcxx,HttpServletRequest request,HttpServletResponse response) throws IOException{
		noCache(response);
		tcxx.setCondition("");											//防sql注入
		tcxx.assLike("tcxy","tcmc","xxsm");
		List<Tcxx> list = slwldxService.schMinTcmc(tcxx);			//调用Service查询数据
		/*将数据Bean的集合转换为Json集合*/
		JSONArray datas=new JSONArray();									//构造业务数据的JSON集合
		JSONObject obj;														//声明用于构造Bean的Json对象
		for(Tcxx u:list){												//循环查询的数据集构造Json数据集
			obj=u.getJsonInDict();											//将Bean转换为Json对象
			obj.put("_oper",u.getId());								//为"操作列"设定主键值
			datas.add(obj);													//添加Json对象到Json集合
		}
		/*构造分页数据的JSON对象，并包装到返回页面的Json中*/
		JSONObject res=new JSONObject();									//声明页面返回Json
		res.put("total",tcxx.getDatasize());							//设置数据总记录数
		res.put("rows", datas);												//设置数据集
		response.getWriter().print(res.toJSONString());						//输出到页面
	}
}
