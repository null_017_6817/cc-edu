package com.qtrmoon.slwldx.ctrl;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qtrmoon.common.BaseController;
import com.qtrmoon.slwldx.pojo.AddFare;
import com.qtrmoon.slwldx.pojo.Hbxx;
import com.qtrmoon.slwldx.pojo.JpddParam;
import com.qtrmoon.slwldx.pojo.Jpddxx;
import com.qtrmoon.slwldx.serdao.ISlwldxService;

/**
 * 航班信息控制器
 */
@Controller
@RequestMapping("/slwldx/hbxx")
public class HbxxController extends BaseController{
	@Autowired
	private ISlwldxService slwldxService;								//声明模块Service实例

	/**
	 * 查询方法，自带分页设置。
	 * @param hbxx 承载查询条件的Bean
	 * @param request 可用来获取非Bean内的参数
	 * @param response 返回请求,输出Json
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/schHbxx.action")
	public void schHbxx(Hbxx hbxx,HttpServletRequest request,HttpServletResponse response) throws IOException{
		noCache(response);
		hbxx.setCondition("");											//防sql注入
		hbxx.assLike("hkgs","hbh","qfsj","ddsj","rq","cfcs","ddcs","qfjc","ddjc");

		List<Hbxx> list=slwldxService.schHbxx(hbxx);		//调用Service查询数据
		/*将数据Bean的集合转换为Json集合*/
		JSONArray datas=new JSONArray();									//构造业务数据的JSON集合
		JSONObject obj;														//声明用于构造Bean的Json对象
		for(Hbxx u:list){												//循环查询的数据集构造Json数据集
			obj=u.getJsonInDict();											//将Bean转换为Json对象
			obj.put("_oper",u.getId());								//为"操作列"设定主键值
			datas.add(obj);													//添加Json对象到Json集合
		}
		/*构造分页数据的JSON对象，并包装到返回页面的Json中*/
		JSONObject res=new JSONObject();									//声明页面返回Json
		res.put("total",hbxx.getDatasize());							//设置数据总记录数
		res.put("rows", datas);												//设置数据集
		response.getWriter().print(res.toJSONString());						//输出到页面
	}
	
	/**
	 * 预添加or预修改or查看方法
	 * @param hbxx 预添加时可传入一些初始值；预修改和查看时用来承载主键值
	 * @param toL 挂字典的字段是否需要转为字典Label值。true则进行转换
	 * @param response 返回请求,输出Json
	 * @throws IOException
	 */
	@RequestMapping(value="/vieHbxx.action")
	public void vieHbxx(Hbxx hbxx,Boolean toL,HttpServletResponse response) throws IOException{
		noCache(response);													//Ajax方法设定无缓存
		if(toL!=null&&toL){													//查看页面加载数据时调用，返回的对象已进行字典转换
			if(hbxx.getId()!=null&&hbxx.getId()!=0){					//检测主键非空
				hbxx=slwldxService.schHbxxById(hbxx.getId());			//按主键查询数据
				response.getWriter().print(hbxx.getJsonInDict());						//输出经过字典转换Json对象到查看页(vie_)
			}
		}else{																//修改页面加载数据时调用
			if(hbxx.getId()!=null&&hbxx.getId()!=0){	//检查主键非空
				hbxx=slwldxService.schHbxxById(hbxx.getId());		//按主键查询数据
				response.getWriter().print(hbxx.getJson());			//输出Json对象到修改页(upd_)
			}else{															//添加页面加载数据时调用，可设置添加页面中一些属性的预设值。
				response.getWriter().print(new Hbxx().getJson());		//输出Json对象到添加页(upd_)
			}
		}
	}
	
	/**
	 * 添加或修改的提交方法
	 * @param hbxx 数据表单Bean
	 * @param response 返回请求
	 * @throws IOException
	 */
	@RequestMapping(value="/updHbxx.action")
	public void updHbxx(Hbxx hbxx,HttpServletResponse response) throws IOException{
		noCache(response);													//Ajax方法设定无缓存
		if(hbxx.getId()==null){									//检测主键为空则
			slwldxService.addHbxx(hbxx);						//添加数据
		}else{																//主键非空则
			slwldxService.updHbxx(hbxx);						//修改数据
		}
	}
	
	/**
	 * 删除方法
	 * @param ids 待删除数据的主键数组
	 * @param response 返回请求
	 */
	@RequestMapping(value="/delHbxx.action")
	public void delHbxx(@RequestParam(value = "ids[]") Integer[] ids,HttpServletResponse response){
		noCache(response);													//Ajax方法设定无缓存
		slwldxService.delHbxx(ids);									//删除数据
	}
	
	/**
	 * JSP页面跳转
	 * @param page jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value="/page.action")
	public String goPage(String page,String id,Model model){										//放过本Ctrl的页面，不合法的跳转到error页。
		if("index".equals(page)||"hbxx_sch".equals(page)||"hbxx_vie".equals(page)||"hbxx_upd".equals(page)||"hbxx_res".equals(page)){
			model.addAttribute("hbxxid", id);//设置模型属性
			return "/slwldx/"+page;//返回jsp页面
		}else{
			return "/error";//返回error页面
		}
	}
	
	/**
	 * 订票
	 * 2017年10月9日
	 * By 蔚昕
	 * @param response
	 * @throws ParseException 
	 * @throws IOException 
	 */
	/*@RequestMapping(value="/reshbxx.action")
	public String reshbxx(String hbxxid,String ddhbrq,JpddParam jpddList ,HttpServletResponse response) throws ParseException, IOException{
		if(hbxxid!=null){      
			Hbxx hbxx = slwldxService.schHbxxById(Integer.parseInt(hbxxid.toString()));
			Calendar calendar = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			String dpsj = sdf.format(calendar.getTime());
			SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMddhhmmss");
			String ddh = sdf1.format(calendar.getTime());
			List<Jpddxx> jpddList2 = jpddList.getJpddList();
			for (Jpddxx jpddxx : jpddList2) {
				jpddxx.setHbrq(ddhbrq.substring(0, ddhbrq.length()-3));
				jpddxx.setDdh(ddh);
				jpddxx.setHbh(hbxx.getHbh());//根据id获取航班号
				jpddxx.setDpsj(dpsj);//订票时间为当前时间
				slwldxService.addJpddxx(jpddxx);
			}
		}
		return "redirect:/slwldx/hbxx/hbxx_sch.jsp";
	}*/
	
	/**
	 * 订票
	 * 2017年10月9日
	 * By 蔚昕
	 * @param response
	 * @throws ParseException 
	 * @throws IOException 
	 */
	@ResponseBody
	@RequestMapping(value="/reshbxx.action")
	public String reshbxx(String hbxxid,Jpddxx jpddxx ,HttpServletResponse response) throws ParseException, IOException{
		if(hbxxid!=null){      
			Hbxx hbxx = slwldxService.schHbxxById(Integer.parseInt(hbxxid.toString()));//查询hbxxid数据
			Calendar calendar = Calendar.getInstance();//获取实体
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");//转换时间格式
			String dpsj = sdf.format(calendar.getTime());//格式化创建时间
			SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMddhhmmss");//转换时间格式
			String ddh = sdf1.format(calendar.getTime());//格式化创建时间
			/*List<Jpddxx> jpddList2 = jpddList.getJpddList();
			for (Jpddxx jpddxx : jpddList2) {*/
			if(jpddxx != null && jpddxx.getHbrq() != null){//jpddxx属性不为空
				jpddxx.setHbrq(jpddxx.getHbrq().substring(0, jpddxx.getHbrq().length()-3));//设置jpddxx属性
			}	
			jpddxx.setDdh(ddh);//设置jpddxx属性
			jpddxx.setHbh(hbxx.getHbh());//根据id获取航班号
			jpddxx.setDpsj(dpsj);//订票时间为当前时间
			slwldxService.addJpddxx(jpddxx);
			/*}*/
		}
		return "/slwldx/hbxx/hbxx_sch.jsp";
	}
	
	//保存乘客相关联系人信息 
	@RequestMapping(value="/saveThisP.action")
	public void saveThisP(AddFare addFare,HttpServletResponse response) throws IOException{
		noCache(response);													//Ajax方法设定无缓存
		slwldxService.saveThisP(addFare);						//添加数据
	}
}
