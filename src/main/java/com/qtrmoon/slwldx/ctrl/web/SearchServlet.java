package com.qtrmoon.slwldx.ctrl.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import net.sf.json.JSONArray;
/**
 * 物流前端控制器
 *
 */
@Controller
@RequestMapping("/slwldx/web")
public class SearchServlet extends HttpServlet {
	/**
	 * long serialVersionUID 
	 */
	private static final long serialVersionUID = 1L;
	static private List<String> list = new ArrayList<String>();//新建数组
	static{
	}
	/**
	 * 连接前端get方法
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");//设置请求编码
		response.setCharacterEncoding("utf-8");//设置请求编码
		String  keyword  = request.getParameter("keyword");//从request中获取参数
		List<String> res  = new ArrayList<String>();//新建数组

		for (String string : list) {//循环数组
			if (string.contains(keyword)){//校验数组中的关键字
				res.add(string);//数组增加关键字
			}
		}
		JSONArray jsonArray = JSONArray.fromObject(res);//设置数组
		response.setContentType("text/html");//设置返回格式
		response.getWriter().write(jsonArray.toString());//输出返回
	}
}