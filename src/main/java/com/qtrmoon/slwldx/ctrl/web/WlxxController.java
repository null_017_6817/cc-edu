package com.qtrmoon.slwldx.ctrl.web;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javassist.compiler.ast.Keyword;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jboss.netty.util.internal.ConcurrentHashMap;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.qtrmoon.common.BaseController;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.dictionary.bsd.DictBean;
import com.qtrmoon.slwldx.pojo.Wlddxx;
import com.qtrmoon.slwldx.pojo.WlddxxVo;
import com.qtrmoon.slwldx.pojo.Wllzxx;
import com.qtrmoon.slwldx.pojo.Wlzfxx;
import com.qtrmoon.slwldx.serdao.ISlwldxService;

/**
 * 前台物流信息查询控制器
 */
@Controller
@RequestMapping("/slwldx/web")
public class WlxxController extends BaseController{
	@Autowired
	private ISlwldxService slwldxService;											//声明模块Service实例
	
	private static Map<String,Double> map = new ConcurrentHashMap<String,Double>();
	
	private static final double baseDistanceBeijingGuangdong = 853.6534442083477;
	
	static{
		map.put("华唐速递", 12.00);//设置map值
		map.put("天天快递", 12.00);//设置map值
		map.put("圆通快递", 13.00);//设置map值
		map.put("顺丰快递", 22.00);//设置map值
		map.put("申通快递", 15.00);//设置map值
		map.put("韵达快递", 11.00);//设置map值
		map.put("中通快递", 12.00);//设置map值
		map.put("宅急送", 12.00);//设置map值
		map.put("速尔快递", 9.00);//设置map值
		map.put("EMS", 20.00);//设置map值
		map.put("全峰快递", 10.00);//设置map值
		map.put("优速快递", 13.00);//设置map值
		map.put("百世汇通", 12.00); //设置map值
	}
	
	/**
	 * 前台物流资费信息查询方法，自带分页设置。
	 * @param wlzfxx 承载查询条件的Bean
	 * @param request 可用来获取非Bean内的参数
	 * @param response 返回请求,输出Json
	 * @throws IOException
	 */
	@RequestMapping(value="/wlzfxx/schWlzfxx.action")
	public void schWlzfxx(Wlzfxx wlzfxx,HttpServletRequest request,HttpServletResponse response,Model model) throws IOException{
		noCache(response);
		//wlzfxx.setCondition("");													//防sql注入
		//设置查询条件:始发城市,目的城市,物流公司
		//wlzfxx.assLike("sfcs","mdcs","wlgs");	
		
		//查询字典
		String dq1=wlzfxx.getSfcs(),dq2=wlzfxx.getMdcs();
		DictBean db1=DictBuffer.getDictById("CCQ_QQCS", dq1);//从字典缓存读取数据
		DictBean db2=DictBuffer.getDictById("CCQ_QQCS", dq2);//从字典缓存读取数据
		double db1_lat = db1.getExp("LAT")==null ? 0:Double.parseDouble(db1.getExp("LAT"));//从字典缓存读取数据
		double db1_lon = db1.getExp("LON")==null ? 0:Double.parseDouble(db1.getExp("LON"));//从字典缓存读取数据
		double db2_lat = db1.getExp("LAT")==null ? 0:Double.parseDouble(db2.getExp("LAT"));//从字典缓存读取数据
		double db2_lon = db1.getExp("LON")==null ? 0:Double.parseDouble(db2.getExp("LON"));//从字典缓存读取数据
		
		//计算城市之间的距离
		double distance = validInR(db1_lon,db1_lat,db2_lon,db2_lat);
		//根据北京-广州的基础费用，计算实际两地的物流资费
		double baseFee = getBaseFee(wlzfxx.getWlgs());//获取baseFee值
		double targetFee = (baseFee / baseDistanceBeijingGuangdong ) * distance ;//计算targetFee值
		if(targetFee < 9) {//校验targetFee
			targetFee = 9;//设置targetFee值
		}
		
		DecimalFormat    df   = new DecimalFormat("######0");   //设置df值
		String sfcs = db1.getLabel();//获取标签
		String mdcs = db2.getLabel();//获取标签
		
		try {
			JSONObject result =  new JSONObject();//新建result对象
			result.put("sfcs", sfcs);//设置result信息
			result.put("mdcs", mdcs);//设置result信息
			result.put("jg", df.format(targetFee));//设置result信息
			result.put("wlgs", wlzfxx.getWlgs());//设置result信息
			result.put("state", "success");//设置result信息
			response.getWriter().write(result.toJSONString());					//输出到页面
		} catch (Exception e) {
			JSONObject res=new JSONObject();//新建JSONObject对象
			res.put("state", "error");//设置数据错误信息
			response.getWriter().write(res.toJSONString());							//输出到页面
		}
	}
	
	private Double getBaseFee(String wlgs){
		return map.get(wlgs);
	} 
	
	/**
	 * 计算两点的球面距离，单位公里
	 * @param longitudeA;//A点经度
	 * @param latitudeA;//A点纬度
	 * @param longitudeB;//B点经度
	 * @param latitudeB;//B点纬度
	 * @return
	 */
	private static double validInR(Double longitudeA, Double latitudeA,Double longitudeB, Double latitudeB) {
		if(longitudeA!=null&&longitudeB!=null&&latitudeA!=null&&latitudeB!=null){//设置长度
			longitudeA = Math.PI * longitudeA / 180;//计算longitudeA值
			longitudeB = Math.PI * longitudeB / 180;//计算longitudeB值
			latitudeA = Math.PI * latitudeA / 180;//计算latitudeA值
			latitudeB = Math.PI * latitudeB / 180;//计算latitudeB值
			double distance = 6371.3 * Math.acos(Math.sin(longitudeA)//计算distance值
			* Math.sin(longitudeB) + Math.cos(longitudeA)
			* Math.cos(longitudeB) * Math.cos(latitudeA - latitudeB));
			return distance;//返回举例
		}
	return 0;
}

	/**
	 * 前台物流订单信息订单号查询方法，自带分页设置。
	 * @param wlddxx 承载查询条件的Bean
	 * @param request 可用来获取非Bean内的参数
	 * @param response 返回请求,输出Json
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/wlddxx/schWlddxxByDdh.action")
	public void schWlddxxByDdh(WlddxxVo wlddxxVo,HttpServletRequest request,HttpServletResponse response,Model model) throws IOException{
		noCache(response);
		wlddxxVo.setCondition("");														//防sql注入
		//设置订单详情信息查询条件:订单号,物流公司,订单价格,寄件人,收件人,寄件人电话,收件人电话,始发城市,目的城市
		wlddxxVo.assLike("wlgs","ddjg","jjr","sjr","jjrdh","sjrdh","sfcs","mdcs","sfcsxxdz","mdcsxxdz");
		try {
			List<WlddxxVo> wlddxxVoList = slwldxService.schWlxx(wlddxxVo);				//调用Service查询订单详情信息数据
			Wllzxx wllzxx = new Wllzxx();//新建wllzxx
			wllzxx.setDdh(wlddxxVo.getDdh());//设置wllzxx属性
			wllzxx.addOrderDescCol("id");//设置排序描述字段
			wllzxx.setPagesize(20);//设置页面大小
			List<Wllzxx> wllzxxList = slwldxService.schWllzxx(wllzxx); 					//调用Service查询物流流转信息数据
			JSONArray wllzxxListDatas=new JSONArray();									//构造业务数据的JSON集合
			JSONObject obj;																//声明用于构造Bean的Json对象
			for(Wllzxx u:wllzxxList){													//循环查询的数据集构造Json数据集
				obj=u.getJsonInDict();
				wllzxxListDatas.add(obj);												//添加Json对象到Json集合
			}
			
			JSONObject wlddxxJson = wlddxxVoList.get(0).getJsonInDict();				//构造物流订单信息的json数据
			JSONObject res=new JSONObject();											//声明页面返回Json
			res.put("wlddxx", wlddxxJson);												//设置物流订单信息数据		
			res.put("wllzxx", wllzxxListDatas);											//设置物流流转信息数据
			res.put("state", "success");
			response.getWriter().write(res.toJSONString());								//输出到页面
		} catch (Exception e) {
			JSONObject res=new JSONObject();//创建新对象
			res.put("state", "error");//设置错误信息
			response.getWriter().write(res.toJSONString());								//输出到页面
		}
	}
	
	/**
	 * 前台物流订单信息手机号查询方法，自带分页设置。
	 * @param wlddxx 承载查询条件的Bean
	 * @param request 可用来获取非Bean内的参数
	 * @param response 返回请求,输出Json
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/wlddxx/schWlddxxByPhone.action")
	public void schWlddxxByPhone(String phoneNum,String wlgs,HttpServletRequest request,HttpServletResponse response,Model model) throws IOException{
		noCache(response);
		//如果是寄件人电话
		WlddxxVo wlddxxByJjrdh = new WlddxxVo();
		wlddxxByJjrdh.setJjrdh(phoneNum);
		wlddxxByJjrdh.setWlgs(wlgs);
		wlddxxByJjrdh.setCondition("");														  //防sql注入
		//设置订单详情信息查询条件:订单号,物流公司,订单价格,寄件人,收件人,寄件人电话,收件人电话,始发城市,目的城市
		wlddxxByJjrdh.assLike("ddh","wlgs","ddjg","jjr","sjr","sfcs","mdcs","sfcsxxdz","mdcsxxdz");
		List<WlddxxVo> wlddxxListByJjrdh=slwldxService.schWlxx(wlddxxByJjrdh);				  //调用Service查询订单详情信息数据
		//如果是收件人电话
		WlddxxVo wlddxxBySjrdh = new WlddxxVo();
		wlddxxBySjrdh.setSjrdh(phoneNum);
		wlddxxBySjrdh.setWlgs(wlgs);
		wlddxxBySjrdh.setCondition("");														   //防sql注入
		wlddxxByJjrdh.assLike("ddh","wlgs","ddjg","jjr","sjr","sfcs","mdcs","sfcsxxdz","mdcsxxdz");
		
		List<WlddxxVo> wlddxxListBySjrdh=slwldxService.schWlxx(wlddxxBySjrdh);				   //调用Service查询订单详情信息数据
		//判断哪个查询的值不为空
		if(wlddxxListByJjrdh.size() > 0){
			JSONObject wlddxxJsonByJjrdh = wlddxxListByJjrdh.get(0).getJsonInDict();//设置json对象
			Wllzxx wllzxx = new Wllzxx();//新建Wllzxx对象
			wllzxx.setDdh(wlddxxListByJjrdh.get(0).getDdh());//设置对象属性
			wllzxx.addOrderDescCol("id");//增加排序描述字段
			wllzxx.setPagesize(20);//设置页面大小
			List<Wllzxx> wllzxxList = slwldxService.schWllzxx(wllzxx); 							//调用Service查询物流流转信息数据
			JSONArray wllzxxListDatas=new JSONArray();											//构造业务数据的JSON集合
			JSONObject obj;																		//声明用于构造Bean的Json对象
			for(Wllzxx u:wllzxxList){															//循环查询的数据集构造Json数据集
				obj=u.getJsonInDict();															//将Bean转换为Json对象
				wllzxxListDatas.add(obj);														//添加Json对象到Json集合
			}
			JSONObject res=new JSONObject();													//声明页面返回Json
			res.put("wlddxx", wlddxxJsonByJjrdh);												//设置物流订单信息数据		
			res.put("wllzxx", wllzxxListDatas);													//设置物流流转信息数据
			res.put("state", "success");
			response.getWriter().write(res.toJSONString());										//输出到页面
		}else if(wlddxxListBySjrdh.size() > 0){
			JSONObject wlddxxJsonBySjrdh = wlddxxListBySjrdh.get(0).getJsonInDict();//将字典转换为json字符串
			Wllzxx wllzxx = new Wllzxx();//新建Wllzxx对象
			wllzxx.setDdh(wlddxxListBySjrdh.get(0).getDdh());//设置对象属性
			wllzxx.addOrderDescCol("id");//设置对象排序字段
			wllzxx.setPagesize(20);//设置分页大小
			List<Wllzxx> wllzxxList = slwldxService.schWllzxx(wllzxx); 							//调用Service查询物流流转信息数据
			JSONArray wllzxxListDatas=new JSONArray();											//构造业务数据的JSON集合
			JSONObject obj;																		//声明用于构造Bean的Json对象
			for(Wllzxx u:wllzxxList){															//循环查询的数据集构造Json数据集
				obj=u.getJsonInDict();															//将Bean转换为Json对象
				wllzxxListDatas.add(obj);														//添加Json对象到Json集合
			}
			JSONObject res=new JSONObject();													//声明页面返回Json
			res.put("wlddxx", wlddxxJsonBySjrdh);												//设置物流订单信息数据		
			res.put("wllzxx", wllzxxListDatas);													//设置物流流转信息数据
			res.put("state", "success");
			response.getWriter().write(res.toJSONString());										//输出到页面
		}else{
			JSONObject res=new JSONObject();//新建res对象
			res.put("state", "error");//设置返回值
			response.getWriter().write(res.toJSONString());										//输出到页面
		}
	}
	
	/**
	 * 返回物流公司列表
	 * @param void
	 * @return JsonArray
	 * @throws Exception 
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/wlxx/schWlgs.action")
	public void schWlgs(HttpServletResponse response) throws Exception{							//放过本Ctrl的页面，不合法的跳转到error页。
		List<DictBean>  list =DictBuffer.getDictById("CCQ_WLGS", DictBuffer.dummyRootId).getChildren("CCQ_WLGS"); 
		JSONArray datas=new JSONArray();														//构造业务数据的JSON集合
		for(DictBean u:list){																	//循环查询的数据集构造Json数据集
			datas.add(u.getLabel());															//添加Json对象到Json集合
		}
		response.getWriter().write(datas.toJSONString());//输出页面
	}
	
	/**
	 * JSP页面跳转
	 * @param page jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value="/page.action")
	public String goPage(String page){															//放过本Ctrl的页面，不合法的跳转到error页。
		if("wlddxx_sch".equals(page)||"wlzfxx_sch".equals(page)){
			return "/slwldx/wlxx/"+page;//设置查询页面
		}else{
			return "/error";//设置错误页面
		}
	}
	
	/**
	 * 模糊查询手机号
	 * 2017年10月31日
	 * By 蔚昕
	 * @param response
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/wlxx/schPull.action")
	public void schPull(HttpServletResponse response) throws Exception{							//放过本Ctrl的页面，不合法的跳转到error页。
		List<DictBean>  list =DictBuffer.getDictById("CCQ_PULLWLDD", DictBuffer.dummyRootId).getChildren("CCQ_PULLWLDD"); 
		JSONArray datas=new JSONArray();														//构造业务数据的JSON集合
		for(DictBean u:list){																	//循环查询的数据集构造Json数据集
			datas.add(u.getLabel());															//添加Json对象到Json集合
		}
		response.getWriter().write(datas.toJSONString());//返回数据字符串
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/schWlgs.action")
	public void schWlgs(Wlddxx wlddxx,HttpServletRequest request,HttpServletResponse response) throws IOException{
		noCache(response);
		wlddxx.setCondition("");											//防sql注入
		wlddxx.assLike("ddh","jjr","sjr","jjrdh","sjrdh","sfcs","mdcs","sfcsxxdz","mdcsxxdz","jjsj","sjsj","ddzt");//防sql注入

		List<Wlddxx> list=slwldxService.schWlddxx(wlddxx);		//调用Service查询数据
		String  keyword  = request.getParameter("keyword");
		for (Wlddxx wlddxx2 : list) {//循环list
			if((wlddxx2.getDdh()).contains(keyword)){//设置关键词
				
			}
		}
		/*将数据Bean的集合转换为Json集合*/
		JSONArray datas=new JSONArray();									//构造业务数据的JSON集合
		JSONObject obj;														//声明用于构造Bean的Json对象
		for(Wlddxx u:list){												//循环查询的数据集构造Json数据集
			obj=u.getJsonInDict();											//将Bean转换为Json对象
			obj.put("_oper",u.getId());								//为"操作列"设定主键值
			datas.add(obj);													//添加Json对象到Json集合
		}
		/*构造分页数据的JSON对象，并包装到返回页面的Json中*/
		JSONObject res=new JSONObject();									//声明页面返回Json
		res.put("total",wlddxx.getDatasize());							//设置数据总记录数
		res.put("rows", datas);												//设置数据集
		response.getWriter().print(res.toJSONString());						//输出到页面
	}
}
