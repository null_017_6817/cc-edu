package com.qtrmoon.slwldx.pojo;
import org.json.simple.JSONObject;

import com.qtrmoon.common.PageForm;
import com.qtrmoon.dictionary.DictBuffer;

/** 套餐信息实体类 */
public class Tcxx extends PageForm{
	//Fields
	
	private Integer id;// 主键
	private String tcxy;// 套餐行业
	private String tcmc;// 套餐名称
	private Integer zf;// 资费（元）
	private Integer dx;// 短信（条数）
	private Integer gnll;// 国内流量（M）
	private Integer bdll;// 本地流量（M）
	private Integer bdth;// 本地通话(分钟)
	private Integer qgth;// 全国通话（分钟）
	private String xxsm;// 详细说明
	private String xttc;//相同套餐
	private String minzf;//最小资费
	private String maxzf;//最大资费
	//Constructors
	/** default constructor */
	public Tcxx() {
	
	}	
	//getter and setter
	/** 获取主键 */
	public Integer getId() {
		return this.id;
	}
	/** 设置主键 */
	public void setId(Integer id) {
		this.id = id;
	}
	/** 获取套餐行业 */
	public String getTcxy() {
		return this.tcxy;
	}
	/** 设置套餐行业 */
	public void setTcxy(String tcxy) {
		this.tcxy = tcxy;
	}
	/** 获取套餐名称 */
	public String getTcmc() {
		return this.tcmc;
	}
	/** 设置套餐名称 */
	public void setTcmc(String tcmc) {
		this.tcmc = tcmc;
	}
	/** 获取资费（元） */
	public Integer getZf() {
		return this.zf;
	}
	/** 设置资费（元） */
	public void setZf(Integer zf) {
		this.zf = zf;
	}
	/** 获取短信（条数） */
	public Integer getDx() {
		return this.dx;
	}
	/** 设置短信（条数） */
	public void setDx(Integer dx) {
		this.dx = dx;
	}
	/** 获取国内流量（M） */
	public Integer getGnll() {
		return this.gnll;
	}
	/** 设置国内流量（M） */
	public void setGnll(Integer gnll) {
		this.gnll = gnll;
	}
	/** 获取本地流量（M） */
	public Integer getBdll() {
		return this.bdll;
	}
	/** 设置本地流量（M） */
	public void setBdll(Integer bdll) {
		this.bdll = bdll;
	}
	/** 获取本地通话(分钟) */
	public Integer getBdth() {
		return this.bdth;
	}
	/** 设置本地通话(分钟) */
	public void setBdth(Integer bdth) {
		this.bdth = bdth;
	}
	/** 获取全国通话（分钟） */
	public Integer getQgth() {
		return this.qgth;
	}
	/** 设置全国通话（分钟） */
	public void setQgth(Integer qgth) {
		this.qgth = qgth;
	}
	/** 获取详细说明 */
	public String getXxsm() {
		return this.xxsm;
	}
	/** 设置详细说明 */
	public void setXxsm(String xxsm) {
		this.xxsm = xxsm;
	}
	/** 获取相同套餐 */
	public String getXttc() {
		return xttc;
	}
	/** 设置相同套餐 */
	public void setXttc(String xttc) {
		this.xttc = xttc;
	}
	/** 获取最小资费 */
	public String getMinzf() {
		return minzf;
	}
	/** 设置最小资费 */
	public void setMinzf(String minzf) {
		this.minzf = minzf;
	}
	/** 获取最大资费 */
	public String getMaxzf() {
		return maxzf;
	}
	/** 设置最大资费 */
	public void setMaxzf(String maxzf) {
		this.maxzf = maxzf;
	}
	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("tcxy", tcxy);
		obj.put("tcmc", tcmc);
		obj.put("zf", zf);
		obj.put("dx", dx);
		obj.put("gnll", gnll);
		obj.put("bdll", bdll);
		obj.put("bdth", bdth);
		obj.put("qgth", qgth);
		obj.put("xxsm", xxsm);
		obj.put("xttc", xttc);
		obj.put("minzf", minzf);
		obj.put("maxzf", maxzf);
		return obj;
	}
	
	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		if(tcxy!=null&&!tcxy.equals("")){//字典id值转label
			obj.put("tcxy", DictBuffer.getLabel("CCQ_TCHY", tcxy.toString()));
		}
		obj.put("tcmc", tcmc);
		obj.put("zf", zf);
		obj.put("dx", dx);
		obj.put("gnll", gnll);
		obj.put("bdll", bdll);
		obj.put("bdth", bdth);
		obj.put("qgth", qgth);
		obj.put("xxsm", xxsm);
		obj.put("xttc", xttc);
		obj.put("minzf", minzf);
		obj.put("maxzf", maxzf);
		return obj;
	}
}
