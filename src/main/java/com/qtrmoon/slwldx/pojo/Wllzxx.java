package com.qtrmoon.slwldx.pojo;
import java.util.Date;
import java.util.Set;
import java.util.HashSet;
import org.json.simple.JSONObject;
import com.qtrmoon.common.PageForm;
import com.qtrmoon.toolkit.DateTransfer;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.dictionary.bsd.DictBean;

/** 物流流转信息实体类 */
public class Wllzxx extends PageForm{
	//Fields
	
	private Integer id;// 主键
	private String ddh;// 订单号
	private String jbrxm;// 经办人姓名
	private String jbrdh;// 经办人电话
	private String dqdz;// 当前地址
	private String sdsj;// 送达时间
	//Constructors
	/** default constructor */
	public Wllzxx() {
	
	}	
	//getter and setter
	/** 获取主键 */
	public Integer getId() {
		return this.id;
	}
	/** 设置主键 */
	public void setId(Integer id) {
		this.id = id;
	}
	/** 获取订单号 */
	public String getDdh() {
		return this.ddh;
	}
	/** 设置订单号 */
	public void setDdh(String ddh) {
		this.ddh = ddh;
	}
	/** 获取经办人姓名 */
	public String getJbrxm() {
		return this.jbrxm;
	}
	/** 设置经办人姓名 */
	public void setJbrxm(String jbrxm) {
		this.jbrxm = jbrxm;
	}
	/** 获取经办人电话 */
	public String getJbrdh() {
		return this.jbrdh;
	}
	/** 设置经办人电话 */
	public void setJbrdh(String jbrdh) {
		this.jbrdh = jbrdh;
	}
	/** 获取当前地址 */
	public String getDqdz() {
		return this.dqdz;
	}
	/** 设置当前地址 */
	public void setDqdz(String dqdz) {
		this.dqdz = dqdz;
	}
	/** 获取送达时间 */
	public String getSdsj() {
		return this.sdsj;
	}
	/** 设置送达时间 */
	public void setSdsj(String sdsj) {
		this.sdsj = sdsj;
	}
	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("ddh", ddh);
		obj.put("jbrxm", jbrxm);
		obj.put("jbrdh", jbrdh);
		obj.put("dqdz", dqdz);
		obj.put("sdsj", sdsj);
		return obj;
	}
	
	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("ddh", ddh);
		obj.put("jbrxm", jbrxm);
		obj.put("jbrdh", jbrdh);
		obj.put("dqdz", dqdz);
		obj.put("sdsj", sdsj);
		return obj;
	}
}
