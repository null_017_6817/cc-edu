package com.qtrmoon.slwldx.pojo;
import java.util.Date;
import java.util.Set;
import java.util.HashSet;
import org.json.simple.JSONObject;
import com.qtrmoon.common.PageForm;
import com.qtrmoon.toolkit.DateTransfer;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.dictionary.bsd.DictBean;

/** 航班信息实体类 */
public class Hbxx extends PageForm{
	//Fields
	
	private Integer id;// 主键
	private String hkgs;// 航空公司
	private String hbh;// 航班号
	private String qfsj;// 起飞时间
	private String ddsj;// 到达时间
	private String rq;// 日期
	private String cfcs;// 出发城市
	private String ddcs;// 达到城市
	private String qfjc;// 起飞机场
	private String ddjc;// 抵达机场
	private Integer jg;// 价格
	private Integer zdl;// 准点率
	private Integer discount;// 折扣
	
	//Constructors
	/** default constructor */
	public Hbxx() {
	
	}	
	//getter and setter
	/** 获取主键 */
	public Integer getId() {
		return this.id;
	}
	/** 设置主键 */
	public void setId(Integer id) {
		this.id = id;
	}
	/** 获取航空公司 */
	public String getHkgs() {
		return this.hkgs;
	}
	/** 设置航空公司 */
	public void setHkgs(String hkgs) {
		this.hkgs = hkgs;
	}
	/** 获取航班号 */
	public String getHbh() {
		return this.hbh;
	}
	/** 设置航班号 */
	public void setHbh(String hbh) {
		this.hbh = hbh;
	}
	/** 获取起飞时间 */
	public String getQfsj() {
		return this.qfsj;
	}
	/** 设置起飞时间 */
	public void setQfsj(String qfsj) {
		this.qfsj = qfsj;
	}
	/** 获取到达时间 */
	public String getDdsj() {
		return this.ddsj;
	}
	/** 设置到达时间 */
	public void setDdsj(String ddsj) {
		this.ddsj = ddsj;
	}
	/** 获取日期 */
	public String getRq() {
		return this.rq;
	}
	/** 设置日期 */
	public void setRq(String rq) {
		this.rq = rq;
	}
	/** 获取出发城市 */
	public String getCfcs() {
		return this.cfcs;
	}
	/** 设置出发城市 */
	public void setCfcs(String cfcs) {
		this.cfcs = cfcs;
	}
	/** 获取达到城市 */
	public String getDdcs() {
		return this.ddcs;
	}
	/** 设置达到城市 */
	public void setDdcs(String ddcs) {
		this.ddcs = ddcs;
	}
	/** 获取起飞机场 */
	public String getQfjc() {
		return this.qfjc;
	}
	/** 设置起飞机场 */
	public void setQfjc(String qfjc) {
		this.qfjc = qfjc;
	}
	/** 获取抵达机场 */
	public String getDdjc() {
		return this.ddjc;
	}
	/** 设置抵达机场 */
	public void setDdjc(String ddjc) {
		this.ddjc = ddjc;
	}
	/** 获取价格 */
	public Integer getJg() {
		return this.jg;
	}
	/** 设置价格 */
	public void setJg(Integer jg) {
		this.jg = jg;
	}
	/** 获取准点率 */
	public Integer getZdl() {
		return this.zdl;
	}
	/** 设置准点率 */
	public void setZdl(Integer zdl) {
		this.zdl = zdl;
	}
	public Integer getDiscount() {
		return discount;
	}
	public void setDiscount(Integer discount) {
		this.discount = discount;
	}
	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("hkgs", hkgs);
		obj.put("hbh", hbh);
		obj.put("qfsj", qfsj);
		obj.put("ddsj", ddsj);
		obj.put("rq", rq);
		obj.put("cfcs", cfcs);
		obj.put("ddcs", ddcs);
		obj.put("qfjc", qfjc);
		obj.put("ddjc", ddjc);
		obj.put("jg", jg);
		obj.put("zdl", zdl);
		obj.put("discount", discount);
		return obj;
	}
	
	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("hkgs", hkgs);
		obj.put("hbh", hbh);
		obj.put("qfsj", qfsj);
		obj.put("ddsj", ddsj);
		obj.put("rq", rq);
		if(cfcs!=null&&!cfcs.equals("")){//字典id值转label
			obj.put("cfcs", DictBuffer.getLabel("CCQ_CS", cfcs.toString()));
		}
		if(ddcs!=null&&!ddcs.equals("")){//字典id值转label
			obj.put("ddcs", DictBuffer.getLabel("CCQ_CS", ddcs.toString()));
		}
		obj.put("qfjc", qfjc);
		obj.put("ddjc", ddjc);
		obj.put("jg", jg);
		obj.put("zdl", zdl);
		obj.put("discount", discount);
		return obj;
	}
}
