package com.qtrmoon.slwldx.pojo;
import java.util.Date;
import java.util.Set;
import java.util.HashSet;
import org.json.simple.JSONObject;
import com.qtrmoon.common.PageForm;
import com.qtrmoon.toolkit.DateTransfer;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.dictionary.bsd.DictBean;

/** 物流资费信息实体类 */
public class Wlzfxx extends PageForm{
	//Fields
	
	private Integer id;// 主键
	private String sfcs;// 始发城市
	private String mdcs;// 目的城市
	private String wlgs;// 物流公司
	private Integer jg;// 价格
	//Constructors
	/** default constructor */
	public Wlzfxx() {
	
	}	
	//getter and setter
	/** 获取主键 */
	public Integer getId() {
		return this.id;
	}
	/** 设置主键 */
	public void setId(Integer id) {
		this.id = id;
	}
	/** 获取始发城市 */
	public String getSfcs() {
		return this.sfcs;
	}
	/** 设置始发城市 */
	public void setSfcs(String sfcs) {
		this.sfcs = sfcs;
	}
	/** 获取目的城市 */
	public String getMdcs() {
		return this.mdcs;
	}
	/** 设置目的城市 */
	public void setMdcs(String mdcs) {
		this.mdcs = mdcs;
	}
	/** 获取物流公司 */
	public String getWlgs() {
		return this.wlgs;
	}
	/** 设置物流公司 */
	public void setWlgs(String wlgs) {
		this.wlgs = wlgs;
	}
	/** 获取价格 */
	public Integer getJg() {
		return this.jg;
	}
	/** 设置价格 */
	public void setJg(Integer jg) {
		this.jg = jg;
	}
	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("sfcs", sfcs);
		obj.put("mdcs", mdcs);
		obj.put("wlgs", wlgs);
		obj.put("jg", jg);
		return obj;
	}
	
	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		if(sfcs!=null&&!sfcs.equals("")){//字典id值转label
			obj.put("sfcs", DictBuffer.getLabel("CCQ_QQCS", sfcs.toString()));
		}
		if(mdcs!=null&&!mdcs.equals("")){//字典id值转label
			obj.put("mdcs", DictBuffer.getLabel("CCQ_QQCS", mdcs.toString()));
		}
		if(wlgs!=null&&!wlgs.equals("")){//字典id值转label
			obj.put("wlgs", DictBuffer.getLabel("CCQ_WLGS", wlgs.toString()));
		}
		obj.put("jg", jg);
		return obj;
	}
}
