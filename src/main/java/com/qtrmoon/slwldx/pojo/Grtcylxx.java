package com.qtrmoon.slwldx.pojo;
import org.json.simple.JSONObject;

import com.qtrmoon.common.PageForm;
import com.qtrmoon.dictionary.DictBuffer;

/** 个人套餐用量信息实体类 */
public class Grtcylxx extends PageForm{
	//Fields
	
	private Integer id;// 主键
	private String sjh;// 手机号
	private String xm;// 姓名
	private Integer tcid;// 套餐ID
	private Integer dxsy;// 短信剩余（条数）
	private Integer gnllsy;// 国内流量剩余（M）
	private Integer bdllsy;// 本地流量剩余（M）
	private Integer bdthsy;// 本地通话剩余(分钟)
	private Integer qgthsy;// 全国通话剩余（分钟）
	private Float hfye;//话费余额（元）
	private String sfzh;//话费余额（元）
	private String xzlltc;//是否开通国际长途（元）
	private String gjctsj;//根据套餐新增资费（元）
	
	//Constructors
	/** default constructor */
	public Grtcylxx() {
	
	}	
	
	public Grtcylxx(Integer id, String sjh, String xm, Integer tcid,
			Integer dxsy, Integer gnllsy, Integer bdllsy, Integer bdthsy,
			Integer qgthsy, Float hfye, String sfzh) {
		super();
		this.id = id;
		this.sjh = sjh;
		this.xm = xm;
		this.tcid = tcid;
		this.dxsy = dxsy;
		this.gnllsy = gnllsy;
		this.bdllsy = bdllsy;
		this.bdthsy = bdthsy;
		this.qgthsy = qgthsy;
		this.hfye = hfye;
		this.sfzh = sfzh;
	}

	//getter and setter
	/** 获取主键 */
	public Integer getId() {
		return this.id;
	}
	/** 设置主键 */
	public void setId(Integer id) {
		this.id = id;
	}
	/** 获取手机号 */
	public String getSjh() {
		return this.sjh;
	}
	/** 设置手机号 */
	public void setSjh(String sjh) {
		this.sjh = sjh;
	}
	/** 获取姓名 */
	public String getXm() {
		return this.xm;
	}
	/** 设置姓名 */
	public void setXm(String xm) {
		this.xm = xm;
	}
	/** 获取套餐ID */
	public Integer getTcid() {
		return this.tcid;
	}
	/** 设置套餐ID */
	public void setTcid(Integer tcid) {
		this.tcid = tcid;
	}
	/** 获取短信剩余（条数） */
	public Integer getDxsy() {
		return this.dxsy;
	}
	/** 设置短信剩余（条数） */
	public void setDxsy(Integer dxsy) {
		this.dxsy = dxsy;
	}
	/** 获取国内流量剩余（M） */
	public Integer getGnllsy() {
		return this.gnllsy;
	}
	/** 设置国内流量剩余（M） */
	public void setGnllsy(Integer gnllsy) {
		this.gnllsy = gnllsy;
	}
	/** 获取本地流量剩余（M） */
	public Integer getBdllsy() {
		return this.bdllsy;
	}
	/** 设置本地流量剩余（M） */
	public void setBdllsy(Integer bdllsy) {
		this.bdllsy = bdllsy;
	}
	/** 获取本地通话剩余(分钟) */
	public Integer getBdthsy() {
		return this.bdthsy;
	}
	/** 设置本地通话剩余(分钟) */
	public void setBdthsy(Integer bdthsy) {
		this.bdthsy = bdthsy;
	}
	/** 获取全国通话剩余（分钟） */
	public Integer getQgthsy() {
		return this.qgthsy;
	}
	/** 设置全国通话剩余（分钟） */
	public void setQgthsy(Integer qgthsy) {
		this.qgthsy = qgthsy;
	}
	
	public Float getHfye() {
		return hfye;
	}
	/** 话费余额（元） */
	public void setHfye(Float hfye) {
		this.hfye = hfye;
	}
	public String getSfzh() {
		return sfzh;
	}
	public void setSfzh(String sfzh) {
		this.sfzh = sfzh;
	}
	public String getXzlltc() {
		return xzlltc;
	}
	public void setXzlltc(String xzlltc) {
		this.xzlltc = xzlltc;
	}
	public String getGjctsj() {
		return gjctsj;
	}
	public void setGjctsj(String gjctsj) {
		this.gjctsj = gjctsj;
	}

	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("sjh", sjh);
		obj.put("xm", xm);
		obj.put("tcid", tcid);
		obj.put("dxsy", dxsy);
		obj.put("gnllsy", gnllsy);
		obj.put("bdllsy", bdllsy);
		obj.put("bdthsy", bdthsy);
		obj.put("qgthsy", qgthsy);
		obj.put("hfye", hfye);
		obj.put("sfzh", sfzh);
		obj.put("xzlltc", xzlltc);
		obj.put("gjctsj", gjctsj);
		return obj;
	}
	
	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("sjh", sjh);
		obj.put("xm", xm);
		if(tcid!=null&&!tcid.equals("")){//字典id值转label
			obj.put("tcid", DictBuffer.getLabel("CCQ_TC", tcid.toString()));
		}
		obj.put("dxsy", dxsy);
		obj.put("gnllsy", gnllsy);
		obj.put("bdllsy", bdllsy);
		obj.put("bdthsy", bdthsy);
		obj.put("qgthsy", qgthsy);
		obj.put("hfye", hfye); 
		obj.put("sfzh", sfzh);
		obj.put("xzlltc", xzlltc);
		obj.put("gjctsj", gjctsj);
		return obj;
	}
}
