package com.qtrmoon.slwldx.pojo;
import java.util.Date;
import java.util.Set;
import java.util.HashSet;
import org.json.simple.JSONObject;
import com.qtrmoon.common.PageForm;
import com.qtrmoon.toolkit.DateTransfer;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.dictionary.bsd.DictBean;

/** 机票订单信息实体类 */
public class Jpddxx extends PageForm{
	//Fields
	
	private Integer id;// 主键
	private String ddh;// 订单号
	private String hbh;// 航班号
	private String hbrq;// 航班日期
	private String dprxm;// 订票人姓名
	private String dprxb;// 订票人性别
	private String dpsj;// 订票时间
	private String sfzh;// 身份证号
	private String dh;// 电话
	private String ddzt;// 订单状态
	//Constructors
	/** default constructor */
	public Jpddxx() {
	
	}	
	//getter and setter
	/** 获取主键 */
	public Integer getId() {
		return this.id;
	}
	/** 设置主键 */
	public void setId(Integer id) {
		this.id = id;
	}
	/** 获取订单号 */
	public String getDdh() {
		return this.ddh;
	}
	/** 设置订单号 */
	public void setDdh(String ddh) {
		this.ddh = ddh;
	}
	/** 获取航班号 */
	public String getHbh() {
		return this.hbh;
	}
	/** 设置航班号 */
	public void setHbh(String hbh) {
		this.hbh = hbh;
	}
	/** 获取航班日期 */
	public String getHbrq() {
		return this.hbrq;
	}
	/** 设置航班日期 */
	public void setHbrq(String hbrq) {
		this.hbrq = hbrq;
	}
	/** 获取订票人姓名 */
	public String getDprxm() {
		return this.dprxm;
	}
	/** 设置订票人姓名 */
	public void setDprxm(String dprxm) {
		this.dprxm = dprxm;
	}
	/** 获取订票人性别 */
	public String getDprxb() {
		return this.dprxb;
	}
	/** 设置订票人性别 */
	public void setDprxb(String dprxb) {
		this.dprxb = dprxb;
	}
	/** 获取订票时间 */
	public String getDpsj() {
		return this.dpsj;
	}
	/** 设置订票时间 */
	public void setDpsj(String dpsj) {
		this.dpsj = dpsj;
	}
	/** 获取身份证号 */
	public String getSfzh() {
		return this.sfzh;
	}
	/** 设置身份证号 */
	public void setSfzh(String sfzh) {
		this.sfzh = sfzh;
	}
	/** 获取电话 */
	public String getDh() {
		return this.dh;
	}
	/** 设置电话 */
	public void setDh(String dh) {
		this.dh = dh;
	}
	/** 获取订单状态 */
	public String getDdzt() {
		return this.ddzt;
	}
	/** 设置订单状态 */
	public void setDdzt(String ddzt) {
		this.ddzt = ddzt;
	}
	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("ddh", ddh);
		obj.put("hbh", hbh);
		obj.put("hbrq", hbrq);
		obj.put("dprxm", dprxm);
		obj.put("dprxb", dprxb);
		obj.put("dpsj", dpsj);
		obj.put("sfzh", sfzh);
		obj.put("dh", dh);
		obj.put("ddzt", ddzt);
		return obj;
	}
	
	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("ddh", ddh);
		obj.put("hbh", hbh);
		obj.put("hbrq", hbrq);
		obj.put("dprxm", dprxm);
		if(dprxb!=null&&!dprxb.equals("")){//字典id值转label
			obj.put("dprxb", DictBuffer.getLabel("ZD_XBNORMAL", dprxb.toString()));
		}
		obj.put("dpsj", dpsj);
		obj.put("sfzh", sfzh);
		obj.put("dh", dh);
		if(ddzt!=null&&!ddzt.equals("")){//字典id值转label
			obj.put("ddzt", DictBuffer.getLabel("CCQ_JPDDZT", ddzt.toString()));
		}
		return obj;
	}
}
