package com.qtrmoon.slwldx.pojo;
import org.json.simple.JSONObject;

import com.qtrmoon.common.PageForm;
import com.qtrmoon.dictionary.DictBuffer;

/** 套餐信息实体类 */
public class AddFare extends PageForm{
	//Fields
	
	private Integer jpddxxId;// 机票订单查询Id
	private Integer sfzh;// 身份证号
	private Integer dh;// 电话号
	private String dprxm;// 姓名
	private Integer dprxb;// 性别
	
	//无参
	public AddFare() {
	}
	/**
	 * 获取机票订单查询Id
	 * @return
	 */
	public Integer getJpddxxId() {
		return jpddxxId;
	}
	/**
	 * 设置机票订单查询Id
	 * @param jpddxxId
	 */
	public void setJpddxxId(Integer jpddxxId) {
		this.jpddxxId = jpddxxId;
	}
	
	/**
	 * 获取身份证号
	 * @return
	 */
	public Integer getSfzh() {
		return sfzh;
	}
	/**
	 * 设置身份证号
	 * @param sfzh
	 */
	public void setSfzh(Integer sfzh) {
		this.sfzh = sfzh;
	}
	/**
	 * 获取电话号
	 * @return
	 */
	public Integer getDh() {
		return dh;
	}
	/**
	 * 设置电话号
	 * @param dh
	 */
	public void setDh(Integer dh) {
		this.dh = dh;
	}
	/**
	 * 获取姓名
	 * @return
	 */
	public String getDprxm() {
		return dprxm;
	}
	/**
	 * 设置姓名
	 * @param dprxm
	 */
	public void setDprxm(String dprxm) {
		this.dprxm = dprxm;
	}
	/**
	 * 获取性别
	 * @return
	 */
	public Integer getDprxb() {
		return dprxb;
	}
	/**
	 * 设置性别
	 * @param dprxb
	 */
	public void setDprxb(Integer dprxb) {
		this.dprxb = dprxb;
	}

	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj=new JSONObject();
		obj.put("jpddxxid", getJpddxxId());
		obj.put("sfzh", sfzh);
		obj.put("dh", dh);
		obj.put("dprxm", dprxm);
		obj.put("dprxb", dprxb);
		return obj;
	}
	
	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj=new JSONObject();
		obj.put("jpddxxid", getJpddxxId());
		obj.put("sfzh", sfzh);
		obj.put("dh", dh);
		obj.put("dprxm", dprxm);
		obj.put("dprxb", dprxb);
		return obj;
	}
}
