package com.qtrmoon.slwldx.pojo;

import java.util.List;

/** 多个乘客信息*/
public class JpddParam{
	
	private List<Jpddxx>  jpddList;//多个乘客信息提交用到的list

	//Constructors
	public JpddParam() {
	}
	
	//getter 乘客信息list
	public List<Jpddxx> getJpddList() {
		return jpddList;
	}

	//setter 乘客信息list
	public void setJpddList(List<Jpddxx> jpddList) {
		this.jpddList = jpddList;
	}
	
}
