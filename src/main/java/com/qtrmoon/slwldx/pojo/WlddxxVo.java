package com.qtrmoon.slwldx.pojo;
import java.util.Date;
import java.util.Set;
import java.util.HashSet;

import org.json.simple.JSONObject;

import com.qtrmoon.common.PageForm;
import com.qtrmoon.toolkit.DateTransfer;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.dictionary.bsd.DictBean;

/** 物流订单信息实体类 */
public class WlddxxVo extends PageForm{
	//Fields
	private String ddh;// 订单号
	private String wlgs;//物流公司
	private Integer ddjg;// 订单价格
	private String jjr;// 寄件人
	private String sjr;// 收件人
	private String jjrdh;// 寄件人电话
	private String sjrdh;// 收件人电话
	private String sfcs;// 始发城市
	private String mdcs;// 目的城市
	private String sfcsxxdz;// 始发城市详细地址
	private String mdcsxxdz;// 目的城市详细地址
	
	//Constructors
	/** default constructor */
	public WlddxxVo() {
	
	}	
	//getter and setter
	/** 获取订单号 */
	public String getDdh() {
		return this.ddh;
	}
	/** 设置订单号 */
	public void setDdh(String ddh) {
		this.ddh = ddh;
	}
	public String getWlgs() {
		return wlgs;
	}
	/** 设置物流公司 */
	public void setWlgs(String wlgs) {
		this.wlgs = wlgs;
	}
	/** 获取物流公司 */
	public Integer getDdjg() {
		return this.ddjg;
	}
	/** 设置订单价格 */
	public void setDdjg(Integer ddjg) {
		this.ddjg = ddjg;
	}
	/** 获取寄件人 */
	public String getJjr() {
		return this.jjr;
	}
	/** 设置寄件人 */
	public void setJjr(String jjr) {
		this.jjr = jjr;
	}
	/** 获取收件人 */
	public String getSjr() {
		return this.sjr;
	}
	/** 设置收件人 */
	public void setSjr(String sjr) {
		this.sjr = sjr;
	}
	/** 获取寄件人电话 */
	public String getJjrdh() {
		return this.jjrdh;
	}
	/** 设置寄件人电话 */
	public void setJjrdh(String jjrdh) {
		this.jjrdh = jjrdh;
	}
	/** 获取收件人电话 */
	public String getSjrdh() {
		return this.sjrdh;
	}
	/** 设置收件人电话 */
	public void setSjrdh(String sjrdh) {
		this.sjrdh = sjrdh;
	}
	/** 获取始发城市 */
	public String getSfcs() {
		return this.sfcs;
	}
	/** 设置始发城市 */
	public void setSfcs(String sfcs) {
		this.sfcs = sfcs;
	}
	/** 获取目的城市 */
	public String getMdcs() {
		return this.mdcs;
	}
	/** 设置目的城市 */
	public void setMdcs(String mdcs) {
		this.mdcs = mdcs;
	}
	/** 获取始发城市详细地址 */
	public String getSfcsxxdz() {
		return sfcsxxdz;
	}
	/** 设置始发城市详细地址 */
	public void setSfcsxxdz(String sfcsxxdz) {
		this.sfcsxxdz = sfcsxxdz;
	}
	/** 获取目的城市详细地址 */
	public String getMdcsxxdz() {
		return mdcsxxdz;
	}
	/** 设置目的城市详细地址 */
	public void setMdcsxxdz(String mdcsxxdz) {
		this.mdcsxxdz = mdcsxxdz;
	}
	
	/** 获取原始数据的Json对象(未替换字典，修改用) */
	@SuppressWarnings("unchecked")
	public JSONObject getJson() {
		JSONObject obj=new JSONObject();
		obj.put("ddh", ddh);
		obj.put("wlgs", wlgs);
		obj.put("ddjg", ddjg);
		obj.put("jjr", jjr);
		obj.put("sjr", sjr);
		obj.put("jjrdh", jjrdh);
		obj.put("sjrdh", sjrdh);
		obj.put("sfcs", sfcs);
		obj.put("mdcs", mdcs);
		obj.put("sfcsxxdz", sfcsxxdz);
		obj.put("mdcsxxdz", mdcsxxdz);
		return obj;
	}
	
	/** 获取数据的Json对象(已替换字典，列表查询用) */
	@SuppressWarnings("unchecked")
	public JSONObject getJsonInDict() {
		JSONObject obj=new JSONObject();
		obj.put("ddh", ddh);
		obj.put("wlgs", wlgs);
		obj.put("ddjg", ddjg);
		obj.put("jjr", jjr);
		obj.put("sjr", sjr);
		obj.put("jjrdh", jjrdh);
		obj.put("sjrdh", sjrdh);
		obj.put("sfcsxxdz", sfcsxxdz);
		obj.put("mdcsxxdz", mdcsxxdz);
		if(sfcs!=null&&!sfcs.equals("")){//字典id值转label
			obj.put("sfcs", DictBuffer.getLabel("CCQ_QQCS", sfcs.toString()));
		}
		if(mdcs!=null&&!mdcs.equals("")){//字典id值转label
			obj.put("mdcs", DictBuffer.getLabel("CCQ_QQCS", mdcs.toString()));
		}
		return obj;
	}
}
