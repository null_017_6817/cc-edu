package com.qtrmoon.paper.ctrl;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.jdom.JDOMException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.qtrmoon.book.util.FilePath;
import com.qtrmoon.common.BaseController;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.examination.pojo.ExerAnswer;
import com.qtrmoon.examination.serdao.IExaminationService;
import com.qtrmoon.examination.util.TopicTypeTransUtil;
import com.qtrmoon.manager.constant.TopicTypeConstant;
import com.qtrmoon.manager.pojo.Bank;
import com.qtrmoon.manager.pojo.BankStep;
import com.qtrmoon.manager.pojo.BankTopic;
import com.qtrmoon.manager.pojo.Person;
import com.qtrmoon.manager.serdao.IManagerService;
import com.qtrmoon.paper.ext.BankStepExt;
import com.qtrmoon.paper.serdao.IPaperService;
import com.qtrmoon.sysmanage.SysConstant;
import com.qtrmoon.sysmanage.pojo.Role;
import com.qtrmoon.sysmanage.serdao.ISysmanageService;

/**
 * 创建问卷控制器
 * @Class Name QuestionController
 */
@Controller
@RequestMapping("/paper/question")
public class QuestionController extends BaseController {

	@Autowired
	private IExaminationService examinationService;// 声明模块Service实例
	@Autowired
	private IPaperService paperService;// 声明模块Service实例
	@Autowired
	private IManagerService managerService; // 声明模块Service实例
	@Autowired
	private ISysmanageService sysmanageService;//注入service

	// ===========================Exer开始=========================================

	/**
	 * 查询问卷
	 * @param exer
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schQuestion.action")
	public void schQuestion(Bank bank, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		noCache(response);// ajax无缓存
		bank.setCondition(" order by bank_id desc"); // 防sql注入
		bank.assLike("vccId", "bankId", "bankName", "profession", "remark",
				"createTime");// 拼接%

		List<Bank> list = managerService.schBank(bank); // 调用Service查询数据
		/* 将数据Bean的集合转换为Json集合 */
		JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
		JSONObject obj; // 声明用于构造Bean的Json对象
		for (Bank u : list) { // 循环查询的数据集构造Json数据集
			
			obj = u.getJsonInDict(); // 将Bean转换为Json对象
			if(u != null && u.getBankId() != null){
				BankTopic bt = new BankTopic();
				bt.setBankId(u.getBankId());
				List<BankTopic> bts = managerService.schBankTopicList(bt);
				if(bts != null && bts.size() > 0){
					obj.put("topicNums", bts.size());
				}else{
					obj.put("topicNums", 0);
				}
			}
			obj.put("_oper", u.getBankId()); // 为"操作列"设定主键值
			datas.add(obj); // 添加Json对象到Json集合
		}
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		JSONObject res = new JSONObject(); // 声明页面返回Json
		res.put("total", bank.getDatasize()); // 设置数据总记录数
		res.put("rows", datas); // 设置数据集
		response.getWriter().print(res.toJSONString()); // 输出到页面
	}

	/**
	 * 预添加or预修改or查看方法
	 * @param exer
	 * 预添加时可传入一些初始值；预修改和查看时用来承载主键值
	 * @param toL
	 * 挂字典的字段是否需要转为字典Label值。true则进行转换
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/vieQuestion.action")
	public void vieQuestion(Bank bank, Boolean toL, HttpServletResponse response)
			throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (toL != null && toL) { // 查看页面加载数据时调用，返回的对象已进行字典转换
			if (bank.getBankId() != null && !"".equals(bank.getBankId())) { // 检测主键非空
				bank = managerService.schBankById(bank.getBankId()); // 按主键查询数据
				response.getWriter().print(bank.getJsonInDict()); // 输出经过字典转换Json对象到查看页(vie_)
			}
		} else { // 修改页面加载数据时调用
			if (bank.getBankId() != null && !"".equals(bank.getBankId())) { // 检查主键非空
				bank = managerService.schBankById(bank.getBankId()); // 按主键查询数据
				if(bank != null){
					response.getWriter().print(bank.getJson()); // 输出Json对象到修改页(upd_)
				}else{
					response.getWriter().print(new Bank().getJson()); // 输出Json对象到修改页(upd_)
				}
			} else { // 添加页面加载数据时调用，可设置添加页面中一些属性的预设值。
				response.getWriter().print(new Bank().getJson()); // 输出Json对象到添加页(upd_)
			}
		}
	}

	/**
	 * 添加或修改问卷/练习 
	 * @param exer
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/updQuestion.action")
	public void updQuestion(Bank bank, HttpServletResponse response)
			throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (StringUtils.isBlank(bank.getBankId())) { // 检测主键为空则
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String now = sdf.format(new Date());
			bank.setCreateTime(now);
			bank.setType("1");// 0:老数据，1：新数据
			managerService.addBank(bank); // 添加数据
		} else { // 主键非空则
			managerService.updBank(bank); // 修改数据
		}
		DictBuffer.updCache("ZD_BANK");// 更新缓存
	}

	/**
	 * 删除 练习/问卷 
	 * @param ids
	 * @param response
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value = "/delQuestion.action")
	public Map<String, Object> delExer(
			@RequestParam(value = "ids[]") String[] ids,
			HttpServletResponse response) {
		noCache(response); // Ajax方法设定无缓存
		Map<String, Object> map = new HashMap<String, Object>();// 返回集合
		try {
			String eids = "";// 定义常量
			if (ids != null && ids.length > 0) {// 判断
				for (int i = 0; i < ids.length; i++) {// 循环
					if (ids[i] != null && !"".equals(ids[i])) {// 判断
						if ("".equals(eids)) {// 判断
							eids = "'" + ids[i] + "'";// 拼接
						} else {
							eids = eids + "," + "'" + ids[i] + "'";// 拼接
						}
					}
				}
				BankTopic bankTopic = new BankTopic();// 定义对象
				bankTopic.setCondition("AND bank_id in (" + eids + ")");// 设置sql
				List<BankTopic> list = managerService.schBankTopic(bankTopic); // 调用Service查询数据
				if (list != null && list.size() > 0) {// 判断
					map.put("success", false);//设置属性
					map.put("msg", "所选练习含有试题，禁止删除");//设置属性
				} else {
					managerService.delBank(ids); // 删除数据
					map.put("success", true);//设置属性
					map.put("msg", "删除成功");//设置属性
				}
				DictBuffer.updCache("ZD_BANK");// 更新缓存
			} else {
				map.put("success", false);//设置属性
				map.put("msg", "参数为空，删除失败");//设置属性
			}
		} catch (Exception e) {
			e.printStackTrace();//打印异常
			map.put("success", false);//设置属性
			map.put("msg", "删除异常");//设置属性
		} finally {
			return map;// 返回结果
		}
	}

	// ===========================Exer结束=========================================

	// ===========================Topic开始=========================================
	/**
	 * 查询题目列表 
	 * @param exerTopic
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schBankTopic.action")
	public void schBankTopic(BankTopic bankTopic, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		noCache(response);// ajax无缓存
		bankTopic.setCondition(""); // 防sql注入
		bankTopic.assLike("vccId", "topicId", "title", "type", "level",// 拼接%
				"difficulty", "author", "creattime", "remark", "beginStepId",// 拼接%
				"createTime", "docATitle", "docBTitle", "docCTitle",// 拼接%
				"docDTitle", "docETitle");// 拼接%
		
		if(StringUtils.isEmpty(bankTopic.getOrderCol()) || ",".equals(bankTopic.getOrderCol())){
			bankTopic.setOrderCol("topic_id");
			bankTopic.descOrder();
		}
		
		List<BankTopic> list = managerService.schBankTopic(bankTopic); // 调用Service查询数据
		/* 将数据Bean的集合转换为Json集合 */
		JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
		JSONObject obj;
		for (BankTopic u : list) { // 循环查询的数据集构造Json数据集
			obj = u.getJsonInDict(); // 将Bean转换为Json对象
			obj.put("_oper", u.getTopicId()); // 为"操作列"设定主键值
			datas.add(obj); // 添加Json对象到Json集合
		}
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		JSONObject res = new JSONObject(); // 声明页面返回Json
		res.put("total", bankTopic.getDatasize()); // 设置数据总记录数
		res.put("rows", datas); // 设置数据集
		response.getWriter().print(res.toJSONString()); // 输出到页面
	}

	/**
	 * 修改或查看预处理 
	 * @param exerTopic
	 * @param toL
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/vieBankTopic.action")
	public void vieBankTopic(BankTopic bankTopic, Boolean toL,
			HttpServletResponse response) throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (toL != null && toL) { // 查看页面加载数据时调用，返回的对象已进行字典转换
			if (bankTopic.getTopicId() != null
					&& !"".equals(bankTopic.getTopicId())) { // 检测主键非空
				bankTopic = managerService.schBankTopicById(bankTopic
						.getTopicId()); // 按主键查询数据
				response.getWriter().print(bankTopic.getJsonInDict()); // 输出经过字典转换Json对象到查看页(vie_)
			}
		} else { // 修改页面加载数据时调用
			if (bankTopic.getTopicId() != null
					&& !"".equals(bankTopic.getTopicId())) { // 检查主键非空
				bankTopic = managerService.schBankTopicById(bankTopic
						.getTopicId()); // 按主键查询数据
				response.getWriter().print(bankTopic.getJson()); // 输出Json对象到修改页(upd_)
			} else { // 添加页面加载数据时调用，可设置添加页面中一些属性的预设值。
				BankTopic topic = new BankTopic();
				topic.setBankId(bankTopic.getBankId());
				response.getWriter().print(topic.getJson()); // 输出Json对象到添加页(upd_)
			}
		}
	}

	/**
	 * 修改数据 
	 * @param exerTopic
	 * @param response
	 * @throws IOException
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 * @throws JDOMException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/saveTopicAndStep.action")
	public void saveTopicAndStep(BankTopic topic, 
			String topicIdParam,@RequestBody BankStepExt bkstep, HttpServletRequest request,
			HttpServletResponse response) throws IOException,
			IllegalAccessException, InvocationTargetException, JDOMException {
		noCache(response); // Ajax方法设定无缓存
		String stepType = request.getParameter("stepType");// 获取参数
		JSONObject obj = new JSONObject();// 定义对象
		BankStep bankStep = new BankStep();
		String[] params = null;
		try {
			if (topic != null) { // 检测主键为空则
				if(bkstep != null){
					params = bkstep.getStepXmls();
				}
				if (bankStep != null && bkstep.getStepTtitle() != null// 检测参数是否为空
						&& !"".equals(bkstep.getStepTtitle())) {// 检测参数是否为空
					bankStep.setParam(bkstep.getParam());
					bankStep.setStandardAnswer(bkstep.getStandardAnswer());
					bankStep.setTitle(bkstep.getStepTtitle());
					bankStep.setType(stepType);//设置属性
				} else {
					bankStep = null;// 赋值null
				}
				Map<String,Object> result = paperService.addTopicAndStep(topic, params, bankStep,// 调用service
						topicIdParam);// 调用service
				obj.put("success", true);//设置属性
				obj.put("msg", "操作成功");//设置属性
				if(result != null && result.get("msg") != null
						&& !"".equals(result.get("msg").toString())){
					obj.put("success", false);//设置属性
					obj.put("msg", result.get("msg"));//设置属性
				}
			} else {
				obj.put("success", false);//设置属性
				obj.put("msg", "topic不能为null");//设置属性
			}
		} catch (Exception e) {
			e.printStackTrace();// 打印异常
			obj.put("success", false);//设置属性
			obj.put("msg", "操作异常");//设置属性
		} finally {
			response.getWriter().print(obj.toJSONString());// 返回json数据
		}
	}

	/**
	 * 获取topic和step 
	 * @param topicId
	 * @param request
	 * @param response
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value = "getTopicAndStep.action")
	public Map<String, Object> getTopicAndStep(String topicId,
			HttpServletRequest request, HttpServletResponse response) {
		noCache(response);// ajax无缓存
		Map<String, Object> map = new HashMap<String, Object>();// 返回集合
		try {
			String projectName = request.getContextPath();// 获取参数
			Map<String, Object> data = paperService.getTopicAndStep(topicId,// 调用service
					projectName);// 调用service
			map.put("msg", "操作成功");//设置属性
			map.put("data", data);//设置属性
			map.put("topicTypeMap", TopicTypeTransUtil.getTopicTypeToMap());// 题目类型集合
			map.put("stepTypeMap", TopicTypeTransUtil.getStepTypeToMap());// 步骤类型集合
			map.put("success", true);//设置属性
		} catch (Exception e) {
			e.printStackTrace();// 打印异常
			map.put("success", false);//设置属性
			map.put("msg", "操作异常");//设置属性
		} finally {
			return map;// 返回json数据
		}
	}

	/**
	 * 删除topic 
	 * @param ids
	 * @param response
	 */
	@RequestMapping(value = "/delExerTopic.action")
	public void delExerTopic(@RequestParam(value = "ids[]") String[] ids,
			HttpServletResponse response) {
		noCache(response); // Ajax方法设定无缓存
		managerService.delBankTopic(ids); // 删除数据
	}

	// ===========================Topic结束=========================================

	/**
	 * 上传录音 
	 * @param file
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/uploadAudio.action")
	@ResponseBody
	public Map<String, Object> uploadVoice(
			@RequestParam(value = "audioData", required = false) CommonsMultipartFile file,
			String bankId, String topicId, HttpServletRequest request) {
		Map<String, Object> map = new HashMap<String, Object>();// 返回集合
		try {
			String projectName = request.getContextPath();// 获取参数
			String path = FilePath.getFilePath("dataAudioAddress") + bankId// 拼接参数
					+ File.separator + topicId;// 拼接参数
			int rand = (int) (Math.random() * 10) + 10;// 两位随机数
			Date now = new Date();// 当前时间
			String pattern = ".wav";//文件格式
			String fullName = now.getTime() + rand + pattern;// 拼接参数
			String showpath = projectName// 拼接参数
					+ "/paper/question/appendUrl.action?filename=" + bankId// 拼接参数
					+ File.separator + topicId + File.separator + fullName;// 拼接参数
			File targetFile = new File(path, fullName);// 文件对象
			if (!targetFile.exists()) {// 判断
				targetFile.mkdirs();// 创建文件夹
			}
			file.transferTo(targetFile);//上传文件
			map.put("success", true);// 设置属性
			map.put("fileUrl", showpath);// 设置属性
			map.put("fullName", fullName);// 设置属性
		} catch (Exception e) {
			e.printStackTrace();// 打印异常
			map.put("success", false);// 设置属性
		}
		return map;// 返回结果
	}

	/**
	 * 上传录音 
	 * @param file
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/handleUploadVoice.action")
	public Map<String, Object> handleUploadVoice(
			@RequestParam(value = "file", required = false) CommonsMultipartFile file,
			String bankId, String topicId, HttpServletRequest request) {
		Map<String, Object> map = new HashMap<String, Object>();// 返回集合
		try {
			String projectName = request.getContextPath();// 获取参数
			String path = FilePath.getFilePath("dataAudioAddress") + bankId// 拼接参数
					+ File.separator + topicId;// 拼接参数
			int rand = (int) (Math.random() * 10) + 10;// 两位随机数
			Date now = new Date();// 当前时间
			String pattern = ".wav";//文件格式
			String fullName = now.getTime() + rand + pattern;// 拼接参数
			String showpath = projectName// 拼接参数
					+ "/paper/question/appendUrl.action?filename=" + bankId// 拼接参数
					+ File.separator + topicId + File.separator + fullName;// 拼接参数
			File targetFile = new File(path, fullName);// 文件对象
			if (!targetFile.exists()) {// 判断
				targetFile.mkdirs();// 创建文件夹
			}
			file.transferTo(targetFile);//上传文件
			map.put("success", true);// 设置属性
			map.put("fileUrl", showpath);// 设置属性
			map.put("fullName", fullName);// 设置属性
		} catch (Exception e) {
			e.printStackTrace();// 打印异常
			map.put("success", false);// 设置属性
		}
		return map;// 返回结果
	}

	/**
	 * 上传录压缩包
	 * @param file
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/handleUploadZip.action")
	public Map<String, Object> handleUploadZip(
			@RequestParam(value = "zipFile", required = false) CommonsMultipartFile zipFile,
			String bankId, String topicId, HttpServletRequest request) {
		Map<String, Object> map = new HashMap<String, Object>();// 返回集合
		try {
			if(zipFile != null && !StringUtils.isEmpty( zipFile.getOriginalFilename() )){
				
				String filename = zipFile.getOriginalFilename();//文件原始名称
				
				String pattern = "";// 定义常量
				if ((filename != null) && (filename.length() > 0)) { // 判断
					int dot = filename.lastIndexOf('.');   // 截取
					if ((dot >-1) && (dot < (filename.length() - 1))) {  // 判断 
						pattern = filename.substring(dot + 1);   // 截取
					}
				}
				
				String projectName = request.getContextPath();// 获取参数
				String path = FilePath.getFilePath("dataAudioAddress") + bankId// 拼接参数
						+ File.separator + topicId;// 拼接参数
				int rand = (int) (Math.random() * 10) + 10;// 两位随机数
				Date now = new Date();// 当前时间
				String fullName = now.getTime() + rand + "." + pattern;// 拼接参数
				String showpath = projectName// 拼接参数
						+ "/paper/question/downloadZip.action?filename=" + bankId// 拼接参数
						+ File.separator + topicId + File.separator + fullName;// 拼接参数
				File targetFile = new File(path, fullName);// 文件对象
				if (!targetFile.exists()) {// 判断
					targetFile.mkdirs();// 创建文件夹
				}
				
				zipFile.transferTo(targetFile);//上传文件
				map.put("success", true);// 成功标识
				map.put("fileUrl", showpath);// 文件访问（下载路径）
				map.put("fullName", fullName);// 修改后的名字
				map.put("origiName", filename);// 文件原名称
			}
		} catch (Exception e) {
			e.printStackTrace();// 打印异常
			map.put("success", false);// 设置属性
		}
		return map;// 返回结果
	}
	
	/**
	 * 上传压缩包(学生答案)
	 * @param file
	 * @param request
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value = "/uploadZipAnswer.action")
	public Map<String, Object> uploadZipAnswer(
			@RequestParam(value = "zipFile", required = false) CommonsMultipartFile zipFile,
			String eid, String etid,String esid, HttpServletRequest request) {
		Map<String, Object> map = new HashMap<String, Object>();// 返回集合
		try {
			Person person = (Person)request.getSession(true).getAttribute(SysConstant.CURRENT_USER);
			if(person != null && StringUtils.isNotEmpty(person.getNumber()) 
					&& zipFile != null && !StringUtils.isEmpty( zipFile.getOriginalFilename() )){
				String filename = zipFile.getOriginalFilename();//文件原始名称
				filename = filename == null? "" : filename;
				String suffix = "";// 定义常量
				if ((filename != null) && (filename.length() > 0)) {  // 判断 
					int dot = filename.lastIndexOf('.');  // 截取 
					if ((dot >-1) && (dot < (filename.length() - 1))) {  // 判断 
						suffix = filename.substring(dot + 1); // 截取  
					}
				}
				if( suffix != null ){
					String epath =  File.separator + eid	// 试卷ID
							+ File.separator + etid	// 题目ID
							+ File.separator + esid // 步骤ID
							+ File.separator + person.getNumber();// 学生学号
					String path = FilePath.getFilePath("stuzippath")+epath;//路径前缀，配置文件中
					Calendar calendar = Calendar.getInstance();// 调用Instance
					SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSSS");// 格式刷时间
					String time = sdf.format(calendar.getTime());// 格式刷时间
					String fullName = time;
					if( !"".equals(suffix) ){
						fullName += "." + suffix;// 拼接参数
					}
					File targetFile = new File(path, fullName);// 文件对象
					if (!targetFile.exists()) {// 判断
						targetFile.mkdirs();// 创建文件夹
					}
					if(eid != null && etid != null && esid != null && person.getNumber() != null){
						ExerAnswer exerAnswer = new ExerAnswer();
						exerAnswer.setEid(Integer.valueOf(eid));
						exerAnswer.setEtid(Integer.valueOf(etid));
						exerAnswer.setEsid(Integer.valueOf(esid));
						exerAnswer.setEsid(Integer.valueOf(esid));
						exerAnswer.setStudNumber(person.getNumber());
						List<ExerAnswer> eas = examinationService.schExerAnswer(exerAnswer);
						if(eas != null && eas.size() > 0 && eas.get(0) != null ){
							ExerAnswer ea = eas.get(0);
							ea.setFileUrl(fullName);
							ea.setFileOriginalName(filename);
							ea.setUploadTime(new Date());
							examinationService.updExerAnswer(ea);
						}
					}
					
					zipFile.transferTo(targetFile);//上传文件
					map.put("success", true);// 成功标识
					map.put("fullName", fullName);// 修改后的名字
					map.put("origiName", filename);// 文件原名称
					map.put("msg", "上传文件成功，文件名称："+filename);// 文件原名称
				}else{
					map.put("success", false);// 成功标识
					map.put("msg", "上传失败,非正常格式");// 文件原名称
				}
			}else{
				map.put("success", false);// 成功标识
				map.put("msg", "上传失败，失败原因[必要参数缺失]");// 文件原名称
			}
		} catch (Exception e) {
			e.printStackTrace();// 打印异常
			map.put("success", false);// 设置属性
			map.put("msg", "上传失败，失败原因[服务器异常]");// 设置属性
		}finally{
			return map;// 返回结果
		}
	}
	
	@RequestMapping(value = "/downZipAnswer.action")
	public void downZipAnswer(String eid,String etid,String esid,String studNum,
			HttpServletRequest request,HttpServletResponse response) {
		
		if(eid != null && !"".equals(eid)
			&& etid != null && !"".equals(etid)
			&& etid != null && !"".equals(etid)
			&& esid != null && !"".equals(esid)
			&& studNum != null && !"".equals(studNum)){
			
			
			ExerAnswer exerAnswer = new ExerAnswer();
			exerAnswer.setEid(Integer.valueOf(eid));
			exerAnswer.setEtid(Integer.valueOf(etid));
			exerAnswer.setEsid(Integer.valueOf(esid));
			exerAnswer.setStudNumber(studNum);
			List<ExerAnswer> eas = examinationService.schExerAnswer(exerAnswer);
			ExerAnswer ea = null;
			if(eas != null && eas.size() > 0 && eas.get(0) != null){
				ea = eas.get(0);
			}
			
			try{
				OutputStream toClient = new BufferedOutputStream(
						response.getOutputStream());// 输出流
				if(ea != null && ea.getFileUrl() != null && ea.getFileOriginalName() != null){
					String epath =  File.separator + eid	// 试卷ID
							+ File.separator + etid	// 题目ID
							+ File.separator + esid // 步骤ID
							+ File.separator + studNum;// 学生学号
					
					String fileUrl = ea.getFileUrl();
					String fileOriginalName = ea.getFileOriginalName();
					
					String fullPath = FilePath.getFilePath("stuzippath")+epath+File.separator+fileUrl;
					File file = new File(fullPath);
					
					BufferedInputStream fis = new BufferedInputStream(
							new FileInputStream(fullPath));// 输入流
					byte[] buffer = new byte[fis.available()];// 定义字节
					fis.read(buffer);// 读
					fis.close();// 关闭
					// 清空response
					response.reset();
					// 设置response的Header
					response.addHeader("Content-Disposition", "attachment;filename="
							+ new String(fileOriginalName.getBytes(), "ISO-8859-1"));// 设置response的Header
					response.addHeader("Content-Length", "" + file.length());// 设置response的Header
					
					response.setContentType("application/vnd.ms-excel;charset=utf-8");// 设置属性
					toClient.write(buffer);// 写
					toClient.flush();// 刷新
					toClient.close();// 关闭
				}else{
					String msg = "<div style='text-align: center;padding-top: 15%;'><span style='text-align: center;"
							+ "color: red;font-size: 20px;'>文件不存在，下载失败!<span></div>";
					response.setContentType("text/html");//设置contentType属性
					toClient = new BufferedOutputStream(
							response.getOutputStream());// 输出流
					toClient.write(msg.getBytes());// 写
					toClient.flush();// 刷新
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 下载实操题的压缩包
	 * @param fileName
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/downloadZip.action")
	public void downloadZip( HttpServletRequest request , 
			HttpServletResponse response){
		String bankId = request.getParameter("bankId");// 题库id
		String topicId = request.getParameter("topicId");// 题目id
		String fileName = request.getParameter("fileName");// 系统文件名称
		String origiName = request.getParameter("origiName");// 源文件名称
		if( !StringUtils.isBlank(fileName) && !StringUtils.isBlank(bankId)
				&& !StringUtils.isBlank(topicId)&& !StringUtils.isBlank(origiName)){
			String path = bankId+File.separator+topicId+File.separator+fileName;
			String fullPath = FilePath.getFilePath("dataAudioAddress")+path;
			try{
				File file = new File(fullPath);
				OutputStream toClient = null;
				if(file.exists()){
					BufferedInputStream fis = new BufferedInputStream(
							new FileInputStream(fullPath));// 输入流
					byte[] buffer = new byte[fis.available()];// 定义字节
					fis.read(buffer);// 读
					fis.close();// 关闭
					// 清空response
					response.reset();
					// 设置response的Header
					response.addHeader("Content-Disposition", "attachment;filename="
							+ new String(origiName.getBytes(), "ISO-8859-1"));// 设置response的Header
					response.addHeader("Content-Length", "" + file.length());// 设置response的Header
					toClient = new BufferedOutputStream(
							response.getOutputStream());// 输出流
					response.setContentType("application/vnd.ms-excel;charset=utf-8");// 设置属性
					toClient.write(buffer);// 写
					toClient.flush();// 刷新
					toClient.close();// 关闭
				}else{
					String msg = "<div style='text-align: center;padding-top: 15%;'><span style='text-align: center;"
							+ "color: red;font-size: 20px;'>文件不存在，下载失败!<span></div>";
					response.setContentType("text/html");//设置contentType属性
					toClient = new BufferedOutputStream(
							response.getOutputStream());// 输出流
					toClient.write(msg.getBytes());// 写
					toClient.flush();// 刷新
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * JSP页面跳转
	 * @param page
	 * jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value = "/page.action")
	public String goPage(String page, String id, Model model,HttpServletRequest request) { // 放过本Ctrl的页面，不合法的跳转到error页。
		if ("index".equals(page) || "question_sch".equals(page)// 判断
				|| "question_vie".equals(page) || "question_upd".equals(page)// 判断
				|| "topic_sch".equals(page) || "topic_vie".equals(page)// 判断
				|| "topic_upd".equals(page)) {// 判断
			Person person = (Person)request.getSession(true).getAttribute(SysConstant.CURRENT_USER);
			Role role=sysmanageService.schRoleById(Integer.parseInt(person.getRoles()));
			if("内部人员".equals(role.getName())){
				model.addAttribute("export", "yes");// 设置参数
			}else{
				model.addAttribute("export", "no");// 设置参数
			}
			model.addAttribute("paramId", id);// 设置参数
			return "/question/" + page;// 返回页面
		} else {
			return "/error";// 返回错误页面
		}
	}

	/**
	 * 拼接录音文件 
	 * @param filename
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/appendUrl.action")
	public void appendUrl(String filename, HttpServletRequest request,
			HttpServletResponse response) {
		if (filename != null) {// 判断是否为空
			String path = FilePath.getFilePath("dataAudioAddress") + filename;// 获取路径
			try {
				File file = new File(path);// 文件对象
				if (file.exists()) {// 判断
					FileInputStream in = new FileInputStream(path);// 文件对象输入流
					ServletOutputStream out = response.getOutputStream();// 输出流
					IOUtils.copy(in, out);// 以流的形式展示到前台
					IOUtils.closeQuietly(in);// 关闭流
					IOUtils.closeQuietly(out);// 关闭流
				} else {
					logger.info("文件不存在！不产生数据流输出到前台页面。缺失文件及路径为：" + path);// log
				}
			} catch (Exception e) {
				e.printStackTrace();// 打印异常
			}
		}
	}

	/**
	 * html文件夹 
	 * @param filename
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/appendHtmlUrl.action")
	public void appendHtmlUrl(String eid, String etid, String etsid,
			HttpServletRequest request, HttpServletResponse response) {
		String path = FilePath.getFilePath("freemarkerHtml") + File.separator// 拼接参数
				+ "eid" + eid + File.separator + etid + "_" + etsid + ".html";// 拼接参数
		try {
			File file = new File(path);// 文件对象
			if (file.exists()) {// 判断
				ServletOutputStream out = response.getOutputStream();// 输出流
				String str = getStrByFile(file,request);
				InputStream in = new ByteArrayInputStream( str.getBytes());
				IOUtils.copy(in, out);// 以流的形式展示到前台
				IOUtils.closeQuietly(in);// 关闭流
				IOUtils.closeQuietly(out);// 关闭流
			} else {
				logger.info("文件不存在！不产生数据流输出到前台页面。缺失文件及路径为：" + path);// log
			}
		} catch (Exception e) {
			e.printStackTrace();//异常信息
		}
	}

	// 根据html文件转换成字符串
	@SuppressWarnings("finally")
	private String getStrByFile(File file,HttpServletRequest request){
		String result = "";
		try{
			FileInputStream in = new FileInputStream(file);
			int size = in.available();
			byte[] buffer = new byte[size];
			in.read(buffer);
			in.close();
			result = new String(buffer,"utf-8");
			
			String stepType = request.getParameter("stepType");
			if(stepType != null && !"".equals(stepType) && 
					!TopicTypeConstant.CLOZE.equals(stepType)){
				result = result.replace("/cc-edu/paper/question/appendUrl.action?filename=","");
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			return result;
		}
	}
	
	/**
	 * 获取新topic
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value = "/getNewTopicId.action")
	public Map<String, Object> getNewTopicId() {
		Map<String, Object> result = new HashMap<String, Object>();// 返回结果对象
		try {
			// 18位时间戳
			Calendar calendar = Calendar.getInstance();// 获取时间
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSSS");// 时间格式
			String topicId = sdf.format(calendar.getTime());// 格式化时间
			int ran = (int) (Math.random() * 10) + 10;// 两位随机数
			topicId = topicId + ran;// 拼接
			result.put("success", true);// 设置属性
			result.put("data", topicId);// 设置属性
		} catch (Exception e) {
			e.printStackTrace();// 异常信息
			result.put("success", false);// 设置属性
			result.put("msg", "服务器异常");// 设置属性
		} finally {
			return result;// 返回结果
		}
	}
}
