package com.qtrmoon.paper.ext;

public class BankStepExt {

	private String stepTtitle;
	private String param;
	private String standardAnswer;
	private String[] stepXmls;
	
	public String getStepTtitle() {
		return stepTtitle;
	}
	public void setStepTtitle(String stepTtitle) {
		this.stepTtitle = stepTtitle;
	}
	public String getParam() {
		return param;
	}
	public void setParam(String param) {
		this.param = param;
	}
	public String getStandardAnswer() {
		return standardAnswer;
	}
	public void setStandardAnswer(String standardAnswer) {
		this.standardAnswer = standardAnswer;
	}
	public String[] getStepXmls() {
		return stepXmls;
	}
	public void setStepXmls(String[] stepXmls) {
		this.stepXmls = stepXmls;
	}
}
