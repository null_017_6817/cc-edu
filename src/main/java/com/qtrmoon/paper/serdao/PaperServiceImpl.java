package com.qtrmoon.paper.serdao;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.DocumentException;
import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xml.sax.InputSource;

import com.qtrmoon.examination.pojo.Prompt;
import com.qtrmoon.examination.pojo.StepInfo;
import com.qtrmoon.examination.serdao.FreemakerTools;
import com.qtrmoon.manager.constant.TopicTypeConstant;
import com.qtrmoon.manager.pojo.BankStep;
import com.qtrmoon.manager.pojo.BankTopic;
import com.qtrmoon.manager.serdao.BankStepMapper;
import com.qtrmoon.manager.serdao.BankTopicMapper;
/**
 * 问卷业务层实现类
 */
@Service("paperService")
public class PaperServiceImpl implements IPaperService{

	/**
	 * 日志对象
	 */
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private BankTopicMapper bankTopicMapper; // Mapper调用
	@Autowired
	private BankStepMapper bankStepMapper;// Mapper调用
	
	/**
	 * 添加topic和step
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 * @throws IOException 
	 * @throws JDOMException 
	 */
	public Map<String ,Object> addTopicAndStep(BankTopic topic,String[] params,BankStep paramStep,String topicIdParam) throws IllegalAccessException, InvocationTargetException, IOException{
		Map<String, Object> obj = new HashMap<String,Object>();
		if(topic != null && !"".equals(topic.getType())){// 判断
			if(topic.getTopicId() != null && !"".equals(topic.getTopicId())){// 判断
				if(!"实训营地".equals(topic.getType())){// 判断
					int total = getToalScore(params);// 获取总分
					topic.setTotalScore(total);//设置总分
				}
				bankTopicMapper.updBankTopic(topic);//更新题目
				BankStep bankStep = new BankStep();// 定义对象
				bankStep.setTopicId(topic.getTopicId());// 设置属性
				List<BankStep> steps = bankStepMapper.schBankStep(bankStep);//查询步骤
				if(steps != null && steps.size() > 0){// 判断
					Integer[] stepIds = new Integer[steps.size()];// 定义常量
					for( int i = 0; i < steps.size(); i++ ){// 循环
						if(steps.get(i) != null && steps.get(i).getId() != null){// 循环
							stepIds[i] = steps.get(i).getId();//汇总stepid
						}
					}
					bankStepMapper.delBankSteps(stepIds);//删除步骤
				}
			}else{
				Calendar calendar = Calendar.getInstance();// 获取时间
				topic.setTopicId(topicIdParam);//设置题目id
				SimpleDateFormat sdfs = new SimpleDateFormat("yyyyMMddhhmmss");// 时间格式化
				String creatTime = sdfs.format(calendar.getTime());// 时间格式化
				topic.setCreateTime(creatTime);//设置属性
				topic.setCreattime(creatTime);//设置属性
				if(!"实训营地".equals(topic.getType())){// 判断
					int total = getToalScore(params);// 获取总分
					topic.setTotalScore(total);//设置总分
				}
				bankTopicMapper.addBankTopic(topic);//添加题目
			}
			if(!"实训营地".equals(topic.getType())){// 判断
				try{
					doStepXml(topic.getBankId() ,topic.getTopicId(),params);//步骤
				}catch(JDOMException e){
					obj.put("msg", "禁止输入非法字符");
					e.printStackTrace();
				}
			}
			if("实训营地".equals(topic.getType()) && paramStep != null  ){// 判断
				paramStep.setBankId(topic.getBankId());// 设置属性
				paramStep.setTopicId(topic.getTopicId());// 设置属性
				paramStep.setOrderNo(0);// 设置属性
				paramStep.setType(topic.getType());// 设置属性
				if(!"".equals(paramStep.getTitle())){// 判断
					bankStepMapper.addBankStep(paramStep);//添加数据
				}
			}
		}
		return obj;
	}
	
	/**
	 * 获取题目总分
	 * @param params
	 * @return
	 */
	private int getToalScore(String[] params){
		int total = 0;// 定义常量
			if(params != null){// 判断
				for( int i = 0 ; i < params.length; i++ ){// 循环
					String step = params[i];// 获取第i个元素
					String nstr = step.replaceAll("@@", "\\,");
					/** *创建一个新的字符串*** */
					StringReader xmlReader = new StringReader(nstr);
					/** **创建新的输入源SAX 解析器将使用 InputSource 对象来确定如何读取 XML 输入 */
					InputSource xmlSource = new InputSource(xmlReader);
					/** *创建一个SAXBuilder* */
					SAXBuilder builder = new SAXBuilder();
					try{
						/** *通过输入源SAX构造一个Document** */
						Document doc = builder.build(xmlSource);
						Element root = doc.getRootElement();// 获取根节点
						Element scorEle = root.getChild("score");// 获取属性
						if(scorEle != null && scorEle.getText() != null){// 判断
							String[] scoArry =  scorEle.getText().split(",");// 拆分
							if(scoArry != null){// 判断
								float score = 0;// 定义常量
								for(int s = 0; s < scoArry.length; s++ ){// 循环
									if(scoArry[s] != null && !"".equals(scoArry[s])){// 判断
										score = score + Float.valueOf(scoArry[s]);// 求和
									}
								}
								total += score;// 总分
							}
						}
					}catch(Exception e){
						e.printStackTrace();//打印异常
						logger.info("计算总分异常");// log
					}
				}
			}
		return total;// 返回数据
	}
	
	/**
	 * 处理stepXML字符串
	 * @param bankId
	 * @param topicId
	 * @param params
	 * @throws JDOMException
	 * @throws IOException
	 */
	private void doStepXml(String bankId, String topicId,String[] params) throws JDOMException, IOException{
		if(bankId != null && topicId != null && params != null && params.length > 0){// 判断
			for( int i = 0 ; i < params.length; i++ ){// 循环
				String step = params[i];// 定义常量
				String nstr = step.replaceAll("@@", "\\,");
				/** *创建一个新的字符串*** */
				StringReader xmlReader = new StringReader(nstr);
				/** **创建新的输入源SAX 解析器将使用 InputSource 对象来确定如何读取 XML 输入 */
				InputSource xmlSource = new InputSource(xmlReader);
				/** *创建一个SAXBuilder* */
				SAXBuilder builder = new SAXBuilder();
				/** *通过输入源SAX构造一个Document** */
				Document doc = builder.build(xmlSource);
				Element root = doc.getRootElement();//获取根节点
				BankStep bankStep = new BankStep();// 实例化对象
				Element scorEle = root.getChild("score");//获取分数
				if(scorEle != null && scorEle.getText() != null){// 判断
					String[] scoArry =  scorEle.getText().split(",");// 拆分
					if(scoArry != null){// 判断
						float score = 0;// 定义常量
						for(int s = 0; s < scoArry.length; s++ ){// 循环
							if(scoArry[s] != null && !"".equals(scoArry[s])){// 判断
								score = score + Float.valueOf(scoArry[s]);//单个题分相加
							}
						}
						bankStep.setScore(String.valueOf(score));// 总分
					}
				}
				
				Attribute stepTypeAtri = root.getAttribute("type");//步骤节点
				if(stepTypeAtri != null){// 判断
					String stepType = stepTypeAtri.getValue();//获取步骤类型
					if( !"question".equals(stepType) ){// 判断
						bankStep.setType(stepType);//设置步骤类型
					}else{
						bankStep.setType("question");//设置步骤类型
					}
				}
				
				Element titleEle = root.getChild("title");// 获取子节点
				bankStep.setTitle(titleEle.getText());// 标题
				Element standardAnswerEle = root.getChild("standard_answer");// 获取子节点
				bankStep.setStandardAnswer(standardAnswerEle.getText());// 正确答案
				bankStep.setParam(nstr);//设置属性
				bankStep.setBankId(bankId);//设置属性
				bankStep.setTopicId(topicId);//设置属性
				bankStep.setOrderNo(i);//设置属性
				bankStepMapper.addBankStep(bankStep);//添加步骤
			}
		}
	}
	
	/**
	 * 获取topic和step列表
	 * @param topicId
	 * @param projectName
	 */
	public Map<String,Object> getTopicAndStep(String topicId,String projectName) throws DocumentException, JDOMException, IOException{
		Map<String,Object> result = new HashMap<String,Object>();// 返回对象
		List<StepInfo> stepInfoLis = new ArrayList<StepInfo>();// 实例化集合
		if( topicId != null ){// 判断
			BankTopic bankTopic = bankTopicMapper.schBankTopicById(topicId);//获取题目
			if( bankTopic != null && !"".equals(bankTopic.getTopicId()) ){// 判断
				result.put("topic", bankTopic);// put值
				BankStep bankStep = new BankStep();// 实例化对象
				bankStep.setTopicId(bankTopic.getTopicId());// 设置属性
				bankStep.setOrderCol("id");
				List<BankStep> stepList = bankStepMapper.schBankStep(bankStep);//查询步骤
				for( int i = 0; i < stepList.size(); i++ ){// 循环
					BankStep step = stepList.get(i);// 获取第i个元素
					StepInfo stepInfo = null;// 设置对象
					if( bankTopic.getType() != null && !"实训营地".equals(bankTopic.getType()) 
							&& !TopicTypeConstant.CAMPING.equals(bankTopic.getType())
							&& step != null && step.getType() != null && !TopicTypeConstant.STEP_CAMPING.equals(step.getType())
							&& !TopicTypeConstant.STEP_CAMPING_STR.equals(step.getType())){
						stepInfo = FreemakerTools.xmlToStep(step.getParam(), projectName// 调用工具方法
								+"/paper/question/appendUrl.action?filename="// 调用工具方法
								+step.getBankId()+File.separator+step.getTopicId()+File.separator);// 调用工具方法
						if(stepInfo != null){// 判断
							
							String stepType = step.getType();
							if("cloze".equals(stepInfo.getType()) && step != null && step.getParam() != null){// 判断
								String xml = step.getParam();// 获取属性
								StringReader xmlReader = new StringReader(xml);// 定义对象
								InputSource xmlSource = new InputSource(xmlReader);// 定义对象
								SAXBuilder builder = new SAXBuilder();// 定义对象
								//** *通过输入源SAX构造一个Document** *//*
								Document doc = builder.build(xmlSource);
								Element root = doc.getRootElement();// 获取节点
								Element scorEle = root.getChild("score");//获取属性
								stepInfo.setScore(scorEle != null? scorEle.getText():"0");// 设置总分
							}else{
								stepInfo.setScore(step.getScore());// 设置总分
							}
							if(stepType.length() <= 2){//TODO 数据清洗后去掉判断，直接执行判断里面的代码
								stepInfo.setType(stepType);
							}
							stepInfo.setBankId(bankTopic.getBankId());
							stepInfo.setTopicId(bankTopic.getTopicId()); 
							stepInfoLis.add(stepInfo);
							
							stepInfo.setEsid(step.getId());
						}
					}
				}
				result.put("stepInfoList", stepInfoLis);// put数据
			}
		}
		return result;// 返回结果
	}
}
