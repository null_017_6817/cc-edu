package com.qtrmoon.paper.serdao;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import org.dom4j.DocumentException;
import org.jdom.JDOMException;

import com.qtrmoon.manager.pojo.BankStep;
import com.qtrmoon.manager.pojo.BankTopic;
/**
 * 问卷业务接口
 */
public interface IPaperService {

	
	/**
	 * 添加topic和step()
	 * @param param
	 */
	public Map<String ,Object> addTopicAndStep(BankTopic topic,String[] params,BankStep bankStep,String topicIdParam)throws IllegalAccessException, InvocationTargetException, JDOMException, IOException;
	
	/**
	 * 获取topic和step列表
	 * @param topicId
	 * @return
	 * @throws DocumentException
	 * @throws JDOMException
	 * @throws IOException
	 */
	public Map<String,Object> getTopicAndStep(String topicId,String projectName)throws DocumentException, JDOMException, IOException;
}
