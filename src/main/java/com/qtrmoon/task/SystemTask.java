package com.qtrmoon.task;

import java.util.List;

import com.qtrmoon.common.BaseController;
import com.qtrmoon.manager.constant.YesOrNoConstant;
import com.qtrmoon.sysmanage.pojo.User;
import com.qtrmoon.sysmanage.serdao.ISysmanageService;
import com.qtrmoon.util.ApplicationContextUtil;

public class SystemTask extends BaseController {

	/**
	 * 批量修改学生状态（修改为退出状态）
	 */
	public void batchStudLogOut(){
		logger.info(">>>>>>>>>进 入 定 时 任 务【批量修改学生状态（修改为退出状态）：batchStudLogOut】>>>>>>>>>>>>>>>>>>>>>>>");
		ISysmanageService sysmanageService = (ISysmanageService) ApplicationContextUtil.getApplicationContext()
				.getBean("sysmanageService");//获取service
		User user = new User();
		user.setIsLogin(YesOrNoConstant.YES);
		List<User> users = sysmanageService.schUserList(user);//查询学生表
		logger.info("查询到的学生数据：{}",users);
		for( User us : users ){
			if(us != null){
				us.setAgentId("");//设置坐席号
				us.setIsLogin(YesOrNoConstant.NO);// 修改为已登录
				sysmanageService.updUser(us);//更新学生
			}
		}
		logger.info(">>>>>>>>>定  时  任  务  结  束【批量修改学生状态（修改为退出状态）：batchStudLogOut】>>>>>>>>>>>>>>>>>>>>>>>");
		// TODO 删除未保存试卷（脏数据）
	}
}
