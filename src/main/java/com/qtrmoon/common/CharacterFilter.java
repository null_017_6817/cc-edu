package com.qtrmoon.common;
import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.dictionary.bsd.DBUtil;

/**
 * 字符过滤器
 */
@WebFilter(filterName="myfilter",urlPatterns="/*")  
public class CharacterFilter implements Filter {
	
	/**
	 * 重写接口destroy方法
	 */
	@Override
	public void destroy() {
		
	}

	/**
	 * 拦截方法
	 * @param request
	 * @param response
	 * @param chain
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		request.setCharacterEncoding("UTF-8");// 设置字符编码
		response.setCharacterEncoding("UTF-8");// 设置字符编码
		request.setAttribute("projectName", Constant.PRONAME);// 设置项目名称
		chain.doFilter(request, response);// 执行下一个过滤，或跳转页面
	}

	/**
	 * 初始化方法
	 * @param arg0
	 */
	@Override
	public void init(FilterConfig arg0) throws ServletException {
		String projectName=arg0.getServletContext().getContextPath();// 获取项目名称
		projectName=projectName.replace("/", "");// 替换字符串
		Constant.PRONAME=projectName;// 设置项目名称
		Constant.createConstant();// 调用常量类方法
		String path=arg0.getServletContext().getRealPath("/");// 获取项目名称
		Constant.getExpire(path);
		DictBuffer.init("jdbc.properties");
		//new DBUtil().initDefault("jdbc.properties");// 加载数据库配置类
	}

}