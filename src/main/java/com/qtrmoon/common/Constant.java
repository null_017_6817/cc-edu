package com.qtrmoon.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.jdom.Element;

import com.qtrmoon.toolkit.XmlUtil;
import com.qtrmoon.util.EncryptUtil;

/**
 * 系统常量类
 */
public class Constant {
	
	public static String CONDITION_IN_SESSION = "CONDITION_IN_SESSION";// 查询条件session常量

	public static Map<String, String> CONFIG_VALUE = new HashMap<String, String>(); // 静态集合常量

	public static String PRONAME; // 项目名称

	/** 授权用 */
	public static Date expireTime;// 过期时间
	public static long expire;// 过期时间long
	public static String key = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";// 加密key
	public static String charset = "UTF-8";// 字符编码
	
	
	/**
	 * 静态变量添加数据
	 * @param key
	 * @param value
	 */
	public static void push(String key, String value) {
		CONFIG_VALUE.put(key, value);
	}

	/**
	 * 静态变量获取数据
	 * @param key
	 * @return
	 */
	public static String getConstant(String key) {
		return CONFIG_VALUE.get(key.toUpperCase());// 返回结果
	}

	/**
	 * 判断根据key获取是否含有值
	 * @param key
	 * @return
	 */
	public static boolean getBooleanConstant(String key) {
		return Boolean.parseBoolean(CONFIG_VALUE.get(key.toUpperCase()));// 返回结果
	}

	/**
	 * 创建常量类
	 * @param 
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void createConstant() {
		String path = getPath();
		path = path.replaceAll("%20", " ");// 替换字符串
		XmlUtil xmlUtil = new XmlUtil();// 实例化xml对象
		Element groupERoot = xmlUtil.loadXml(path);// 解析xml
		List<Element> constantList = groupERoot.getChildren();// 获取根节点下所有子节点
		String workspace = null,nodeName;// 设置常量
		for (Element elem:constantList) {// 循环获取节点
			nodeName=elem.getName().toUpperCase();// 大小写转换
			List<Element> cList = elem.getChildren();// 获取子节点
			if(cList.size()>0){//判断子节点集合长度
				for (Element ce:cList) {// 循环将节点名称和节点值存放到map中
					push(nodeName+"."+ce.getName().toUpperCase(),ce.getValue());// 添加值
					push(ce.getName().toUpperCase(),ce.getValue());// 添加值
				}
			}else{
				push(nodeName, elem.getValue());// 将节点名称和节点值存放到map中
			}
		}

		Set key = CONFIG_VALUE.keySet();// 迭代map
		CONFIG_VALUE.put("projectName", PRONAME);// 添加值
		workspace = CONFIG_VALUE.get("WORKSPACE");// 获取WORKSPACE
		String name,value;// 定义常量
		for (Iterator<String> iter = key.iterator(); iter.hasNext();) {// 迭代循环
			name = iter.next();// 取下一个人
			value = CONFIG_VALUE.get(name);// 获取名称
			value = value.replaceAll("\\$\\{workspace\\}", workspace).replaceAll("\\$\\{projectName\\}", PRONAME);// 转换符号
			CONFIG_VALUE.put(name, fmt(value));// 迭代将map中的值存放到静态变量中
		}
	}

	/**
	 * 获取配置文件，根据不同系统获取不同文件
	 * @return
	 */
	private static String getPath() {
		String cfg="";
		Properties sysProperty=System.getProperties(); //系统属性
		if(sysProperty.getProperty("os.name").toLowerCase().indexOf("windows")>=0){//Linux Mac Windows Solaris SunOS
			cfg="system-config-windows.xml";// 配置文件名称
		}else{
			cfg="system-config-linux.xml";// 配置文件名称
		}
		if(new File(Constant.class.getResource("/").getPath()+cfg).exists()){// 如果存在则获取相应文件，不存在则获取默认文件
			return Constant.class.getResource("/").getPath()+cfg;// 返回配置文件名称
		}else{
			return Constant.class.getResource("/").getPath()+"system-config.xml";// 不存在则获取默认文件
		}
	}

	/**
	 * 格式化
	 * @param value
	 * @return
	 */
	private static String fmt(String value) {
		value = value.replaceAll("\\\\", "/");// 替换字符串
		while(value.indexOf("//")>=0){
			value=value.replaceAll("//", "/");// 替换字符串
		}
		return value;// 返回字符串
	}

	/**
	 * 当服务没有启动或服务尚未启动时使用。
	 * createConstant是在编码过滤器init时调用的，如果在过滤器初始化前使用getConstant则获取不到值，例如ContextServlet启动的监听时就无法使用。
	 * @param string
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static String getConstantNoServer(String name) {
		String res=null;// 定义常量
		String path = getPath();// 调用方法
		path = path.replaceAll("%20", " ");// 替换
		XmlUtil xmlUtil = new XmlUtil();// 实例化 xml解析实体
		Element root = xmlUtil.loadXml(path);// 解析xml文件
		List<Element> constantList = root.getChildren();// 获取子节点
		String workspace = null, projectName = null, value;// 定义常量
		for (Element elem:constantList) {// 循环
			value = elem.getValue();
			if(elem.getName().equals("workspace")){// 获取工作空间名
				workspace=value;// 赋值
			}
			if(elem.getName().equals("projectName")){// 获取项目名
				projectName=value;// 赋值
			}
			if(elem.getName().equals(name)){
				value = value.replaceAll("[${}]", "#");// 会把${}三个字符转换成#。
				res=value;// 赋值
			}
		}
		if(res!=null){// 判断参数是否为null
			res = res.replaceAll("##workspace#", workspace);// 替换字符串
			res = res.replaceAll("##projectName#", projectName);// 替换字符串
			res = res.replaceAll("\\\\", "/");// 替换字符串
			res = res.replaceAll("//", "/");// 替换字符串
			res = res.replaceAll("//", "/");// 替换字符串
		}
		return res;// 赋值
	}
	
	
	public static void getExpire(String path){
		String outfile = path+File.separator+File.separator+"book"
				+File.separator+"img"+File.separator+"icon"+File.separator+"ss.jpg";
		
		try{
			
			InputStream out2 = new FileInputStream(outfile);
			out2.read(new byte[2]);
			byte[] tm=new byte[24];
			out2.read(tm);
			out2.close();
			String time=new String(tm);
			EncryptUtil enc = new EncryptUtil(key,charset);
			time = enc.decode(time);
			if(time != null){
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
				expireTime = sdf.parse(time);
				expire = expireTime.getTime();
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
