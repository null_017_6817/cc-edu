package com.qtrmoon.common;

/**
 * 模拟电话bean
 */
public class OrderEntity {

	public String fromUser; // 拨打者学号
	public String toUser; // 接听者CC座席号
	public String profession; // 行业
	public String callTime; // 拨打时间

	/**
	 * 获取拨打人
	 * @return
	 */
	public String getFromUser() {
		return fromUser;
	}

	/**
	 * 设置拨打人
	 * @param fromUser
	 */
	public void setFromUser(String fromUser) {
		this.fromUser = fromUser;
	}

	/**
	 * 获取接听人
	 * @return
	 */
	public String getToUser() {
		return toUser;
	}

	/**
	 * 设置接听人
	 * @param toUser
	 */
	public void setToUser(String toUser) {
		this.toUser = toUser;
	}

	/**
	 * 获取行业
	 * @return
	 */
	public String getProfession() {
		return profession;
	}

	/**
	 * 设置行业
	 * @param profession
	 */
	public void setProfession(String profession) {
		this.profession = profession;
	}

	/**
	 * 获取拨打时间
	 * @return
	 */
	public String getCallTime() {
		return callTime;
	}

	/**
	 * 设置拨打时间
	 * @param callTime
	 */
	public void setCallTime(String callTime) {
		this.callTime = callTime;
	}
}
