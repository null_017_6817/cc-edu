package com.qtrmoon.common;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationContext;

import com.qtrmoon.manager.pojo.Stud;
import com.qtrmoon.manager.serdao.StudMapper;
import com.qtrmoon.toolkit.DateTransfer;
import com.qtrmoon.util.ApplicationContextUtil;

/**
 * 工单缓存类
 */
public class OrderCache {
	
	public static List<OrderEntity> cacheList = new ArrayList<OrderEntity>();// 工单缓存列表
	
	/**
	 * 添加
	 * @param from 拨打人（客户:学号）
	 * @param to 接听人（客服：坐席号）
	 * @param billId
	 */
	public static boolean add( String from , String to ,String profession){
		boolean result = false; // 成功标识
		if(from != null && to != null){// 拨打人和接听人不为空
			OrderEntity orderEntity = new OrderEntity();
			orderEntity.setFromUser(from);// 设置拨打人
			orderEntity.setToUser(to);// 设置接听人
			orderEntity.setProfession(profession);// 设置行业
			orderEntity.setCallTime(DateTransfer.toString(new Date(),"yyyy-MM-dd HH:mm:ss"));// 设置时间
			boolean has = false;
			for( OrderEntity item : cacheList ){// 判断是否是否已含有接听人的数据
				if( item != null && to.equals(item.getToUser()) // 判断参数是否为指定值
						&& from.equals(item.getFromUser()) ){// 判断参数是否为指定值
					has = true;// 设置含有标识
				}
			}
			if(!has){// 如果未含有，则添加
				cacheList.add(orderEntity);// 缓存添加数据
				result = true;// 设置含有标识
			}
		}
		return result;// 返回结果
	}
	
	/**
	 * 删除数据
	 * @param from
	 * @param to
	 */
	public static void remove( String from , String to ){
		if(from != null && to != null){// 判断必要参数
			for( int i = 0; i < cacheList.size(); i++ ){// 循环
				OrderEntity item = cacheList.get(i);// 获取缓存数据
				if( item != null && to.equals(item.getToUser()) // 判断参数
						&& from.equals(item.getFromUser()) ){// 根据拨打人和接听人判断数据，如果相同则删除掉
					cacheList.remove(i);// 删除缓存数据
				}
			}
		}
	}
	
	
	/**
	 * 删除数据(根据接听者清除占线状态)
	 * @param to
	 */
	public static boolean removeByTo( String to){
		boolean result = true;// 初始化返回结果
		if(to != null){// 判断必要参数是否为空
			for( int i = 0; i < cacheList.size(); i++ ){// 循环
				OrderEntity item = cacheList.get(i);// 获取缓存数据
				if( item != null && to.equals(item.getToUser()) ){// 如果接听人和缓存中接听人相同则删除
					cacheList.remove(i);// 删除数据
				}
			}
		}else{
			result = false;// 设置成功标识
		}
		return result;// 返回结果
	}
	
	/**
	 * 根据客服获取客户
	 * @param to
	 * @return
	 */
	public static Map<String,String> getFromByTo(String to){
		Map<String,String> map = new HashMap<String,String>();// 初始化返回结果集
		if( to != null){// 判断必要参数是否为空
			for( int i = 0; i < cacheList.size(); i++ ){// 循环
				OrderEntity item = cacheList.get(i);
				if( item != null && to.equals(item.getToUser()) ){// 如果缓存中接听者和当前参数相同
					map.put("from" , item.getFromUser());// 赋值拨打人
					map.put("profession" , item.getProfession());// 赋值行业
					map.put("callTime" , item.getCallTime());// 赋值拨打时间
				}
			}
		}
		return map;// 返回结果
	}
	
	/**
	 * 获取学生正在拨打电话的列表
	 * @return
	 */
	public static List<Map<String,Object>> getCallList(){
		ApplicationContext ac  = ApplicationContextUtil.getApplicationContext();
		StudMapper studMapper = (StudMapper) ac.getBean("studMapper");// 注册学生mapper到该类
		List<Map<String,Object>> result =  new ArrayList<Map<String,Object>>();// 初始化返回结果集
		if(cacheList != null){// 判断必要参数是否为空
			for( OrderEntity item : cacheList){// 循环
				Map<String,Object> obj = new HashMap<String,Object>();// 实例化结果集
				if(item != null){// 判断参数是否为null
					Stud fromStu = studMapper.schStudByXh(item.getFromUser());// 查询学生
					Stud st = new Stud();// 实例化学生实体
					st.setAgentId(item.getToUser());// 设置坐席号
					List<Stud> toStus = studMapper.schStud(st);// 查询学生列表
					if(fromStu != null && toStus != null && toStus.size() == 1){// 判断参数
						Stud toStu = toStus.get(0);// 获取第一个实体
						obj.put("fromStu", fromStu);	// 封装对象拨打人 fromStu
						obj.put("toStu", toStu);		// 封装对象接听人 toStu
					}else{
						continue;// 继续循环
					}
					result.add(obj);// 添加数据
				}else{
					continue;// 继续循环
				}
			}
		}
		return result;// 返回结果
	}
}
