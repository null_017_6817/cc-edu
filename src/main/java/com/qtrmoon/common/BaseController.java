package com.qtrmoon.common;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 控制器基类
 */
public class BaseController {
	/**
	 * 日志对象
	 */
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	/**
	 * 设置page参数
	 * @param request
	 * @param cla
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	protected PageForm setPage(HttpServletRequest request,Class cla) {
		PageForm form=null;// 初始化参数
		try {
			HttpSession session=request.getSession();// 获取session
			Object o=session.getAttribute(cla.getName());// 获取类
			if(o==null){// 判断必要参数是否为null
				form=(PageForm)cla.newInstance();// 类的实例
				session.setAttribute(cla.getName(),form);// 如果session中没有，则存放到session中
			}else{
				form=(PageForm)o; // 传递参数
			}
			String currentPage=request.getParameter("page");// 获取当前page参数
			if (currentPage != null && !currentPage.equals("")) {// 判断必要参数是否为空
				form.setPage(Integer.parseInt(currentPage));// 设置page
			}
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();// 打印异常信息
		}
		return form;// 返回结果
	}
	
	/**
	 * 设置ajax无缓存
	 * @param response
	 */
	public void noCache(HttpServletResponse response){
		response.setHeader("Cache-Control", "no-cache,must-revalidate");// 设置无缓存
	}
}
