package com.qtrmoon.common;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;

import com.alibaba.fastjson.JSON;

/**
 * json和类相互转换工具类
 */
public class JSONHandler implements TypeHandler<Object>{
	
	private static final char SPLIT = '/'; //json数据和类名的分隔符号

	/**
	 * 设置参数
	 * @param ps
	 * @param i
	 * @param parameter
	 * @param jdbcType
	 */
	public void setParameter(PreparedStatement ps, int i, Object parameter,
			JdbcType jdbcType) throws SQLException {
		if (parameter == null) {// 判断参数是否为空
			ps.setString(i, null);// 设置string
			return;// 返回
		}
		String json = JSON.toJSONString(parameter);// 转换为json字符串
		json = json + SPLIT + parameter.getClass().getName();// json字符串拼接类名
		ps.setString(i, json);// 设置String
	}

	/**
	 * 获取result
	 * @param rs
	 * @param columnName
	 */
	public Object getResult(ResultSet rs, String columnName)
			throws SQLException {
		String json = rs.getString(columnName);// 转换为字符串
		return jsonToObject(json);// 返回json对象
	}
	
	/**
	 * 获取result
	 * @param cs
	 * @param columnIndex
	 */
	public Object getResult(CallableStatement cs, int columnIndex)
			throws SQLException {
		String json = cs.getString(columnIndex);// 转换为字符串
		return jsonToObject(json); // 返回json对象
	}

	/**
	 * json 转换成对象
	 * @param json
	 */
	private Object jsonToObject(String json) {
		if (json == null) {// 判断参数是否为空
			return null;// 返回结果
		}
		int index = json.lastIndexOf(SPLIT);// 获取最后一个'/'
		if (index < 0) {// 判断index是否小于0
			return null;// 返回结果
		}
		String key = json.substring(index + 1, json.length());// 截取字符串
		json = json.substring(0, index);// 截取字符串
		Class<?> cls = null;// 初始化class
		try {
			cls = Class.forName(key);// 序列化成json找到指定的类
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("序列化成json时找不到指定的类", e);// 抛出运行时异常
		}
		Object ob = JSON.parseObject(json, cls);// json格式化
		return ob;// 返回结果
	}

	/**
	 * 获取结果
	 * @param rs
	 * @param columnIndex
	 */
	@Override
	public Object getResult(ResultSet rs, int columnIndex) throws SQLException {
		String json = rs.getString(columnIndex);// 转换为字符串
		return jsonToObject(json);// 返回obj对象
	}
}
