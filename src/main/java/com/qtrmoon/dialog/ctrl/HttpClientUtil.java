package com.qtrmoon.dialog.ctrl;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import cn.dreampie.client.Client;
import cn.dreampie.client.ClientRequest;
import cn.dreampie.client.ClientResult;

/**
 * httpClient工具类
 */
public class HttpClientUtil {
	private Client httpClient;
	
	// 构造方法
	public HttpClientUtil(String url) {
		if(httpClient == null) {// 判断是否为null
			httpClient = new Client(url);// new一个对象
		}
	}
	
	/**
	 * 获取token
	 * @return
	 */
	private String getToken() {
		ClientRequest postRequest = new ClientRequest("login");// 实例化一个request对象
		postRequest
				.setJsonParam("{\"username\": \"admin\", \"password\": \"admin\"}");// 设置用户名密码
		ClientResult postResult = httpClient.build(postRequest).post();// post调用
		String json = postResult.getResult();// 返回结果

		JSONObject jsonObject = new JSONObject(json);// 转换
		String token = (String) jsonObject.get("token");// 获取token
		return token;
	}
	
	/**
	 * get方法
	 * @param api
	 * @param agentId
	 * @return
	 */
	public String get(String api, String agentId) {
		String assess = "";// 初始化参数
		if("".equals(agentId) || agentId == null) {// 判断必要参数
			assess = api;// 赋值参数
		}else {
			assess = api + "/" + agentId;// 拼接参数
		}
		String token = getToken();// 获取token
		ClientRequest postRequest = new ClientRequest(assess);//设置get访问参数
		Map<String, String> headers = new HashMap<String, String>() ;// 实例化对象
		headers.put("Authorization", "Nway " + token) ;//设置get访问参数
		postRequest.setHeaders(headers);//设置get访问参数
		return httpClient.build(postRequest).get().getResult();//get访问
	}
	
	/**
	 * post访问
	 * @param api
	 * @param paramString
	 * @return
	 */
	public String post(String api, String paramString) {
		String token = getToken();// 获取token
		ClientRequest postRequest = new ClientRequest(api);//设置post访问参数
		postRequest.setJsonParam(paramString);
		Map<String, String> headers = new HashMap<String, String>() ;
		headers.put("Content-Type", "application/json");//设置post访问参数
		headers.put("Authorization", "Nway " + token) ;//设置post访问参数
		postRequest.setHeaders(headers);//设置post访问参数
		return httpClient.build(postRequest).post().getResult();// post访问
	}
	
}