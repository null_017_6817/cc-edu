package com.qtrmoon.dialog.ctrl;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qtrmoon.common.BaseController;
import com.qtrmoon.dialog.serdao.IDialogService;
import com.qtrmoon.dialog.serdao.IMyFsService;
import com.qtrmoon.manager.pojo.Person;
import com.qtrmoon.manager.serdao.IManagerService;
import com.qtrmoon.sysmanage.Online;
import com.qtrmoon.sysmanage.SysConstant;

/**
 * 监控列表控制器
 */
@Controller
@RequestMapping("/dialog")
public class DialogController extends BaseController{
	
	@Autowired
	private IDialogService dialogService; // 声明模块Service实例
	
	@Autowired
	private IManagerService managerService; // 声明模块Service实例
	
	@Autowired
	private IMyFsService myFsService;
	
	/**
	 * 监听方法
	 * @param agentId
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/eavesdrop.action")
	public void eavesdrop(String agentId, HttpServletRequest request, HttpServletResponse response) throws IOException{
		Person person = (Person) request.getSession().getAttribute(SysConstant.CURRENT_USER); // 获取当前用户
		JSONObject res=new JSONObject();
		if(person == null) {// 当前用户为null，则提示登录
			res.put("result", "请登录后操作");
			response.getWriter().print(res.toString());// 输出json文件
		}else{
			String teacherId = person.getAgentId();// 获取坐席号
			String eavesdrop = dialogService.eavesdrop(agentId, teacherId);// 调用监听service
			//String eavesdrop = myFsService.eavesdrop(agentId, teacherId);
			response.getWriter().print(eavesdrop);// 输出json数据
		}
	}
	
	/**
	 * 挂断方法
	 * @param agentId
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping("/hangup.action")
	public void hangup(String agentId, HttpServletResponse response) throws IOException {
		String hangup = dialogService.hangup(agentId);// 调用挂断service
		response.getWriter().print(hangup);// 输出json数据
	}
	
	/**
	 * 转接方法
	 * @param agentId
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/transfer.action")
	public void transfer(String agentId, HttpServletRequest request, HttpServletResponse response) throws IOException {
		Person person = (Person) request.getSession().getAttribute(SysConstant.CURRENT_USER); // 获取当前用户
		JSONObject res=new JSONObject();// 实例化json对象
		if(person == null) {// 当前用户为null，则提示登录
			res.put("result", "请登录后操作");
			response.getWriter().print(res.toString());// 输出json数据
		}else{
			String teacherId = person.getAgentId();// 获取坐席号
			String transfer = dialogService.transfer(agentId, teacherId);// 调用转接service
			response.getWriter().print(transfer);// 输出json数据
		}
		
	}
	
	/**
	 * 三方通话
	 * @param agentId
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/threeway.action")
	public void threeway(String agentId, HttpServletRequest request, HttpServletResponse response) throws IOException {
		Person person = (Person) request.getSession().getAttribute(SysConstant.CURRENT_USER);// 获取当前用户
		JSONObject res=new JSONObject();// 实例化json对象
		if(person == null) {// 当前用户为null，则提示登录
			res.put("result", "请登录后操作");
			response.getWriter().print(res.toString());// 输出json数据
		}else{
			String teacherId = person.getAgentId();// 获取坐席号
			String threeway = dialogService.threeway(agentId, teacherId);// 调用三方通话service
			// String threeway = myFsService.threeway(agentId, teacherId);
			response.getWriter().print(threeway);// 输出json数据
		}
	}
	
	/**
	 * 录音
	 * @param agentId
	 * @param filename
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping("/record.action")
	public void record(String agentId, String filename, HttpServletResponse response) throws IOException {
		String record = dialogService.record(agentId, filename);// 调用录音service
		response.getWriter().print(record);// 输出json数据
	}
	
	/**
	 * 查看通话列表
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@RequestMapping("/schStudCalling.action")
	@ResponseBody
	public List<Person> schStudCalling( HttpServletResponse response) throws IOException {
		Map<String, Person> onlinePerson = Online.getOnlinePerson(); // 在线学生人数
		List<Person> personList = new ArrayList<Person>();// 初始化集合
		Set<Entry<String, Person>> entrySet = onlinePerson.entrySet();// 去重
		for (Entry<String, Person> entry : entrySet) {// 循环
			Person person = entry.getValue();// 获取person
			if(person.getAnswerState() != null) {// 判断参数是否为null
				person.setMenu(null);
				person.setOrgan(null);
				personList.add(person);// 将数据放到集合中
			}
		}
		return personList; // 返回集合
	}
		
	
	/**
	 * JSP页面跳转
	 * @param page jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value="/page.action")
	public String goPage(String page){	//放过本Ctrl的页面，不合法的跳转到error页。
		if("listen".equals(page)){ // 跳转
			return "/listen/"+page; // 跳转页面
		}else{
			return "/error";// 跳转错误页面
		}
	}
}
