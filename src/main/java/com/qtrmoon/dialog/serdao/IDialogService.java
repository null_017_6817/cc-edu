package com.qtrmoon.dialog.serdao;

/**
 * 监听service接口
 */
public interface IDialogService {
	
	/**
	 * 挂断电话
	 * @param agentId
	 * @return
	 */
	public String hangup(String agentId);
	
	/**
	 * 监听
	 * @param agentId
	 * @param teacherId
	 * @return
	 */
	public String eavesdrop(String agentId, String teacherId);
	
	/**
	 * 录音
	 * @param agentId
	 * @param filePath
	 * @return
	 */
	public String record(String agentId, String filePath);
	
	/**
	 * 转接
	 * @param agentId
	 * @param teacherId
	 * @return
	 */
	public String transfer(String agentId, String teacherId);
	
	/**
	 * 三方通话
	 * @param agentId
	 * @param teacherId
	 * @return
	 */
	public String threeway(String agentId, String teacherId); 
}
