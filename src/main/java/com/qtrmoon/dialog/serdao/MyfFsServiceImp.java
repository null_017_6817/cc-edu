package com.qtrmoon.dialog.serdao;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.qtrmoon.book.util.FilePath;
import com.qtrmoon.dialog.ctrl.MyFsHttpUtil;
import com.qtrmoon.util.PropertyUitls;

@Service("myFsService")
public class MyfFsServiceImp implements IMyFsService {

	@Override
	public String eavesdrop(String agentId, String teacherId) {
		String url = PropertyUitls.getSysConfigSet("dialogEavesdrop");// 获取free的ip配置
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("EvdedPhone", agentId);// 设置参数
		map.put("EvdPhone", teacherId);// 设置参数
		MyFsHttpUtil.post(url, map);
		return null;
	}

	@Override
	public String record(String agentId, String filename) {
		String url = PropertyUitls.getSysConfigSet("dialogAudio");// 获取free的ip配置
		String path = FilePath.getFilePath("recordServer");// 录音路径
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("agentId", agentId);// 设置参数
		map.put("filename", path+filename);// 设置参数
		MyFsHttpUtil.post(url, map);
		return null;
	}
	
	@Override
	public String threeway(String agentId, String teacherId) {
		String url = PropertyUitls.getSysConfigSet("dialogThreeway");// 获取free的ip配置
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("EvdedPhone", agentId);// 设置参数
		map.put("EvdPhone", teacherId);// 设置参数
		MyFsHttpUtil.post(url, map);
		return null;
	}
	
}
