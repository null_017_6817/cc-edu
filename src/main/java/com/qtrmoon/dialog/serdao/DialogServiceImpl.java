package com.qtrmoon.dialog.serdao;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qtrmoon.dialog.ctrl.HttpClientUtil;
import com.qtrmoon.dialog.ctrl.MyFsHttpUtil;
import com.qtrmoon.util.PropertyUitls;

/**
 * 监控service
 */
@Service("dialogService")
public class DialogServiceImpl implements IDialogService {

	/**
	 * 监听
	 * @param agentId
	 * @param teacherId
	 */
	public String eavesdrop(String agentId, String teacherId) {
		hangup(teacherId);// 挂断电话
		try {
			Thread.sleep(1000);// 休眠一秒
		} catch (InterruptedException e1) {
			e1.printStackTrace();// 打印异常信息
		}
		String url = PropertyUitls.getSysConfigSet("dialogAPI");// 获取free的ip配置
		HttpClientUtil util = new HttpClientUtil(url);// 设置访问url
		Map<String, Object> map = new HashMap<String, Object>();// 实例化集合
		map.put("EvdedPhone", agentId);// 设置参数
		map.put("EvdPhone", teacherId);// 设置参数
		map.put("EvdGateway", "");// 设置参数
		ObjectMapper json = new ObjectMapper();// 实例化对象
		String param = "";//初始化参数
		try {
			param = json.writeValueAsString(map);// 转换字符串
		} catch (Exception e) {
			throw new RuntimeException(e);// 抛出运行时异常（事务回滚）
		}
		String post = util.post("eavesdrop", param);// 执行post远程调用
		return post;// 返回post
	}

	/**
	 * 挂断电话
	 * @param agentId
	 */
	public String hangup(String agentId) {
		String url = PropertyUitls.getSysConfigSet("dialogAPI");// 获取free的ip配置
		HttpClientUtil util = new HttpClientUtil(url);// 实例化
		return util.get("hangup", agentId);// 挂断电话
	}

	/**
	 * 转接
	 * @param agentId
	 * @param teacherId
	 */
	public String transfer(String agentId, String teacherId) {
		String url = PropertyUitls.getSysConfigSet("dialogAPI");// 获取free的ip配置
		HttpClientUtil util = new HttpClientUtil(url); // 实例化httpclient
		Map<String, Object> map = new HashMap<String, Object>();// 实例化集合
		map.put("EvdedPhone", agentId);// 设置参数
		map.put("EvdPhone", teacherId);// 设置参数
		map.put("EvdGateway", "");// 设置参数
		ObjectMapper json = new ObjectMapper();// 实例化objmapper
		String param = "";// 初始化参数
		try {
			param = json.writeValueAsString(map);// 转换字符串
		} catch (Exception e) {
			throw new RuntimeException(e);// 抛出运行时异常（事务回滚）
		}
		return util.post("transfer", param);// post调用
	}

	/**
	 * 三方通话
	 * @param agentId
	 * @param teacherId
	 */
	public String threeway(String agentId, String teacherId) {
		hangup(teacherId);// 挂断电话
		try {
			Thread.sleep(1000);// 
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		String url = PropertyUitls.getSysConfigSet("dialogAPI");// 获取free的ip配置
		HttpClientUtil util = new HttpClientUtil(url);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("EvdedPhone", agentId);// 设置参数
		map.put("EvdPhone", teacherId);// 设置参数
		map.put("EvdGateway", "");// 设置参数
		ObjectMapper json = new ObjectMapper();
		String param = "";
		try {
			param = json.writeValueAsString(map);// 转换字符串
		} catch (Exception e) {
			throw new RuntimeException(e);// 抛出运行时异常（事务回滚）
		}
		return util.post("threeway", param);// post调用
	}

	/**
	 * 录音
	 * @param agentId
	 * @param filename
	 */
	public String record(String agentId, String filename) {
		String url = PropertyUitls.getSysConfigSet("dialogAPI");// 获取free的ip配置
		HttpClientUtil util = new HttpClientUtil(url);// 实例化
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("PhoneNumber", agentId);// 设置参数
		map.put("FilePath", filename);// 设置参数
		map.put("Flag", "start");// 设置参数
		map.put("Limit", "0");// 设置参数
		ObjectMapper json = new ObjectMapper();
		String param = "";
		try {
			param = json.writeValueAsString(map);// 转换字符串
		} catch (Exception e) {
			throw new RuntimeException(e);// 抛出运行时异常（事务回滚）
		}
		return util.post("record", param);// post调用
	}
}
