package com.qtrmoon.dialog.serdao;

public interface IMyFsService {
	
	/**
	 * 监听
	 * @param agentId
	 * @param teacherId
	 * @return
	 */
	public String eavesdrop(String agentId, String teacherId);
	
	/**
	 * 录音
	 * @param agentId
	 * @param filePath
	 * @return
	 */
	public String record(String agentId, String filePath);
	
	/**
	 * 监听
	 * @param agentId
	 * @param teacherId
	 * @return
	 */
	public String threeway(String agentId, String teacherId);
}
