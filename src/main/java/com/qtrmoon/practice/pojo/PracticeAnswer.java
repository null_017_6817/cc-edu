package com.qtrmoon.practice.pojo;
import java.util.Date;
import java.util.Set;
import java.util.HashSet;
import org.json.simple.JSONObject;
import com.qtrmoon.common.PageForm;
import com.qtrmoon.toolkit.DateTransfer;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.dictionary.bsd.DictBean;

/** 实体类 */
public class PracticeAnswer extends PageForm{
	//Fields
	
	private Integer id;// 
	private Integer eid;// 
	private Integer etid;// 
	private String stunum;// 
	private String title;// 
	private String stepinfo;// 
	private String stuanswer;// 
	//Constructors
	/** default constructor */
	public PracticeAnswer() {
	
	}	
	//getter and setter
	/** 获取 */
	public Integer getId() {
		return this.id;
	}
	/** 设置 */
	public void setId(Integer id) {
		this.id = id;
	}
	/** 获取 */
	public Integer getEid() {
		return this.eid;
	}
	/** 设置 */
	public void setEid(Integer eid) {
		this.eid = eid;
	}
	/** 获取 */
	public Integer getEtid() {
		return this.etid;
	}
	/** 设置 */
	public void setEtid(Integer etid) {
		this.etid = etid;
	}
	/** 获取 */
	public String getStunum() {
		return this.stunum;
	}
	/** 设置 */
	public void setStunum(String stunum) {
		this.stunum = stunum;
	}
	/** 获取 */
	public String getTitle() {
		return this.title;
	}
	/** 设置 */
	public void setTitle(String title) {
		this.title = title;
	}
	/** 获取 */
	public String getStepinfo() {
		return this.stepinfo;
	}
	/** 设置 */
	public void setStepinfo(String stepinfo) {
		this.stepinfo = stepinfo;
	}
	/** 获取 */
	public String getStuanswer() {
		return this.stuanswer;
	}
	/** 设置 */
	public void setStuanswer(String stuanswer) {
		this.stuanswer = stuanswer;
	}
	/**
 * 判断主键有值
 * @return 有值返回true
 */
	public boolean hasKey() {
		if(id!=null&&id>0)return true;
		return false;
	}
	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("eid", eid);
		obj.put("etid", etid);
		obj.put("stunum", stunum);
		obj.put("title", title);
		obj.put("stepinfo", stepinfo);
		obj.put("stuanswer", stuanswer);
		return obj;
	}
	
	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("eid", eid);
		obj.put("etid", etid);
		obj.put("stunum", stunum);
		obj.put("title", title);
		obj.put("stepinfo", stepinfo);
		obj.put("stuanswer", stuanswer);
		return obj;
	}
}
