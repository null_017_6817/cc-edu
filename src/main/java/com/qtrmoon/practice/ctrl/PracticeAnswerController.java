package com.qtrmoon.practice.ctrl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qtrmoon.common.BaseController;
import com.qtrmoon.examination.pojo.ExerAnswer;
import com.qtrmoon.examination.pojo.ExerStep;
import com.qtrmoon.examination.serdao.IExaminationService;
import com.qtrmoon.manager.pojo.Person;
import com.qtrmoon.practice.pojo.PracticeAnswer;
import com.qtrmoon.practice.serdao.IPracticeService;
import com.qtrmoon.sysmanage.SysConstant;

/**
 * 控制器
 */
@Controller
@RequestMapping("/practice/practiceAnswer")
public class PracticeAnswerController extends BaseController{
	@Autowired
	private IPracticeService practiceService;								//声明模块Service实例
	@Autowired
	private IExaminationService examinationService; // 声明模块Service实例
	
	/**
	 * 查询方法，自带分页设置。
	 * @param practiceAnswer 承载查询条件的Bean
	 * @param request 可用来获取非Bean内的参数
	 * @param response 返回请求,输出Json
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/schPracticeAnswer.action")
	public void schPracticeAnswer(PracticeAnswer practiceAnswer,HttpServletRequest request,HttpServletResponse response) throws IOException{
		noCache(response);
		practiceAnswer.setCondition("");											//防sql注入
		practiceAnswer.assLike("stunum","title","stepinfo","stuanswer");

		List<PracticeAnswer> list=practiceService.schPracticeAnswer(practiceAnswer);		//调用Service查询数据
		/*将数据Bean的集合转换为Json集合*/
		JSONArray datas=new JSONArray();									//构造业务数据的JSON集合
		JSONObject obj;														//声明用于构造Bean的Json对象
		for(PracticeAnswer u:list){												//循环查询的数据集构造Json数据集
			obj=u.getJsonInDict();											//将Bean转换为Json对象
			obj.put("_oper",u.getId());								//为"操作列"设定主键值
			datas.add(obj);													//添加Json对象到Json集合
		}
		/*构造分页数据的JSON对象，并包装到返回页面的Json中*/
		JSONObject res=new JSONObject();									//声明页面返回Json
		res.put("total",practiceAnswer.getDatasize());							//设置数据总记录数
		res.put("rows", datas);												//设置数据集
		response.getWriter().print(res.toJSONString());						//输出到页面
	}
	
	/**
	 * 预添加or预修改or查看方法
	 * @param practiceAnswer 预添加时可传入一些初始值；预修改和查看时用来承载主键值
	 * @param toL 挂字典的字段是否需要转为字典Label值。true则进行转换
	 * @param response 返回请求,输出Json
	 * @throws IOException
	 */
	@RequestMapping(value="/viePracticeAnswer.action")
	public void viePracticeAnswer(PracticeAnswer practiceAnswer,Boolean toL,HttpServletResponse response) throws IOException{
		noCache(response);													//Ajax方法设定无缓存
		if(toL!=null&&toL){													//查看页面加载数据时调用，返回的对象已进行字典转换
			if(practiceAnswer.hasKey()){					//检测主键非空
				practiceAnswer=practiceService.schPracticeAnswerById(practiceAnswer.getId());			//按主键查询数据
				response.getWriter().print(practiceAnswer.getJsonInDict());						//输出经过字典转换Json对象到查看页(vie_)
			}
		}else{																//修改页面加载数据时调用
			if(practiceAnswer.hasKey()){	//检查主键非空
				practiceAnswer=practiceService.schPracticeAnswerById(practiceAnswer.getId());		//按主键查询数据
				response.getWriter().print(practiceAnswer.getJson());			//输出Json对象到修改页(upd_)
			}else{															//添加页面加载数据时调用。
				PracticeAnswer newpracticeAnswer=new PracticeAnswer();						//实例化新的Bean
				//newpracticeAnswer.set...	//设置添加时的默认值
				response.getWriter().print(newpracticeAnswer.getJson());		//输出Json对象到添加页(upd_)
			}
		}
	}
	
	/**
	 * 添加或修改的提交方法
	 * @param practiceAnswer 数据表单Bean
	 * @param response 返回请求
	 * @throws IOException
	 */
	@RequestMapping(value="/updPracticeAnswer.action")
	public void updPracticeAnswer(PracticeAnswer practiceAnswer,HttpServletResponse response) throws IOException{
		noCache(response);													//Ajax方法设定无缓存
		if(practiceAnswer.getId()==null){									//检测主键为空则
			practiceService.addPracticeAnswer(practiceAnswer);						//添加数据
		}else{																//主键非空则
			practiceService.updPracticeAnswer(practiceAnswer);						//修改数据
		}
	}
	
	/**
	 * 删除方法
	 * @param ids 待删除数据的主键数组
	 * @param response 返回请求
	 */
	@RequestMapping(value="/delPracticeAnswer.action")
	public void delPracticeAnswer(@RequestParam(value = "ids[]") Integer[] ids,HttpServletResponse response){
		noCache(response);													//Ajax方法设定无缓存
		practiceService.delPracticeAnswer(ids);									//删除数据
	}
	
	/**
	 * JSP页面跳转
	 * @param page jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value="/page.action")
	public String goPage(String page){										//放过本Ctrl的页面，不合法的跳转到error页。
		if("index".equals(page)||"practiceAnswer_sch".equals(page)||"practiceAnswer_vie".equals(page)||"practiceAnswer_upd".equals(page)){
			return "/practice/"+page;
		}else{
			return "/error";
		}
	}
	
	/**
	 * 根据topicid查询step
	 * @param exerStep
	 * @param request
	 * @param response
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value = "/getStepByTopicId.action")
	public Map<String, Object> getStepByTopicId(ExerStep exerStep,
			HttpServletRequest request, HttpServletResponse response) {

		Map<String, Object> result = new HashMap<String, Object>();// 实例化返回结果集
		try {
			Person person = (Person) request.getSession(true).getAttribute(// 获取当前用户
					SysConstant.CURRENT_USER);// 获取当前用户
			// 定义返回结果集
			List<ExerStep> resultlist = new ArrayList<ExerStep>();
			// 查询学生答案
			PracticeAnswer pa = new PracticeAnswer();
			pa.setStunum(person.getNumber());
			pa.setEtid(exerStep.getEtid());
			List<PracticeAnswer> pas = practiceService.schPracticeAnswerList(pa);
			// 默认步骤
			List<ExerStep> list = examinationService.schExerStepList(exerStep);// 查询数据
			ExerStep defalt = new ExerStep();
			defalt.setTitle(list.get(0).getTitle());
			defalt.setStepinfo(list.get(0).getStepinfo());
			defalt.setStandardAnswer(list.get(0).getStandardAnswer());
			defalt.setEtid(list.get(0).getEtid());
			defalt.setEid(list.get(0).getEid());
			defalt.setId(0);
			if(pas != null && pas.size() > 0){
				for( int i = 0; i< pas.size(); i++ ){
					PracticeAnswer pta = pas.get(i);
					if(pta != null){
						ExerStep es = new ExerStep();
						es.setEtid(es.getEtid());
						es.setTitle(pta.getTitle());
						es.setId(pta.getId());
						es.setStepinfo(defalt.getStepinfo());
						es.setStandardAnswer(pta.getStuanswer());
						resultlist.add(es);
					}
				}
			}else{
				resultlist.add(defalt);
			}
			result.put("data", resultlist);// 返回数据
			result.put("success", true);// 返回成功状态
			result.put("msg", "查询成功");// 返回提示信息
		} catch (Exception e) {
			e.printStackTrace();// 打印异常信息
			result.put("success", false);// 返回成功状态
			result.put("msg", "查询异常");// 返回提示信息
		} finally {
			return result;// 返回结果集
		}
	}
	
	/**
	 * 保存step
	 * @param exerStep
	 * @param answerId
	 * @param request
	 * @param response
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value = "/saveSxAnswer.action")
	public Map<String, Object> saveSxAnswer(@RequestBody List<ExerStep> exerSteps, Integer answerId,
			HttpServletRequest request, HttpServletResponse response) {

		Map<String, Object> result = new HashMap<String, Object>();// 初始化返回结果集
		try {
			Person person = (Person) request.getSession(true).getAttribute(// 获取当前用户
					SysConstant.CURRENT_USER);// 获取当前用户
			if (person != null) {// 判断当前用户是否为空
				List<ExerStep> steps = practiceService.saveSxAnswer(exerSteps,person.getNumber());
				result.put("success", true);
				result.put("data", steps);
			} else {
				result.put("success", false);// 返回成功状态
				result.put("msg", "请先登录");// 返回提示信息
			}
		} catch (Exception e) {
			e.printStackTrace();// 打印异常信息
			result.put("success", false);// 返回成功状态
			result.put("msg", "更新异常");// 返回提示信息
		} finally {
			return result;// 返回结果
		}
	}
	
	/**
	 * 实训营地删除脚本
	 * @param exerStep
	 * @param request
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value = "/delSxAnswer.action")
	public Map<String, Object> delSxAnswer(ExerStep exerStep,HttpServletRequest request){
		Map<String, Object> result = new HashMap<String, Object>();// 初始化返回结果集
		try {
			Person person = (Person) request.getSession(true).getAttribute(SysConstant.CURRENT_USER);// 获取当前用户
			if(exerStep != null && person != null && person.getNumber() != null){
				practiceService.delPracticeAnswer(exerStep.getId());
				result.put("success", true);
			}else{
				result.put("success", false);
				result.put("msg", "未登录或参数错误，请刷新后再试");
			}
		}catch(Exception e){
			e.printStackTrace();
			result.put("success", false);
			result.put("msg", "服务器异常");
		}finally{
			return result;
		}
	}
	
}
