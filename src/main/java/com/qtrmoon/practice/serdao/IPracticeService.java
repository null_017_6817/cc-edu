package com.qtrmoon.practice.serdao;
import java.util.List;
import java.util.Map;

import com.qtrmoon.examination.pojo.ExerStep;
import com.qtrmoon.practice.pojo.*;
/** practice服务接口 */
public interface IPracticeService {
	
	/***************** PracticeAnswer方法组 *****************/
	/**
	 * PracticeAnswer的条件查询方法
	 * @param practiceAnswer 承载查询条件的Bean
	 * @return 返回PracticeAnswer的集合
	 */
	public List<PracticeAnswer> schPracticeAnswer(PracticeAnswer practiceAnswer);
	
	/**
	 * PracticeAnswer的条件查询方法(无分页)
	 * @param practiceAnswer 承载查询条件的Bean
	 * @return 返回PracticeAnswer的集合
	 */
	public List<PracticeAnswer> schPracticeAnswerList(PracticeAnswer practiceAnswer);
	
	/**
	 * PracticeAnswer的主键查询方法
	 * @param id 主键值
	 * @return 返回PracticeAnswer实体
	 */
	public PracticeAnswer schPracticeAnswerById(Integer id);
	
	/**
	 * PracticeAnswer的添加方法
	 * @param practiceAnswer 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addPracticeAnswer(PracticeAnswer practiceAnswer);
	
	/**
	 * PracticeAnswer的修改方法
	 * @param practiceAnswer 承载数据的Bean
	 */
	public void updPracticeAnswer(PracticeAnswer practiceAnswer);
	
	/**
	 * PracticeAnswer的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delPracticeAnswer(Integer id);
	
	/**
	 * PracticeAnswer的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delPracticeAnswer(Integer[] ids);
	
	/**
	 * 保存实训答案
	 * @param exerSteps
	 * @param studNum
	 * @return
	 */
	public List<ExerStep> saveSxAnswer(List<ExerStep> exerSteps,String studNum);

}//end
