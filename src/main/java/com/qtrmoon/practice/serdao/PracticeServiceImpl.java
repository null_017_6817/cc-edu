package com.qtrmoon.practice.serdao;
import java.util.List;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qtrmoon.examination.pojo.ExerAnswer;
import com.qtrmoon.examination.pojo.ExerStep;
import com.qtrmoon.practice.pojo.*;

/** practice服务实现类 */
@Service("practiceService")
public class PracticeServiceImpl implements IPracticeService {
	@Autowired
	private PracticeAnswerMapper practiceAnswerMapper;


	/***************** PracticeAnswer方法组 *****************/
	/**
	 * PracticeAnswer的条件查询方法
	 * @param practiceAnswer 承载查询条件的Bean
	 * @return 返回PracticeAnswer的集合
	 */
	@Override
	public List<PracticeAnswer> schPracticeAnswer(PracticeAnswer practiceAnswer) {
		if(practiceAnswer.getPagesize()>0)PageHelper.startPage(practiceAnswer.getPage(), practiceAnswer.getPagesize());//设置分页显示
		List<PracticeAnswer> list=practiceAnswerMapper.schPracticeAnswer(practiceAnswer);
		if(practiceAnswer.getPagesize()>0){
			Page<PracticeAnswer> page = (Page<PracticeAnswer>)list;//为了设置总记录数先类型强转
			practiceAnswer.setDatasize(page.getTotal());
		}else{
			practiceAnswer.setDatasize(list.size());
		}
		return list;
	}
	
	/**
	 * PracticeAnswer的条件查询方法(无分页)
	 * @param practiceAnswer 承载查询条件的Bean
	 * @return 返回PracticeAnswer的集合
	 */
	@Override
	public List<PracticeAnswer> schPracticeAnswerList(PracticeAnswer practiceAnswer) {
		List<PracticeAnswer> list=practiceAnswerMapper.schPracticeAnswer(practiceAnswer);
		return list;
	}
	
	/**
	 * PracticeAnswer的主键查询方法
	 * @param id 主键值
	 * @return 返回PracticeAnswer实体
	 */
	@Override
	public PracticeAnswer schPracticeAnswerById(Integer id) {
		return practiceAnswerMapper.schPracticeAnswerById(id);
	}
	
	/**
	 * PracticeAnswer的添加方法
	 * @param practiceAnswer 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	@Override
	public Integer addPracticeAnswer(PracticeAnswer practiceAnswer) {
		return practiceAnswerMapper.addPracticeAnswer(practiceAnswer);
	}

	/**
	 * PracticeAnswer的修改方法
	 * @param practiceAnswer 承载数据的Bean
	 */
	@Override
	public void updPracticeAnswer(PracticeAnswer practiceAnswer) {
		practiceAnswerMapper.updPracticeAnswer(practiceAnswer);
	}

	/**
	 * PracticeAnswer的单记录删除方法
	 * @param id 要删除的主键值
	 */
	@Override
	public void delPracticeAnswer(Integer id) {
		practiceAnswerMapper.delPracticeAnswer(id);
	}

	/**
	 * PracticeAnswer的批量删除方法
	 * @param ids 主键值的数组
	 */
	@Override
	public void delPracticeAnswer(Integer[] ids) {
		practiceAnswerMapper.delPracticeAnswers(ids);
	}

	/**
	 * 保存实训步骤和答案
	 */
	public List<ExerStep> saveSxAnswer(List<ExerStep> exerSteps,String studNum){
		if(exerSteps != null && exerSteps.size() > 0 && studNum != null){
			for( int i = 0 ; i < exerSteps.size(); i ++ ){
				ExerStep step = exerSteps.get(i);
				if(step != null){
					PracticeAnswer pa = new PracticeAnswer();
					pa.setTitle(step.getTitle());
					pa.setEtid(step.getEtid());
					pa.setEid(step.getEid());
					pa.setStepinfo(step.getStepinfo());
					pa.setStuanswer(step.getStandardAnswer());
					pa.setStunum(studNum);
					if(step.getId() == 0){
						practiceAnswerMapper.addPracticeAnswer(pa);
						exerSteps.get(i).setId(pa.getId());
					}else{
						pa.setId(step.getId());
						practiceAnswerMapper.updPracticeAnswer(pa);
					}
				}
			}
		}
		return exerSteps;
	}
	
	
}//end
