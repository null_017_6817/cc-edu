package com.qtrmoon.mail.serdao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.qtrmoon.mail.pojo.Autograph;
/** 个性签名表实体映射 */
@Component
public interface AutographMapper {
	/**
	 * Autograph的条件查询方法
	 * @param autograph 承载查询条件
	 * @return 返回Autograph的集合
	 */
	public List<Autograph> schAutograph(Autograph autograph);
	
	/**
	 * Autograph的主键查询方法
	 * @param id 主键值
	 * @return 返回Autograph实体
	 */
	public Autograph schAutographById(Integer id);
	
	/**
	 * Autograph的添加方法
	 * @param autograph 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addAutograph(Autograph autograph);
	
	/**
	 * Autograph的修改方法
	 * @param autograph 承载数据的Bean
	 */
	public void updAutograph(Autograph autograph);
	
	/**
	 * Autograph的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delAutograph(Integer id);
	
	/**
	 * Autograph的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delAutographs(Integer[] ids);
	
	/**
	 * 根据学号查询数据
	 * @param number
	 * @return
	 */
	public Autograph schAutographByNum(String number);
}
