package com.qtrmoon.mail.serdao;

import java.util.List;
import org.springframework.stereotype.Component;

import com.qtrmoon.mail.pojo.Attac;
/** 附件表（邮件用）实体映射 */
@Component
public interface AttacMapper {
	/**
	 * Attac的条件查询方法
	 * @param attac 承载查询条件
	 * @return 返回Attac的集合
	 */
	public List<Attac> schAttac(Attac attac);
	
	/**
	 * Attac的主键查询方法
	 * @param id 主键值
	 * @return 返回Attac实体
	 */
	public Attac schAttacById(Integer id);
	
	/**
	 * Attac的添加方法
	 * @param attac 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addAttac(Attac attac);
	
	/**
	 * Attac的修改方法
	 * @param attac 承载数据的Bean
	 */
	public void updAttac(Attac attac);
	
	/**
	 * Attac的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delAttac(Integer id);
	
	/**
	 * Attac的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delAttacs(Integer[] ids);
}
