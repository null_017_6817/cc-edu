package com.qtrmoon.mail.serdao;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.qtrmoon.mail.pojo.Attac;
import com.qtrmoon.mail.pojo.Autograph;
import com.qtrmoon.mail.pojo.MailReceive;
import com.qtrmoon.mail.pojo.MailSend;
import com.qtrmoon.mail.pojo.Message;
/** mail服务接口 */
public interface IMailService {
	
	/***************** MailReceive方法组 *****************/
	/**
	 * MailReceive的条件查询方法
	 * @param mailReceive 承载查询条件的Bean
	 * @return 返回MailReceive的集合
	 */
	public List<MailReceive> schMailReceive(MailReceive mailReceive);
	
	/**
	 * MailReceive的条件查询方法(无分页)
	 * @param mailReceive 承载查询条件的Bean
	 * @return 返回MailReceive的集合
	 */
	public List<MailReceive> schMailReceiveList(MailReceive mailReceive);
	
	/**
	 * MailReceive的主键查询方法
	 * @param id 主键值
	 * @return 返回MailReceive实体
	 */
	public MailReceive schMailReceiveById(String id);
	
	/**
	 * MailReceive的添加方法
	 * @param mailReceive 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addMailReceive(MailReceive mailReceive);
	
	/**
	 * MailReceive的修改方法
	 * @param mailReceive 承载数据的Bean
	 */
	public void updMailReceive(MailReceive mailReceive);
	
	/**
	 * 更新（删除状态）数据
	 * @param ids
	 */
	public void updMailReceiveDel(String[] ids);
	
	/**
	 * MailReceive的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delMailReceive(String id);
	
	/**
	 * MailReceive的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delMailReceive(String[] ids);


	
	/***************** MailSend方法组 *****************/
	/**
	 * MailSend的条件查询方法
	 * @param mailSend 承载查询条件的Bean
	 * @return 返回MailSend的集合
	 */
	public List<MailSend> schMailSend(MailSend mailSend);
	
	/**
	 * MailSend的条件查询方法(无分页)
	 * @param mailSend 承载查询条件的Bean
	 * @return 返回MailSend的集合
	 */
	public List<MailSend> schMailSendList(MailSend mailSend);
	
	/**
	 * MailSend的主键查询方法
	 * @param id 主键值
	 * @return 返回MailSend实体
	 */
	public MailSend schMailSendById(String id);
	
	/**
	 * MailSend的添加方法
	 * @param mailSend 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addMailSend(MailSend mailSend);
	
	/**
	 * MailSend的修改方法
	 * @param mailSend 承载数据的Bean
	 */
	public void updMailSend(MailSend mailSend);
	
	/**
	 * MailSend的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delMailSend(Integer id);
	
	/**
	 * MailSend的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delMailSend(String[] ids);

	/**
	 * 发送邮件
	 * @param mailSend
	 */
	public List<String> updSend(MailSend mailSend);
	
	
	/***************** Attac方法组 *****************/
	/**
	 * Attac的条件查询方法
	 * @param attac 承载查询条件的Bean
	 * @return 返回Attac的集合
	 */
	public List<Attac> schAttac(Attac attac);
	
	/**
	 * Attac的条件查询方法(无分页)
	 * @param attac 承载查询条件的Bean
	 * @return 返回Attac的集合
	 */
	public List<Attac> schAttacList(Attac attac);
	
	/**
	 * Attac的主键查询方法
	 * @param id 主键值
	 * @return 返回Attac实体
	 */
	public Attac schAttacById(Integer id);
	
	/**
	 * Attac的添加方法
	 * @param attac 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addAttac(Attac attac);
	
	/**
	 * Attac的修改方法
	 * @param attac 承载数据的Bean
	 */
	public void updAttac(Attac attac);
	
	/**
	 * Attac的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delAttac(Integer id);
	
	/**
	 * Attac的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delAttac(Integer[] ids);


	
	/***************** Message方法组 *****************/
	/**
	 * Message的条件查询方法
	 * @param message 承载查询条件的Bean
	 * @return 返回Message的集合
	 */
	public List<Message> schMessage(Message message);
	
	/**
	 * Message的条件查询方法(无分页)
	 * @param message 承载查询条件的Bean
	 * @return 返回Message的集合
	 */
	public List<Message> schMessageList(Message message);
	
	/**
	 * Message的主键查询方法
	 * @param id 主键值
	 * @return 返回Message实体
	 */
	public Message schMessageById(String id);
	
	/**
	 * Message的添加方法
	 * @param message 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addMessage(Message message);
	
	/**
	 * 群发消息
	 * @param numbers
	 * @param message
	 * @return
	 */
	public void AddGroupMessage(String[] numbers ,Message message);
	
	/**
	 * Message的修改方法
	 * @param message 承载数据的Bean
	 */
	public void updMessage(Message message);
	
	/**
	 * Message的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delMessage(String id);
	
	/**
	 * Message的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delMessage(String[] ids);

	/**
	 * 根据发送人和接收人查询消息个数
	 * @param param
	 * @return
	 */
	public Integer schMsgNum(Map<String,Object> param);
	
	/**
	 * 批量更新已读状态
	 * @param param
	 * @return
	 */
	public Integer updMsgIsRead(Message message);
	
	
	public Attac addUploadFile(CommonsMultipartFile file,String path,Attac attac)throws IllegalStateException, IOException;
	

	
	/***************** Autograph方法组 *****************/
	/**
	 * Autograph的条件查询方法
	 * @param autograph 承载查询条件的Bean
	 * @return 返回Autograph的集合
	 */
	public List<Autograph> schAutograph(Autograph autograph);
	
	/**
	 * Autograph的主键查询方法
	 * @param id 主键值
	 * @return 返回Autograph实体
	 */
	public Autograph schAutographById(Integer id);
	
	/**
	 * Autograph的添加方法
	 * @param autograph 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addAutograph(Autograph autograph);
	
	/**
	 * Autograph的修改方法
	 * @param autograph 承载数据的Bean
	 */
	public void updAutograph(Autograph autograph);
	
	/**
	 * Autograph的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delAutograph(Integer id);
	
	/**
	 * Autograph的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delAutograph(Integer[] ids);

	/**
	 * 根据学号查询签名
	 * @param number
	 * @return
	 */
	public Autograph schAutographByNum(String number);
}//end