package com.qtrmoon.mail.serdao;

import java.util.List;
import org.springframework.stereotype.Component;

import com.qtrmoon.mail.pojo.MailReceive;
/** 邮件接收表实体映射 */
@Component
public interface MailReceiveMapper {
	/**
	 * MailReceive的条件查询方法
	 * @param mailReceive 承载查询条件
	 * @return 返回MailReceive的集合
	 */
	public List<MailReceive> schMailReceive(MailReceive mailReceive);
	
	/**
	 * MailReceive的主键查询方法
	 * @param id 主键值
	 * @return 返回MailReceive实体
	 */
	public MailReceive schMailReceiveById(String id);
	
	/**
	 * MailReceive的添加方法
	 * @param mailReceive 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addMailReceive(MailReceive mailReceive);
	
	/**
	 * MailReceive的修改方法
	 * @param mailReceive 承载数据的Bean
	 */
	public void updMailReceive(MailReceive mailReceive);
	
	/**
	 * MailReceive的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delMailReceive(String id);
	
	/**
	 * MailReceive的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delMailReceives(String[] ids);
}
