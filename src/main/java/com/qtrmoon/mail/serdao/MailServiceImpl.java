package com.qtrmoon.mail.serdao;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.qtrmoon.mail.constants.MailConstants;
import com.qtrmoon.mail.pojo.Attac;
import com.qtrmoon.mail.pojo.Autograph;
import com.qtrmoon.mail.pojo.MailReceive;
import com.qtrmoon.mail.pojo.MailSend;
import com.qtrmoon.mail.pojo.Message;
import com.qtrmoon.util.UuidUtil;

/** mail服务实现类 */
@Service("mailService")
public class MailServiceImpl implements IMailService {
	@Autowired
	private MailReceiveMapper mailReceiveMapper;


	/***************** MailReceive方法组 *****************/
	/**
	 * MailReceive的条件查询方法
	 * @param mailReceive 承载查询条件的Bean
	 * @return 返回MailReceive的集合
	 */
	@Override
	public List<MailReceive> schMailReceive(MailReceive mailReceive) {
		PageHelper.startPage(mailReceive.getPage(), mailReceive.getPagesize());//设置分页显示
		List<MailReceive> list=mailReceiveMapper.schMailReceive(mailReceive);
		Page<MailReceive> page = (Page<MailReceive>)list;//为了设置总记录数先类型强转
		mailReceive.setDatasize(page.getTotal());
		return list;
	}
	
	/**
	 * MailReceive的条件查询方法（无分页）
	 * @param mailReceive 承载查询条件的Bean
	 * @return 返回MailReceive的集合
	 */
	@Override
	public List<MailReceive> schMailReceiveList(MailReceive mailReceive) {
		List<MailReceive> list=mailReceiveMapper.schMailReceive(mailReceive);
		return list;
	}
	
	/**
	 * MailReceive的主键查询方法
	 * @param id 主键值
	 * @return 返回MailReceive实体
	 */
	@Override
	public MailReceive schMailReceiveById(String id) {
		return mailReceiveMapper.schMailReceiveById(id);
	}
	
	/**
	 * MailReceive的添加方法
	 * @param mailReceive 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	@Override
	public Integer addMailReceive(MailReceive mailReceive) {
		return mailReceiveMapper.addMailReceive(mailReceive);
	}

	/**
	 * MailReceive的修改方法
	 * @param mailReceive 承载数据的Bean
	 */
	@Override
	public void updMailReceive(MailReceive mailReceive) {
		mailReceiveMapper.updMailReceive(mailReceive);
	}

	/**
	 * 更新（删除状态）数据
	 * @param ids
	 */
	@Override
	public void updMailReceiveDel(String[] ids){
		if(ids != null ){
			for(String id : ids){
				MailReceive mail = mailReceiveMapper.schMailReceiveById(id);
				if(MailConstants.UN_DELETE.equals(mail.getIsDelete())){// 如果为未删除，则更新为回收站
					mail.setIsDelete(MailConstants.IS_DELETE);
					mailReceiveMapper.updMailReceive(mail);
				}else{
					// 如果为回收站，则删除数据
					mailReceiveMapper.delMailReceive(mail.getId());
				}
			}
		}
	}
	
	/**
	 * MailReceive的单记录删除方法
	 * @param id 要删除的主键值
	 */
	@Override
	public void delMailReceive(String id) {
		mailReceiveMapper.delMailReceive(id);
	}

	/**
	 * MailReceive的批量删除方法
	 * @param ids 主键值的数组
	 */
	@Override
	public void delMailReceive(String[] ids) {
		mailReceiveMapper.delMailReceives(ids);
	}


	@Autowired
	private MailSendMapper mailSendMapper;


	/***************** MailSend方法组 *****************/
	/**
	 * MailSend的条件查询方法
	 * @param mailSend 承载查询条件的Bean
	 * @return 返回MailSend的集合
	 */
	@Override
	public List<MailSend> schMailSend(MailSend mailSend) {
		PageHelper.startPage(mailSend.getPage(), mailSend.getPagesize());//设置分页显示
		List<MailSend> list=mailSendMapper.schMailSend(mailSend);
		Page<MailSend> page = (Page<MailSend>)list;//为了设置总记录数先类型强转
		mailSend.setDatasize(page.getTotal());
		return list;
	}
	
	/**
	 * MailSend的条件查询方法（无分页）
	 * @param mailSend 承载查询条件的Bean
	 * @return 返回MailSend的集合
	 */
	public List<MailSend> schMailSendList(MailSend mailSend) {
		List<MailSend> list=mailSendMapper.schMailSend(mailSend);
		return list;
	}
	
	/**
	 * MailSend的主键查询方法
	 * @param id 主键值
	 * @return 返回MailSend实体
	 */
	@Override
	public MailSend schMailSendById(String id) {
		return mailSendMapper.schMailSendById(id);
	}
	
	/**
	 * MailSend的添加方法
	 * @param mailSend 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	@Override
	public Integer addMailSend(MailSend mailSend) {
		return mailSendMapper.addMailSend(mailSend);
	}

	/**
	 * MailSend的修改方法
	 * @param mailSend 承载数据的Bean
	 */
	@Override
	public void updMailSend(MailSend mailSend) {
		mailSendMapper.updMailSend(mailSend);
	}

	/**
	 * MailSend的单记录删除方法
	 * @param id 要删除的主键值
	 */
	@Override
	public void delMailSend(Integer id) {
		mailSendMapper.delMailSend(id);
	}

	/**
	 * MailSend的批量删除方法
	 * @param ids 主键值的数组
	 */
	@Override
	public void delMailSend(String[] ids) {
		mailSendMapper.delMailSends(ids);
	}

	/**
	 * 发送邮件
	 */
	@Override
	public List<String> updSend(MailSend mailSend){
		
		List<String> allStu = new ArrayList<String>();
		// 收件人处理（由逗号拼接的字符串转换为字符串数组再转换为字符串list）
		String receive = mailSend.getReceiveNumShow();
		String[] receiveArg = receive.split(",");
		if(receiveArg != null){
			List<String> receives = Arrays.asList(receiveArg);
			allStu.addAll(receives);
		}
		// 抄送人处理（由逗号拼接的字符串转换为字符串数组再转换为字符串list）
		String duplicate = mailSend.getDuplicateNumShow();
		String[] duplicateArg = duplicate.split(",");
		if(duplicateArg != null){
			List<String> duplicates = Arrays.asList(duplicateArg);
			allStu.addAll(duplicates);
		}
		// 接收人与抄送人的总集合
		if(allStu != null && allStu.size() > 0){
			// 接收人与抄送人去重
			Set<String> linkedHashSet = new LinkedHashSet<String>(allStu);
			allStu = new ArrayList<String>(linkedHashSet);
			
			Date now = new Date();// 服务器当前时间
			mailSend.setCreateTime(now);// 创建时间
			mailSend.setUpdateTime(now);// 更新时间
			mailSend.setSendTime(now);// 发送时间
			mailSend.setIsSend(MailConstants.IS_SEND);// 发送状态为已发送
			mailSend.setIsDelete(MailConstants.UN_DELETE);// 删除状态为未删除
			mailSend.setId(UuidUtil.get32UUID());// uuid 
			mailSendMapper.addMailSend(mailSend);// 邮件发送表添加数据
			String title = mailSend.getTitle();
			String content = mailSend.getContent();
			for( String stuNum : allStu ){
				MailReceive mailReceive = new MailReceive();
				mailReceive.setId(UuidUtil.get32UUID());
				mailReceive.setSendNum(mailSend.getSendNum());// 发件人
				mailReceive.setReceiveNum(stuNum);// 收件人
				mailReceive.setReceiveNumShow(receive);// 收件人显示用
				mailReceive.setDuplicateNumShow(duplicate);// 抄送人显示用
				mailReceive.setTitle(title);// 邮件标题
				mailReceive.setContent(content);// 邮件内容
				mailReceive.setCreateTime(now);// 创建时间
				mailReceive.setUpdateTime(now);// 更新时间
				mailReceive.setReceiveTime(now);// 接收时间
				mailReceive.setIsRead(MailConstants.UN_READ);// 已读状态：未读
				mailReceive.setIsDelete(MailConstants.UN_DELETE);// 删除（回收站）状态：未删除
				mailReceive.setFileId(mailSend.getFileId());// 附件id
				mailReceive.setImgId(mailSend.getImgId());// 图片id
				mailReceiveMapper.addMailReceive(mailReceive);// 邮件接收表添加数据
			}
		}
		return allStu;
	}
	

	@Autowired
	private AttacMapper attacMapper;


	/***************** Attac方法组 *****************/
	/**
	 * Attac的条件查询方法
	 * @param attac 承载查询条件的Bean
	 * @return 返回Attac的集合
	 */
	@Override
	public List<Attac> schAttac(Attac attac) {
		PageHelper.startPage(attac.getPage(), attac.getPagesize());//设置分页显示
		List<Attac> list=attacMapper.schAttac(attac);
		Page<Attac> page = (Page<Attac>)list;//为了设置总记录数先类型强转
		attac.setDatasize(page.getTotal());
		return list;
	}
	
	/**
	 * Attac的条件查询方法（无分页）
	 * @param attac 承载查询条件的Bean
	 * @return 返回Attac的集合
	 */
	public List<Attac> schAttacList(Attac attac){
		List<Attac> list=attacMapper.schAttac(attac);
		return list;
	}
	
	/**
	 * Attac的主键查询方法
	 * @param id 主键值
	 * @return 返回Attac实体
	 */
	@Override
	public Attac schAttacById(Integer id) {
		return attacMapper.schAttacById(id);
	}
	
	/**
	 * Attac的添加方法
	 * @param attac 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	@Override
	public Integer addAttac(Attac attac) {
		return attacMapper.addAttac(attac);
	}

	/**
	 * Attac的修改方法
	 * @param attac 承载数据的Bean
	 */
	@Override
	public void updAttac(Attac attac) {
		attacMapper.updAttac(attac);
	}

	/**
	 * Attac的单记录删除方法
	 * @param id 要删除的主键值
	 */
	@Override
	public void delAttac(Integer id) {
		attacMapper.delAttac(id);
	}

	/**
	 * Attac的批量删除方法
	 * @param ids 主键值的数组
	 */
	@Override
	public void delAttac(Integer[] ids) {
		attacMapper.delAttacs(ids);
	}


	@Autowired
	private MessageMapper messageMapper;


	/***************** Message方法组 *****************/
	/**
	 * Message的条件查询方法
	 * @param message 承载查询条件的Bean
	 * @return 返回Message的集合
	 */
	@Override
	public List<Message> schMessage(Message message) {
		PageHelper.startPage(message.getPage(), message.getPagesize());//设置分页显示
		List<Message> list=messageMapper.schMessage(message);
		Page<Message> page = (Page<Message>)list;//为了设置总记录数先类型强转
		message.setDatasize(page.getTotal());
		return list;
	}
	
	/**
	 * Message的条件查询方法
	 * @param message 承载查询条件的Bean
	 * @return 返回Message的集合
	 */
	@Override
	public List<Message> schMessageList(Message message){
		List<Message> list=messageMapper.schMessage(message);
		return list;
	}
	
	/**
	 * Message的主键查询方法
	 * @param id 主键值
	 * @return 返回Message实体
	 */
	@Override
	public Message schMessageById(String id) {
		return messageMapper.schMessageById(id);
	}
	
	/**
	 * Message的添加方法
	 * @param message 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	@Override
	public Integer addMessage(Message message) {
		return messageMapper.addMessage(message);
	}

	/**
	 * @param message 承载数据的Bean，numbers批量发送的接收者
	 * @return 返回影响的行数
	 */
	public void AddGroupMessage(String[] numbers ,Message message){
		for( int i = 0; i < numbers.length; i++ ){
			Message entity = new Message();
			entity.setReceiveNum(numbers[i]);
			entity.setSendNum(message.getSendNum());
			entity.setContent(message.getContent());
			entity.setCreateTime(message.getCreateTime());
			entity.setIsRead(message.getIsRead());
			entity.setId(UuidUtil.get32UUID());
			messageMapper.addMessage(entity);
		}
	}
	
	/**
	 * Message的修改方法
	 * @param message 承载数据的Bean
	 */
	@Override
	public void updMessage(Message message) {
		messageMapper.updMessage(message);
	}

	/**
	 * Message的单记录删除方法
	 * @param id 要删除的主键值
	 */
	@Override
	public void delMessage(String id) {
		messageMapper.delMessage(id);
	}

	/**
	 * Message的批量删除方法
	 * @param ids 主键值的数组
	 */
	@Override
	public void delMessage(String[] ids) {
		messageMapper.delMessages(ids);
	}

	/**
	 * 根据发送人和接收人查询消息个数
	 * @param param
	 * @return
	 */
	@Override
	public Integer schMsgNum(Map<String,Object> param){
		return messageMapper.schMsgNum(param);
	}
	
	/**
	 * 批量更新已读状态
	 * @param param
	 * @return
	 */
	@Override
	public Integer updMsgIsRead(Message message){
		return messageMapper.updMsgIsRead(message);
	}
	
	/**
	 * 上传文件
	 */
	@Override
	public Attac addUploadFile(CommonsMultipartFile file ,String path,Attac attac) throws IllegalStateException, IOException{
		String realAllName = file.getOriginalFilename();
		String pattern = "";//文件格式
		if ((realAllName != null) && (realAllName.length() > 0)) { // 判断
			int dot = realAllName.lastIndexOf('.');   // 截取
			if ((dot >-1) && (dot < (realAllName.length() - 1))) {  // 判断 
				pattern = "."+realAllName.substring(dot + 1);   // 截取
			}
		}
		int rand = (int) (Math.random() * 10) + 10;// 两位随机数
		Date now = new Date();// 当前时间
		String fullName = now.getTime() + rand + pattern;// 拼接参数
		File targetFile = new File(path, fullName);// 文件对象
		if (!targetFile.exists()) {// 判断
			targetFile.mkdirs();// 创建文件夹
		}
		file.transferTo(targetFile);//上传文件
		attac.setUrl(fullName);
		attac.setName(realAllName);
		attac.setCreateTime(new Date());
		attacMapper.addAttac(attac);
		return attac;
	}

	@Autowired
	private AutographMapper autographMapper;


	/***************** Autograph方法组 *****************/
	/**
	 * Autograph的条件查询方法
	 * @param autograph 承载查询条件的Bean
	 * @return 返回Autograph的集合
	 */
	@Override
	public List<Autograph> schAutograph(Autograph autograph) {
		PageHelper.startPage(autograph.getPage(), autograph.getPagesize());//设置分页显示
		List<Autograph> list=autographMapper.schAutograph(autograph);
		Page<Autograph> page = (Page<Autograph>)list;//为了设置总记录数先类型强转
		autograph.setDatasize(page.getTotal());
		return list;
	}
	
	/**
	 * Autograph的主键查询方法
	 * @param id 主键值
	 * @return 返回Autograph实体
	 */
	@Override
	public Autograph schAutographById(Integer id) {
		return autographMapper.schAutographById(id);
	}
	
	/**
	 * Autograph的添加方法
	 * @param autograph 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	@Override
	public Integer addAutograph(Autograph autograph) {
		return autographMapper.addAutograph(autograph);
	}

	/**
	 * Autograph的修改方法
	 * @param autograph 承载数据的Bean
	 */
	@Override
	public void updAutograph(Autograph autograph) {
		autographMapper.updAutograph(autograph);
	}

	/**
	 * Autograph的单记录删除方法
	 * @param id 要删除的主键值
	 */
	@Override
	public void delAutograph(Integer id) {
		autographMapper.delAutograph(id);
	}

	/**
	 * Autograph的批量删除方法
	 * @param ids 主键值的数组
	 */
	@Override
	public void delAutograph(Integer[] ids) {
		autographMapper.delAutographs(ids);
	}

	/**
	 * 根据学号查询数据
	 * @param ids 主键值的数组
	 */
	@Override
	public Autograph schAutographByNum(String number){
		
		return null;
	}
	
}//end