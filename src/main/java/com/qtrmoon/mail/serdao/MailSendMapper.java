package com.qtrmoon.mail.serdao;

import java.util.List;
import org.springframework.stereotype.Component;

import com.qtrmoon.mail.pojo.MailSend;
/** 邮件接收表实体映射 */
@Component
public interface MailSendMapper {
	/**
	 * MailSend的条件查询方法
	 * @param mailSend 承载查询条件
	 * @return 返回MailSend的集合
	 */
	public List<MailSend> schMailSend(MailSend mailSend);
	
	/**
	 * MailSend的主键查询方法
	 * @param id 主键值
	 * @return 返回MailSend实体
	 */
	public MailSend schMailSendById(String id);
	
	/**
	 * MailSend的添加方法
	 * @param mailSend 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addMailSend(MailSend mailSend);
	
	/**
	 * MailSend的修改方法
	 * @param mailSend 承载数据的Bean
	 */
	public void updMailSend(MailSend mailSend);
	
	/**
	 * MailSend的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delMailSend(Integer id);
	
	/**
	 * MailSend的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delMailSends(String[] ids);
}
