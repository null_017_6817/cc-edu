package com.qtrmoon.mail.serdao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.qtrmoon.mail.pojo.Message;
/** 短信实训表实体映射 */
@Component
public interface MessageMapper {
	/**
	 * Message的条件查询方法
	 * @param message 承载查询条件
	 * @return 返回Message的集合
	 */
	public List<Message> schMessage(Message message);
	
	/**
	 * Message的主键查询方法
	 * @param id 主键值
	 * @return 返回Message实体
	 */
	public Message schMessageById(String id);
	
	/**
	 * Message的添加方法
	 * @param message 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addMessage(Message message);
	
	
	
	
	/**
	 * Message的修改方法
	 * @param message 承载数据的Bean
	 */
	public void updMessage(Message message);
	
	/**
	 * Message的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delMessage(String id);
	
	/**
	 * Message的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delMessages(String[] ids);
	
	/**
	 * （根据发送人和接收人）查询消息个数
	 * @param param
	 * @return
	 */
	public Integer schMsgNum(Map<String,Object> param);
	
	/**
	 * 批量更新已读状态
	 * @param param
	 */
	public Integer updMsgIsRead(Message message);
}
