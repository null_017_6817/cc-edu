package com.qtrmoon.mail.ctrl;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.qtrmoon.book.util.FilePath;
import com.qtrmoon.common.BaseController;
import com.qtrmoon.mail.pojo.Attac;
import com.qtrmoon.mail.serdao.IMailService;

/**
 * 附件表（邮件用）控制器
 */
@Controller
@RequestMapping("/mail/attac")
public class AttacController extends BaseController{
	@Autowired
	private IMailService mailService;								//声明模块Service实例

	/**
	 * 查询方法，自带分页设置。
	 * @param attac 承载查询条件的Bean
	 * @param request 可用来获取非Bean内的参数
	 * @param response 返回请求,输出Json
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/schAttac.action")
	public void schAttac(Attac attac,HttpServletRequest request,HttpServletResponse response) throws IOException{
		noCache(response);
		attac.setCondition("");											//防sql注入
		attac.assLike();

		List<Attac> list=mailService.schAttac(attac);		//调用Service查询数据
		/*将数据Bean的集合转换为Json集合*/
		JSONArray datas=new JSONArray();									//构造业务数据的JSON集合
		JSONObject obj;														//声明用于构造Bean的Json对象
		for(Attac u:list){												//循环查询的数据集构造Json数据集
			obj=u.getJsonInDict();											//将Bean转换为Json对象
			obj.put("_oper",u.getId());								//为"操作列"设定主键值
			datas.add(obj);													//添加Json对象到Json集合
		}
		/*构造分页数据的JSON对象，并包装到返回页面的Json中*/
		JSONObject res=new JSONObject();									//声明页面返回Json
		res.put("total",attac.getDatasize());							//设置数据总记录数
		res.put("rows", datas);												//设置数据集
		response.getWriter().print(res.toJSONString());						//输出到页面
	}
	
	/**
	 * 预添加or预修改or查看方法
	 * @param attac 预添加时可传入一些初始值；预修改和查看时用来承载主键值
	 * @param toL 挂字典的字段是否需要转为字典Label值。true则进行转换
	 * @param response 返回请求,输出Json
	 * @throws IOException
	 */
	@RequestMapping(value="/vieAttac.action")
	public void vieAttac(Attac attac,Boolean toL,HttpServletResponse response) throws IOException{
		noCache(response);													//Ajax方法设定无缓存
		if(toL!=null&&toL){													//查看页面加载数据时调用，返回的对象已进行字典转换
			if(attac.getId()!=null&&attac.getId()!=0){					//检测主键非空
				attac=mailService.schAttacById(attac.getId());			//按主键查询数据
				response.getWriter().print(attac.getJsonInDict());						//输出经过字典转换Json对象到查看页(vie_)
			}
		}else{																//修改页面加载数据时调用
			if(attac.getId()!=null&&attac.getId()!=0){	//检查主键非空
				attac=mailService.schAttacById(attac.getId());		//按主键查询数据
				response.getWriter().print(attac.getJson());			//输出Json对象到修改页(upd_)
			}else{															//添加页面加载数据时调用，可设置添加页面中一些属性的预设值。
				response.getWriter().print(new Attac().getJson());		//输出Json对象到添加页(upd_)
			}
		}
	}
	
	/**
	 * 添加或修改的提交方法
	 * @param attac 数据表单Bean
	 * @param response 返回请求
	 * @throws IOException
	 */
	@RequestMapping(value="/updAttac.action")
	public void updAttac(Attac attac,HttpServletResponse response) throws IOException{
		noCache(response);													//Ajax方法设定无缓存
		if(attac.getId()==null){									//检测主键为空则
			mailService.addAttac(attac);						//添加数据
		}else{																//主键非空则
			mailService.updAttac(attac);						//修改数据
		}
	}
	
	/**
	 * 删除方法
	 * @param ids 待删除数据的主键数组
	 * @param response 返回请求
	 */
	@RequestMapping(value="/delAttac.action")
	public void delAttac(@RequestParam(value = "ids[]") Integer[] ids,HttpServletResponse response){
		noCache(response);													//Ajax方法设定无缓存
		mailService.delAttac(ids);									//删除数据
	}
	
	/**
	 * JSP页面跳转
	 * @param page jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value="/page.action")
	public String goPage(String page){										//放过本Ctrl的页面，不合法的跳转到error页。
		if("index".equals(page)||"attac_sch".equals(page)||"attac_vie".equals(page)||"attac_upd".equals(page)){
			return "/mail/"+page;
		}else{
			return "/error";
		}
	}
	
	/**
	 * 上传文件
	 * @param fileInput
	 * @param request
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value = "/uploadFile.action")
	public Map<String,Object> uploadFile(@RequestParam(value = "fileInput", required = false) 
		CommonsMultipartFile fileInput,HttpServletRequest request){
		Map<String, Object> map = new HashMap<String, Object>();// 返回集合
		try {
			Attac param = new Attac();
			param.setType("file");
			String path = FilePath.getFilePath("mailFile");// 拼接
			Attac attac = mailService.addUploadFile(fileInput,path,param);
			if(attac != null && attac.getId() != null && !StringUtils.isEmpty(attac.getName())){
				map.put("success", true);// 设置属性
				map.put("msg", "操作成功");// 设置属性
				map.put("data", attac);// 设置属性
			}else{
				map.put("success", false);// 设置属性
				map.put("msg", "操作失败");// 设置属性
			}
		} catch (Exception e) {
			e.printStackTrace();// 打印异常
			map.put("success", false);// 设置属性
			map.put("msg", "服务器异常");// 设置属性
		}finally{
			return map;// 返回结果
		}
	}
	
	/**
	 * 上传图片
	 * @param fileInput
	 * @param request
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value = "/uploadImg.action")
	public Map<String,Object> uploadImg(@RequestParam(value = "imgInput", required = false) 
		CommonsMultipartFile imgInput,HttpServletRequest request){
		Map<String, Object> map = new HashMap<String, Object>();// 返回集合
		try {
			
			Attac param = new Attac();
			param.setType("img");
			String path = FilePath.getFilePath("mailImg");// 拼接
			Attac attac = mailService.addUploadFile(imgInput,path,param);
			if(attac != null && attac.getId() != null && !StringUtils.isEmpty(attac.getName())){
				map.put("success", true);// 设置属性
				map.put("msg", "操作成功");// 设置属性
				map.put("data", attac);// 设置属性
			}else{
				map.put("success", false);// 设置属性
				map.put("msg", "操作失败");// 设置属性
			}
		} catch (Exception e) {
			e.printStackTrace();// 打印异常
			map.put("success", false);// 设置属性
			map.put("msg", "服务器异常");// 设置属性
		}finally{
			return map;// 返回结果
		}
	}
	
	/**
	 * 根据ids查询数据
	 * @param ids
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value = "/getAttacList.action")
	public Map<String,Object> getAttacList(@RequestBody String[] ids){
		Map<String, Object> result = new HashMap<String, Object>();// 返回集合
		try{
			if(ids != null){
				String idsParam = "";// 拼接参数
				for( String id : ids ){
					if( !StringUtils.isEmpty(id) ){// 拼接参数
						if( !StringUtils.isEmpty(idsParam)){// 拼接参数
							idsParam = idsParam + ","+id;// 拼接参数
						}else{// 拼接参数
							idsParam = id;// 拼接参数
						}// 拼接参数
					}
				}
				if( !StringUtils.isEmpty(idsParam) ){
					Attac attac = new Attac();
					attac.setCondition("AND id In("+idsParam+")");
					List<Attac> list=mailService.schAttacList(attac);//调用Service查询数据
					result.put("data", list);
				}
				result.put("success", true);
				result.put("msg", "查询成功");
			}else{
				result.put("success", false);
				result.put("msg", "缺少必要参数");
			}
		} catch ( Exception e ){
			e.printStackTrace();
			result.put("success", false);
			result.put("msg", "获取失败服务器异常");
		} finally {
			return result;
		}
	}
	
	/**
	 * 下载附件或图片
	 * @param attac
	 * @param request
	 * @param response
	 * @throws IOException 
	 */
	@RequestMapping(value = "/download.action")
	public void download(Attac attac,HttpServletRequest request,HttpServletResponse response) throws IOException{
		
		if(attac != null && attac.getId() != null){
			Attac entity = mailService.schAttacById(attac.getId());
			if(entity != null && !StringUtils.isEmpty(entity.getName()) && !StringUtils.isEmpty(entity.getUrl())
					&& !StringUtils.isEmpty(entity.getType()) ){
				String realname = entity.getName();
				String filename = entity.getUrl();
				String type = entity.getType();
				String path = "";
				if("img".equals(type)){
					path = FilePath.getFilePath("mailImg");
				}else{
					path = FilePath.getFilePath("mailFile");
				}
				String filePath = path+filename;
				try {
					File file = new File(filePath);// 文件对象
					BufferedInputStream fis = new BufferedInputStream(
							new FileInputStream(filePath));// 输入流
					byte[] buffer = new byte[fis.available()];// 定义字节
					fis.read(buffer);// 读
					fis.close();// 关闭
					// 清空response
					response.reset();
					// 设置response的Header
					response.addHeader("Content-Disposition", "attachment;filename="
							+ new String(realname.getBytes(),"iso-8859-1"));// 设置response的Header
					response.addHeader("Content-Length", "" + file.length());// 设置response的Header
					OutputStream toClient = new BufferedOutputStream(
							response.getOutputStream());// 输出流
					//response.setContentType("application/vnd.ms-excel;charset=gb2312");// 设置属性
					toClient.write(buffer);// 写
					toClient.flush();// 刷新
					toClient.close();// 关闭
				} catch (Exception e) {
					e.printStackTrace();// 打印异常
				}
			}else{
				String print = new String("未查询到附件信息".getBytes("UTF-8"));
				response.setHeader("Content-type", "text/html;charset=UTF-8"); 
				response.setCharacterEncoding("UTF-8"); 
				response.getWriter().print(print);
			}
		}else{
			String print = new String("查询条件缺失".getBytes("UTF-8"));
			response.setHeader("Content-type", "text/html;charset=UTF-8"); 
			response.setCharacterEncoding("UTF-8"); 
			response.getWriter().print(print);
		}
	}
		
	
}
