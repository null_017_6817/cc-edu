package com.qtrmoon.mail.ctrl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qtrmoon.camp.ctrl.MessageSocket;
import com.qtrmoon.common.BaseController;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.dictionary.bsd.DictBean;
import com.qtrmoon.dictionary.bsd.DictCatalog;
import com.qtrmoon.mail.constants.MailConstants;
import com.qtrmoon.mail.constants.ReadEnum;
import com.qtrmoon.mail.pojo.Message;
import com.qtrmoon.mail.serdao.IMailService;
import com.qtrmoon.manager.pojo.Person;
import com.qtrmoon.sysmanage.SysConstant;
import com.qtrmoon.toolkit.DateTransfer;
import com.qtrmoon.util.UuidUtil;

/**
 * 短信实训表控制器
 */
@Controller
@RequestMapping("/mail/message")
public class MessageController extends BaseController{
	@Autowired
	private IMailService mailService;								//声明模块Service实例

	/**
	 * 查询方法，自带分页设置。
	 * @param message 承载查询条件的Bean
	 * @param request 可用来获取非Bean内的参数
	 * @param response 返回请求,输出Json
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/schMessage.action")
	public void schMessage(Message message,HttpServletRequest request,HttpServletResponse response) throws IOException{
		noCache(response);
		message.setCondition("");											//防sql注入
		message.assLike("content");
		message.setOrderCol("create_time DESC");
		List<Message> list=mailService.schMessage(message);		//调用Service查询数据
		/*将数据Bean的集合转换为Json集合*/
		JSONArray datas=new JSONArray();									//构造业务数据的JSON集合
		JSONObject obj;														//声明用于构造Bean的Json对象
		for(Message u:list){												//循环查询的数据集构造Json数据集
			obj=u.getJsonInDict();											//将Bean转换为Json对象
			obj.put("_oper",u.getId());								//为"操作列"设定主键值
			datas.add(obj);													//添加Json对象到Json集合
		}
		/*构造分页数据的JSON对象，并包装到返回页面的Json中*/
		JSONObject res=new JSONObject();									//声明页面返回Json
		res.put("total",message.getDatasize());							//设置数据总记录数
		res.put("rows", datas);												//设置数据集
		response.getWriter().print(res.toJSONString());						//输出到页面
	}
	
	/**
	 * 预添加or预修改or查看方法
	 * @param message 预添加时可传入一些初始值；预修改和查看时用来承载主键值
	 * @param toL 挂字典的字段是否需要转为字典Label值。true则进行转换
	 * @param response 返回请求,输出Json
	 * @throws IOException
	 */
	@RequestMapping(value="/vieMessage.action")
	public void vieMessage(Message message,Boolean toL,HttpServletResponse response) throws IOException{
		noCache(response);													//Ajax方法设定无缓存
		if(toL!=null&&toL){													//查看页面加载数据时调用，返回的对象已进行字典转换
			if(message != null && !StringUtils.isEmpty(message.getId())){					//检测主键非空
				message=mailService.schMessageById(message.getId());			//按主键查询数据
				response.getWriter().print(message.getJsonInDict());						//输出经过字典转换Json对象到查看页(vie_)
			}
		}else{													//修改页面加载数据时调用
			if(message != null && !StringUtils.isEmpty(message.getId())){	//检查主键非空
				message=mailService.schMessageById(message.getId());		//按主键查询数据
				response.getWriter().print(message.getJson());			//输出Json对象到修改页(upd_)
			}else{															//添加页面加载数据时调用，可设置添加页面中一些属性的预设值。
				response.getWriter().print(new Message().getJson());		//输出Json对象到添加页(upd_)
			}
		}
	}
	
	/**
	 * 添加或修改的提交方法
	 * @param message 数据表单Bean
	 * @param response 返回请求
	 * @throws IOException
	 */
	@RequestMapping(value="/updMessage.action")
	public void updMessage(Message message,HttpServletResponse response) throws IOException{
		noCache(response);													//Ajax方法设定无缓存
		if(message.getId()==null){									//检测主键为空则
			mailService.addMessage(message);						//添加数据
		}else{																//主键非空则
			mailService.updMessage(message);						//修改数据
		}
	}
	
	/**
	 * 删除方法
	 * @param ids 待删除数据的主键数组
	 * @param response 返回请求
	 */
	@RequestMapping(value="/delMessage.action")
	public void delMessage(@RequestParam(value = "ids[]") String[] ids,HttpServletResponse response){
		noCache(response);													//Ajax方法设定无缓存
		mailService.delMessage(ids);									//删除数据
	}
	
	/**
	 * JSP页面跳转
	 * @param page jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value="/page.action")
	public String goPage(String page){										//放过本Ctrl的页面，不合法的跳转到error页。
		if("index".equals(page)||"message_sch".equals(page)||"message_vie".equals(page)||"message_upd".equals(page)){
			if("message_sch".equals(page)){
				// 是否已读的字典
				List<DictBean> dictList = DictBuffer.getDict("IS_READ_SHOW");
				if(dictList == null || dictList.size() == 0){
					DictCatalog ca = new DictCatalog("IS_READ_SHOW");
					ca.setStyle("L#D#N");
					dictList = new ArrayList<DictBean>();
					for(ReadEnum r : ReadEnum.values()){
						if(r != null && r.getCode() != null && r.getLabel() != null ){
							DictBean dic = new DictBean();
							dic.setId(r.getCode());
							dic.setLabel(r.getLabel());
							dictList.add(dic);
						}
					}
					DictBuffer.addCatalogToCache(ca, dictList);
				}
			}
			return "/mail/"+page;
		}else{
			return "/error";
		}
	}
	
	/**
	 * 查询信息列表
	 * @param message
	 * @param request
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value="/schMsgList.action")
	public Map<String,Object> schMsgList(Message message,HttpServletRequest request){
		Map<String,Object> result = new HashMap<String,Object>();
		Person person = (Person)request.getSession(true).getAttribute(SysConstant.CURRENT_USER);
		try{
			if(person != null && !StringUtils.isEmpty(person.getNumber()) 
					&& message != null && !StringUtils.isEmpty(message.getSendNum())){
				String sendNum = message.getSendNum();
				String receiveNum = person.getNumber();
				Message param = new Message();
				String sql = "AND ( (SEND_NUM = '"+sendNum+"' AND RECEIVE_NUM = '"+receiveNum+"')"
						+ " || (SEND_NUM = '"+receiveNum+"' AND RECEIVE_NUM = '"+sendNum+"') )";
				param.setCondition(sql);
				param.setOrderCol("CREATE_TIME ASC");
				List<Message> list = mailService.schMessageList(param);	// 调用Service查询数据
				result.put("success", true);
				result.put("msg", "查询成功");
				result.put("data", list);
			}else{
				result.put("success", false);
				result.put("msg", "查询失败<必要条件丢失>");
			}
		}catch(Exception e){
			e.printStackTrace();
			result.put("success", false);
			result.put("msg", "查询失败<服务器异常>");
		}finally{
			return result;
		}
	}
	
	/**
	 * 发送消息
	 * @param message
	 * @param request
	 * @return
	 */
	@SuppressWarnings({ "finally", "unchecked" })
	@ResponseBody
	@RequestMapping(value="/sendMsg.action")
	public Map<String,Object> sendMsg(Message message,HttpServletRequest request){
		Map<String,Object> result = new HashMap<String,Object>();
		Person person = (Person)request.getSession(true).getAttribute(SysConstant.CURRENT_USER);
		try{
			if(person != null && !StringUtils.isEmpty( person.getNumber() ) 
					&& message != null && !StringUtils.isEmpty(message.getReceiveNum()) ){
				message.setId(UuidUtil.get32UUID());
				Date now = new Date();
				message.setCreateTime(now);
				message.setSendNum(person.getNumber());
				message.setIsRead(MailConstants.UN_READ);
				mailService.addMessage(message);
				CopyOnWriteArraySet<MessageSocket> sockets = MessageSocket.getWebSocketSet();// 获取socket对象列表
				for(MessageSocket socket : sockets){
					if( socket != null && message.getReceiveNum().equals(socket.getNumber()) ){
						JSONObject obj = new JSONObject();
						obj.put("success", "success");
						obj.put("content", message.getContent());
						obj.put("sendNum", person.getNumber());
						obj.put("sendName", person.getName());
						obj.put("createTime", DateTransfer.toString(now,"yyyy-MM-dd HH:mm:ss"));
						socket.sendMessage(obj.toJSONString());// 发送socket消息
					}
				}
				result.put("success", true);
				result.put("msg", "发送成功");
				result.put("data", message);
			}else{
				result.put("success", false);
				result.put("msg", "查询失败<必要条件丢失>");
			}
		}catch(Exception e){
			e.printStackTrace();
			result.put("success", false);
			result.put("msg", "查询失败<服务器异常>");
		}finally{
			return result;
		}
	}
	
	/**
	 * 群发消息
	 * @param numbers
	 * @param message
	 * @param request
	 * @return
	 */
	@SuppressWarnings({ "finally", "unchecked" })
	@ResponseBody
	@RequestMapping(value="/sendGroupMsg.action")
	public Map<String,Object> sendGroupMsg( @RequestBody Message message,HttpServletRequest request){
		Map<String,Object> result = new HashMap<String,Object>();
		Person person = (Person)request.getSession(true).getAttribute(SysConstant.CURRENT_USER);
		String[] numbers = message.getReceiveNums();
		try{
			if( person != null && !StringUtils.isEmpty(person.getNumber()) && 
					numbers != null && message != null && !StringUtils.isEmpty(message.getContent()) ){
				message.setSendNum(person.getNumber());
				message.setCreateTime(new Date());
				message.setIsRead(MailConstants.UN_READ);
				mailService.AddGroupMessage(numbers, message);
				CopyOnWriteArraySet<MessageSocket> sockets = MessageSocket.getWebSocketSet();// 获取socket对象列表
				for( int i = 0; i < numbers.length; i++ ){
					for(MessageSocket socket : sockets){
						if( socket != null && numbers[i] != null && numbers[i].equals(socket.getNumber()) ){
							JSONObject obj = new JSONObject();
							obj.put("content", message.getContent());
							obj.put("sendNum", person.getNumber());
							obj.put("sendName", person.getName());
							socket.sendMessage(obj.toJSONString());// 发送socket消息
						}
					}
				}
				result.put("success", true);
				result.put("msg", "发送成功");
				result.put("data", message);
			}else{
				result.put("success", false);
				result.put("msg", "查询失败<必要条件丢失>");
			}
		}catch(Exception e){
			e.printStackTrace();
			result.put("success", false);
			result.put("msg", "查询失败<服务器异常>");
		}finally{
			return result;
		}
	}
	
	/**
	 * 批量更新已读状态
	 * @param message
	 * @param request
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value="/changeIsRead.action")
	public Map<String,Object> changeIsRead(Message message,HttpServletRequest request){
		Map<String,Object> result = new HashMap<String,Object>();
		Person person = (Person)request.getSession(true).getAttribute(SysConstant.CURRENT_USER);
		try{
			if(message != null && person != null && !StringUtils.isEmpty(message.getSendNum())
					&& !StringUtils.isEmpty(person.getNumber())){
				message.setIsRead(MailConstants.IS_READ);
				message.setReceiveNum(person.getNumber());
				mailService.updMsgIsRead(message);
				result.put("success", true);
				result.put("msg", "发送成功");
			}else{
				result.put("success", false);
				result.put("msg", "更新失败<必要条件丢失>");
			}
		}catch(Exception e){
			e.printStackTrace();
			result.put("success", false);
			result.put("msg", "查询失败<服务器异常>");
		}finally{
			return result;
		}
	}
	
	/**
	 * 根据老师班级
	 * @param request
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value="getClassByUser.action")
	public Map<String,Object> getClassByUser( HttpServletRequest request ){
		Map<String,Object> result = new HashMap<String,Object>();
		// 获取当前登录用户
		Person person = (Person)request.getSession(true).
			getAttribute(SysConstant.CURRENT_USER);
		try{
			if(person != null && person.getCnumber() != null){
				String cnumber = person.getCnumber();
				List<Map<String,String>> dicts = new ArrayList<Map<String,String>>();
				if(cnumber.contains(",")){
					String[] cnumberArr = cnumber.split(",");
					for( int i = 0 ; i < cnumberArr.length; i++ ){
						if(cnumberArr[i] != null){
							DictBean dict = DictBuffer.getDictById("CC_CLASSES", cnumberArr[i]);
							if(dict != null ){
								Map<String,String> map = new HashMap<String,String>();
								map.put("id", dict.getId());
								map.put("label", dict.getLabel());
								dicts.add(map);
								
							}
						}
					}
				}else{
					DictBean dict = DictBuffer.getDictById("CC_CLASSES", cnumber);
					if(dict != null){
						Map<String,String> map = new HashMap<String,String>();
						map.put("id", dict.getId());
						map.put("label", dict.getLabel());
						dicts.add(map);
					}
				}
				result.put("success", true);
				result.put("data", dicts);
			}else{
				result.put("success", false);
				result.put("msg", "请刷新后再试，或未设置班级");
			}
		}catch( Exception e ){
			result.put("success", false);
			result.put("msg", "服务器异常");
			logger.error("根据老师班级构造字典：");
			e.printStackTrace();
		}finally{
			return result;
		}
	}
}
