package com.qtrmoon.mail.ctrl;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qtrmoon.common.BaseController;
import com.qtrmoon.mail.constants.MailConstants;
import com.qtrmoon.mail.pojo.MailReceive;
import com.qtrmoon.mail.serdao.IMailService;
import com.qtrmoon.manager.pojo.Person;
import com.qtrmoon.manager.pojo.Stud;
import com.qtrmoon.manager.serdao.IManagerService;
import com.qtrmoon.sysmanage.SysConstant;

/**
 * 邮件接收表控制器
 */
@Controller
@RequestMapping("/mail/mailReceive")
public class MailReceiveController extends BaseController{
	@Autowired
	private IMailService mailService;								//声明模块Service实例
	@Autowired
	private IManagerService managerService; // 声明模块Service实例
	
	/**
	 * 查询方法，自带分页设置。
	 * @param mailReceive 承载查询条件的Bean
	 * @param request 可用来获取非Bean内的参数
	 * @param response 返回请求,输出Json
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/schMailReceive.action")
	public void schMailReceive(MailReceive mailReceive,HttpServletRequest request,HttpServletResponse response) throws IOException{
		noCache(response);
		mailReceive.setCondition("");											//防sql注入
		mailReceive.assLike("content","receiveNumShow");

		List<MailReceive> list=mailService.schMailReceive(mailReceive);		//调用Service查询数据
		/*将数据Bean的集合转换为Json集合*/
		JSONArray datas=new JSONArray();									//构造业务数据的JSON集合
		JSONObject obj;														//声明用于构造Bean的Json对象
		for(MailReceive u:list){												//循环查询的数据集构造Json数据集
			obj=u.getJsonInDict();											//将Bean转换为Json对象
			obj.put("_oper",u.getId());								//为"操作列"设定主键值
			datas.add(obj);													//添加Json对象到Json集合
		}
		/*构造分页数据的JSON对象，并包装到返回页面的Json中*/
		JSONObject res=new JSONObject();									//声明页面返回Json
		res.put("total",mailReceive.getDatasize());							//设置数据总记录数
		res.put("rows", datas);												//设置数据集
		response.getWriter().print(res.toJSONString());						//输出到页面
	}
	
	/**
	 * 预添加or预修改or查看方法
	 * @param mailReceive 预添加时可传入一些初始值；预修改和查看时用来承载主键值
	 * @param toL 挂字典的字段是否需要转为字典Label值。true则进行转换
	 * @param response 返回请求,输出Json
	 * @throws IOException
	 */
	@RequestMapping(value="/vieMailReceive.action")
	public void vieMailReceive(MailReceive mailReceive,Boolean toL,HttpServletResponse response) throws IOException{
		noCache(response);													//Ajax方法设定无缓存
		if(toL!=null&&toL){													//查看页面加载数据时调用，返回的对象已进行字典转换
			if(!StringUtils.isEmpty(mailReceive.getId())){					//检测主键非空
				mailReceive=mailService.schMailReceiveById(mailReceive.getId());			//按主键查询数据
				response.getWriter().print(mailReceive.getJsonInDict());						//输出经过字典转换Json对象到查看页(vie_)
			}
		}else{																//修改页面加载数据时调用
			if(!StringUtils.isEmpty(mailReceive.getId())){	//检查主键非空
				mailReceive=mailService.schMailReceiveById(mailReceive.getId());		//按主键查询数据
				response.getWriter().print(mailReceive.getJson());			//输出Json对象到修改页(upd_)
			}else{															//添加页面加载数据时调用，可设置添加页面中一些属性的预设值。
				response.getWriter().print(new MailReceive().getJson());		//输出Json对象到添加页(upd_)
			}
		}
	}
	
	/**
	 * 添加或修改的提交方法
	 * @param mailReceive 数据表单Bean
	 * @param response 返回请求
	 * @throws IOException
	 */
	@RequestMapping(value="/updMailReceive.action")
	public void updMailReceive(MailReceive mailReceive,HttpServletResponse response) throws IOException{
		noCache(response);													//Ajax方法设定无缓存
		if(mailReceive.getId()==null){									//检测主键为空则
			mailService.addMailReceive(mailReceive);						//添加数据
		}else{																//主键非空则
			mailService.updMailReceive(mailReceive);						//修改数据
		}
	}
	
	/**
	 * 删除方法
	 * @param ids 待删除数据的主键数组
	 * @param response 返回请求
	 */
	@RequestMapping(value="/delMailReceive.action")
	public void delMailReceive(@RequestParam(value = "ids[]") String[] ids,HttpServletResponse response){
		noCache(response);													//Ajax方法设定无缓存
		mailService.delMailReceive(ids);									//删除数据
	}
	
	/**
	 * JSP页面跳转
	 * @param page jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value="/page.action")
	public String goPage(String page){										//放过本Ctrl的页面，不合法的跳转到error页。
		if("index".equals(page)||"mailReceive_sch".equals(page)||"mailReceive_vie".equals(page)||"mailReceive_upd".equals(page)){
			return "/mail/"+page;
		}else{
			return "/error";
		}
	}
	
	
	/**
	 * 查询列表数据（无分页）
	 * @param mailReceive
	 * @param request
	 * @param response
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value="/schMailReceiveList.action")
	public Map<String,Object> schMailReceiveList(@RequestBody MailReceive mailReceive,HttpServletRequest request){
		Map<String,Object> result = new HashMap<String,Object>();
		try{
			Person person = (Person)request.getSession(true).getAttribute(SysConstant.CURRENT_USER);
			if( person != null && !StringUtils.isEmpty(person.getNumber()) ){
				mailReceive.assLike("title");
				mailReceive.setReceiveNum(person.getNumber());//设置接收人为当前用户
				mailReceive.setOrderCol("update_time DESC");
				List<MailReceive> list = mailService.schMailReceiveList(mailReceive);		//调用Service查询数据
				if(list != null && list.size() > 0){
					for( MailReceive mail : list ){
						if(mail != null && !StringUtils.isEmpty( mail.getReceiveNumShow() ) ){
							String re = mail.getReceiveNumShow() == null? "":mail.getReceiveNumShow();
							String[] reArg = re.split(",");
							String receive = "";
							for(String num : reArg){
								if(!StringUtils.isEmpty(num)){
									Stud stu = managerService.schStudByXh(num);
									if(stu != null){
										if(!StringUtils.isEmpty(receive)){
											receive = receive + "," + stu.getName();
										}else{
											receive = stu.getName();
										}
									}
								}
							}
							if(!StringUtils.isEmpty(receive)){
								mail.setReceiveNumShow(receive);
							}
							
							String du = mail.getDuplicateNumShow() == null? "":mail.getDuplicateNumShow();
							String[] duArg = du.split(",");
							String dupli = "";
							for(String num : duArg){
								if(!StringUtils.isEmpty(num)){
									Stud stu = managerService.schStudByXh(num);
									if(stu != null){
										if(!StringUtils.isEmpty(dupli)){
											dupli = dupli + "," + stu.getName();
										}else{
											dupli = stu.getName();
										}
									}
								}
							}
							if(!StringUtils.isEmpty(dupli)){
								mail.setDuplicateNumShow(dupli);
							}
						}
					}
				}
				result.put("success", true);
				result.put("msg", "查询成功");
				result.put("data", list);
			}else{
				result.put("success", false);
				result.put("msg", "当前登录状态异常，请重新登录");
			}
		}catch(Exception e){
			e.printStackTrace();
			result.put("success", false);
			result.put("msg", "服务器异常");
		}finally{
			return result;
		}
	}
	
	/**
	 * 修改邮件状态为已读
	 * @param mailReceive
	 * @param request
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value="/readMail.action")
	public Map<String,Object> readMail(MailReceive mailReceive,HttpServletRequest request){
		Map<String,Object> result = new HashMap<String,Object>();
		try{
			if(mailReceive != null && !StringUtils.isEmpty(mailReceive.getId())){
				MailReceive mail = mailService.schMailReceiveById(mailReceive.getId());
				if( mail != null ){
					mail.setIsRead(MailConstants.IS_READ);// 更新状态为已读
					mailService.updMailReceive(mail);
					result.put("success", true);
					result.put("msg", "更新成功");
					result.put("data", mail);
				}else{
					result.put("success", false);
					result.put("msg", "更新失败！<未查询到数据>");
				}
			}else{
				result.put("success", false);
				result.put("msg", "更新失败！<必要参数为空>");
			}
		}catch(Exception e){
			e.printStackTrace();
			result.put("success", false);
			result.put("msg", "更新失败！<服务器异常>");
		}finally{
			return result;
		}
	}
	
	/**
	 * 放入回收站或删除数据
	 * @param mailReceive
	 * @param request
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value="/recycleOrDelete.action")
	public Map<String,Object> recycleOrDelete(@RequestBody String[] ids){
		Map<String,Object> result = new HashMap<String,Object>();
		try{
			if(ids != null ){
				mailService.updMailReceiveDel(ids);
				result.put("success", true);
				result.put("msg", "删除成功！");
			}else{
				result.put("success", false);
				result.put("msg", "删除失败！<必要参数为空>");
			}
		}catch(Exception e){
			e.printStackTrace();
			result.put("success", false);
			result.put("msg", "删除失败！<服务器异常>");
		}finally{
			return result;
		}
	}
	
	/**
	 * 删除数据
	 * @param ids
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value="/setTop.action")
	public Map<String,Object> setTop(@RequestBody String[] ids){
		Map<String ,Object> result = new HashMap<String,Object>();
		try{
			if(ids != null){
				MailReceive receive = mailService.schMailReceiveById(ids[0]);
				receive.setUpdateTime(new Date());
				mailService.updMailReceive(receive);
				result.put("success", true);
				result.put("msg", "置顶成功");
				result.put("data", ids);
			}else{
				result.put("success", false);
				result.put("msg", "置顶失败！<必要参数丢失>");
			}
		}catch(Exception e){
			e.printStackTrace();
			result.put("success", false);
			result.put("msg", "置顶失败！<服务器异常>");
		}finally{
			return result;
		}
	}
	
}
