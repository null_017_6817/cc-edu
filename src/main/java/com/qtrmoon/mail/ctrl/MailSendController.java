package com.qtrmoon.mail.ctrl;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qtrmoon.camp.ctrl.MailSocket;
import com.qtrmoon.common.BaseController;
import com.qtrmoon.mail.constants.MailConstants;
import com.qtrmoon.mail.pojo.MailSend;
import com.qtrmoon.mail.serdao.IMailService;
import com.qtrmoon.manager.pojo.Person;
import com.qtrmoon.manager.pojo.Stud;
import com.qtrmoon.manager.serdao.IManagerService;
import com.qtrmoon.sysmanage.SysConstant;
import com.qtrmoon.util.UuidUtil;

/**
 * 邮件接收表控制器
 */
@Controller
@RequestMapping("/mail/mailSend")
public class MailSendController extends BaseController{
	@Autowired
	private IMailService mailService;								//声明模块Service实例
	@Autowired
	private IManagerService managerService; // 声明模块Service实例
	
	/**
	 * 查询方法，自带分页设置。
	 * @param mailSend 承载查询条件的Bean
	 * @param request 可用来获取非Bean内的参数
	 * @param response 返回请求,输出Json
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/schMailSend.action")
	public void schMailSend(MailSend mailSend,HttpServletRequest request,HttpServletResponse response) throws IOException{
		noCache(response);
		mailSend.setCondition("");											//防sql注入
		if(mailSend.getReceiveNum() != null && !"".equals(mailSend.getReceiveNum()) ){
			mailSend.setReceiveNumShow(mailSend.getReceiveNum());
			mailSend.assLike("title","content","receiveNumShow");
		}else{
			mailSend.assLike("title","content");
		}
		mailSend.assLike("cnumber");

		List<MailSend> list=mailService.schMailSend(mailSend);		//调用Service查询数据
		/*将数据Bean的集合转换为Json集合*/
		JSONArray datas=new JSONArray();									//构造业务数据的JSON集合
		JSONObject obj;														//声明用于构造Bean的Json对象
		for(MailSend u:list){												//循环查询的数据集构造Json数据集
			obj=u.getJsonInDict();											//将Bean转换为Json对象
			obj.put("_oper",u.getId());								//为"操作列"设定主键值
			datas.add(obj);													//添加Json对象到Json集合
		}
		/*构造分页数据的JSON对象，并包装到返回页面的Json中*/
		JSONObject res=new JSONObject();									//声明页面返回Json
		res.put("total",mailSend.getDatasize());							//设置数据总记录数
		res.put("rows", datas);												//设置数据集
		response.getWriter().print(res.toJSONString());						//输出到页面
	}
	
	/**
	 * 预添加or预修改or查看方法
	 * @param mailSend 预添加时可传入一些初始值；预修改和查看时用来承载主键值
	 * @param toL 挂字典的字段是否需要转为字典Label值。true则进行转换
	 * @param response 返回请求,输出Json
	 * @throws IOException
	 */
	@RequestMapping(value="/vieMailSend.action")
	public void vieMailSend(MailSend mailSend,Boolean toL,HttpServletResponse response) throws IOException{
		noCache(response);													//Ajax方法设定无缓存
		if(toL!=null&&toL){													//查看页面加载数据时调用，返回的对象已进行字典转换
			if( !StringUtils.isEmpty(mailSend.getId())){					//检测主键非空
				mailSend=mailService.schMailSendById(mailSend.getId());			//按主键查询数据
				response.getWriter().print(mailSend.getJsonInDict());						//输出经过字典转换Json对象到查看页(vie_)
			}
		}else{																//修改页面加载数据时调用
			if(!StringUtils.isEmpty(mailSend.getId()) ){	//检查主键非空
				mailSend=mailService.schMailSendById(mailSend.getId());		//按主键查询数据
				response.getWriter().print(mailSend.getJson());			//输出Json对象到修改页(upd_)
			}else{															//添加页面加载数据时调用，可设置添加页面中一些属性的预设值。
				response.getWriter().print(new MailSend().getJson());		//输出Json对象到添加页(upd_)
			}
		}
	}
	
	/**
	 * 添加或修改的提交方法
	 * @param mailSend 数据表单Bean
	 * @param response 返回请求
	 * @throws IOException
	 */
	@RequestMapping(value="/updMailSend.action")
	public void updMailSend(MailSend mailSend,HttpServletResponse response) throws IOException{
		noCache(response);													//Ajax方法设定无缓存
		if(mailSend.getId()==null){									//检测主键为空则
			mailService.addMailSend(mailSend);						//添加数据
		}else{																//主键非空则
			mailService.updMailSend(mailSend);						//修改数据
		}
	}
	
	/**
	 * 删除方法
	 * @param ids 待删除数据的主键数组
	 * @param response 返回请求
	 */
	@RequestMapping(value="/delMailSend.action")
	public void delMailSend(@RequestParam(value = "ids[]") String[] ids,HttpServletResponse response){
		noCache(response);													//Ajax方法设定无缓存
		mailService.delMailSend(ids);									//删除数据
	}
	
	/**
	 * JSP页面跳转
	 * @param page jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value="/page.action")
	public String goPage(String page){										//放过本Ctrl的页面，不合法的跳转到error页。
		if( "index".equals(page) || "mailSend_sch".equals(page)
				|| "mailSend_vie".equals(page)||"mailSend_upd".equals(page)
				|| "mailManage".equals(page) ){
			return "/mail/"+page;
		}else{
			return "/error";
		}
	}
	
	/**
	 * 发送邮件
	 * @param mailSend
	 * @param request
	 * @param response
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value="/sendMail.action")
	public Map<String,Object> sendMail(MailSend mailSend ,@RequestBody Map<String,String> contentJson,HttpServletRequest request){
		Map<String ,Object> result = new HashMap<String,Object>();
		Person person = (Person)request.getSession(true).getAttribute(SysConstant.CURRENT_USER);
		try{
			if( contentJson != null ){
				String content = contentJson.get("contentJson");
				if(content != null && mailSend != null){
					mailSend.setContent(content);
				}
			}
			if(person != null && !StringUtils.isEmpty(person.getNumber()) ){
				if(mailSend != null && !StringUtils.isEmpty(mailSend.getReceiveNumShow())){
					mailSend.setSendNum(person.getNumber());// 设置发送人
					List<String> allStud = mailService.updSend(mailSend);
					CopyOnWriteArraySet<MailSocket> allSocket = MailSocket.getWebSocketSet();
					if(allStud != null && allSocket != null){
						for(String stu : allStud){
							for(MailSocket socket : allSocket){
								if(!StringUtils.isEmpty(stu) && socket != null
										&& stu.equals(socket.getNumber())){
									socket.sendMessage("【"+person.getName()+"】给您发来一封邮件，请查收!");
								}
							}
						}
					}
					result.put("success", true);
					result.put("msg", "发送成功！");
				}else{
					result.put("success", false);
					result.put("msg", "发送失败！<必要参数丢失>");
				}
			}else{
				result.put("success", false);
				result.put("msg", "发送失败！<当前登录状态异常，请重新登录>");
			}
		}catch(Exception e){
			e.printStackTrace();
			result.put("success", false);
			result.put("msg", "发送失败！<服务器异常>");
		}finally{
			return result;
		}
	}
	
	/**
	 * 查询已发送邮件列表
	 * @param mailSend
	 * @param request
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value="/schMailSendList.action")
	public Map<String,Object> schMailSendList(@RequestBody MailSend mailSend ,HttpServletRequest request){
		Map<String ,Object> result = new HashMap<String,Object>();
		Person person = (Person)request.getSession(true).getAttribute(SysConstant.CURRENT_USER);
		try{
			if( person != null && !StringUtils.isEmpty(person.getNumber()) ){
				mailSend.setSendNum(person.getNumber());
				mailSend.addOrderCol("update_time DESC");
				mailSend.assLike("title");
				List<MailSend> list = mailService.schMailSendList(mailSend);
				if(list != null && list.size() > 0){
					for( MailSend mail : list ){
						if(mail != null && !StringUtils.isEmpty( mail.getReceiveNumShow() ) ){
							String re = mail.getReceiveNumShow() == null? "":mail.getReceiveNumShow();// 接收人
							String[] reArg = re.split(",");// 接收人
							String receive = "";// 接收人
							for(String num : reArg){
								if(!StringUtils.isEmpty(num)){
									Stud stu = managerService.schStudByXh(num);
									if(stu != null){
										if(!StringUtils.isEmpty(receive)){
											receive = receive + "," + stu.getName();
										}else{
											receive = stu.getName();
										}
									}
								}
							}
							if(!StringUtils.isEmpty(receive)){
								mail.setReceiveNumShow(receive);
							}
							
							String du = mail.getDuplicateNumShow() == null? "":mail.getDuplicateNumShow();// 抄送人
							String[] duArg = du.split(",");// 抄送人
							String dupli = "";// 抄送人
							for(String num : duArg){// 抄送人
								if(!StringUtils.isEmpty(num)){
									Stud stu = managerService.schStudByXh(num);
									if(stu != null){
										if(!StringUtils.isEmpty(dupli)){
											dupli = dupli + "," + stu.getName();
										}else{
											dupli = stu.getName();
										}
									}
								}
							}
							if(!StringUtils.isEmpty(dupli)){
								mail.setDuplicateNumShow(dupli);
							}
						}
					}
				}
				result.put("success", true);
				result.put("msg", "查询成功");
				result.put("data", list);
			}else{
				result.put("success", false);
				result.put("msg", "当前登录状态异常，请重新登录");
			}
		}catch(Exception e){
			e.printStackTrace();
			result.put("success", false);
			result.put("msg", "查询失败！<服务器异常>");
		}finally{
			return result;
		}
	}
	
	/**
	 * 删除数据
	 * @param ids
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value="/deleteMailSends.action")
	public Map<String,Object> deleteMailSends(@RequestBody String[] ids){
		Map<String ,Object> result = new HashMap<String,Object>();
		try{
			if(ids != null){
				mailService.delMailSend(ids);
				result.put("success", true);
				result.put("msg", "删除成功");
				result.put("data", ids);
			}else{
				result.put("success", false);
				result.put("msg", "删除失败！<必要参数丢失>");
			}
		}catch(Exception e){
			e.printStackTrace();
			result.put("success", false);
			result.put("msg", "删除失败！<服务器异常>");
		}finally{
			return result;
		}
	}
	
	/**
	 * 删除数据
	 * @param ids
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value="/setTop.action")
	public Map<String,Object> setTop(@RequestBody String[] ids){
		Map<String ,Object> result = new HashMap<String,Object>();
		try{
			if(ids != null){
				MailSend send = mailService.schMailSendById(ids[0]);
				send.setUpdateTime(new Date());
				mailService.updMailSend(send);
				result.put("success", true);
				result.put("msg", "置顶成功");
				result.put("data", ids);
			}else{
				result.put("success", false);
				result.put("msg", "置顶失败！<必要参数丢失>");
			}
		}catch(Exception e){
			e.printStackTrace();
			result.put("success", false);
			result.put("msg", "置顶失败！<服务器异常>");
		}finally{
			return result;
		}
	}
	
	/**
	 * 
	 * @param mailSend
	 * @param request
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value="/saveDraft.action")
	public Map<String,Object> saveDraft(MailSend mailSend ,HttpServletRequest request){
		Map<String ,Object> result = new HashMap<String,Object>();
		Person person = (Person)request.getSession(true).getAttribute(SysConstant.CURRENT_USER);
		try{
			if( mailSend != null && person != null && !StringUtils.isEmpty(person.getNumber())){
				MailSend entity = new MailSend();
				entity.setTitle(mailSend.getTitle());// 邮件标题
				entity.setContent(mailSend.getContent());// 邮件内容
				entity.setSendNum(person.getNumber());// 发送人
				entity.setIsSend(MailConstants.UN_SEND);// 未发送
				Date now = new Date();
				entity.setCreateTime(now);// 创建时间
				entity.setUpdateTime(now);// 更新时间（置顶用）
				entity.setSendTime(now);// 发送时间
				entity.setId(UuidUtil.get32UUID());
				mailService.addMailSend(entity);
				result.put("success", true);
				result.put("msg", "保存草稿成功");
				result.put("data", entity);
			}else{
				result.put("success", false);
				result.put("msg", "保存草稿失败！<缺少必要参数>");
			}
		}catch(Exception e){
			e.printStackTrace();
			result.put("success", false);
			result.put("msg", "保存草稿失败！<服务器异常>");
		}finally{
			return result;
		}
	}
}
