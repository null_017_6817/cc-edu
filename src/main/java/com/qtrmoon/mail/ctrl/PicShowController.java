package com.qtrmoon.mail.ctrl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.qtrmoon.book.util.FilePath;
import com.qtrmoon.common.BaseController;
import com.qtrmoon.manager.pojo.Stud;
import com.qtrmoon.manager.serdao.IManagerService;
import com.qtrmoon.sysmanage.SysConstant;
import com.qtrmoon.sysmanage.pojo.User;
import com.qtrmoon.sysmanage.serdao.ISysmanageService;
import com.qtrmoon.util.FileUpload;
import com.qtrmoon.util.PathUtil;

@Controller
@RequestMapping("/picshow")
public class PicShowController extends BaseController{

	@Autowired
	private IManagerService managerService; // 声明模块Service实例
	
	@Autowired
	private ISysmanageService sysmanageService;//注入service
	
	/**
	 * JSP页面跳转
	 * @param page
	 * jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value = "/page.action")
	public String goPage(String page) { // 放过本Ctrl的页面，不合法的跳转到error页。
		if ( "index".equals(page) ){// 判断页面) {// 判断页面
			return "/common/pic_show/" + page;// 跳转页面
		} else {
			return "/error";// 错误页面
		}
	}
	
	/**
	 * 上传头像
	 * @param file
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value="/uploadByMt.action")
	public Map<String ,Object> uploadByMt(@RequestParam("file") MultipartFile file , 
			HttpServletRequest request,HttpServletResponse response ) {
		Map<String,Object> map = new HashMap<String,Object>();
		try {
			
			String number = request.getParameter("number");
			String filename = file.getOriginalFilename();
			String projectName = request.getContextPath();// 获取参数
			String path = FilePath.getFilePath("photoUrl");// 拼接参数
			int rand = (int) (Math.random() * 10) + 10;// 两位随机数
			Date now = new Date();// 当前时间
			String tzm = "";// 定义常量
			if ((filename != null) && (filename.length() > 0)) { // 判断
				int dot = filename.lastIndexOf('.');   // 截取
				if ((dot >-1) && (dot < (filename.length() - 1))) {  // 判断 
					tzm = filename.substring(dot + 1);   // 截取
				}
			}
			
			
			//String showpath = projectName+ "/picshow/appendUrl.action?filename=" + fullName;// 拼接参数
			if(!StringUtils.isEmpty(number)){
				User user =sysmanageService.schUserByNumber(number);
				//Stud stu = managerService.schStudByXh(number);
				//在文件系统下删除旧文件
				if( user != null ){
					String oldFileUrl = user.getPhotoUrl();
					if(!StringUtils.isEmpty(oldFileUrl)){
						File oldFile = new File(path+oldFileUrl);
						if(oldFile.exists()){
							oldFile.delete();
						}
					}
					
					//更新头像旧路径
					String fullName = user.getNumber()+ "." + tzm;// 拼接参数
					user.setPhotoUrl(fullName);
					//managerService.updStudData(stu);
					sysmanageService.updUser(user);
					//在文件系统下创建新文件
					path = PathUtil.appendBackSlant(path)+user.getCnumber();//拼接班级信息
					File targetFile = new File(path, fullName);// 文件对象
					if (!targetFile.exists()) {// 判断
						targetFile.mkdirs();// 创建文件夹
					}
					file.transferTo(targetFile);//上传文件
					
					//在服务器下拷贝新文件
					String webPath=request.getSession().getServletContext().getRealPath("")+File.separator+"imgs"+File.separator+"photos"+File.separator+user.getCnumber()+File.separator+fullName;
					System.out.println(webPath);
					File oldWebFile = new File(webPath);
					if(oldWebFile.exists()){
						oldWebFile.delete();
					}
					FileUpload.copyFile(targetFile.getPath(), webPath);
					
					//更新当前用户数据
					User us = (User)request.getSession(true).getAttribute(SysConstant.CURRENT_USER);//获取当前用户
					us.setPhotoUrl(fullName);//增加图片信息
					request.getSession(true).setAttribute(SysConstant.CURRENT_USER,us);//设置当前用户
					
					
					map.put("success", true);// 设置属性
					//map.put("fileUrl", showpath);
					
					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();// 打印异常
			map.put("success", false);// 设置属性
		}finally{
			return map;
		}
	}
	
	/**
	 * 回显头像调用
	 * @param filename
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/appendUrl.action")
	public void appendUrl(String filename, HttpServletRequest request,
			HttpServletResponse response) {
		if (filename != null) {// 判断是否为空
			String path = FilePath.getFilePath("photoUrl") + filename;// 获取路径
			try {
				File file = new File(path);// 文件对象
				if (file.exists()) {// 判断
					FileInputStream in = new FileInputStream(path);// 文件对象输入流
					ServletOutputStream out = response.getOutputStream();// 输出流
					IOUtils.copy(in, out);// 以流的形式展示到前台
					IOUtils.closeQuietly(in);// 关闭流
					IOUtils.closeQuietly(out);// 关闭流
				} else {
					logger.info("文件不存在！不产生数据流输出到前台页面。缺失文件及路径为：" + path);// log
				}
			} catch (Exception e) {
				e.printStackTrace();// 打印异常
			}
		}
	}
}

