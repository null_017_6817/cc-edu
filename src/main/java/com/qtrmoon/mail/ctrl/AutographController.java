package com.qtrmoon.mail.ctrl;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qtrmoon.common.BaseController;
import com.qtrmoon.mail.pojo.Autograph;
import com.qtrmoon.mail.serdao.IMailService;
import com.qtrmoon.manager.pojo.Person;
import com.qtrmoon.sysmanage.SysConstant;

/**
 * 个性签名表控制器
 */
@Controller
@RequestMapping("/mail/autograph")
public class AutographController extends BaseController{
	@Autowired
	private IMailService mailService;								//声明模块Service实例

	/**
	 * 查询方法，自带分页设置。
	 * @param autograph 承载查询条件的Bean
	 * @param request 可用来获取非Bean内的参数
	 * @param response 返回请求,输出Json
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/schAutograph.action")
	public void schAutograph(Autograph autograph,HttpServletRequest request,HttpServletResponse response) throws IOException{
		noCache(response);
		autograph.setCondition("");											//防sql注入
		autograph.assLike();

		List<Autograph> list=mailService.schAutograph(autograph);		//调用Service查询数据
		/*将数据Bean的集合转换为Json集合*/
		JSONArray datas=new JSONArray();									//构造业务数据的JSON集合
		JSONObject obj;														//声明用于构造Bean的Json对象
		for(Autograph u:list){												//循环查询的数据集构造Json数据集
			obj=u.getJsonInDict();											//将Bean转换为Json对象
			obj.put("_oper",u.getId());								//为"操作列"设定主键值
			datas.add(obj);													//添加Json对象到Json集合
		}
		/*构造分页数据的JSON对象，并包装到返回页面的Json中*/
		JSONObject res=new JSONObject();									//声明页面返回Json
		res.put("total",autograph.getDatasize());							//设置数据总记录数
		res.put("rows", datas);												//设置数据集
		response.getWriter().print(res.toJSONString());						//输出到页面
	}
	
	/**
	 * 预添加or预修改or查看方法
	 * @param autograph 预添加时可传入一些初始值；预修改和查看时用来承载主键值
	 * @param toL 挂字典的字段是否需要转为字典Label值。true则进行转换
	 * @param response 返回请求,输出Json
	 * @throws IOException
	 */
	@RequestMapping(value="/vieAutograph.action")
	public void vieAutograph(Autograph autograph,Boolean toL,HttpServletResponse response) throws IOException{
		noCache(response);													//Ajax方法设定无缓存
		if(toL!=null&&toL){													//查看页面加载数据时调用，返回的对象已进行字典转换
			if(autograph.getId()!=null&&autograph.getId()!=0){					//检测主键非空
				autograph=mailService.schAutographById(autograph.getId());			//按主键查询数据
				response.getWriter().print(autograph.getJsonInDict());						//输出经过字典转换Json对象到查看页(vie_)
			}
		}else{																//修改页面加载数据时调用
			if(autograph.getId()!=null&&autograph.getId()!=0){	//检查主键非空
				autograph=mailService.schAutographById(autograph.getId());		//按主键查询数据
				response.getWriter().print(autograph.getJson());			//输出Json对象到修改页(upd_)
			}else{															//添加页面加载数据时调用，可设置添加页面中一些属性的预设值。
				response.getWriter().print(new Autograph().getJson());		//输出Json对象到添加页(upd_)
			}
		}
	}
	
	/**
	 * 添加或修改的提交方法
	 * @param autograph 数据表单Bean
	 * @param response 返回请求
	 * @throws IOException
	 */
	@RequestMapping(value="/updAutograph.action")
	public void updAutograph(Autograph autograph,HttpServletResponse response) throws IOException{
		noCache(response);													//Ajax方法设定无缓存
		if(autograph.getId()==null){									//检测主键为空则
			mailService.addAutograph(autograph);						//添加数据
		}else{																//主键非空则
			mailService.updAutograph(autograph);						//修改数据
		}
	}
	
	/**
	 * 删除方法
	 * @param ids 待删除数据的主键数组
	 * @param response 返回请求
	 */
	@RequestMapping(value="/delAutograph.action")
	public void delAutograph(@RequestParam(value = "ids[]") Integer[] ids,HttpServletResponse response){
		noCache(response);													//Ajax方法设定无缓存
		mailService.delAutograph(ids);									//删除数据
	}
	
	/**
	 * JSP页面跳转
	 * @param page jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value="/page.action")
	public String goPage(String page){										//放过本Ctrl的页面，不合法的跳转到error页。
		if("index".equals(page)||"autograph_sch".equals(page)||"autograph_vie".equals(page)||"autograph_upd".equals(page)){
			return "/mail/"+page;
		}else{
			return "/error";
		}
	}
	
	/**
	 * 获取当前用户的个性签名
	 * @param request
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value="/getCurentAutograph.action")
	public Map<String,Object> getCurentAutograph(HttpServletRequest request){
		Map<String,Object> result = new HashMap<String,Object>();
		Person person = (Person)request.getSession(true).getAttribute(SysConstant.CURRENT_USER);
		try{
			if(person != null){
				Autograph autograph = new Autograph();
				autograph.setNumber(person.getNumber());
				List<Autograph> list = mailService.schAutograph(autograph);
				if(list != null && list.size() > 0){
					result.put("data", list.get(0));
				}
				result.put("success", true);
				result.put("msg", "操作成功");
			}else{
				result.put("success", false);
				result.put("msg", "登录超时");
			}
		}catch(Exception e){
			e.printStackTrace();
			result.put("success", false);
			result.put("msg", "服务器异常");
		}finally{
			return result;
		}
	}
	
	/**
	 * 保存当前用户的个性签名
	 * @param request
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value="/saveAutograph.action")
	public Map<String,Object> saveAutograph(Autograph autograph, HttpServletRequest request){
		Map<String,Object> result = new HashMap<String,Object>();
		Person person = (Person)request.getSession(true).getAttribute(SysConstant.CURRENT_USER);
		try{
			if(person != null && autograph != null){
				Autograph param = new Autograph();
				param.setNumber(person.getNumber());
				List<Autograph> entitys = mailService.schAutograph(param);
				if(entitys != null && entitys.size() > 0){
					Autograph entity = entitys.get(0);
					entity.setContent(autograph.getContent());
					mailService.updAutograph(entity);
					result.put("data", entity);
				}else{
					autograph.setNumber(person.getNumber());
					mailService.addAutograph(autograph);
					result.put("data", autograph);
				}
				result.put("success", true);
				result.put("msg", "操作成功");
			}else{
				result.put("success", false);
				result.put("msg", "缺少必要参数");
			}
		}catch(Exception e){
			e.printStackTrace();
			result.put("success", false);
			result.put("msg", "服务器异常");
		}finally{
			return result;
		}
	}
	
}
