package com.qtrmoon.mail.ctrl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.qtrmoon.book.util.FilePath;
import com.qtrmoon.common.BaseController;
import com.qtrmoon.util.UuidUtil;

/**
 * ueditor 文件上传控制器
 */
@Controller
public class UeditorController extends BaseController{

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/uploaditemimage.action")
	public void uploadItemImage(HttpServletRequest request,HttpServletResponse response) throws IllegalStateException, IOException {  
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;  
			MultipartFile file = multipartRequest.getFile("upfile");  
			String path = FilePath.getFilePath("ueditorImg");// 拼接
			String title = file.getOriginalFilename();
			String pattern = "";//文件格式
			if ((title != null) && (title.length() > 0)) { // 判断
				int dot = title.lastIndexOf('.');   // 截取
				if ((dot >-1) && (dot < (title.length() - 1))) {  // 判断 
					pattern = "."+title.substring(dot + 1);   // 截取
				}
			}
			String ori = UuidUtil.get32UUID();
			ori = ori+pattern;
			File targetFile = new File(path, ori);// 文件对象
			if (!targetFile.exists()) {// 判断
				targetFile.mkdirs();// 创建文件夹
			}
			file.transferTo(targetFile);//上传文件
			//这里获取到文件 处理上传后返回路径  
			//String projectName = request.getContextPath();
			JSONObject res = new JSONObject(); // 声明页面返回Json
			res.put("state", "SUCCESS"); // 成功状态
			res.put("url", "/getUeditorImg.action?imgName="+ori); // 访问路径
			res.put("title", title); // title
			res.put("original", ori); // origin
			response.getWriter().print(res.toJSONString()); // 输出到页面
			
	}
	
	/**
	 * html文件夹 
	 * @param filename
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/getUeditorImg.action")
	public void appendHtmlUrl(String imgName,HttpServletRequest request, HttpServletResponse response) {
		String path = FilePath.getFilePath("ueditorImg") + imgName;// 拼接参数
		try {
			File file = new File(path);// 文件对象
			if (file.exists()) {// 判断
				FileInputStream in = new FileInputStream(path);// 文件对象输入流
				ServletOutputStream out = response.getOutputStream();// 输出流
				IOUtils.copy(in, out);// 以流的形式展示到前台
				IOUtils.closeQuietly(in);// 关闭流
				IOUtils.closeQuietly(out);// 关闭流
			} else {
				logger.info("文件不存在！不产生数据流输出到前台页面。缺失文件及路径为：" + path);// log
			}
		} catch (Exception e) {
			e.printStackTrace();//异常信息
		}
	}
	
}
