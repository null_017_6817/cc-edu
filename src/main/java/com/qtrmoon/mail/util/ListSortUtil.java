package com.qtrmoon.mail.util;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;



/**
 * 自定义排序
 */
public class ListSortUtil{

	public static List<Map<String,Object>> sort(List<Map<String,Object>> dataList){
		
		Collections.sort(dataList, new Comparator<Map<String,Object>>() {

			@Override
			public int compare(Map<String, Object> o1, Map<String, Object> o2) {
				if(o1 != null && o2 != null){
					Date timestr1 = o1.get("creatTime") == null ? null : (Date)o1.get("creatTime");
					Date timestr2 = o2.get("creatTime") == null ? null : (Date)o2.get("creatTime");
					if ( timestr1 != null && timestr2 != null ){
						if ( timestr1.after(timestr2) ){
							return -1;
						} else if (timestr1.before(timestr2)){
							return 1;
						} else {
							return 0;
						}
					}else{
						if( timestr1 == null && timestr2 == null){
							return 0;
						}
						if( timestr1 != null && timestr2 == null ){
							return -1;
						}
						if( timestr1 == null && timestr2 != null ){
							return 1;
						}
					}
				}else{
					return 0;
				}
				return 0;
			}
		});
		
		return dataList;
	}
}
