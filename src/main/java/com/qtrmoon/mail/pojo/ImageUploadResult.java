package com.qtrmoon.mail.pojo;
import com.qtrmoon.common.PageForm;

/** 附件表（邮件用）实体类 */
public class ImageUploadResult extends PageForm{
	public String state;  
	public String url;  
	public String title;  
	public String original;
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getOriginal() {
		return original;
	}
	public void setOriginal(String original) {
		this.original = original;
	}
}
