package com.qtrmoon.mail.pojo;
import java.util.Date;

import org.json.simple.JSONObject;

import com.qtrmoon.common.PageForm;
import com.qtrmoon.mail.constants.MailConstants;
import com.qtrmoon.toolkit.DateTransfer;

/** 邮件接收表实体类 */
public class MailSend extends PageForm{
	//Fields
	
	private String id;// 
	private String sendNum;// 发送人学号
	private String sendName;// 发送人姓名
	private String receiveNum;// 接收人学号
	private String receiveName;// 接收人姓名
	private String receiveNumShow; // 接收人学号显示用全部学号
	private String duplicateNumShow; // 抄送人学号显示用全部学号
	private String isDelete;// 删除标识（1-已删除，0-未删除）
	private String isSend;// 发送状态（1：已发送，0：未发送-草稿 ）
	private String isRead = MailConstants.IS_READ;// 已读状态（1：已读，0：未读 ）
	private String title;// 邮件标题
	private String content;// 邮件内容
	private Date sendTime;// 邮件发送时间
	private String sendTimeBeg,sendTimeEnd;//时间条件的跨度查询属性
	private Date createTime;// 创建时间
	private String createTimeBeg,createTimeEnd;//时间条件的跨度查询属性
	private Date updateTime;// 更新时间（置顶用）
	private String updateTimeBeg,updateTimeEnd;//时间条件的跨度查询属性
	private String imgId;// 图片id
	private String fileId;// 附件id
	
	// 班级（页面检索用）
	private String cnumber;
	
	//Constructors
	/** default constructor */
	public MailSend() {
	
	}	
	//getter and setter
	/** 获取 */
	public String getId() {
		return this.id;
	}
	/** 设置 */
	public void setId(String id) {
		this.id = id;
	}
	/** 获取发送人学号 */
	public String getSendNum() {
		return this.sendNum;
	}
	/** 设置发送人学号 */
	public void setSendNum(String sendNum) {
		this.sendNum = sendNum;
	}
	/** 获取接收人学号 */
	public String getReceiveNum() {
		return this.receiveNum;
	}
	/** 设置接收人学号 */
	public void setReceiveNum(String receiveNum) {
		this.receiveNum = receiveNum;
	}
	/** 获取删除标识（1-已删除，0-未删除） */
	public String getIsDelete() {
		return this.isDelete;
	}
	/** 设置删除标识（1-已删除，0-未删除） */
	public void setIsDelete(String isDelete) {
		this.isDelete = isDelete;
	}
	/** 获取发送状态（1：已发送，0：未发送-草稿 ） */
	public String getIsSend() {
		return this.isSend;
	}
	/** 设置发送状态（1：已发送，0：未发送-草稿 ） */
	public void setIsSend(String isSend) {
		this.isSend = isSend;
	}
	/** 获取邮件标题 */
	public String getTitle() {
		return this.title;
	}
	/** 设置邮件标题 */
	public void setTitle(String title) {
		this.title = title;
	}
	/** 获取邮件内容 */
	public String getContent() {
		return this.content;
	}
	/** 设置邮件内容 */
	public void setContent(String content) {
		this.content = content;
	}
	/** 获取邮件发送时间 */
	public Date getSendTime() {
		return this.sendTime;
	}
	/** 设置邮件发送时间 */
	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}
	/** 设定[邮件发送时间]时间转载字段值，页面提交时由springmvc自动装载 */
	public void setSendTimestr(String tm) {
		this.sendTime=_getTime(tm);
	}
	/** 获取[邮件发送时间]时间起始条件 */
	public String getSendTimeBeg() {
		return sendTimeBeg;
	}
	/** 获取[邮件发送时间]时间起始条件(Dao/Mapper查询时调用) */
	public Date getSendTimeBegDate() {
		return _getBegDate(sendTimeBeg);
	}
	/** 设置[邮件发送时间]时间起始条件(表单提交时自动装载) */
	public void setSendTimeBeg(String sendTimeBeg) {
		this.sendTimeBeg = sendTimeBeg;
	}
	
	/** 获取[邮件发送时间]时间结束条件 */
	public String getSendTimeEnd() {
		return sendTimeEnd;
	}
	/** 获取[邮件发送时间]时间结束条件(Dao/Mapper查询时调用) */
	public Date getSendTimeEndDate() {
		return _getEndDate(sendTimeEnd);
	}
	/** 设置[邮件发送时间]时间结束条件(表单提交时自动装载) */
	public void setSendTimeEnd(String sendTimeEnd) {
		this.sendTimeEnd = sendTimeEnd;
	}
	/** 获取创建时间 */
	public Date getCreateTime() {
		return this.createTime;
	}
	/** 设置创建时间 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/** 设定[创建时间]时间转载字段值，页面提交时由springmvc自动装载 */
	public void setCreateTimestr(String tm) {
		this.createTime=_getTime(tm);
	}
	/** 获取[创建时间]时间起始条件 */
	public String getCreateTimeBeg() {
		return createTimeBeg;
	}
	/** 获取[创建时间]时间起始条件(Dao/Mapper查询时调用) */
	public Date getCreateTimeBegDate() {
		return _getBegDate(createTimeBeg);
	}
	/** 设置[创建时间]时间起始条件(表单提交时自动装载) */
	public void setCreateTimeBeg(String createTimeBeg) {
		this.createTimeBeg = createTimeBeg;
	}
	
	/** 获取[创建时间]时间结束条件 */
	public String getCreateTimeEnd() {
		return createTimeEnd;
	}
	/** 获取[创建时间]时间结束条件(Dao/Mapper查询时调用) */
	public Date getCreateTimeEndDate() {
		return _getEndDate(createTimeEnd);
	}
	/** 设置[创建时间]时间结束条件(表单提交时自动装载) */
	public void setCreateTimeEnd(String createTimeEnd) {
		this.createTimeEnd = createTimeEnd;
	}
	/** 获取更新时间（置顶用） */
	public Date getUpdateTime() {
		return this.updateTime;
	}
	/** 设置更新时间（置顶用） */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/** 设定[更新时间（置顶用）]时间转载字段值，页面提交时由springmvc自动装载 */
	public void setUpdateTimestr(String tm) {
		this.updateTime=_getTime(tm);
	}
	/** 获取[更新时间（置顶用）]时间起始条件 */
	public String getUpdateTimeBeg() {
		return updateTimeBeg;
	}
	/** 获取[更新时间（置顶用）]时间起始条件(Dao/Mapper查询时调用) */
	public Date getUpdateTimeBegDate() {
		return _getBegDate(updateTimeBeg);
	}
	/** 设置[更新时间（置顶用）]时间起始条件(表单提交时自动装载) */
	public void setUpdateTimeBeg(String updateTimeBeg) {
		this.updateTimeBeg = updateTimeBeg;
	}
	
	/** 获取[更新时间（置顶用）]时间结束条件 */
	public String getUpdateTimeEnd() {
		return updateTimeEnd;
	}
	/** 获取[更新时间（置顶用）]时间结束条件(Dao/Mapper查询时调用) */
	public Date getUpdateTimeEndDate() {
		return _getEndDate(updateTimeEnd);
	}
	/** 设置[更新时间（置顶用）]时间结束条件(表单提交时自动装载) */
	public void setUpdateTimeEnd(String updateTimeEnd) {
		this.updateTimeEnd = updateTimeEnd;
	}
	
	public String getReceiveNumShow() {
		return receiveNumShow;
	}
	public void setReceiveNumShow(String receiveNumShow) {
		this.receiveNumShow = receiveNumShow;
	}
	public String getDuplicateNumShow() {
		return duplicateNumShow;
	}
	public void setDuplicateNumShow(String duplicateNumShow) {
		this.duplicateNumShow = duplicateNumShow;
	}
	
	public String getIsRead() {
		return isRead;
	}
	public void setIsRead(String isRead) {
		this.isRead = isRead;
	}
	
	public String getSendName() {
		return sendName;
	}
	public void setSendName(String sendName) {
		this.sendName = sendName;
	}
	public String getReceiveName() {
		return receiveName;
	}
	public void setReceiveName(String receiveName) {
		this.receiveName = receiveName;
	}
	
	public String getImgId() {
		return imgId;
	}
	public void setImgId(String imgId) {
		this.imgId = imgId;
	}
	public String getFileId() {
		return fileId;
	}
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}
	
	public String getCnumber() {
		return cnumber;
	}
	public void setCnumber(String cnumber) {
		this.cnumber = cnumber;
	}
	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("sendNum", sendNum);
		obj.put("receiveNum", receiveNum);
		obj.put("receiveNumShow", receiveNumShow);
		obj.put("duplicateNumShow", duplicateNumShow);
		obj.put("isDelete", isDelete);
		obj.put("isSend", isSend);
		obj.put("title", title);
		obj.put("content", content);
		//时间字段处理。sendTime查询列表页使用，sendTimestr修改页使用
		obj.put("sendTime", DateTransfer.toString(sendTime,"yyyy/MM/dd"));
		obj.put("sendTimestr", DateTransfer.toString(sendTime,"yyyy/MM/dd"));
		//时间字段处理。createTime查询列表页使用，createTimestr修改页使用
		obj.put("createTime", DateTransfer.toString(createTime,"yyyy/MM/dd"));
		obj.put("createTimestr", DateTransfer.toString(createTime,"yyyy/MM/dd"));
		//时间字段处理。updateTime查询列表页使用，updateTimestr修改页使用
		obj.put("updateTime", DateTransfer.toString(updateTime,"yyyy/MM/dd"));
		obj.put("updateTimestr", DateTransfer.toString(updateTime,"yyyy/MM/dd"));
		return obj;
	}
	
	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("sendNum", sendNum);
		obj.put("receiveNum", receiveNum);
		obj.put("receiveNumShow", receiveNumShow);
		obj.put("duplicateNumShow", duplicateNumShow);
		obj.put("isDelete", isDelete);
		obj.put("isSend", isSend);
		obj.put("title", title);
		obj.put("content", content);
		//时间字段处理。sendTime查询列表页使用，sendTimestr修改页使用
		obj.put("sendTime", DateTransfer.toString(sendTime,"yyyy/MM/dd"));
		obj.put("sendTimestr", DateTransfer.toString(sendTime,"yyyy/MM/dd"));
		//时间字段处理。createTime查询列表页使用，createTimestr修改页使用
		obj.put("createTime", DateTransfer.toString(createTime,"yyyy/MM/dd"));
		obj.put("createTimestr", DateTransfer.toString(createTime,"yyyy/MM/dd"));
		//时间字段处理。updateTime查询列表页使用，updateTimestr修改页使用
		obj.put("updateTime", DateTransfer.toString(updateTime,"yyyy/MM/dd"));
		obj.put("updateTimestr", DateTransfer.toString(updateTime,"yyyy/MM/dd"));
		return obj;
	}
}
