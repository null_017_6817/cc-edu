package com.qtrmoon.mail.pojo;
import java.util.Date;
import java.util.Set;
import java.util.HashSet;
import org.json.simple.JSONObject;
import com.qtrmoon.common.PageForm;
import com.qtrmoon.toolkit.DateTransfer;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.dictionary.bsd.DictBean;

/** 个性签名表实体类 */
public class Autograph extends PageForm{
	//Fields
	
	private Integer id;// 
	private String number;// 学号
	private String content;// 
	//Constructors
	/** default constructor */
	public Autograph() {
	
	}	
	//getter and setter
	/** 获取 */
	public Integer getId() {
		return this.id;
	}
	/** 设置 */
	public void setId(Integer id) {
		this.id = id;
	}
	/** 获取学号 */
	public String getNumber() {
		return this.number;
	}
	/** 设置学号 */
	public void setNumber(String number) {
		this.number = number;
	}
	/** 获取 */
	public String getContent() {
		return this.content;
	}
	/** 设置 */
	public void setContent(String content) {
		this.content = content;
	}
	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("number", number);
		obj.put("content", content);
		return obj;
	}
	
	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("number", number);
		obj.put("content", content);
		return obj;
	}
}
