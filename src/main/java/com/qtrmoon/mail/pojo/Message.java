package com.qtrmoon.mail.pojo;
import java.util.Date;
import java.util.Set;
import java.util.HashSet;

import org.json.simple.JSONObject;

import com.qtrmoon.common.PageForm;
import com.qtrmoon.mail.constants.MailConstants;
import com.qtrmoon.mail.constants.ReadEnum;
import com.qtrmoon.toolkit.DateTransfer;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.dictionary.bsd.DictBean;

/** 短信实训表实体类 */
public class Message extends PageForm{
	//Fields
	
	private String id;// 
	private String sendNum;// 发送人学号
	private String receiveNum;// 接收人学号
	private Date createTime;// 创建时间
	private String createTimeBeg,createTimeEnd;//时间条件的跨度查询属性
	private String content;// 信息内容
	private String isRead;// 已读标识
	private String[] receiveNums;// 接收人学号（批量发送用）
	
	private String sendName; //发送人名称(前台展示用)
	private String receiveName; //发送人名称(前台展示用)
	private String cnumber;//班级
	
	//Constructors
	/** default constructor */
	public Message() {
	
	}	
	//getter and setter
	/** 获取 */
	public String getId() {
		return this.id;
	}
	/** 设置 */
	public void setId(String id) {
		this.id = id;
	}
	/** 获取发送人学号 */
	public String getSendNum() {
		return this.sendNum;
	}
	/** 设置发送人学号 */
	public void setSendNum(String sendNum) {
		this.sendNum = sendNum;
	}
	/** 获取接收人学号 */
	public String getReceiveNum() {
		return this.receiveNum;
	}
	/** 设置接收人学号 */
	public void setReceiveNum(String receiveNum) {
		this.receiveNum = receiveNum;
	}
	/** 获取创建时间 */
	public Date getCreateTime() {
		return this.createTime;
	}
	/** 设置创建时间 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/** 设定[创建时间]时间转载字段值，页面提交时由springmvc自动装载 */
	public void setCreateTimestr(String tm) {
		this.createTime=_getTime(tm);
	}
	/** 获取[创建时间]时间起始条件 */
	public String getCreateTimeBeg() {
		return createTimeBeg;
	}
	/** 获取[创建时间]时间起始条件(Dao/Mapper查询时调用) */
	public Date getCreateTimeBegDate() {
		return _getBegDate(createTimeBeg);
	}
	/** 设置[创建时间]时间起始条件(表单提交时自动装载) */
	public void setCreateTimeBeg(String createTimeBeg) {
		this.createTimeBeg = createTimeBeg;
	}
	
	/** 获取[创建时间]时间结束条件 */
	public String getCreateTimeEnd() {
		return createTimeEnd;
	}
	/** 获取[创建时间]时间结束条件(Dao/Mapper查询时调用) */
	public Date getCreateTimeEndDate() {
		return _getEndDate(createTimeEnd);
	}
	/** 设置[创建时间]时间结束条件(表单提交时自动装载) */
	public void setCreateTimeEnd(String createTimeEnd) {
		this.createTimeEnd = createTimeEnd;
	}
	/** 获取信息内容 */
	public String getContent() {
		return this.content;
	}
	/** 设置信息内容 */
	public void setContent(String content) {
		this.content = content;
	}
	/** 获取已读标识 */
	public String getIsRead() {
		return this.isRead;
	}
	/** 设置已读标识 */
	public void setIsRead(String isRead) {
		this.isRead = isRead;
	}
	public String[] getReceiveNums() {
		return receiveNums;
	}
	public void setReceiveNums(String[] receiveNums) {
		this.receiveNums = receiveNums;
	}
	
	public String getSendName() {
		return sendName;
	}
	public void setSendName(String sendName) {
		this.sendName = sendName;
	}
	
	public String getReceiveName() {
		return receiveName;
	}
	public void setReceiveName(String receiveName) {
		this.receiveName = receiveName;
	}
	
	public String getCnumber() {
		return cnumber;
	}
	public void setCnumber(String cnumber) {
		this.cnumber = cnumber;
	}
	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("sendNum", sendNum);
		obj.put("receiveNum", receiveNum);
		//时间字段处理。createTime查询列表页使用，createTimestr修改页使用
		obj.put("createTime", DateTransfer.toString(createTime,"yyyy/MM/dd"));
		obj.put("createTimestr", DateTransfer.toString(createTime,"yyyy/MM/dd"));
		obj.put("content", content);
		obj.put("isRead", isRead);
		if(isRead != null){
			obj.put("isReadShow", ReadEnum.getLabel(isRead));
		}
		obj.put("sendName", sendName);
		obj.put("receiveName", receiveName);
		return obj;
	}
	
	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("sendNum", sendNum);
		obj.put("receiveNum", receiveNum);
		//时间字段处理。createTime查询列表页使用，createTimestr修改页使用
		obj.put("createTime", DateTransfer.toString(createTime,"yyyy/MM/dd"));
		obj.put("createTimestr", DateTransfer.toString(createTime,"yyyy/MM/dd"));
		obj.put("content", content);
		obj.put("isRead", isRead);
		if(isRead != null){
			obj.put("isReadShow", ReadEnum.getLabel(isRead));
		}
		obj.put("sendName", sendName);
		obj.put("receiveName", receiveName);
		return obj;
	}
}
