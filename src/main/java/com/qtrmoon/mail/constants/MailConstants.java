package com.qtrmoon.mail.constants;

/**
 * 邮件常量
 */
public interface MailConstants {

	public String UN_READ = "0";// 未读
	public String IS_READ = "1";// 已读
	
	public String UN_DELETE = "0";// 未删除
	public String IS_DELETE = "1";// 已删除
	
	public String UN_SEND = "0";// 未发送
	public String IS_SEND = "1";// 已发送
}
