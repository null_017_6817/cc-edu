package com.qtrmoon.mail.constants;

import org.springframework.util.StringUtils;

/**
 * 已读标识的枚举
 */
public enum ReadEnum {

	UN_READ (MailConstants.UN_READ,"未读"),// 未读
	IS_READ (MailConstants.IS_READ,"已读");// 已读
	
	private String code;
	private String label;
	
	private ReadEnum(String code ,String label){
		this.code = code;
		this.label = label;
	}

	//根据code获取label
	public static String getLabel(String code){
		if( !StringUtils.isEmpty(code) ){
			for( ReadEnum r : ReadEnum.values()){
				if( code.equals(r.code) ){
					return r.label;
				}
			}
		}
		return "";
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
}
