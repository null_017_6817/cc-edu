package com.qtrmoon.util;

import java.security.MessageDigest;

import org.apache.log4j.Logger;
/**
 * 
 * MD5加密类
 */
public class MD5 {
	//日志类
	protected static Logger logger = Logger.getLogger(MD5.class);
	/**
	 * 生成md5
	 * @param str
	 * @return
	 */
	public static String md5(String str) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");//获取md5实例
			md.update(str.getBytes());//使用指定的 byte 数组更新摘要
			byte b[] = md.digest();//通过执行诸如填充之类的最终操作完成哈希计算。在调用此方法之后，摘要被重置

			int i;

			StringBuffer buf = new StringBuffer("");
			for (int offset = 0; offset < b.length; offset++) {
				i = b[offset];//获取字节
				if (i < 0)
					i += 256;//添加字节
				if (i < 16)
					buf.append("0");
				buf.append(Integer.toHexString(i));//追加随机16进制数
			}
			str = buf.toString();//返回字符串
		} catch (Exception e) {
			e.printStackTrace();//打印异常

		}
		return str;//返回字符串
	}
}
