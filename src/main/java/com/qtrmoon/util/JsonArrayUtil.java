package com.qtrmoon.util;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.json.JSONObject;

import net.sf.json.JSONArray;
/**
 * 
 * json转换工具类
 */
public class JsonArrayUtil {
	/**
	 * json字符串的
	 * @param param
	 * @return
	 */
	public static String jsonArray(String param){
		JSONArray jsonArray = JSONArray.fromObject(param);//字符串转对象
		@SuppressWarnings({ "unchecked", "rawtypes" })
		List<Map<String,Object>> mapListJson = (List)jsonArray;//对象强转list
//		List<LinkedHashMap<String, String>> list = new ArrayList<LinkedHashMap<String, String>>();
		String value = "";
		for (int i = 1; i < mapListJson.size(); i++) {  
			Map<String,Object> obj= mapListJson.get(i);  //获取单个map
			for(Entry<String,Object> entry : obj.entrySet()){ 
				Object strval1 = entry.getValue();  //获取map值
				
				if(strval1!=null){
					value = strval1.toString();//赋值map值
				}
			}
		}
		return value;
	}
	/**
	 * @param jsonArrayString  需要转化的JSON字符串。
	 * @param keys 需要转化的列。
	 * @return List<LinkedHashMap<Integer, String>> 格式的数据。
	 */
	public static List<LinkedHashMap<Integer, String>> getNeedListResults(String jsonArrayString,String[] keys){
		
		JSONArray jsonArray = JSONArray.fromObject(jsonArrayString);//字符串转对象
		List<LinkedHashMap<Integer, String>> list = new ArrayList<LinkedHashMap<Integer, String>>();//初始化list
		@SuppressWarnings({ "unchecked", "rawtypes" })
		List<Map<String,Object>> mapListJson = (List)jsonArray;  //对象强转list
		for (int i = 1; i < mapListJson.size(); i++) {  
			Map<String,Object> obj= mapListJson.get(i);  //获取map
			LinkedHashMap<Integer,String> result_map = new LinkedHashMap<Integer,String>();//创建map
			
			for(int k=0;k<keys.length;k++){
				String en = keys[k].trim();//获取key值
				Object o = obj.get(en);//获取值
				String value = "";
				if(o!=null){
					value = o.toString();//赋值value
					if("null".equals(value)||value==null){
						value = "";
					}
				}
				result_map.put(k, value);//填充map
			}
			list.add(result_map);//添加list
		}  
		return list;//返回list
	}
	/**
	 * @param jsonArrayString  需要转化的JSON字符串。
	 * @param keys 需要转化的列。
	 * @return List<LinkedHashMap<String, String>> 格式的数据。
	 */
	public static List<LinkedHashMap<String, String>> getListResults(String jsonArrayString,String[] keys){
		
		JSONArray jsonArray = JSONArray.fromObject(jsonArrayString);//字符串转对象
		List<LinkedHashMap<String, String>> list = new ArrayList<LinkedHashMap<String, String>>();//初始化list
		@SuppressWarnings({ "unchecked", "rawtypes" })
		List<Map<String,Object>> mapListJson = (List)jsonArray;  //对象强转list
		for (int i = 1; i < mapListJson.size(); i++) {  
			Map<String,Object> obj= mapListJson.get(i);  //获取map
			LinkedHashMap<String,String> result_map = new LinkedHashMap<String,String>();//创建map
			
			for(int k=0;k<keys.length;k++){
				String en = keys[k].trim();//获取key值
				Object o = obj.get(en);//获取值
				String value = "";
				if(o!=null){
					value = o.toString();//赋值value
					if("null".equals(value)||value==null){
						value = "";
					}
				}
				result_map.put(en, value);//填充map
			}
			list.add(result_map);//添加list
		}  
		return list;//返回list
	}
	
	/**
	 * @param jsonArrayString 需要转化的JSON字符串。
	 * @return List<LinkedHashMap<String, String>>
	 */
	public static List<LinkedHashMap<String, String>> getListResults(String jsonArrayString){
		
		JSONArray jsonArray = JSONArray.fromObject(jsonArrayString);//字符串转对象
		List<LinkedHashMap<String, String>> list = new ArrayList<LinkedHashMap<String, String>>();//初始化list
		@SuppressWarnings({ "unchecked", "rawtypes" })
		List<Map<String,Object>> mapListJson = (List)jsonArray;  //对象强转list
		for (int i = 1; i < mapListJson.size(); i++) {  
			Map<String,Object> obj= mapListJson.get(i);  //获取map
			LinkedHashMap<String,String> result_map = new LinkedHashMap<String,String>();
			for(Entry<String,Object> entry : obj.entrySet()){  
				String strkey1 = entry.getKey();  //获取键
				Object strval1 = entry.getValue();  //获取值
				String value = null;
				if(strval1!=null){
					value = strval1.toString();
				}
				result_map.put(strkey1, value);//填充map
			}  
			list.add(result_map);//填充list
		}
		return list;
	}
	/**
	 * 根据key获取值
	 * @param jsonStr
	 * @param key
	 * @return
	 */
	public static String getJsonByKey(String jsonStr, String key) {
		JSONObject resultJson = new JSONObject(jsonStr);//创建json对象
		String result = (String) resultJson.get(key);//获取值
		return result;//返回值
	}
}
