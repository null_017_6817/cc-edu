package com.qtrmoon.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

/**
 * 随机数工具类
 *
 */
public class RandomUtils {

	/***
	 * 获取指定[0-bond)范围内的随机整数
	 * @param bound
	 * @return
	 */
	public static int getRandomInteger(int bound){
		if(bound <=0) return 0;//返回正数
		return new Random().nextInt(bound);//返回随机数
	}
	
	/** 
	 * 随机指定范围内N个不重复的数 
	 * 最简单最基本的方法 
	 * @param min 指定范围最小值 
	 * @param max 指定范围最大值 
	 * @param n 随机数个数 
	 */
	public static int[] randomCommon(int min, int max, int n){ 
		if (n > (max - min + 1) || max < min) { 
			return null; 
		} 
		int[] result = new int[n]; 
		int count = 0; 
		while(count < n) { 
			int num = (int) (Math.random() * (max - min)) + min; 
			boolean flag = true; 
			for (int j = 0; j < n; j++) { 
				if(num == result[j]){ 
					flag = false; 
					break; 
				} 
				if(flag){ 
					result[count] = num; 
					count++; 
				}
			}
		} 
		return result; 
	}
	
	/** 
	 * 随机指定范围内N个不重复的数 
	 * 最简单最基本的方法 
	 * @param min 指定范围最小值 
	 * @param max 指定范围最大值 
	 * @param n 随机数个数 
	 */
	public static int[] myRandom(int min, int max, int n){ 
		if (n > (max - min + 1) || max < min) { 
			return null; 
		} 
		List<Integer> temp = new ArrayList<Integer>();
		for(int i = min ; i < max; i++){
			temp.add(i);
		}
		int[] result = new int[n];
		int count = 0;
		Random rand =new Random();
		while(count < n){
			int tempSize = temp.size();
			int num = rand.nextInt(tempSize);
			count++;
			for(int j=0 ; j < count;j++){
				result[j]=temp.get(num);
				temp.remove(num);
			}
		}
		return result; 
	}
	
	/** 
	 * 随机指定范围内N个不重复的数 
	 * 利用HashSet的特征，只能存放不同的值 
	 * @param min 指定范围最小值 
	 * @param max 指定范围最大值 
	 * @param n 随机数个数 
	 * @param HashSet<Integer> set 随机数结果集 
	 */
	public static void randomSet(int min, int max, int n, HashSet<Integer> set) { 
		if (n > (max - min + 1) || max < min) { 
			return; 
		} 
		for (int i = 0; i < n; i++) { 
			// 调用Math.random()方法 
			int num = (int) (Math.random() * (max - min)) + min; 
			set.add(num);// 将不同的数存入HashSet中 
			if(set.size() == n){
				break;
			}
		} 
		int setSize = set.size(); 
		// 如果存入的数小于指定生成的个数，则调用递归再生成剩余个数的随机数，如此循环，直到达到指定大小 
		if (setSize < n) { 
			randomSet(min, max, n, set);// 递归 
		} 
	}
}
