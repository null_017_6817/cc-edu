package com.qtrmoon.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.jboss.netty.util.internal.ConcurrentHashMap;
import org.springframework.context.support.ClassPathXmlApplicationContext;
/**
 * 
 * 物流信息类
 */
public class WlsldxDataGenMain {
	//订单号集合
	private static List<String> orderList = new ArrayList<String>();
	//订单号
	static {
		orderList.add("500463629666");//填充list
		orderList.add("500463629665");//填充list
		orderList.add("500463629664");//填充list
		orderList.add("500463629663");//填充list
		orderList.add("500463629662");//填充list
		orderList.add("500463629667");//填充list
		orderList.add("500463629645");//填充list
		orderList.add("500463629646");//填充list
		orderList.add("500463629653");//填充list
		orderList.add("500463629659");//填充list
		orderList.add("500463629676");//填充list
		orderList.add("500463629670");//填充list
		orderList.add("500463629672");//填充list

	}
	//快递价格映射
	private static Map<String, Double> map = new ConcurrentHashMap<String, Double>();
	//快递价格映射填充
	static {
		map.put("天天快递", 12.00);//填充map
		map.put("圆通快递", 13.00);//填充map
		map.put("顺丰快递", 22.00);//填充map
		map.put("申通快递", 15.00);//填充map
		map.put("韵达快递", 11.00);//填充map
		map.put("中通快递", 12.00);//填充map
		map.put("宅急送", 12.00);//填充map
		map.put("速尔快递", 9.00);//填充map
		map.put("EMS", 20.00);//填充map
		map.put("全峰快递", 10.00);//填充map
		map.put("优速快递", 13.00);//填充map
		map.put("百世汇通", 12.00);//填充map
	}
	/*
	 * 获取快递价格
	 */
	private static Double getBaseFee(String wlgs) {
		return map.get(wlgs);
	}
	
	public static void main(String[] args) {
		//初始化spring容器
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
				"classpath:SpringConf.xml");
		//反转获取模拟数据生成工具类
		DataGenUtil dataGenUtil = (DataGenUtil) context.getBean("dataGenUtil");
		
		genWlOrderData(dataGenUtil);//批量生成物流订单
		context.close();//关闭上下文
	}
	/**
	 * 批量生成物流订单
	 * @param dataGenUtil
	 */
	public static void genWlOrderData(DataGenUtil dataGenUtil) {
		String wlgs = "圆通快递";
		for (int i = 0; i < orderList.size(); i++) {
			//模拟生成物流订单数据
			dataGenUtil.genWlOrderData(orderList.get(i), getBaseFee(wlgs)
					.intValue(), RandChinseNameUtils.getChineseName(),
					RandChinseNameUtils.getChineseName(), PhoneUtils
							.genRandomPhoneNumber(), PhoneUtils
							.genRandomPhoneNumber(), wlgs);
		}
	}
	/**
	 * 批量生成个人套餐使用剩余情况
	 * @param dataGenUtil
	 */
	public static void genPersonDxZFData(DataGenUtil dataGenUtil) {
		for (int i = 0; i < 1000; i++) {
			//生成个人套餐使用剩余情况
			System.err.println(dataGenUtil.genPersonDxZFData(7,
					PhoneUtils.genRandomPhoneNumber(),
					RandChinseNameUtils.getChineseName()));
		}
	}
}
