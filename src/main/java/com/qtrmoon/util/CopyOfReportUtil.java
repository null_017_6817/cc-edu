package com.qtrmoon.util;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import jxl.CellView;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.qtrmoon.dictionary.bsd.KeyVal;

public class CopyOfReportUtil {

	public static void writeExcel(List<KeyVal> heads, JSONArray datas, File file) {
		try {
			if (!file.exists()) {
				file.createNewFile();
			}
			WritableWorkbook wwb = Workbook.createWorkbook(file);
			WritableSheet ws = wwb.createSheet("datasheet", 0);
			CellView cellView = new CellView();
			cellView.setAutosize(true);
			WritableFont wfc = new WritableFont(WritableFont.ARIAL, 15,
					WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE,
					Colour.BLACK);
			WritableCellFormat wcfFC = new WritableCellFormat(wfc);
			wcfFC.setAlignment(Alignment.CENTRE);
			wcfFC.setVerticalAlignment(VerticalAlignment.CENTRE);
			wcfFC.setWrap(false);
			Label labelC = null;

			int[] colsize = new int[heads.size()];
			int column = 0;
			for (KeyVal head : heads) {
				labelC = new Label(column, 0, head.getVal(), wcfFC);
				ws.addCell(labelC);
				colsize[column] = Math.max(colsize[column], head.getVal().getBytes().length);
				column++;
			}
			JSONObject data;
			Object localObject;
			for (int i = 0; i < datas.size(); i++) {
				data = (JSONObject) datas.get(i);
				column = 0;
				for (localObject = heads.iterator(); ((Iterator) localObject).hasNext();) {
					KeyVal head = (KeyVal) ((Iterator) localObject).next();
					if (data.get(head.getKey()) == null) {
						column++;
					} else {
						labelC = new Label(column, i + 1, data.get(head.getKey()).toString(), wcfFC);
						ws.addCell(labelC);
						colsize[column] = Math.max(colsize[column], data.get(head.getKey()).toString().getBytes().length);
						column++;
					}
				}
			}
			column = 0;
			int head =colsize.length;
			for (int localKeyVal1 = 0; localKeyVal1 < head; localKeyVal1++) {
				int onesize = colsize[localKeyVal1];
				if(onesize>15)onesize=15;
				ws.setColumnView(column, (int) (onesize * 1.5D));
				column++;
			}
			wwb.write();
			wwb.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	  
}
