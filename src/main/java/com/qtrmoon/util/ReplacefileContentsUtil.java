package com.qtrmoon.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * 
 * @author Raven 批量替换指定目录下所有文件中内容
 */
public class ReplacefileContentsUtil {

	/**
	 * @param file
	 * @return String 读取的文件内容
	 */
	private static String readFile(File file) {

		StringBuffer res = new StringBuffer();
		String line = null;
		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			while ((line = reader.readLine()) != null) {
				res.append(line + "\n");
			}
			reader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return res.toString();
	}

	/**
	 * 
	 * @param cont
	 *            已经替换好的文件内容
	 * @param file
	 *            写入原路径文件
	 */
	private static void writeFile(String cont, File file) {
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(file));
			writer.write(cont);
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param src
	 *            指定目录
	 * @param target
	 *            要替换的内容
	 * @param replacement
	 *            要替换成的内容
	 */
	private static void foreachFile(File src, String target, String replacement) {

		File[] fs = src.listFiles();

		for (File f : fs) {
			if (f.isDirectory()) // 若是目录，则递归打印该目录下的文件
				foreachFile(f, target, replacement);
			if (f.isFile()) {
				String cont = readFile(f);
				cont = cont.replaceAll(target, replacement);
				writeFile(cont, f);
			}

		}
	}

	/**
	 * 
	 * @param src
	 *            指定目录路径
	 * @param target
	 *            要替换的内容
	 * @param replacement
	 *            要替换成的内容
	 * @return boolean 替换成功返回true，失败返回false
	 */
	public static boolean replaceContents(String src, String target, String replacement) {
		File file = new File(src);
		if (file.exists()) {// 判断指定的路径是否存在
			
			if (file.isFile()) {// 如果指定的不是目录
				String cont = readFile(file);
				cont = cont.replaceAll(target, replacement);
				writeFile(cont, file);
				return true;
			}
			foreachFile(file, target, replacement);
			return true;
		} 
		return false;		
	}
	public static void main(String[] args) {
		System.out.println(replaceContents("E:\\html", "cc-pt", "cc-edu"));	
	}
}
