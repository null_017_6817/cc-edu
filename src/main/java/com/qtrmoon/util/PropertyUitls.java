package com.qtrmoon.util;
 
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * 读取配置文件通用类
 * 
 */
public class PropertyUitls {
	protected static Logger logger = Logger.getLogger(PropertyUitls.class);//日志类

	 //保存系统文件。
	 private static String sysFile = "config.properties";
	 private static Properties Sysproperties ;//声明配置类
	 static {
		 Sysproperties = PropertyUitls.getProperties(sysFile);//初始化配置读取类
	 }
	
	/**
	 * 获取指定路径下的配置文件信息
	 * 
	 * @param configPath
	 * @return
	 */
	public static Properties getProperties(String configPath) {
		Properties pros = new Properties();//定义配置类
		try {
			ClassLoader cl = PropertyUitls.class.getClassLoader();//获取加载器
			pros.load(cl.getResourceAsStream(configPath));//读取配置文件
		} catch (IOException e) {
			e.printStackTrace();
		}
		return pros;
	}
	/**
	 * 获取配置文件值
	 * @param key
	 * @return
	 */
	public static String getSysConfigSet(String key){
		return Sysproperties.getProperty(key);
	}
}
