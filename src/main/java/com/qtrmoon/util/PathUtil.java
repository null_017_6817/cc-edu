package com.qtrmoon.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;



/**
 * url工具类
 *
 */
public class PathUtil {
	/**
	 * url拼接 "/"
	 * @return
	 */
	public static String appendBackSlant(String path) {
		if(StringUtils.isNotBlank(path)){
			if(!path.endsWith("/")){
				return path +"/";
			}else{
				return path;
			}
		}
		return path;
	}

	/**
	 * 把掺杂文件名和部分路径的字符串拆分开
	 * @param pathName
	 * @return
	 */
	public static Map<String,Object> splitPathName(String pathName){
		Map<String,Object> result = new HashMap<String,Object>();
		if( pathName != null && !"".equals(pathName) ){
			String fileName = pathName.substring(pathName.lastIndexOf("\\")+1);
			String filePath = pathName.substring(0,pathName.lastIndexOf("\\"));
			System.out.println(fileName);
			System.out.println(filePath);
			if(fileName != null ){
				result.put("fileName", fileName);
			}else{
				result.put("fileName", "");
			}
			if(filePath != null){
				result.put("filePath", filePath);
			}else{
				result.put("filePath", "");
			}
			
		}else{
			result.put("filePath", "");
			result.put("fileName", "");
		}
		return result;
	}

	
	public static void main(String[] args){
		String path = "D:/asd/ddd/fff/ggg/hh/jj/asd.wav";
		splitPathName(path);
	}
}
