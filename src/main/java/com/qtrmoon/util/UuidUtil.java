package com.qtrmoon.util;

import java.util.UUID;

import org.apache.log4j.Logger;

/**
 * 
 * uuid生成工具类
 */
public class UuidUtil {
	//日志记录
	protected static Logger logger = Logger.getLogger(UuidUtil.class);
	/**
	 * 获取32位uuid
	 * @return
	 */
	public static String get32UUID() {
		String uuid = UUID.randomUUID().toString().trim().replaceAll("-", "");//生成32位uuid
		return uuid;
	}
	/**
	 * 生成32位uuid
	 * @param args
	 */
	public static void main(String[] args) {
		logger.info(get32UUID());
	}
}
