package com.qtrmoon.util;

import java.util.concurrent.CopyOnWriteArraySet;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.qtrmoon.camp.ctrl.StudSocket;
import com.qtrmoon.manager.constant.YesOrNoConstant;
import com.qtrmoon.manager.pojo.Stud;
import com.qtrmoon.manager.serdao.IManagerService;
import com.qtrmoon.sysmanage.pojo.User;
import com.qtrmoon.sysmanage.serdao.ISysmanageService;

/**
 * 用户登录工具类
 */
public class LoginStudUtil {

	/**
	 * 日志对象
	 */
	public static Logger logger = LoggerFactory.getLogger(LoginStudUtil.class);
	
	/**
	 * 有用户登录
	 * @param stud
	 */
	public static void studLogin(User user){
		if( user != null && user.getId() != null 
				&& user.getNumber() != null && !"".equals(user.getNumber())){
			logger.error("检测到学生 【{}】 学号 【{}】  在进行登录操作",user.getName(),user.getNumber());
			// 调用发送即时消息的方法
			sendSocketMsg(user,"login");
		}
	}
	
	/**
	 * 有用户退出
	 * @param stud
	 */
	public static void studLogout(User user){
		
		if( user != null && user.getId() != null 
				&& user.getNumber() != null && !"".equals(user.getNumber())){
			logger.error("检测到学生 【{}】 学号 【{}】  在进行注销操作",user.getName(),user.getNumber());
			ISysmanageService sysmanageService = (ISysmanageService) ApplicationContextUtil.getApplicationContext()
					.getBean("sysmanageService");//获取service
			User us = sysmanageService.schUserById(user.getId());//查询学生表
			if(us != null){
				us.setAgentId("");//设置坐席号
				us.setIsLogin(YesOrNoConstant.NO);// 修改为已登录
				sysmanageService.updUser(us);//更新学生
			}
			sendSocketMsg(user,"logout");
		}
	}
	
	/**
	 * 发送即时消息工具方法
	 * @param stud
	 * @param doType
	 */
	@SuppressWarnings("unchecked")
	private static void sendSocketMsg(User user, String doType){
		String number = user.getNumber(); // 学号
		String agentId = user.getAgentId(); // 坐席号
		try {
			// 获取全部soket对象
			CopyOnWriteArraySet<StudSocket> sockets = StudSocket.getWebSocketSet();
			for (StudSocket socket : sockets) {
				JSONObject jsObj = new JSONObject();
				jsObj.put("doType", doType);// 操作类型为登录
				jsObj.put("number", number);// 学号
				jsObj.put("agentId", agentId);// 坐席号
				//发送即时消息
				logger.info(">>>>发送即时消息,学号:{}, 坐席号:{}, 操作类型:{} <<<<",number,agentId,doType);
				socket.sendMessage(jsObj.toJSONString());
			}
		}catch(Exception e){
			logger.error(">>>>发送即时消息异常<<<<");
			e.printStackTrace();
		}
	}
	
	
}
