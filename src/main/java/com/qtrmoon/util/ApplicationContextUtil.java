package com.qtrmoon.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
/**
 * spring上下文工具类
 *
 */
public class ApplicationContextUtil implements ApplicationContextAware {
	//声明上下文
	private static ApplicationContext applicationContext;
	//获取上下文
	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}
	//注入上下文
	public void setApplicationContext(ApplicationContext app) throws BeansException {
		
		applicationContext = app;
		RunTask rt = new RunTask();//初始化线程
		rt.run();//开启线程
	}
	//定义线程类
	class RunTask extends Thread{
		public void run(){
			//开启sip监听
			SipOnLineUtil.inBand();
		}
	}
}
