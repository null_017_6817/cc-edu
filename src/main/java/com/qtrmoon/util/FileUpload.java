package com.qtrmoon.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.FileUtils;
import org.springframework.web.multipart.MultipartFile;

/**
 * 文件上传
 * @version
 */
public class FileUpload {
	/**
	 * 上传文件
	 * 
	 * @param file
	 *            //文件对象
	 * @param filePath
	 *            //上传路径
	 * @param uuid
	 *            //文件uuid名
	 * @return 文件名
	 * @throws IOException
	 */
	public static void fileUp(MultipartFile file, String filePath, String uuid)
			throws IOException {
		String extName = ""; // 扩展名格式
		if (file.getOriginalFilename().lastIndexOf(".") >= 0) {//字符串有.
			extName = file.getOriginalFilename().substring(
					file.getOriginalFilename().lastIndexOf("."));// 获取文件后缀名
		}
		// 写文件到当前目录的upload目录中
		copyFile(file.getInputStream(), filePath, uuid + extName);
	}

	/**
	 * 写文件到当前目录的upload目录中
	 * 
	 * @param in
	 * @param fileName
	 * @throws IOException
	 */
	public static void copyFile(InputStream in, String dir, String uuidName)
			throws IOException {
		File file = new File(dir, uuidName);// 获取文件
		if (!file.exists()) {
			if (!file.getParentFile().exists()) {
				file.getParentFile().mkdirs();// 创建文件
			}
			file.createNewFile();//创建文件
			FileUtils.copyInputStreamToFile(in, file);// copy流数据到文件
		}
	}

	/**
	 * 富文本上传
	 * 
	 * @param file
	 *            上传文件对象
	 * @param filePath
	 *            上传路径
	 * @throws IOException
	 */
	public static void fileUpForUeditor(MultipartFile file, String filePath)
			throws IOException {
		String extName = file.getOriginalFilename(); // 扩展名格式
		copyFileForUeditor(file.getInputStream(), filePath, extName);// copy流数据到文件
	}

	/**
	 * 富文本上传
	 * 
	 * @param in
	 *            流数据
	 * @param dir
	 *            目录
	 * @param uuidName
	 *            文件名
	 * @throws IOException
	 */
	public static void copyFileForUeditor(InputStream in, String dir,
			String uuidName) throws IOException {
		File file = new File(dir, uuidName);// 创建文件对象
		if (!file.exists()) {
			if (!file.getParentFile().exists()) {
				file.getParentFile().mkdirs();//生成目录
			}
			file.createNewFile();//创建文件
			FileUtils.copyInputStreamToFile(in, file);//copy流数据到文件
		}
	}
	
	/**
	 * 拷贝目录
	 * 
	 * @param sourcePath
	 *            源文件路径
	 * @param newPath
	 *            新文件路径
	 * @throws IOException
	 */
	
	public static void copyDir(String sourcePath, String newPath) throws IOException {
        File file = new File(sourcePath);
        String[] filePath = file.list();
        
        if (!(new File(newPath)).exists()) {
            (new File(newPath)).mkdir();
        }
        
        if(filePath != null){
        	for (int i = 0; i < filePath.length; i++) {
        		if ((new File(sourcePath + file.separator + filePath[i])).isDirectory()) {
        			copyDir(sourcePath  + file.separator  + filePath[i], newPath  + file.separator + filePath[i]);
        		}
        		
        		if (new File(sourcePath  + file.separator + filePath[i]).isFile()) {
        			copyFile(sourcePath + file.separator + filePath[i], newPath + file.separator + filePath[i]);
        		}
        		
        	}
        }
    }
	
	/**
	 * 拷贝文件
	 * 
	 * @param sourcePath
	 *            源文件路径
	 * @param newPath
	 *            新文件路径
	 * @throws IOException
	 */
	
	
	public static void copyFile(String oldPath, String newPath) throws IOException {
	    File oldFile = new File(oldPath);
	    File file = new File(newPath);
	    FileInputStream in = new FileInputStream(oldFile);
	    FileOutputStream out = new FileOutputStream(file);;
	
	    byte[] buffer=new byte[2097152];
	    int readByte = 0;
	    while((readByte = in.read(buffer)) != -1){
	        out.write(buffer, 0, readByte);
	    }
	
	    in.close();
	    out.close();
	}
}