package com.qtrmoon.util;


/***
 *  手机号码工具类
 *
 */
public class PhoneUtils {

	//移动号码段
	public static final String CHINA_MOBILE_PHONE_AREA_STR = "134,135,136,137,138,139,147,150,151,152,157,158,159,178,182,183,184,187,188";
	//联通号码段
	public static final String CHINA_UNICOM_PHONE_AREA_STR = "130,131,132,155,156,185,186,145,176";
	//电信号码段
	public static final String CHINA_TELECOM_PHONE_AREA_STR = "133,153,177,180,181,189";
	
	/**
	 * 生成随机手机号码
	 * @return
	 */
	public static String  genRandomPhoneNumber(){
		 String phone = "";
		 while(true){
			 //全网号码段
			 String phoneArea = CHINA_MOBILE_PHONE_AREA_STR.concat(CHINA_TELECOM_PHONE_AREA_STR).concat(CHINA_UNICOM_PHONE_AREA_STR);
			 String phones[] = phoneArea.split(",");
			 int index = getNum(0,phones.length-1); //随机获取坐标 
			 String first = phones[index];  //获取前三位
			 String second = String.valueOf(getNum(1,888)+10000).substring(1);//获取中间四位  
			 String thrid=String.valueOf(getNum(1,9100)+10000).substring(1);  
			 
			 phone = first.concat(second).concat(thrid);//获取号码
			 if(phone.length()==11){
				break;
			 }
		 }
		return phone;
	}
	/**
	 * 获取随机数
	 * @param start
	 * @param end
	 * @return
	 */
	 private static int getNum(int start,int end) {  
	   return (int)(Math.random()*(end-start+1)+start);  
	}
	/**
	 * 打印随机数
	 * @param args
	 */
	 public static void main(String[] args) {
		System.err.println(PhoneUtils.genRandomPhoneNumber());
	}
}
