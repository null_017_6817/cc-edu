package com.qtrmoon.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArraySet;

import org.apache.commons.lang.StringUtils;
import org.freeswitch.esl.client.IEslEventListener;
import org.freeswitch.esl.client.inbound.Client;
import org.freeswitch.esl.client.inbound.InboundConnectionFailure;
import org.freeswitch.esl.client.transport.event.EslEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.qtrmoon.book.util.FilePath;
import com.qtrmoon.camp.ctrl.WebSocketTest;
import com.qtrmoon.dialog.serdao.IDialogService;
import com.qtrmoon.dialog.serdao.IMyFsService;
import com.qtrmoon.manager.pojo.Person;
import com.qtrmoon.manager.pojo.Record;
import com.qtrmoon.manager.serdao.IManagerService;
import com.qtrmoon.sysmanage.Online;
import com.qtrmoon.toolkit.DateTransfer;
/**
 * sip连接类
 *
 */
public class SipOnLineUtil {
	private static final Logger log = LoggerFactory
			.getLogger(SipOnLineUtil.class);//日志
	private static String host = FilePath.getFilePath("freeswitchServerIp");//获取软电话ip
	private static int port = Integer.valueOf(FilePath.getFilePath("freeswitchServerPort"));//获取软电话端口
	private static String password = "ClueCon";//软电话密码
	/**
	 * 初始化sip
	 */
	public static void inBand() {
		final Client client = new Client();//创建客户端
		try {
			client.connect(host, port, password, 100);//连接软电话
			client.setLoggingLevel(password);//认证
		} catch (InboundConnectionFailure e) {
			log.error("Connect failed", e);//打印错误
			e.printStackTrace();//打印异常
			return;
		}
		// 注册事件处理程序
		client.addEventListener(new IEslEventListener() {
			public void eventReceived(EslEvent event) {
				// 通道创建
				if(event.getEventName().equals("CHANNEL_CREATE")){
					
				}
				if (event.getEventName().equals("CHANNEL_ANSWER")) {//通道应答
					Map<String,String> headers=event.getEventHeaders();//获取头信息
					String from=headers.get("Caller-Orig-Caller-ID-Number");//来电号码
					String to=headers.get("Caller-Destination-Number");//去电号码
					if(from!=null&&to!=null&&!from.equals("")&&!to.equals("")){//来电去电不为空
						Person fromPerson = Online.getOnlinePerson().get(from);//获取来电学生
						Person toPerson = Online.getOnlinePerson().get(to);//获取去电学生
						if(fromPerson != null && toPerson != null && fromPerson.getAnswerState() == null) {//已登录未接通电话
							
							CopyOnWriteArraySet<WebSocketTest> webSocketSet = WebSocketTest.getWebSocketSet();//创建socket连接
							for (WebSocketTest webSocketTest : webSocketSet) {//遍历连接
								try {
									webSocketTest.sendMessage("change");//发送消息
								} catch (IOException e) {
									e.printStackTrace();//打印异常
								}
								if(webSocketTest.getAgentId()!= null && webSocketTest.getAgentId().equals(to)) {//获取去电信息
									try {
										webSocketTest.sendMessage(toPerson.getAgentId());//发送去电坐席
									} catch (IOException e) {
										e.printStackTrace();//打印异常
									}
								}
							}
							fromPerson.setAnswerState(1);//标记已接
							if(StringUtils.isNotBlank(toPerson.getName()) && StringUtils.isNotBlank(toPerson.getName()) && StringUtils.isNotBlank(toPerson.getName())) {//判断去电名字
								List<String> toList = new ArrayList<String>();//创建集合
								toList.add(toPerson.getName());//添加名字
								toList.add(toPerson.getNumber());//添加学号
								toList.add(toPerson.getAgentId());//添加坐席
								fromPerson.setTo(toList);//设置去电信息
							}
							if(StringUtils.isNotBlank(fromPerson.getName()) && StringUtils.isNotBlank(fromPerson.getName()) && StringUtils.isNotBlank(fromPerson.getName())) {//判断去电名字
								String callTime = DateTransfer.toString(new Date(),"yyyy-MM-dd HH:mm:ss");//获取时间
								toPerson.setCallTime(callTime);//设置拨打时间
								List<String> fromList = new ArrayList<String>();//创建集合
								fromList.add(fromPerson.getName());//添加名字
								fromList.add(fromPerson.getNumber());//添加学号
								fromList.add(fromPerson.getAgentId());//添加坐席
								toPerson.setProfession(fromPerson.getProfession());//设置行业
								toPerson.setTo(fromList);//设置来电信息
							}
						}
						String channelState = headers.get("Channel-State");//获取状态
						
						if("CS_CONSUME_MEDIA".equals(channelState)) {//判断状态
							try {
								Thread.sleep(200);//睡眠
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
							IManagerService managerService = (IManagerService) ApplicationContextUtil.getApplicationContext().getBean("managerService");//获取service
							String suffix = ".wav";//后缀名
							String filename=null;//文件名
							int random=(int)(Math.random()*100);//获取随机数
							String randomStr=String.valueOf(random);//随机字符串
							String timeStr=DateTransfer.toString(new Date(), "yyMMddHHmmss");//时间串
							filename = randomStr+timeStr+suffix;//文件名
							Record record  = new Record();//创建录音实体
							if(fromPerson != null && toPerson != null && fromPerson.getAnswerState() == 1) {//接通电话
								record.setFromNum(fromPerson.getNumber());//设置来电学号
								record.setToNum(toPerson.getNumber());//设置去电学号
								record.setTaskId(toPerson.getTaskId());//设置任务id
								if( fromPerson.getTaskType()!= null && !"".equals(fromPerson.getTaskType())){
									record.setType(fromPerson.getTaskType());
								}else{
									if(toPerson.getTaskType() != null && !"".equals(toPerson.getTaskType())){
										record.setType(toPerson.getTaskType());
									}
								}
							
							}
							record.setVoice(filename);					// 录音名
							record.setFromAgent(from);//设置来电坐席
							record.setToAgent(to);//设置去电学习
							// 调用 fsGUI(go) 来录音
							IDialogService dialogService = (IDialogService) ApplicationContextUtil.getApplicationContext().getBean("dialogService");//获取service
							dialogService.record(from, filename);//录音(接口)
							// 调用自己的java程序来录音
							// IMyFsService myFsService = (IMyFsService) ApplicationContextUtil.getApplicationContext().getBean("myFsService");//获取service
							// myFsService.record(from, filename);
							managerService.addRecord(record);//记录录音
						}
					}
				}
				if (event.getEventName().equals("HEARTBEAT")) {//心跳
				}
				if (event.getEventName().equals("CHANNEL_DESTROY")) {//通道摧毁
				}
				if (event.getEventName().equals("CHANNEL_HANGUP_COMPLETE")) {//挂断电话
					// 挂断
					Map<String,String> headers=event.getEventHeaders();//获取头信息
					String from=headers.get("Caller-Orig-Caller-ID-Number");//获取来电号码
					String to=headers.get("Caller-Destination-Number");//获取去电号码
					if(from!=null&&to!=null&&!from.equals("")&&!to.equals("")){//不为空
						Person fromPerson = Online.getOnlinePerson().get(from);//获取来电学生
						
						Person toPerson = Online.getOnlinePerson().get(to);//去电学生
						if(fromPerson != null) {
							if(fromPerson.getAnswerState() != null && fromPerson.getAnswerState() == 1) {//接通电话
								if(fromPerson != null && toPerson != null) {//学生登录
									CopyOnWriteArraySet<WebSocketTest> webSocketSet = WebSocketTest.getWebSocketSet();//获取websocket
									for (WebSocketTest webSocketTest : webSocketSet) {
										try {
											webSocketTest.sendMessage("change");//发送消息
										} catch (IOException e) {
											e.printStackTrace();//打印异常
										}
									}
								}
							}
							fromPerson.setAnswerState(null);//设置挂断
						}
					}
				}
			}

			public void backgroundJobResultReceived(EslEvent event) {
				String uuid = event.getEventHeaders().get("Job-UUID");//获取uuid
				log.info("Background job result received+:"+ event.getEventName() + "/" + event.getEventHeaders() + uuid);//打印信息
			}
		});

		// 定义事件日志输出格式,但是java esl 目前只支持plain格式
		// ，http://wiki.freeswitch.org/wiki/Event_Socket
		// 2012-12-25 19:20:30 [ main:426 ] - [ ERROR ]
		// java.lang.IllegalStateException: Only 'plain' event format is
		// supported at present
		client.setEventSubscriptions("plain", "all");//打印心跳

	}
}