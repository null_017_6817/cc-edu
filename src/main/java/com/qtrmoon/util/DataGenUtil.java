package com.qtrmoon.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.qtrmoon.slwldx.pojo.Grtcylxx;
import com.qtrmoon.slwldx.pojo.Tcxx;
import com.qtrmoon.slwldx.pojo.Wlddxx;
import com.qtrmoon.slwldx.serdao.ISlwldxService;

/***
 * 模拟数据生成工具类
 */
@Component
public class DataGenUtil {

	@Autowired
	private ISlwldxService slwldxService; // 声明模块Service实例

	/**
	 * 模拟生成个人套餐使用剩余情况
	 * 
	 * @param tcId
	 *            套餐Id
	 * @param phone
	 *            手机号
	 * @param personName
	 *            姓名
	 */
	public boolean genPersonDxZFData(int tcId, String phone, String personName) {
		// 获取套餐信息
		Tcxx tcxx = slwldxService.schTcxxById(tcId);
		int dxsy = 0;// 短信剩余（条数）
		int gnllsy = 0;// 国内流量剩余（M）
		int bdllsy = 0;// 本地流量剩余（M）
		int bdthsy = 0;// 本地通话剩余(分钟)
		int qgthsy = 0;// 全国通话剩余（分钟）
		float hfye = 0;// 话费余额
		if (tcxx != null) {
			dxsy = RandomUtils.getRandomInteger(tcxx.getDx());//随机短信剩余
			gnllsy = RandomUtils.getRandomInteger(tcxx.getGnll());//随机国内流量剩余
			bdllsy = RandomUtils.getRandomInteger(tcxx.getBdll());//随机本地流量剩余
			bdthsy = RandomUtils.getRandomInteger(tcxx.getBdth());//随机本地通话剩余
			qgthsy = RandomUtils.getRandomInteger(tcxx.getQgth());//随机全国通话剩余
			hfye = RandomUtils.getRandomInteger(200);//随机话费余额
		}
		//初始化个人套餐用量信息
		Grtcylxx grtcylxx = new Grtcylxx(qgthsy, phone, personName, tcId, dxsy,
				gnllsy, bdllsy, bdthsy, qgthsy, hfye, personName);
		return slwldxService.addGrtcylxx(grtcylxx) > 0 ? true : false;
	}

	/***
	 * 模拟生成物流订单数据
	 * 
	 * @param order
	 * @param ddjg
	 * @param jjr
	 * @param sjr
	 * @param jjrdh
	 * @param sjrdh
	 * @param wlgs
	 */
	public void genWlOrderData(String order, Integer ddjg, String jjr,
			String sjr, String jjrdh, String sjrdh, String wlgs) {
		Wlddxx wlddxx = new Wlddxx();//物流订单信息
		wlddxx.setDdh(order);//设置订单号
		wlddxx.setDdjg(ddjg);//设置订单金额
		wlddxx.setJjr(jjr);//设置寄件人
		wlddxx.setSjr(sjr);//设置收件人
		wlddxx.setJjrdh(jjrdh);//设置寄件人电话
		wlddxx.setSjrdh(sjrdh);//设置收件人电话
		wlddxx.setWlgs(wlgs);//设置物流公司
		
		slwldxService.addWlddxx(wlddxx);//物流订单入库
	}

}
