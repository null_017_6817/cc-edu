package com.qtrmoon.camp.serdao;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.qtrmoon.camp.pojo.Dxwj;
import com.qtrmoon.camp.pojo.Hyanswer;
import com.qtrmoon.camp.pojo.Hytopic;
import com.qtrmoon.camp.pojo.Score;
import com.qtrmoon.camp.pojo.TrainCamp;
import com.qtrmoon.camp.pojo.ZdRepository;
import com.qtrmoon.camp.vo.GroupScoreVo;
import com.qtrmoon.camp.vo.IndexScoreVo;
import com.qtrmoon.camp.vo.PersonScoreVo;
import com.qtrmoon.camp.vo.ScoreDetailVo;
import com.qtrmoon.camp.vo.ScoreVo;
import com.qtrmoon.camp.vo.TeacherScoreVo;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.examination.pojo.ExerTopic;
import com.qtrmoon.examination.serdao.IExaminationService;
import com.qtrmoon.manager.pojo.Classes;
import com.qtrmoon.template.dto.TemplateIndexDTO;
import com.qtrmoon.template.pojo.Template;
import com.qtrmoon.template.pojo.TemplateCategory;
import com.qtrmoon.template.serdao.ITemplateService;

/**
 * 评分service实现类
 */
@Service("campService")
public class CampServiceImpl implements ICampService {
	
	@Autowired
	private TrainCampMapper trainCampMapper;// 声明模块Mapper实例
	
	@Autowired
	private IExaminationService examinationService; // 声明模块Service实例
	
	@Autowired
	private ITemplateService templateService; // 声明模块Service实例
	
	/***************** TrainCamp方法组 *****************/
	/**
	 * TrainCamp的条件查询方法
	 * @param trainCamp 承载查询条件的Bean
	 * @return 返回TrainCamp的集合
	 */
	@Override
	public List<TrainCamp> schTrainCamp(TrainCamp trainCamp) {
		PageHelper.startPage(trainCamp.getPage(), trainCamp.getPagesize());//设置分页显示
		List<TrainCamp> list=trainCampMapper.schTrainCamp(trainCamp);
		Page<TrainCamp> page = (Page<TrainCamp>)list;//为了设置总记录数先类型强转
		trainCamp.setDatasize(page.getTotal());
		return list;
	}
	
	/**
	 * TrainCamp的主键查询方法
	 * @param id 主键值
	 * @return 返回TrainCamp实体
	 */
	@Override
	public TrainCamp schTrainCampById(Integer id) {
		return trainCampMapper.schTrainCampById(id);
	}
	
	/**
	 * TrainCamp的添加方法
	 * @param trainCamp 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	@Override
	public Integer addTrainCamp(TrainCamp trainCamp) {
		return trainCampMapper.addTrainCamp(trainCamp);
	}

	/**
	 * 添加评分数据
	 * @param trainCamp
	 */
	/*public void addTrainCampAndPart(TrainCamp trainCamp) {
		addorupPart(trainCamp);
		trainCamp.setPartnerscore1(null);//互评1为null
		trainCamp.setPartnerscore2(null);//互评2为null
		trainCamp.setPartnerscore3(null);//互评3为null
		addTrainCamp(trainCamp);//调用addTrainCamp方法
	}*/
	
	/**
	 * TrainCamp的修改方法
	 * @param trainCamp 承载数据的Bean
	 */
	@Override
	public void updTrainCamp(TrainCamp trainCamp) {
		trainCampMapper.updTrainCamp(trainCamp);// 更新数据
	}

	/**
	 * TrainCamp的单记录删除方法
	 * @param id 要删除的主键值
	 */
	@Override
	public void delTrainCamp(Integer id) {
		trainCampMapper.delTrainCamp(id);// 删除数据
	}

	/**
	 * TrainCamp的批量删除方法
	 * @param ids 主键值的数组
	 */
	@Override
	public void delTrainCamp(Integer[] ids) {
		trainCampMapper.delTrainCamps(ids);// 批量删除数据
	}
	
	/**
	 * TrainCamp的Bookid和其他id查询方法
	 * @param trainCamp 承载数据的Bean
	 * @return 返回TrainCamp实体
	 */
	public TrainCamp schTrainCampByBookIdAndQtId(TrainCamp trainCamp){
		TrainCamp result = trainCampMapper.schTrainCampByBookIdAndQtId(trainCamp);// 根据id查询数据
		TrainCamp param = new TrainCamp();// 实例化对象
		param.setBookid(trainCamp.getBookid());//设置任务id
		param.setClassid(trainCamp.getClassid());//设置班级id
		param.setPartnerid(trainCamp.getStudid());// 设置互评id
		param.setStudid(trainCamp.getPartnerid());// 设置学生id
		TrainCamp part = trainCampMapper.schTrainCampByBookIdAndQtId(param);// 查询数据
		if(part != null) {
			result.setPartnerscore1(part.getPartnerscore1());// 设置互评1
			result.setPartnerscore2(part.getPartnerscore2());// 设置互评2
			result.setPartnerscore3(part.getPartnerscore3());// 设置互评3
		}
		return result;// 返回结果
	}

	/**
	 * 通过classid查询页面索要显示的字段
	 * @param classid 班级的id
	 */ 
	@Override
	public List<TrainCamp> schTeacherEvaluate(Map<String,Object> param) {
		return trainCampMapper.schTeacherEvaluate(param);// 返回查询结果
	}
	/**
	 * 通过classid查询页面索要显示的字段
	 * @param classid 班级的id
	 */ 
	@Override
	public List<TrainCamp> schStudentEvaluate(Map<String,Object> param) {
		return trainCampMapper.schStudentEvaluate(param);// 返回查询结果
	}

	/**
	 * 查询所有的班级
	 */
	@Override
	public List<Classes> schClasses() {
		return trainCampMapper.schClasses();// 返回查询结果
	}

	/**
	 * 数据修改后通过学生id查询TrainCamp的原有属性值
	 * @param studid学生id
	 */
	@Override
	public TrainCamp schTrainCampBystudid(Map<String,Object> param) {
		return trainCampMapper.schTrainCampBystudid(param);// 返回查询结果
	}

	/**
	 * 教师评价分数更改后修改TrainCamp的属性值
	 * @param TrainCamp
	 */
	@Override
	public void updTeacher(TrainCamp trainCamp) {
		trainCampMapper.updTeacher(trainCamp);// 更新数据
	}
	
	/**
	 * 老师添加评分
	 */
	/*@Override
	public List<TrainCamp> addByTeacer(List<TrainCamp> jsonStr,Person person){
		List<TrainCamp> result = new ArrayList<TrainCamp>();	// 实例化结果集
		for (TrainCamp trainCamp : jsonStr) {// 循环数据
			TrainCamp tc1 = new TrainCamp();// 实例化对象
			tc1.setBookid(trainCamp.getBookid());// 设置任务id
			tc1.setClassid(trainCamp.getClassid());// 设置班级id
			tc1.setStudid(trainCamp.getStudid());// 设置学生id
			if(trainCamp.getId() != null){// 判断id是否为null
				tc1 = trainCampMapper.schTrainCampById(trainCamp.getId());// 根据id查询数据
			}
			if(tc1 == null){// 判断必要参数是否为null
				tc1 = new TrainCamp();// 如果为null
			}
			if(trainCamp.getTeacherscore1() != null){// 如果数据为空，默认为0
				tc1.setTeacherscore1(trainCamp.getTeacherscore1());// 不为空则赋值
			}else{
				tc1.setTeacherscore1(0);// 设置默认值0
			}
			if(trainCamp.getTeacherscore2() != null){// 如果数据为空，默认为0
				tc1.setTeacherscore2(trainCamp.getTeacherscore2());// 不为空则赋值
			}else{
				tc1.setTeacherscore2(0);// 设置默认值0
			}
			if(trainCamp.getTeacherscore3() != null){// 如果数据为空，默认为0
				tc1.setTeacherscore3(trainCamp.getTeacherscore3());// 不为空则赋值
			}else{
				tc1.setTeacherscore3(0);// 设置默认值0
			}
			float total = 0;// 初始化参数为0
			float total1 = 0;// 初始化参数为0
			float total2 = 0;// 初始化参数为0
			if(person.getState() == "2") {// 学生自评分和互评分总和
				tc1.setTeacherid(person.getId());// 设置老师id
			}
			TrainCamp param = new TrainCamp();// 实例化对象
			param.setBookid(trainCamp.getBookid());// 设置bookid
			param.setStudid(trainCamp.getStudid());// 设置学生id
			TrainCamp entity = trainCampMapper.schTrainCampByBookIdAndQtId(trainCamp);// 查询评分数据
			if(entity != null){
				Integer partnerscore1 = entity.getPartnerscore1() == null ? 0 : entity.getPartnerscore1();// 三目获取互评1
				Integer partnerscore2 = entity.getPartnerscore2() == null ? 0 : entity.getPartnerscore2();//三目获取互评2
				Integer partnerscore3 = entity.getPartnerscore3() == null ? 0 : entity.getPartnerscore3();//三目获取互评3
				Integer personalscore1 = entity.getPersonalscore1() == null ? 0 : entity.getPersonalscore1();//三目获取自评1
				Integer personalscore2 = entity.getPersonalscore2() == null ? 0 : entity.getPersonalscore2();//三目获取自评2
				Integer personalscore3 = entity.getPersonalscore3() == null ? 0 : entity.getPersonalscore3();//三目获取自评3
				total1 = (partnerscore1 + partnerscore2 + partnerscore3) * (0.3f)+// 求和
						(personalscore1 + personalscore2 + personalscore3) * (0.3f);// 求和
			}
			if(entity != null && tc1 != null){// 判断必要参数是否为空
				tc1.setTm(entity.getTm());// 设置时间
			}
			
			total2 =  total2 + (tc1.getTeacherscore1() + tc1.getTeacherscore2() + tc1.getTeacherscore3()) * (0.4f);// 总分加上老师得分
			total = total1 + total2;// 求平均数除以3
			tc1.setScore(total);// 设置总分
			if(tc1.getId() != null){// 判断id是否为空
				trainCampMapper.updTeacher(tc1);// 更新数据库
			}else{
				if(entity != null ){// 判断实体是否为空
					tc1.setId(entity.getId());// 设置id
					trainCampMapper.updTeacher(tc1);// 更新数据库
				}else{
					trainCampMapper.addTrainCamp(tc1);// 添加数据到数据库
				}
			}
			result.add(tc1);// 结果集添加对象
		}
		return result;
	}*/

	/**
	 * 更新数据
	 * @param TrainCamp
	 */
	@Override
	public void updTrainCampDyna(TrainCamp trainCamp) {
		trainCampMapper.updTrainCampDyna(trainCamp);// 更新数据
	}
	
	/**
	 * 更新评分和互评分
	 * @param TrainCamp
	 */
	/*public void updTrainCampDynaAndPart(TrainCamp trainCamp) {
		addorupPart(trainCamp);
		trainCamp.setPartnerscore1(null);// 设置Partnerscore1为null
		trainCamp.setPartnerscore2(null);// 设置Partnerscore2为null
		trainCamp.setPartnerscore3(null);// 设置Partnerscore3为null
		updTrainCampDyna(trainCamp);// 更新数据
	}*/
	
	/**
	 * 添加数据互评分数
	 * @param trainCamp
	 */
	/*private void addorupPart(TrainCamp trainCamp) {
		TrainCamp parampart = new TrainCamp();
		parampart.setBookid(trainCamp.getBookid());// 设置任务id
		parampart.setStudid(trainCamp.getPartnerid());// 设置学生id
		parampart.setPartnerid(trainCamp.getStudid());// 设置互评id
		TrainCamp entitypart = schTrainCampByBookIdAndQtId(parampart);
		TrainCamp part = new TrainCamp();
		part.setBookid(trainCamp.getBookid());// 设置任务id
		part.setStudid(trainCamp.getPartnerid());// 设置学生id
		part.setPartnerid(trainCamp.getStudid());// 设置互评id
		part.setClassid(trainCamp.getClassid());// 设置班级id
		part.setPartnerscore1(trainCamp.getPartnerscore1());// 设置互评分1
		part.setPartnerscore2(trainCamp.getPartnerscore2());// 设置互评分2
		part.setPartnerscore3(trainCamp.getPartnerscore3());// 设置互评分3
		Integer t1 = trainCamp.getPartnerscore1()==null? 0:trainCamp.getPartnerscore1();// 获取互评分1
		Integer t2 =  trainCamp.getPartnerscore2()==null? 0:trainCamp.getPartnerscore2();// 获取互评分2
		Integer t3 =  trainCamp.getPartnerscore3()==null? 0:trainCamp.getPartnerscore3();// 获取互评分3
		Integer score = t1 + t2 + t3;// 分数汇总
		part.setScore(score);// 设置分数
		if(entitypart == null) {
			addTrainCamp(part);// 添加数据
		}else{
			updTrainCampDyna(part);// 更新数据
		}
	}*/

	/**
	 * 更新权重分数
	 * @param TrainCamp
	 */
	/*@Override
	public void weightscore(TrainCamp trainCamp) {
		List<TrainCamp> list=trainCampMapper.schTrainCamp(trainCamp); // 查询列表
		Integer personweight = trainCamp.getPersonweight(); // 获取个人权重
		Integer partnerweight = trainCamp.getPartnerweight(); // 获取互评权重
		Integer teacherweight = trainCamp.getTeacherweight(); // 获取教师权重
		float totalweight = personweight + partnerweight + teacherweight;// 总权重
		
		for (TrainCamp result : list) {
			Integer personalscore1 = result.getPersonalscore1() == null ? 0 : result.getPersonalscore1();// 获取自评1
			Integer personalscore2 = result.getPersonalscore2() == null ? 0 : result.getPersonalscore2();// 获取自评2
			Integer personalscore3 = result.getPersonalscore3() == null ? 0 : result.getPersonalscore3();// 获取自评3
			Integer personscore = personalscore1 + personalscore2 + personalscore3;
			Integer partnerscore1 = result.getPartnerscore1() == null ? 0 : result.getPartnerscore1();// 获取互评1
			Integer partnerscore2 = result.getPartnerscore2() == null ? 0 : result.getPartnerscore2();// 获取互评2
			Integer partnerscore3 = result.getPartnerscore3() == null ? 0 : result.getPartnerscore3();// 获取互评3
			Integer partnerscore = partnerscore1 + partnerscore2 + partnerscore3;
			Integer teacherscore1 = result.getTeacherscore1() == null ? 0 : result.getTeacherscore1();// 教师评1
			Integer teacherscore2 = result.getTeacherscore2() == null ? 0 : result.getTeacherscore2();// 教师评2
			Integer teacherscore3 = result.getTeacherscore3() == null ? 0 : result.getTeacherscore3();// 教师评3
			Integer teacherscore = teacherscore1 + teacherscore2 + teacherscore3;
			
			float person =  personscore*personweight;// 自评乘以自评权重
			float part = partnerscore*partnerweight;// 互评乘以互评权重
			float teacher = teacherscore*teacherweight;// 教师评乘以教师评权重
			float score = person/totalweight + part/totalweight + teacher/totalweight;// 总分=自评除以总权重+互评除以总权重+教师评除以总权重
			result.setScore(score);// 设置总分
			updTrainCampDyna(result);// 更新数据
 		}
	}*/

	/*
	 * 查询该学生有录音的个数
	 * @param item
	 */
	@Override
	public Integer schCount(TrainCamp item) {
		return trainCampMapper.schCount(item);// 返回查询结果
	}
	
	/**
	 * 更新自评分
	 * @param trainCamp
	 */
	@Override
	public void updTrainCampPersonalScore(TrainCamp trainCamp) {
		trainCampMapper.updTrainCampPersonalScore(trainCamp);
	}
	
	/**
	 * 更新小组评分
	 * @param trainCamp
	 */
	@Override
	public void updTrainCampPartnerScore(TrainCamp trainCamp) {
		trainCampMapper.updTrainCampPartnerScore(trainCamp);
	}
	
	/**
	 * 更新老师评分
	 * @param trainCamp
	 */
	@Override
	public void updTrainCampTeacherScore(TrainCamp trainCamp) {
		trainCampMapper.updTrainCampTeacherScore(trainCamp);
	}
	
	/**
	 * 更新总分
	 * @param trainCamp
	 */
	@Override
	public void updTrainCampScore(TrainCamp trainCamp) {
		trainCampMapper.updTrainCampScore(trainCamp);
	}

	@Autowired
	private HytopicMapper hytopicMapper;// 声明模块Mapper实例


	/***************** Hytopic方法组 *****************/
	/**
	 * Hytopic的条件查询方法
	 * @param hytopic 承载查询条件的Bean
	 * @return 返回Hytopic的集合
	 */
	@Override
	public List<Hytopic> schHytopic(Hytopic hytopic) {
		PageHelper.startPage(hytopic.getPage(), hytopic.getPagesize());//设置分页显示
		List<Hytopic> list=hytopicMapper.schHytopic(hytopic);
		Page<Hytopic> page = (Page<Hytopic>)list;//为了设置总记录数先类型强转
		hytopic.setDatasize(page.getTotal());// 设置数据长度
		return list;// 返回查询结果
	}
	
	/**
	 * Hytopic的条件查询方法(无分页)
	 * @param hytopic 承载查询条件的Bean
	 * @return 返回Hytopic的集合
	 */
	@Override
	public List<Hytopic> schHytopicList(Hytopic hytopic) {
		List<Hytopic> list=hytopicMapper.schHytopic(hytopic);// 查询结果集
		return list;// 返回查询结果
	}
	
	/**
	 * Hytopic的主键查询方法
	 * @param id 主键值
	 * @return 返回Hytopic实体
	 */
	@Override
	public Hytopic schHytopicById(Integer id) {
		return hytopicMapper.schHytopicById(id);// 返回查询结果
	}
	
	/**
	 * Hytopic的添加方法
	 * @param hytopic 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	@Override
	public Integer addHytopic(Hytopic hytopic) {
		return hytopicMapper.addHytopic(hytopic);// 返回查询结果
	}

	/**
	 * Hytopic的修改方法
	 * @param hytopic 承载数据的Bean
	 */
	@Override
	public void updHytopic(Hytopic hytopic) {
		hytopicMapper.updHytopic(hytopic);// 更新数据
	}

	/**
	 * Hytopic的单记录删除方法
	 * @param id 要删除的主键值
	 */
	@Override
	public void delHytopic(Integer id) {
		hytopicMapper.delHytopic(id);// 删除数据
	}

	/**
	 * Hytopic的批量删除方法
	 * @param ids 主键值的数组
	 */
	@Override
	public void delHytopic(Integer[] ids) {
		hytopicMapper.delHytopics(ids);// 删除数据
	}

	@Autowired
	private DxwjMapper dxwjMapper;// 声明模块Mapper实例


	/***************** Dxwj方法组 *****************/
	/**
	 * Dxwj的条件查询方法
	 * @param dxwj 承载查询条件的Bean
	 * @return 返回Dxwj的集合
	 */
	@Override
	public List<Dxwj> schDxwj(Dxwj dxwj) {
		PageHelper.startPage(dxwj.getPage(), dxwj.getPagesize());//设置分页显示
		List<Dxwj> list=dxwjMapper.schDxwj(dxwj);// 查询数据
		Page<Dxwj> page = (Page<Dxwj>)list;//为了设置总记录数先类型强转
		dxwj.setDatasize(page.getTotal());// 设置数据长度
		return list;// 返回查询结果
	}
	
	/**
	 * Dxwj的主键查询方法
	 * @param id 主键值
	 * @return 返回Dxwj实体
	 */
	@Override
	public Dxwj schDxwjById(Integer id) {
		return dxwjMapper.schDxwjById(id);// 返回查询结果
	}
	
	/**
	 * Dxwj的添加方法
	 * @param dxwj 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	@Override
	public Integer addDxwj(Dxwj dxwj) {
		return dxwjMapper.addDxwj(dxwj);// 返回查询结果
	}

	/**
	 * Dxwj的修改方法
	 * @param dxwj 承载数据的Bean
	 */
	@Override
	public void updDxwj(Dxwj dxwj) {
		dxwjMapper.updDxwj(dxwj);// 更新数据
	}

	/**
	 * Dxwj的单记录删除方法
	 * @param id 要删除的主键值
	 */
	@Override
	public void delDxwj(Integer id) {
		dxwjMapper.delDxwj(id);//删除数据
	}

	/**
	 * Dxwj的批量删除方法
	 * @param ids 主键值的数组
	 */
	@Override
	public void delDxwj(Integer[] ids) {
		dxwjMapper.delDxwjs(ids);// 删除数据
	}


	@Autowired
	private HyanswerMapper hyanswerMapper;// 声明模块Mapper实例


	/***************** Hyanswer方法组 *****************/
	/**
	 * Hyanswer的条件查询方法
	 * @param hyanswer 承载查询条件的Bean
	 * @return 返回Hyanswer的集合
	 */
	@Override
	public List<Hyanswer> schHyanswer(Hyanswer hyanswer) {
		PageHelper.startPage(hyanswer.getPage(), hyanswer.getPagesize());//设置分页显示
		List<Hyanswer> list=hyanswerMapper.schHyanswer(hyanswer);
		Page<Hyanswer> page = (Page<Hyanswer>)list;//为了设置总记录数先类型强转
		hyanswer.setDatasize(page.getTotal());// 设置数据长度
		return list;// 返回查询结果
	}
	
	/**
	 * Hyanswer的主键查询方法
	 * @param id 主键值
	 * @return 返回Hyanswer实体
	 */
	@Override
	public Hyanswer schHyanswerById(Integer id) {
		return hyanswerMapper.schHyanswerById(id);// 返回查询结果
	}
	
	/**
	 * Hyanswer的添加方法
	 * @param hyanswer 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	@Override
	public Integer addHyanswer(Hyanswer hyanswer) {
		return hyanswerMapper.addHyanswer(hyanswer);// 返回查询结果
	}

	/**
	 * Hyanswer的修改方法
	 * @param hyanswer 承载数据的Bean
	 */
	@Override
	public void updHyanswer(Hyanswer hyanswer) {
		hyanswerMapper.updHyanswer(hyanswer);// 更新数据
	}

	/**
	 * Hyanswer的单记录删除方法
	 * @param id 要删除的主键值
	 */
	@Override
	public void delHyanswer(Integer id) {
		hyanswerMapper.delHyanswer(id);// 删除数据
	}

	/**
	 * Hyanswer的批量删除方法
	 * @param ids 主键值的数组
	 */
	@Override
	public void delHyanswer(Integer[] ids) {
		hyanswerMapper.delHyanswers(ids);// 删除数据
	}
	
	@Autowired
	private ZdRepositoryMapper repositoryMapper;// 声明模块Mapper实例


	/***************** Repository方法组 *****************/
	/**
	 * Repository的条件查询方法
	 * @param repository 承载查询条件的Bean
	 * @return 返回Repository的集合
	 */
	@Override
	public List<ZdRepository> schRepository(ZdRepository repository) {
		PageHelper.startPage(repository.getPage(), repository.getPagesize());//设置分页显示
		List<ZdRepository> list=repositoryMapper.schRepository(repository);// 查询数据
		Page<ZdRepository> page = (Page<ZdRepository>)list;//为了设置总记录数先类型强转
		repository.setDatasize(page.getTotal());//设置数据长度
		return list;// 返回结果集
	}
	
	/**
	 * Repository的条件查询方法(无分页)
	 * @param repository 承载查询条件的Bean
	 * @return 返回Repository的集合
	 */
	@Override
	public List<ZdRepository> schRepositoryList(ZdRepository repository) {
		List<ZdRepository> list=repositoryMapper.schRepository(repository);// 查询数据
		return list;// 返回结果集
	}
	
	/**
	 * Repository的主键查询方法
	 * @param id 主键值
	 * @return 返回Repository实体
	 */
	@Override
	public ZdRepository schRepositoryById(Integer id) {
		return repositoryMapper.schRepositoryById(id);// 返回结果集
	}
	
	/**
	 * Repository的添加方法
	 * @param repository 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	@Override
	public Integer addRepository(ZdRepository repository) {
		return repositoryMapper.addRepository(repository);// 返回结果集
	}

	/**
	 * Repository的修改方法
	 * @param repository 承载数据的Bean
	 */
	@Override
	public void updRepository(ZdRepository repository) {
		repositoryMapper.updRepository(repository);// 更新数据
	}

	/**
	 * Repository的单记录删除方法
	 * @param id 要删除的主键值
	 */
	@Override
	public void delRepository(Integer id) {
		repositoryMapper.delRepository(id);// 删除数据
	}

	/**
	 * Repository的批量删除方法
	 * @param ids 主键值的数组
	 */
	@Override
	public void delRepository(Integer[] ids) {
		repositoryMapper.delRepositorys(ids);// 删除数据
	}

	/**
	 * 更新学生分数
	 */
	/*@Override
	public List<TrainCamp> updStudCore(List<TrainCamp> jsonStr, Person person, String stuNum) {
		List<TrainCamp> result = new ArrayList<TrainCamp>();// 实例化集合对象
		for (TrainCamp trainCamp : jsonStr) {// 循环
			TrainCamp tc1 = new TrainCamp();// 实例化对象
			tc1.setBookid(trainCamp.getBookid()); // 设置任务id
			tc1.setClassid(trainCamp.getClassid()); // 设置班级id
			tc1.setStudid(trainCamp.getStudid()); // 设置学生id
			if(trainCamp.getId() != null){// 判断id是否为空
				tc1 = trainCampMapper.schTrainCampById(trainCamp.getId());// 根据id查询
			}
			if(tc1 == null){// 判断对象是否为null
				tc1 = new TrainCamp();// 实例化对象
			}
			// 如果数据为空，默认为0
			float total = 0;// 初始化参数
			float total1 = 0;// 初始化参数
			float total2 = 0;// 初始化参数
			float total3 = 0;// 初始化参数
			if(person.getNumber().equals(stuNum) ) {// 判断学号是否相等
				if(trainCamp.getPersonalscore1() != null){//判断值是否为空
					tc1.setPersonalscore1(trainCamp.getPersonalscore1());// 设置自评1
				}else{
					tc1.setPersonalscore1(0);// 设置为0
				}
				if(trainCamp.getPersonalscore2() != null){//判断值是否为空
					tc1.setPersonalscore2(trainCamp.getPersonalscore2());// 设置自评2
				}else{
					tc1.setPersonalscore2(0);// 设置为0
				}
				if(trainCamp.getPersonalscore3() != null){//判断值是否为空
					tc1.setPersonalscore3(trainCamp.getPersonalscore3());// 设置自评3
				}else{
					tc1.setPersonalscore3(0);// 设置为0
				}
			}else{
				if(trainCamp.getPartnerscore1() != null){//判断值是否为空
					tc1.setPartnerscore1(trainCamp.getPartnerscore1());// 设置互评1
				}else{
					tc1.setPartnerscore1(0);// 设置为0
				}
				if(trainCamp.getPartnerscore2() != null){//判断值是否为空
					tc1.setPartnerscore2(trainCamp.getPartnerscore2());// 设置互评2
				}else{
					tc1.setPartnerscore2(0);// 设置为0
				}
				if(trainCamp.getPartnerscore3() != null){//判断值是否为空
					tc1.setPartnerscore3(trainCamp.getPartnerscore3());// 设置互评3
				}else{
					tc1.setPartnerscore3(0);// 设置为0
				}
			}
			// 学生自评分和互评分总和
			TrainCamp param = new TrainCamp();// 实例化对象
			param.setBookid(trainCamp.getBookid()); // 设置任务id
			param.setStudid(trainCamp.getStudid()); // 设置学生id
			TrainCamp entity = trainCampMapper.schTrainCampByBookIdAndQtId(trainCamp);// 查询数据
			if(entity != null){
				Integer teacherscore1 = tc1.getTeacherscore1();// 获取教师评1
				Integer teacherscore2 = tc1.getTeacherscore2();// 获取教师评2
				Integer teacherscore3 = tc1.getTeacherscore3();// 获取教师评3
				teacherscore1 = teacherscore1 == null ? 0 : teacherscore1; // 三目判断教师1是否为null，是null则赋值为0
				teacherscore2 = teacherscore2 == null ? 0 : teacherscore2; // 三目判断教师2是否为null，是null则赋值为0
				teacherscore3 = teacherscore3 == null ? 0 : teacherscore3; // 三目判断教师3是否为null，是null则赋值为0
				total3 = total3 + teacherscore1 + teacherscore2 + teacherscore3;// 教师评总分
			}
			if(entity != null && tc1 != null){// 判断必要参数是否为空
				tc1.setTm(entity.getTm()); // 设置时间
			}
			total1 = (tc1.getPersonalscore1() == null ? 0 : tc1.getPersonalscore1()) + (tc1.getPersonalscore2()==null?0:tc1.getPersonalscore2()) + (tc1.getPersonalscore3()==null?0:tc1.getPersonalscore3());
			total2 = (tc1.getPartnerscore1()==null?0:tc1.getPartnerscore1()) + (tc1.getPartnerscore2()==null?0:tc1.getPartnerscore2()) + (tc1.getPartnerscore3()==null?0:tc1.getPartnerscore3());
			total = total1*(0.3f) + total2*(0.3f) + total3*(0.4f);// 总分除以3
			tc1.setScore(total); // 设置总分
			if(tc1.getId() != null){
				// 更新数据库
				trainCampMapper.updTeacher(tc1);// 更新老师评分
			}else{
				if(entity != null ){
					tc1.setId(entity.getId());// 设置id
					trainCampMapper.updTeacher(tc1);// 更新老师评分
				}else{
					trainCampMapper.addTrainCamp(tc1);// 添加数据
				}
			}
			result.add(tc1);// 添加数据
		}
		return result;// 返回结果集
	}*/


	@Autowired
	private ScoreMapper scoreMapper;


	/***************** Score方法组 *****************/
	/**
	 * Score的条件查询方法
	 * @param score 承载查询条件的Bean
	 * @return 返回Score的集合
	 */
	@Override
	public List<Score> schScore(Score score) {
		PageHelper.startPage(score.getPage(), score.getPagesize());//设置分页显示
		List<Score> list=scoreMapper.schScore(score);
		Page<Score> page = (Page<Score>)list;//为了设置总记录数先类型强转
		score.setDatasize(page.getTotal());
		return list;
	}
	
	/**
	 * Score的主键查询方法
	 * @param id 主键值
	 * @return 返回Score实体
	 */
	@Override
	public Score schScoreById(Integer id) {
		return scoreMapper.schScoreById(id);
	}
	
	/**
	 * Score的添加方法
	 * @param score 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	@Override
	public Integer addScore(Score score) {
		return scoreMapper.addScore(score);
	}

	/**
	 * Score的修改方法
	 * @param score 承载数据的Bean
	 */
	@Override
	public void updScore(Score score) {
		scoreMapper.updScore(score);
	}

	/**
	 * Score的单记录删除方法
	 * @param id 要删除的主键值
	 */
	@Override
	public void delScore(Integer id) {
		scoreMapper.delScore(id);
	}

	/**
	 * Score的批量删除方法
	 * @param ids 主键值的数组
	 */
	@Override
	public void delScore(Integer[] ids) {
		scoreMapper.delScores(ids);
	}

	/**
	 * 查询互评学生 
	 * @param taskId 任务id
	 * @param cnumber 班级
	 * @return 返回结果的集合
	 */
	@Override
	public List<Map<String, Object>> schGroupStudent(Integer taskId,
			String cnumber) {
		return scoreMapper.schGroupStudent(taskId, cnumber);
	}

	/**
	 * 查询学生自评分
	 * @param bookid 任务ID
	 * @param studid 学生ID
	 * @return
	 */
	@Override
	public PersonScoreVo getStudentPersonScore(Integer bookid, Integer studid) {
		ExerTopic topic = examinationService.schExerTopicById(bookid);// 任务
		Integer templateId = topic.getTemplateId(); //模板ID
		Template template = templateService.schTemplateById(templateId);//模板
		Score score=new Score();
		score.setPfrw(bookid);
		score.setMbid(templateId);
		score.setPfxsxh(studid.toString());
		score.setBpfxsxh(studid.toString());
		PersonScoreVo teacherScoreVo = new PersonScoreVo();
		teacherScoreVo.setStuNo(String.valueOf(studid)); //学号
		teacherScoreVo.setStuName(DictBuffer.getLabel("ZD_STUD", studid.toString()));//学生姓名
		ScoreVo scoreVo = getScoreVo(template, bookid, studid, 1,0);//获取评分
		List<Score> list=scoreMapper.schScore(score);		//调用Service查询数据
		if(list!=null&&list.size()>0){
			scoreVo.setYdgjzc(list.get(0).getYdgjzc());
		}
		teacherScoreVo.setScoreVo(scoreVo);//设置评分
		return teacherScoreVo;//返回
	}

	/**
	 * 获取评分
	 * @param template 模板
	 * @param bookid 任务ID
	 * @param studid 学生ID
	 * @param pflx 评分类型 1自评  2小组评 3师评
	 * @param xzpfrid 小组评分人ID 仅针对小组评分，自评和教师评分可以设置为任意值
	 * @return
	 */
	private ScoreVo getScoreVo(Template template,Integer bookid, Integer studid,int pflx,Integer xzpfrid){
		//查询明细
		Score scoreQuery = new Score();
		scoreQuery.setBpfxsxh(String.valueOf(studid));//设置被评分人ID
		scoreQuery.setPfrw(bookid);//设置评分任务
		scoreQuery.setPflx(pflx);//设置评分类型
		scoreQuery.setMbid(template.getId());//设置模板ID
		List<Score> scoreList = scoreMapper.schScore(scoreQuery);//获取评分
		
		List<org.json.simple.JSONObject> targetScoreList = new ArrayList<org.json.simple.JSONObject>();//获取评分
		ScoreVo scoreVo = new ScoreVo();
		if(scoreList != null && scoreList.size() > 0) {
			if(pflx == 2) { //如果是小组评分
				for(Score score :scoreList){
					targetScoreList.add(score.getJsonInDict());
				}
				scoreVo.setMemberScores(targetScoreList); //设置小组评分列表
				if(xzpfrid != null && xzpfrid > 0) {//查询指定小组评分人的评分
					scoreQuery = getScoreByPfrid(scoreList, xzpfrid);
				}else {
					scoreQuery = scoreList.get(0);//默认取第一条记录查看评分明细
					scoreVo.setPfxsxh(scoreList.get(0).getPfxsxh());
				}
			}else {//非小组评分
				scoreQuery = scoreList.get(0);//默认取第一条记录查看评分明细
			}
		}
		scoreVo.setTotalScore(scoreQuery.getZf());//总分

		
		//查询学生各总分
		TrainCamp trainCamp = new TrainCamp();
		trainCamp.setBookid(bookid);
		trainCamp.setStudid(studid);
		List<TrainCamp> trainCampList = trainCampMapper.schTrainCamp(trainCamp);
		if(trainCampList != null && trainCampList.size() > 0) {
			trainCamp = trainCampList.get(0);
		}
		if(pflx == 1) {//自评分
			scoreVo.setFinalScore(trainCamp.getPersonalscore1());//设置自评分
			scoreVo.setRatio(template.getTotalScoreRatio().split(":")[0]);//设置占比
		}else if(pflx == 2) {//小组评分
			scoreVo.setFinalScore(trainCamp.getPartnerscore1());//设置小组评分
			scoreVo.setRatio(template.getTotalScoreRatio().split(":")[1]);//设置占比
		}else if(pflx == 3) {//教师评分
			scoreVo.setFinalScore(trainCamp.getTeacherscore1());//设置教师评分
			scoreVo.setRatio(template.getTotalScoreRatio().split(":")[2]);//设置占比
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		scoreVo.setScoreDate(sdf.format(scoreQuery.getPfrq()));//评分日期
		
		String pf = scoreQuery.getPfjg();//评分结果
		JSONArray jsonArray = JSONArray.parseArray(pf);
		List<ScoreDetailVo> scoreDetails = new ArrayList<ScoreDetailVo>();
		if(jsonArray != null && jsonArray.size() > 0) {
			for(int i =0 ;i < jsonArray.size(); i++) {
				ScoreDetailVo scoreDetailVo = new ScoreDetailVo();
				
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				Integer feilieId = jsonObject.getInteger("classify");//解析classify
				TemplateCategory tc = templateService.schTemplateCategoryById(feilieId);
				if(tc != null) {
					scoreDetailVo.setCategory(tc.getClassificationName());//设置分类
					scoreDetailVo.setRatio(tc.getCalculationProportion()); //设置比例
					
					JSONArray indexs = jsonObject.getJSONArray("target");//解析target
					JSONArray scores = jsonObject.getJSONArray("score"); //解析分数
					List<IndexScoreVo> indexScores = new ArrayList<IndexScoreVo>();
					if(indexs != null && indexs.size() > 0) {
						for(int k =0 ; k < indexs.size();k++) {
							Integer index =  Integer.parseInt((String) indexs.get(k)); //指标序号
							String score = (String)scores.get(k); //分数
							String indexName = indexName(index, tc.getIndexContent());//指标名称
							IndexScoreVo indexScoreVo = new IndexScoreVo(); //指标分数
							indexScoreVo.setIndexContent(indexName);//设置指标
							indexScoreVo.setScore(score);//设置分数
							indexScores.add(indexScoreVo);
						}
					}
					scoreDetailVo.setIndexScores(indexScores);//设置指标分数集合
				}
				scoreDetails.add(scoreDetailVo);//追加评分明细
			}
		}
		scoreVo.setScoreDetails(scoreDetails);//设置评分明细
		return scoreVo;
	}
	
	/**
	 * 查询评分人的评分
	 * @param scoreList 评分人列表
	 * @param pfrid 评分人ID
	 * @return 评分对象
	 */
	private Score getScoreByPfrid(List<Score> scoreList,Integer pfrid){
		if(scoreList == null || scoreList.size() == 0) {
			return null;
		}
		for(Score score :scoreList){
			if(score.getPfxsxh().equals(String.valueOf(pfrid))){
				return score;
			}
		}
		return null;
	}
	
	
	/**
	 * 获取指标名称
	 * @param serialNumber 序号
	 * @param indexContent 指标内容
	 * @return
	 */
	private String indexName(Integer serialNumber,String indexContents){
		if(indexContents != null && !indexContents.equals("") && serialNumber!=null && serialNumber >0) {
			List<TemplateIndexDTO> templateIndexDTOs = JSONArray.parseArray(indexContents,TemplateIndexDTO.class);
			if(templateIndexDTOs != null && templateIndexDTOs.size() > 0) {
				for(TemplateIndexDTO templateIndexDTO:templateIndexDTOs) {
					if(templateIndexDTO.getSerialNumber() == serialNumber){
						return templateIndexDTO.getIndexContent();
					}
				}
			}
		}
		return "";
	}

	
	/**
	 * 查询教师评分
	 * @param bookid 任务ID
	 * @param studid 学生ID
	 * @param teacherId 教师ID
	 * @return
	 */
	@Override
	public TeacherScoreVo getTeacherScore(Integer bookid, Integer studid,String teacherId) {
		ExerTopic topic = examinationService.schExerTopicById(bookid);// 任务
		Integer templateId = topic.getTemplateId(); //模板ID
		Template template = templateService.schTemplateById(templateId);//模板
		Score score=new Score();
		score.setPfrw(bookid);
		score.setMbid(templateId);
		score.setPfxsxh(teacherId);
		score.setBpfxsxh(studid.toString());
		TeacherScoreVo teacherScoreVo = new TeacherScoreVo();
		teacherScoreVo.setTeacherName(DictBuffer.getLabel("ZD_USER", teacherId.toString()));//教师姓名
		List<Score> list=scoreMapper.schScore(score);		//调用Service查询数据
		ScoreVo scoreVo = getScoreVo(template, bookid, studid, 3,0);//获取评分
		if(list!=null&&list.size()>0){
			scoreVo.setYdgjzc(list.get(0).getYdgjzc());
		}
		teacherScoreVo.setScoreVo(scoreVo);//设置评分
		return teacherScoreVo;//返回
	}

	/**
	 * 查询小组评分
	 * @param bookid 任务ID
	 * @param studid 被评分学生ID
	 * @param xzpfrid 小组评分人ID
	 * @return
	 */
	@Override
	public GroupScoreVo getGroupScore(Integer bookid, Integer studid,Integer xzpfrid) {
		ExerTopic topic = examinationService.schExerTopicById(bookid);// 任务
		Integer templateId = topic.getTemplateId(); //模板ID
		Template template = templateService.schTemplateById(templateId);//模板
		Score score=new Score();
		score.setPfrw(bookid);
		score.setMbid(templateId);
		score.setBpfxsxh(studid.toString());
		GroupScoreVo gScoreVo = new GroupScoreVo();
		//设置评分明细
		ScoreVo scoreVo = getScoreVo(template, bookid, studid, 2,xzpfrid);//获取评分
		if(xzpfrid!=null&&xzpfrid>0){
			score.setPfxsxh(xzpfrid.toString());
		}else{
			score.setPfxsxh(scoreVo.getPfxsxh());
		}
		List<Score> list=scoreMapper.schScore(score);		//调用Service查询数据
		if(list!=null&&list.size()>0){
			scoreVo.setYdgjzc(list.get(0).getYdgjzc());
		}
		gScoreVo.setScoreVo(scoreVo);//设置评分
		return gScoreVo;//返回
	}

}//end
