package com.qtrmoon.camp.serdao;

import java.util.List;
import org.springframework.stereotype.Component;

import com.qtrmoon.camp.pojo.Hytopic;

/** 实体映射 */
@Component
public interface HytopicMapper {
	
	/**
	 * Hytopic的条件查询方法
	 * @param hytopic 承载查询条件
	 * @return 返回Hytopic的集合
	 */
	public List<Hytopic> schHytopic(Hytopic hytopic);
	
	/**
	 * Hytopic的主键查询方法
	 * @param id 主键值
	 * @return 返回Hytopic实体
	 */
	public Hytopic schHytopicById(Integer id);
	
	/**
	 * Hytopic的添加方法
	 * @param hytopic 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addHytopic(Hytopic hytopic);
	
	/**
	 * Hytopic的修改方法
	 * @param hytopic 承载数据的Bean
	 */
	public void updHytopic(Hytopic hytopic);
	
	/**
	 * Hytopic的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delHytopic(Integer id);
	
	/**
	 * Hytopic的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delHytopics(Integer[] ids);
}
