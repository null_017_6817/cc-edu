package com.qtrmoon.camp.serdao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.qtrmoon.camp.pojo.ZdRepository;
/** 实体映射 */
@Component
public interface ZdRepositoryMapper {
	
	/**
	 * Repository的条件查询方法
	 * @param repository 承载查询条件
	 * @return 返回Repository的集合
	 */
	public List<ZdRepository> schRepository(ZdRepository repository);
	
	/**
	 * Repository的主键查询方法
	 * @param id 主键值
	 * @return 返回Repository实体
	 */
	public ZdRepository schRepositoryById(Integer id);
	
	/**
	 * Repository的添加方法
	 * @param repository 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addRepository(ZdRepository repository);
	
	/**
	 * Repository的修改方法
	 * @param repository 承载数据的Bean
	 */
	public void updRepository(ZdRepository repository);
	
	/**
	 * Repository的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delRepository(Integer id);
	
	/**
	 * Repository的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delRepositorys(Integer[] ids);

	/**
	 * 查询列表
	 * @return
	 */
	public ZdRepository schRootRepository();

	/**
	 * 根据父id查询列表
	 * @param id
	 * @return
	 */
	public List<ZdRepository> schRepositoryByPId(Integer id);
}
