package com.qtrmoon.camp.serdao;
import java.util.List;
import java.util.Map;

import com.qtrmoon.camp.pojo.Dxwj;
import com.qtrmoon.camp.pojo.Hyanswer;
import com.qtrmoon.camp.pojo.Hytopic;
import com.qtrmoon.camp.pojo.Score;
import com.qtrmoon.camp.pojo.TrainCamp;
import com.qtrmoon.camp.pojo.ZdRepository;
import com.qtrmoon.camp.vo.GroupScoreVo;
import com.qtrmoon.camp.vo.PersonScoreVo;
import com.qtrmoon.camp.vo.TeacherScoreVo;
import com.qtrmoon.manager.pojo.Classes;

/**
 * 评分service接口
 */
public interface ICampService {
	
	/***************** TrainCamp方法组 *****************/
	/**
	 * TrainCamp的条件查询方法
	 * @param trainCamp 承载查询条件的Bean
	 * @return 返回TrainCamp的集合
	 */
	public List<TrainCamp> schTrainCamp(TrainCamp trainCamp);
	
	/**
	 * TrainCamp的主键查询方法
	 * @param id 主键值
	 * @return 返回TrainCamp实体
	 */
	public TrainCamp schTrainCampById(Integer id);
	
	/**
	 * TrainCamp的添加方法
	 * @param trainCamp 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addTrainCamp(TrainCamp trainCamp);
	
	/**
	 * TrainCamp的修改方法
	 * @param trainCamp 承载数据的Bean
	 */
	public void updTrainCamp(TrainCamp trainCamp);
	
	/**
	 * TrainCamp的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delTrainCamp(Integer id);
	
	/**
	 * TrainCamp的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delTrainCamp(Integer[] ids);
	
	/**
	 * TrainCamp的Bookid和其他id查询方法
	 * @param trainCamp 承载数据的Bean
	 * @return
	 */
	public TrainCamp schTrainCampByBookIdAndQtId(TrainCamp trainCamp);

	/**
	 * 通过classid查询页面索要显示的字段
	 * @param classid 班级的id
	 */
	public List<TrainCamp> schTeacherEvaluate(Map<String,Object> param);

	/**
	 * 查询所有的班级
	 */
	public List<Classes> schClasses();

	/**
	 * 数据修改后通过学生id查询TrainCamp的原有属性值
	 * @param studid学生id
	 */
	public TrainCamp schTrainCampBystudid(Map<String,Object> param);

	/**
	 * 教师评价分数更改后修改TrainCamp的属性值
	 * @param TrainCamp
	 */
	public void updTeacher(TrainCamp trainCamp);
	
	/**
	 * 教师评分（保存数据代码转移到service，待定）
	 * @return
	 */
	/*public List<TrainCamp> addByTeacer(List<TrainCamp> jsonStr,Person person);*/

	/**
	 * 更新评分
	 * @param trainCamp
	 */
	public void updTrainCampDyna(TrainCamp trainCamp);

	/**
	 * 添加评分
	 * @param trainCamp
	 */
	/*public void addTrainCampAndPart(TrainCamp trainCamp);*/

	/**
	 * 更新评分
	 * @param trainCamp
	 */
	/*public void updTrainCampDynaAndPart(TrainCamp trainCamp);*/

	/**
	 * 根据权重计算分数
	 * @param trainCamp
	 */
	/*public void weightscore(TrainCamp trainCamp);*/

	/**
	 * 查询该学生有录音的个数
	 */
	public Integer schCount(TrainCamp item);
	
	/**
	 * 更新自评分
	 * @param trainCamp
	 */
	public void updTrainCampPersonalScore(TrainCamp trainCamp);
	
	/**
	 * 更新小组评分
	 * @param trainCamp
	 */
	public void updTrainCampPartnerScore(TrainCamp trainCamp);
	
	/**
	 * 更新老师评分
	 * @param trainCamp
	 */
	public void updTrainCampTeacherScore(TrainCamp trainCamp);
	
	/**
	 * 更新总分
	 * @param trainCamp
	 */
	public void updTrainCampScore(TrainCamp trainCamp);

	/***************** Hytopic方法组 *****************/
	/**
	 * Hytopic的条件查询方法
	 * @param hytopic 承载查询条件的Bean
	 * @return 返回Hytopic的集合
	 */
	public List<Hytopic> schHytopic(Hytopic hytopic);
	
	/**
	 * Hytopic的条件查询方法(无分页)
	 * @param hytopic 承载查询条件的Bean
	 * @return 返回Hytopic的集合
	 */
	public List<Hytopic> schHytopicList(Hytopic hytopic);
	
	/**
	 * Hytopic的主键查询方法
	 * @param id 主键值
	 * @return 返回Hytopic实体
	 */
	public Hytopic schHytopicById(Integer id);
	
	/**
	 * Hytopic的添加方法
	 * @param hytopic 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addHytopic(Hytopic hytopic);
	
	/**
	 * Hytopic的修改方法
	 * @param hytopic 承载数据的Bean
	 */
	public void updHytopic(Hytopic hytopic);
	
	/**
	 * Hytopic的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delHytopic(Integer id);
	
	/**
	 * Hytopic的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delHytopic(Integer[] ids);


	
	/***************** Dxwj方法组 *****************/
	/**
	 * Dxwj的条件查询方法
	 * @param dxwj 承载查询条件的Bean
	 * @return 返回Dxwj的集合
	 */
	public List<Dxwj> schDxwj(Dxwj dxwj);
	
	/**
	 * Dxwj的主键查询方法
	 * @param id 主键值
	 * @return 返回Dxwj实体
	 */
	public Dxwj schDxwjById(Integer id);
	
	/**
	 * Dxwj的添加方法
	 * @param dxwj 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addDxwj(Dxwj dxwj);
	
	/**
	 * Dxwj的修改方法
	 * @param dxwj 承载数据的Bean
	 */
	public void updDxwj(Dxwj dxwj);
	
	/**
	 * Dxwj的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delDxwj(Integer id);
	
	/**
	 * Dxwj的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delDxwj(Integer[] ids);
	
	/***************** Hyanswer方法组 *****************/
	/**
	 * Hyanswer的条件查询方法
	 * @param hyanswer 承载查询条件的Bean
	 * @return 返回Hyanswer的集合
	 */
	public List<Hyanswer> schHyanswer(Hyanswer hyanswer);
	
	/**
	 * Hyanswer的主键查询方法
	 * @param id 主键值
	 * @return 返回Hyanswer实体
	 */
	public Hyanswer schHyanswerById(Integer id);
	
	/**
	 * Hyanswer的添加方法
	 * @param hyanswer 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addHyanswer(Hyanswer hyanswer);
	
	/**
	 * Hyanswer的修改方法
	 * @param hyanswer 承载数据的Bean
	 */
	public void updHyanswer(Hyanswer hyanswer);
	
	/**
	 * Hyanswer的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delHyanswer(Integer id);
	
	/**
	 * Hyanswer的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delHyanswer(Integer[] ids);

	
	/***************** Repository方法组 *****************/
	/**
	 * Repository的条件查询方法
	 * @param repository 承载查询条件的Bean
	 * @return 返回Repository的集合
	 */
	public List<ZdRepository> schRepository(ZdRepository repository);
	
	/**
	 * Repository的条件查询方法(无分页)
	 * @param repository 承载查询条件的Bean
	 * @return 返回Repository的集合
	 */
	public List<ZdRepository> schRepositoryList(ZdRepository repository);
	
	/**
	 * Repository的主键查询方法
	 * @param id 主键值
	 * @return 返回Repository实体
	 */
	public ZdRepository schRepositoryById(Integer id);
	
	/**
	 * Repository的添加方法
	 * @param repository 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addRepository(ZdRepository repository);
	
	/**
	 * Repository的修改方法
	 * @param repository 承载数据的Bean
	 */
	public void updRepository(ZdRepository repository);
	
	/**
	 * Repository的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delRepository(Integer id);
	
	/**
	 * Repository的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delRepository(Integer[] ids);

	/**
	 * 查询列表
	 * @param param
	 * @return
	 */
	public List<TrainCamp> schStudentEvaluate(Map<String, Object> param);

	/**
	 * 更新学生分数
	 * @param jsonStr
	 * @param person
	 * @param stuNum
	 * @return
	 */
	/*public List<TrainCamp> updStudCore(List<TrainCamp> jsonStr, Person person,String stuNum);*/


	
	/***************** Score方法组 *****************/
	/**
	 * Score的条件查询方法
	 * @param score 承载查询条件的Bean
	 * @return 返回Score的集合
	 */
	public List<Score> schScore(Score score);
	
	/**
	 * Score的主键查询方法
	 * @param id 主键值
	 * @return 返回Score实体
	 */
	public Score schScoreById(Integer id);
	
	/**
	 * Score的添加方法
	 * @param score 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addScore(Score score);
	
	/**
	 * Score的修改方法
	 * @param score 承载数据的Bean
	 */
	public void updScore(Score score);
	
	/**
	 * Score的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delScore(Integer id);
	
	/**
	 * Score的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delScore(Integer[] ids);
	
	/**
	 * 查询互评学生 
	 * @param taskId 任务id
	 * @param cnumber 班级
	 * @return 返回结果的集合
	 */
	public List<Map<String,Object>> schGroupStudent(Integer taskId,String cnumber);

	/**
	 * 查询学生自评分
	 * @param bookid 任务ID
	 * @param studid 学生ID
	 * @return
	 */
	public PersonScoreVo getStudentPersonScore(Integer bookid, Integer studid);
	
	/**
	 * 查询教师评分
	 * @param bookid 任务ID
	 * @param studid 学生ID
	 * @param teacherId 教师ID
	 * @return
	 */
	public TeacherScoreVo getTeacherScore(Integer bookid, Integer studid,String teacherId);
	
	
	/**
	 * 查询小组评分
	 * @param bookid 任务ID
	 * @param studid 被评分学生ID
	 * @param xzpfrid 小组评分人ID
	 * @return
	 */
	public GroupScoreVo getGroupScore(Integer bookid, Integer studid,Integer xzpfrid);



}//end
