package com.qtrmoon.camp.serdao;

import java.util.List;
import org.springframework.stereotype.Component;

import com.qtrmoon.camp.pojo.Dxwj;

/** 电信问卷实体映射 */
@Component
public interface DxwjMapper {
	
	/**
	 * Dxwj的条件查询方法
	 * @param dxwj 承载查询条件
	 * @return 返回Dxwj的集合
	 */
	public List<Dxwj> schDxwj(Dxwj dxwj);
	
	/**
	 * Dxwj的主键查询方法
	 * @param id 主键值
	 * @return 返回Dxwj实体
	 */
	public Dxwj schDxwjById(Integer id);
	
	/**
	 * Dxwj的添加方法
	 * @param dxwj 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addDxwj(Dxwj dxwj);
	
	/**
	 * Dxwj的修改方法
	 * @param dxwj 承载数据的Bean
	 */
	public void updDxwj(Dxwj dxwj);
	
	/**
	 * Dxwj的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delDxwj(Integer id);
	
	/**
	 * Dxwj的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delDxwjs(Integer[] ids);
}
