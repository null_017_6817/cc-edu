package com.qtrmoon.camp.serdao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.qtrmoon.camp.pojo.TrainCamp;
import com.qtrmoon.manager.pojo.Classes;

/**
 * mapper接口
 */
@Component
public interface TrainCampMapper {
	
	/**
	 * TrainCamp的条件查询方法
	 * @param trainCamp 承载查询条件
	 * @return 返回TrainCamp的集合
	 */
	public List<TrainCamp> schTrainCamp(TrainCamp trainCamp);
	
	/**
	 * TrainCamp的主键查询方法
	 * @param id 主键值
	 * @return 返回TrainCamp实体
	 */
	public TrainCamp schTrainCampById(Integer id);
	
	/**
	 * TrainCamp的添加方法
	 * @param trainCamp 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addTrainCamp(TrainCamp trainCamp);
	
	/**
	 * TrainCamp的修改方法
	 * @param trainCamp 承载数据的Bean
	 */
	public void updTrainCamp(TrainCamp trainCamp);
	
	/**
	 * TrainCamp的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delTrainCamp(Integer id);
	
	/**
	 * TrainCamp的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delTrainCamps(Integer[] ids);

	/**
	 * 通过classid查询页面索要显示的字段
	 * @param classid 班级的id
	 */
	public List<TrainCamp> schTeacherEvaluate(Map<String,Object> param);

	/**
	 * 查询所有的班级
	 */
	public List<Classes> schClasses();


	/**
	 * 数据修改后通过学生id查询TrainCamp的原有属性值
	 * @param studid学生id
	 */
	public TrainCamp schTrainCampBystudid(Map<String,Object> param);

	/**
	 * 教师评价分数更改后修改TrainCamp的属性值
	 * @param TrainCamp
	 */
	public void updTeacher(TrainCamp trainCamp);
	
	/**
	 * TrainCamp的学生Bookid和其他id查询方法
	 * @param trainCamp 承载数据的Bean
	 * @return
	 */
	public TrainCamp schTrainCampByBookIdAndQtId(TrainCamp trainCamp);

	/**
	 * 更新评分根据id
	 * @param trainCamp
	 */
	public void updTrainCampByIdDyna(TrainCamp trainCamp);

	/**
	 * 更新评分
	 * @param trainCamp
	 */
	public void updTrainCampDyna(TrainCamp trainCamp);

	/**
	 * 查询该学生有录音的个数
	 */
	public Integer schCount(TrainCamp item);

	/**
	 * 查询评分
	 * @param param
	 * @return
	 */
	public List<TrainCamp> schStudentEvaluate(Map<String, Object> param);
	
	
	/**
	 * 更新自评分
	 * @param trainCamp
	 */
	public void updTrainCampPersonalScore(TrainCamp trainCamp);
	
	/**
	 * 更新小组评分
	 * @param trainCamp
	 */
	public void updTrainCampPartnerScore(TrainCamp trainCamp);
	
	/**
	 * 更新老师评分
	 * @param trainCamp
	 */
	public void updTrainCampTeacherScore(TrainCamp trainCamp);
	
	/**
	 * 更新总分
	 * @param trainCamp
	 */
	public void updTrainCampScore(TrainCamp trainCamp);
}
