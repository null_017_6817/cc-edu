package com.qtrmoon.camp.serdao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import com.qtrmoon.camp.pojo.Score;
/** 评分表实体映射 */
@Component
public interface ScoreMapper {
	/**
	 * Score的条件查询方法
	 * @param score 承载查询条件
	 * @return 返回Score的集合
	 */
	public List<Score> schScore(Score score);
	
	/**
	 * Score的主键查询方法
	 * @param id 主键值
	 * @return 返回Score实体
	 */
	public Score schScoreById(Integer id);
	
	/**
	 * Score的添加方法
	 * @param score 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addScore(Score score);
	
	/**
	 * Score的修改方法
	 * @param score 承载数据的Bean
	 */
	public void updScore(Score score);
	
	/**
	 * Score的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delScore(Integer id);
	
	/**
	 * Score的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delScores(Integer[] ids);
	
	/**
	 * 查询互评学生 
	 * @param taskId 任务id
	 * @param cnumber 班级
	 * @return 返回结果的集合
	 */
	public List<Map<String,Object>> schGroupStudent(@Param("taskId")Integer taskId,@Param("cnumber")String cnumber);
}
