package com.qtrmoon.camp.serdao;

import java.util.List;
import org.springframework.stereotype.Component;

import com.qtrmoon.camp.pojo.Hyanswer;

/** 实体映射 */
@Component
public interface HyanswerMapper {
	
	/**
	 * Hyanswer的条件查询方法
	 * @param hyanswer 承载查询条件
	 * @return 返回Hyanswer的集合
	 */
	public List<Hyanswer> schHyanswer(Hyanswer hyanswer);
	
	/**
	 * Hyanswer的主键查询方法
	 * @param id 主键值
	 * @return 返回Hyanswer实体
	 */
	public Hyanswer schHyanswerById(Integer id);
	
	/**
	 * Hyanswer的添加方法
	 * @param hyanswer 承载数据的Bean，执行添加后会更新入库后的主键值
	 * @return 返回影响的行数
	 */
	public Integer addHyanswer(Hyanswer hyanswer);
	
	/**
	 * Hyanswer的修改方法
	 * @param hyanswer 承载数据的Bean
	 */
	public void updHyanswer(Hyanswer hyanswer);
	
	/**
	 * Hyanswer的单记录删除方法
	 * @param id 要删除的主键值
	 */
	public void delHyanswer(Integer id);
	
	/**
	 * Hyanswer的批量删除方法
	 * @param ids 主键值的数组
	 */
	public void delHyanswers(Integer[] ids);
}
