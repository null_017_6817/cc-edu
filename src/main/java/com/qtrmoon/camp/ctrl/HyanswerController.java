package com.qtrmoon.camp.ctrl;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qtrmoon.common.BaseController;
import com.qtrmoon.camp.pojo.Hyanswer;
import com.qtrmoon.camp.serdao.ICampService;
import com.qtrmoon.manager.pojo.Person;
import com.qtrmoon.sysmanage.SysConstant;

/**
 * 行业答案控制器
 */
@Controller
@RequestMapping("/camp/hyanswer")
public class HyanswerController extends BaseController {
	
	@Autowired
	private ICampService campService; // 声明模块Service实例

	/**
	 * 查询方法，自带分页设置。
	 * @param hyanswer
	 * 承载查询条件的Bean
	 * @param request
	 * 可用来获取非Bean内的参数
	 * @param response
	 * 返回请求,输出Json
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schHyanswer.action")
	public void schHyanswer(Hyanswer hyanswer, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		noCache(response);// 设置ajax无缓存
		hyanswer.setCondition(""); // 防sql注入
		hyanswer.assLike();// 拼接%
		List<Hyanswer> list = campService.schHyanswer(hyanswer); // 调用Service查询数据
		/* 将数据Bean的集合转换为Json集合 */
		JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
		JSONObject obj; // 声明用于构造Bean的Json对象
		for (Hyanswer u : list) { // 循环查询的数据集构造Json数据集
			obj = u.getJsonInDict(); // 将Bean转换为Json对象
			obj.put("_oper", u.getId()); // 为"操作列"设定主键值
			datas.add(obj); // 添加Json对象到Json集合
		}
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		JSONObject res = new JSONObject(); // 声明页面返回Json
		res.put("total", hyanswer.getDatasize()); // 设置数据总记录数
		res.put("rows", datas); // 设置数据集
		response.getWriter().print(res.toJSONString()); // 输出到页面
	}

	/**
	 * 预添加or预修改or查看方法
	 * @param hyanswer
	 * 预添加时可传入一些初始值；预修改和查看时用来承载主键值
	 * @param toL
	 * 挂字典的字段是否需要转为字典Label值。true则进行转换
	 * @param response
	 * 返回请求,输出Json
	 * @throws IOException
	 */
	@RequestMapping(value = "/vieHyanswer.action")
	public void vieHyanswer(Hyanswer hyanswer, Boolean toL,
			HttpServletResponse response) throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (toL != null && toL) { // 查看页面加载数据时调用，返回的对象已进行字典转换
			if (hyanswer.getId() != null && hyanswer.getId() != 0) { // 检测主键非空
				hyanswer = campService.schHyanswerById(hyanswer.getId()); // 按主键查询数据
				response.getWriter().print(hyanswer.getJsonInDict()); // 输出经过字典转换Json对象到查看页(vie_)
			}
		} else { // 修改页面加载数据时调用
			if (hyanswer.getId() != null && hyanswer.getId() != 0) { // 检查主键非空
				hyanswer = campService.schHyanswerById(hyanswer.getId()); // 按主键查询数据
				response.getWriter().print(hyanswer.getJson()); // 输出Json对象到修改页(upd_)
			} else { // 添加页面加载数据时调用，可设置添加页面中一些属性的预设值。
				response.getWriter().print(new Hyanswer().getJson()); // 输出Json对象到添加页(upd_)
			}
		}
	}

	/**
	 * 添加或修改的提交方法
	 * @param hyanswer
	 * 数据表单Bean
	 * @param response
	 * 返回请求
	 * @throws IOException
	 */
	@RequestMapping(value = "/updHyanswer.action")
	public void updHyanswer(Hyanswer hyanswer, HttpServletResponse response)
			throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (hyanswer.getId() == null) { // 检测主键为空则
			campService.addHyanswer(hyanswer); // 添加数据
		} else { // 主键非空则
			campService.updHyanswer(hyanswer); // 修改数据
		}
	}

	/**
	 * 保存问卷
	 * @param hyanswer
	 * @param request
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value = "/saveAnswer.action")
	public Map<String, Object> saveAnswer(Hyanswer hyanswer,
			HttpServletRequest request) {
		Map<String, Object> result = new HashMap<String, Object>();// 实例化返回结果及
		Person person = (Person) request.getSession(true).getAttribute(//获取当前用户
				SysConstant.CURRENT_USER);//获取当前用户
		try {
			
			if (person != null) {// 当前用户不为null的场合
				if (hyanswer != null) {// 必要参数不为null的场合
					Hyanswer hysch = new Hyanswer();
					hysch.setTaskid(hyanswer.getTaskid());
					hysch.setFromno(hyanswer.getFromno());
					List<Hyanswer> hws = campService.schHyanswer(hysch);
					if(hws != null && hws.size() > 0){
						Hyanswer hy = hws.get(0);
						if(hy != null){
							hy.setContent(hyanswer.getContent());
							campService.updHyanswer(hy);
						}
					}else{
						hyanswer.setFromno(person.getNumber());// 设置拨打人学号
						hyanswer.setFromagentid(person.getAgentId());// 设置拨打人坐席号
						Date now = new Date();// 获取系统时间
						hyanswer.setCreatetime(now);// 设置创建时间
						hyanswer.setUpdatetime(now);// 设置更新时间
						campService.addHyanswer(hyanswer);// 添加数据
					}
					result.put("success", true);// 设置成功状态
					result.put("msg", "操作成功");// 设置提示信息
				} else {
					result.put("success", false);// 设置成功状态
					result.put("msg", "必要参数为空");// 设置提示信息
				}
			} else {
				result.put("success", false);// 设置成功状态
				result.put("msg", "登录超时，请重新登录");// 设置提示信息
			}
		} catch (Exception e) {
			e.printStackTrace();// 打印异常信息
			result.put("success", false);// 设置成功状态
			result.put("msg", "服务器异常");// 设置提示信息
		} finally {
			return result;// 返回结果集
		}
	}

	/**
	 * 删除方法
	 * @param ids
	 * 待删除数据的主键数组
	 * @param response
	 * 返回请求
	 */
	@RequestMapping(value = "/delHyanswer.action")
	public void delHyanswer(@RequestParam(value = "ids[]") Integer[] ids,
			HttpServletResponse response) {
		noCache(response); // Ajax方法设定无缓存
		campService.delHyanswer(ids); // 删除数据
	}

	/**
	 * JSP页面跳转
	 * @param page
	 * jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value = "/page.action")
	public String goPage(String page) { // 放过本Ctrl的页面，不合法的跳转到error页。
		if ("index".equals(page) || "hyanswer_sch".equals(page)
				|| "hyanswer_vie".equals(page) || "hyanswer_upd".equals(page)) {
			return "/camp/" + page;//跳转页面
		} else {
			return "/error";// 跳转错误页面
		}
	}
}
