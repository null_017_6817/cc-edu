package com.qtrmoon.camp.ctrl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.poifs.filesystem.DirectoryEntry;
import org.apache.poi.poifs.filesystem.DocumentEntry;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 导出word
 */
@Controller
@RequestMapping("/export")
public class ExportWordController {
	
	/**
	 * 导出word
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings({ "rawtypes", "unused" })
	@RequestMapping("/exportword.action")
	public void exportWord(HttpServletResponse response) throws IOException{
		response.setCharacterEncoding("utf-8");
		ArrayList records = new ArrayList<>();// 获取数据库数据
		if (null != records && 0 != records.size()) {
			// html拼接出word内容
			String content = "<html>";
			for (int i = 0; i < records.size(); i++) {
				// 从数据库中获得数据，将oracle中的clob数据类型转换成string类型
				// html拼接出word内容
				content += "<div style=\"text-align: center\"><span style=\"font-size: 24px\"><span style=\"font-family: 黑体\">"
						+ "你好啊" + "<br/> <br/> </span></span></div>";
				content += "<div style=\"text-align: left\"><span >" + "你不好"
						+ "<br/> <br/> </span></span></div>";
				// 插入分页符
				content += "<span lang=EN-US style='font-size:12.0pt;line-height:150%;mso-fareast-font-family:宋体;mso-font-kerning:1.0pt;mso-ansi-language:EN-US;mso-fareast-language:ZH-CN;mso-bidi-language:AR-SA'><br clear=all style='page-break-before:always'></span>";
				content += "<p class=MsoNormal style='line-height:150%'><span lang=EN-US style='font-size:12.0pt;line-height:150%'><o:p> </o:p></span></p>";

			}
			content += "</html>";// html拼接出word内容
			byte b[] = content.getBytes("utf-8");// 设置编码格式为utf-8
			ByteArrayInputStream bais = new ByteArrayInputStream(b);// 实例化数据流
			POIFSFileSystem poifs = new POIFSFileSystem();// 实例化POI流
			DirectoryEntry directory = poifs.getRoot();// 获取根
			DocumentEntry documentEntry = directory.createDocument("WordDocument", bais);// 实例化对象设置为word格式
			// 输出文件
			String name = "导出知识";
			response.reset();// response清空
			response.setHeader("Content-Disposition", "attachment;filename="// 设置输出属性
					+ new String((name + ".doc").getBytes("utf-8"), "utf-8"));// 设置输出属性
			response.setContentType("application/msword");// 设置输出属性
			OutputStream ostream = response.getOutputStream();// 实例化输出流
			poifs.writeFilesystem(ostream);// 输出文件的话，new一个文件流
			ostream.flush();// 刷新流
			ostream.close();// 关闭流
			bais.close();// 关闭流
		}
	}
}
