package com.qtrmoon.camp.ctrl;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qtrmoon.camp.pojo.Dxwj;
import com.qtrmoon.camp.pojo.Hyanswer;
import com.qtrmoon.camp.serdao.ICampService;
import com.qtrmoon.common.BaseController;
import com.qtrmoon.manager.pojo.Person;
import com.qtrmoon.sysmanage.SysConstant;

/**
 * 电信问卷控制器
 */
@Controller
@RequestMapping("/camp/dxwj")
public class DxwjController extends BaseController {
	
	@Autowired
	private ICampService campService; // 声明模块Service实例

	/**
	 * 查询方法，自带分页设置。
	 * @param dxwj
	 * 承载查询条件的Bean
	 * @param request
	 * 可用来获取非Bean内的参数
	 * @param response
	 * 返回请求,输出Json
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schDxwj.action")
	public void schDxwj(Dxwj dxwj, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		noCache(response);
		dxwj.setCondition(""); // 防sql注入
		dxwj.assLike();// 拼接%
		List<Dxwj> list = campService.schDxwj(dxwj); // 调用Service查询数据
		/* 将数据Bean的集合转换为Json集合 */
		JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
		JSONObject obj; // 声明用于构造Bean的Json对象
		for (Dxwj u : list) { // 循环查询的数据集构造Json数据集
			obj = u.getJsonInDict(); // 将Bean转换为Json对象
			obj.put("_oper", u.getId()); // 为"操作列"设定主键值
			datas.add(obj); // 添加Json对象到Json集合
		}
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		JSONObject res = new JSONObject(); // 声明页面返回Json
		res.put("total", dxwj.getDatasize()); // 设置数据总记录数
		res.put("rows", datas); // 设置数据集
		response.getWriter().print(res.toJSONString()); // 输出到页面
	}

	/**
	 * 预添加or预修改or查看方法
	 * @param dxwj
	 * 预添加时可传入一些初始值；预修改和查看时用来承载主键值
	 * @param toL
	 * 挂字典的字段是否需要转为字典Label值。true则进行转换
	 * @param response
	 *  返回请求,输出Json
	 * @throws IOException
	 */
	@RequestMapping(value = "/vieDxwj.action")
	public void vieDxwj(Dxwj dxwj, Boolean toL, HttpServletResponse response)
			throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (toL != null && toL) { // 查看页面加载数据时调用，返回的对象已进行字典转换
			if (dxwj.getId() != null && dxwj.getId() != 0) { // 检测主键非空
				dxwj = campService.schDxwjById(dxwj.getId()); // 按主键查询数据
				response.getWriter().print(dxwj.getJsonInDict()); // 输出经过字典转换Json对象到查看页(vie_)
			}
		} else { // 修改页面加载数据时调用
			if (dxwj.getId() != null && dxwj.getId() != 0) { // 检查主键非空
				dxwj = campService.schDxwjById(dxwj.getId()); // 按主键查询数据
				response.getWriter().print(dxwj.getJson()); // 输出Json对象到修改页(upd_)
			} else { // 添加页面加载数据时调用，可设置添加页面中一些属性的预设值。
				response.getWriter().print(new Dxwj().getJson()); // 输出Json对象到添加页(upd_)
			}
		}
	}

	/**
	 * 添加或修改的提交方法
	 * @param dxwj
	 * 数据表单Bean
	 * @param response
	 * 返回请求
	 * @throws IOException
	 */
	@RequestMapping(value = "/updDxwj.action")
	public void updDxwj(Dxwj dxwj, HttpServletResponse response)
			throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (dxwj.getId() == null) { // 检测主键为空则
			campService.addDxwj(dxwj); // 添加数据
		} else { // 主键非空则
			campService.updDxwj(dxwj); // 修改数据
		}
	}

	/**
	 * 保存电信问卷
	 * @param dxwj
	 * @param request
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value = "/saveDxwj.action")
	public Map<String, Object> saveDxwj(Dxwj dxwj, HttpServletRequest request) {
		Map<String, Object> result = new HashMap<String, Object>();// 实例化返回结果集
		Person person = (Person) request.getSession(true).getAttribute(// 获取当前用户
				SysConstant.CURRENT_USER);// 获取当前用户
		try {
			if (dxwj != null && dxwj.getRid() != null && person != null) {// 判断必要参数是否为空
				Dxwj param = new Dxwj();// 实例化电信问卷实体
				param.setRid(dxwj.getRid());// 设置关联ID
				List<Dxwj> list = campService.schDxwj(param);// 查询电信问卷列表
				if (list != null && list.size() > 0) {//如果集合不为空
					Dxwj updDxwj = list.get(0);// 则获取第一个对象
					updDxwj.setContent(dxwj.getContent());// 设置内容
					updDxwj.setUpdatetime(new Date());// 设置更新时间
					updDxwj.setUpdateby(person.getName());// 设置更新人
					campService.updDxwj(updDxwj);// 执行更新操作
					dxwj.setId(updDxwj.getId());//设置id
				} else {
					Date now = new Date();// 获取系统时间
					dxwj.setCreateby(person.getName());// 设置创建人
					dxwj.setUpdateby(person.getName());// 设置更新人
					dxwj.setCreatetime(now);// 设置创建时间
					dxwj.setUpdatetime(now);// 设置更新时间
					campService.addDxwj(dxwj);// 添加电信问卷
				}
				result.put("success", true);// 设置操作状态
				result.put("data", dxwj);// 设置数据
			} else {
				result.put("success", false);// 设置操作状态
				result.put("msg", "必要参数缺失");// 设置提示消息
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);// 设置操作状态
			result.put("msg", "服务器异常");// 设置提示消息
		} finally {
			return result;// 返回结果
		}
	}

	/**
	 * 获取电信问卷
	 * @param dxwj
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value = "/getDxwj.action")
	public Map<String, Object> getDxwj(Dxwj dxwj,HttpServletRequest request) {
		Map<String, Object> result = new HashMap<String, Object>(); // 初始化返回结果集
		Person person = (Person) request.getSession(true).getAttribute(//获取当前用户
				SysConstant.CURRENT_USER);//获取当前用户
		try {
			if (dxwj != null && dxwj.getRid() != null) { // 判断必要参数是否为空
				
				Hyanswer hysch = new Hyanswer();
				hysch.setTaskid(dxwj.getRid());
				hysch.setFromno(person.getNumber());
				List<Hyanswer> hws = campService.schHyanswer(hysch);
				
				Dxwj param = new Dxwj(); //实例化电信问卷实体
				param.setRid(dxwj.getId()); //设置关联ID
				List<Dxwj> list = campService.schDxwj(param); // 查询电信问卷列表
				result.put("success", true);//设置返回状态
				if (list != null && list.size() > 0) { //判断查询结果是否为空
					if(hws != null && hws.size() > 0 && hws.get(0).getContent() != null){
						list.get(0).setContent(hws.get(0).getContent());
					}
					result.put("data", list.get(0)); // 查询结果赋值给返回结果集
				}
			} else {
				result.put("success", false); // 设置返回状态
				result.put("msg", "必要参数缺失"); // 提示参数
			}
		} catch (Exception e) {
			e.printStackTrace();// 打印异常信息
			result.put("success", false); // 设置返回状态
			result.put("msg", "服务器异常"); // 提示参数
		} finally {
			return result; // 返回结果集
		}
	}

	/**
	 * 删除方法
	 * @param ids
	 * 待删除数据的主键数组
	 * @param response
	 * 返回请求
	 */
	@RequestMapping(value = "/delDxwj.action")
	public void delDxwj(@RequestParam(value = "ids[]") Integer[] ids,
			HttpServletResponse response) {
		noCache(response); // Ajax方法设定无缓存
		campService.delDxwj(ids); // 删除数据
	}

	/**
	 * JSP页面跳转
	 * @param page
	 * jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value = "/page.action")
	public String goPage(String page) { // 放过本Ctrl的页面，不合法的跳转到error页。
		if ("index".equals(page) || "dxwj_sch".equals(page)//判断跳转页面
				|| "dxwj_vie".equals(page) || "dxwj_upd".equals(page)) {//判断跳转页面
			return "/camp/" + page;// 返回页面
		} else {
			return "/error";// 返回error页面
		}
	}
}
