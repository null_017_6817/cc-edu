package com.qtrmoon.camp.ctrl;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.alibaba.fastjson.JSON;
import com.qtrmoon.common.BaseController;
import com.qtrmoon.camp.pojo.Score;
import com.qtrmoon.camp.pojo.TrainCamp;
import com.qtrmoon.camp.serdao.ICampService;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.dictionary.bsd.DictBean;
import com.qtrmoon.manager.pojo.Person;
import com.qtrmoon.sysmanage.SysConstant;
import com.qtrmoon.template.pojo.Template;
import com.qtrmoon.template.pojo.TemplateCategory;
import com.qtrmoon.template.serdao.ITemplateService;

/**
 * 评分表控制器
 */
@Controller
@RequestMapping("/camp/score")
public class ScoreController extends BaseController{
	@Autowired
	private ICampService campService;								//声明模块Service实例

	@Autowired
	private ITemplateService templateService;								//声明模块Service实例
	/**
	 * 查询方法，自带分页设置。
	 * @param score 承载查询条件的Bean
	 * @param request 可用来获取非Bean内的参数
	 * @param response 返回请求,输出Json
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/schScore.action")
	public void schScore(Score score,HttpServletRequest request,HttpServletResponse response) throws IOException{
		noCache(response);
		score.setCondition("");											//防sql注入
		score.assLike("pfxsxh","bpfxsxh","pfjg","zf");

		List<Score> list=campService.schScore(score);		//调用Service查询数据
		/*将数据Bean的集合转换为Json集合*/
		JSONArray datas=new JSONArray();									//构造业务数据的JSON集合
		JSONObject obj;														//声明用于构造Bean的Json对象
		for(Score u:list){												//循环查询的数据集构造Json数据集
			obj=u.getJsonInDict();											//将Bean转换为Json对象
			obj.put("_oper",u.getId());								//为"操作列"设定主键值
			datas.add(obj);													//添加Json对象到Json集合
		}
		/*构造分页数据的JSON对象，并包装到返回页面的Json中*/
		JSONObject res=new JSONObject();									//声明页面返回Json
		res.put("total",score.getDatasize());							//设置数据总记录数
		res.put("rows", datas);												//设置数据集
		response.getWriter().print(res.toJSONString());						//输出到页面
	}
	
	/**
	 * 预添加or预修改or查看方法
	 * @param score 预添加时可传入一些初始值；预修改和查看时用来承载主键值
	 * @param toL 挂字典的字段是否需要转为字典Label值。true则进行转换
	 * @param response 返回请求,输出Json
	 * @throws IOException
	 */
	@RequestMapping(value="/vieScore.action")
	public void vieScore(Score score,Boolean toL,HttpServletResponse response) throws IOException{
		noCache(response);													//Ajax方法设定无缓存
		if(toL!=null&&toL){													//查看页面加载数据时调用，返回的对象已进行字典转换
			if(score.getId()!=null&&score.getId()!=0){					//检测主键非空
				score=campService.schScoreById(score.getId());			//按主键查询数据
				response.getWriter().print(score.getJsonInDict());						//输出经过字典转换Json对象到查看页(vie_)
			}
		}else{																//修改页面加载数据时调用
			if(score.getId()!=null&&score.getId()!=0){	//检查主键非空
				score=campService.schScoreById(score.getId());		//按主键查询数据
				response.getWriter().print(score.getJson());			//输出Json对象到修改页(upd_)
			}else{															//添加页面加载数据时调用，可设置添加页面中一些属性的预设值。
				response.getWriter().print(new Score().getJson());		//输出Json对象到添加页(upd_)
			}
		}
	}
	
	/**
	 * 添加或修改的提交方法
	 * @param score 数据表单Bean
	 * @param response 返回请求
	 * @throws IOException
	 */
	@RequestMapping(value="/updScore.action")
	public void updScore(Score score,String templateId,HttpServletResponse response,HttpServletRequest request) throws IOException{
		noCache(response);													//Ajax方法设定无缓存
		Person person = (Person) request.getSession(true).getAttribute(SysConstant.CURRENT_USER);
		String s=DictBuffer.getLabel("ZD_SCORE_SCALE",templateId);	//获取评分占比
		logger.info("看这个=========================={}",s);
		String[] proportion=s.split(":");	//转换为数组
		
		int  zpercent=Integer.parseInt(proportion[0]); //自评分占比
		int  xpercent=Integer.parseInt(proportion[1]); //小组评占比
		int  tpercent=Integer.parseInt(proportion[2]); //教师评占比
		Date date=new Date();
		score.setMbid(Integer.parseInt(templateId));	//设置模板id
		score.setPfrq(date);	//设置评分日期
		if(score.getPflx()==3){
			Score ts=new Score();
			ts.setPfxsxh(score.getPfxsxh());
			ts.setBpfxsxh(score.getBpfxsxh());
			ts.setPfrw(score.getPfrw());
			List<Score> tslist=campService.schScore(ts);		//调用Service查询数据
			if(tslist!=null&&tslist.size()>0){
				score.setId(tslist.get(0).getId());
				campService.updScore(score);						//修改数据
			}else{
				campService.addScore(score);						//添加数据
			}
			
		}else{
			if(score.getId()==null){									//检测主键为空则
				campService.addScore(score);						//添加数据
			}else{																//主键非空则
				campService.updScore(score);						//修改数据
			}
		}
		TrainCamp trainCamp=new TrainCamp();	//创建实训营地评分对象
		trainCamp.setBookid(score.getPfrw());	//任务id
		trainCamp.setStudid(Integer.parseInt(score.getBpfxsxh()));	//学生id
		List<TrainCamp> list =campService.schTrainCamp(trainCamp);	//查询评分总表是否存在记录	
		if(list!=null&&list.size()>0){	//总表存在记录
			if(score.getPflx()==1){	//如果添加或修改的评分为自评时更新评分总表的自评数据
				TrainCamp camp=new TrainCamp();
				camp.setId(list.get(0).getId());	//设置id
				Float zpf=Float.parseFloat(score.getZf())*zpercent/100;
				camp.setPersonalscore1(String.format("%.1f", zpf));	//设置自评分
				camp.setTm(date);	//设置时间
				campService.updTrainCampPersonalScore(camp);	//更新数据
			}else if(score.getPflx()==2){ //如果添加或修改的评分为小组评时更新评分总表的小组评分数据
				Score sc=new Score();	//创建评分对象
				sc.setBpfxsxh(score.getBpfxsxh());	//设置被评分人
				sc.setPflx(2);	//设置评分类型（小组评）
				sc.setPfrw(score.getPfrw()); //设置评分任务
				List<Score> scList=campService.schScore(sc);		//调用Service查询数据
				Float xpf=0.0f;
				int pfcs=0;
				if(scList.size()>0){
					for (Score score2 : scList) {
						xpf+=Float.parseFloat(score2.getZf());
						pfcs++;
					}
				}
				xpf=xpf/pfcs*xpercent/100;	//算出小组评分的值
				TrainCamp camp=new TrainCamp();
				camp.setId(list.get(0).getId());	//设置id	
				camp.setPartnerscore1(String.format("%.1f", xpf));	//设置小组评分
				camp.setTm(date);	//设置时间
				campService.updTrainCampPartnerScore(camp);	//更新数据
			}else if(score.getPflx()==3){ //如果添加或修改的评分为教师评时更新评分总表的教师评分数据
				TrainCamp camp=new TrainCamp();
				camp.setId(list.get(0).getId());	//设置id
				Float tpf=Float.parseFloat(score.getZf())*tpercent/100;
				camp.setTeacherscore1(String.format("%.1f", tpf));		//设置教师评分
				camp.setTeacherid(person.getNumber());
				camp.setTm(date);	//设置时间
				camp.setTeacherscorestatus(1);//设置教师评分状态为已评分
				camp.setTeacherscores(score.getZf());
				campService.updTrainCampTeacherScore(camp);	//更新数据
			}
		}else{ //评分总表不存在记录，创建新的记录
			if(score.getPflx()==1){ //添加实训营地评分包括自评分
				TrainCamp t=new TrainCamp();
				t.setBookid(score.getPfrw());	//设置任务
				t.setStudid(Integer.parseInt(score.getBpfxsxh()));	//设置学号
				Float zpf=Float.parseFloat(score.getZf())*zpercent/100;
				t.setPersonalscore1(String.format("%.1f", zpf)); //设置自评分
				t.setClassid(Integer.parseInt(person.getCnumber()));	//设置班级id
				t.setTm(date); //设置时间
				campService.addTrainCamp(t);	//添加数据
			}else if(score.getPflx()==2){ //添加实训营地评分包括小组评分
				TrainCamp t=new TrainCamp();
				t.setBookid(score.getPfrw());	//设置任务
				t.setStudid(Integer.parseInt(score.getBpfxsxh())); 	//设置学号
				Float xpf=Float.parseFloat(score.getZf())*xpercent/100;
				t.setPartnerscore1(String.format("%.1f", xpf));	//设置小组评分
				t.setClassid(Integer.parseInt(person.getCnumber()));	//设置班级id
				t.setTm(date);	//设置时间
				campService.addTrainCamp(t); //添加数据
			}else if(score.getPflx()==3){ //添加实训营地评分包括教师评分
				TrainCamp t=new TrainCamp();	
				t.setBookid(score.getPfrw());	//设置任务
				t.setStudid(Integer.parseInt(score.getBpfxsxh()));	//设置学号
				Float tpf=Float.parseFloat(score.getZf())*tpercent/100;
				t.setTeacherscore1(String.format("%.1f", tpf));	//设置教师评分
				t.setClassid(Integer.parseInt(DictBuffer.getLabel("ZD_GETCNUMBER", score.getBpfxsxh())));	//设置班级id
				t.setTeacherid(person.getNumber());	//设置教师id
				t.setTm(date);	//设置时间
				t.setTeacherscorestatus(1); //设置教师评分状态为已评分
				t.setTeacherscores(score.getZf());
				campService.addTrainCamp(t); //添加数据
			}
		}
		TrainCamp t=new TrainCamp();	//创建实训营地评分对象
		t.setBookid(score.getPfrw());	//任务id
		t.setStudid(Integer.parseInt(score.getBpfxsxh()));	//学生id
		List<TrainCamp> tList =campService.schTrainCamp(trainCamp);	//评分总表记录
		String zpf=tList.get(0).getPersonalscore1(); //自评分
		String xzpf=tList.get(0).getPartnerscore1(); //小组评分
		String jspf=tList.get(0).getTeacherscore1(); //教师评分
		Float z=0.0f; //自评分给个默认初始值
		Float x=0.0f; //小组评分给个默认初始值
		Float j=0.0f; //教师评分给个默认初始值
		if(!"".equals(zpf)&&zpf!=null&&!"0".equals(zpf)){ //数据库里存在自评分的话，给自评分赋值
			z=Float.parseFloat(zpf);
		}
		if(!"".equals(xzpf)&&xzpf!=null&&!"0".equals(xzpf)){ //数据库里存在小组评分的话，给小组评分赋值
			x=Float.parseFloat(xzpf);
		}
		if(!"".equals(jspf)&&jspf!=null&&!"0".equals(jspf)){ //数据库里存在教师评分的话，给教师评分赋值
			j=Float.parseFloat(jspf);
		}
		Float zf=z+x+j;	//计算总分
		TrainCamp tc=new TrainCamp();
		tc.setId(tList.get(0).getId());
		tc.setScore(zf);
		campService.updTrainCampScore(tc); //更新总分值
	}
	
	/**
	 * 删除方法
	 * @param ids 待删除数据的主键数组
	 * @param response 返回请求
	 */
	@RequestMapping(value="/delScore.action")
	public void delScore(@RequestParam(value = "ids[]") Integer[] ids,HttpServletResponse response){
		noCache(response);													//Ajax方法设定无缓存
		campService.delScore(ids);									//删除数据
	}
	
	
	/**
	 * 查询实训评分模板。
	 * @param templateCategory 承载查询条件的Bean
	 * @param request 可用来获取非Bean内的参数
	 * @param response 返回请求,输出Json
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/schTemplateCategory.action")
	public void schTemplateCategory(TemplateCategory templateIndex,String templateId,HttpServletRequest request,HttpServletResponse response) throws IOException{
		noCache(response);
		if("0".equals(templateId)){
			response.getWriter().print("");						//输出到页面
		}else{
			templateIndex.setCondition("");											//防sql注入
			templateIndex.assLike("classificationName","indexContent");
			templateIndex.setTemplateId(Integer.parseInt(templateId));
			List<TemplateCategory> list=templateService.schTemplateCategory(templateIndex);		//调用Service查询数据
			//将数据Bean的集合转换为Json集合
			JSONArray datas=new JSONArray();									//构造业务数据的JSON集合
			JSONObject obj;														//声明用于构造Bean的Json对象
			for(TemplateCategory u:list){												//循环查询的数据集构造Json数据集
				obj=u.getJsonInDict();											//将Bean转换为Json对象
				obj.put("_oper",u.getId());								//为"操作列"设定主键值
				datas.add(obj);													//添加Json对象到Json集合
			}
			//构造分页数据的JSON对象，并包装到返回页面的Json中
			JSONObject res=new JSONObject();									//声明页面返回Json
			res.put("total",templateIndex.getDatasize());							//设置数据总记录数
			res.put("rows", datas);												//设置数据集
			String score=DictBuffer.getLabel("ZD_INDEX_SCORE", templateId);
			res.put("score", score);				//评分模板中可下拉选择的分数
			response.getWriter().print(res.toJSONString());						//输出到页面
		}
	}
	
	/**
	 * 查询自己的评分方法。
	 * @param score 承载查询条件的Bean
	 * @param request 可用来获取非Bean内的参数
	 * @param response 返回请求,输出Json
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/schSelfScore.action")
	public void schSelfScore(Score score,String templateId,HttpServletRequest request,HttpServletResponse response) throws IOException{
		noCache(response);
		score.setCondition("");											//防sql注入
		JSONObject obj=new JSONObject();		//构造json对象
		boolean sfzp=false;			//设置是否自评初始值未false
		List<Score> list=campService.schScore(score);		//调用Service查询数据
		if(score.getPflx()==1){ 	//如果评分类型为自评
			if(list!=null&&list.size()>0){ //查询到的集合存在数据
				sfzp=true; //设置是否自评为true
				com.alibaba.fastjson.JSONArray jsonx = com.alibaba.fastjson.JSONArray.parseArray(list.get(0).getPfjg()); //获取集合中的评分结果（评分结果为json串）
				for(int i=0;i<jsonx.size();i++){
					int tid= Integer.parseInt(jsonx.getJSONObject(i).get("classify").toString());	//评分模板id
					TemplateCategory template=templateService.schTemplateCategoryById(tid); //查询评分模板
					jsonx.getJSONObject(i).put("classifyName", template.getClassificationName()); //评分结果中添加评分分类名称
					jsonx.getJSONObject(i).put("proportion", template.getCalculationProportion()); //评分结果中添加计算比例
					com.alibaba.fastjson.JSONArray targets= (com.alibaba.fastjson.JSONArray) jsonx.getJSONObject(i).get("target"); //考核指标json数组
					String[] tar=new String[targets.size()]; //创建新的数组用于存放考核指标内容，之前json串中保存的是考核指标id
					com.alibaba.fastjson.JSONArray json = com.alibaba.fastjson.JSONArray.parseArray(template.getIndexContent()); //模板中的考核指标数组
					for(int k=0;k<targets.size();k++){
						for(int j=0;j<json.size();j++){
							if(targets.get(k).equals(json.getJSONObject(j).get("serialNumber"))){ 
								tar[k]=json.getJSONObject(j).get("indexContent").toString(); //考核指标id和模板中的一样时，数组添加指标内容，结束本次循环
								break;
							}
							
						}
					}
					jsonx.getJSONObject(i).put("targets", tar); //评分结果中添加考核指标内容
					
				}
				list.get(0).setPfjg(jsonx.toString()); //设置修改后的评分结果
				obj=list.get(0).getJsonInDict();
			}
			/*构造分页数据的JSON对象，并包装到返回页面的Json中*/
			JSONObject res=new JSONObject();									//声明页面返回Json
			res.put("sfzp",sfzp);				//设置是否自评			
			String s=DictBuffer.getLabel("ZD_INDEX_SCORE", templateId);
			res.put("score", s);	
			res.put("result", obj);			//设置结果
			response.getWriter().print(res.toJSONString());						//输出到页面
		}else if(score.getPflx()==2){
			/*将数据Bean的集合转换为Json集合*/
			JSONArray datas=new JSONArray();									//构造业务数据的JSON集合
			JSONObject objScore;														//声明用于构造Bean的Json对象
			for(Score u:list){												//循环查询的数据集构造Json数据集
				objScore=u.getJsonInDict();											//将Bean转换为Json对象
				datas.add(objScore);													//添加Json对象到Json集合
			}
			/*构造分页数据的JSON对象，并包装到返回页面的Json中*/
			JSONObject res=new JSONObject();									//声明页面返回Json
			res.put("rows", datas);												//设置数据集
			response.getWriter().print(res.toJSONString());						//输出到页面
		}
		
	}
	
	/**
	 * 查询小组评分模板。
	 * @param templateCategory 承载查询条件的Bean
	 * @param request 可用来获取非Bean内的参数
	 * @param response 返回请求,输出Json
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/schGroupTemplateCategory.action")
	public void schGroupTemplateCategory(TemplateCategory templateIndex,String templateId,Integer pfrw,HttpServletRequest request,HttpServletResponse response) throws IOException{
		noCache(response);
		templateIndex.setCondition("");											//防sql注入
		templateIndex.assLike("classificationName","indexContent");
		templateIndex.setTemplateId(Integer.parseInt(templateId));
		List<TemplateCategory> list=templateService.schTemplateCategory(templateIndex);		//调用Service查询数据
		//将数据Bean的集合转换为Json集合
		JSONArray datas=new JSONArray();									//构造业务数据的JSON集合
		JSONObject obj;														//声明用于构造Bean的Json对象
		for(TemplateCategory u:list){												//循环查询的数据集构造Json数据集
			obj=u.getJsonInDict();											//将Bean转换为Json对象
			obj.put("_oper",u.getId());								//为"操作列"设定主键值
			datas.add(obj);													//添加Json对象到Json集合
		}
		Person person = (Person) request.getSession(true).getAttribute(SysConstant.CURRENT_USER);
		List<Map<String,Object>> students = campService.schGroupStudent(pfrw, person.getCnumber()); //查询可以互评的学生
		String stus="";
		if(students.size()>0){
			for(int i=0;i<students.size();i++){
				if(person.getNumber().equals(students.get(i).get("stu").toString())){ //下拉小组评的学生不保存当前登录学生的学号
					continue;
				}
				stus+=students.get(i).get("stu").toString();
				if(i<students.size()-1){
					stus+=",";
				}
			}
		}
		//构造分页数据的JSON对象，并包装到返回页面的Json中
		JSONObject res=new JSONObject();									//声明页面返回Json
		res.put("total",templateIndex.getDatasize());							//设置数据总记录数
		res.put("rows", datas);												//设置数据集
		String score=DictBuffer.getLabel("ZD_INDEX_SCORE", templateId);
		res.put("score", score);						//设置可以下拉选择的评分分数
		res.put("stus", stus);						//可以互评的学生
		response.getWriter().print(res.toJSONString());						//输出到页面
	}
	
	/**
	 * 根据评分id查询评分内容。
	 * @param pfid 评分id
	 * @param request 可用来获取非Bean内的参数
	 * @param response 返回请求,输出Json
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/schScoreById.action")
	public void schScoreById(Integer pfid,String templateId,HttpServletRequest request,HttpServletResponse response) throws IOException{
		noCache(response);
		Score score=campService.schScoreById(pfid);  //根据评分id查询评分内容
		JSONObject obj=new JSONObject();		//构造json对象
		com.alibaba.fastjson.JSONArray jsonx = com.alibaba.fastjson.JSONArray.parseArray(score.getPfjg());	//获取集合中的评分结果（评分结果为json串）
		for(int i=0;i<jsonx.size();i++){
			int tid= Integer.parseInt(jsonx.getJSONObject(i).get("classify").toString());	//评分模板id
			TemplateCategory template=templateService.schTemplateCategoryById(tid);		//查询评分模板
			jsonx.getJSONObject(i).put("classifyName", template.getClassificationName());	//评分结果中添加评分分类名称
			jsonx.getJSONObject(i).put("proportion", template.getCalculationProportion());	//评分结果中添加计算比例
			com.alibaba.fastjson.JSONArray targets= (com.alibaba.fastjson.JSONArray) jsonx.getJSONObject(i).get("target");	//考核指标json数组
			String[] tar=new String[targets.size()];	//创建新的数组用于存放考核指标内容，之前json串中保存的是考核指标id
			com.alibaba.fastjson.JSONArray json = com.alibaba.fastjson.JSONArray.parseArray(template.getIndexContent());	//模板中的考核指标数组
			for(int k=0;k<targets.size();k++){	
				for(int j=0;j<json.size();j++){
					if(targets.get(k).equals(json.getJSONObject(j).get("serialNumber"))){
						tar[k]=json.getJSONObject(j).get("indexContent").toString();	//考核指标id和模板中的一样时，数组添加指标内容，结束本次循环
						break;
					}
				}
			}
			jsonx.getJSONObject(i).put("targets", tar);	//评分结果中添加考核指标内容
		}
		score.setPfjg(jsonx.toString());	//设置修改后的评分结果
		obj=score.getJsonInDict();
		/*构造分页数据的JSON对象，并包装到返回页面的Json中*/
		JSONObject res=new JSONObject();									//声明页面返回Json
		String s=DictBuffer.getLabel("ZD_INDEX_SCORE", templateId);
		res.put("score", s);						//设置可以下拉选择的评分分数
		res.put("result", obj);				//设置返回结果
		response.getWriter().print(res.toJSONString());						//输出到页面
	}
	
	
	/**
	 * 查询对当前登录用户的评分信息。
	 * @param pfrw 评分任务
	 * @param request 可用来获取非Bean内的参数
	 * @param response 返回请求,输出Json
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/schMyScore.action")
	public void schMyScore(Integer pfrw,String templateId,HttpServletRequest request,HttpServletResponse response) throws IOException{
		noCache(response);
		Person person = (Person) request.getSession(true).getAttribute(SysConstant.CURRENT_USER);
		Score score=new Score();
		score.setBpfxsxh(person.getNumber());	//设置被评分人
		score.setPfrw(pfrw);		//设置评分任务
		score.addOrderDescCol("PFLX");
		List<Score> list=campService.schScore(score);		//调用Service查询数据
		//将数据Bean的集合转换为Json集合
		JSONArray datas=new JSONArray();									//构造业务数据的JSON集合
		JSONObject obj;														//声明用于构造Bean的Json对象
		for(Score u:list){												//循环查询的数据集构造Json数据集
			obj=u.getJsonInDict();											//将Bean转换为Json对象
			obj.put("_oper",u.getId());								//为"操作列"设定主键值
			datas.add(obj);													//添加Json对象到Json集合
		}
		//构造分页数据的JSON对象，并包装到返回页面的Json中
		JSONObject res=new JSONObject();									//声明页面返回Json
		res.put("rows", datas);												//设置数据集
		String s=DictBuffer.getLabel("ZD_SCORE_SCALE", templateId);
		res.put("score", s);	
		response.getWriter().print(res.toJSONString());						//输出到页面
	}
	
	/**
	 * 查询当前登录用户的总分。
	 * @param pfrw 评分任务
	 * @param request 可用来获取非Bean内的参数
	 * @param response 返回请求,输出Json
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/schMyTotalScore.action")
	public void schMyTotalScore(Score score,HttpServletRequest request,HttpServletResponse response) throws IOException{
		noCache(response);
		List<Score> list=campService.schScore(score);		//调用Service查询数据
		int zscore=0;		//是否存在自评分设置默认值
		int xscore=0;		//是否存在小组评分设置默认值
		int tscore=0;		//是否存在教师评分设置默认值
		String totalScore="";		//总分设置默认值
		boolean complete=false;		//是否所有评分都完成设置默认值
		if(list.size()>0){
			for (Score s : list) {
				if(s.getPflx()==1){	//集合中存在自评分类型
					zscore=1;
				}else if(s.getPflx()==2){	//集合中存在小组评分
					xscore=1;
				}else if(s.getPflx()==3){	//集合中存在教师评分
					tscore=1;
				}
			}
		}
		if(zscore+xscore+tscore==3||zscore+xscore==2){	//如果等于3说明所有评分都完成，存在教师忙的情况可能只有自评和小组评
			complete=true;
			TrainCamp tp=new TrainCamp();	
			tp.setStudid(Integer.parseInt(score.getBpfxsxh()));
			tp.setBookid(score.getPfrw());
			List<TrainCamp> tlist=campService.schTrainCamp(tp);	//查询总评分
			totalScore=String.valueOf(tlist.get(0).getScore());
		}
		//构造分页数据的JSON对象，并包装到返回页面的Json中
		JSONObject res=new JSONObject();									//声明页面返回Json
		res.put("complete", complete);												//设置完成情况
		res.put("totalScore", totalScore);					//设置总评分
		response.getWriter().print(res.toJSONString());						//输出到页面
	}
	
	
	/**
	 * 查询教师评分内容。
	 * @param score 
	 * @param request 可用来获取非Bean内的参数
	 * @param response 返回请求,输出Json
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/schTeacherScore.action")
	public void schTeacherScore(Score score,String templateId,HttpServletRequest request,HttpServletResponse response) throws IOException{
		noCache(response);
		List<Score> list=campService.schScore(score);		//调用Service查询数据
		if(list!=null&&list.size()>0){
			JSONObject obj=new JSONObject();		//构造json对象
			com.alibaba.fastjson.JSONArray jsonx = com.alibaba.fastjson.JSONArray.parseArray(list.get(0).getPfjg());	//获取集合中的评分结果（评分结果为json串）
			for(int i=0;i<jsonx.size();i++){
				int tid= Integer.parseInt(jsonx.getJSONObject(i).get("classify").toString());	//评分模板id
				TemplateCategory template=templateService.schTemplateCategoryById(tid);		//查询评分模板
				jsonx.getJSONObject(i).put("classifyName", template.getClassificationName());	//评分结果中添加评分分类名称
				jsonx.getJSONObject(i).put("proportion", template.getCalculationProportion());	//评分结果中添加计算比例
				com.alibaba.fastjson.JSONArray targets= (com.alibaba.fastjson.JSONArray) jsonx.getJSONObject(i).get("target");	//考核指标json数组
				String[] tar=new String[targets.size()];	//创建新的数组用于存放考核指标内容，之前json串中保存的是考核指标id
				com.alibaba.fastjson.JSONArray json = com.alibaba.fastjson.JSONArray.parseArray(template.getIndexContent());	//模板中的考核指标数组
				for(int k=0;k<targets.size();k++){	
					for(int j=0;j<json.size();j++){
						if(targets.get(k).equals(json.getJSONObject(j).get("serialNumber"))){
							tar[k]=json.getJSONObject(j).get("indexContent").toString();	//考核指标id和模板中的一样时，数组添加指标内容，结束本次循环
							break;
						}
					}
				}
				jsonx.getJSONObject(i).put("targets", tar);	//评分结果中添加考核指标内容
			}
			score.setYdgjzc(list.get(0).getYdgjzc());
			score.setPfjg(jsonx.toString());	//设置修改后的评分结果
			obj=score.getJsonInDict();
			/*构造分页数据的JSON对象，并包装到返回页面的Json中*/
			JSONObject res=new JSONObject();									//声明页面返回Json
			String s=DictBuffer.getLabel("ZD_INDEX_SCORE", templateId);
			res.put("score", s);						//设置可以下拉选择的评分分数
			res.put("result", obj);				//设置返回结果
			response.getWriter().print(res.toJSONString());						//输出到页面
		}
	}
	
	/**
	 * JSP页面跳转
	 * @param page jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value="/page.action")
	public String goPage(String page){										//放过本Ctrl的页面，不合法的跳转到error页。
		if("index".equals(page)||"score_sch".equals(page)||"score_vie".equals(page)||"score_upd".equals(page)){
			return "/camp/"+page;
		}else{
			return "/error";
		}
	}
	
	
}
