package com.qtrmoon.camp.ctrl;

import java.io.IOException;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;

import com.qtrmoon.util.LoginStudUtil;

/**
 * 学生在线websocket控制器
 */
@Controller("studSocket")
@ServerEndpoint("/studSocket")
public class StudSocket {
	
	/**
	 * 日志对象
	 */
	public static Logger logger = LoggerFactory.getLogger(StudSocket.class);
	
	
	//静态变量，用来记录当前在线连接数。应该把它设计成线程安全的。
	private static int onlineCount = 0;
	
	//concurrent包的线程安全Set，用来存放每个客户端对应的MyWebSocket对象。若要实现服务端与单一客户端通信的话，可以使用Map来存放，其中Key可以为用户标识
	private static CopyOnWriteArraySet<StudSocket> webSocketSet = new CopyOnWriteArraySet<StudSocket>();
	
	private Session session;//与某个客户端的连接会话，需要通过它来给客户端发送数据
	
	
	/**
	* 连接建立成功调用的方法
	* @param session  可选的参数。session为与某个客户端的连接会话，需要通过它来给客户端发送数据
	*/
	@OnOpen
	public void onOpen( Session session){
		this.session = session;
		webSocketSet.add(this);	//加入set中
		addOnlineCount();		//在线数加1
	}
	
	/**
	* 连接关闭调用的方法
	*/
	@OnClose
	public void onClose(){
		webSocketSet.remove(this);	//从set中删除
		subOnlineCount();			//在线数减1
	}
	
	/**
	* 收到客户端消息后调用的方法
	* @param message 客户端发送过来的消息
	* @param session 可选的参数
	*/
	@OnMessage
	public void onMessage(String message, Session session) {
		for(StudSocket item: webSocketSet){// 循环
			try {
				item.sendMessage(message);// 发送WebSocket消息
				logger.info("点选学生socket消息：{}", message);
			} catch (IOException e) {
				e.printStackTrace();// 打印异常消息
				continue;// 继续循环代码
			}
		}
	}
	
	/**
	 * 发生错误时调用
	 * @param session
	 * @param error
	 */
	@OnError
	public void onError(Session session, Throwable error){
		//error.printStackTrace();
	}
	 
	/**
	 * 这个方法与上面几个方法不一样。没有用注解，是根据自己需要添加的方法。
	 * @param message
	 * @throws IOException
	 */
	public synchronized void sendMessage(String message) throws IOException{
		this.session.getBasicRemote().sendText(message);//或者这样写 this.session.getAsyncRemote().sendText(message);
	}
	
	/**
	 * 获取当前在线总数
	 * @return
	 */
	public static synchronized int getOnlineCount() {
		return onlineCount;
	}
	
	/**
	 * 添加当前在线总数
	 */
	public static synchronized void addOnlineCount() {
		StudSocket.onlineCount++;
	}
	
	/**
	 * 减少当前在线总数
	 */
	public static synchronized void subOnlineCount() {
		StudSocket.onlineCount--;
	}

	/**
	 * 获取 webSocketSet
	 * @return
	 */
	public static CopyOnWriteArraySet<StudSocket> getWebSocketSet() {
		return webSocketSet;
	}

	/**
	 * 设置webSocketSet
	 * @param webSocketSet
	 */
	public static void setWebSocketSet(
			CopyOnWriteArraySet<StudSocket> webSocketSet) {
		StudSocket.webSocketSet = webSocketSet;
	}
	
}
