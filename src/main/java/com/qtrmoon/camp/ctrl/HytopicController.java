package com.qtrmoon.camp.ctrl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qtrmoon.camp.constant.DxwjQuestionType;
import com.qtrmoon.camp.pojo.Hytopic;
import com.qtrmoon.camp.serdao.ICampService;
import com.qtrmoon.common.BaseController;
import com.qtrmoon.manager.pojo.Person;
import com.qtrmoon.sysmanage.SysConstant;
import com.qtrmoon.sysmanage.pojo.User;
import com.qtrmoon.sysmanage.serdao.ISysmanageService;

/**
 * 行业场景控制器
 */
@Controller
@RequestMapping("/camp/hytopic")
public class HytopicController extends BaseController {
	
	@Autowired
	private ICampService campService; // 声明模块Service实例
	
	@Autowired
	private ISysmanageService sysmanageService;// 声明模块Service实例

	/**
	 * 查询方法，自带分页设置。
	 * @param hytopic
	 * 承载查询条件的Bean
	 * @param request
	 * 可用来获取非Bean内的参数
	 * @param response
	 * 返回请求,输出Json
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schHytopic.action")
	public void schHytopic(Hytopic hytopic, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		noCache(response);// 设置ajax无缓存
		hytopic.setCondition(""); // 防sql注入
		hytopic.assLike("title"); // 拼接%
		List<Hytopic> list = campService.schHytopic(hytopic); // 调用Service查询数据
		/* 将数据Bean的集合转换为Json集合 */
		JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
		JSONObject obj; // 声明用于构造Bean的Json对象
		for (Hytopic u : list) { // 循环查询的数据集构造Json数据集
			obj = u.getJsonInDict(); // 将Bean转换为Json对象
			obj.put("_oper", u.getId()); // 为"操作列"设定主键值
			trainUser(obj);
			datas.add(obj); // 添加Json对象到Json集合
		}
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		JSONObject res = new JSONObject(); // 声明页面返回Json
		res.put("total", hytopic.getDatasize()); // 设置数据总记录数
		res.put("rows", datas); // 设置数据集
		response.getWriter().print(res.toJSONString()); // 输出到页面
	}

	/**
	 * 用户id转换为name
	 * @param obj
	 */
	@SuppressWarnings("unchecked")
	private void trainUser(JSONObject obj) {
		if (obj != null) {// 判断必要参数是否为空
			if (obj.get("createby") != null) {// 判断必要参数是否为空
				String creat = (String) obj.get("createby");// 获取创建人
				User creatUser = sysmanageService.schUserById(Integer
						.valueOf(creat));// 根据创建人id查询数据
				if (creatUser != null) {//如果查询出来不为空 
					obj.put("createby", creatUser.getName());// 替换名称
				}
			}
			if (obj.get("updateby") != null) {// 判断更新人id是否为空
				String upd = (String) obj.get("updateby");// 获取更信任id
				User updUser = sysmanageService.schUserById(Integer
						.valueOf(upd));// 根据更新人id查询数据
				if (updUser != null) {// 查询数据不为空的场合
					obj.put("updateby", updUser.getName());// 替换名称
				}
			}
		}
	}

	/**
	 * 根据行业id获取全部场景数据
	 * @param hytopic
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value = "/getListByRid.action")
	public Map<String, Object> getListByRid(Hytopic hytopic) {
		Map<String, Object> result = new HashMap<String, Object>(); // 实例化返回结果集
		try {
			if (hytopic != null && hytopic.getRid() != null) {// 判断必要参数是否为空
				Hytopic param = new Hytopic();// 实例化对象
				param.setRid(hytopic.getRid());// 设置关联ID
				List<Hytopic> list = campService.schHytopic(param); // 查询数据
				result.put("success", true);// 设置成功状态
				result.put("data", list);// 设置返回数据
			} else {
				result.put("success", false); // 设置成功状态
				result.put("msg", "必要参数缺失"); // 设置提示信息
			}
		} catch (Exception e) {
			result.put("success", false); // 设置成功状态
			result.put("msg", "查询异常"); // 设置提示信息
			e.printStackTrace(); // 打印异常信息
		} finally {
			return result; // 返回结果集
		}
	}

	/**
	 * 根据行业id获取全部场景数据
	 * @param hytopic
	 * @return
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value = "/getTopicById.action")
	public Map<String, Object> getTopicById(Hytopic hytopic) {
		Map<String, Object> result = new HashMap<String, Object>(); // 实例化返回结果集
		try {
			if (hytopic != null && hytopic.getId() != null) { // 判断必要参数是否为空
				Hytopic topic = campService.schHytopicById(hytopic.getId()); // 查询数据
				result.put("success", true); // 设置成功状态
				result.put("data", topic);// 设置返回数据
			} else {
				result.put("success", false);// 设置成功状态
				result.put("msg", "必要参数缺失");// 设置提示信息
			}
		} catch (Exception e) {
			result.put("success", false);// 设置成功状态
			result.put("msg", "查询异常");// 设置提示信息
			e.printStackTrace();// 打印异常信息
		} finally {
			return result;// 返回结果集
		}
	}

	/**
	 * 预添加or预修改or查看方法
	 * @param hytopic
	 * 预添加时可传入一些初始值；预修改和查看时用来承载主键值
	 * @param toL
	 * 挂字典的字段是否需要转为字典Label值。true则进行转换
	 * @param response
	 * 返回请求,输出Json
	 * @throws IOException
	 */
	@RequestMapping(value = "/vieHytopic.action")
	public void vieHytopic(Hytopic hytopic, Boolean toL,
			HttpServletResponse response) throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (toL != null && toL) { // 查看页面加载数据时调用，返回的对象已进行字典转换
			if (hytopic.getId() != null && hytopic.getId() != 0) { // 检测主键非空
				hytopic = campService.schHytopicById(hytopic.getId()); // 按主键查询数据
				response.getWriter().print(hytopic.getJsonInDict()); // 输出经过字典转换Json对象到查看页(vie_)
			}
		} else { // 修改页面加载数据时调用
			if (hytopic.getId() != null && hytopic.getId() != 0) { // 检查主键非空
				hytopic = campService.schHytopicById(hytopic.getId()); // 按主键查询数据
				response.getWriter().print(hytopic.getJson()); // 输出Json对象到修改页(upd_)
			} else { // 添加页面加载数据时调用，可设置添加页面中一些属性的预设值。
				response.getWriter().print(new Hytopic().getJson()); // 输出Json对象到添加页(upd_)
			}
		}
	}

	/**
	 * 添加或修改的提交方法
	 * @param hytopic
	 * 数据表单Bean
	 * @param response
	 * 返回请求
	 * @throws IOException
	 */
	@RequestMapping(value = "/updHytopic.action")
	public void updHytopic(Hytopic hytopic, HttpServletResponse response)
			throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (hytopic.getId() == null) { // 检测主键为空则
			campService.addHytopic(hytopic); // 添加数据
		} else { // 主键非空则
			campService.updHytopic(hytopic); // 修改数据
		}
	}

	/**
	 * 删除方法
	 * @param ids
	 * 待删除数据的主键数组
	 * @param response
	 * 返回请求
	 */
	@RequestMapping(value = "/delHytopic.action")
	public void delHytopic(@RequestParam(value = "ids[]") Integer[] ids,
			HttpServletResponse response) {
		noCache(response); // Ajax方法设定无缓存
		campService.delHytopic(ids); // 删除数据
	}

	/**
	 * JSP页面跳转
	 * @param page
	 * jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value = "/page.action")
	public String goPage(String page, Model model) { // 放过本Ctrl的页面，不合法的跳转到error页。
		if ("index".equals(page) || "hytopic_sch".equals(page)// 判断跳转页面
				|| "hytopic_vie".equals(page) || "hytopic_upd".equals(page) // 判断跳转页面
				|| "hytopic_dxwjupd".equals(page) || "hytopic_cj".equals(page)) { // 判断跳转页面
			if ("hytopic_dxwjupd".equals(page) || "hytopic_sch".equals(page)) { // 判断跳转页面
				List<Map<String, Object>> list = new ArrayList<Map<String, Object>>(); // 实例化结果集
				for (DxwjQuestionType s : DxwjQuestionType.values()) { // 循环
					Map<String, Object> map = new HashMap<String, Object>(); // 实例化map集合
					map.put("name", s.getName()); // 赋值name属性
					map.put("code", s.getCode()); // 赋值code属性
					list.add(map); // 添加到结果集中
				}
				model.addAttribute("selectOp", list); // 结果集放到页面上 
			}
			return "/camp/" + page; // 跳转页面
		} else {
			return "/error"; // 跳转错误页面
		}
	}

	/**
	 * 根据id查询行业场景数据
	 * @param hytopic
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings("finally")
	@RequestMapping(value = "/getHytopicById.action")
	public Map<String, Object> schHytopicById(Hytopic hytopic,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Map<String, Object> result = new HashMap<String, Object>(); // 实例化返回结果集
		try {
			if (hytopic != null && hytopic.getId() != null) {// 判断必要参数是否为空
				Hytopic resultData = campService
						.schHytopicById(hytopic.getId()); // 查询数据
				if (resultData != null) { // 判断查询结果是否为空
					result.put("success", true); // 赋值成功状态
					result.put("data", resultData); // 赋值返回数据
				} else {
					result.put("success", false); // 赋值成功状态
					result.put("msg", "未查询到数据"); // 返回提示信息
				}
			} else {
				result.put("success", false); // 赋值成功状态
				result.put("msg", "必要参数缺失"); // 返回提示信息
			}
		} catch (Exception e) {
			e.printStackTrace(); // 打印异常信息
			result.put("success", false); // 赋值成功状态
			result.put("msg", "查询异常"); // 返回提示信息
		} finally {
			return result; // 返回结果集
		}
	}

	/**
	 * 添加或修改的提交方法
	 * @param hytopic
	 * 数据表单Bean
	 * @param response
	 * 返回请求
	 * @throws IOException
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value = "/saveHytopic.action")
	public Map<String, Object> saveHytopic(Hytopic hytopic,
			HttpServletRequest request) throws IOException {
		Map<String, Object> result = new HashMap<String, Object>(); // 实例化返回结果集
		Person person = (Person) request.getSession(true).getAttribute(// 获取当前用户
				SysConstant.CURRENT_USER); // 获取当前用户
		try {
			if (hytopic != null && person != null) {// 判断必要参数是否为空
				if (hytopic.getId() != null) {// 判断必要参数是否为空
					Hytopic up = campService.schHytopicById(hytopic.getId()); // 根据id查询数据
					if (up != null) {
						// 更新数据
						up.setUpdateby(person.getId().toString());// 更新人
						up.setUpdatetime(new Date()); // 更新时间
						up.setTitle(hytopic.getTitle()); // 设置title
						up.setRid(hytopic.getRid()); // 设置关联id
						up.setContent(hytopic.getContent()); // 设置内筒
						campService.updHytopic(up); // 更新数据
						result.put("success", true); // 设置成功状态
						result.put("msg", "操作成功！"); // 设置提示消息
					} else {
						result.put("success", false); // 设置成功状态
						result.put("msg", "更新失败，未查询到数据"); // 设置提示消息
					}
				} else {
					// 添加数据
					Hytopic add = new Hytopic(); // 实例化对象
					add.setTitle(hytopic.getTitle()); // 设置title
					add.setRid(hytopic.getRid()); // 设置关联id
					add.setContent(hytopic.getContent()); // 设置内容
					add.setCreatetime(new Date()); // 设置创建时间
					add.setCreateby(person.getId().toString()); // 设置创建人
					add.setUpdatetime(add.getCreatetime()); // 设置更新时间
					add.setUpdateby(person.getId().toString()); // 设置更新人
					campService.addHytopic(add); // 添加数据
					result.put("success", true); // 设置成功状态
					result.put("msg", "操作成功！"); // 设置提示消息
				}
			} else {
				result.put("success", false); // 设置成功状态
				result.put("msg", "必要参数缺失"); // 设置提示消息
			}
		} catch (Exception e) {
			e.printStackTrace(); // 打印异常信息
		} finally {
			return result; // 安徽结果集
		}
	}
}
