package com.qtrmoon.camp.ctrl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.qtrmoon.camp.pojo.ZdRepository;
import com.qtrmoon.camp.serdao.ICampService;
import com.qtrmoon.common.BaseController;
import com.qtrmoon.dictionary.DictBuffer;

/**
 * 行业控制器
 */
@Controller
@RequestMapping("/camp/repository")
public class ZdRepositoryController extends BaseController {
	
	@Autowired
	private ICampService campService; // 声明模块Service实例

	/**
	 * 查询方法，自带分页设置。
	 * @param repository
	 * 承载查询条件的Bean
	 * @param request
	 * 可用来获取非Bean内的参数
	 * @param response
	 * 返回请求,输出Json
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schRepository.action")
	public void schRepository(ZdRepository repository,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		noCache(response); // ajax设置调用无缓存
		repository.setCondition(""); // 防sql注入
		repository.assLike("label");// label拼接%
		List<Integer> idArray = new ArrayList<Integer>(); // 初始化集合对象
		if (repository != null && repository.getId() != null) {// 判断集合是否为空
			getChildren(idArray, repository.getId());// 获取所有子节点
		}
		String ids = "";// 初始化ids参数
		if (idArray != null && idArray.size() > 0) {// 判断集合是否为空
			for (Integer id : idArray) {// 循环
				if (id != null) {// 如果id不为null
					if (!"".equals(ids)) {// 如果ids不为空
						ids += "," + id.toString();//拼接id
					} else {
						ids = id.toString();//拼接id 
					}
				}
			}
			repository.setCondition("AND id in (" + ids + ")");// 设置动态sql
		}
		List<ZdRepository> list = campService.schRepository(repository); // 调用Service查询数据
		/* 将数据Bean的集合转换为Json集合 */
		JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
		JSONObject obj; // 声明用于构造Bean的Json对象
		for (ZdRepository u : list) { // 循环查询的数据集构造Json数据集
			obj = u.getJsonInDict(); // 将Bean转换为Json对象
			obj.put("_oper", u.getId()); // 为"操作列"设定主键值
			datas.add(obj); // 添加Json对象到Json集合
		}
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		JSONObject res = new JSONObject(); // 声明页面返回Json
		res.put("total", repository.getDatasize()); // 设置数据总记录数
		res.put("rows", datas); // 设置数据集
		response.getWriter().print(res.toJSONString()); // 输出到页面
	}

	/**
	 * 递归查询所有子节点
	 * @param idArray
	 * @param id
	 */
	private void getChildren(List<Integer> idArray, Integer id) {
		if (id != null) {// 判断id是否为null
			idArray.add(id);// 数组添加id
			ZdRepository zdRepository = new ZdRepository();// 实例化对象
			zdRepository.setPid(id);// 设置参数
			List<ZdRepository> zdLis = campService// 查询列表数据
					.schRepositoryList(zdRepository);// 查询列表数据
			if (zdLis != null && zdLis.size() > 0) {// 判断集合是否为空
				for (ZdRepository zd : zdLis) {// 循环
					getChildren(idArray, zd.getId());// 递归查询
				}
			}
		}
	}

	/**
	 * 预添加or预修改or查看方法
	 * @param repository
	 * 预添加时可传入一些初始值；预修改和查看时用来承载主键值
	 * @param toL
	 * 挂字典的字段是否需要转为字典Label值。true则进行转换
	 * @param response
	 * 返回请求,输出Json
	 * @throws IOException
	 */
	@RequestMapping(value = "/vieRepository.action")
	public void vieRepository(ZdRepository repository, Boolean toL,
			HttpServletResponse response) throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (toL != null && toL) { // 查看页面加载数据时调用，返回的对象已进行字典转换
			if (repository.getId() != null && repository.getId() != 0) { // 检测主键非空
				repository = campService.schRepositoryById(repository.getId()); // 按主键查询数据
				response.getWriter().print(repository.getJsonInDict()); // 输出经过字典转换Json对象到查看页(vie_)
			}
		} else { // 修改页面加载数据时调用
			if (repository.getId() != null && repository.getId() != 0) { // 检查主键非空
				repository = campService.schRepositoryById(repository.getId()); // 按主键查询数据
				response.getWriter().print(repository.getJson()); // 输出Json对象到修改页(upd_)
			} else { // 添加页面加载数据时调用，可设置添加页面中一些属性的预设值。
				if(repository.getPid()!=null&&repository.getPid()>0){
					ZdRepository r=new ZdRepository();
					r.setPid(repository.getPid());
					response.getWriter().print(r.getJson()); // 输出Json对象到添加页(upd_)
				}else{
					response.getWriter().print(new ZdRepository().getJson()); // 输出Json对象到添加页(upd_)
				}
			}
		}
	}

	/**
	 * 添加或修改的提交方法
	 * @param repository
	 * 数据表单Bean
	 * @param response
	 * 返回请求
	 * @throws IOException
	 */
	@RequestMapping(value = "/updRepository.action")
	public void updRepository(ZdRepository repository,
			HttpServletResponse response) throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (repository.getId() == null) { // 检测主键为空则
			campService.addRepository(repository); // 添加数据
		} else { // 主键非空则
			campService.updRepository(repository); // 修改数据
		}
		DictBuffer.updCache("ZD_REPOSITORY");// 更新缓存数据
	}
	
	/**
	 * 
	 * @param repository
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/getRepositoryById.action")
	public void getRepository(@RequestParam(value = "id") Integer id, HttpServletResponse response) throws IOException {
		JSONObject res = new JSONObject();
		if (id != null && id != 0) { // 检查主键非空
			ZdRepository repository = campService.schRepositoryById(id); // 按主键查询数据
			res.put("data", repository.getJson());
			res.put("code", "1");
			response.getWriter().print(res.toJSONString()); // 输出Json对象到修改页(upd_)
		} else {
			res.put("data", null);
			res.put("code", "0");
			response.getWriter().print(res.toJSONString());
		}
	}

	/**
	 * 删除方法
	 * @param ids
	 * 待删除数据的主键数组
	 * @param response
	 * 返回请求
	 */
	@RequestMapping(value = "/delRepository.action")
	public void delRepository(@RequestParam(value = "ids[]") Integer[] ids,
			HttpServletResponse response) {
		noCache(response); // Ajax方法设定无缓存
		List<Integer> idAllArray = new ArrayList<Integer>();// 实例化id集合
		if (ids != null) {
			for (Integer id : ids) {//循环id集合
				List<Integer> idArray = new ArrayList<Integer>();// 初始化integer集合
				if (id != null) {
					getChildren(idArray, id);// 递归查询所有子id
				}
				idAllArray.addAll(idArray);// 子id汇总到集合中
			}
		}
		Integer[] idparam = idAllArray.toArray(new Integer[idAllArray.size()]);// id List集合转换为数组
		campService.delRepository(idparam); // 删除数据
		DictBuffer.updCache("ZD_REPOSITORY");// 更新缓存数据
	}

	/**
	 * JSP页面跳转
	 * @param page
	 * jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value = "/page.action")
	public String goPage(String page) { // 放过本Ctrl的页面，不合法的跳转到error页。
		if ("index".equals(page) || "zdRepository_sch".equals(page)
				|| "zdRepository_vie".equals(page)// 判断跳转页面
				|| "zdRepository_upd".equals(page)) {// 判断跳转页面
			return "/camp/" + page;// 跳转页面
		} else {
			return "/error";// 跳转页面
		}
	}
}
