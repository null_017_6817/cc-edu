package com.qtrmoon.camp.ctrl;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qtrmoon.book.pojo.Book;
import com.qtrmoon.book.serdao.IBookService;
import com.qtrmoon.book.util.FilePath;
import com.qtrmoon.camp.pojo.TrainCamp;
import com.qtrmoon.camp.serdao.ICampService;
import com.qtrmoon.camp.vo.GroupScoreVo;
import com.qtrmoon.camp.vo.PersonScoreVo;
import com.qtrmoon.camp.vo.TeacherScoreVo;
import com.qtrmoon.common.BaseController;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.examination.pojo.ExerTopic;
import com.qtrmoon.examination.serdao.IExaminationService;
import com.qtrmoon.manager.pojo.Classes;
import com.qtrmoon.manager.pojo.Person;
import com.qtrmoon.manager.pojo.Record;
import com.qtrmoon.manager.pojo.Stud;
import com.qtrmoon.manager.serdao.IManagerService;
import com.qtrmoon.sysmanage.SysConstant;
import com.qtrmoon.sysmanage.pojo.User;
import com.qtrmoon.sysmanage.serdao.ISysmanageService;
import com.qtrmoon.template.pojo.TemplateCategory;

/**
 * 评分控制器
 */
@Controller
@RequestMapping("/camp/trainCamp")
public class TrainCampController extends BaseController {
	
	@Autowired
	private ICampService campService; // 声明模块Service实例
	
	@Autowired
	private IManagerService managerService; // 声明模块Service实例
	
	@Autowired
	private ISysmanageService sysmanageService; // 声明模块Service实例
	
	@Autowired
	private IExaminationService examinationService; // 声明模块Service实例
	
	@Autowired
	private IBookService bookService; // 声明模块Service实例

	/**
	 * 查询方法，自带分页设置。
	 * @param trainCamp
	 * 承载查询条件的Bean
	 * @param request
	 * 可用来获取非Bean内的参数
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schTrainCamp.action")
	public void schTrainCamp(TrainCamp trainCamp, String zjId,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		noCache(response);// 设置ajax无缓存
		trainCamp.setCondition(""); // 防sql注入
		trainCamp.assLike("taskName");// 拼接%
		trainCamp.addOrderDescCol("tm");
		List<TrainCamp> list = new ArrayList<TrainCamp>(); // 实例化集合
		if (zjId != null && !"".equals(zjId)) {// 判断必要参数是否为空
			List<Book> books = new ArrayList<Book>(); // 实例化集合
			getBookIds(books, Integer.valueOf(zjId)); // 获取所有子节点
			String bookids = ""; // 初始化参数bookids
			if (books != null && books.size() > 0) {// 判断集合是否为空
				for (Book bok : books) {// 循环
					if (!"".equals(bookids)) { // 判断参数是否为空
						bookids = bookids + "," + bok.getId();// 拼接参数
					} else {
						bookids += bok.getId();// 拼接参数
					}
				}
			}
			if ("".equals(bookids)) {// 判断参数是否为空
				bookids = zjId;
			}
			ExerTopic exerTopic = new ExerTopic(); // 实例化topic
			exerTopic.setType("实训营地");// 设置类型
			exerTopic.setCondition("AND book_id IN(" + bookids + ")");// 动态sql查询条件
			String topicIds = "";// 初始化参数
			List<ExerTopic> topics = examinationService.schExerTopic(exerTopic);// 查询列表
			if (topics != null && topics.size() > 0) {// 判断参数是否为空
				for (ExerTopic topic : topics) {// 循环
					if (!"".equals(topicIds)) {// 判断参数是否为空
						topicIds = topicIds + "," + topic.getId(); // 拼接参数
					} else {
						topicIds += topic.getId();// 拼接参数
					}
				}
			}
			if (!"".equals(topicIds)) {// 判断参数是否为空
				trainCamp.setCondition("AND bookid in (" + topicIds + ")");// 设置动态sql
				list = campService.schTrainCamp(trainCamp); // 调用Service查询数据
			}
		} else {
			list = campService.schTrainCamp(trainCamp); // 调用Service查询数据
		}

		/* 将数据Bean的集合转换为Json集合 */
		JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
		JSONObject obj; // 声明用于构造Bean的Json对象
		for (TrainCamp u : list) { // 循环查询的数据集构造Json数据集
			obj = u.getJsonInDict(); // 将Bean转换为Json对象
			// 任务名称
			ExerTopic topic = examinationService
					.schExerTopicById(u.getBookid()); // 根据id查询数据
			String zhangjieId = ""; //章节ID
			if (topic != null) {// 判断参数是否为空
				zhangjieId = topic.getBookId();
				obj.put("booktitle", topic.getTitle()); // 设置title
			} else {
				obj.put("booktitle", "");// 设置title
			}
			obj.put("bookid", u.getBookid());//设置任务ID
			//章节名称
			if(!zhangjieId.equals("")) {
				zhangjieId = DictBuffer.getLabel("CC_BOOK", zhangjieId);
			}
			obj.put("zhangjieId", zhangjieId);//设置章节
			User user=new User();
			user.setNumber(u.getTeacherid());
			List<User> ulist=sysmanageService.schUser(user);//查询用户
			if (ulist != null&&ulist.size()>0) {
				obj.put("teachername", ulist.get(0).getName()); // 获取老师名称
			} else {
				obj.put("teachername", "");// 获取老师名称
			}
			obj.put("teacherid", u.getTeacherid());// 获取老师id 
			Stud stu = managerService.schStudByXh(u.getStudid() == null ? ""// 根据学号查询学生
					: u.getStudid().toString());// 根据学号查询学生
			if (stu != null) {
				obj.put("studname", stu.getName()); // 显示学生名称
			} else {
				obj.put("studname", "");// 显示学生名称
			}
			obj.put("studid", u.getStudid()); //学生ID
			Stud pat = managerService.schStudByXh(u.getPartnerid() == null ? ""// 根据学号查询学生
					: u.getPartnerid().toString());// 根据学号查询学生
			if (pat != null) {
				obj.put("partnerid", pat.getName()); // 显示学生名称
			} else {
				obj.put("partnerid", "");// 显示学生名称
			}
			obj.put("_oper", u.getId()); // 为"操作列"设定主键值
			datas.add(obj); // 添加Json对象到Json集合
		}
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		JSONObject res = new JSONObject(); // 声明页面返回Json
		res.put("total", trainCamp.getDatasize()); // 设置数据总记录数
		res.put("rows", datas); // 设置数据集
		response.getWriter().print(res.toJSONString()); // 输出到页面
	}

	/**
	 * 获取章节树 
	 * @param results
	 * @param pId
	 */
	private void getBookIds(List<Book> results, int pId) {
		List<Book> books = bookService.schBookByPid(Integer.valueOf(pId)); // 查询所有子数据
		if (books != null && books.size() > 0) { // 判断集合是否为空
			results.addAll(books);// 添加到集合中
			for (Book book : books) {// 循环
				if (book != null) { // 判断对象是否为空
					getBookIds(results, book.getId());// 递归调用
				}
			}
		} else {
			return;// 返回
		}
	}

	/**
	 * 根据班级查询所有数据。
	 * @param classid 班级号
	 * @param bookid 图书id
	 * @param response
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/schTeacherEvaluate.action")
	public Object schTeacherEvaluate(Integer classid, String bookId,Integer status,
			HttpServletResponse response, HttpServletRequest request)
			throws IOException {
		noCache(response); // 设置response无缓存
		Map<String, Object> param = new HashMap<String, Object>(); // 实例化参数集合
		param.put("classid", classid);// 参数集合添加参数classid
		param.put("bookId", bookId);// 参数集合添加参数bookId
		param.put("teacherscorestatus", status);
		List<TrainCamp> list = campService.schTeacherEvaluate(param); // 调用Service查询数据
		if (list != null && list.size() > 0) { // 判断查询集合是否为空
			for (TrainCamp item : list) {// 循环
				if (item != null && classid != null) {// 判断必要参数是否为空
					item.setClassid(classid);// 设置参数
					item.setBookid(Integer.parseInt(bookId));// 设置参数
					Integer count = campService.schCount(item);// 查询汇总数
					if (count != 0) {// 如果count不等于0
						item.setNum(count);// 设置count值
					}
				}
			}
		}
		return list; // 返回结果集合
	}

	/**
	 * 根据班级查询所有数据。
	 * @param classid 班级id
	 * @param bookid 图书ID
	 * @param response
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/schStudentEvaluate.action")
	public Object schStudentEvaluate(Integer classid, String bookId,
			String studid, HttpServletResponse response,
			HttpServletRequest request) throws IOException {
		noCache(response);// 设置response无缓存
		Map<String, Object> param = new HashMap<String, Object>(); // 实例化参数集合
		param.put("classid", classid);// 参数集合添加参数classid
		param.put("bookId", bookId);// 参数集合添加参数bookId
		param.put("studid", studid);// 参数集合添加参数studid
		List<TrainCamp> list = campService.schStudentEvaluate(param); // 调用Service查询数据
		if (list != null && list.size() > 0) {// 判断查询集合知否为空
			for (TrainCamp item : list) {// 循环
				if (item != null && classid != null) {// 判断参数是否为空
					item.setClassid(classid);// 设置参数
					item.setBookid(Integer.parseInt(bookId));// 设置参数
					Integer count = campService.schCount(item);// 查询数据
					if (count != 0) {// 如果count不等于0
						item.setNum(count);// 设置count值
					}
				}
			}
		}
		return list;// 返回结果集合
	}

	/**
	 * 根据班级查询所有数据。
	 * @param classid
	 * @param response
	 * @throws IOException
	 */
	/*@ResponseBody
	@RequestMapping(value = "/weightscore.action")
	public void weightscore(TrainCamp trainCamp, HttpServletResponse response)
			throws IOException {
		noCache(response);// 设置response无缓存
		campService.weightscore(trainCamp);// 权重分数
	}*/

	/**
	 * 查询所有的班级。
	 */
	@ResponseBody
	@RequestMapping(value = "/schClasses.action")
	public Object schClasses() {
		List<Classes> list = campService.schClasses(); // 调用Service查询数据
		return list; //返回结果集
	}

	/**
	 * 点击保存按钮，更新表格数据
	 * @param response
	 * @param TrainCamp的集合
	 * 表格内容封装成对象集合
	 */
	/*@ResponseBody
	@RequestMapping(value = "/updTeacher.action")
	public List<TrainCamp> updTeachers(@RequestBody List<TrainCamp> jsonStr,
			HttpServletRequest request, HttpServletResponse response) {
		noCache(response); // Ajax方法设定无缓存
		List<TrainCamp> result = new ArrayList<TrainCamp>(); // 实例化集合对象
		Person person = (Person) request.getSession(true).getAttribute(
				SysConstant.CURRENT_USER);// 获取当前用户
		try {
			if (person != null && "2".equals(person.getState())) {// 判断参数
				result = campService.addByTeacer(jsonStr, person);// 查询数据
			}
		} catch (Exception e) {
			e.printStackTrace();// 打印异常信息
			throw new RuntimeException(e);// 抛出运行时异常
		}
		return result;// 返回结果集
	}*/

	/**
	 * 更新学生分数
	 * @param jsonStr
	 * @param stuNum
	 * @param request
	 * @param response
	 * @return
	 */
	/*@ResponseBody
	@RequestMapping(value = "/updStudCore.action")
	public List<TrainCamp> updStudCore(@RequestBody List<TrainCamp> jsonStr,
			String stuNum, HttpServletRequest request,
			HttpServletResponse response) {
		noCache(response); // Ajax方法设定无缓存
		List<TrainCamp> result = new ArrayList<TrainCamp>();// 实例化结果集
		Person person = (Person) request.getSession(true).getAttribute(
				SysConstant.CURRENT_USER);// 当前用户
		try {
			if (person != null) {// 如果当前用户不为null
				result = campService.updStudCore(jsonStr, person, stuNum);// 更新数据
			}
		} catch (Exception e) {
			e.printStackTrace();// 打印异常信息
			throw new RuntimeException(e);// 抛出运行时异常
		}
		return result;// 返回结果集
	}*/

	/**
	 * 查询方法，自带分页设置。
	 * @param stud
	 * 承载查询条件的Bean
	 * @param request
	 * 可用来获取非Bean内的参数
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schStud.action")
	public void schStud(String cnum, Stud stud, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		noCache(response);// Ajax方法设定无缓存
		stud.setCondition(" and cnumber=" + cnum); // 防sql注入
		stud.assLike("name", "sex", "info", "passwd", "roles", "login",// 拼接%
				"callExpired", "partnerClass", "partnerId", "partnerAgent",// 拼接%
				"curExercise", "myExercise", "remark");// 拼接%

		List<Stud> list = managerService.schStudAll(stud); // 调用Service查询数据
		/* 将数据Bean的集合转换为Json集合 */
		JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
		JSONObject obj; // 声明用于构造Bean的Json对象
		for (Stud u : list) { // 循环查询的数据集构造Json数据集
			obj = u.getJsonInDict(); // 将Bean转换为Json对象
			obj.put("_oper", u.getId()); // 为"操作列"设定主键值
			datas.add(obj); // 添加Json对象到Json集合
		}
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		JSONObject res = new JSONObject(); // 声明页面返回Json
		res.put("total", stud.getDatasize()); // 设置数据总记录数
		res.put("rows", datas); // 设置数据集
		response.getWriter().print(res.toJSONString()); // 输出到页面
	}

	/**
	 * 查询方法，自带分页设置。
	 * @param stud
	 * 承载查询条件的Bean
	 * @param request
	 * 可用来获取非Bean内的参数
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/schStudNotMe.action")
	public void schStudNotMe(String cnum, Stud stud,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		noCache(response);// Ajax方法设定无缓存
		stud.setCondition(" and cnumber=" + cnum); // 防sql注入
		stud.assLike("name", "sex", "info", "passwd", "roles", "login",// 拼接%
				"callExpired", "partnerClass", "partnerId", "partnerAgent",// 拼接%
				"curExercise", "myExercise", "remark");// 拼接%
		Person person = (Person) request.getSession().getAttribute(
				SysConstant.CURRENT_USER);
		String number = person.getNumber();
		stud.setNumber(number);
		List<Stud> list = managerService.schStudNotMe(stud); // 调用Service查询数据
		/* 将数据Bean的集合转换为Json集合 */
		JSONArray datas = new JSONArray(); // 构造业务数据的JSON集合
		JSONObject obj; // 声明用于构造Bean的Json对象
		for (Stud u : list) { // 循环查询的数据集构造Json数据集
			obj = u.getJsonInDict(); // 将Bean转换为Json对象
			obj.put("_oper", u.getId()); // 为"操作列"设定主键值
			datas.add(obj); // 添加Json对象到Json集合
		}
		/* 构造分页数据的JSON对象，并包装到返回页面的Json中 */
		JSONObject res = new JSONObject(); // 声明页面返回Json
		res.put("rows", datas); // 设置数据集
		response.getWriter().print(res.toJSONString()); // 输出到页面
	}

	/**
	 * 预添加or预修改or查看方法
	 * @param trainCamp
	 * 预添加时可传入一些初始值；预修改和查看时用来承载主键值
	 * @param toL
	 * 挂字典的字段是否需要转为字典Label值。true则进行转换
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/vieTrainCamp.action")
	public void vieTrainCamp(TrainCamp trainCamp, Boolean toL,
			HttpServletResponse response) throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (toL != null && toL) { // 查看页面加载数据时调用，返回的对象已进行字典转换
			if (trainCamp.getId() != null && trainCamp.getId() != 0) { // 检测主键非空
				trainCamp = campService.schTrainCampById(trainCamp.getId()); // 按主键查询数据
				JSONObject jsObj = trainCamp.getJsonInDict();// 获取json数据
				ExerTopic topic = examinationService.schExerTopicById(trainCamp.getBookid());// 任务名称
				if (topic != null) {
					jsObj.put("bookid", topic.getTitle());// 显示任务名称
				} else {
					jsObj.put("bookid", "");// 显示任务名称
				}
				User user=new User();
				user.setNumber(trainCamp.getTeacherid());
				List<User> ulist=sysmanageService.schUser(user);//查询用户
				if (ulist != null&&ulist.size()>0) {// 判断必要参数是否为null
					jsObj.put("teacherid", ulist.get(0).getName());// 显示教师名称
				} else {
					jsObj.put("teacherid", "");// 显示教师名称
				}
				Stud stu = managerService// 查询学生
						.schStudByXh(trainCamp.getStudid() == null ? ""// 查询学生
								: trainCamp.getStudid().toString());// 查询学生
				if (stu != null) {// 判断必要参数是否为null
					jsObj.put("studid", stu.getName());// 显示学生名称
				} else {
					jsObj.put("studid", "");// 显示学生名称
				}
				Stud pat = managerService// 查询学生
						.schStudByXh(trainCamp.getPartnerid() == null ? ""// 查询学生
								: trainCamp.getPartnerid().toString());// 查询学生
				if (pat != null) {// 判断必要参数是否为null
					jsObj.put("partnerid", pat.getName());// 显示学生名称
				} else {
					jsObj.put("partnerid", "");// 显示学生名称
				}
				response.getWriter().print(jsObj); // 输出经过字典转换Json对象到查看页(vie_)
			}
		} else { // 修改页面加载数据时调用
			if (trainCamp.getId() != null && trainCamp.getId() != 0) { // 检查主键非空
				trainCamp = campService.schTrainCampById(trainCamp.getId()); // 按主键查询数据
				response.getWriter().print(trainCamp.getJson()); // 输出Json对象到修改页(upd_)
			} else { // 添加页面加载数据时调用，可设置添加页面中一些属性的预设值。
				response.getWriter().print(new TrainCamp().getJson()); // 输出Json对象到添加页(upd_)
			}
		}
	}

	/**
	 * 添加或修改的提交方法
	 * @param trainCamp
	 * 数据表单Bean
	 * @param response
	 * @throws IOException
	 */
	/*@RequestMapping(value = "/updTrainCamp.action")
	public void updTrainCamp(TrainCamp trainCamp, HttpServletResponse response)
			throws IOException {
		noCache(response); // Ajax方法设定无缓存
		Integer persScore = trainCamp.getPersonalscore1()
				+ trainCamp.getPersonalscore2() + trainCamp.getPersonalscore3();// 学生自评分求和
		Integer partScore = trainCamp.getPartnerscore1()
				+ trainCamp.getPartnerscore2() + trainCamp.getPartnerscore3();// 学生互评分求和
		Integer score = persScore + partScore;// 总分求和
		TrainCamp param = new TrainCamp(); // 实例化对象
		param.setBookid(trainCamp.getBookid());// 设置bookid
		param.setStudid(trainCamp.getStudid());// 设置studid
		param.setPartnerid(trainCamp.getPartnerid());// 设置partnerid
		TrainCamp entity = campService.schTrainCampByBookIdAndQtId(param);// 查询数据

		if (entity != null) {// 必要参数不为空
			Integer t1 = entity.getTeacherscore1() == null ? 0 : entity// 老师评分1
					.getTeacherscore1();// 老师评分1
			Integer t2 = entity.getTeacherscore2() == null ? 0 : entity// 老师评分2
					.getTeacherscore2();// 老师评分2
			Integer t3 = entity.getTeacherscore3() == null ? 0 : entity// 老师评分3
					.getTeacherscore3();// 老师评分3
			score = score + t1 + t2 + t3;// 总分
			trainCamp.setTeacherid(entity.getTeacherid());// 设置teacherid
		}
		trainCamp.setScore(score);
		trainCamp.setTm(new Date());// 设置更新时间
		if (entity != null) {
			campService.updTrainCampDynaAndPart(trainCamp); // 修改数据
		} else {
			campService.addTrainCampAndPart(trainCamp); // 添加数据
		}
	}*/

	/**
	 * 删除方法
	 * @param ids
	 * 待删除数据的主键数组
	 * @param response
	 */
	@RequestMapping(value = "/delTrainCamp.action")
	public void delTrainCamp(@RequestParam(value = "ids[]") Integer[] ids,
			HttpServletResponse response) {
		noCache(response); // Ajax方法设定无缓存
		campService.delTrainCamp(ids); // 删除数据
	}

	/**
	 * JSP页面跳转
	 * @param cnumber 班级号
	 * @param bookId bookid
	 * @param topicId 题目
	 * @param zpf 自评分
	 * @param pfid 评分id
	 * @param stus 该题目已经小组评价过的学生
	 * @param stuNumber 学号
	 * @param page
	 * jsp页面文件名，无模块前缀和.jsp后缀
	 * @return 返回跳转的页面
	 */
	@RequestMapping(value = "/page.action")
	public String goPage(HttpServletRequest request, String page,
			String cnumber, String bookId, ModelMap map,String topicId,String zpf,String pfid,String stuNumber,String stus,String teacher,String templateId) { // 放过本Ctrl的页面，不合法的跳转到error页。
		if ("index".equals(page) || "trainCamp_sch".equals(page)// 判断跳转页面
				|| "trainCamp_vie".equals(page) || "trainCamp_upd".equals(page)// 判断跳转页面
				|| "xspj".equals(page) || "teacher_sch".equals(page)|| "xssxpj".equals(page)|| "xssxpj_zp".equals(page)|| "xssxpj_xzp".equals(page)|| "xssxpj_xzp_upd".equals(page)|| "xssxpj_ckpj".equals(page)|| "xssxpj_tcp".equals(page)
				|| "xssxpj_tcp_upd".equals(page)|| "trainCamp_self_score".equals(page) || "trainCamp_group_score".equals(page)  || "trainCamp_teacher_score".equals(page)||"xssxpj_teacher".equals(page)) {// 判断跳转页面
			Person person = (Person) request.getSession(true).getAttribute(
					SysConstant.CURRENT_USER); // 获取当前用户
			if (person != null) {// 当前用户不为null的场合
				map.put("cnum", person.getCnumber());// 设置cnum
				map.put("studid", person.getNumber());// 设置studid
				map.put("stuName", person.getName()); //设置学生姓名
			}
			map.put("bookId", bookId);// 设置bookid
			map.put("topicId", topicId); //设置题目
			map.put("zpf", zpf); //设置自评分
			map.put("pfid", pfid); //设置评分id
			map.put("stus", stus); //设置该题目已经小组评价过的学生
			map.put("teacher", teacher);//设置教师
			map.put("templateId", templateId);//设置模板id
			if(stuNumber!=null){
				map.put("stuNumber", stuNumber); //设置教师评页面中的学号
				map.put("stuXm", DictBuffer.getLabel("ZD_STUD", stuNumber)); //设置教师评页面中的学生姓名
			}
			return "/camp/" + page;//跳转页面
		} else {
			return "/error";// 跳转错误页面
		}
	}

	/**
	 * 获取训练营评分内容
	 * @param trainCamp
	 * 数据表单Bean
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/getTrainCamp.action")
	public void getTrainCamp(TrainCamp trainCamp, HttpServletResponse response)
			throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (trainCamp.getStudid() != null && trainCamp.getStudid() != 0) // 检测studid非空
		{
			trainCamp = campService.schTrainCampByBookIdAndQtId(trainCamp); // 按BookID和其他ID查询数据
			if (trainCamp != null) {
				response.getWriter().print(trainCamp.getJson()); // 输出到页面
			}
		}
	}

	/**
	 * 查询录音列表 
	 * @param record
	 * @param request
	 * @param response
	 */
	@SuppressWarnings("finally")
	@ResponseBody
	@RequestMapping(value = "/getRecords.action")
	public Map<String, Object> getRecords(Record record,
			HttpServletRequest request, HttpServletResponse response) {
		noCache(response); // Ajax方法设定无缓存
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			if (record != null && record.getTaskId() != null// 判断必要参数（任务id）是否为空
					&& !"".equals(record.getTaskId())// 判断必要参数（任务id）是否为空
					&& record.getToNum() != null// 判断必要参数（接听人）是否为空
					&& !"".equals(record.getToNum())) {// 判断必要参数（接听人）是否为空
				record.setCondition("");// 防止sql注入
				List<Record> records = managerService.schRecord(record);// 查询录音列表
				if (records != null && records.size() > 0) {// 判断集合是否为空
					for (int i = 0; i < records.size(); i++) {// 循环
						Record re = records.get(i);// 获取第i个元素
						if (re != null) {// 当前循环录音对象不为null的场合
							if (re.getVoice() != null) {// 录音不为null的场合
								String projectName = request.getContextPath();// 获取项目名称
								re.setVoice(projectName
										+ "/camp/trainCamp/downloadVoice.action?wav="// 设置录音
										+ re.getVoice());// 设置录音
							}
							if (re.getFromNum() != null) {// 如果拨打人不为空
								Stud stu = managerService.schStudByXh(re// 根据学号查询学生
										.getFromNum());// 根据学号查询学生
								re.setFromNum(stu.getName() + "("
										+ stu.getNumber() + ")");//设置显示的拨打人（姓名+学号显示）
							}
							if (re.getToNum() != null) {// 如果接听人不为空
								Stud stu = managerService.schStudByXh(re// 根据学号查询学生
										.getToNum());// 根据学号查询学生
								re.setToNum(stu.getName() + "("//设置显示的接听人（姓名+学号显示）
										+ stu.getNumber() + ")");//设置显示的接听人（姓名+学号显示）
							}
						}
					}
				}
				result.put("success", true);// 设置返回成功状态
				result.put("data", records);// 设置返回数据
			} else {
				result.put("success", false);// 设置返回成功状态
				result.put("msg", "必要参数为空");// 设置返回msg
			}
		} catch (Exception e) {
			e.printStackTrace();// 打印异常信息
			result.put("success", false);// 设置返回成功状态
			result.put("msg", "服务器异常");// 设置返回msg
		} finally {
			return result; // 返回结果
		}
	}

	
	/**
	 * 查询录音个数
	 * @param record
	 * @param request
	 * @param response
	 */
	@ResponseBody
	@RequestMapping(value = "/getRecordsSize.action")
	public Map<String, Object> getRecordsSize(Record record,
			HttpServletRequest request, HttpServletResponse response) {
		noCache(response); // Ajax方法设定无缓存
		Map<String, Object> result = new HashMap<String, Object>();
		List<Record> records = managerService.schRecord(record);// 查询录音列表
		result.put("data", records.size());// 设置返回数据
		return result; // 返回结果
	}
	
	/**
	 * 获取录音 
	 * @param wav
	 * @param request
	 * @param response
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	@RequestMapping(value = "/downloadVoice.action")
	public void downloadVoice(String wav, HttpServletRequest request,
			HttpServletResponse response) {
		String path = FilePath.getFilePath("recordServer") + wav;// 拼接路径
		try {
			FileInputStream in = new FileInputStream(path);// 文件对象输入流
			ServletOutputStream out = response.getOutputStream();// 输出流
			IOUtils.copy(in, out);// 以流的形式展示到前台
			IOUtils.closeQuietly(in);// 关闭流
			IOUtils.closeQuietly(out);// 关闭流
		} catch (Exception e) {
			e.printStackTrace();// 打印异常信息
			logger.info("获取通话录音流失败,录音名称为：" + path);// 输出log
		}

	}
	
	
	/**
	 * 查看学生自评分
	 * @param trainCamp 数据表单Bean
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/viewPersonScoreDetail.action")
	public void viewPersonScoreDetail(TrainCamp trainCamp, HttpServletResponse response)
			throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (trainCamp.getStudid() != null && trainCamp.getStudid() != 0 && trainCamp.getBookid() != null) {// 检测非空
			PersonScoreVo personScoreVo = campService.getStudentPersonScore(trainCamp.getBookid(),trainCamp.getStudid());//调用服务获取自评分
			response.getWriter().print(com.alibaba.fastjson.JSONObject.toJSONString(personScoreVo)); // 输出到页面
		}
	}
	
	/**
	 * 查看教师评分
	 * @param trainCamp 数据表单Bean
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/viewTeacherScoreDetail.action")
	public void viewTeacherScoreDetail(TrainCamp trainCamp, HttpServletResponse response)
			throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (trainCamp.getStudid() != null && trainCamp.getStudid() != 0 && trainCamp.getBookid() != null) {// 检测非空
			TeacherScoreVo teacherScoreVo = campService.getTeacherScore(trainCamp.getBookid(),trainCamp.getStudid(),trainCamp.getTeacherid());//调用服务获取自评分
			response.getWriter().print(com.alibaba.fastjson.JSONObject.toJSONString(teacherScoreVo)); // 输出到页面
		}
	}
	
	
   /** 查看小组评分
	 * @param trainCamp 数据表单Bean
	 * @param xzpfrid 小组评分人ID
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/viewGroupScoreDetail.action")
	public void viewGroupScoreDetail(TrainCamp trainCamp, @RequestParam(name="xzpfrid",required=false) Integer xzpfrid, HttpServletResponse response)
			throws IOException {
		noCache(response); // Ajax方法设定无缓存
		if (trainCamp.getStudid() != null && trainCamp.getStudid() != 0 && trainCamp.getBookid() != null) {// 检测非空
			GroupScoreVo gScoreVo = campService.getGroupScore(trainCamp.getBookid(),trainCamp.getStudid(),xzpfrid);//调用服务获取自评分
			response.getWriter().print(com.alibaba.fastjson.JSONObject.toJSONString(gScoreVo)); // 输出到页面
		}
	}
	
	/**
	 * 查询教师管理的班级。
	 * @throws IOException 
	 */
	@ResponseBody
	@RequestMapping(value = "/schMyClasses.action")
	public void schMyClasses(HttpServletRequest request,HttpServletResponse response) throws IOException {
		Person person = (Person) request.getSession(true).getAttribute(SysConstant.CURRENT_USER); // 获取当前用户
		User user=new User();	//实例化用户
		user.setNumber(person.getNumber());	//用户设置学号（教师工号）
		List<User> list=sysmanageService.schUser(user);
		String[] classes=list.get(0).getCnumber().split(",");	//教师所管理的班级号数组
		List<Classes> clist=new ArrayList<Classes>();
		if(classes.length>0){
			for(int i=0;i<classes.length;i++){
				Classes c=new Classes();
				c.setNumber(classes[i]);
				c.setName(DictBuffer.getLabel("CC_CLASSES", classes[i].toString()));
				clist.add(c);
			}
		}
		//将数据Bean的集合转换为Json集合
		JSONArray datas=new JSONArray();									//构造业务数据的JSON集合
		JSONObject obj;														//声明用于构造Bean的Json对象
		for(Classes u:clist){												//循环查询的数据集构造Json数据集
			obj=u.getJsonInDict();											//将Bean转换为Json对象
			datas.add(obj);													//添加Json对象到Json集合
		}
		JSONObject res=new JSONObject();									//声明页面返回Json
		res.put("classes", datas);	
		response.getWriter().print(res.toJSONString());						//输出到页面
	}
}
