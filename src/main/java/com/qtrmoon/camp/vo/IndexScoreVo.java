package com.qtrmoon.camp.vo;

/**
 * 指标分数
 */
public class IndexScoreVo {
	private String indexContent;//指标内容
	private String score;//分数
	
	/** 获取指标内容 */
	public String getIndexContent() {
		return indexContent;
	}
	/** 设置指标内容 */
	public void setIndexContent(String indexContent) {
		this.indexContent = indexContent;
	}
	/** 获取分数 */
	public String getScore() {
		return score;
	}
	/** 设置分数 */
	public void setScore(String score) {
		this.score = score;
	}
}
