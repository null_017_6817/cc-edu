package com.qtrmoon.camp.vo;

/**
 * 教师评分
 */
public class TeacherScoreVo{
	private String teacherName; //教师姓名
	private ScoreVo scoreVo; //评分
	
	/** 获取教师姓名 */
	public String getTeacherName() {
		return teacherName;
	}
	/** 设置教师姓名 */
	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}
	/** 设置评分 */
	public ScoreVo getScoreVo() {
		return scoreVo;
	}
	/** 设置评分 */
	public void setScoreVo(ScoreVo scoreVo) {
		this.scoreVo = scoreVo;
	}
}
