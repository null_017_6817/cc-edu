package com.qtrmoon.camp.vo;


/**
 * 小组评分Vo
 *
 */
public class GroupScoreVo {

	private ScoreVo scoreVo; //评分
	
	/** 获取评分 */
	public ScoreVo getScoreVo() {
		return scoreVo;
	}
	/** 设置评分 */
	public void setScoreVo(ScoreVo scoreVo) {
		this.scoreVo = scoreVo;
	}
}
