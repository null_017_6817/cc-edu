package com.qtrmoon.camp.vo;

import java.util.List;

/**
 * 评分详情
 */
public class ScoreDetailVo {
	private String category; //分类
	private Integer ratio; //比例
	private List<IndexScoreVo> indexScores; //指标评分
	
	/** 获取分类 */
	public String getCategory() {
		return category;
	}
	/** 设置取分类 */
	public void setCategory(String category) {
		this.category = category;
	}
	/** 获取指标评分 */
	public List<IndexScoreVo> getIndexScores() {
		return indexScores;
	}
	/** 设置指标评分 */
	public void setIndexScores(List<IndexScoreVo> indexScores) {
		this.indexScores = indexScores;
	}
	/** 获取比例 */
	public Integer getRatio() {
		return ratio;
	}
	/** 设置比例 */
	public void setRatio(Integer ratio) {
		this.ratio = ratio;
	}
}
