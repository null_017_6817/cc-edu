package com.qtrmoon.camp.vo;

/**
 * 自评分
 */
public class PersonScoreVo{
	private String stuName; //学生姓名
	private String stuNo; //学生学号
	private ScoreVo scoreVo; //评分
	
	/** 获取学生姓名 */
	public String getStuName() {
		return stuName;
	}
	/** 设置学生姓名 */
	public void setStuName(String stuName) {
		this.stuName = stuName;
	}
	/** 获取学生学号 */
	public String getStuNo() {
		return stuNo;
	}
	/** 设置学生学号 */
	public void setStuNo(String stuNo) {
		this.stuNo = stuNo;
	}
	/** 设置评分 */
	public ScoreVo getScoreVo() {
		return scoreVo;
	}
	/** 设置评分 */
	public void setScoreVo(ScoreVo scoreVo) {
		this.scoreVo = scoreVo;
	}
}
