package com.qtrmoon.camp.vo;

import java.util.List;

import org.json.simple.JSONObject;

import com.qtrmoon.camp.pojo.Score;

/**
 * 评分Vo
 *
 */
public class ScoreVo {
	private String totalScore; //总分
	private String ratio; //比例
	private String finalScore; //最终得分
	private String scoreDate;//评分日期
	private String ydgjzc;//有待改进之处
	private String pfxsxh;//评分学生学号
	private List<ScoreDetailVo> scoreDetails; //评分明细
	private List<JSONObject> memberScores; //小组成员评分列表
	
	/** 获取总分 */
	public String getTotalScore() {
		return totalScore;
	}
	/** 设置总分 */
	public void setTotalScore(String totalScore) {
		this.totalScore = totalScore;
	}
	/** 获取比例 */
	public String getRatio() {
		return ratio;
	}
	/** 设置比例 */
	public void setRatio(String ratio) {
		this.ratio = ratio;
	}
	/** 获取最终得分 */
	public String getFinalScore() {
		return finalScore;
	}
	/** 设置最终得分 */
	public void setFinalScore(String finalScore) {
		this.finalScore = finalScore;
	}
	/** 获取评分明细 */
	public List<ScoreDetailVo> getScoreDetails() {
		return scoreDetails;
	}
	/** 设置评分明细 */
	public void setScoreDetails(List<ScoreDetailVo> scoreDetails) {
		this.scoreDetails = scoreDetails;
	}
	/** 获取评分日期 */
	public String getScoreDate() {
		return scoreDate;
	}
	/** 设置评分日期 */
	public void setScoreDate(String scoreDate) {
		this.scoreDate = scoreDate;
	}
	/** 获取有待改进之处 */
	public String getYdgjzc() {
		return ydgjzc;
	}
	/** 设置有待改进之处 */
	public void setYdgjzc(String ydgjzc) {
		this.ydgjzc = ydgjzc;
	}
	/** 获取小组成员评分列表 */
	public List<JSONObject> getMemberScores() {
		return memberScores;
	}
	/** 设置小组成员评分列表 */
	public void setMemberScores(List<JSONObject> memberScores) {
		this.memberScores = memberScores;
	}
	/** 获取评分学生学号 */
	public String getPfxsxh() {
		return pfxsxh;
	}
	/** 设置评分学生学号 */
	public void setPfxsxh(String pfxsxh) {
		this.pfxsxh = pfxsxh;
	}
	
}
