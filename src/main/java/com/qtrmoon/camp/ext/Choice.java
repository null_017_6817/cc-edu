package com.qtrmoon.camp.ext;

import java.io.Serializable;

/**
 * 选项对象bean
 */
public class Choice implements Serializable{
	private static final long serialVersionUID = 1952603559538090318L;
	private String chooseTxt;		// 选项内容；
	private String isChoose;		// 是否选择；
	private String chooseType;		// 选择则题类型
	
	/**
	 * 获取选项内容
	 * @return
	 */
	public String getChooseTxt() {
		return chooseTxt;
	}
	
	/**
	 * 设置选项内容
	 * @param chooseTxt
	 */
	public void setChooseTxt(String chooseTxt) {
		this.chooseTxt = chooseTxt;
	}
	
	/**
	 * 获取是否选择
	 * @return
	 */
	public String getIsChoose() {
		return isChoose;
	}
	/**
	 * 设置是否选择
	 * @param isChoose
	 */
	public void setIsChoose(String isChoose) {
		this.isChoose = isChoose;
	}
	
	/**
	 * 获取选择类型
	 * @return
	 */
	public String getChooseType() {
		return chooseType;
	}
	
	/**
	 * 设置选择类型
	 * @param chooseType
	 */
	public void setChooseType(String chooseType) {
		this.chooseType = chooseType;
	}
}
