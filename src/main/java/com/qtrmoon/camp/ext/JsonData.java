package com.qtrmoon.camp.ext;

import java.io.Serializable;
import java.util.List;

/**
 * json数据bean
 */
public class JsonData implements Serializable{
	private static final long serialVersionUID = 9198218471008538895L;//序列化序列号
	private List<DxwjJson> dxwjJsons;// 内容

	/**
	 * 获取内容
	 * @return
	 */
	public List<DxwjJson> getDxwjJsons() {
		return dxwjJsons;
	}

	/**
	 * 设置内容
	 * @param dxwjJsons
	 */
	public void setDxwjJsons(List<DxwjJson> dxwjJsons) {
		this.dxwjJsons = dxwjJsons;
	}
}
