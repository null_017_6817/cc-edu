package com.qtrmoon.camp.ext;

import java.io.Serializable;
import java.util.List;

/**
 * 问卷对象bean
 */
public class DxwjJson implements Serializable{
	
	private static final long serialVersionUID = -1312125042354029634L;
	private String type;			// 题型 1：问答题，2：单选，3：多选
	private String question;		// 题干
	private String answer;			// 答案
	private List<Choice> choices;	// 选项
	
	/**
	 * 获取题型
	 * @return
	 */
	public String getType() {
		return type;
	}
	
	/**
	 * 设置题型
	 * @param type
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	/**
	 * 获取题干
	 * @return
	 */
	public String getQuestion() {
		return question;
	}
	
	/**
	 * 设置题干
	 * @param question
	 */
	public void setQuestion(String question) {
		this.question = question;
	}
	
	/**
	 * 获取答案
	 * @return
	 */
	public String getAnswer() {
		return answer;
	}
	
	/**
	 * 设置答案
	 * @param answer
	 */
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	
	/**
	 * 获取选项
	 * @return
	 */
	public List<Choice> getChoices() {
		return choices;
	}
	
	/**
	 * 设置选项
	 * @param choices
	 */
	public void setChoices(List<Choice> choices) {
		this.choices = choices;
	}
}
