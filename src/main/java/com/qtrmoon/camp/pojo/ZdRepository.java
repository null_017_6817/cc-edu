package com.qtrmoon.camp.pojo;
import java.util.List;

import org.json.simple.JSONObject;

import com.qtrmoon.common.PageForm;
import com.qtrmoon.dictionary.DictBuffer;

/** 实体类 */
public class ZdRepository extends PageForm{
	
	private Integer id;// id
	private Integer pid;// 父id
	private String label;// label
	private Integer bookId; // bookId
	
	private List<ZdRepository> childList;// 子对象集合
	//Constructors
	/** default constructor */
	public ZdRepository() {
	
	}	
	//getter and setter
	/** 获取id */
	public Integer getId() {
		return this.id;
	}
	/** 设置id */
	public void setId(Integer id) {
		this.id = id;
	}
	/** 获取父id */
	public Integer getPid() {
		return this.pid;
	}
	/** 设置父id */
	public void setPid(Integer pid) {
		this.pid = pid;
	}
	/** 获取label */
	public String getLabel() {
		return this.label;
	}
	/** 设置label */
	public void setLabel(String label) {
		this.label = label;
	}
	/**
	 * 获取子节点集合
	 * @return
	 */
	public List<ZdRepository> getChildList() {
		return childList;
	}
	
	public Integer getBookId() {
		return bookId;
	}
	public void setBookId(Integer bookId) {
		this.bookId = bookId;
	}
	/**
	 * 设置子节点集合
	 * @return
	 */
	public void setChildList(List<ZdRepository> childList) {
		this.childList = childList;
	}
	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("pid", pid);
		obj.put("label", label);
		obj.put("bookId", bookId);
		return obj;
	}
	
	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		if(pid != null){
			obj.put("pidLabel", DictBuffer.getLabel("ZD_REPOSITORY", pid.toString()));
		}
		obj.put("label", label);
		obj.put("bookId", bookId);
		return obj;
	}
}
