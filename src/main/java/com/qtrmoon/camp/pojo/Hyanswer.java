package com.qtrmoon.camp.pojo;
import java.util.Date;

import org.json.simple.JSONObject;

import com.qtrmoon.camp.ext.JsonData;
import com.qtrmoon.common.PageForm;
import com.qtrmoon.toolkit.DateTransfer;

/** 行业答案实体类 */
public class Hyanswer extends PageForm{
	//Fields
	
	private Integer id;// 主键
	private String fromno;// 拨打人学号
	private String fromagentid;// 拨打人坐席号
	private String tono;// 接听人学号
	private String toagentid;// 接听人坐席id
	private Integer taskid;// 任务id（电信行业场景id）
	private String type;// 类型
	private JsonData content;// 内容
	private String rid; // 关联id（关联录音表里的rid）
	private Date createtime;// 创建时间
	private String createtimeBeg,createtimeEnd;//时间条件的跨度查询属性
	private Date updatetime;// 修改时间
	private String updatetimeBeg,updatetimeEnd;//时间条件的跨度查询属性
	//Constructors
	/** default constructor */
	public Hyanswer() {
	
	}	
	//getter and setter
	/** 获取主键 */
	public Integer getId() {
		return this.id;
	}
	/** 设置主键 */
	public void setId(Integer id) {
		this.id = id;
	}
	/** 获取拨打人学号 */
	public String getFromno() {
		return this.fromno;
	}
	/** 设置拨打人学号 */
	public void setFromno(String fromno) {
		this.fromno = fromno;
	}
	/** 获取拨打人坐席号 */
	public String getFromagentid() {
		return this.fromagentid;
	}
	/** 设置拨打人坐席号 */
	public void setFromagentid(String fromagentid) {
		this.fromagentid = fromagentid;
	}
	/** 获取接听人学号 */
	public String getTono() {
		return this.tono;
	}
	/** 设置接听人学号 */
	public void setTono(String tono) {
		this.tono = tono;
	}
	/** 获取接听人坐席id */
	public String getToagentid() {
		return this.toagentid;
	}
	/** 设置接听人坐席id */
	public void setToagentid(String toagentid) {
		this.toagentid = toagentid;
	}
	/** 获取任务id（电信行业场景id） */
	public Integer getTaskid() {
		return this.taskid;
	}
	/** 设置任务id（电信行业场景id） */
	public void setTaskid(Integer taskid) {
		this.taskid = taskid;
	}
	/** 获取类型 */
	public String getType() {
		return this.type;
	}
	/** 设置类型 */
	public void setType(String type) {
		this.type = type;
	}
	/** 获取创建时间 */
	public Date getCreatetime() {
		return this.createtime;
	}
	/** 设置创建时间 */
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	/** 设定[创建时间]时间转载字段值，页面提交时由springmvc自动装载 */
	public void setCreatetimestr(String tm) {
		this.createtime=_getTime(tm);
	}
	/** 获取[创建时间]时间起始条件 */
	public String getCreatetimeBeg() {
		return createtimeBeg;
	}
	/** 获取[创建时间]时间起始条件(Dao/Mapper查询时调用) */
	public Date getCreatetimeBegDate() {
		return _getBegDate(createtimeBeg);
	}
	/** 设置[创建时间]时间起始条件(表单提交时自动装载) */
	public void setCreatetimeBeg(String createtimeBeg) {
		this.createtimeBeg = createtimeBeg;
	}
	
	/** 获取[创建时间]时间结束条件 */
	public String getCreatetimeEnd() {
		return createtimeEnd;
	}
	/** 获取[创建时间]时间结束条件(Dao/Mapper查询时调用) */
	public Date getCreatetimeEndDate() {
		return _getEndDate(createtimeEnd);
	}
	/** 设置[创建时间]时间结束条件(表单提交时自动装载) */
	public void setCreatetimeEnd(String createtimeEnd) {
		this.createtimeEnd = createtimeEnd;
	}
	/** 获取修改时间 */
	public Date getUpdatetime() {
		return this.updatetime;
	}
	/** 设置修改时间 */
	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}
	/** 设定[修改时间]时间转载字段值，页面提交时由springmvc自动装载 */
	public void setUpdatetimestr(String tm) {
		this.updatetime=_getTime(tm);
	}
	/** 获取[修改时间]时间起始条件 */
	public String getUpdatetimeBeg() {
		return updatetimeBeg;
	}
	/** 获取[修改时间]时间起始条件(Dao/Mapper查询时调用) */
	public Date getUpdatetimeBegDate() {
		return _getBegDate(updatetimeBeg);
	}
	/** 设置[修改时间]时间起始条件(表单提交时自动装载) */
	public void setUpdatetimeBeg(String updatetimeBeg) {
		this.updatetimeBeg = updatetimeBeg;
	}
	
	/** 获取[修改时间]时间结束条件 */
	public String getUpdatetimeEnd() {
		return updatetimeEnd;
	}
	/** 获取[修改时间]时间结束条件(Dao/Mapper查询时调用) */
	public Date getUpdatetimeEndDate() {
		return _getEndDate(updatetimeEnd);
	}
	/** 设置[修改时间]时间结束条件(表单提交时自动装载) */
	public void setUpdatetimeEnd(String updatetimeEnd) {
		this.updatetimeEnd = updatetimeEnd;
	}
	/** 获取内容 */
	public JsonData getContent() {
		return content;
	}
	
	/** 设置内容 */
	public void setContent(JsonData content) {
		this.content = content;
	}
	
	/** 获取关联id */
	public String getRid() {
		return rid;
	}
	
	/** 设置关联id */
	public void setRid(String rid) {
		this.rid = rid;
	}
	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("fromno", fromno);
		obj.put("fromagentid", fromagentid);
		obj.put("tono", tono);
		obj.put("toagentid", toagentid);
		obj.put("taskid", taskid);
		obj.put("type", type);
		obj.put("content", content);
		obj.put("rid", rid);
		//时间字段处理。createtime查询列表页使用，createtimestr修改页使用
		obj.put("createtime", DateTransfer.toString(createtime,"yyyy/MM/dd"));
		obj.put("createtimestr", DateTransfer.toString(createtime,"yyyy/MM/dd"));
		//时间字段处理。updatetime查询列表页使用，updatetimestr修改页使用
		obj.put("updatetime", DateTransfer.toString(updatetime,"yyyy/MM/dd"));
		obj.put("updatetimestr", DateTransfer.toString(updatetime,"yyyy/MM/dd"));
		return obj;
	}
	
	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("fromno", fromno);
		obj.put("fromagentid", fromagentid);
		obj.put("tono", tono);
		obj.put("toagentid", toagentid);
		obj.put("taskid", taskid);
		obj.put("type", type);
		obj.put("content", content);
		obj.put("rid", rid);
		//时间字段处理。createtime查询列表页使用，createtimestr修改页使用
		obj.put("createtime", DateTransfer.toString(createtime,"yyyy/MM/dd"));
		obj.put("createtimestr", DateTransfer.toString(createtime,"yyyy/MM/dd"));
		//时间字段处理。updatetime查询列表页使用，updatetimestr修改页使用
		obj.put("updatetime", DateTransfer.toString(updatetime,"yyyy/MM/dd"));
		obj.put("updatetimestr", DateTransfer.toString(updatetime,"yyyy/MM/dd"));
		return obj;
	}
}
