package com.qtrmoon.camp.pojo;
import java.util.Date;
import java.util.Set;
import java.util.HashSet;
import org.json.simple.JSONObject;
import com.qtrmoon.common.PageForm;
import com.qtrmoon.toolkit.DateTransfer;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.dictionary.bsd.DictBean;

/** 评分表实体类 */
public class Score extends PageForm{
	//Fields
	
	private Integer id;// 主键
	private Integer mbid;// 模板ID
	private String pfxsxh;// 评分学生学号
	private String bpfxsxh;// 被评分学生学号
	private Integer pflx;// 评分类型（自评、互评、教师评）
	private String pfjg;// 评分结果
	private String zf;// 总分
	private Date pfrq;// 评分日期
	private String pfrqBeg,pfrqEnd;//时间条件的跨度查询属性
	private Integer pfrw;// 评分任务
	private String ydgjzc;//有待改进之处
	//Constructors
	/** default constructor */
	public Score() {
	
	}	
	//getter and setter
	/** 获取主键 */
	public Integer getId() {
		return this.id;
	}
	/** 设置主键 */
	public void setId(Integer id) {
		this.id = id;
	}
	/** 获取模板ID */
	public Integer getMbid() {
		return this.mbid;
	}
	/** 设置模板ID */
	public void setMbid(Integer mbid) {
		this.mbid = mbid;
	}
	/** 获取评分学生学号 */
	public String getPfxsxh() {
		return this.pfxsxh;
	}
	/** 设置评分学生学号 */
	public void setPfxsxh(String pfxsxh) {
		this.pfxsxh = pfxsxh;
	}
	/** 获取被评分学生学号 */
	public String getBpfxsxh() {
		return this.bpfxsxh;
	}
	/** 设置被评分学生学号 */
	public void setBpfxsxh(String bpfxsxh) {
		this.bpfxsxh = bpfxsxh;
	}
	/** 获取评分类型（自评、互评、教师评） */
	public Integer getPflx() {
		return this.pflx;
	}
	/** 设置评分类型（自评、互评、教师评） */
	public void setPflx(Integer pflx) {
		this.pflx = pflx;
	}
	/** 获取评分结果 */
	public String getPfjg() {
		return this.pfjg;
	}
	/** 设置评分结果 */
	public void setPfjg(String pfjg) {
		this.pfjg = pfjg;
	}
	/** 获取总分 */
	public String getZf() {
		return this.zf;
	}
	/** 设置总分 */
	public void setZf(String zf) {
		this.zf = zf;
	}
	/** 获取评分日期 */
	public Date getPfrq() {
		return this.pfrq;
	}
	/** 设置评分日期 */
	public void setPfrq(Date pfrq) {
		this.pfrq = pfrq;
	}
	/** 设定[评分日期]时间转载字段值，页面提交时由springmvc自动装载 */
	public void setPfrqstr(String tm) {
		this.pfrq=_getTime(tm);
	}
	/** 获取[评分日期]时间起始条件 */
	public String getPfrqBeg() {
		return pfrqBeg;
	}
	/** 获取[评分日期]时间起始条件(Dao/Mapper查询时调用) */
	public Date getPfrqBegDate() {
		return _getBegDate(pfrqBeg);
	}
	/** 设置[评分日期]时间起始条件(表单提交时自动装载) */
	public void setPfrqBeg(String pfrqBeg) {
		this.pfrqBeg = pfrqBeg;
	}
	
	/** 获取[评分日期]时间结束条件 */
	public String getPfrqEnd() {
		return pfrqEnd;
	}
	/** 获取[评分日期]时间结束条件(Dao/Mapper查询时调用) */
	public Date getPfrqEndDate() {
		return _getEndDate(pfrqEnd);
	}
	/** 设置[评分日期]时间结束条件(表单提交时自动装载) */
	public void setPfrqEnd(String pfrqEnd) {
		this.pfrqEnd = pfrqEnd;
	}
	/** 获取评分任务 */
	public Integer getPfrw() {
		return this.pfrw;
	}
	/** 设置评分任务 */
	public void setPfrw(Integer pfrw) {
		this.pfrw = pfrw;
	}
	/** 获取有待改进之处 */
	public String getYdgjzc() {
		return ydgjzc;
	}
	/** 设置有待改进之处 */
	public void setYdgjzc(String ydgjzc) {
		this.ydgjzc = ydgjzc;
	}
	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("mbid", mbid);
		obj.put("pfxsxh", pfxsxh);
		obj.put("bpfxsxh", bpfxsxh);
		obj.put("pflx", pflx);
		obj.put("pfjg", pfjg);
		obj.put("zf", zf);
		//时间字段处理。pfrq查询列表页使用，pfrqstr修改页使用
		obj.put("pfrq", DateTransfer.toString(pfrq,"yyyy/MM/dd"));
		obj.put("pfrqstr", DateTransfer.toString(pfrq,"yyyy/MM/dd"));
		obj.put("pfrw", pfrw);
		obj.put("ydgjzc", ydgjzc);
		return obj;
	}
	
	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("mbid", mbid);
		obj.put("pfxsxh", pfxsxh);
		
		if(pfxsxh != null) {
			obj.put("pfxsxm", DictBuffer.getLabel("ZD_STUD", pfxsxh.toString()));
		}
		obj.put("bpfxsxh", bpfxsxh);
		obj.put("pflx", pflx);
		obj.put("pfjg", pfjg);
		obj.put("zf", zf);
		//时间字段处理。pfrq查询列表页使用，pfrqstr修改页使用
		obj.put("pfrq", DateTransfer.toString(pfrq,"yyyy/MM/dd"));
		obj.put("pfrqstr", DateTransfer.toString(pfrq,"yyyy/MM/dd"));
		if(bpfxsxh!=null){
			obj.put("bpfxsxm", DictBuffer.getLabel("ZD_STUD", bpfxsxh.toString()));
			obj.put("bpfxsbj", DictBuffer.getLabel("ZD_GETCNUMBER", bpfxsxh.toString()));
			obj.put("bpfxstx", DictBuffer.getLabel("ZD_PHOTO", bpfxsxh.toString()));
		}
		if(pfxsxh!=null){
			obj.put("pfrxm", DictBuffer.getLabel("ZD_USER", pfxsxh.toString()));
		}
		obj.put("ydgjzc", ydgjzc);
		return obj;
	}
}
