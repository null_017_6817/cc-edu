package com.qtrmoon.camp.pojo;

import java.util.Date;

import org.json.simple.JSONObject;

import com.qtrmoon.common.PageForm;
import com.qtrmoon.dictionary.DictBuffer;
import com.qtrmoon.toolkit.DateTransfer;

/** 实训营地评分Object类 */
public class TrainCamp extends PageForm {

	private Integer id; // 主键
	private Integer bookid; // 任务
	private Integer studid; // 学生id
	private String name; // 学生姓名
	private Integer partnerid; // 组队学生
	private String teacherid; // 评分老师
	private String personalscore1; // 自评分
	private Integer personalscore2; // 任务完成互评
	private Integer personalscore3; // 小组合作教师评
	private String partnerscore1; // 互评分
	private Integer partnerscore2; // 任务完成互评
	private Integer partnerscore3; // 小组合作教师评
	private String teacherscore1; // 师评分

	private Integer teacherscore2; // 任务完成互评
	private Integer teacherscore3; // 小组合作教师评
	private Date tm; // 时间
	private String tmString; // 字符串行时间
	private float score; // 总分
	private Integer classid; // 班级id
	private Integer num; // 录音条数
	private Integer personweight; // 自评权重
	private Integer partnerweight; // 互评权重
	private Integer teacherweight; // 老师权重

	private String partnername; // 互评者名称
	private Integer teacherscorestatus;//教师评分状态0未评分，1已评分
	private String teacherscores;//教师评分，查询用
	/** view ***/
	private String taskName; // 任务名称
	private String photo_url;//头像

	// Constructors
	/** default constructor */
	public TrainCamp() {

	}

	/** 获取主键 */
	public Integer getId() {
		return this.id;
	}

	/** 获取姓名 */
	public String getName() {
		return name;
	}

	/** 设置姓名 */
	public void setName(String name) {
		this.name = name;
	}

	/** 获取前台时间展示 */
	public String getTmString() {
		return tmString;
	}

	/** 设置前台时间展示 */
	public void setTmString(String tmString) {
		this.tmString = tmString;
	}

	/** 获取总分 */
	public float getScore() {
		return score;
	}

	/** 设置总分 */
	public void setScore(float score) {
		this.score = score;
	}

	/** 设置主键 */
	public void setId(Integer id) {
		this.id = id;
	}

	/** 获取任务 */
	public Integer getBookid() {
		return this.bookid;
	}

	/** 设置任务 */
	public void setBookid(Integer bookid) {
		this.bookid = bookid;
	}

	/** 获取学生id */
	public Integer getStudid() {
		return this.studid;
	}

	/** 设置学生id */
	public void setStudid(Integer studid) {
		this.studid = studid;
	}

	/** 获取组队学生 */
	public Integer getPartnerid() {
		return this.partnerid;
	}

	/** 设置组队学生 */
	public void setPartnerid(Integer partnerid) {
		this.partnerid = partnerid;
	}

	/** 获取评分老师 */
	public String getTeacherid() {
		return this.teacherid;
	}

	/** 设置评分老师 */
	public void setTeacherid(String teacherid) {
		this.teacherid = teacherid;
	}

	/** 获取知识掌握自评 */
	public String getPersonalscore1() {
		return this.personalscore1;
	}

	/** 设置知识掌握自评 */
	public void setPersonalscore1(String personalscore1) {
		this.personalscore1 = personalscore1;
	}

	/** 获取任务完成互评 */
	public Integer getPersonalscore2() {
		return this.personalscore2;
	}

	/** 设置任务完成互评 */
	public void setPersonalscore2(Integer personalscore2) {
		this.personalscore2 = personalscore2;
	}

	/** 获取小组合作教师评 */
	public Integer getPersonalscore3() {
		return this.personalscore3;
	}

	/** 设置小组合作教师评 */
	public void setPersonalscore3(Integer personalscore3) {
		this.personalscore3 = personalscore3;
	}

	/** 获取知识掌握 */
	public String getPartnerscore1() {
		return this.partnerscore1;
	}

	/** 设置知识掌握 */
	public void setPartnerscore1(String partnerscore1) {
		this.partnerscore1 = partnerscore1;
	}

	/** 获取任务完成互评 */
	public Integer getPartnerscore2() {
		return this.partnerscore2;
	}

	/** 设置任务完成互评 */
	public void setPartnerscore2(Integer partnerscore2) {
		this.partnerscore2 = partnerscore2;
	}

	/** 获取小组合作教师评 */
	public Integer getPartnerscore3() {
		return this.partnerscore3;
	}

	/** 设置小组合作教师评 */
	public void setPartnerscore3(Integer partnerscore3) {
		this.partnerscore3 = partnerscore3;
	}

	/** 获取知识掌握 */
	public String getTeacherscore1() {
		return this.teacherscore1;
	}

	/** 设置知识掌握 */
	public void setTeacherscore1(String teacherscore1) {
		this.teacherscore1 = teacherscore1;
	}

	/** 获取任务完成互评 */
	public Integer getTeacherscore2() {
		return this.teacherscore2;
	}

	/** 设置任务完成互评 */
	public void setTeacherscore2(Integer teacherscore2) {
		this.teacherscore2 = teacherscore2;
	}

	/** 获取小组合作教师评 */
	public Integer getTeacherscore3() {
		return this.teacherscore3;
	}

	/** 设置小组合作教师评 */
	public void setTeacherscore3(Integer teacherscore3) {
		this.teacherscore3 = teacherscore3;
	}

	/** 获取时间 */
	public Date getTm() {
		return this.tm;
	}

	/** 设置时间 */
	public void setTm(Date tm) {
		this.tm = tm;
	}

	/** 设定时间转载字段值，页面提交时由springmvc自动装载 */
	public void setTmstr(String tm) {
		this.tm = _getTime(tm);
	}

	/** 获取班级id */
	public Integer getClassid() {
		return this.classid;
	}

	/** 设置班级id */
	public void setClassid(Integer classid) {
		this.classid = classid;
	}

	/** 获取任务名称 */
	public String getTaskName() {
		return taskName;
	}

	/** 设置任务名称 */
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	/** 获取自评权重 */
	public Integer getPersonweight() {
		return personweight;
	}

	/** 设置自评权重 */
	public void setPersonweight(Integer personweight) {
		this.personweight = personweight;
	}

	/** 获取互评权重 */
	public Integer getPartnerweight() {
		return partnerweight;
	}

	/** 设置互评权重 */
	public void setPartnerweight(Integer partnerweight) {
		this.partnerweight = partnerweight;
	}

	/** 获取教师权重 */
	public Integer getTeacherweight() {
		return teacherweight;
	}

	/** 设置教师权重 */
	public void setTeacherweight(Integer teacherweight) {
		this.teacherweight = teacherweight;
	}

	/** 获取分数 */
	public Integer getNum() {
		return num;
	}

	/** 设置分数 */
	public void setNum(Integer num) {
		this.num = num;
	}

	/** 获取互评人名称 */
	public String getPartnername() {
		return partnername;
	}

	/** 设置互评人名称 */
	public void setPartnername(String partnername) {
		this.partnername = partnername;
	}
	
	/** 获取教师评分状态 */
	public Integer getTeacherscorestatus() {
		return teacherscorestatus;
	}
	/** 设置教师评分状态 */
	public void setTeacherscorestatus(Integer teacherscorestatus) {
		this.teacherscorestatus = teacherscorestatus;
	}
	/** 获取教师评分 */
	public String getTeacherscores() {
		return teacherscores;
	}
	/** 设置教师评分 */
	public void setTeacherscores(String teacherscores) {
		this.teacherscores = teacherscores;
	}
	/** 获取头像 */
	public String getPhoto_url() {
		return photo_url;
	}
	/** 设置头像 */
	public void setPhoto_url(String photo_url) {
		this.photo_url = photo_url;
	}

	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj = new JSONObject();
		obj.put("id", id);
		obj.put("bookid", bookid);
		obj.put("studid", studid);
		obj.put("name", name);
		obj.put("partnerid", partnerid);
		obj.put("teacherid", teacherid);
		obj.put("personalscore1", personalscore1);
		obj.put("personalscore2", personalscore2);
		obj.put("personalscore3", personalscore3);
		obj.put("partnerscore1", partnerscore1);
		obj.put("partnerscore2", partnerscore2);
		obj.put("partnerscore3", partnerscore3);
		obj.put("teacherscore1", teacherscore1);
		obj.put("teacherscore2", teacherscore2);
		obj.put("teacherscore3", teacherscore3);
		obj.put("personweight", personweight);
		obj.put("partnerweight", partnerweight);
		obj.put("teacherweight", teacherweight);
		obj.put("partnername", partnername);
		obj.put("tm", DateTransfer.toString(tm, "yyyy-MM-dd HH:mm:ss"));
		obj.put("tmstr", DateTransfer.toString(tm, "yyyy-MM-dd HH:mm:ss"));
		obj.put("score", score);
		obj.put("classid", classid);
		obj.put("num", num);
		obj.put("teacherscorestatus", teacherscorestatus);
		obj.put("teacherscores", teacherscores);
		return obj;
	}

	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj = new JSONObject();
		obj.put("id", id);
		obj.put("bookid", bookid);
		obj.put("studid", studid);
		obj.put("name", name);
		obj.put("partnerid", partnerid);
		obj.put("teacherid", teacherid);
		obj.put("personalscore1", personalscore1);
		obj.put("personalscore2", personalscore2);
		obj.put("personalscore3", personalscore3);
		obj.put("partnerscore1", partnerscore1);
		obj.put("partnerscore2", partnerscore2);
		obj.put("partnerscore3", partnerscore3);
		obj.put("teacherscore1", teacherscore1);
		obj.put("teacherscore2", teacherscore2);
		obj.put("teacherscore3", teacherscore3);
		obj.put("personweight", personweight);
		obj.put("partnerweight", partnerweight);
		obj.put("teacherweight", teacherweight);
		obj.put("tmString", tmString);
		obj.put("partnername", partnername);
		obj.put("tm", DateTransfer.toString(tm, "yyyy-MM-dd HH:mm:ss"));
		obj.put("tmstr", DateTransfer.toString(tm, "yyyy-MM-dd HH:mm:ss"));
		obj.put("score", score);
		if(classid!=null){
			obj.put("classid", DictBuffer.getLabel("CC_CLASSES", classid.toString()));
		}
		if(teacherscorestatus!=null){
			obj.put("teacherscorestatus", DictBuffer.getLabel("ZD_SCORESTATUS", teacherscorestatus.toString()));
		}
		obj.put("num", num);
		obj.put("teacherscores", teacherscores);
		obj.put("photo_url", photo_url);
		return obj;
	}
}
