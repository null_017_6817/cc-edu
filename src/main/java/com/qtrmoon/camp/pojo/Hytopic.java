package com.qtrmoon.camp.pojo;
import java.util.Date;
import org.json.simple.JSONObject;
import com.qtrmoon.common.PageForm;
import com.qtrmoon.toolkit.DateTransfer;
import com.qtrmoon.dictionary.DictBuffer;

/** 行业场景实体类 */
public class Hytopic extends PageForm{
	//Fields
	
	private Integer id;// 主键
	private String rid;// 关联id
	private String title;// 标题
	private String content;// 内容
	private Date createtime;// 创建时间
	private String createtimeBeg,createtimeEnd;//时间条件的跨度查询属性
	private String createby;// 创建人
	private Date updatetime;// 修改时间
	private String updatetimeBeg,updatetimeEnd;//时间条件的跨度查询属性
	private String updateby;// 修改人
	//Constructors
	/** default constructor */
	public Hytopic() {
	
	}	
	//getter and setter
	/** 获取主键 */
	public Integer getId() {
		return this.id;
	}
	/** 设置主键 */
	public void setId(Integer id) {
		this.id = id;
	}
	/** 获取关联id */
	public String getRid() {
		return this.rid;
	}
	/** 设置关联id */
	public void setRid(String rid) {
		this.rid = rid;
	}
	/** 获取标题 */
	public String getTitle() {
		return this.title;
	}
	/** 设置标题 */
	public void setTitle(String title) {
		this.title = title;
	}
	/** 获取内容 */
	public String getContent() {
		return this.content;
	}
	/** 设置内容 */
	public void setContent(String content) {
		this.content = content;
	}
	/** 获取创建时间 */
	public Date getCreatetime() {
		return this.createtime;
	}
	/** 设置创建时间 */
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	/** 设定[创建时间]时间转载字段值，页面提交时由springmvc自动装载 */
	public void setCreatetimestr(String tm) {
		this.createtime=_getTime(tm);
	}
	/** 获取[创建时间]时间起始条件 */
	public String getCreatetimeBeg() {
		return createtimeBeg;
	}
	/** 获取[创建时间]时间起始条件(Dao/Mapper查询时调用) */
	public Date getCreatetimeBegDate() {
		return _getBegDate(createtimeBeg);
	}
	/** 设置[创建时间]时间起始条件(表单提交时自动装载) */
	public void setCreatetimeBeg(String createtimeBeg) {
		this.createtimeBeg = createtimeBeg;
	}
	
	/** 获取[创建时间]时间结束条件 */
	public String getCreatetimeEnd() {
		return createtimeEnd;
	}
	/** 获取[创建时间]时间结束条件(Dao/Mapper查询时调用) */
	public Date getCreatetimeEndDate() {
		return _getEndDate(createtimeEnd);
	}
	/** 设置[创建时间]时间结束条件(表单提交时自动装载) */
	public void setCreatetimeEnd(String createtimeEnd) {
		this.createtimeEnd = createtimeEnd;
	}
	/** 获取创建人 */
	public String getCreateby() {
		return this.createby;
	}
	/** 设置创建人 */
	public void setCreateby(String createby) {
		this.createby = createby;
	}
	/** 获取修改时间 */
	public Date getUpdatetime() {
		return this.updatetime;
	}
	/** 设置修改时间 */
	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}
	/** 设定[修改时间]时间转载字段值，页面提交时由springmvc自动装载 */
	public void setUpdatetimestr(String tm) {
		this.updatetime=_getTime(tm);
	}
	/** 获取[修改时间]时间起始条件 */
	public String getUpdatetimeBeg() {
		return updatetimeBeg;
	}
	/** 获取[修改时间]时间起始条件(Dao/Mapper查询时调用) */
	public Date getUpdatetimeBegDate() {
		return _getBegDate(updatetimeBeg);
	}
	/** 设置[修改时间]时间起始条件(表单提交时自动装载) */
	public void setUpdatetimeBeg(String updatetimeBeg) {
		this.updatetimeBeg = updatetimeBeg;
	}
	
	/** 获取[修改时间]时间结束条件 */
	public String getUpdatetimeEnd() {
		return updatetimeEnd;
	}
	/** 获取[修改时间]时间结束条件(Dao/Mapper查询时调用) */
	public Date getUpdatetimeEndDate() {
		return _getEndDate(updatetimeEnd);
	}
	/** 设置[修改时间]时间结束条件(表单提交时自动装载) */
	public void setUpdatetimeEnd(String updatetimeEnd) {
		this.updatetimeEnd = updatetimeEnd;
	}
	/** 获取修改人 */
	public String getUpdateby() {
		return this.updateby;
	}
	/** 设置修改人 */
	public void setUpdateby(String updateby) {
		this.updateby = updateby;
	}
	/** 获取原始数据的Json对象(未替换字典，修改用) */
	public JSONObject getJson() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		obj.put("rid", rid);
		obj.put("title", title);
		obj.put("content", content);
		//时间字段处理。createtime查询列表页使用，createtimestr修改页使用
		obj.put("createtime", DateTransfer.toString(createtime,"yyyy/MM/dd"));
		obj.put("createtimestr", DateTransfer.toString(createtime,"yyyy/MM/dd"));
		obj.put("createby", createby);
		//时间字段处理。updatetime查询列表页使用，updatetimestr修改页使用
		obj.put("updatetime", DateTransfer.toString(updatetime,"yyyy/MM/dd"));
		obj.put("updatetimestr", DateTransfer.toString(updatetime,"yyyy/MM/dd"));
		obj.put("updateby", updateby);
		return obj;
	}
	
	/** 获取数据的Json对象(已替换字典，列表查询用) */
	public JSONObject getJsonInDict() {
		JSONObject obj=new JSONObject();
		obj.put("id", id);
		if(rid != null){
			obj.put("rid", DictBuffer.getLabel("ZD_TRAININDUSTY", rid.toString()));
		}
		obj.put("title", title);
		obj.put("content", content);
		//时间字段处理。createtime查询列表页使用，createtimestr修改页使用
		obj.put("createtime", DateTransfer.toString(createtime,"yyyy-MM-dd hh:mm:ss"));
		obj.put("createtimestr", DateTransfer.toString(createtime,"yyyy-MM-dd hh:mm:ss"));
		obj.put("createby", createby);
		//时间字段处理。updatetime查询列表页使用，updatetimestr修改页使用
		obj.put("updatetime", DateTransfer.toString(updatetime,"yyyy-MM-dd hh:mm:ss"));
		obj.put("updatetimestr", DateTransfer.toString(updatetime,"yyyy-MM-dd hh:mm:ss"));
		obj.put("updateby", updateby);
		return obj;
	}
}
