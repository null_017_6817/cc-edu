package com.qtrmoon.camp.constant;

/**
 * 电信问卷题型枚举
 */
public enum DxwjQuestionType {
	DANXUANTI("单选题",1),// 单选
	DUOXUANTI("多选题",2),// 多选
	WENDATI("问答题",3);// 问答
	
	private String name ;// 名称
	private int code ;// 码值
	// 构造函数
	private DxwjQuestionType( String name , int index ){
		this.name = name ;// 赋值名称
		this.code = index ;// 赋值码值
	}
	// 获取名称
	public String getName() {
		return name;
	}
	// 设置名称
	public void setName(String name) {
		this.name = name;
	}
	// 获取码值
	public int getCode() {
		return code;
	}
	// 设置码值
	public void setCode(int code) {
		this.code = code;
	}
}
